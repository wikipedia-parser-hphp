
#ifndef __GENERATED_cls_testrecorder_h__
#define __GENERATED_cls_testrecorder_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 44716 */
class c_testrecorder : virtual public ObjectData {
  BEGIN_CLASS_MAP(testrecorder)
  END_CLASS_MAP(testrecorder)
  DECLARE_CLASS(testrecorder, TestRecorder, ObjectData)
  void init();
  public: Variant m_parent;
  public: Variant m_term;
  public: void t___construct(Variant v_parent);
  public: ObjectData *create(Variant v_parent);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_start();
  public: void t_record(CVarRef v_test, CVarRef v_result);
  public: void t_end();
  public: void t_report();
  public: bool t_reportpercentage(CVarRef v_success, CVarRef v_total);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_testrecorder_h__
