
#ifndef __GENERATED_cls_parser_h__
#define __GENERATED_cls_parser_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 35180 */
class c_parser : virtual public ObjectData {
  BEGIN_CLASS_MAP(parser)
  END_CLASS_MAP(parser)
  DECLARE_CLASS(parser, Parser, ObjectData)
  void init();
  public: Variant m_mTagHooks;
  public: Variant m_mTransparentTagHooks;
  public: Variant m_mFunctionHooks;
  public: Variant m_mFunctionSynonyms;
  public: Variant m_mVariables;
  public: Variant m_mImageParams;
  public: Variant m_mImageParamsMagicArray;
  public: Variant m_mStripList;
  public: Variant m_mMarkerIndex;
  public: Variant m_mPreprocessor;
  public: Variant m_mExtLinkBracketedRegex;
  public: Variant m_mUrlProtocols;
  public: Variant m_mDefaultStripList;
  public: Variant m_mVarCache;
  public: Variant m_mConf;
  public: Variant m_mOutput;
  public: Variant m_mAutonumber;
  public: Variant m_mDTopen;
  public: Variant m_mStripState;
  public: Variant m_mIncludeCount;
  public: Variant m_mArgStack;
  public: Variant m_mLastSection;
  public: Variant m_mInPre;
  public: Variant m_mLinkHolders;
  public: Variant m_mLinkID;
  public: Variant m_mIncludeSizes;
  public: Variant m_mPPNodeCount;
  public: Variant m_mDefaultSort;
  public: Variant m_mTplExpandCache;
  public: Variant m_mTplRedirCache;
  public: Variant m_mTplDomCache;
  public: Variant m_mHeadings;
  public: Variant m_mDoubleUnderscores;
  public: Variant m_mExpensiveFunctionCount;
  public: Variant m_mFileCache;
  public: Variant m_mOptions;
  public: Variant m_mTitle;
  public: Variant m_mOutputType;
  public: Variant m_ot;
  public: Variant m_mRevisionId;
  public: Variant m_mRevisionTimestamp;
  public: Variant m_mRevIdForTs;
  public: virtual void destruct();
  public: void t___construct(Variant v_conf = ScalarArrays::sa_[0]);
  public: ObjectData *create(Variant v_conf = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t___destruct();
  public: void t_firstcallinit();
  public: void t_clearstate();
  public: void t_setoutputtype(CVarRef v_ot);
  public: void t_settitle(Variant v_t);
  public: Variant t_uniqprefix();
  public: Variant t_parse(Variant v_text, p_title v_title, p_parseroptions v_options, CVarRef v_linestart = true, CVarRef v_clearState = true, CVarRef v_revid = null_variant);
  public: Variant t_recursivetagparse(Variant v_text);
  public: Variant t_preprocess(Variant v_text, CVarRef v_title, CVarRef v_options, CVarRef v_revid = null_variant);
  public: String t_getrandomstring();
  public: Variant t_gettitle();
  public: Variant t_getoptions();
  public: Variant t_getrevisionid();
  public: Variant t_getoutput();
  public: Primitive t_nextlinkid();
  public: Variant t_getfunctionlang();
  public: Variant t_getpreprocessor();
  public: String t_extracttagsandparams(CVarRef v_elements, Variant v_text, Variant v_matches, CVarRef v_uniq_prefix = "");
  public: Variant t_getstriplist();
  public: Variant t_strip(CVarRef v_text, CVarRef v_state, bool v_stripcomments = false, CArrRef v_dontstrip = ScalarArrays::sa_[0]);
  public: Variant t_unstrip(Variant v_text, Object v_state);
  public: Variant t_unstripnowiki(Variant v_text, CVarRef v_state);
  public: Variant t_unstripforhtml(Variant v_text);
  public: Variant t_insertstripitem(Variant v_text);
  public: static Variant ti_tidy(const char* cls, CVarRef v_text);
  public: Variant t_dotablestuff(CVarRef v_text);
  public: Variant t_internalparse(Variant v_text);
  public: Variant t_domagiclinks(Variant v_text);
  public: Variant t_magiclinkcallback(CVarRef v_m);
  public: String t_makefreeexternallink(Variant v_url);
  public: Variant t_doheadings(Variant v_text);
  public: Variant t_doallquotes(CVarRef v_text);
  public: Variant t_doquotes(CVarRef v_text);
  public: Variant t_replaceexternallinks(Variant v_text);
  public: Variant t_getexternallinkattribs(CVarRef v_url = false);
  public: static Variant ti_replaceunusualescapes(const char* cls, CVarRef v_url);
  public: static Variant ti_replaceunusualescapescallback(const char* cls, CVarRef v_matches);
  public: Variant t_maybemakeexternalimage(Variant v_url);
  public: Variant t_replaceinternallinks(Variant v_s);
  public: p_linkholderarray t_replaceinternallinks2(Variant v_s);
  public: Variant t_makelinkholder(Variant v_nt, Variant v_text = "", Variant v_query = "", Variant v_trail = "", Variant v_prefix = "");
  public: String t_makeknownlinkholder(Variant v_nt, Variant v_text = "", Variant v_query = "", Variant v_trail = "", Variant v_prefix = "");
  public: Variant t_armorlinks(CVarRef v_text);
  public: bool t_aresubpagesallowed();
  public: Variant t_maybedosubpagelink(Variant v_target, Variant v_text);
  public: String t_closeparagraph();
  public: int64 t_getcommon(CVarRef v_st1, CVarRef v_st2);
  public: String t_openlist(CVarRef v_char);
  public: String t_nextitem(CVarRef v_char);
  public: String t_closelist(CVarRef v_char);
  public: Variant t_doblocklevels(CVarRef v_text, Variant v_linestart);
  public: Variant t_findcolonnolinks(CVarRef v_str, Variant v_before, Variant v_after);
  public: Variant t_getvariablevalue(Variant v_index);
  public: void t_initialisevariables();
  public: Variant t_preprocesstodom(Variant v_text, Variant v_flags = 0LL);
  public: static Array ti_splitwhitespace(const char* cls, CVarRef v_s);
  public: Variant t_replacevariables(Variant v_text, Variant v_frame = false, CVarRef v_argsOnly = false);
  public: static Variant ti_createassocargs(const char* cls, CArrRef v_args);
  public: void t_limitationwarn(CStrRef v_limitationType, CVarRef v_current = null_variant, CVarRef v_max = null_variant);
  public: Variant t_bracesubstitution(Variant v_piece, Variant v_frame);
  public: Array t_gettemplatedom(Variant v_title);
  public: Array t_fetchtemplateandtitle(CVarRef v_title);
  public: Variant t_fetchtemplate(CVarRef v_title);
  public: static Array ti_statelessfetchtemplate(const char* cls, Variant v_title, CVarRef v_parser = false);
  public: Variant t_interwikitransclude(CVarRef v_title, CStrRef v_action);
  public: Variant t_fetchscarytemplatemaybefromcache(CVarRef v_url);
  public: Array t_argsubstitution(Variant v_piece, CVarRef v_frame);
  public: Variant t_extensionsubstitution(Variant v_params, CVarRef v_frame);
  public: Variant t_incrementincludesize(Variant v_type, int v_size);
  public: bool t_incrementexpensivefunctioncount();
  public: Variant t_dodoubleunderscore(Variant v_text);
  public: Variant t_formatheadings(CVarRef v_text, bool v_isMain = true);
  public: Variant t_presavetransform(Variant v_text, Variant v_title, CVarRef v_user, CVarRef v_options, CVarRef v_clearState = true);
  public: Variant t_pstpass2(Variant v_text, Variant v_user);
  public: Variant t_getusersig(Variant v_user);
  public: Variant t_validatesig(CVarRef v_text);
  public: Variant t_cleansig(Variant v_text, bool v_parsing = false);
  public: Variant t_cleansiginsig(Variant v_text);
  public: void t_startexternalparse(Variant v_title, CVarRef v_options, CVarRef v_outputType, bool v_clearState = true);
  public: Variant t_transformmsg(Variant v_text, CVarRef v_options);
  public: Variant t_sethook(Variant v_tag, CVarRef v_callback);
  public: Variant t_settransparenttaghook(String v_tag, CVarRef v_callback);
  public: void t_cleartaghooks();
  public: Variant t_setfunctionhook(CVarRef v_id, CVarRef v_callback, CVarRef v_flags = 0LL);
  public: Variant t_getfunctionhooks();
  public: Variant t_replacelinkholders(Variant v_text, int64 v_options = 0LL);
  public: Variant t_replacelinkholderstext(Variant v_text);
  public: String t_renderpretag(CVarRef v_text, Variant v_attribs);
  public: Variant t_renderimagegallery(CVarRef v_text, Variant v_params);
  public: Array t_getimageparams(CVarRef v_handler);
  public: Variant t_makeimage(Variant v_title, CVarRef v_options, CVarRef v_holders = false);
  public: Variant t_stripalttext(Variant v_caption, CVarRef v_holders);
  public: void t_disablecache();
  public: Variant t_attributestripcallback(Variant v_text, CVarRef v_frame = false);
  public: Variant t_title(CVarRef v_x = null_variant);
  public: Variant t_options(CVarRef v_x = null_variant);
  public: Variant t_outputtype(CVarRef v_x = null_variant);
  public: Variant t_gettags();
  public: Variant t_extractsections(CVarRef v_text, CVarRef v_section, CStrRef v_mode, CVarRef v_newText = "");
  public: Variant t_getsection(CVarRef v_text, CVarRef v_section, CVarRef v_deftext = "");
  public: Variant t_replacesection(CVarRef v_oldtext, CVarRef v_section, CVarRef v_text);
  public: Variant t_getrevisiontimestamp();
  public: void t_setdefaultsort(CVarRef v_sort);
  public: Variant t_getdefaultsort();
  public: Variant t_getcustomdefaultsort();
  public: Variant t_guesssectionnamefromwikitext(Variant v_text);
  public: Variant t_stripsectionname(Variant v_text);
  public: Variant t_srvus(CVarRef v_text);
  public: Variant t_testsrvus(Variant v_text, Variant v_title, CVarRef v_options, int64 v_outputType = 1LL /* parser::OT_HTML */);
  public: Variant t_testpst(CVarRef v_text, Variant v_title, CVarRef v_options);
  public: Variant t_testpreprocess(CVarRef v_text, Variant v_title, CVarRef v_options);
  public: String t_markerskipcallback(CVarRef v_s, CVarRef v_callback);
  public: Variant t_serialisehalfparsedtext(CVarRef v_text);
  public: Variant t_unserialisehalfparsedtext(CVarRef v_data, Variant v_intPrefix = null);
  public: static Variant t_replaceunusualescapescallback(CVarRef v_matches) { return ti_replaceunusualescapescallback("parser", v_matches); }
  public: static Array t_statelessfetchtemplate(CVarRef v_title, CVarRef v_parser = false) { return ti_statelessfetchtemplate("parser", v_title, v_parser); }
  public: static Variant t_replaceunusualescapes(CVarRef v_url) { return ti_replaceunusualescapes("parser", v_url); }
  public: static Array t_splitwhitespace(CVarRef v_s) { return ti_splitwhitespace("parser", v_s); }
  public: static Variant t_createassocargs(CArrRef v_args) { return ti_createassocargs("parser", v_args); }
  public: static Variant t_tidy(CVarRef v_text) { return ti_tidy("parser", v_text); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parser_h__
