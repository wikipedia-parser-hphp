
#include <php/Parser.h>
#include <php/Preprocessor.h>
#include <php/Preprocessor_DOM.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: Preprocessor_DOM.php line 789 */
Variant c_ppdpart::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppdpart::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ppdpart::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("out", m_out.isReferenced() ? ref(m_out) : m_out));
  c_ObjectData::o_get(props);
}
bool c_ppdpart::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x7C801AC012E9F722LL, out, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppdpart::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7C801AC012E9F722LL, m_out,
                         out, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppdpart::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x7C801AC012E9F722LL, m_out,
                      out, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppdpart::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x7C801AC012E9F722LL, m_out,
                         out, 3);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppdpart::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppdpart)
ObjectData *c_ppdpart::create(Variant v_out //  = ""
) {
  init();
  t___construct(v_out);
  return this;
}
ObjectData *c_ppdpart::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_ppdpart::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_ppdpart::cloneImpl() {
  c_ppdpart *obj = NEW(c_ppdpart)();
  cloneSet(obj);
  return obj;
}
void c_ppdpart::cloneSet(c_ppdpart *clone) {
  clone->m_out = m_out.isReferenced() ? ref(m_out) : m_out;
  ObjectData::cloneSet(clone);
}
Variant c_ppdpart::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppdpart::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppdpart::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppdpart$os_get(const char *s) {
  return c_ppdpart::os_get(s, -1);
}
Variant &cw_ppdpart$os_lval(const char *s) {
  return c_ppdpart::os_lval(s, -1);
}
Variant cw_ppdpart$os_constant(const char *s) {
  return c_ppdpart::os_constant(s);
}
Variant cw_ppdpart$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppdpart::os_invoke(c, s, params, -1, fatal);
}
void c_ppdpart::init() {
  m_out = null;
}
/* SRC: Preprocessor_DOM.php line 797 */
void c_ppdpart::t___construct(Variant v_out //  = ""
) {
  INSTANCE_METHOD_INJECTION(PPDPart, PPDPart::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_out = v_out);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_DOM.php line 720 */
Variant c_ppdstackelement::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppdstackelement::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ppdstackelement::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("open", m_open.isReferenced() ? ref(m_open) : m_open));
  props.push_back(NEW(ArrayElement)("close", m_close.isReferenced() ? ref(m_close) : m_close));
  props.push_back(NEW(ArrayElement)("count", m_count.isReferenced() ? ref(m_count) : m_count));
  props.push_back(NEW(ArrayElement)("parts", m_parts.isReferenced() ? ref(m_parts) : m_parts));
  props.push_back(NEW(ArrayElement)("lineStart", m_lineStart.isReferenced() ? ref(m_lineStart) : m_lineStart));
  props.push_back(NEW(ArrayElement)("partClass", m_partClass.isReferenced() ? ref(m_partClass) : m_partClass));
  c_ObjectData::o_get(props);
}
bool c_ppdstackelement::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_EXISTS_STRING(0x2DAB7B8AC94308B1LL, partClass, 9);
      break;
    case 5:
      HASH_EXISTS_STRING(0x0F84CB175598D0A5LL, lineStart, 9);
      break;
    case 6:
      HASH_EXISTS_STRING(0x6A760D2EC60228C6LL, open, 4);
      break;
    case 11:
      HASH_EXISTS_STRING(0x36B43082F052EC6BLL, close, 5);
      HASH_EXISTS_STRING(0x3D66B5980D54BABBLL, count, 5);
      break;
    case 13:
      HASH_EXISTS_STRING(0x2708FDA74562AD8DLL, parts, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppdstackelement::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_RETURN_STRING(0x2DAB7B8AC94308B1LL, m_partClass,
                         partClass, 9);
      break;
    case 5:
      HASH_RETURN_STRING(0x0F84CB175598D0A5LL, m_lineStart,
                         lineStart, 9);
      break;
    case 6:
      HASH_RETURN_STRING(0x6A760D2EC60228C6LL, m_open,
                         open, 4);
      break;
    case 11:
      HASH_RETURN_STRING(0x36B43082F052EC6BLL, m_close,
                         close, 5);
      HASH_RETURN_STRING(0x3D66B5980D54BABBLL, m_count,
                         count, 5);
      break;
    case 13:
      HASH_RETURN_STRING(0x2708FDA74562AD8DLL, m_parts,
                         parts, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppdstackelement::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_SET_STRING(0x2DAB7B8AC94308B1LL, m_partClass,
                      partClass, 9);
      break;
    case 5:
      HASH_SET_STRING(0x0F84CB175598D0A5LL, m_lineStart,
                      lineStart, 9);
      break;
    case 6:
      HASH_SET_STRING(0x6A760D2EC60228C6LL, m_open,
                      open, 4);
      break;
    case 11:
      HASH_SET_STRING(0x36B43082F052EC6BLL, m_close,
                      close, 5);
      HASH_SET_STRING(0x3D66B5980D54BABBLL, m_count,
                      count, 5);
      break;
    case 13:
      HASH_SET_STRING(0x2708FDA74562AD8DLL, m_parts,
                      parts, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppdstackelement::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 1:
      HASH_RETURN_STRING(0x2DAB7B8AC94308B1LL, m_partClass,
                         partClass, 9);
      break;
    case 5:
      HASH_RETURN_STRING(0x0F84CB175598D0A5LL, m_lineStart,
                         lineStart, 9);
      break;
    case 6:
      HASH_RETURN_STRING(0x6A760D2EC60228C6LL, m_open,
                         open, 4);
      break;
    case 11:
      HASH_RETURN_STRING(0x36B43082F052EC6BLL, m_close,
                         close, 5);
      HASH_RETURN_STRING(0x3D66B5980D54BABBLL, m_count,
                         count, 5);
      break;
    case 13:
      HASH_RETURN_STRING(0x2708FDA74562AD8DLL, m_parts,
                         parts, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppdstackelement::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppdstackelement)
ObjectData *c_ppdstackelement::create(Variant v_data //  = ScalarArrays::sa_[0]
) {
  init();
  t___construct(v_data);
  return this;
}
ObjectData *c_ppdstackelement::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_ppdstackelement::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_ppdstackelement::cloneImpl() {
  c_ppdstackelement *obj = NEW(c_ppdstackelement)();
  cloneSet(obj);
  return obj;
}
void c_ppdstackelement::cloneSet(c_ppdstackelement *clone) {
  clone->m_open = m_open.isReferenced() ? ref(m_open) : m_open;
  clone->m_close = m_close.isReferenced() ? ref(m_close) : m_close;
  clone->m_count = m_count.isReferenced() ? ref(m_count) : m_count;
  clone->m_parts = m_parts.isReferenced() ? ref(m_parts) : m_parts;
  clone->m_lineStart = m_lineStart.isReferenced() ? ref(m_lineStart) : m_lineStart;
  clone->m_partClass = m_partClass.isReferenced() ? ref(m_partClass) : m_partClass;
  ObjectData::cloneSet(clone);
}
Variant c_ppdstackelement::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x27E7DBA875AD17E1LL, getflags) {
        return (t_getflags());
      }
      break;
    case 2:
      HASH_GUARD(0x499E72B719A4CAC2LL, addpart) {
        int count = params.size();
        if (count <= 0) return (t_addpart(), null);
        return (t_addpart(params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x1A5F5A2CB292E516LL, getaccum) {
        return ref(t_getaccum());
      }
      break;
    case 9:
      HASH_GUARD(0x1CF88FA5EFC88C39LL, breaksyntax) {
        int count = params.size();
        if (count <= 0) return (t_breaksyntax());
        return (t_breaksyntax(params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x1726802D706D7BECLL, getcurrentpart) {
        return (t_getcurrentpart());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppdstackelement::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x27E7DBA875AD17E1LL, getflags) {
        return (t_getflags());
      }
      break;
    case 2:
      HASH_GUARD(0x499E72B719A4CAC2LL, addpart) {
        if (count <= 0) return (t_addpart(), null);
        return (t_addpart(a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x1A5F5A2CB292E516LL, getaccum) {
        return ref(t_getaccum());
      }
      break;
    case 9:
      HASH_GUARD(0x1CF88FA5EFC88C39LL, breaksyntax) {
        if (count <= 0) return (t_breaksyntax());
        return (t_breaksyntax(a0));
      }
      break;
    case 12:
      HASH_GUARD(0x1726802D706D7BECLL, getcurrentpart) {
        return (t_getcurrentpart());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppdstackelement::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppdstackelement$os_get(const char *s) {
  return c_ppdstackelement::os_get(s, -1);
}
Variant &cw_ppdstackelement$os_lval(const char *s) {
  return c_ppdstackelement::os_lval(s, -1);
}
Variant cw_ppdstackelement$os_constant(const char *s) {
  return c_ppdstackelement::os_constant(s);
}
Variant cw_ppdstackelement$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppdstackelement::os_invoke(c, s, params, -1, fatal);
}
void c_ppdstackelement::init() {
  m_open = null;
  m_close = null;
  m_count = null;
  m_parts = null;
  m_lineStart = null;
  m_partClass = "PPDPart";
}
/* SRC: Preprocessor_DOM.php line 729 */
void c_ppdstackelement::t___construct(Variant v_data //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(PPDStackElement, PPDStackElement::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant v_class;
  Primitive v_name = 0;
  Variant v_value;

  (v_class = m_partClass);
  (m_parts = Array(ArrayInit(1).set(0, LINE(731,create_object(toString(v_class), Array()))).create()));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_data.begin("ppdstackelement"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3->second();
      v_name = iter3->first();
      {
        (o_lval(toString(v_name), -1LL) = v_value);
      }
    }
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_DOM.php line 738 */
Variant c_ppdstackelement::t_getaccum() {
  INSTANCE_METHOD_INJECTION(PPDStackElement, PPDStackElement::getAccum);
  return ref(lval(m_parts.rvalAt(LINE(739,x_count(m_parts)) - 1LL).o_lval("out", 0x7C801AC012E9F722LL)));
} /* function */
/* SRC: Preprocessor_DOM.php line 742 */
void c_ppdstackelement::t_addpart(Variant v_s //  = ""
) {
  INSTANCE_METHOD_INJECTION(PPDStackElement, PPDStackElement::addPart);
  Variant v_class;

  (v_class = m_partClass);
  m_parts.append((LINE(744,create_object(toString(v_class), Array(ArrayInit(1).set(0, ref(v_s)).create())))));
} /* function */
/* SRC: Preprocessor_DOM.php line 747 */
Variant c_ppdstackelement::t_getcurrentpart() {
  INSTANCE_METHOD_INJECTION(PPDStackElement, PPDStackElement::getCurrentPart);
  return m_parts.rvalAt(LINE(748,x_count(m_parts)) - 1LL);
} /* function */
/* SRC: Preprocessor_DOM.php line 751 */
Array c_ppdstackelement::t_getflags() {
  INSTANCE_METHOD_INJECTION(PPDStackElement, PPDStackElement::getFlags);
  int v_partCount = 0;
  bool v_findPipe = false;

  (v_partCount = LINE(752,x_count(m_parts)));
  (v_findPipe = !equal(m_open, "\n") && !equal(m_open, "["));
  return Array(ArrayInit(3).set(0, "findPipe", v_findPipe, 0x5AE3699687C882F9LL).set(1, "findEquals", v_findPipe && more(v_partCount, 1LL) && !(toObject(m_parts.rvalAt(v_partCount - 1LL))->t___isset("eqpos")), 0x1B3A1B4FC8E2BF86LL).set(2, "inHeading", equal(m_open, "\n"), 0x73F30E0F2D277D2DLL).create());
} /* function */
/* SRC: Preprocessor_DOM.php line 764 */
Variant c_ppdstackelement::t_breaksyntax(Variant v_openingCount //  = false
) {
  INSTANCE_METHOD_INJECTION(PPDStackElement, PPDStackElement::breakSyntax);
  Variant v_s;
  bool v_first = false;
  Variant v_part;

  if (equal(m_open, "\n")) {
    (v_s = m_parts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("out", 0x7C801AC012E9F722LL));
  }
  else {
    if (same(v_openingCount, false)) {
      (v_openingCount = m_count);
    }
    (v_s = LINE(771,x_str_repeat(toString(m_open), toInt32(v_openingCount))));
    (v_first = true);
    {
      LOOP_COUNTER(4);
      Variant map5 = m_parts;
      for (ArrayIterPtr iter6 = map5.begin("ppdstackelement"); !iter6->end(); iter6->next()) {
        LOOP_COUNTER_CHECK(4);
        v_part = iter6->second();
        {
          if (v_first) {
            (v_first = false);
          }
          else {
            concat_assign(v_s, "|");
          }
          concat_assign(v_s, toString(v_part.o_get("out", 0x7C801AC012E9F722LL)));
        }
      }
    }
  }
  return v_s;
} /* function */
/* SRC: Preprocessor_DOM.php line 1389 */
Variant c_ppnode_dom::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppnode_dom::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ppnode_dom::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("node", m_node.isReferenced() ? ref(m_node) : m_node));
  c_ObjectData::o_get(props);
}
bool c_ppnode_dom::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x75AF6DCF22783AC1LL, node, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppnode_dom::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x75AF6DCF22783AC1LL, m_node,
                         node, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppnode_dom::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x75AF6DCF22783AC1LL, m_node,
                      node, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppnode_dom::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x75AF6DCF22783AC1LL, m_node,
                         node, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppnode_dom::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppnode_dom)
ObjectData *c_ppnode_dom::create(Variant v_node, Variant v_xpath //  = false
) {
  init();
  t___construct(v_node, v_xpath);
  return this;
}
ObjectData *c_ppnode_dom::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_ppnode_dom::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_ppnode_dom::cloneImpl() {
  c_ppnode_dom *obj = NEW(c_ppnode_dom)();
  cloneSet(obj);
  return obj;
}
void c_ppnode_dom::cloneSet(c_ppnode_dom *clone) {
  clone->m_node = m_node.isReferenced() ? ref(m_node) : m_node;
  ObjectData::cloneSet(clone);
}
Variant c_ppnode_dom::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 0:
      HASH_GUARD(0x73E7C3304C0C5360LL, splitarg) {
        return (t_splitarg());
      }
      break;
    case 5:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 6:
      HASH_GUARD(0x0A41DBE0830902C6LL, item) {
        return (t_item(params.rvalAt(0)));
      }
      break;
    case 9:
      HASH_GUARD(0x050681239965E069LL, getfirstchild) {
        return (t_getfirstchild());
      }
      break;
    case 12:
      HASH_GUARD(0x47D4EE3CB3EB696CLL, splitext) {
        return (t_splitext());
      }
      break;
    case 13:
      HASH_GUARD(0x0621AE2D22A1922DLL, splitheading) {
        return (t_splitheading());
      }
      break;
    case 14:
      HASH_GUARD(0x5ABDC66291F1CA2ELL, getchildrenoftype) {
        return (t_getchildrenoftype(params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x732EC1BDA8EC520FLL, getchildren) {
        return (t_getchildren());
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 20:
      HASH_GUARD(0x41EB078EF92C44D4LL, getlength) {
        return (t_getlength());
      }
      break;
    case 25:
      HASH_GUARD(0x4CB333CF2D880119LL, getnextsibling) {
        return (t_getnextsibling());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppnode_dom::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 0:
      HASH_GUARD(0x73E7C3304C0C5360LL, splitarg) {
        return (t_splitarg());
      }
      break;
    case 5:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 6:
      HASH_GUARD(0x0A41DBE0830902C6LL, item) {
        return (t_item(a0));
      }
      break;
    case 9:
      HASH_GUARD(0x050681239965E069LL, getfirstchild) {
        return (t_getfirstchild());
      }
      break;
    case 12:
      HASH_GUARD(0x47D4EE3CB3EB696CLL, splitext) {
        return (t_splitext());
      }
      break;
    case 13:
      HASH_GUARD(0x0621AE2D22A1922DLL, splitheading) {
        return (t_splitheading());
      }
      break;
    case 14:
      HASH_GUARD(0x5ABDC66291F1CA2ELL, getchildrenoftype) {
        return (t_getchildrenoftype(a0));
      }
      break;
    case 15:
      HASH_GUARD(0x732EC1BDA8EC520FLL, getchildren) {
        return (t_getchildren());
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 20:
      HASH_GUARD(0x41EB078EF92C44D4LL, getlength) {
        return (t_getlength());
      }
      break;
    case 25:
      HASH_GUARD(0x4CB333CF2D880119LL, getnextsibling) {
        return (t_getnextsibling());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppnode_dom::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppnode_dom$os_get(const char *s) {
  return c_ppnode_dom::os_get(s, -1);
}
Variant &cw_ppnode_dom$os_lval(const char *s) {
  return c_ppnode_dom::os_lval(s, -1);
}
Variant cw_ppnode_dom$os_constant(const char *s) {
  return c_ppnode_dom::os_constant(s);
}
Variant cw_ppnode_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppnode_dom::os_invoke(c, s, params, -1, fatal);
}
void c_ppnode_dom::init() {
  m_node = null;
}
/* SRC: Preprocessor_DOM.php line 1392 */
void c_ppnode_dom::t___construct(Variant v_node, Variant v_xpath //  = false
) {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_node = v_node);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_DOM.php line 1396 */
Variant c_ppnode_dom::t___get(Variant v_name) {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::__get);
  if (equal(v_name, "xpath")) {
    (o_lval("xpath", 0x1359AF361FE1E1EALL) = ((Object)(LINE(1398,p_domxpath(p_domxpath(NEWOBJ(c_domxpath)())->create(m_node.o_get("ownerDocument", 0x0222D017809853C8LL)))))));
  }
  return o_get("xpath", 0x1359AF361FE1E1EALL);
} /* function */
/* SRC: Preprocessor_DOM.php line 1396 */
Variant &c_ppnode_dom::___lval(Variant v_name) {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::__get);
  Variant &v = get_global_variables()->__lvalProxy;
  v = t___get(v_name);
  return v;
} /* function */
/* SRC: Preprocessor_DOM.php line 1403 */
String c_ppnode_dom::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::__toString);
  Variant v_s;
  Variant v_node;

  if (instanceOf(m_node, "DOMNodeList")) {
    (v_s = "");
    {
      LOOP_COUNTER(7);
      Variant map8 = m_node;
      for (ArrayIterPtr iter9 = map8.begin("ppnode_dom"); !iter9->end(); iter9->next()) {
        LOOP_COUNTER_CHECK(7);
        v_node = iter9->second();
        {
          concat_assign(v_s, toString(LINE(1407,v_node.o_get("ownerDocument", 0x0222D017809853C8LL).o_invoke_few_args("saveXML", 0x26D66F56DDDC32E5LL, 1, v_node))));
        }
      }
    }
  }
  else {
    (v_s = LINE(1410,m_node.o_get("ownerDocument", 0x0222D017809853C8LL).o_invoke_few_args("saveXML", 0x26D66F56DDDC32E5LL, 1, m_node)));
  }
  return toString(v_s);
} /* function */
/* SRC: Preprocessor_DOM.php line 1415 */
Variant c_ppnode_dom::t_getchildren() {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::getChildren);
  return toBoolean(m_node.o_get("childNodes", 0x795010C5D07D8B8ELL)) ? ((Variant)(LINE(1416,create_object("self", Array(ArrayInit(1).set(0, m_node.o_get("childNodes", 0x795010C5D07D8B8ELL)).create()))))) : ((Variant)(false));
} /* function */
/* SRC: Preprocessor_DOM.php line 1419 */
Variant c_ppnode_dom::t_getfirstchild() {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::getFirstChild);
  return toBoolean(m_node.o_get("firstChild", 0x22862CD26C0AAB50LL)) ? ((Variant)(LINE(1420,create_object("self", Array(ArrayInit(1).set(0, m_node.o_get("firstChild", 0x22862CD26C0AAB50LL)).create()))))) : ((Variant)(false));
} /* function */
/* SRC: Preprocessor_DOM.php line 1423 */
Variant c_ppnode_dom::t_getnextsibling() {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::getNextSibling);
  return toBoolean(m_node.o_get("nextSibling", 0x0B3EC7643EE81822LL)) ? ((Variant)(LINE(1424,create_object("self", Array(ArrayInit(1).set(0, m_node.o_get("nextSibling", 0x0B3EC7643EE81822LL)).create()))))) : ((Variant)(false));
} /* function */
/* SRC: Preprocessor_DOM.php line 1427 */
Object c_ppnode_dom::t_getchildrenoftype(Variant v_type) {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::getChildrenOfType);
  return LINE(1428,create_object("self", Array(ArrayInit(1).set(0, o_get("xpath", 0x1359AF361FE1E1EALL).o_invoke_few_args("query", 0x356758D4414DA377LL, 2, v_type, m_node)).create())));
} /* function */
/* SRC: Preprocessor_DOM.php line 1431 */
Variant c_ppnode_dom::t_getlength() {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::getLength);
  if (instanceOf(m_node, "DOMNodeList")) {
    return m_node.o_get("length", 0x2993E8EB119CAB21LL);
  }
  else {
    return false;
  }
  return null;
} /* function */
/* SRC: Preprocessor_DOM.php line 1439 */
Variant c_ppnode_dom::t_item(Variant v_i) {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::item);
  Variant v_item;

  (v_item = LINE(1440,m_node.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, v_i)));
  return toBoolean(v_item) ? ((Variant)(LINE(1441,create_object("self", Array(ArrayInit(1).set(0, v_item).create()))))) : ((Variant)(false));
} /* function */
/* SRC: Preprocessor_DOM.php line 1444 */
Variant c_ppnode_dom::t_getname() {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::getName);
  if (instanceOf(m_node, "DOMNodeList")) {
    return "#nodelist";
  }
  else {
    return m_node.o_get("nodeName", 0x009B66FACA616B3CLL);
  }
  return null;
} /* function */
/* SRC: Preprocessor_DOM.php line 1458 */
Array c_ppnode_dom::t_splitarg() {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::splitArg);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_names;
  Variant v_values;
  Variant v_name;
  Variant v_index;

  (v_names = LINE(1459,o_get("xpath", 0x1359AF361FE1E1EALL).o_invoke_few_args("query", 0x356758D4414DA377LL, 2, "name", m_node)));
  (v_values = LINE(1460,o_get("xpath", 0x1359AF361FE1E1EALL).o_invoke_few_args("query", 0x356758D4414DA377LL, 2, "value", m_node)));
  if (!(toBoolean(v_names.o_get("length", 0x2993E8EB119CAB21LL))) || !(toBoolean(v_values.o_get("length", 0x2993E8EB119CAB21LL)))) {
    throw_exception(LINE(1462,create_object("mwexception", Array(ArrayInit(1).set(0, "Invalid brace node passed to PPNode_DOM::splitArg").create()))));
  }
  (v_name = LINE(1464,v_names.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)));
  (v_index = LINE(1465,v_name.o_invoke_few_args("getAttribute", 0x49F89C466612FC28LL, 1, "index")));
  return (assignCallTemp(eo_0, LINE(1467,create_object("self", Array(ArrayInit(1).set(0, v_name).create())))),assignCallTemp(eo_1, v_index),assignCallTemp(eo_2, LINE(1469,create_object("self", Array(ArrayInit(1).set(0, v_values.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)).create())))),Array(ArrayInit(3).set(0, "name", eo_0, 0x0BCDB293DC3CBDDCLL).set(1, "index", eo_1, 0x440D5888C0FF3081LL).set(2, "value", eo_2, 0x69E7413AE0C88471LL).create()));
} /* function */
/* SRC: Preprocessor_DOM.php line 1476 */
Variant c_ppnode_dom::t_splitext() {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::splitExt);
  Variant eo_0;
  Variant eo_1;
  Variant v_names;
  Variant v_attrs;
  Variant v_inners;
  Variant v_closes;
  Variant v_parts;

  (v_names = LINE(1477,o_get("xpath", 0x1359AF361FE1E1EALL).o_invoke_few_args("query", 0x356758D4414DA377LL, 2, "name", m_node)));
  (v_attrs = LINE(1478,o_get("xpath", 0x1359AF361FE1E1EALL).o_invoke_few_args("query", 0x356758D4414DA377LL, 2, "attr", m_node)));
  (v_inners = LINE(1479,o_get("xpath", 0x1359AF361FE1E1EALL).o_invoke_few_args("query", 0x356758D4414DA377LL, 2, "inner", m_node)));
  (v_closes = LINE(1480,o_get("xpath", 0x1359AF361FE1E1EALL).o_invoke_few_args("query", 0x356758D4414DA377LL, 2, "close", m_node)));
  if (!(toBoolean(v_names.o_get("length", 0x2993E8EB119CAB21LL))) || !(toBoolean(v_attrs.o_get("length", 0x2993E8EB119CAB21LL)))) {
    throw_exception(LINE(1482,create_object("mwexception", Array(ArrayInit(1).set(0, "Invalid ext node passed to PPNode_DOM::splitExt").create()))));
  }
  (v_parts = (assignCallTemp(eo_0, LINE(1485,create_object("self", Array(ArrayInit(1).set(0, v_names.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)).create())))),assignCallTemp(eo_1, LINE(1486,create_object("self", Array(ArrayInit(1).set(0, v_attrs.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)).create())))),Array(ArrayInit(2).set(0, "name", eo_0, 0x0BCDB293DC3CBDDCLL).set(1, "attr", eo_1, 0x64311A2C8443755DLL).create())));
  if (toBoolean(v_inners.o_get("length", 0x2993E8EB119CAB21LL))) {
    v_parts.set("inner", (LINE(1488,create_object("self", Array(ArrayInit(1).set(0, v_inners.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)).create())))), 0x4F20D07BB803C1FELL);
  }
  if (toBoolean(v_closes.o_get("length", 0x2993E8EB119CAB21LL))) {
    v_parts.set("close", (LINE(1491,create_object("self", Array(ArrayInit(1).set(0, v_closes.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)).create())))), 0x36B43082F052EC6BLL);
  }
  return v_parts;
} /* function */
/* SRC: Preprocessor_DOM.php line 1499 */
Array c_ppnode_dom::t_splitheading() {
  INSTANCE_METHOD_INJECTION(PPNode_DOM, PPNode_DOM::splitHeading);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  if (equal(!(toBoolean(o_get("nodeName", 0x009B66FACA616B3CLL))), "h")) {
    throw_exception(LINE(1501,create_object("mwexception", Array(ArrayInit(1).set(0, "Invalid h node passed to PPNode_DOM::splitHeading").create()))));
  }
  return (assignCallTemp(eo_0, LINE(1504,m_node.o_invoke_few_args("getAttribute", 0x49F89C466612FC28LL, 1, "i"))),assignCallTemp(eo_1, LINE(1505,m_node.o_invoke_few_args("getAttribute", 0x49F89C466612FC28LL, 1, "level"))),assignCallTemp(eo_2, LINE(1506,o_root_invoke_few_args("getChildren", 0x732EC1BDA8EC520FLL, 0))),Array(ArrayInit(3).set(0, "i", eo_0, 0x0EB22EDA95766E98LL).set(1, "level", eo_1, 0x26B910814038EBA3LL).set(2, "contents", eo_2, 0x5FAC83E143BEACFELL).create()));
} /* function */
/* SRC: Preprocessor_DOM.php line 642 */
Variant c_ppdstack::os_get(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x03CADA938C98C156LL, g->s_ppdstack$$false,
                  false);
      break;
    default:
      break;
  }
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppdstack::os_lval(const char *s, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  if (hash < 0) hash = hash_string(s);
  switch (hash & 1) {
    case 0:
      HASH_RETURN(0x03CADA938C98C156LL, g->s_ppdstack$$false,
                  false);
      break;
    default:
      break;
  }
  return c_ObjectData::os_lval(s, hash);
}
void c_ppdstack::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("stack", m_stack.isReferenced() ? ref(m_stack) : m_stack));
  props.push_back(NEW(ArrayElement)("rootAccum", m_rootAccum.isReferenced() ? ref(m_rootAccum) : m_rootAccum));
  props.push_back(NEW(ArrayElement)("top", m_top.isReferenced() ? ref(m_top) : m_top));
  props.push_back(NEW(ArrayElement)("out", m_out.isReferenced() ? ref(m_out) : m_out));
  props.push_back(NEW(ArrayElement)("elementClass", m_elementClass.isReferenced() ? ref(m_elementClass) : m_elementClass));
  c_ObjectData::o_get(props);
}
bool c_ppdstack::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_EXISTS_STRING(0x7C801AC012E9F722LL, out, 3);
      break;
    case 3:
      HASH_EXISTS_STRING(0x7DD6ABCF558AD1B3LL, elementClass, 12);
      break;
    case 8:
      HASH_EXISTS_STRING(0x1854FD73A00D89E8LL, top, 3);
      break;
    case 11:
      HASH_EXISTS_STRING(0x5084E637B870262BLL, stack, 5);
      break;
    case 15:
      HASH_EXISTS_STRING(0x5F4D08F214717E0FLL, rootAccum, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppdstack::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x7C801AC012E9F722LL, m_out,
                         out, 3);
      break;
    case 3:
      HASH_RETURN_STRING(0x7DD6ABCF558AD1B3LL, m_elementClass,
                         elementClass, 12);
      break;
    case 8:
      HASH_RETURN_STRING(0x1854FD73A00D89E8LL, m_top,
                         top, 3);
      break;
    case 11:
      HASH_RETURN_STRING(0x5084E637B870262BLL, m_stack,
                         stack, 5);
      break;
    case 15:
      HASH_RETURN_STRING(0x5F4D08F214717E0FLL, m_rootAccum,
                         rootAccum, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppdstack::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_SET_STRING(0x7C801AC012E9F722LL, m_out,
                      out, 3);
      break;
    case 3:
      HASH_SET_STRING(0x7DD6ABCF558AD1B3LL, m_elementClass,
                      elementClass, 12);
      break;
    case 8:
      HASH_SET_STRING(0x1854FD73A00D89E8LL, m_top,
                      top, 3);
      break;
    case 11:
      HASH_SET_STRING(0x5084E637B870262BLL, m_stack,
                      stack, 5);
      break;
    case 15:
      HASH_SET_STRING(0x5F4D08F214717E0FLL, m_rootAccum,
                      rootAccum, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppdstack::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x7C801AC012E9F722LL, m_out,
                         out, 3);
      break;
    case 3:
      HASH_RETURN_STRING(0x7DD6ABCF558AD1B3LL, m_elementClass,
                         elementClass, 12);
      break;
    case 8:
      HASH_RETURN_STRING(0x1854FD73A00D89E8LL, m_top,
                         top, 3);
      break;
    case 11:
      HASH_RETURN_STRING(0x5084E637B870262BLL, m_stack,
                         stack, 5);
      break;
    case 15:
      HASH_RETURN_STRING(0x5F4D08F214717E0FLL, m_rootAccum,
                         rootAccum, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppdstack::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppdstack)
ObjectData *c_ppdstack::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_ppdstack::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_ppdstack::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_ppdstack::cloneImpl() {
  c_ppdstack *obj = NEW(c_ppdstack)();
  cloneSet(obj);
  return obj;
}
void c_ppdstack::cloneSet(c_ppdstack *clone) {
  clone->m_stack = m_stack.isReferenced() ? ref(m_stack) : m_stack;
  clone->m_rootAccum = m_rootAccum.isReferenced() ? ref(m_rootAccum) : m_rootAccum;
  clone->m_top = m_top.isReferenced() ? ref(m_top) : m_top;
  clone->m_out = m_out.isReferenced() ? ref(m_out) : m_out;
  clone->m_elementClass = m_elementClass.isReferenced() ? ref(m_elementClass) : m_elementClass;
  ObjectData::cloneSet(clone);
}
Variant c_ppdstack::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x5CEFA5A265104D10LL, count) {
        return (t_count());
      }
      break;
    case 1:
      HASH_GUARD(0x27E7DBA875AD17E1LL, getflags) {
        return (t_getflags());
      }
      break;
    case 2:
      HASH_GUARD(0x499E72B719A4CAC2LL, addpart) {
        int count = params.size();
        if (count <= 0) return (t_addpart(), null);
        return (t_addpart(params.rvalAt(0)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x773C5A963CD2AC13LL, pop) {
        return (t_pop());
      }
      break;
    case 6:
      HASH_GUARD(0x1A5F5A2CB292E516LL, getaccum) {
        return ref(t_getaccum());
      }
      break;
    case 8:
      HASH_GUARD(0x321E2BF5D878AA38LL, push) {
        return (t_push(params.rvalAt(0)), null);
      }
      break;
    case 12:
      HASH_GUARD(0x1726802D706D7BECLL, getcurrentpart) {
        return (t_getcurrentpart());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppdstack::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x5CEFA5A265104D10LL, count) {
        return (t_count());
      }
      break;
    case 1:
      HASH_GUARD(0x27E7DBA875AD17E1LL, getflags) {
        return (t_getflags());
      }
      break;
    case 2:
      HASH_GUARD(0x499E72B719A4CAC2LL, addpart) {
        if (count <= 0) return (t_addpart(), null);
        return (t_addpart(a0), null);
      }
      break;
    case 3:
      HASH_GUARD(0x773C5A963CD2AC13LL, pop) {
        return (t_pop());
      }
      break;
    case 6:
      HASH_GUARD(0x1A5F5A2CB292E516LL, getaccum) {
        return ref(t_getaccum());
      }
      break;
    case 8:
      HASH_GUARD(0x321E2BF5D878AA38LL, push) {
        return (t_push(a0), null);
      }
      break;
    case 12:
      HASH_GUARD(0x1726802D706D7BECLL, getcurrentpart) {
        return (t_getcurrentpart());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppdstack::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppdstack$os_get(const char *s) {
  return c_ppdstack::os_get(s, -1);
}
Variant &cw_ppdstack$os_lval(const char *s) {
  return c_ppdstack::os_lval(s, -1);
}
Variant cw_ppdstack$os_constant(const char *s) {
  return c_ppdstack::os_constant(s);
}
Variant cw_ppdstack$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppdstack::os_invoke(c, s, params, -1, fatal);
}
void c_ppdstack::init() {
  m_stack = null;
  m_rootAccum = null;
  m_top = null;
  m_out = null;
  m_elementClass = "PPDStackElement";
}
void c_ppdstack::os_static_initializer() {
  DECLARE_GLOBAL_VARIABLES(g);
  g->s_ppdstack$$false = false;
}
void csi_ppdstack() {
  c_ppdstack::os_static_initializer();
}
/* SRC: Preprocessor_DOM.php line 649 */
void c_ppdstack::t___construct() {
  INSTANCE_METHOD_INJECTION(PPDStack, PPDStack::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_stack = ScalarArrays::sa_[0]);
  (m_top = false);
  (m_rootAccum = "");
  (o_lval("accum", 0x4761A194B0333B28LL) = ref(lval(m_rootAccum)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_DOM.php line 656 */
int c_ppdstack::t_count() {
  INSTANCE_METHOD_INJECTION(PPDStack, PPDStack::count);
  return LINE(657,x_count(m_stack));
} /* function */
/* SRC: Preprocessor_DOM.php line 660 */
Variant c_ppdstack::t_getaccum() {
  INSTANCE_METHOD_INJECTION(PPDStack, PPDStack::getAccum);
  return ref(lval(o_lval("accum", 0x4761A194B0333B28LL)));
} /* function */
/* SRC: Preprocessor_DOM.php line 664 */
Variant c_ppdstack::t_getcurrentpart() {
  INSTANCE_METHOD_INJECTION(PPDStack, PPDStack::getCurrentPart);
  if (same(m_top, false)) {
    return false;
  }
  else {
    return LINE(668,m_top.o_invoke_few_args("getCurrentPart", 0x1726802D706D7BECLL, 0));
  }
  return null;
} /* function */
/* SRC: Preprocessor_DOM.php line 672 */
void c_ppdstack::t_push(Variant v_data) {
  INSTANCE_METHOD_INJECTION(PPDStack, PPDStack::push);
  Variant v_class;

  if (instanceOf(v_data, toString(m_elementClass))) {
    m_stack.append((v_data));
  }
  else {
    (v_class = m_elementClass);
    m_stack.append((LINE(677,create_object(toString(v_class), Array(ArrayInit(1).set(0, ref(v_data)).create())))));
  }
  (m_top = m_stack.rvalAt(LINE(679,x_count(m_stack)) - 1LL));
  (o_lval("accum", 0x4761A194B0333B28LL) = ref(LINE(680,m_top.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
} /* function */
/* SRC: Preprocessor_DOM.php line 683 */
Variant c_ppdstack::t_pop() {
  INSTANCE_METHOD_INJECTION(PPDStack, PPDStack::pop);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_temp;

  if (!(toBoolean(LINE(684,x_count(m_stack))))) {
    throw_exception(LINE(685,create_object("mwexception", Array(ArrayInit(1).set(0, "PPDStack::pop: no elements remaining").create()))));
  }
  (v_temp = LINE(687,x_array_pop(ref(lval(m_stack)))));
  if (toBoolean(LINE(689,x_count(m_stack)))) {
    (m_top = m_stack.rvalAt(LINE(690,x_count(m_stack)) - 1LL));
    (o_lval("accum", 0x4761A194B0333B28LL) = ref(LINE(691,m_top.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
  }
  else {
    (m_top = g->s_ppdstack$$false);
    (o_lval("accum", 0x4761A194B0333B28LL) = ref(lval(m_rootAccum)));
  }
  return v_temp;
} /* function */
/* SRC: Preprocessor_DOM.php line 699 */
void c_ppdstack::t_addpart(Variant v_s //  = ""
) {
  INSTANCE_METHOD_INJECTION(PPDStack, PPDStack::addPart);
  LINE(700,m_top.o_invoke_few_args("addPart", 0x499E72B719A4CAC2LL, 1, v_s));
  (o_lval("accum", 0x4761A194B0333B28LL) = ref(LINE(701,m_top.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
} /* function */
/* SRC: Preprocessor_DOM.php line 704 */
Variant c_ppdstack::t_getflags() {
  INSTANCE_METHOD_INJECTION(PPDStack, PPDStack::getFlags);
  if (!(toBoolean(LINE(705,x_count(m_stack))))) {
    return ScalarArrays::sa_[8];
  }
  else {
    return LINE(712,m_top.o_invoke_few_args("getFlags", 0x27E7DBA875AD17E1LL, 0));
  }
  return null;
} /* function */
/* SRC: Preprocessor_DOM.php line 6 */
const int64 q_preprocessor_dom_CACHE_VERSION = 1LL;
Variant c_preprocessor_dom::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_preprocessor_dom::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_preprocessor_dom::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("parser", m_parser.isReferenced() ? ref(m_parser) : m_parser));
  props.push_back(NEW(ArrayElement)("memoryLimit", m_memoryLimit.isReferenced() ? ref(m_memoryLimit) : m_memoryLimit));
  c_ObjectData::o_get(props);
}
bool c_preprocessor_dom::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_EXISTS_STRING(0x18A8B9D71D4F2D02LL, parser, 6);
      break;
    case 3:
      HASH_EXISTS_STRING(0x65AD8A7E91F7756BLL, memoryLimit, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_preprocessor_dom::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                         parser, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x65AD8A7E91F7756BLL, m_memoryLimit,
                         memoryLimit, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_preprocessor_dom::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_SET_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                      parser, 6);
      break;
    case 3:
      HASH_SET_STRING(0x65AD8A7E91F7756BLL, m_memoryLimit,
                      memoryLimit, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_preprocessor_dom::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 2:
      HASH_RETURN_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                         parser, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x65AD8A7E91F7756BLL, m_memoryLimit,
                         memoryLimit, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_preprocessor_dom::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x6AD33BF0C16EEC43LL, q_preprocessor_dom_CACHE_VERSION, CACHE_VERSION);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(preprocessor_dom)
ObjectData *c_preprocessor_dom::create(Variant v_parser) {
  init();
  t___construct(v_parser);
  return this;
}
ObjectData *c_preprocessor_dom::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_preprocessor_dom::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_preprocessor_dom::cloneImpl() {
  c_preprocessor_dom *obj = NEW(c_preprocessor_dom)();
  cloneSet(obj);
  return obj;
}
void c_preprocessor_dom::cloneSet(c_preprocessor_dom *clone) {
  clone->m_parser = m_parser.isReferenced() ? ref(m_parser) : m_parser;
  clone->m_memoryLimit = m_memoryLimit.isReferenced() ? ref(m_memoryLimit) : m_memoryLimit;
  ObjectData::cloneSet(clone);
}
Variant c_preprocessor_dom::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x5A11FE5B629AF7A0LL, newcustomframe) {
        return (t_newcustomframe(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0BA55A9913645813LL, preprocesstoobj) {
        int count = params.size();
        if (count <= 1) return (t_preprocesstoobj(params.rvalAt(0)));
        return (t_preprocesstoobj(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 5:
      HASH_GUARD(0x787B6141D7721675LL, newframe) {
        return (t_newframe());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_preprocessor_dom::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x5A11FE5B629AF7A0LL, newcustomframe) {
        return (t_newcustomframe(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x0BA55A9913645813LL, preprocesstoobj) {
        if (count <= 1) return (t_preprocesstoobj(a0));
        return (t_preprocesstoobj(a0, a1));
      }
      break;
    case 5:
      HASH_GUARD(0x787B6141D7721675LL, newframe) {
        return (t_newframe());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_preprocessor_dom::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_preprocessor_dom$os_get(const char *s) {
  return c_preprocessor_dom::os_get(s, -1);
}
Variant &cw_preprocessor_dom$os_lval(const char *s) {
  return c_preprocessor_dom::os_lval(s, -1);
}
Variant cw_preprocessor_dom$os_constant(const char *s) {
  return c_preprocessor_dom::os_constant(s);
}
Variant cw_preprocessor_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_preprocessor_dom::os_invoke(c, s, params, -1, fatal);
}
void c_preprocessor_dom::init() {
  m_parser = null;
  m_memoryLimit = null;
}
/* SRC: Preprocessor_DOM.php line 11 */
void c_preprocessor_dom::t___construct(Variant v_parser) {
  INSTANCE_METHOD_INJECTION(Preprocessor_DOM, Preprocessor_DOM::__construct);
  bool oldInCtor = gasInCtor(true);
  String v_mem;
  Variant v_m;

  (m_parser = v_parser);
  (v_mem = LINE(13,x_ini_get("memory_limit")));
  (m_memoryLimit = false);
  if (!same(LINE(15,x_strval(v_mem)), "") && !equal(v_mem, -1LL)) {
    if (toBoolean(LINE(16,x_preg_match("/^\\d+$/", v_mem)))) {
      (m_memoryLimit = v_mem);
    }
    else if (toBoolean(LINE(18,x_preg_match("/^(\\d+)M$/i", v_mem, ref(v_m))))) {
      (m_memoryLimit = v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL) * 1048576LL);
    }
  }
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_DOM.php line 24 */
p_ppframe_dom c_preprocessor_dom::t_newframe() {
  INSTANCE_METHOD_INJECTION(Preprocessor_DOM, Preprocessor_DOM::newFrame);
  return ((Object)(LINE(25,p_ppframe_dom(p_ppframe_dom(NEWOBJ(c_ppframe_dom)())->create(((Object)(this)))))));
} /* function */
/* SRC: Preprocessor_DOM.php line 28 */
p_ppcustomframe_dom c_preprocessor_dom::t_newcustomframe(CVarRef v_args) {
  INSTANCE_METHOD_INJECTION(Preprocessor_DOM, Preprocessor_DOM::newCustomFrame);
  return ((Object)(LINE(29,p_ppcustomframe_dom(p_ppcustomframe_dom(NEWOBJ(c_ppcustomframe_dom)())->create(((Object)(this)), v_args)))));
} /* function */
/* SRC: Preprocessor_DOM.php line 32 */
bool c_preprocessor_dom::t_memcheck() {
  INSTANCE_METHOD_INJECTION(Preprocessor_DOM, Preprocessor_DOM::memCheck);
  int64 v_usage = 0;
  int64 v_limit = 0;

  if (same(m_memoryLimit, false)) {
    return false;
  }
  (v_usage = LINE(36,x_memory_get_usage()));
  if (more(v_usage, toDouble(m_memoryLimit) * 0.90000000000000002)) {
    (v_limit = LINE(38,x_intval(toDouble(divide(toDouble(m_memoryLimit) * 0.90000000000000002, 1048576LL)) + 0.5)));
    throw_exception(LINE(39,create_object("mwexception", Array(ArrayInit(1).set(0, concat3("Preprocessor hit 90% memory limit (", toString(v_limit), " MB)")).create()))));
  }
  return not_more(v_usage, toDouble(m_memoryLimit) * 0.80000000000000004);
} /* function */
/* SRC: Preprocessor_DOM.php line 66 */
p_ppnode_dom c_preprocessor_dom::t_preprocesstoobj(CVarRef v_text, Variant v_flags //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(Preprocessor_DOM, Preprocessor_DOM::preprocessToObj);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgMemc __attribute__((__unused__)) = g->GV(wgMemc);
  Variant &gv_wgPreprocessorCacheThreshold __attribute__((__unused__)) = g->GV(wgPreprocessorCacheThreshold);
  Variant v_xml;
  bool v_cacheable = false;
  Variant v_cacheKey;
  Variant v_cacheValue;
  Variant v_version;
  p_domdocument v_dom;
  Variant v_result;
  p_ppnode_dom v_obj;

  LINE(67,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToObj").create()), 0x0000000075359BAFLL));
  {
  }
  (v_xml = false);
  (v_cacheable = more(LINE(71,x_strlen(toString(v_text))), gv_wgPreprocessorCacheThreshold));
  if (v_cacheable) {
    LINE(73,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToObj-cacheable").create()), 0x0000000075359BAFLL));
    (v_cacheKey = LINE(75,(assignCallTemp(eo_1, ref(x_md5(toString(v_text)))),assignCallTemp(eo_2, ref(v_flags)),invoke_failed("wfmemckey", Array(ArrayInit(3).set(0, "preprocess-xml").set(1, eo_1).set(2, eo_2).create()), 0x0000000072AF623FLL))));
    (v_cacheValue = LINE(76,gv_wgMemc.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_cacheKey)));
    if (toBoolean(v_cacheValue)) {
      (v_version = LINE(78,x_substr(toString(v_cacheValue), toInt32(0LL), toInt32(8LL))));
      if (equal(LINE(79,x_intval(v_version)), 1LL /* preprocessor_dom::CACHE_VERSION */)) {
        (v_xml = LINE(80,x_substr(toString(v_cacheValue), toInt32(8LL))));
        LINE(82,(assignCallTemp(eo_1, concat3("Loaded preprocessor XML from memcached (key ", toString(v_cacheKey), ")")),invoke_failed("wfdebuglog", Array(ArrayInit(2).set(0, "Preprocessor").set(1, eo_1).create()), 0x00000000E6942489LL)));
      }
    }
  }
  if (same(v_xml, false)) {
    if (v_cacheable) {
      LINE(88,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToObj-cache-miss").create()), 0x0000000075359BAFLL));
      (v_xml = LINE(89,t_preprocesstoxml(v_text, v_flags)));
      (v_cacheValue = concat(LINE(90,x_sprintf(2, "%08d", ScalarArrays::sa_[2])), toString(v_xml)));
      LINE(91,gv_wgMemc.o_invoke_few_args("set", 0x399A6427C2185621LL, 3, v_cacheKey, v_cacheValue, 86400LL));
      LINE(92,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToObj-cache-miss").create()), 0x00000000B599F276LL));
      LINE(93,(assignCallTemp(eo_1, concat3("Saved preprocessor XML to memcached (key ", toString(v_cacheKey), ")")),invoke_failed("wfdebuglog", Array(ArrayInit(2).set(0, "Preprocessor").set(1, eo_1).create()), 0x00000000E6942489LL)));
    }
    else {
      (v_xml = LINE(95,t_preprocesstoxml(v_text, v_flags)));
    }
  }
  LINE(99,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToObj-loadXML").create()), 0x0000000075359BAFLL));
  ((Object)((v_dom = ((Object)(LINE(100,create_object("domdocument", Array())))))));
  LINE(101,invoke_failed("wfsuppresswarnings", Array(), 0x00000000FCF68A6CLL));
  (v_result = LINE(102,v_dom->t_loadxml(toString(v_xml))));
  LINE(103,invoke_failed("wfrestorewarnings", Array(), 0x000000001BD025E4LL));
  if (!(toBoolean(v_result))) {
    (v_xml = LINE(106,throw_fatal("unknown class utfnormal", ((void*)NULL))));
    (v_result = LINE(107,v_dom->t_loadxml(toString(v_xml))));
    if (!(toBoolean(v_result))) {
      throw_exception(LINE(109,create_object("mwexception", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToObj generated invalid XML").create()))));
    }
  }
  ((Object)((v_obj = ((Object)(LINE(112,p_ppnode_dom(p_ppnode_dom(NEWOBJ(c_ppnode_dom)())->create(v_dom.o_get("documentElement", 0x34E37E9EAB48521BLL)))))))));
  LINE(113,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToObj-loadXML").create()), 0x00000000B599F276LL));
  if (v_cacheable) {
    LINE(115,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToObj-cacheable").create()), 0x00000000B599F276LL));
  }
  LINE(117,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToObj").create()), 0x00000000B599F276LL));
  return ((Object)(v_obj));
} /* function */
/* SRC: Preprocessor_DOM.php line 121 */
Variant c_preprocessor_dom::t_preprocesstoxml(Variant v_text, Variant v_flags //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(Preprocessor_DOM, Preprocessor_DOM::preprocessToXml);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_rules;
  Variant v_forInclusion;
  Variant v_xmlishElements;
  Variant v_enableOnlyinclude;
  Variant v_ignoredTags;
  Variant v_ignoredElements;
  Variant v_xmlishRegex;
  Variant v_elementsRegex;
  Variant v_stack;
  Variant v_searchBase;
  Variant v_revText;
  Variant v_i;
  Variant v_accum;
  Variant v_findEquals;
  Variant v_findPipe;
  Variant v_headingIndex;
  Variant v_inHeading;
  Variant v_noMoreGT;
  Variant v_findOnlyinclude;
  Variant v_fakeLineStart;
  Variant v_startPos;
  Variant v_tagEndPos;
  Variant v_found;
  Variant v_curChar;
  Variant v_search;
  Variant v_currentClosing;
  Variant v_rule;
  Variant v_literalLength;
  Variant v_matches;
  Variant v_endPos;
  Variant v_inner;
  Variant v_wsStart;
  Variant v_wsEnd;
  Variant v_wsLength;
  Variant v_part;
  Variant v_name;
  Variant v_lowerName;
  Variant v_attrStart;
  Variant v_tagStartPos;
  Variant v_attrEnd;
  Variant v_close;
  Variant v_attr;
  Variant v_count;
  Variant v_piece;
  Variant v_searchStart;
  Variant v_equalsLength;
  Variant v_element;
  Variant v_maxCount;
  Variant v_matchingCount;
  Variant v_parts;
  Variant v_title;
  Variant v_argIndex;
  Variant v_partIndex;
  Variant v_argName;
  Variant v_argValue;
  Variant v_names;
  Variant v_skippedBraces;
  Variant v_enclosingAccum;
  Variant v_xml;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_text; Variant &v_flags; Variant &v_rules; Variant &v_forInclusion; Variant &v_xmlishElements; Variant &v_enableOnlyinclude; Variant &v_ignoredTags; Variant &v_ignoredElements; Variant &v_xmlishRegex; Variant &v_elementsRegex; Variant &v_stack; Variant &v_searchBase; Variant &v_revText; Variant &v_i; Variant &v_accum; Variant &v_findEquals; Variant &v_findPipe; Variant &v_headingIndex; Variant &v_inHeading; Variant &v_noMoreGT; Variant &v_findOnlyinclude; Variant &v_fakeLineStart; Variant &v_startPos; Variant &v_tagEndPos; Variant &v_found; Variant &v_curChar; Variant &v_search; Variant &v_currentClosing; Variant &v_rule; Variant &v_literalLength; Variant &v_matches; Variant &v_endPos; Variant &v_inner; Variant &v_wsStart; Variant &v_wsEnd; Variant &v_wsLength; Variant &v_part; Variant &v_name; Variant &v_lowerName; Variant &v_attrStart; Variant &v_tagStartPos; Variant &v_attrEnd; Variant &v_close; Variant &v_attr; Variant &v_count; Variant &v_piece; Variant &v_searchStart; Variant &v_equalsLength; Variant &v_element; Variant &v_maxCount; Variant &v_matchingCount; Variant &v_parts; Variant &v_title; Variant &v_argIndex; Variant &v_partIndex; Variant &v_argName; Variant &v_argValue; Variant &v_names; Variant &v_skippedBraces; Variant &v_enclosingAccum; Variant &v_xml;
    VariableTable(Variant &r_text, Variant &r_flags, Variant &r_rules, Variant &r_forInclusion, Variant &r_xmlishElements, Variant &r_enableOnlyinclude, Variant &r_ignoredTags, Variant &r_ignoredElements, Variant &r_xmlishRegex, Variant &r_elementsRegex, Variant &r_stack, Variant &r_searchBase, Variant &r_revText, Variant &r_i, Variant &r_accum, Variant &r_findEquals, Variant &r_findPipe, Variant &r_headingIndex, Variant &r_inHeading, Variant &r_noMoreGT, Variant &r_findOnlyinclude, Variant &r_fakeLineStart, Variant &r_startPos, Variant &r_tagEndPos, Variant &r_found, Variant &r_curChar, Variant &r_search, Variant &r_currentClosing, Variant &r_rule, Variant &r_literalLength, Variant &r_matches, Variant &r_endPos, Variant &r_inner, Variant &r_wsStart, Variant &r_wsEnd, Variant &r_wsLength, Variant &r_part, Variant &r_name, Variant &r_lowerName, Variant &r_attrStart, Variant &r_tagStartPos, Variant &r_attrEnd, Variant &r_close, Variant &r_attr, Variant &r_count, Variant &r_piece, Variant &r_searchStart, Variant &r_equalsLength, Variant &r_element, Variant &r_maxCount, Variant &r_matchingCount, Variant &r_parts, Variant &r_title, Variant &r_argIndex, Variant &r_partIndex, Variant &r_argName, Variant &r_argValue, Variant &r_names, Variant &r_skippedBraces, Variant &r_enclosingAccum, Variant &r_xml) : v_text(r_text), v_flags(r_flags), v_rules(r_rules), v_forInclusion(r_forInclusion), v_xmlishElements(r_xmlishElements), v_enableOnlyinclude(r_enableOnlyinclude), v_ignoredTags(r_ignoredTags), v_ignoredElements(r_ignoredElements), v_xmlishRegex(r_xmlishRegex), v_elementsRegex(r_elementsRegex), v_stack(r_stack), v_searchBase(r_searchBase), v_revText(r_revText), v_i(r_i), v_accum(r_accum), v_findEquals(r_findEquals), v_findPipe(r_findPipe), v_headingIndex(r_headingIndex), v_inHeading(r_inHeading), v_noMoreGT(r_noMoreGT), v_findOnlyinclude(r_findOnlyinclude), v_fakeLineStart(r_fakeLineStart), v_startPos(r_startPos), v_tagEndPos(r_tagEndPos), v_found(r_found), v_curChar(r_curChar), v_search(r_search), v_currentClosing(r_currentClosing), v_rule(r_rule), v_literalLength(r_literalLength), v_matches(r_matches), v_endPos(r_endPos), v_inner(r_inner), v_wsStart(r_wsStart), v_wsEnd(r_wsEnd), v_wsLength(r_wsLength), v_part(r_part), v_name(r_name), v_lowerName(r_lowerName), v_attrStart(r_attrStart), v_tagStartPos(r_tagStartPos), v_attrEnd(r_attrEnd), v_close(r_close), v_attr(r_attr), v_count(r_count), v_piece(r_piece), v_searchStart(r_searchStart), v_equalsLength(r_equalsLength), v_element(r_element), v_maxCount(r_maxCount), v_matchingCount(r_matchingCount), v_parts(r_parts), v_title(r_title), v_argIndex(r_argIndex), v_partIndex(r_partIndex), v_argName(r_argName), v_argValue(r_argValue), v_names(r_names), v_skippedBraces(r_skippedBraces), v_enclosingAccum(r_enclosingAccum), v_xml(r_xml) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 127) {
        case 0:
          HASH_RETURN(0x2FA2EA3BE4385180LL, v_ignoredElements,
                      ignoredElements);
          break;
        case 1:
          HASH_RETURN(0x4823DBE38D294B01LL, v_argValue,
                      argValue);
          break;
        case 6:
          HASH_RETURN(0x1B3A1B4FC8E2BF86LL, v_findEquals,
                      findEquals);
          break;
        case 9:
          HASH_RETURN(0x1AFD3E7207DEC289LL, v_element,
                      element);
          break;
        case 13:
          HASH_RETURN(0x6AFDA85728FAE70DLL, v_flags,
                      flags);
          HASH_RETURN(0x2708FDA74562AD8DLL, v_parts,
                      parts);
          break;
        case 16:
          HASH_RETURN(0x7A62DFE604197490LL, v_rule,
                      rule);
          break;
        case 24:
          HASH_RETURN(0x0EB22EDA95766E98LL, v_i,
                      i);
          break;
        case 29:
          HASH_RETURN(0x749C3FAB1E74371DLL, v_startPos,
                      startPos);
          break;
        case 30:
          HASH_RETURN(0x1D7680FEEF32A39ELL, v_attrStart,
                      attrStart);
          HASH_RETURN(0x3ABE2A7FF197E79ELL, v_maxCount,
                      maxCount);
          break;
        case 34:
          HASH_RETURN(0x7985846879851FA2LL, v_revText,
                      revText);
          HASH_RETURN(0x6F571EAFB109D7A2LL, v_headingIndex,
                      headingIndex);
          break;
        case 36:
          HASH_RETURN(0x07D9889BE123B524LL, v_searchBase,
                      searchBase);
          break;
        case 37:
          HASH_RETURN(0x78D6949B972CFD25LL, v_rules,
                      rules);
          break;
        case 40:
          HASH_RETURN(0x4761A194B0333B28LL, v_accum,
                      accum);
          break;
        case 41:
          HASH_RETURN(0x2DD17595E81D9BA9LL, v_xmlishElements,
                      xmlishElements);
          break;
        case 43:
          HASH_RETURN(0x5084E637B870262BLL, v_stack,
                      stack);
          break;
        case 45:
          HASH_RETURN(0x73F30E0F2D277D2DLL, v_inHeading,
                      inHeading);
          break;
        case 46:
          HASH_RETURN(0x7F0BF9704771F92ELL, v_literalLength,
                      literalLength);
          HASH_RETURN(0x75716BF7E20AFAAELL, v_matchingCount,
                      matchingCount);
          break;
        case 47:
          HASH_RETURN(0x7451350C607F1BAFLL, v_noMoreGT,
                      noMoreGT);
          break;
        case 48:
          HASH_RETURN(0x6FD137A54D1BE930LL, v_wsStart,
                      wsStart);
          break;
        case 49:
          HASH_RETURN(0x5893A9585C6502B1LL, v_xml,
                      xml);
          break;
        case 50:
          HASH_RETURN(0x485CF5F18ACB2632LL, v_endPos,
                      endPos);
          break;
        case 57:
          HASH_RETURN(0x61CFEEED54DD0AB9LL, v_matches,
                      matches);
          break;
        case 59:
          HASH_RETURN(0x3D66B5980D54BABBLL, v_count,
                      count);
          break;
        case 62:
          HASH_RETURN(0x4B89E9FF5DB696BELL, v_currentClosing,
                      currentClosing);
          break;
        case 67:
          HASH_RETURN(0x2A28A0084DD3A743LL, v_text,
                      text);
          HASH_RETURN(0x79E99297D9CF6243LL, v_search,
                      search);
          break;
        case 75:
          HASH_RETURN(0x4D0B41A665B57E4BLL, v_searchStart,
                      searchStart);
          break;
        case 78:
          HASH_RETURN(0x41CC31743A0270CELL, v_found,
                      found);
          HASH_RETURN(0x5AE6844A0CCFE9CELL, v_attrEnd,
                      attrEnd);
          HASH_RETURN(0x0B812E4C8D98FBCELL, v_argIndex,
                      argIndex);
          break;
        case 86:
          HASH_RETURN(0x2C356137E4207156LL, v_tagStartPos,
                      tagStartPos);
          break;
        case 89:
          HASH_RETURN(0x3FB915AA8F8F8ED9LL, v_fakeLineStart,
                      fakeLineStart);
          break;
        case 91:
          HASH_RETURN(0x66866B45F7FF96DBLL, v_wsEnd,
                      wsEnd);
          HASH_RETURN(0x7132B63767EAFEDBLL, v_piece,
                      piece);
          break;
        case 92:
          HASH_RETURN(0x0BCDB293DC3CBDDCLL, v_name,
                      name);
          break;
        case 93:
          HASH_RETURN(0x64311A2C8443755DLL, v_attr,
                      attr);
          break;
        case 99:
          HASH_RETURN(0x40B8ADAF2E1FB7E3LL, v_elementsRegex,
                      elementsRegex);
          break;
        case 101:
          HASH_RETURN(0x7FE16A3727F18DE5LL, v_findOnlyinclude,
                      findOnlyinclude);
          HASH_RETURN(0x0AE2F3887B84C665LL, v_tagEndPos,
                      tagEndPos);
          break;
        case 104:
          HASH_RETURN(0x5D2342E7226E4E68LL, v_skippedBraces,
                      skippedBraces);
          break;
        case 107:
          HASH_RETURN(0x36B43082F052EC6BLL, v_close,
                      close);
          break;
        case 108:
          HASH_RETURN(0x0FCC00A0960D4D6CLL, v_xmlishRegex,
                      xmlishRegex);
          HASH_RETURN(0x5DBFA1F0A183E9ECLL, v_curChar,
                      curChar);
          HASH_RETURN(0x7F144219D39BF1ECLL, v_part,
                      part);
          break;
        case 109:
          HASH_RETURN(0x669278D2AC3068EDLL, v_ignoredTags,
                      ignoredTags);
          break;
        case 114:
          HASH_RETURN(0x51A687446C1579F2LL, v_names,
                      names);
          break;
        case 117:
          HASH_RETURN(0x217F5FC11124CC75LL, v_wsLength,
                      wsLength);
          break;
        case 119:
          HASH_RETURN(0x5C804792579E2477LL, v_forInclusion,
                      forInclusion);
          break;
        case 120:
          HASH_RETURN(0x77F20E4209A12DF8LL, v_enableOnlyinclude,
                      enableOnlyinclude);
          HASH_RETURN(0x573F2D2FEF6C9378LL, v_argName,
                      argName);
          break;
        case 121:
          HASH_RETURN(0x5AE3699687C882F9LL, v_findPipe,
                      findPipe);
          HASH_RETURN(0x75E78845AE202B79LL, v_enclosingAccum,
                      enclosingAccum);
          break;
        case 124:
          HASH_RETURN(0x35537723FE7928FCLL, v_lowerName,
                      lowerName);
          break;
        case 125:
          HASH_RETURN(0x3179991995176C7DLL, v_partIndex,
                      partIndex);
          break;
        case 126:
          HASH_RETURN(0x4F20D07BB803C1FELL, v_inner,
                      inner);
          HASH_RETURN(0x66AD900A2301E2FELL, v_title,
                      title);
          break;
        case 127:
          HASH_RETURN(0x41E37259D0CE717FLL, v_equalsLength,
                      equalsLength);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
    virtual bool exists(const char *s, int64 hash /* = -1 */) const {
      if (hash < 0) hash = hash_string(s);
      switch (hash & 127) {
        case 0:
          HASH_INITIALIZED(0x2FA2EA3BE4385180LL, v_ignoredElements,
                           ignoredElements);
          break;
        case 1:
          HASH_INITIALIZED(0x4823DBE38D294B01LL, v_argValue,
                           argValue);
          break;
        case 6:
          HASH_INITIALIZED(0x1B3A1B4FC8E2BF86LL, v_findEquals,
                           findEquals);
          break;
        case 9:
          HASH_INITIALIZED(0x1AFD3E7207DEC289LL, v_element,
                           element);
          break;
        case 13:
          HASH_INITIALIZED(0x6AFDA85728FAE70DLL, v_flags,
                           flags);
          HASH_INITIALIZED(0x2708FDA74562AD8DLL, v_parts,
                           parts);
          break;
        case 16:
          HASH_INITIALIZED(0x7A62DFE604197490LL, v_rule,
                           rule);
          break;
        case 24:
          HASH_INITIALIZED(0x0EB22EDA95766E98LL, v_i,
                           i);
          break;
        case 29:
          HASH_INITIALIZED(0x749C3FAB1E74371DLL, v_startPos,
                           startPos);
          break;
        case 30:
          HASH_INITIALIZED(0x1D7680FEEF32A39ELL, v_attrStart,
                           attrStart);
          HASH_INITIALIZED(0x3ABE2A7FF197E79ELL, v_maxCount,
                           maxCount);
          break;
        case 34:
          HASH_INITIALIZED(0x7985846879851FA2LL, v_revText,
                           revText);
          HASH_INITIALIZED(0x6F571EAFB109D7A2LL, v_headingIndex,
                           headingIndex);
          break;
        case 36:
          HASH_INITIALIZED(0x07D9889BE123B524LL, v_searchBase,
                           searchBase);
          break;
        case 37:
          HASH_INITIALIZED(0x78D6949B972CFD25LL, v_rules,
                           rules);
          break;
        case 40:
          HASH_INITIALIZED(0x4761A194B0333B28LL, v_accum,
                           accum);
          break;
        case 41:
          HASH_INITIALIZED(0x2DD17595E81D9BA9LL, v_xmlishElements,
                           xmlishElements);
          break;
        case 43:
          HASH_INITIALIZED(0x5084E637B870262BLL, v_stack,
                           stack);
          break;
        case 45:
          HASH_INITIALIZED(0x73F30E0F2D277D2DLL, v_inHeading,
                           inHeading);
          break;
        case 46:
          HASH_INITIALIZED(0x7F0BF9704771F92ELL, v_literalLength,
                           literalLength);
          HASH_INITIALIZED(0x75716BF7E20AFAAELL, v_matchingCount,
                           matchingCount);
          break;
        case 47:
          HASH_INITIALIZED(0x7451350C607F1BAFLL, v_noMoreGT,
                           noMoreGT);
          break;
        case 48:
          HASH_INITIALIZED(0x6FD137A54D1BE930LL, v_wsStart,
                           wsStart);
          break;
        case 49:
          HASH_INITIALIZED(0x5893A9585C6502B1LL, v_xml,
                           xml);
          break;
        case 50:
          HASH_INITIALIZED(0x485CF5F18ACB2632LL, v_endPos,
                           endPos);
          break;
        case 57:
          HASH_INITIALIZED(0x61CFEEED54DD0AB9LL, v_matches,
                           matches);
          break;
        case 59:
          HASH_INITIALIZED(0x3D66B5980D54BABBLL, v_count,
                           count);
          break;
        case 62:
          HASH_INITIALIZED(0x4B89E9FF5DB696BELL, v_currentClosing,
                           currentClosing);
          break;
        case 67:
          HASH_INITIALIZED(0x2A28A0084DD3A743LL, v_text,
                           text);
          HASH_INITIALIZED(0x79E99297D9CF6243LL, v_search,
                           search);
          break;
        case 75:
          HASH_INITIALIZED(0x4D0B41A665B57E4BLL, v_searchStart,
                           searchStart);
          break;
        case 78:
          HASH_INITIALIZED(0x41CC31743A0270CELL, v_found,
                           found);
          HASH_INITIALIZED(0x5AE6844A0CCFE9CELL, v_attrEnd,
                           attrEnd);
          HASH_INITIALIZED(0x0B812E4C8D98FBCELL, v_argIndex,
                           argIndex);
          break;
        case 86:
          HASH_INITIALIZED(0x2C356137E4207156LL, v_tagStartPos,
                           tagStartPos);
          break;
        case 89:
          HASH_INITIALIZED(0x3FB915AA8F8F8ED9LL, v_fakeLineStart,
                           fakeLineStart);
          break;
        case 91:
          HASH_INITIALIZED(0x66866B45F7FF96DBLL, v_wsEnd,
                           wsEnd);
          HASH_INITIALIZED(0x7132B63767EAFEDBLL, v_piece,
                           piece);
          break;
        case 92:
          HASH_INITIALIZED(0x0BCDB293DC3CBDDCLL, v_name,
                           name);
          break;
        case 93:
          HASH_INITIALIZED(0x64311A2C8443755DLL, v_attr,
                           attr);
          break;
        case 99:
          HASH_INITIALIZED(0x40B8ADAF2E1FB7E3LL, v_elementsRegex,
                           elementsRegex);
          break;
        case 101:
          HASH_INITIALIZED(0x7FE16A3727F18DE5LL, v_findOnlyinclude,
                           findOnlyinclude);
          HASH_INITIALIZED(0x0AE2F3887B84C665LL, v_tagEndPos,
                           tagEndPos);
          break;
        case 104:
          HASH_INITIALIZED(0x5D2342E7226E4E68LL, v_skippedBraces,
                           skippedBraces);
          break;
        case 107:
          HASH_INITIALIZED(0x36B43082F052EC6BLL, v_close,
                           close);
          break;
        case 108:
          HASH_INITIALIZED(0x0FCC00A0960D4D6CLL, v_xmlishRegex,
                           xmlishRegex);
          HASH_INITIALIZED(0x5DBFA1F0A183E9ECLL, v_curChar,
                           curChar);
          HASH_INITIALIZED(0x7F144219D39BF1ECLL, v_part,
                           part);
          break;
        case 109:
          HASH_INITIALIZED(0x669278D2AC3068EDLL, v_ignoredTags,
                           ignoredTags);
          break;
        case 114:
          HASH_INITIALIZED(0x51A687446C1579F2LL, v_names,
                           names);
          break;
        case 117:
          HASH_INITIALIZED(0x217F5FC11124CC75LL, v_wsLength,
                           wsLength);
          break;
        case 119:
          HASH_INITIALIZED(0x5C804792579E2477LL, v_forInclusion,
                           forInclusion);
          break;
        case 120:
          HASH_INITIALIZED(0x77F20E4209A12DF8LL, v_enableOnlyinclude,
                           enableOnlyinclude);
          HASH_INITIALIZED(0x573F2D2FEF6C9378LL, v_argName,
                           argName);
          break;
        case 121:
          HASH_INITIALIZED(0x5AE3699687C882F9LL, v_findPipe,
                           findPipe);
          HASH_INITIALIZED(0x75E78845AE202B79LL, v_enclosingAccum,
                           enclosingAccum);
          break;
        case 124:
          HASH_INITIALIZED(0x35537723FE7928FCLL, v_lowerName,
                           lowerName);
          break;
        case 125:
          HASH_INITIALIZED(0x3179991995176C7DLL, v_partIndex,
                           partIndex);
          break;
        case 126:
          HASH_INITIALIZED(0x4F20D07BB803C1FELL, v_inner,
                           inner);
          HASH_INITIALIZED(0x66AD900A2301E2FELL, v_title,
                           title);
          break;
        case 127:
          HASH_INITIALIZED(0x41E37259D0CE717FLL, v_equalsLength,
                           equalsLength);
          break;
        default:
          break;
      }
      return LVariableTable::exists(s, hash);
    }
  } variableTable(v_text, v_flags, v_rules, v_forInclusion, v_xmlishElements, v_enableOnlyinclude, v_ignoredTags, v_ignoredElements, v_xmlishRegex, v_elementsRegex, v_stack, v_searchBase, v_revText, v_i, v_accum, v_findEquals, v_findPipe, v_headingIndex, v_inHeading, v_noMoreGT, v_findOnlyinclude, v_fakeLineStart, v_startPos, v_tagEndPos, v_found, v_curChar, v_search, v_currentClosing, v_rule, v_literalLength, v_matches, v_endPos, v_inner, v_wsStart, v_wsEnd, v_wsLength, v_part, v_name, v_lowerName, v_attrStart, v_tagStartPos, v_attrEnd, v_close, v_attr, v_count, v_piece, v_searchStart, v_equalsLength, v_element, v_maxCount, v_matchingCount, v_parts, v_title, v_argIndex, v_partIndex, v_argName, v_argValue, v_names, v_skippedBraces, v_enclosingAccum, v_xml);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  LINE(122,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToXml").create()), 0x0000000075359BAFLL));
  (v_rules = ScalarArrays::sa_[3]);
  (v_forInclusion = bitwise_and(v_flags, 1LL /* parser::PTD_FOR_INCLUSION */));
  (v_xmlishElements = LINE(143,m_parser.o_invoke_few_args("getStripList", 0x151B6C9D84BBC1F5LL, 0)));
  (v_enableOnlyinclude = false);
  if (toBoolean(v_forInclusion)) {
    (v_ignoredTags = ScalarArrays::sa_[4]);
    (v_ignoredElements = ScalarArrays::sa_[5]);
    v_xmlishElements.append(("noinclude"));
    if (!same(LINE(149,x_strpos(toString(v_text), "<onlyinclude>")), false) && !same(x_strpos(toString(v_text), "</onlyinclude>"), false)) {
      (v_enableOnlyinclude = true);
    }
  }
  else {
    (v_ignoredTags = ScalarArrays::sa_[6]);
    (v_ignoredElements = ScalarArrays::sa_[7]);
    v_xmlishElements.append(("includeonly"));
  }
  (v_xmlishRegex = LINE(157,(assignCallTemp(eo_1, x_array_merge(2, v_xmlishElements, Array(ArrayInit(1).set(0, v_ignoredTags).create()))),x_implode("|", eo_1))));
  (v_elementsRegex = LINE(160,concat3("~(", toString(v_xmlishRegex), ")(\?:\\s|\\/>|>)|(!--)~iA")));
  (v_stack = ((Object)(LINE(162,p_ppdstack(p_ppdstack(NEWOBJ(c_ppdstack)())->create())))));
  (v_searchBase = "[{<\n");
  (v_revText = LINE(165,x_strrev(toString(v_text))));
  (v_i = 0LL);
  (v_accum = ref(LINE(168,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
  (v_accum = "<root>");
  (v_findEquals = false);
  (v_findPipe = false);
  (v_headingIndex = 1LL);
  (v_inHeading = false);
  (v_noMoreGT = false);
  (v_findOnlyinclude = v_enableOnlyinclude);
  (v_fakeLineStart = true);
  LOOP_COUNTER(10);
  {
    while (true) {
      LOOP_COUNTER_CHECK(10);
      {
        if (toBoolean(v_findOnlyinclude)) {
          (v_startPos = LINE(183,x_strpos(toString(v_text), "<onlyinclude>", toInt32(v_i))));
          if (same(v_startPos, false)) {
            concat_assign(v_accum, LINE(186,(assignCallTemp(eo_1, x_htmlspecialchars(toString(x_substr(toString(v_text), toInt32(v_i))))),concat3("<ignore>", eo_1, "</ignore>"))));
            break;
          }
          (v_tagEndPos = v_startPos + LINE(189,x_strlen("<onlyinclude>")));
          concat_assign(v_accum, LINE(190,(assignCallTemp(eo_1, x_htmlspecialchars(toString(x_substr(toString(v_text), toInt32(v_i), toInt32(v_tagEndPos - v_i))))),concat3("<ignore>", eo_1, "</ignore>"))));
          (v_i = v_tagEndPos);
          (v_findOnlyinclude = false);
        }
        if (toBoolean(v_fakeLineStart)) {
          (v_found = "line-start");
          (v_curChar = "");
        }
        else {
          (v_search = v_searchBase);
          if (same(v_stack.o_get("top", 0x1854FD73A00D89E8LL), false)) {
            (v_currentClosing = "");
          }
          else {
            (v_currentClosing = v_stack.o_get("top", 0x1854FD73A00D89E8LL).o_get("close", 0x36B43082F052EC6BLL));
            concat_assign(v_search, toString(v_currentClosing));
          }
          if (toBoolean(v_findPipe)) {
            concat_assign(v_search, "|");
          }
          if (toBoolean(v_findEquals)) {
            concat_assign(v_search, "=");
          }
          setNull(v_rule);
          (v_literalLength = LINE(216,x_strcspn(toString(v_text), toString(v_search), toInt32(v_i))));
          if (more(v_literalLength, 0LL)) {
            concat_assign(v_accum, LINE(218,x_htmlspecialchars(toString(x_substr(toString(v_text), toInt32(v_i), toInt32(v_literalLength))))));
            v_i += v_literalLength;
          }
          if (not_less(v_i, LINE(221,x_strlen(toString(v_text))))) {
            if (equal(v_currentClosing, "\n")) {
              (v_curChar = "");
              (v_found = "line-end");
            }
            else {
              break;
            }
          }
          else {
            (v_curChar = v_text.rvalAt(v_i));
            if (equal(v_curChar, "|")) {
              (v_found = "pipe");
            }
            else if (equal(v_curChar, "=")) {
              (v_found = "equals");
            }
            else if (equal(v_curChar, "<")) {
              (v_found = "angle");
            }
            else if (equal(v_curChar, "\n")) {
              if (toBoolean(v_inHeading)) {
                (v_found = "line-end");
              }
              else {
                (v_found = "line-start");
              }
            }
            else if (equal(v_curChar, v_currentClosing)) {
              (v_found = "close");
            }
            else if (isset(v_rules, v_curChar)) {
              (v_found = "open");
              (v_rule = v_rules.rvalAt(v_curChar));
            }
            else {
              ++v_i;
              continue;
            }
          }
        }
        if (equal(v_found, "angle")) {
          (v_matches = false);
          if (toBoolean(v_enableOnlyinclude) && equal(LINE(261,(assignCallTemp(eo_0, toString(v_text)),assignCallTemp(eo_1, toInt32(v_i)),assignCallTemp(eo_2, x_strlen("</onlyinclude>")),x_substr(eo_0, eo_1, eo_2))), "</onlyinclude>")) {
            (v_findOnlyinclude = true);
            continue;
          }
          if (!(toBoolean(LINE(267,x_preg_match(toString(v_elementsRegex), toString(v_text), ref(v_matches), toInt32(0LL), toInt32(v_i + 1LL)))))) {
            concat_assign(v_accum, "&lt;");
            ++v_i;
            continue;
          }
          if (isset(v_matches, 2LL, 0x486AFCC090D5F98CLL) && equal(v_matches.rvalAt(2LL, 0x486AFCC090D5F98CLL), "!--")) {
            (v_endPos = LINE(280,x_strpos(toString(v_text), "-->", toInt32(v_i + 4LL))));
            if (same(v_endPos, false)) {
              (v_inner = LINE(283,x_substr(toString(v_text), toInt32(v_i))));
              concat_assign(v_accum, LINE(284,(assignCallTemp(eo_1, x_htmlspecialchars(toString(v_inner))),concat3("<comment>", eo_1, "</comment>"))));
              (v_i = LINE(285,x_strlen(toString(v_text))));
            }
            else {
              (v_wsStart = toBoolean(v_i) ? ((Variant)((v_i - LINE(288,(assignCallTemp(eo_0, toString(v_revText)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_text)) - v_i)),x_strspn(eo_0, " ", eo_2)))))) : ((Variant)(0LL)));
              (v_wsEnd = v_endPos + 2LL + LINE(291,x_strspn(toString(v_text), " ", toInt32(v_endPos + 3LL))));
              if (more(v_wsStart, 0LL) && equal(LINE(296,x_substr(toString(v_text), toInt32(v_wsStart - 1LL), toInt32(1LL))), "\n") && equal(LINE(297,x_substr(toString(v_text), toInt32(v_wsEnd + 1LL), toInt32(1LL))), "\n")) {
                (v_startPos = v_wsStart);
                (v_endPos = v_wsEnd + 1LL);
                (v_wsLength = v_i - v_wsStart);
                if (more(v_wsLength, 0LL) && same_rev(LINE(304,x_str_repeat(" ", toInt32(v_wsLength))), x_substr(toString(v_accum), toInt32(negate(v_wsLength))))) {
                  (v_accum = LINE(305,x_substr(toString(v_accum), toInt32(0LL), toInt32(negate(v_wsLength)))));
                }
                (v_fakeLineStart = true);
              }
              else {
                (v_startPos = v_i);
                v_endPos += 2LL;
              }
              if (toBoolean(v_stack.o_get("top", 0x1854FD73A00D89E8LL))) {
                (v_part = LINE(316,v_stack.o_get("top", 0x1854FD73A00D89E8LL).o_invoke_few_args("getCurrentPart", 0x1726802D706D7BECLL, 0)));
                if (toObject(v_part)->t___isset("commentEnd") && equal(v_part.o_get("commentEnd", 0x5537E70C21E6C099LL), v_wsStart - 1LL)) {
                  (v_part.o_lval("commentEnd", 0x5537E70C21E6C099LL) = v_wsEnd);
                }
                else {
                  (v_part.o_lval("visualEnd", 0x101972FF17696EF2LL) = v_wsStart);
                  (v_part.o_lval("commentEnd", 0x5537E70C21E6C099LL) = v_endPos);
                }
              }
              (v_i = v_endPos + 1LL);
              (v_inner = LINE(326,x_substr(toString(v_text), toInt32(v_startPos), toInt32(v_endPos - v_startPos + 1LL))));
              concat_assign(v_accum, LINE(327,(assignCallTemp(eo_1, x_htmlspecialchars(toString(v_inner))),concat3("<comment>", eo_1, "</comment>"))));
            }
            continue;
          }
          (v_name = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
          (v_lowerName = LINE(332,x_strtolower(toString(v_name))));
          (v_attrStart = v_i + LINE(333,x_strlen(toString(v_name))) + 1LL);
          (v_tagEndPos = toBoolean(v_noMoreGT) ? ((Variant)(false)) : ((Variant)(LINE(336,x_strpos(toString(v_text), ">", toInt32(v_attrStart))))));
          if (same(v_tagEndPos, false)) {
            (v_noMoreGT = true);
            concat_assign(v_accum, "&lt;");
            ++v_i;
            continue;
          }
          if (LINE(347,x_in_array(v_lowerName, v_ignoredTags))) {
            concat_assign(v_accum, LINE(348,(assignCallTemp(eo_1, x_htmlspecialchars(toString(x_substr(toString(v_text), toInt32(v_i), toInt32(v_tagEndPos - v_i + 1LL))))),concat3("<ignore>", eo_1, "</ignore>"))));
            (v_i = v_tagEndPos + 1LL);
            continue;
          }
          (v_tagStartPos = v_i);
          if (equal(v_text.rvalAt(v_tagEndPos - 1LL), "/")) {
            (v_attrEnd = v_tagEndPos - 1LL);
            setNull(v_inner);
            (v_i = v_tagEndPos + 1LL);
            (v_close = "");
          }
          else {
            (v_attrEnd = v_tagEndPos);
            if (toBoolean(LINE(363,(assignCallTemp(eo_0, LINE(362,(assignCallTemp(eo_6, x_preg_quote(toString(v_name), "/")),concat3("/<\\/", eo_6, "\\s*>/i")))),assignCallTemp(eo_1, toString(v_text)),assignCallTemp(eo_2, ref(v_matches)),assignCallTemp(eo_4, toInt32(v_tagEndPos + 1LL)),x_preg_match(eo_0, eo_1, eo_2, toInt32(256LL) /* PREG_OFFSET_CAPTURE */, eo_4))))) {
              (v_inner = LINE(365,x_substr(toString(v_text), toInt32(v_tagEndPos + 1LL), toInt32(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL) - v_tagEndPos - 1LL))));
              (v_i = v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL) + LINE(366,x_strlen(toString(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
              (v_close = LINE(367,(assignCallTemp(eo_1, x_htmlspecialchars(toString(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))),concat3("<close>", eo_1, "</close>"))));
            }
            else {
              (v_inner = LINE(370,x_substr(toString(v_text), toInt32(v_tagEndPos + 1LL))));
              (v_i = LINE(371,x_strlen(toString(v_text))));
              (v_close = "");
            }
          }
          if (LINE(376,x_in_array(v_lowerName, v_ignoredElements))) {
            concat_assign(v_accum, LINE(378,(assignCallTemp(eo_1, LINE(377,x_htmlspecialchars(toString(x_substr(toString(v_text), toInt32(v_tagStartPos), toInt32(v_i - v_tagStartPos)))))),concat3("<ignore>", eo_1, "</ignore>"))));
            continue;
          }
          concat_assign(v_accum, "<ext>");
          if (not_more(v_attrEnd, v_attrStart)) {
            (v_attr = "");
          }
          else {
            (v_attr = LINE(386,x_substr(toString(v_text), toInt32(v_attrStart), toInt32(v_attrEnd - v_attrStart))));
          }
          concat_assign(v_accum, concat(concat_rev(LINE(391,x_htmlspecialchars(toString(v_attr))), (assignCallTemp(eo_1, LINE(388,x_htmlspecialchars(toString(v_name)))),concat3("<name>", eo_1, "</name><attr>"))), "</attr>"));
          if (!same(v_inner, null)) {
            concat_assign(v_accum, LINE(393,(assignCallTemp(eo_1, x_htmlspecialchars(toString(v_inner))),concat3("<inner>", eo_1, "</inner>"))));
          }
          concat_assign(v_accum, concat(toString(v_close), "</ext>"));
        }
        else if (equal(v_found, "line-start")) {
          if (toBoolean(v_fakeLineStart)) {
            (v_fakeLineStart = false);
          }
          else {
            concat_assign(v_accum, toString(v_curChar));
            v_i++;
          }
          (v_count = LINE(408,x_strspn(toString(v_text), "=", toInt32(v_i), toInt32(6LL))));
          if (equal(v_count, 1LL) && toBoolean(v_findEquals)) {
          }
          else if (more(v_count, 0LL)) {
            (v_piece = Array(ArrayInit(5).set(0, "open", "\n", 0x6A760D2EC60228C6LL).set(1, "close", "\n", 0x36B43082F052EC6BLL).set(2, "parts", Array(ArrayInit(1).set(0, ((Object)(LINE(417,p_ppdpart(p_ppdpart(NEWOBJ(c_ppdpart)())->create(x_str_repeat("=", toInt32(v_count)))))))).create()), 0x2708FDA74562AD8DLL).set(3, "startPos", v_i, 0x749C3FAB1E74371DLL).set(4, "count", v_count, 0x3D66B5980D54BABBLL).create()));
            LINE(420,v_stack.o_invoke_few_args("push", 0x321E2BF5D878AA38LL, 1, v_piece));
            (v_accum = ref(LINE(421,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
            (v_flags = LINE(422,v_stack.o_invoke_few_args("getFlags", 0x27E7DBA875AD17E1LL, 0)));
            LINE(423,extract(variables, toArray(v_flags)));
            v_i += v_count;
          }
        }
        else if (equal(v_found, "line-end")) {
          (v_piece = v_stack.o_get("top", 0x1854FD73A00D89E8LL));
          LINE(431,x_assert(equal(v_piece.o_get("open", 0x6A760D2EC60228C6LL), "\n")));
          (v_part = LINE(432,v_piece.o_invoke_few_args("getCurrentPart", 0x1726802D706D7BECLL, 0)));
          (v_wsLength = LINE(435,(assignCallTemp(eo_0, toString(v_revText)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_text)) - v_i)),x_strspn(eo_0, " \t", eo_2))));
          (v_searchStart = v_i - v_wsLength);
          if (toObject(v_part)->t___isset("commentEnd") && equal(v_searchStart - 1LL, v_part.o_get("commentEnd", 0x5537E70C21E6C099LL))) {
            (v_searchStart = v_part.o_get("visualEnd", 0x101972FF17696EF2LL));
            v_searchStart -= LINE(441,(assignCallTemp(eo_0, toString(v_revText)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_text)) - v_searchStart)),x_strspn(eo_0, " \t", eo_2)));
          }
          (v_count = v_piece.o_get("count", 0x3D66B5980D54BABBLL));
          (v_equalsLength = LINE(444,(assignCallTemp(eo_0, toString(v_revText)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_text)) - v_searchStart)),x_strspn(eo_0, "=", eo_2))));
          if (more(v_equalsLength, 0LL)) {
            if (equal(v_i - v_equalsLength, v_piece.o_get("startPos", 0x749C3FAB1E74371DLL))) {
              (v_count = v_equalsLength);
              if (less(v_count, 3LL)) {
                (v_count = 0LL);
              }
              else {
                (v_count = LINE(454,(assignCallTemp(eo_1, x_intval(divide((v_count - 1LL), 2LL))),x_min(2, 6LL, Array(ArrayInit(1).set(0, eo_1).create())))));
              }
            }
            else {
              (v_count = LINE(457,x_min(2, v_equalsLength, Array(ArrayInit(1).set(0, v_count).create()))));
            }
            if (more(v_count, 0LL)) {
              (v_element = concat("<h level=\"", LINE(461,concat6(toString(v_count), "\" i=\"", toString(v_headingIndex), "\">", toString(v_accum), "</h>"))));
              v_headingIndex++;
            }
            else {
              (v_element = v_accum);
            }
          }
          else {
            (v_element = v_accum);
          }
          LINE(472,v_stack.o_invoke_few_args("pop", 0x773C5A963CD2AC13LL, 0));
          (v_accum = ref(LINE(473,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
          (v_flags = LINE(474,v_stack.o_invoke_few_args("getFlags", 0x27E7DBA875AD17E1LL, 0)));
          LINE(475,extract(variables, toArray(v_flags)));
          concat_assign(v_accum, toString(v_element));
        }
        else if (equal(v_found, "open")) {
          (v_count = LINE(488,x_strspn(toString(v_text), toString(v_curChar), toInt32(v_i))));
          if (not_less(v_count, v_rule.rvalAt("min", 0x66FFC7365E2D00CALL))) {
            (v_piece = Array(ArrayInit(4).set(0, "open", v_curChar, 0x6A760D2EC60228C6LL).set(1, "close", v_rule.rvalAt("end", 0x6DE935C204DC3D01LL), 0x36B43082F052EC6BLL).set(2, "count", v_count, 0x3D66B5980D54BABBLL).set(3, "lineStart", (more(v_i, 0LL) && equal(v_text.rvalAt(v_i - 1LL), "\n")), 0x0F84CB175598D0A5LL).create()));
            LINE(500,v_stack.o_invoke_few_args("push", 0x321E2BF5D878AA38LL, 1, v_piece));
            (v_accum = ref(LINE(501,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
            (v_flags = LINE(502,v_stack.o_invoke_few_args("getFlags", 0x27E7DBA875AD17E1LL, 0)));
            LINE(503,extract(variables, toArray(v_flags)));
          }
          else {
            concat_assign(v_accum, LINE(506,x_htmlspecialchars(x_str_repeat(toString(v_curChar), toInt32(v_count)))));
          }
          v_i += v_count;
        }
        else if (equal(v_found, "close")) {
          (v_piece = v_stack.o_get("top", 0x1854FD73A00D89E8LL));
          (v_maxCount = v_piece.o_get("count", 0x3D66B5980D54BABBLL));
          (v_count = LINE(515,x_strspn(toString(v_text), toString(v_curChar), toInt32(v_i), toInt32(v_maxCount))));
          (v_matchingCount = 0LL);
          (v_rule = v_rules.rvalAt(v_piece.o_get("open", 0x6A760D2EC60228C6LL)));
          if (more(v_count, v_rule.rvalAt("max", 0x022BD5C706BD9850LL))) {
            (v_matchingCount = v_rule.rvalAt("max", 0x022BD5C706BD9850LL));
          }
          else {
            (v_matchingCount = v_count);
            LOOP_COUNTER(11);
            {
              while (more(v_matchingCount, 0LL) && !(LINE(530,x_array_key_exists(v_matchingCount, v_rule.rvalAt("names", 0x51A687446C1579F2LL))))) {
                LOOP_COUNTER_CHECK(11);
                {
                  --v_matchingCount;
                }
              }
            }
          }
          if (not_more(v_matchingCount, 0LL)) {
            concat_assign(v_accum, LINE(538,x_htmlspecialchars(x_str_repeat(toString(v_curChar), toInt32(v_count)))));
            v_i += v_count;
            continue;
          }
          (v_name = v_rule.rvalAt("names", 0x51A687446C1579F2LL).rvalAt(v_matchingCount));
          if (same(v_name, null)) {
            (v_element = concat_rev(LINE(545,x_str_repeat(toString(v_rule.rvalAt("end", 0x6DE935C204DC3D01LL)), toInt32(v_matchingCount))), toString(v_piece.o_invoke_few_args("breakSyntax", 0x1CF88FA5EFC88C39LL, 1, v_matchingCount))));
          }
          else {
            (v_parts = v_piece.o_get("parts", 0x2708FDA74562AD8DLL));
            (v_title = v_parts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("out", 0x7C801AC012E9F722LL));
            v_parts.weakRemove(0LL, 0x77CFA1EEF01BCA90LL);
            if (equal(v_maxCount, v_matchingCount) && !(empty(toObject(v_piece).o_get("lineStart", 0x0F84CB175598D0A5LL)))) {
              (v_attr = " lineStart=\"1\"");
            }
            else {
              (v_attr = "");
            }
            (v_element = LINE(549,(assignCallTemp(eo_1, toString(v_name)),assignCallTemp(eo_2, toString(v_attr)),assignCallTemp(eo_4, LINE(562,concat3("<title>", toString(v_title), "</title>"))),concat5("<", eo_1, eo_2, ">", eo_4))));
            (v_argIndex = 1LL);
            {
              LOOP_COUNTER(12);
              for (ArrayIterPtr iter14 = v_parts.begin("preprocessor_dom"); !iter14->end(); iter14->next()) {
                LOOP_COUNTER_CHECK(12);
                v_part = iter14->second();
                v_partIndex = iter14->first();
                {
                  if (toObject(v_part)->t___isset("eqpos")) {
                    (v_argName = LINE(566,x_substr(toString(v_part.o_get("out", 0x7C801AC012E9F722LL)), toInt32(0LL), toInt32(v_part.o_get("eqpos", 0x33437B517F4D41D7LL)))));
                    (v_argValue = LINE(567,x_substr(toString(v_part.o_get("out", 0x7C801AC012E9F722LL)), toInt32(v_part.o_get("eqpos", 0x33437B517F4D41D7LL) + 1LL))));
                    concat_assign(v_element, LINE(568,concat5("<part><name>", toString(v_argName), "</name>=<value>", toString(v_argValue), "</value></part>")));
                  }
                  else {
                    concat_assign(v_element, LINE(570,concat5("<part><name index=\"", toString(v_argIndex), "\" /><value>", toString(v_part.o_get("out", 0x7C801AC012E9F722LL)), "</value></part>")));
                    v_argIndex++;
                  }
                }
              }
            }
            concat_assign(v_element, LINE(574,concat3("</", toString(v_name), ">")));
          }
          v_i += v_matchingCount;
          LINE(581,v_stack.o_invoke_few_args("pop", 0x773C5A963CD2AC13LL, 0));
          (v_accum = ref(LINE(582,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
          if (less(v_matchingCount, v_piece.o_get("count", 0x3D66B5980D54BABBLL))) {
            (v_piece.o_lval("parts", 0x2708FDA74562AD8DLL) = Array(ArrayInit(1).set(0, ((Object)(LINE(586,p_ppdpart(p_ppdpart(NEWOBJ(c_ppdpart)())->create()))))).create()));
            v_piece.o_lval("count", 0x3D66B5980D54BABBLL) -= v_matchingCount;
            (v_names = v_rules.rvalAt(v_piece.o_get("open", 0x6A760D2EC60228C6LL)).rvalAt("names", 0x51A687446C1579F2LL));
            (v_skippedBraces = 0LL);
            (v_enclosingAccum = ref(v_accum));
            LOOP_COUNTER(15);
            {
              while (toBoolean(v_piece.o_get("count", 0x3D66B5980D54BABBLL))) {
                LOOP_COUNTER_CHECK(15);
                {
                  if (LINE(593,x_array_key_exists(v_piece.o_get("count", 0x3D66B5980D54BABBLL), v_names))) {
                    LINE(594,v_stack.o_invoke_few_args("push", 0x321E2BF5D878AA38LL, 1, v_piece));
                    (v_accum = ref(LINE(595,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
                    break;
                  }
                  --lval(v_piece.o_lval("count", 0x3D66B5980D54BABBLL));
                  v_skippedBraces++;
                }
              }
            }
            concat_assign(v_enclosingAccum, LINE(601,x_str_repeat(toString(v_piece.o_get("open", 0x6A760D2EC60228C6LL)), toInt32(v_skippedBraces))));
          }
          (v_flags = LINE(603,v_stack.o_invoke_few_args("getFlags", 0x27E7DBA875AD17E1LL, 0)));
          LINE(604,extract(variables, toArray(v_flags)));
          concat_assign(v_accum, toString(v_element));
        }
        else if (equal(v_found, "pipe")) {
          (v_findEquals = true);
          LINE(612,v_stack.o_invoke_few_args("addPart", 0x499E72B719A4CAC2LL, 0));
          (v_accum = ref(LINE(613,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
          ++v_i;
        }
        else if (equal(v_found, "equals")) {
          (v_findEquals = false);
          (LINE(619,v_stack.o_invoke_few_args("getCurrentPart", 0x1726802D706D7BECLL, 0)).o_lval("eqpos", 0x33437B517F4D41D7LL) = x_strlen(toString(v_accum)));
          concat_assign(v_accum, "=");
          ++v_i;
        }
      }
    }
  }
  {
    LOOP_COUNTER(16);
    Variant map17 = v_stack.o_get("stack", 0x5084E637B870262BLL);
    for (ArrayIterPtr iter18 = map17.begin("preprocessor_dom"); !iter18->end(); iter18->next()) {
      LOOP_COUNTER_CHECK(16);
      v_piece = iter18->second();
      {
        concat_assign(v_stack.o_lval("rootAccum", 0x5F4D08F214717E0FLL), toString(LINE(627,v_piece.o_invoke_few_args("breakSyntax", 0x1CF88FA5EFC88C39LL, 0))));
      }
    }
  }
  concat_assign(v_stack.o_lval("rootAccum", 0x5F4D08F214717E0FLL), "</root>");
  (v_xml = v_stack.o_get("rootAccum", 0x5F4D08F214717E0FLL));
  LINE(632,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Preprocessor_DOM::preprocessToXml").create()), 0x00000000B599F276LL));
  return v_xml;
} /* function */
/* SRC: Preprocessor_DOM.php line 806 */
Variant c_ppframe_dom::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppframe_dom::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ppframe_dom::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("preprocessor", m_preprocessor.isReferenced() ? ref(m_preprocessor) : m_preprocessor));
  props.push_back(NEW(ArrayElement)("parser", m_parser.isReferenced() ? ref(m_parser) : m_parser));
  props.push_back(NEW(ArrayElement)("title", m_title.isReferenced() ? ref(m_title) : m_title));
  props.push_back(NEW(ArrayElement)("titleCache", m_titleCache.isReferenced() ? ref(m_titleCache) : m_titleCache));
  props.push_back(NEW(ArrayElement)("loopCheckHash", m_loopCheckHash.isReferenced() ? ref(m_loopCheckHash) : m_loopCheckHash));
  props.push_back(NEW(ArrayElement)("depth", m_depth.isReferenced() ? ref(m_depth) : m_depth));
  c_ObjectData::o_get(props);
}
bool c_ppframe_dom::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_EXISTS_STRING(0x18A8B9D71D4F2D02LL, parser, 6);
      break;
    case 3:
      HASH_EXISTS_STRING(0x0CD4D2916BE2CE13LL, titleCache, 10);
      break;
    case 5:
      HASH_EXISTS_STRING(0x757439BA20184735LL, loopCheckHash, 13);
      break;
    case 7:
      HASH_EXISTS_STRING(0x1AE700EECE6274C7LL, depth, 5);
      break;
    case 14:
      HASH_EXISTS_STRING(0x2940E17D1F5401AELL, preprocessor, 12);
      HASH_EXISTS_STRING(0x66AD900A2301E2FELL, title, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppframe_dom::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                         parser, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x0CD4D2916BE2CE13LL, m_titleCache,
                         titleCache, 10);
      break;
    case 5:
      HASH_RETURN_STRING(0x757439BA20184735LL, m_loopCheckHash,
                         loopCheckHash, 13);
      break;
    case 7:
      HASH_RETURN_STRING(0x1AE700EECE6274C7LL, m_depth,
                         depth, 5);
      break;
    case 14:
      HASH_RETURN_STRING(0x2940E17D1F5401AELL, m_preprocessor,
                         preprocessor, 12);
      HASH_RETURN_STRING(0x66AD900A2301E2FELL, m_title,
                         title, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppframe_dom::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_SET_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                      parser, 6);
      break;
    case 3:
      HASH_SET_STRING(0x0CD4D2916BE2CE13LL, m_titleCache,
                      titleCache, 10);
      break;
    case 5:
      HASH_SET_STRING(0x757439BA20184735LL, m_loopCheckHash,
                      loopCheckHash, 13);
      break;
    case 7:
      HASH_SET_STRING(0x1AE700EECE6274C7LL, m_depth,
                      depth, 5);
      break;
    case 14:
      HASH_SET_STRING(0x2940E17D1F5401AELL, m_preprocessor,
                      preprocessor, 12);
      HASH_SET_STRING(0x66AD900A2301E2FELL, m_title,
                      title, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppframe_dom::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                         parser, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x0CD4D2916BE2CE13LL, m_titleCache,
                         titleCache, 10);
      break;
    case 5:
      HASH_RETURN_STRING(0x757439BA20184735LL, m_loopCheckHash,
                         loopCheckHash, 13);
      break;
    case 7:
      HASH_RETURN_STRING(0x1AE700EECE6274C7LL, m_depth,
                         depth, 5);
      break;
    case 14:
      HASH_RETURN_STRING(0x2940E17D1F5401AELL, m_preprocessor,
                         preprocessor, 12);
      HASH_RETURN_STRING(0x66AD900A2301E2FELL, m_title,
                         title, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppframe_dom::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppframe_dom)
ObjectData *c_ppframe_dom::create(Variant v_preprocessor) {
  init();
  t___construct(v_preprocessor);
  return this;
}
ObjectData *c_ppframe_dom::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_ppframe_dom::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_ppframe_dom::cloneImpl() {
  c_ppframe_dom *obj = NEW(c_ppframe_dom)();
  cloneSet(obj);
  return obj;
}
void c_ppframe_dom::cloneSet(c_ppframe_dom *clone) {
  clone->m_preprocessor = m_preprocessor.isReferenced() ? ref(m_preprocessor) : m_preprocessor;
  clone->m_parser = m_parser.isReferenced() ? ref(m_parser) : m_parser;
  clone->m_title = m_title.isReferenced() ? ref(m_title) : m_title;
  clone->m_titleCache = m_titleCache.isReferenced() ? ref(m_titleCache) : m_titleCache;
  clone->m_loopCheckHash = m_loopCheckHash.isReferenced() ? ref(m_loopCheckHash) : m_loopCheckHash;
  clone->m_depth = m_depth.isReferenced() ? ref(m_depth) : m_depth;
  ObjectData::cloneSet(clone);
}
Variant c_ppframe_dom::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        int count = params.size();
        if (count <= 3) return (t_virtualbracketedimplode(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        return (t_virtualbracketedimplode(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        int count = params.size();
        if (count <= 1) return (t_expand(params.rvalAt(0)));
        return (t_expand(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        int count = params.size();
        if (count <= 2) return (t_implodewithflags(count, params.rvalAt(0), params.rvalAt(1)));
        return (t_implodewithflags(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        int count = params.size();
        if (count <= 1) return (t_virtualimplode(count, params.rvalAt(0)));
        return (t_virtualimplode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        int count = params.size();
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(params.rvalAt(0)));
        return (t_newchild(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        int count = params.size();
        if (count <= 1) return (t_implode(count, params.rvalAt(0)));
        return (t_implode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(params.rvalAt(0)));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppframe_dom::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        if (count <= 3) return (t_virtualbracketedimplode(count, a0, a1, a2));
        Array params;
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualbracketedimplode(count,a0, a1, a2, params));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        if (count <= 1) return (t_expand(a0));
        return (t_expand(a0, a1));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        if (count <= 2) return (t_implodewithflags(count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implodewithflags(count,a0, a1, params));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        if (count <= 1) return (t_virtualimplode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualimplode(count,a0, params));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(a0));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(a0));
        return (t_newchild(a0, a1));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        if (count <= 1) return (t_implode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implode(count,a0, params));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(a0));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppframe_dom::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppframe_dom$os_get(const char *s) {
  return c_ppframe_dom::os_get(s, -1);
}
Variant &cw_ppframe_dom$os_lval(const char *s) {
  return c_ppframe_dom::os_lval(s, -1);
}
Variant cw_ppframe_dom$os_constant(const char *s) {
  return c_ppframe_dom::os_constant(s);
}
Variant cw_ppframe_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppframe_dom::os_invoke(c, s, params, -1, fatal);
}
void c_ppframe_dom::init() {
  m_preprocessor = null;
  m_parser = null;
  m_title = null;
  m_titleCache = null;
  m_loopCheckHash = null;
  m_depth = null;
}
/* SRC: Preprocessor_DOM.php line 827 */
void c_ppframe_dom::t___construct(Variant v_preprocessor) {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_preprocessor = v_preprocessor);
  (m_parser = v_preprocessor.o_get("parser", 0x18A8B9D71D4F2D02LL));
  (m_title = m_parser.o_get("mTitle", 0x4C804C46307BF0EALL));
  (m_titleCache = Array(ArrayInit(1).set(0, toBoolean(m_title) ? ((Variant)(LINE(831,m_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)))) : ((Variant)(false))).create()));
  (m_loopCheckHash = ScalarArrays::sa_[0]);
  (m_depth = 0LL);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_DOM.php line 840 */
p_pptemplateframe_dom c_ppframe_dom::t_newchild(Variant v_args //  = false
, Variant v_title //  = false
) {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::newChild);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_namedArgs;
  Variant v_numberedArgs;
  Variant v_xpath;
  Variant v_arg;
  Variant v_nameNodes;
  Variant v_value;
  Variant v_index;
  String v_name;

  (v_namedArgs = ScalarArrays::sa_[0]);
  (v_numberedArgs = ScalarArrays::sa_[0]);
  if (same(v_title, false)) {
    (v_title = m_title);
  }
  if (!same(v_args, false)) {
    (v_xpath = false);
    if (instanceOf(v_args, "PPNode")) {
      (v_args = v_args.o_get("node", 0x75AF6DCF22783AC1LL));
    }
    {
      LOOP_COUNTER(19);
      for (ArrayIterPtr iter21 = v_args.begin("ppframe_dom"); !iter21->end(); iter21->next()) {
        LOOP_COUNTER_CHECK(19);
        v_arg = iter21->second();
        {
          if (!(toBoolean(v_xpath))) {
            (v_xpath = ((Object)(LINE(853,p_domxpath(p_domxpath(NEWOBJ(c_domxpath)())->create(v_arg.o_get("ownerDocument", 0x0222D017809853C8LL)))))));
          }
          (v_nameNodes = LINE(856,v_xpath.o_invoke_few_args("query", 0x356758D4414DA377LL, 2, "name", v_arg)));
          (v_value = LINE(857,v_xpath.o_invoke_few_args("query", 0x356758D4414DA377LL, 2, "value", v_arg)));
          if (toBoolean((assignCallTemp(eo_0, toObject(LINE(858,v_nameNodes.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)))),eo_0.o_invoke_few_args("hasAttributes", 0x3CFA6EECF6F5AEB1LL, 0)))) {
            (v_index = (assignCallTemp(eo_1, toObject(LINE(860,v_nameNodes.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)).o_get("attributes", 0x3E5975BA0FC37E03LL))),eo_1.o_invoke_few_args("getNamedItem", 0x5D3CEC627F9ADC3ALL, 1, "index")).o_get("textContent", 0x716294CDF72E1E6ELL));
            v_numberedArgs.set(v_index, (LINE(861,v_value.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL))));
            v_namedArgs.weakRemove(v_index);
          }
          else {
            (v_name = LINE(865,x_trim(toString((assignCallTemp(eo_2, ref(v_nameNodes.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL))),o_root_invoke("expand", Array(ArrayInit(2).set(0, eo_2).set(1, 4LL /* ppframe::STRIP_COMMENTS */).create()), 0x39B7BB05F05A37CDLL))))));
            v_namedArgs.set(v_name, (LINE(866,v_value.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL))));
            v_numberedArgs.weakRemove(v_name);
          }
        }
      }
    }
  }
  return ((Object)(LINE(871,p_pptemplateframe_dom(p_pptemplateframe_dom(NEWOBJ(c_pptemplateframe_dom)())->create(m_preprocessor, ((Object)(this)), v_numberedArgs, v_namedArgs, v_title)))));
} /* function */
/* SRC: Preprocessor_DOM.php line 874 */
Variant c_ppframe_dom::t_expand(Variant v_root, Variant v_flags //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::expand);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_expansionDepth __attribute__((__unused__)) = g->sv_ppframe_dom$$expand$$expansionDepth.lvalAt(this->o_getClassName());
  Variant &inited_sv_expansionDepth __attribute__((__unused__)) = g->inited_sv_ppframe_dom$$expand$$expansionDepth.lvalAt(this->o_getClassName());
  Variant v_outStack;
  Variant v_iteratorStack;
  Variant v_indexStack;
  int64 v_level = 0;
  Variant v_iteratorNode;
  Variant v_out;
  Variant v_index;
  Variant v_contextNode;
  Variant v_newIterator;
  p_domxpath v_xpath;
  Variant v_titles;
  Variant v_title;
  Variant v_parts;
  Variant v_lineStart;
  Variant v_params;
  Variant v_ret;
  Variant v_names;
  Variant v_attrs;
  Variant v_inners;
  Variant v_closes;
  Variant v_s;
  Variant v_headingIndex;
  Variant v_titleText;
  int64 v_serial = 0;
  Variant v_marker;
  Variant v_count;

  if (!inited_sv_expansionDepth) {
    (sv_expansionDepth = 0LL);
    inited_sv_expansionDepth = true;
  }
  if (LINE(876,x_is_string(v_root))) {
    return v_root;
  }
  if (more(++lval(m_parser.o_lval("mPPNodeCount", 0x17B591196693E481LL)), m_parser.o_get("mOptions", 0x79AC63B0E96DC6E9LL).o_get("mMaxPPNodeCount", 0x3DD526BE5013BD55LL))) {
    return "<span class=\"error\">Node-count limit exceeded</span>";
  }
  if (more(sv_expansionDepth, m_parser.o_get("mOptions", 0x79AC63B0E96DC6E9LL).o_get("mMaxPPExpandDepth", 0x2F1977A57AF7D6DELL))) {
    return "<span class=\"error\">Expansion depth limit exceeded</span>";
  }
  LINE(888,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "PPFrame_DOM::expand").create()), 0x0000000075359BAFLL));
  ++sv_expansionDepth;
  if (instanceOf(v_root, "PPNode_DOM")) {
    (v_root = v_root.o_get("node", 0x75AF6DCF22783AC1LL));
  }
  if (instanceOf(v_root, "DOMDocument")) {
    (v_root = v_root.o_get("documentElement", 0x34E37E9EAB48521BLL));
  }
  (v_outStack = ScalarArrays::sa_[9]);
  (v_iteratorStack = Array(ArrayInit(2).set(0, false).set(1, v_root).create()));
  (v_indexStack = ScalarArrays::sa_[10]);
  LOOP_COUNTER(22);
  {
    while (more(LINE(902,x_count(v_iteratorStack)), 1LL)) {
      LOOP_COUNTER_CHECK(22);
      {
        (v_level = LINE(903,x_count(v_outStack)) - 1LL);
        (v_iteratorNode = ref(lval(v_iteratorStack.lvalAt(v_level))));
        (v_out = ref(lval(v_outStack.lvalAt(v_level))));
        (v_index = ref(lval(v_indexStack.lvalAt(v_level))));
        if (instanceOf(v_iteratorNode, "PPNode_DOM")) (v_iteratorNode = v_iteratorNode.o_get("node", 0x75AF6DCF22783AC1LL));
        if (LINE(910,x_is_array(v_iteratorNode))) {
          if (not_less(v_index, LINE(911,x_count(v_iteratorNode)))) {
            v_iteratorStack.set(v_level, (false));
            (v_contextNode = false);
          }
          else {
            (v_contextNode = v_iteratorNode.rvalAt(v_index));
            v_index++;
          }
        }
        else if (instanceOf(v_iteratorNode, "DOMNodeList")) {
          if (not_less(v_index, v_iteratorNode.o_get("length", 0x2993E8EB119CAB21LL))) {
            v_iteratorStack.set(v_level, (false));
            (v_contextNode = false);
          }
          else {
            (v_contextNode = LINE(925,v_iteratorNode.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, v_index)));
            v_index++;
          }
        }
        else {
          (v_contextNode = v_iteratorStack.rvalAt(v_level));
          v_iteratorStack.set(v_level, (false));
        }
        if (instanceOf(v_contextNode, "PPNode_DOM")) (v_contextNode = v_contextNode.o_get("node", 0x75AF6DCF22783AC1LL));
        (v_newIterator = false);
        if (same(v_contextNode, false)) {
        }
        else if (LINE(941,x_is_string(v_contextNode))) {
          concat_assign(v_out, toString(v_contextNode));
        }
        else if (LINE(943,x_is_array(v_contextNode)) || instanceOf(v_contextNode, "DOMNodeList")) {
          (v_newIterator = v_contextNode);
        }
        else if (instanceOf(v_contextNode, "DOMNode")) {
          if (equal(v_contextNode.o_get("nodeType", 0x3135FA95D8255239LL), 3LL /* XML_TEXT_NODE */)) {
            concat_assign(v_out, toString(v_contextNode.o_get("nodeValue", 0x410C368CB95B4985LL)));
          }
          else if (equal(v_contextNode.o_get("nodeName", 0x009B66FACA616B3CLL), "template")) {
            ((Object)((v_xpath = ((Object)(LINE(950,p_domxpath(p_domxpath(NEWOBJ(c_domxpath)())->create(v_contextNode.o_get("ownerDocument", 0x0222D017809853C8LL)))))))));
            (v_titles = LINE(951,v_xpath->t_query("title", toObject(v_contextNode))));
            (v_title = LINE(952,v_titles.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)));
            (v_parts = LINE(953,v_xpath->t_query("part", toObject(v_contextNode))));
            if (toBoolean(bitwise_and(v_flags, throw_fatal("unknown class constant ppframe_dom::NO_TEMPLATES")))) {
              (v_newIterator = LINE(955,o_root_invoke_few_args("virtualBracketedImplode", 0x409618A98590618BLL, 5, "{{", "|", "}}", v_title, v_parts)));
            }
            else {
              (v_lineStart = LINE(957,v_contextNode.o_invoke_few_args("getAttribute", 0x49F89C466612FC28LL, 1, "lineStart")));
              (v_params = (assignCallTemp(eo_0, ((Object)(LINE(959,p_ppnode_dom(p_ppnode_dom(NEWOBJ(c_ppnode_dom)())->create(v_title)))))),assignCallTemp(eo_1, ((Object)(LINE(960,p_ppnode_dom(p_ppnode_dom(NEWOBJ(c_ppnode_dom)())->create(v_parts)))))),assignCallTemp(eo_2, v_lineStart),Array(ArrayInit(3).set(0, "title", eo_0, 0x66AD900A2301E2FELL).set(1, "parts", eo_1, 0x2708FDA74562AD8DLL).set(2, "lineStart", eo_2, 0x0F84CB175598D0A5LL).create())));
              (v_ret = LINE(962,m_parser.o_invoke_few_args("braceSubstitution", 0x392E383BDD5B10F7LL, 2, v_params, this)));
              if (isset(v_ret, "object", 0x7F30BC4E222B1861LL)) {
                (v_newIterator = v_ret.rvalAt("object", 0x7F30BC4E222B1861LL));
              }
              else {
                concat_assign(v_out, toString(v_ret.rvalAt("text", 0x2A28A0084DD3A743LL)));
              }
            }
          }
          else if (equal(v_contextNode.o_get("nodeName", 0x009B66FACA616B3CLL), "tplarg")) {
            ((Object)((v_xpath = ((Object)(LINE(971,p_domxpath(p_domxpath(NEWOBJ(c_domxpath)())->create(v_contextNode.o_get("ownerDocument", 0x0222D017809853C8LL)))))))));
            (v_titles = LINE(972,v_xpath->t_query("title", toObject(v_contextNode))));
            (v_title = LINE(973,v_titles.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)));
            (v_parts = LINE(974,v_xpath->t_query("part", toObject(v_contextNode))));
            if (toBoolean(bitwise_and(v_flags, throw_fatal("unknown class constant ppframe_dom::NO_ARGS")))) {
              (v_newIterator = LINE(976,o_root_invoke_few_args("virtualBracketedImplode", 0x409618A98590618BLL, 5, "{{{", "|", "}}}", v_title, v_parts)));
            }
            else {
              (v_params = (assignCallTemp(eo_0, ((Object)(LINE(979,p_ppnode_dom(p_ppnode_dom(NEWOBJ(c_ppnode_dom)())->create(v_title)))))),assignCallTemp(eo_1, ((Object)(LINE(980,p_ppnode_dom(p_ppnode_dom(NEWOBJ(c_ppnode_dom)())->create(v_parts)))))),Array(ArrayInit(2).set(0, "title", eo_0, 0x66AD900A2301E2FELL).set(1, "parts", eo_1, 0x2708FDA74562AD8DLL).create())));
              (v_ret = LINE(981,m_parser.o_invoke_few_args("argSubstitution", 0x333DE406BD9436D1LL, 2, v_params, this)));
              if (isset(v_ret, "object", 0x7F30BC4E222B1861LL)) {
                (v_newIterator = v_ret.rvalAt("object", 0x7F30BC4E222B1861LL));
              }
              else {
                concat_assign(v_out, toString(v_ret.rvalAt("text", 0x2A28A0084DD3A743LL)));
              }
            }
          }
          else if (equal(v_contextNode.o_get("nodeName", 0x009B66FACA616B3CLL), "comment")) {
            if (toBoolean(m_parser.o_get("ot", 0x48663D83DD673D4DLL).rvalAt("html", 0x5CA91C812230855DLL)) || (toBoolean(m_parser.o_get("ot", 0x48663D83DD673D4DLL).rvalAt("pre", 0x633242EDD179FBACLL)) && toBoolean(LINE(992,m_parser.o_get("mOptions", 0x79AC63B0E96DC6E9LL).o_invoke_few_args("getRemoveComments", 0x4E73BF37061917D4LL, 0)))) || (toBoolean(bitwise_and(v_flags, throw_fatal("unknown class constant ppframe_dom::STRIP_COMMENTS"))))) {
              concat_assign(v_out, "");
            }
            else if (toBoolean(m_parser.o_get("ot", 0x48663D83DD673D4DLL).rvalAt("wiki", 0x24648D2BCD794D21LL)) && !((toBoolean(bitwise_and(v_flags, throw_fatal("unknown class constant ppframe_dom::RECOVER_COMMENTS")))))) {
              concat_assign(v_out, toString(LINE(1000,m_parser.o_invoke_few_args("insertStripItem", 0x0A4CA37F020D70E2LL, 1, v_contextNode.o_lval("textContent", 0x716294CDF72E1E6ELL)))));
            }
            else {
              concat_assign(v_out, toString(v_contextNode.o_get("textContent", 0x716294CDF72E1E6ELL)));
            }
          }
          else if (equal(v_contextNode.o_get("nodeName", 0x009B66FACA616B3CLL), "ignore")) {
            if ((!(t___isset("parent")) && toBoolean(m_parser.o_get("ot", 0x48663D83DD673D4DLL).rvalAt("wiki", 0x24648D2BCD794D21LL))) || (toBoolean(bitwise_and(v_flags, throw_fatal("unknown class constant ppframe_dom::NO_IGNORE"))))) {
              concat_assign(v_out, toString(v_contextNode.o_get("textContent", 0x716294CDF72E1E6ELL)));
            }
            else {
              concat_assign(v_out, "");
            }
          }
          else if (equal(v_contextNode.o_get("nodeName", 0x009B66FACA616B3CLL), "ext")) {
            ((Object)((v_xpath = ((Object)(LINE(1018,p_domxpath(p_domxpath(NEWOBJ(c_domxpath)())->create(v_contextNode.o_get("ownerDocument", 0x0222D017809853C8LL)))))))));
            (v_names = LINE(1019,v_xpath->t_query("name", toObject(v_contextNode))));
            (v_attrs = LINE(1020,v_xpath->t_query("attr", toObject(v_contextNode))));
            (v_inners = LINE(1021,v_xpath->t_query("inner", toObject(v_contextNode))));
            (v_closes = LINE(1022,v_xpath->t_query("close", toObject(v_contextNode))));
            (v_params = (assignCallTemp(eo_0, ((Object)(LINE(1024,p_ppnode_dom(p_ppnode_dom(NEWOBJ(c_ppnode_dom)())->create(v_names.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL))))))),assignCallTemp(eo_1, more(v_attrs.o_get("length", 0x2993E8EB119CAB21LL), 0LL) ? ((Variant)(((Object)(LINE(1025,p_ppnode_dom(p_ppnode_dom(NEWOBJ(c_ppnode_dom)())->create(v_attrs.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)))))))) : ((Variant)(null))),assignCallTemp(eo_2, more(v_inners.o_get("length", 0x2993E8EB119CAB21LL), 0LL) ? ((Variant)(((Object)(LINE(1026,p_ppnode_dom(p_ppnode_dom(NEWOBJ(c_ppnode_dom)())->create(v_inners.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)))))))) : ((Variant)(null))),assignCallTemp(eo_3, more(v_closes.o_get("length", 0x2993E8EB119CAB21LL), 0LL) ? ((Variant)(((Object)(LINE(1027,p_ppnode_dom(p_ppnode_dom(NEWOBJ(c_ppnode_dom)())->create(v_closes.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)))))))) : ((Variant)(null))),Array(ArrayInit(4).set(0, "name", eo_0, 0x0BCDB293DC3CBDDCLL).set(1, "attr", eo_1, 0x64311A2C8443755DLL).set(2, "inner", eo_2, 0x4F20D07BB803C1FELL).set(3, "close", eo_3, 0x36B43082F052EC6BLL).create())));
            concat_assign(v_out, toString(LINE(1029,m_parser.o_invoke_few_args("extensionSubstitution", 0x7D371E851A4798E9LL, 2, v_params, this))));
          }
          else if (equal(v_contextNode.o_get("nodeName", 0x009B66FACA616B3CLL), "h")) {
            (v_s = LINE(1032,o_root_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_contextNode.o_lval("childNodes", 0x795010C5D07D8B8ELL), v_flags)));
            if (equal(v_contextNode.o_get("parentNode", 0x59F3B1B79CC95A9DLL).o_get("nodeName", 0x009B66FACA616B3CLL), "root") && toBoolean(m_parser.o_get("ot", 0x48663D83DD673D4DLL).rvalAt("html", 0x5CA91C812230855DLL))) {
              (v_headingIndex = LINE(1040,v_contextNode.o_invoke_few_args("getAttribute", 0x49F89C466612FC28LL, 1, "i")));
              (v_titleText = LINE(1041,m_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)));
              lval(m_parser.o_lval("mHeadings", 0x5EB895444FBC1E6BLL)).append((Array(ArrayInit(2).set(0, v_titleText).set(1, v_headingIndex).create())));
              (v_serial = LINE(1043,x_count(m_parser.o_get("mHeadings", 0x5EB895444FBC1E6BLL))) - 1LL);
              (v_marker = LINE(1044,concat4(toString(m_parser.o_get("mUniqPrefix", 0x22D00FA247E8311DLL)), "-h-", toString(v_serial), "--QINU\177")));
              (v_count = LINE(1045,v_contextNode.o_invoke_few_args("getAttribute", 0x49F89C466612FC28LL, 1, "level")));
              (v_s = concat_rev(toString(LINE(1046,x_substr(toString(v_s), toInt32(v_count)))), concat(toString(x_substr(toString(v_s), toInt32(0LL), toInt32(v_count))), toString(v_marker))));
              LINE(1047,m_parser.o_get("mStripState", 0x7207EA9A5BBFBF69LL).o_get("general", 0x661B15A9C00F7127LL).o_invoke_few_args("setPair", 0x1CA93325C3BDD490LL, 2, v_marker, ""));
            }
            concat_assign(v_out, toString(v_s));
          }
          else {
            (v_newIterator = v_contextNode.o_get("childNodes", 0x795010C5D07D8B8ELL));
          }
        }
        else {
          LINE(1055,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "PPFrame_DOM::expand").create()), 0x00000000B599F276LL));
          throw_exception(LINE(1056,create_object("mwexception", Array(ArrayInit(1).set(0, "PPFrame_DOM::expand: Invalid parameter type").create()))));
        }
        if (!same(v_newIterator, false)) {
          if (instanceOf(v_newIterator, "PPNode_DOM")) {
            (v_newIterator = v_newIterator.o_get("node", 0x75AF6DCF22783AC1LL));
          }
          v_outStack.append((""));
          v_iteratorStack.append((v_newIterator));
          v_indexStack.append((0LL));
        }
        else if (same(v_iteratorStack.rvalAt(v_level), false)) {
          LOOP_COUNTER(23);
          {
            while (same(v_iteratorStack.rvalAt(v_level), false) && more(v_level, 0LL)) {
              LOOP_COUNTER_CHECK(23);
              {
                concat_assign(lval(v_outStack.lvalAt(v_level - 1LL)), toString(v_out));
                LINE(1071,x_array_pop(ref(v_outStack)));
                LINE(1072,x_array_pop(ref(v_iteratorStack)));
                LINE(1073,x_array_pop(ref(v_indexStack)));
                v_level--;
              }
            }
          }
        }
      }
    }
  }
  --sv_expansionDepth;
  LINE(1079,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "PPFrame_DOM::expand").create()), 0x00000000B599F276LL));
  return v_outStack.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: Preprocessor_DOM.php line 1083 */
Variant c_ppframe_dom::t_implodewithflags(int num_args, CVarRef v_sep, Variant v_flags, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::implodeWithFlags);
  Variant eo_0;
  Variant eo_1;
  Variant v_args;
  bool v_first = false;
  Variant v_s;
  Variant v_root;
  Variant v_node;

  (v_args = LINE(1084,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(2).set(0, v_sep).set(1, v_flags).create()),args)),x_array_slice(eo_0, toInt32(2LL)))));
  (v_first = true);
  (v_s = "");
  {
    LOOP_COUNTER(24);
    for (ArrayIterPtr iter26 = v_args.begin("ppframe_dom"); !iter26->end(); iter26->next()) {
      LOOP_COUNTER_CHECK(24);
      v_root = iter26->second();
      {
        if (instanceOf(v_root, "PPNode_DOM")) (v_root = v_root.o_get("node", 0x75AF6DCF22783AC1LL));
        if (!(LINE(1090,x_is_array(v_root))) && !((instanceOf(v_root, "DOMNodeList")))) {
          (v_root = Array(ArrayInit(1).set(0, v_root).create()));
        }
        {
          LOOP_COUNTER(27);
          for (ArrayIterPtr iter29 = v_root.begin("ppframe_dom"); !iter29->end(); iter29->next()) {
            LOOP_COUNTER_CHECK(27);
            v_node = iter29->second();
            {
              if (v_first) {
                (v_first = false);
              }
              else {
                concat_assign(v_s, toString(v_sep));
              }
              concat_assign(v_s, toString(LINE(1099,o_root_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_node, v_flags))));
            }
          }
        }
      }
    }
  }
  return v_s;
} /* function */
/* SRC: Preprocessor_DOM.php line 1109 */
Variant c_ppframe_dom::t_implode(int num_args, CVarRef v_sep, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::implode);
  Variant eo_0;
  Variant eo_1;
  Variant v_args;
  bool v_first = false;
  Variant v_s;
  Variant v_root;
  Variant v_node;

  (v_args = LINE(1110,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(1).set(0, v_sep).create()),args)),x_array_slice(eo_0, toInt32(1LL)))));
  (v_first = true);
  (v_s = "");
  {
    LOOP_COUNTER(30);
    for (ArrayIterPtr iter32 = v_args.begin("ppframe_dom"); !iter32->end(); iter32->next()) {
      LOOP_COUNTER_CHECK(30);
      v_root = iter32->second();
      {
        if (instanceOf(v_root, "PPNode_DOM")) (v_root = v_root.o_get("node", 0x75AF6DCF22783AC1LL));
        if (!(LINE(1116,x_is_array(v_root))) && !((instanceOf(v_root, "DOMNodeList")))) {
          (v_root = Array(ArrayInit(1).set(0, v_root).create()));
        }
        {
          LOOP_COUNTER(33);
          for (ArrayIterPtr iter35 = v_root.begin("ppframe_dom"); !iter35->end(); iter35->next()) {
            LOOP_COUNTER_CHECK(33);
            v_node = iter35->second();
            {
              if (v_first) {
                (v_first = false);
              }
              else {
                concat_assign(v_s, toString(v_sep));
              }
              concat_assign(v_s, toString(LINE(1125,o_root_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_node))));
            }
          }
        }
      }
    }
  }
  return v_s;
} /* function */
/* SRC: Preprocessor_DOM.php line 1135 */
Array c_ppframe_dom::t_virtualimplode(int num_args, CVarRef v_sep, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::virtualImplode);
  Variant eo_0;
  Variant eo_1;
  Variant v_args;
  Array v_out;
  bool v_first = false;
  Variant v_root;
  Variant v_node;

  (v_args = LINE(1136,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(1).set(0, v_sep).create()),args)),x_array_slice(eo_0, toInt32(1LL)))));
  (v_out = ScalarArrays::sa_[0]);
  (v_first = true);
  if (instanceOf(v_root, "PPNode_DOM")) (v_root = v_root.o_get("node", 0x75AF6DCF22783AC1LL));
  {
    LOOP_COUNTER(36);
    for (ArrayIterPtr iter38 = v_args.begin("ppframe_dom"); !iter38->end(); iter38->next()) {
      LOOP_COUNTER_CHECK(36);
      v_root = iter38->second();
      {
        if (!(LINE(1142,x_is_array(v_root))) && !((instanceOf(v_root, "DOMNodeList")))) {
          (v_root = Array(ArrayInit(1).set(0, v_root).create()));
        }
        {
          LOOP_COUNTER(39);
          for (ArrayIterPtr iter41 = v_root.begin("ppframe_dom"); !iter41->end(); iter41->next()) {
            LOOP_COUNTER_CHECK(39);
            v_node = iter41->second();
            {
              if (v_first) {
                (v_first = false);
              }
              else {
                v_out.append((v_sep));
              }
              v_out.append((v_node));
            }
          }
        }
      }
    }
  }
  return v_out;
} /* function */
/* SRC: Preprocessor_DOM.php line 1160 */
Array c_ppframe_dom::t_virtualbracketedimplode(int num_args, CVarRef v_start, CVarRef v_sep, CVarRef v_end, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::virtualBracketedImplode);
  Variant eo_0;
  Variant eo_1;
  Variant v_args;
  Array v_out;
  bool v_first = false;
  Variant v_root;
  Variant v_node;

  (v_args = LINE(1161,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(3).set(0, v_start).set(1, v_sep).set(2, v_end).create()),args)),x_array_slice(eo_0, toInt32(3LL)))));
  (v_out = Array(ArrayInit(1).set(0, v_start).create()));
  (v_first = true);
  {
    LOOP_COUNTER(42);
    for (ArrayIterPtr iter44 = v_args.begin("ppframe_dom"); !iter44->end(); iter44->next()) {
      LOOP_COUNTER_CHECK(42);
      v_root = iter44->second();
      {
        if (instanceOf(v_root, "PPNode_DOM")) (v_root = v_root.o_get("node", 0x75AF6DCF22783AC1LL));
        if (!(LINE(1167,x_is_array(v_root))) && !((instanceOf(v_root, "DOMNodeList")))) {
          (v_root = Array(ArrayInit(1).set(0, v_root).create()));
        }
        {
          LOOP_COUNTER(45);
          for (ArrayIterPtr iter47 = v_root.begin("ppframe_dom"); !iter47->end(); iter47->next()) {
            LOOP_COUNTER_CHECK(45);
            v_node = iter47->second();
            {
              if (v_first) {
                (v_first = false);
              }
              else {
                v_out.append((v_sep));
              }
              v_out.append((v_node));
            }
          }
        }
      }
    }
  }
  v_out.append((v_end));
  return v_out;
} /* function */
/* SRC: Preprocessor_DOM.php line 1183 */
String c_ppframe_dom::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::__toString);
  return "frame{}";
} /* function */
/* SRC: Preprocessor_DOM.php line 1187 */
Variant c_ppframe_dom::t_getpdbk(bool v_level //  = false
) {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::getPDBK);
  if (same(v_level, false)) {
    return LINE(1189,m_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0));
  }
  else {
    return isset(m_titleCache, v_level) ? ((Variant)(m_titleCache.rvalAt(v_level))) : ((Variant)(false));
  }
  return null;
} /* function */
/* SRC: Preprocessor_DOM.php line 1195 */
Array c_ppframe_dom::t_getarguments() {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::getArguments);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: Preprocessor_DOM.php line 1199 */
Array c_ppframe_dom::t_getnumberedarguments() {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::getNumberedArguments);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: Preprocessor_DOM.php line 1203 */
Array c_ppframe_dom::t_getnamedarguments() {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::getNamedArguments);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: Preprocessor_DOM.php line 1210 */
bool c_ppframe_dom::t_isempty() {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::isEmpty);
  return true;
} /* function */
/* SRC: Preprocessor_DOM.php line 1214 */
bool c_ppframe_dom::t_getargument(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::getArgument);
  return false;
} /* function */
/* SRC: Preprocessor_DOM.php line 1221 */
bool c_ppframe_dom::t_loopcheck(CVarRef v_title) {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::loopCheck);
  return !(isset(m_loopCheckHash, LINE(1222,toObject(v_title)->o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0))));
} /* function */
/* SRC: Preprocessor_DOM.php line 1228 */
bool c_ppframe_dom::t_istemplate() {
  INSTANCE_METHOD_INJECTION(PPFrame_DOM, PPFrame_DOM::isTemplate);
  return false;
} /* function */
/* SRC: Preprocessor_DOM.php line 1350 */
Variant c_ppcustomframe_dom::os_get(const char *s, int64 hash) {
  return c_ppframe_dom::os_get(s, hash);
}
Variant &c_ppcustomframe_dom::os_lval(const char *s, int64 hash) {
  return c_ppframe_dom::os_lval(s, hash);
}
void c_ppcustomframe_dom::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("args", m_args.isReferenced() ? ref(m_args) : m_args));
  c_ppframe_dom::o_get(props);
}
bool c_ppcustomframe_dom::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4AF7CD17F976719ELL, args, 4);
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_exists(s, hash);
}
Variant c_ppcustomframe_dom::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4AF7CD17F976719ELL, m_args,
                         args, 4);
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_get(s, hash);
}
Variant c_ppcustomframe_dom::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4AF7CD17F976719ELL, m_args,
                      args, 4);
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_set(s, hash, v, forInit);
}
Variant &c_ppcustomframe_dom::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4AF7CD17F976719ELL, m_args,
                         args, 4);
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_lval(s, hash);
}
Variant c_ppcustomframe_dom::os_constant(const char *s) {
  return c_ppframe_dom::os_constant(s);
}
IMPLEMENT_CLASS(ppcustomframe_dom)
ObjectData *c_ppcustomframe_dom::create(Variant v_preprocessor, Variant v_args) {
  init();
  t___construct(v_preprocessor, v_args);
  return this;
}
ObjectData *c_ppcustomframe_dom::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_ppcustomframe_dom::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_ppcustomframe_dom::cloneImpl() {
  c_ppcustomframe_dom *obj = NEW(c_ppcustomframe_dom)();
  cloneSet(obj);
  return obj;
}
void c_ppcustomframe_dom::cloneSet(c_ppcustomframe_dom *clone) {
  clone->m_args = m_args.isReferenced() ? ref(m_args) : m_args;
  c_ppframe_dom::cloneSet(clone);
}
Variant c_ppcustomframe_dom::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        int count = params.size();
        if (count <= 3) return (t_virtualbracketedimplode(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        return (t_virtualbracketedimplode(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        int count = params.size();
        if (count <= 1) return (t_expand(params.rvalAt(0)));
        return (t_expand(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        int count = params.size();
        if (count <= 2) return (t_implodewithflags(count, params.rvalAt(0), params.rvalAt(1)));
        return (t_implodewithflags(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        int count = params.size();
        if (count <= 1) return (t_virtualimplode(count, params.rvalAt(0)));
        return (t_virtualimplode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        int count = params.size();
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(params.rvalAt(0)));
        return (t_newchild(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        int count = params.size();
        if (count <= 1) return (t_implode(count, params.rvalAt(0)));
        return (t_implode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(params.rvalAt(0)));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_invoke(s, params, hash, fatal);
}
Variant c_ppcustomframe_dom::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        if (count <= 3) return (t_virtualbracketedimplode(count, a0, a1, a2));
        Array params;
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualbracketedimplode(count,a0, a1, a2, params));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        if (count <= 1) return (t_expand(a0));
        return (t_expand(a0, a1));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        if (count <= 2) return (t_implodewithflags(count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implodewithflags(count,a0, a1, params));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        if (count <= 1) return (t_virtualimplode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualimplode(count,a0, params));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(a0));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(a0));
        return (t_newchild(a0, a1));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        if (count <= 1) return (t_implode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implode(count,a0, params));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(a0));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppcustomframe_dom::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ppframe_dom::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppcustomframe_dom$os_get(const char *s) {
  return c_ppcustomframe_dom::os_get(s, -1);
}
Variant &cw_ppcustomframe_dom$os_lval(const char *s) {
  return c_ppcustomframe_dom::os_lval(s, -1);
}
Variant cw_ppcustomframe_dom$os_constant(const char *s) {
  return c_ppcustomframe_dom::os_constant(s);
}
Variant cw_ppcustomframe_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppcustomframe_dom::os_invoke(c, s, params, -1, fatal);
}
void c_ppcustomframe_dom::init() {
  c_ppframe_dom::init();
  m_args = null;
}
/* SRC: Preprocessor_DOM.php line 1353 */
void c_ppcustomframe_dom::t___construct(Variant v_preprocessor, Variant v_args) {
  INSTANCE_METHOD_INJECTION(PPCustomFrame_DOM, PPCustomFrame_DOM::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(1354,c_ppframe_dom::t___construct(v_preprocessor));
  (m_args = v_args);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_DOM.php line 1358 */
String c_ppcustomframe_dom::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPCustomFrame_DOM, PPCustomFrame_DOM::__toString);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  String v_s;
  bool v_first = false;
  Primitive v_name = 0;
  Variant v_value;

  (v_s = "cstmframe{");
  (v_first = true);
  {
    LOOP_COUNTER(48);
    Variant map49 = m_args;
    for (ArrayIterPtr iter50 = map49.begin("ppcustomframe_dom"); !iter50->end(); iter50->next()) {
      LOOP_COUNTER_CHECK(48);
      v_value = iter50->second();
      v_name = iter50->first();
      {
        if (v_first) {
          (v_first = false);
        }
        else {
          concat_assign(v_s, ", ");
        }
        concat_assign(v_s, LINE(1368,(assignCallTemp(eo_1, toString(v_name)),assignCallTemp(eo_3, toString((assignCallTemp(eo_7, v_value.o_invoke_few_args("__toString", 0x642C2D2994B34A13LL, 0)),x_str_replace("\"", "\\\"", eo_7)))),concat5("\"", eo_1, "\":\"", eo_3, "\""))));
      }
    }
  }
  concat_assign(v_s, "}");
  return v_s;
} /* function */
/* SRC: Preprocessor_DOM.php line 1374 */
bool c_ppcustomframe_dom::t_isempty() {
  INSTANCE_METHOD_INJECTION(PPCustomFrame_DOM, PPCustomFrame_DOM::isEmpty);
  return !(toBoolean(LINE(1375,x_count(m_args))));
} /* function */
/* SRC: Preprocessor_DOM.php line 1378 */
Variant c_ppcustomframe_dom::t_getargument(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(PPCustomFrame_DOM, PPCustomFrame_DOM::getArgument);
  if (!(isset(m_args, v_index))) {
    return false;
  }
  return m_args.rvalAt(v_index);
} /* function */
/* SRC: Preprocessor_DOM.php line 1237 */
Variant c_pptemplateframe_dom::os_get(const char *s, int64 hash) {
  return c_ppframe_dom::os_get(s, hash);
}
Variant &c_pptemplateframe_dom::os_lval(const char *s, int64 hash) {
  return c_ppframe_dom::os_lval(s, hash);
}
void c_pptemplateframe_dom::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("numberedArgs", m_numberedArgs.isReferenced() ? ref(m_numberedArgs) : m_numberedArgs));
  props.push_back(NEW(ArrayElement)("namedArgs", m_namedArgs.isReferenced() ? ref(m_namedArgs) : m_namedArgs));
  props.push_back(NEW(ArrayElement)("parent", m_parent.isReferenced() ? ref(m_parent) : m_parent));
  props.push_back(NEW(ArrayElement)("numberedExpansionCache", m_numberedExpansionCache.isReferenced() ? ref(m_numberedExpansionCache) : m_numberedExpansionCache));
  props.push_back(NEW(ArrayElement)("namedExpansionCache", m_namedExpansionCache.isReferenced() ? ref(m_namedExpansionCache) : m_namedExpansionCache));
  c_ppframe_dom::o_get(props);
}
bool c_pptemplateframe_dom::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 5:
      HASH_EXISTS_STRING(0x21A0BD85B8757635LL, numberedExpansionCache, 22);
      break;
    case 8:
      HASH_EXISTS_STRING(0x4C84B2F1BC8BA768LL, namedExpansionCache, 19);
      break;
    case 9:
      HASH_EXISTS_STRING(0x00263ABC07EFFD29LL, numberedArgs, 12);
      break;
    case 10:
      HASH_EXISTS_STRING(0x3F849A2980280C0ALL, namedArgs, 9);
      break;
    case 12:
      HASH_EXISTS_STRING(0x16E2F26FFB10FD8CLL, parent, 6);
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_exists(s, hash);
}
Variant c_pptemplateframe_dom::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 5:
      HASH_RETURN_STRING(0x21A0BD85B8757635LL, m_numberedExpansionCache,
                         numberedExpansionCache, 22);
      break;
    case 8:
      HASH_RETURN_STRING(0x4C84B2F1BC8BA768LL, m_namedExpansionCache,
                         namedExpansionCache, 19);
      break;
    case 9:
      HASH_RETURN_STRING(0x00263ABC07EFFD29LL, m_numberedArgs,
                         numberedArgs, 12);
      break;
    case 10:
      HASH_RETURN_STRING(0x3F849A2980280C0ALL, m_namedArgs,
                         namedArgs, 9);
      break;
    case 12:
      HASH_RETURN_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                         parent, 6);
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_get(s, hash);
}
Variant c_pptemplateframe_dom::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 5:
      HASH_SET_STRING(0x21A0BD85B8757635LL, m_numberedExpansionCache,
                      numberedExpansionCache, 22);
      break;
    case 8:
      HASH_SET_STRING(0x4C84B2F1BC8BA768LL, m_namedExpansionCache,
                      namedExpansionCache, 19);
      break;
    case 9:
      HASH_SET_STRING(0x00263ABC07EFFD29LL, m_numberedArgs,
                      numberedArgs, 12);
      break;
    case 10:
      HASH_SET_STRING(0x3F849A2980280C0ALL, m_namedArgs,
                      namedArgs, 9);
      break;
    case 12:
      HASH_SET_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                      parent, 6);
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_set(s, hash, v, forInit);
}
Variant &c_pptemplateframe_dom::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 5:
      HASH_RETURN_STRING(0x21A0BD85B8757635LL, m_numberedExpansionCache,
                         numberedExpansionCache, 22);
      break;
    case 8:
      HASH_RETURN_STRING(0x4C84B2F1BC8BA768LL, m_namedExpansionCache,
                         namedExpansionCache, 19);
      break;
    case 9:
      HASH_RETURN_STRING(0x00263ABC07EFFD29LL, m_numberedArgs,
                         numberedArgs, 12);
      break;
    case 10:
      HASH_RETURN_STRING(0x3F849A2980280C0ALL, m_namedArgs,
                         namedArgs, 9);
      break;
    case 12:
      HASH_RETURN_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                         parent, 6);
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_lval(s, hash);
}
Variant c_pptemplateframe_dom::os_constant(const char *s) {
  return c_ppframe_dom::os_constant(s);
}
IMPLEMENT_CLASS(pptemplateframe_dom)
ObjectData *c_pptemplateframe_dom::create(Variant v_preprocessor, Variant v_parent //  = false
, Variant v_numberedArgs //  = ScalarArrays::sa_[0]
, Variant v_namedArgs //  = ScalarArrays::sa_[0]
, Variant v_title //  = false
) {
  init();
  t___construct(v_preprocessor, v_parent, v_numberedArgs, v_namedArgs, v_title);
  return this;
}
ObjectData *c_pptemplateframe_dom::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    if (count == 3) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    if (count == 4) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
  } else return this;
}
void c_pptemplateframe_dom::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  if (count == 3) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  if (count == 4) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
}
ObjectData *c_pptemplateframe_dom::cloneImpl() {
  c_pptemplateframe_dom *obj = NEW(c_pptemplateframe_dom)();
  cloneSet(obj);
  return obj;
}
void c_pptemplateframe_dom::cloneSet(c_pptemplateframe_dom *clone) {
  clone->m_numberedArgs = m_numberedArgs.isReferenced() ? ref(m_numberedArgs) : m_numberedArgs;
  clone->m_namedArgs = m_namedArgs.isReferenced() ? ref(m_namedArgs) : m_namedArgs;
  clone->m_parent = m_parent.isReferenced() ? ref(m_parent) : m_parent;
  clone->m_numberedExpansionCache = m_numberedExpansionCache.isReferenced() ? ref(m_numberedExpansionCache) : m_numberedExpansionCache;
  clone->m_namedExpansionCache = m_namedExpansionCache.isReferenced() ? ref(m_namedExpansionCache) : m_namedExpansionCache;
  c_ppframe_dom::cloneSet(clone);
}
Variant c_pptemplateframe_dom::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        int count = params.size();
        if (count <= 3) return (t_virtualbracketedimplode(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        return (t_virtualbracketedimplode(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        int count = params.size();
        if (count <= 1) return (t_expand(params.rvalAt(0)));
        return (t_expand(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        int count = params.size();
        if (count <= 2) return (t_implodewithflags(count, params.rvalAt(0), params.rvalAt(1)));
        return (t_implodewithflags(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        int count = params.size();
        if (count <= 1) return (t_virtualimplode(count, params.rvalAt(0)));
        return (t_virtualimplode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        int count = params.size();
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(params.rvalAt(0)));
        return (t_newchild(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        int count = params.size();
        if (count <= 1) return (t_implode(count, params.rvalAt(0)));
        return (t_implode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(params.rvalAt(0)));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        if (count == 3) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
        if (count == 4) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
      }
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_invoke(s, params, hash, fatal);
}
Variant c_pptemplateframe_dom::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        if (count <= 3) return (t_virtualbracketedimplode(count, a0, a1, a2));
        Array params;
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualbracketedimplode(count,a0, a1, a2, params));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        if (count <= 1) return (t_expand(a0));
        return (t_expand(a0, a1));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        if (count <= 2) return (t_implodewithflags(count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implodewithflags(count,a0, a1, params));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        if (count <= 1) return (t_virtualimplode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualimplode(count,a0, params));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(a0));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(a0));
        return (t_newchild(a0, a1));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        if (count <= 1) return (t_implode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implode(count,a0, params));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(a0));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        if (count == 3) return (t___construct(a0, a1, a2), null);
        if (count == 4) return (t___construct(a0, a1, a2, a3), null);
        return (t___construct(a0, a1, a2, a3, a4), null);
      }
      break;
    default:
      break;
  }
  return c_ppframe_dom::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_pptemplateframe_dom::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ppframe_dom::os_invoke(c, s, params, hash, fatal);
}
Variant cw_pptemplateframe_dom$os_get(const char *s) {
  return c_pptemplateframe_dom::os_get(s, -1);
}
Variant &cw_pptemplateframe_dom$os_lval(const char *s) {
  return c_pptemplateframe_dom::os_lval(s, -1);
}
Variant cw_pptemplateframe_dom$os_constant(const char *s) {
  return c_pptemplateframe_dom::os_constant(s);
}
Variant cw_pptemplateframe_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_pptemplateframe_dom::os_invoke(c, s, params, -1, fatal);
}
void c_pptemplateframe_dom::init() {
  c_ppframe_dom::init();
  m_numberedArgs = null;
  m_namedArgs = null;
  m_parent = null;
  m_numberedExpansionCache = null;
  m_namedExpansionCache = null;
}
/* SRC: Preprocessor_DOM.php line 1241 */
void c_pptemplateframe_dom::t___construct(Variant v_preprocessor, Variant v_parent //  = false
, Variant v_numberedArgs //  = ScalarArrays::sa_[0]
, Variant v_namedArgs //  = ScalarArrays::sa_[0]
, Variant v_title //  = false
) {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_DOM, PPTemplateFrame_DOM::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant v_pdbk;

  LINE(1242,c_ppframe_dom::t___construct(v_preprocessor));
  (m_parent = v_parent);
  (m_numberedArgs = v_numberedArgs);
  (m_namedArgs = v_namedArgs);
  (m_title = v_title);
  (v_pdbk = toBoolean(v_title) ? ((Variant)(LINE(1247,v_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)))) : ((Variant)(false)));
  (m_titleCache = v_parent.o_get("titleCache", 0x0CD4D2916BE2CE13LL));
  m_titleCache.append((v_pdbk));
  (m_loopCheckHash = v_parent.o_get("loopCheckHash", 0x757439BA20184735LL));
  if (!same(v_pdbk, false)) {
    m_loopCheckHash.set(v_pdbk, (true));
  }
  (m_depth = v_parent.o_get("depth", 0x1AE700EECE6274C7LL) + 1LL);
  (m_numberedExpansionCache = (m_namedExpansionCache = ScalarArrays::sa_[0]));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_DOM.php line 1258 */
String c_pptemplateframe_dom::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_DOM, PPTemplateFrame_DOM::__toString);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  String v_s;
  bool v_first = false;
  Variant v_args;
  Primitive v_name = 0;
  Variant v_value;

  (v_s = "tplframe{");
  (v_first = true);
  (v_args = m_numberedArgs + m_namedArgs);
  {
    LOOP_COUNTER(51);
    for (ArrayIterPtr iter53 = v_args.begin("pptemplateframe_dom"); !iter53->end(); iter53->next()) {
      LOOP_COUNTER_CHECK(51);
      v_value = iter53->second();
      v_name = iter53->first();
      {
        if (v_first) {
          (v_first = false);
        }
        else {
          concat_assign(v_s, ", ");
        }
        concat_assign(v_s, LINE(1269,(assignCallTemp(eo_1, toString(v_name)),assignCallTemp(eo_3, toString((assignCallTemp(eo_7, v_value.o_get("ownerDocument", 0x0222D017809853C8LL).o_invoke_few_args("saveXML", 0x26D66F56DDDC32E5LL, 1, v_value)),x_str_replace("\"", "\\\"", eo_7)))),concat5("\"", eo_1, "\":\"", eo_3, "\""))));
      }
    }
  }
  concat_assign(v_s, "}");
  return v_s;
} /* function */
/* SRC: Preprocessor_DOM.php line 1277 */
bool c_pptemplateframe_dom::t_isempty() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_DOM, PPTemplateFrame_DOM::isEmpty);
  return !(toBoolean(LINE(1278,x_count(m_numberedArgs)))) && !(toBoolean(x_count(m_namedArgs)));
} /* function */
/* SRC: Preprocessor_DOM.php line 1281 */
Variant c_pptemplateframe_dom::t_getarguments() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_DOM, PPTemplateFrame_DOM::getArguments);
  Variant eo_0;
  Variant eo_1;
  Variant v_arguments;
  Variant v_key;

  (v_arguments = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(54);
    Variant map55 = LINE(1285,(assignCallTemp(eo_0, LINE(1284,x_array_keys(m_numberedArgs))),assignCallTemp(eo_1, LINE(1285,x_array_keys(m_namedArgs))),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create()))));
    for (ArrayIterPtr iter56 = map55.begin("pptemplateframe_dom"); !iter56->end(); iter56->next()) {
      LOOP_COUNTER_CHECK(54);
      v_key = iter56->second();
      {
        v_arguments.set(v_key, (LINE(1286,o_root_invoke_few_args("getArgument", 0x0A95A4780D41F7B8LL, 1, v_key))));
      }
    }
  }
  return v_arguments;
} /* function */
/* SRC: Preprocessor_DOM.php line 1291 */
Variant c_pptemplateframe_dom::t_getnumberedarguments() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_DOM, PPTemplateFrame_DOM::getNumberedArguments);
  Variant v_arguments;
  Variant v_key;

  (v_arguments = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(57);
    Variant map58 = LINE(1293,x_array_keys(m_numberedArgs));
    for (ArrayIterPtr iter59 = map58.begin("pptemplateframe_dom"); !iter59->end(); iter59->next()) {
      LOOP_COUNTER_CHECK(57);
      v_key = iter59->second();
      {
        v_arguments.set(v_key, (LINE(1294,o_root_invoke_few_args("getArgument", 0x0A95A4780D41F7B8LL, 1, v_key))));
      }
    }
  }
  return v_arguments;
} /* function */
/* SRC: Preprocessor_DOM.php line 1299 */
Variant c_pptemplateframe_dom::t_getnamedarguments() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_DOM, PPTemplateFrame_DOM::getNamedArguments);
  Variant v_arguments;
  Variant v_key;

  (v_arguments = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(60);
    Variant map61 = LINE(1301,x_array_keys(m_namedArgs));
    for (ArrayIterPtr iter62 = map61.begin("pptemplateframe_dom"); !iter62->end(); iter62->next()) {
      LOOP_COUNTER_CHECK(60);
      v_key = iter62->second();
      {
        v_arguments.set(v_key, (LINE(1302,o_root_invoke_few_args("getArgument", 0x0A95A4780D41F7B8LL, 1, v_key))));
      }
    }
  }
  return v_arguments;
} /* function */
/* SRC: Preprocessor_DOM.php line 1307 */
Variant c_pptemplateframe_dom::t_getnumberedargument(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_DOM, PPTemplateFrame_DOM::getNumberedArgument);
  if (!(isset(m_numberedArgs, v_index))) {
    return false;
  }
  if (!(isset(m_numberedExpansionCache, v_index))) {
    m_numberedExpansionCache.set(v_index, (LINE(1313,m_parent.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, m_numberedArgs.refvalAt(v_index), throw_fatal("unknown class constant pptemplateframe_dom::STRIP_COMMENTS")))));
  }
  return m_numberedExpansionCache.rvalAt(v_index);
} /* function */
/* SRC: Preprocessor_DOM.php line 1318 */
Variant c_pptemplateframe_dom::t_getnamedargument(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_DOM, PPTemplateFrame_DOM::getNamedArgument);
  if (!(isset(m_namedArgs, v_name))) {
    return false;
  }
  if (!(isset(m_namedExpansionCache, v_name))) {
    m_namedExpansionCache.set(v_name, (LINE(1325,x_trim(toString(m_parent.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, m_namedArgs.refvalAt(v_name), throw_fatal("unknown class constant pptemplateframe_dom::STRIP_COMMENTS")))))));
  }
  return m_namedExpansionCache.rvalAt(v_name);
} /* function */
/* SRC: Preprocessor_DOM.php line 1330 */
Variant c_pptemplateframe_dom::t_getargument(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_DOM, PPTemplateFrame_DOM::getArgument);
  Variant v_text;

  (v_text = LINE(1331,t_getnumberedargument(v_name)));
  if (same(v_text, false)) {
    (v_text = LINE(1333,t_getnamedargument(v_name)));
  }
  return v_text;
} /* function */
/* SRC: Preprocessor_DOM.php line 1341 */
bool c_pptemplateframe_dom::t_istemplate() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_DOM, PPTemplateFrame_DOM::isTemplate);
  return true;
} /* function */
Object co_ppdpart(CArrRef params, bool init /* = true */) {
  return Object(p_ppdpart(NEW(c_ppdpart)())->dynCreate(params, init));
}
Object co_ppdstackelement(CArrRef params, bool init /* = true */) {
  return Object(p_ppdstackelement(NEW(c_ppdstackelement)())->dynCreate(params, init));
}
Object co_ppnode_dom(CArrRef params, bool init /* = true */) {
  return Object(p_ppnode_dom(NEW(c_ppnode_dom)())->dynCreate(params, init));
}
Object co_ppdstack(CArrRef params, bool init /* = true */) {
  return Object(p_ppdstack(NEW(c_ppdstack)())->dynCreate(params, init));
}
Object co_preprocessor_dom(CArrRef params, bool init /* = true */) {
  return Object(p_preprocessor_dom(NEW(c_preprocessor_dom)())->dynCreate(params, init));
}
Object co_ppframe_dom(CArrRef params, bool init /* = true */) {
  return Object(p_ppframe_dom(NEW(c_ppframe_dom)())->dynCreate(params, init));
}
Object co_ppcustomframe_dom(CArrRef params, bool init /* = true */) {
  return Object(p_ppcustomframe_dom(NEW(c_ppcustomframe_dom)())->dynCreate(params, init));
}
Object co_pptemplateframe_dom(CArrRef params, bool init /* = true */) {
  return Object(p_pptemplateframe_dom(NEW(c_pptemplateframe_dom)())->dynCreate(params, init));
}
Variant pm_php$Preprocessor_DOM_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::Preprocessor_DOM.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$Preprocessor_DOM_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
