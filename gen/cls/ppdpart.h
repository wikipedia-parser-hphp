
#ifndef __GENERATED_cls_ppdpart_h__
#define __GENERATED_cls_ppdpart_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_DOM.php line 789 */
class c_ppdpart : virtual public ObjectData {
  BEGIN_CLASS_MAP(ppdpart)
  END_CLASS_MAP(ppdpart)
  DECLARE_CLASS(ppdpart, PPDPart, ObjectData)
  void init();
  public: Variant m_out;
  public: void t___construct(Variant v_out = "");
  public: ObjectData *create(Variant v_out = "");
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppdpart_h__
