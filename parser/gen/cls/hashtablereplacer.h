
#ifndef __GENERATED_cls_hashtablereplacer_h__
#define __GENERATED_cls_hashtablereplacer_h__

#include <cls/replacer.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 40234 */
class c_hashtablereplacer : virtual public c_replacer {
  BEGIN_CLASS_MAP(hashtablereplacer)
    PARENT_CLASS(replacer)
  END_CLASS_MAP(hashtablereplacer)
  DECLARE_CLASS(hashtablereplacer, HashtableReplacer, replacer)
  void init();
  public: Variant m_table;
  public: Variant m_index;
  public: void t___construct(Variant v_table, Variant v_index = 0LL);
  public: ObjectData *create(Variant v_table, Variant v_index = 0LL);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_replace(CVarRef v_matches);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_hashtablereplacer_h__
