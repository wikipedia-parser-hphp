
#ifndef __GENERATED_cls_block_h__
#define __GENERATED_cls_block_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 6615 */
class c_block : virtual public ObjectData {
  BEGIN_CLASS_MAP(block)
  END_CLASS_MAP(block)
  DECLARE_CLASS(block, Block, ObjectData)
  void init();
  public: Variant m_mAddress;
  public: Variant m_mUser;
  public: Variant m_mBy;
  public: Variant m_mReason;
  public: Variant m_mTimestamp;
  public: Variant m_mAuto;
  public: Variant m_mId;
  public: Variant m_mExpiry;
  public: Variant m_mRangeStart;
  public: Variant m_mRangeEnd;
  public: Variant m_mAnonOnly;
  public: Variant m_mEnableAutoblock;
  public: Variant m_mHideName;
  public: Variant m_mBlockEmail;
  public: Variant m_mByName;
  public: Variant m_mAngryAutoblock;
  public: Variant m_mAllowUsertalk;
  public: Variant m_mNetworkBits;
  public: Variant m_mIntegerAddr;
  public: Variant m_mForUpdate;
  public: Variant m_mFromMaster;
  public: void t___construct(Variant v_address = "", Variant v_user = 0LL, Variant v_by = 0LL, Variant v_reason = "", Variant v_timestamp = "", Variant v_auto = 0LL, Variant v_expiry = "", Variant v_anonOnly = 0LL, Variant v_createAccount = 0LL, Variant v_enableAutoblock = 0LL, Variant v_hideName = 0LL, Variant v_blockEmail = 0LL, Variant v_allowUsertalk = 0LL);
  public: ObjectData *create(Variant v_address = "", Variant v_user = 0LL, Variant v_by = 0LL, Variant v_reason = "", Variant v_timestamp = "", Variant v_auto = 0LL, Variant v_expiry = "", Variant v_anonOnly = 0LL, Variant v_createAccount = 0LL, Variant v_enableAutoblock = 0LL, Variant v_hideName = 0LL, Variant v_blockEmail = 0LL, Variant v_allowUsertalk = 0LL);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static Variant ti_newfromdb(const char* cls, CVarRef v_address, CVarRef v_user = 0LL, bool v_killExpired = true);
  public: static Variant ti_newfromid(const char* cls, CVarRef v_id);
  public: bool t_equals(p_block v_block);
  public: void t_clear();
  public: Variant t_getdboptions(Variant v_options);
  public: bool t_load(CVarRef v_address = "", CVarRef v_user = 0LL, CVarRef v_killExpired = true);
  public: bool t_loadfromresult(CVarRef v_res, CVarRef v_killExpired = true);
  public: bool t_loadrange(CVarRef v_address, CVarRef v_killExpired = true, CVarRef v_user = 0LL);
  public: void t_initfromrow(CVarRef v_row);
  public: void t_initialiserange();
  public: bool t_delete();
  public: bool t_insert();
  public: Variant t_update();
  public: void t_validateblockparams();
  public: void t_doretroactiveautoblock();
  public: static bool ti_iswhitelistedfromautoblocks(const char* cls, CVarRef v_ip);
  public: Variant t_doautoblock(CVarRef v_autoblockIP, CVarRef v_justInserted = false);
  public: bool t_deleteifexpired();
  public: Variant t_isexpired();
  public: bool t_isvalid();
  public: void t_updatetimestamp();
  public: Variant t_getby();
  public: Variant t_getbyname();
  public: Variant t_forupdate(CVarRef v_x = null_variant);
  public: Variant t_frommaster(CVarRef v_x = null_variant);
  public: Variant t_getredactedname();
  public: static Variant ti_encodeexpiry(const char* cls, Variant v_expiry, CVarRef v_db);
  public: static Variant ti_decodeexpiry(const char* cls, CVarRef v_expiry, int64 v_timestampType = 1LL /* TS_MW */);
  public: static Variant ti_getautoblockexpiry(const char* cls, CVarRef v_timestamp);
  public: static String ti_normaliserange(const char* cls, String v_range);
  public: static void ti_purgeexpired(const char* cls);
  public: static String ti_infinity(const char* cls);
  public: static Variant ti_formatexpiry(const char* cls, CVarRef v_encoded_expiry);
  public: static Variant ti_parseexpiryinput(const char* cls, CVarRef v_expiry_input);
  public: static Variant t_newfromid(CVarRef v_id) { return ti_newfromid("block", v_id); }
  public: static Variant t_decodeexpiry(CVarRef v_expiry, int64 v_timestampType = 1LL /* TS_MW */) { return ti_decodeexpiry("block", v_expiry, v_timestampType); }
  public: static String t_normaliserange(CStrRef v_range) { return ti_normaliserange("block", v_range); }
  public: static Variant t_formatexpiry(CVarRef v_encoded_expiry) { return ti_formatexpiry("block", v_encoded_expiry); }
  public: static Variant t_getautoblockexpiry(CVarRef v_timestamp) { return ti_getautoblockexpiry("block", v_timestamp); }
  public: static Variant t_parseexpiryinput(CVarRef v_expiry_input) { return ti_parseexpiryinput("block", v_expiry_input); }
  public: static bool t_iswhitelistedfromautoblocks(CVarRef v_ip) { return ti_iswhitelistedfromautoblocks("block", v_ip); }
  public: static Variant t_encodeexpiry(CVarRef v_expiry, CVarRef v_db) { return ti_encodeexpiry("block", v_expiry, v_db); }
  public: static void t_purgeexpired() { ti_purgeexpired("block"); }
  public: static String t_infinity() { return ti_infinity("block"); }
  public: static Variant t_newfromdb(CVarRef v_address, CVarRef v_user = 0LL, bool v_killExpired = true) { return ti_newfromdb("block", v_address, v_user, v_killExpired); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_block_h__
