
#ifndef __GENERATED_cls_getdb_h__
#define __GENERATED_cls_getdb_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 18 */
class c_getdb : virtual public ObjectData {
  BEGIN_CLASS_MAP(getdb)
  END_CLASS_MAP(getdb)
  DECLARE_CLASS(getdb, GetDB, ObjectData)
  void init();
  public: p_getdb t_select();
  public: Array t_fetchobject();
  public: p_getdb t_freeresult();
  public: p_getdb t_current();
  public: p_getdb t_delete();
  public: p_getdb t_numrows();
  public: p_getdb t_update();
  public: String t_timestamp();
  public: p_getdb t_tablename();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_getdb_h__
