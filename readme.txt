
HipHop Compiler for PHP Usage:

	hphp <options> <inputs>

Options:
  --help                           display this message
  -t [ --target ] arg (=run)       lint | analyze | php | cpp | filecache | run
                                   (default)
  -f [ --format ] arg              lint: (none); 
                                   analyze: (none); 
                                   php: trimmed (default) | inlined | pickled |
                                   typeinfo | <any combination of them by any 
                                   separator>; 
                                   cpp: cluster (default) | file | sys | exe | 
                                   lib; 
                                   run: cluster (default) | file
  --cluster-count arg (=0)         Cluster by file sizes and output roughly 
                                   these many number of files. Use 0 for no 
                                   clustering.
  --input-dir arg                  input directory
  --program arg (=program)         final program name to use
  --args arg                       program arguments
  -i [ --inputs ] arg              input file names
  --input-list arg                 file containing list of file names, one per 
                                   line
  --include-path arg               a list of include paths to search for files 
                                   being included in includes or requires but 
                                   cannot be found assuming relative paths
  --module arg                     directories containing all input files
  --exclude-dir arg                directories to exclude from the input
  --fmodule arg                    same with module, except no exclusion 
                                   checking is performed, so these modules are 
                                   forced to be included
  --ffile arg                      extra PHP files forced to include without 
                                   exclusion checking
  --exclude-file arg               files to exclude from the input, even if 
                                   parse-on-demand finds it
  --exclude-pattern arg            regex (in 'find' command's regex command 
                                   line option format)  of files or directories
                                   to exclude from the input, even if 
                                   parse-on-demand finds it
  --exclude-static-pattern arg     regex (in 'find' command's regex command 
                                   line option format)  of files or directories
                                   to exclude from static content cache it
  --cfile arg                      extra static files forced to include without
                                   exclusion checking
  --cmodule arg                    extra directories for static files without 
                                   exclusion checking
  --parse-on-demand arg (=1)       whether to parse files that are not 
                                   specified from command line
  --branch arg                     SVN branch
  --revision arg                   SVN revision
  -o [ --output-dir ] arg          output directory
  --sync-dir arg                   Files will be created in this directory 
                                   first, then sync with output directory 
                                   without overwriting identical files. Great 
                                   for incremental compilation and build.
  --optimize-level arg (=1)        optimization level
  --gen-stats arg (=0)             whether to generate dependency graphs and 
                                   code errors
  -k [ --keep-tempdir ] arg (=0)   whether to keep the temporary directory
  --static-method-autofix arg (=1) whether to change a method to static if it 
                                   is called statically
  --db-stats arg                   database connection string to save 
                                   dependency graphs and code errors: 
                                   <username>:<password>@<host>:<port>/<db>
  --no-type-inference arg (=0)     turn off type inference for C++ code 
                                   generation
  --no-min-include arg (=0)        turn off minimium include analysis when 
                                   target is "analyze"
  --no-meta-info arg (=0)          do not generate class map, function jump 
                                   table and macros when generating code; good 
                                   for demo purposes
  --config arg                     config file name
  --db-config arg                  database connection string to read 
                                   configurable options from: 
                                   <username>:<password>@<host>:<port>/<db>
  --config-dir arg                 root directory configuration is based on 
                                   (for example, excluded directories may be 
                                   relative path in configuration.
  -v [ --config-value ] arg        individual configuration string in a format 
                                   of name=value, where name can be any valid 
                                   configuration for a config file
  --log arg (=-1)                  -1: (default); 0: no logging; 1: errors 
                                   only; 2: warnings and errors; 3: 
                                   informational as well; 4: really verbose.
  --force arg (=0)                 force to ignore code generation errors and 
                                   continue compilations
  --file-cache arg                 if specified, generate a static file cache 
                                   with this file name
  --rtti-directory arg             the directory of rtti profiling data
  --java-root arg (=php)           the root package of generated Java FFI 
                                   classes
  --generate-ffi arg (=0)          generate ffi stubs
  --dump arg (=0)                  dump the program graph
