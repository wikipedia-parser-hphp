#!/bin/sh
##
## findmissing.sh
## 
## Made by Michael DuPont
## Login   <mdupont@mdupontdesktop2>
## 
## Started on  Wed Feb 11 19:55:40 2009 Michael DuPont
## Last update Wed Feb 11 19:55:40 2009 Michael DuPont
##
## extract the unmatched templates and add rules
for x in `grep "no template found for" xsltproc.log | sort -u | cut "-d " -f6`; do echo "<\!-- $x \-\-\>"; grep $x newxlst.xml ; done
