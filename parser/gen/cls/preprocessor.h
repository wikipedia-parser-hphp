
#ifndef __GENERATED_cls_preprocessor_h__
#define __GENERATED_cls_preprocessor_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 33524 */
class c_preprocessor : virtual public ObjectData {
  BEGIN_CLASS_MAP(preprocessor)
  END_CLASS_MAP(preprocessor)
  DECLARE_CLASS(preprocessor, Preprocessor, ObjectData)
  void init();
  public: void t___construct(Variant v_parser);
  public: ObjectData *create(Variant v_parser);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_newframe();
  public: void t_newcustomframe(CVarRef v_args);
  public: void t_preprocesstoobj(CVarRef v_text, CVarRef v_flags = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_preprocessor_h__
