
#ifndef __GENERATED_cls_ip_h__
#define __GENERATED_cls_ip_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 289 */
class c_ip : virtual public ObjectData {
  BEGIN_CLASS_MAP(ip)
  END_CLASS_MAP(ip)
  DECLARE_CLASS(ip, IP, ObjectData)
  void init();
  public: static bool ti_isipaddress(const char* cls, CVarRef v_ip);
  public: static bool ti_isipv6(const char* cls, CVarRef v_ip);
  public: static Variant ti_isipv4(const char* cls, CVarRef v_ip);
  public: static Variant ti_ipv4toipv6(const char* cls, CVarRef v_ip);
  public: static Variant ti_tounsigned6(const char* cls, Variant v_ip);
  public: static Variant ti_sanitizeip(const char* cls, Variant v_ip);
  public: static Variant ti_tooctet(const char* cls, CVarRef v_ip_int);
  public: static Variant ti_hextooctet(const char* cls, String v_ip_hex);
  public: static String ti_hextoquad(const char* cls, CVarRef v_ip);
  public: static Array ti_parsecidr6(const char* cls, CVarRef v_range);
  public: static Variant ti_parserange6(const char* cls, Variant v_range);
  public: static bool ti_isvalid(const char* cls, CVarRef v_ip);
  public: static bool ti_isvalidblock(const char* cls, CVarRef v_ipblock);
  public: static bool ti_ispublic(const char* cls, CVarRef v_ip);
  public: static Variant ti_toarray(const char* cls, CVarRef v_ipblock);
  public: static Variant ti_tohex(const char* cls, CVarRef v_ip);
  public: static Variant ti_tounsigned(const char* cls, CVarRef v_ip);
  public: static Variant ti_tosigned(const char* cls, CVarRef v_ip);
  public: static Array ti_parsecidr(const char* cls, CVarRef v_range);
  public: static Variant ti_parserange(const char* cls, CVarRef v_range);
  public: static bool ti_isinrange(const char* cls, CVarRef v_addr, CVarRef v_range);
  public: static Variant ti_canonicalize(const char* cls, Variant v_addr);
  public: static Array t_parsecidr(CVarRef v_range) { return ti_parsecidr("ip", v_range); }
  public: static Variant t_sanitizeip(CVarRef v_ip) { return ti_sanitizeip("ip", v_ip); }
  public: static Variant t_parserange6(CVarRef v_range) { return ti_parserange6("ip", v_range); }
  public: static Variant t_tohex(CVarRef v_ip) { return ti_tohex("ip", v_ip); }
  public: static Variant t_tooctet(CVarRef v_ip_int) { return ti_tooctet("ip", v_ip_int); }
  public: static Variant t_tounsigned(CVarRef v_ip) { return ti_tounsigned("ip", v_ip); }
  public: static Variant t_toarray(CVarRef v_ipblock) { return ti_toarray("ip", v_ipblock); }
  public: static Variant t_isipv4(CVarRef v_ip) { return ti_isipv4("ip", v_ip); }
  public: static String t_hextoquad(CVarRef v_ip) { return ti_hextoquad("ip", v_ip); }
  public: static bool t_isipv6(CVarRef v_ip) { return ti_isipv6("ip", v_ip); }
  public: static bool t_isipaddress(CVarRef v_ip) { return ti_isipaddress("ip", v_ip); }
  public: static bool t_isvalidblock(CVarRef v_ipblock) { return ti_isvalidblock("ip", v_ipblock); }
  public: static Variant t_tosigned(CVarRef v_ip) { return ti_tosigned("ip", v_ip); }
  public: static bool t_ispublic(CVarRef v_ip) { return ti_ispublic("ip", v_ip); }
  public: static bool t_isinrange(CVarRef v_addr, CVarRef v_range) { return ti_isinrange("ip", v_addr, v_range); }
  public: static Variant t_parserange(CVarRef v_range) { return ti_parserange("ip", v_range); }
  public: static Variant t_tounsigned6(CVarRef v_ip) { return ti_tounsigned6("ip", v_ip); }
  public: static Variant t_ipv4toipv6(CVarRef v_ip) { return ti_ipv4toipv6("ip", v_ip); }
  public: static Variant t_canonicalize(CVarRef v_addr) { return ti_canonicalize("ip", v_addr); }
  public: static bool t_isvalid(CVarRef v_ip) { return ti_isvalid("ip", v_ip); }
  public: static Variant t_hextooctet(CStrRef v_ip_hex) { return ti_hextooctet("ip", v_ip_hex); }
  public: static Array t_parsecidr6(CVarRef v_range) { return ti_parsecidr6("ip", v_range); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ip_h__
