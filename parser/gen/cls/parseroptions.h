
#ifndef __GENERATED_cls_parseroptions_h__
#define __GENERATED_cls_parseroptions_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 11047 */
class c_parseroptions : virtual public ObjectData {
  BEGIN_CLASS_MAP(parseroptions)
  END_CLASS_MAP(parseroptions)
  DECLARE_CLASS(parseroptions, ParserOptions, ObjectData)
  void init();
  public: Variant m_mUseTeX;
  public: Variant m_mUseDynamicDates;
  public: Variant m_mInterwikiMagic;
  public: Variant m_mAllowExternalImages;
  public: Variant m_mAllowExternalImagesFrom;
  public: Variant m_mEnableImageWhitelist;
  public: Variant m_mSkin;
  public: Variant m_mDateFormat;
  public: Variant m_mEditSection;
  public: Variant m_mNumberHeadings;
  public: Variant m_mAllowSpecialInclusion;
  public: Variant m_mTidy;
  public: Variant m_mInterfaceMessage;
  public: Variant m_mTargetLanguage;
  public: Variant m_mMaxIncludeSize;
  public: Variant m_mMaxPPNodeCount;
  public: Variant m_mMaxPPExpandDepth;
  public: Variant m_mMaxTemplateDepth;
  public: Variant m_mRemoveComments;
  public: Variant m_mTemplateCallback;
  public: Variant m_mEnableLimitReport;
  public: Variant m_mTimestamp;
  public: Variant m_mExternalLinkTarget;
  public: Variant m_mUser;
  public: Variant m_mIsPreview;
  public: Variant m_mIsSectionPreview;
  public: Variant t_getusetex();
  public: Variant t_getusedynamicdates();
  public: Variant t_getinterwikimagic();
  public: Variant t_getallowexternalimages();
  public: Variant t_getallowexternalimagesfrom();
  public: Variant t_getenableimagewhitelist();
  public: Variant t_geteditsection();
  public: Variant t_getnumberheadings();
  public: Variant t_getallowspecialinclusion();
  public: Variant t_gettidy();
  public: Variant t_getinterfacemessage();
  public: Variant t_gettargetlanguage();
  public: Variant t_getmaxincludesize();
  public: Variant t_getmaxppnodecount();
  public: Variant t_getmaxtemplatedepth();
  public: Variant t_getremovecomments();
  public: Variant t_gettemplatecallback();
  public: Variant t_getenablelimitreport();
  public: Variant t_getcleansignatures();
  public: Variant t_getexternallinktarget();
  public: Variant t_getispreview();
  public: Variant t_getissectionpreview();
  public: Variant t_getskin();
  public: Variant t_getdateformat();
  public: Variant t_gettimestamp();
  public: Variant t_setusetex(CVarRef v_x);
  public: Variant t_setusedynamicdates(CVarRef v_x);
  public: Variant t_setinterwikimagic(CVarRef v_x);
  public: Variant t_setallowexternalimages(CVarRef v_x);
  public: Variant t_setallowexternalimagesfrom(CVarRef v_x);
  public: Variant t_setenableimagewhitelist(CVarRef v_x);
  public: Variant t_setdateformat(CVarRef v_x);
  public: Variant t_seteditsection(CVarRef v_x);
  public: Variant t_setnumberheadings(CVarRef v_x);
  public: Variant t_setallowspecialinclusion(CVarRef v_x);
  public: Variant t_settidy(CVarRef v_x);
  public: void t_setskin(CVarRef v_x);
  public: Variant t_setinterfacemessage(CVarRef v_x);
  public: Variant t_settargetlanguage(CVarRef v_x);
  public: Variant t_setmaxincludesize(CVarRef v_x);
  public: Variant t_setmaxppnodecount(CVarRef v_x);
  public: Variant t_setmaxtemplatedepth(CVarRef v_x);
  public: Variant t_setremovecomments(CVarRef v_x);
  public: Variant t_settemplatecallback(CVarRef v_x);
  public: Variant t_enablelimitreport(CVarRef v_x = true);
  public: Variant t_settimestamp(CVarRef v_x);
  public: Variant t_setcleansignatures(CVarRef v_x);
  public: Variant t_setexternallinktarget(CVarRef v_x);
  public: Variant t_setispreview(CVarRef v_x);
  public: Variant t_setissectionpreview(CVarRef v_x);
  public: void t___construct(Variant v_user = null);
  public: ObjectData *create(Variant v_user = null);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static p_parseroptions ti_newfromuser(const char* cls, CVarRef v_user);
  public: void t_initialisefromuser(Variant v_userInput);
  public: static p_parseroptions t_newfromuser(CVarRef v_user) { return ti_newfromuser("parseroptions", v_user); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parseroptions_h__
