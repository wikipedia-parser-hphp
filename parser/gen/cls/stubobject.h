
#ifndef __GENERATED_cls_stubobject_h__
#define __GENERATED_cls_stubobject_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 10860 */
class c_stubobject : virtual public ObjectData {
  BEGIN_CLASS_MAP(stubobject)
  END_CLASS_MAP(stubobject)
  DECLARE_CLASS(stubobject, StubObject, ObjectData)
  void init();
  public: Variant m_mGlobal;
  public: Variant m_mClass;
  public: Variant m_mParams;
  Variant doCall(Variant v_name, Variant v_arguments, bool fatal);
  public: void t___construct(Variant v_global = null, Variant v_class = null, Variant v_params = ScalarArrays::sa_[0]);
  public: ObjectData *create(Variant v_global = null, Variant v_class = null, Variant v_params = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static bool ti_isrealobject(const char* cls, CVarRef v_obj);
  public: Variant t__call(CVarRef v_name, CVarRef v_args);
  public: Variant t__newobject();
  public: Variant t___call(Variant v_name, Variant v_args);
  public: void t__unstub(CVarRef v_name = "_unstub", CVarRef v_level = 2LL);
  public: static bool t_isrealobject(CVarRef v_obj) { return ti_isrealobject("stubobject", v_obj); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_stubobject_h__
