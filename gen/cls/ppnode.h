
#ifndef __GENERATED_cls_ppnode_h__
#define __GENERATED_cls_ppnode_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor.php line 112 */
class c_ppnode : virtual public ObjectData {
  // public: void t_getchildren() = 0;
  // public: void t_getfirstchild() = 0;
  // public: void t_getnextsibling() = 0;
  // public: void t_getchildrenoftype(CVarRef v_type) = 0;
  // public: void t_getlength() = 0;
  // public: void t_item(CVarRef v_i) = 0;
  // public: void t_getname() = 0;
  // public: void t_splitarg() = 0;
  // public: void t_splitext() = 0;
  // public: void t_splitheading() = 0;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppnode_h__
