
#ifndef __GENERATED_cls_fatalerror_h__
#define __GENERATED_cls_fatalerror_h__

#include <cls/mwexception.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 14894 */
class c_fatalerror : virtual public c_mwexception {
  BEGIN_CLASS_MAP(fatalerror)
    PARENT_CLASS(exception)
    PARENT_CLASS(mwexception)
  END_CLASS_MAP(fatalerror)
  DECLARE_CLASS(fatalerror, FatalError, mwexception)
  void init();
  public: Variant t_gethtml();
  public: Variant t_gettext();
  public: static bool t_iscommandline() { return ti_iscommandline("fatalerror"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fatalerror_h__
