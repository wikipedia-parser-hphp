
#ifndef __GENERATED_cls_mediawiki_i18n_h__
#define __GENERATED_cls_mediawiki_i18n_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 3237 */
class c_mediawiki_i18n : virtual public ObjectData {
  BEGIN_CLASS_MAP(mediawiki_i18n)
  END_CLASS_MAP(mediawiki_i18n)
  DECLARE_CLASS(mediawiki_i18n, MediaWiki_I18N, ObjectData)
  void init();
  public: Variant m__context;
  public: void t_set(CVarRef v_varName, CVarRef v_value);
  public: Variant t_translate(Variant v_value);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_mediawiki_i18n_h__
