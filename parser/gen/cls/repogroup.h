
#ifndef __GENERATED_cls_repogroup_h__
#define __GENERATED_cls_repogroup_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 89 */
class c_repogroup : virtual public ObjectData {
  BEGIN_CLASS_MAP(repogroup)
  END_CLASS_MAP(repogroup)
  DECLARE_CLASS(repogroup, RepoGroup, ObjectData)
  void init();
  public: static void ti_destroysingleton(const char* cls);
  public: static void t_destroysingleton() { ti_destroysingleton("repogroup"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_repogroup_h__
