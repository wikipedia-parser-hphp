
#include <php/DateFormatter.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: DateFormatter.php line 8 */
const int64 q_dateformatter_ALL = -1LL;
const int64 q_dateformatter_NONE = 0LL;
const int64 q_dateformatter_MDY = 1LL;
const int64 q_dateformatter_DMY = 2LL;
const int64 q_dateformatter_YMD = 3LL;
const int64 q_dateformatter_ISO1 = 4LL;
const int64 q_dateformatter_LASTPREF = 4LL;
const int64 q_dateformatter_ISO2 = 5LL;
const int64 q_dateformatter_YDM = 6LL;
const int64 q_dateformatter_DM = 7LL;
const int64 q_dateformatter_MD = 8LL;
const int64 q_dateformatter_LAST = 8LL;
Variant c_dateformatter::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_dateformatter::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_dateformatter::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("mSource", m_mSource.isReferenced() ? ref(m_mSource) : m_mSource));
  props.push_back(NEW(ArrayElement)("mTarget", m_mTarget.isReferenced() ? ref(m_mTarget) : m_mTarget));
  props.push_back(NEW(ArrayElement)("monthNames", m_monthNames.isReferenced() ? ref(m_monthNames) : m_monthNames));
  props.push_back(NEW(ArrayElement)("rxDM", m_rxDM.isReferenced() ? ref(m_rxDM) : m_rxDM));
  props.push_back(NEW(ArrayElement)("rxMD", m_rxMD.isReferenced() ? ref(m_rxMD) : m_rxMD));
  props.push_back(NEW(ArrayElement)("rxDMY", m_rxDMY.isReferenced() ? ref(m_rxDMY) : m_rxDMY));
  props.push_back(NEW(ArrayElement)("rxYDM", m_rxYDM.isReferenced() ? ref(m_rxYDM) : m_rxYDM));
  props.push_back(NEW(ArrayElement)("rxMDY", m_rxMDY.isReferenced() ? ref(m_rxMDY) : m_rxMDY));
  props.push_back(NEW(ArrayElement)("rxYMD", m_rxYMD.isReferenced() ? ref(m_rxYMD) : m_rxYMD));
  props.push_back(NEW(ArrayElement)("regexes", m_regexes.isReferenced() ? ref(m_regexes) : m_regexes));
  props.push_back(NEW(ArrayElement)("pDays", m_pDays.isReferenced() ? ref(m_pDays) : m_pDays));
  props.push_back(NEW(ArrayElement)("pMonths", m_pMonths.isReferenced() ? ref(m_pMonths) : m_pMonths));
  props.push_back(NEW(ArrayElement)("pYears", m_pYears.isReferenced() ? ref(m_pYears) : m_pYears));
  props.push_back(NEW(ArrayElement)("rules", m_rules.isReferenced() ? ref(m_rules) : m_rules));
  props.push_back(NEW(ArrayElement)("xMonths", m_xMonths.isReferenced() ? ref(m_xMonths) : m_xMonths));
  props.push_back(NEW(ArrayElement)("preferences", m_preferences.isReferenced() ? ref(m_preferences) : m_preferences));
  c_ObjectData::o_get(props);
}
bool c_dateformatter::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 2:
      HASH_EXISTS_STRING(0x70659BCA13BC63A2LL, regexes, 7);
      break;
    case 3:
      HASH_EXISTS_STRING(0x64CD89056AE8C163LL, pDays, 5);
      break;
    case 4:
      HASH_EXISTS_STRING(0x57C56B91BFFBF664LL, mSource, 7);
      break;
    case 5:
      HASH_EXISTS_STRING(0x78D6949B972CFD25LL, rules, 5);
      break;
    case 6:
      HASH_EXISTS_STRING(0x011256EA65DC1A26LL, rxMDY, 5);
      break;
    case 9:
      HASH_EXISTS_STRING(0x3391EC675F3EA929LL, rxDMY, 5);
      break;
    case 11:
      HASH_EXISTS_STRING(0x527EE98DA9631E8BLL, rxDM, 4);
      break;
    case 12:
      HASH_EXISTS_STRING(0x226965538682896CLL, pMonths, 7);
      HASH_EXISTS_STRING(0x6B1A9691D8C032CCLL, preferences, 11);
      break;
    case 13:
      HASH_EXISTS_STRING(0x417BE58C8690F50DLL, mTarget, 7);
      break;
    case 17:
      HASH_EXISTS_STRING(0x0CCD1A21600BA171LL, monthNames, 10);
      HASH_EXISTS_STRING(0x755F8AFEAFB00311LL, rxMD, 4);
      break;
    case 24:
      HASH_EXISTS_STRING(0x4E1148C557900378LL, pYears, 6);
      break;
    case 29:
      HASH_EXISTS_STRING(0x657321A3DE31AA3DLL, rxYMD, 5);
      break;
    case 31:
      HASH_EXISTS_STRING(0x4159348239CF623FLL, rxYDM, 5);
      HASH_EXISTS_STRING(0x3E939D7D980AE3DFLL, xMonths, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_dateformatter::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 2:
      HASH_RETURN_STRING(0x70659BCA13BC63A2LL, m_regexes,
                         regexes, 7);
      break;
    case 3:
      HASH_RETURN_STRING(0x64CD89056AE8C163LL, m_pDays,
                         pDays, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x57C56B91BFFBF664LL, m_mSource,
                         mSource, 7);
      break;
    case 5:
      HASH_RETURN_STRING(0x78D6949B972CFD25LL, m_rules,
                         rules, 5);
      break;
    case 6:
      HASH_RETURN_STRING(0x011256EA65DC1A26LL, m_rxMDY,
                         rxMDY, 5);
      break;
    case 9:
      HASH_RETURN_STRING(0x3391EC675F3EA929LL, m_rxDMY,
                         rxDMY, 5);
      break;
    case 11:
      HASH_RETURN_STRING(0x527EE98DA9631E8BLL, m_rxDM,
                         rxDM, 4);
      break;
    case 12:
      HASH_RETURN_STRING(0x226965538682896CLL, m_pMonths,
                         pMonths, 7);
      HASH_RETURN_STRING(0x6B1A9691D8C032CCLL, m_preferences,
                         preferences, 11);
      break;
    case 13:
      HASH_RETURN_STRING(0x417BE58C8690F50DLL, m_mTarget,
                         mTarget, 7);
      break;
    case 17:
      HASH_RETURN_STRING(0x0CCD1A21600BA171LL, m_monthNames,
                         monthNames, 10);
      HASH_RETURN_STRING(0x755F8AFEAFB00311LL, m_rxMD,
                         rxMD, 4);
      break;
    case 24:
      HASH_RETURN_STRING(0x4E1148C557900378LL, m_pYears,
                         pYears, 6);
      break;
    case 29:
      HASH_RETURN_STRING(0x657321A3DE31AA3DLL, m_rxYMD,
                         rxYMD, 5);
      break;
    case 31:
      HASH_RETURN_STRING(0x4159348239CF623FLL, m_rxYDM,
                         rxYDM, 5);
      HASH_RETURN_STRING(0x3E939D7D980AE3DFLL, m_xMonths,
                         xMonths, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_dateformatter::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 2:
      HASH_SET_STRING(0x70659BCA13BC63A2LL, m_regexes,
                      regexes, 7);
      break;
    case 3:
      HASH_SET_STRING(0x64CD89056AE8C163LL, m_pDays,
                      pDays, 5);
      break;
    case 4:
      HASH_SET_STRING(0x57C56B91BFFBF664LL, m_mSource,
                      mSource, 7);
      break;
    case 5:
      HASH_SET_STRING(0x78D6949B972CFD25LL, m_rules,
                      rules, 5);
      break;
    case 6:
      HASH_SET_STRING(0x011256EA65DC1A26LL, m_rxMDY,
                      rxMDY, 5);
      break;
    case 9:
      HASH_SET_STRING(0x3391EC675F3EA929LL, m_rxDMY,
                      rxDMY, 5);
      break;
    case 11:
      HASH_SET_STRING(0x527EE98DA9631E8BLL, m_rxDM,
                      rxDM, 4);
      break;
    case 12:
      HASH_SET_STRING(0x226965538682896CLL, m_pMonths,
                      pMonths, 7);
      HASH_SET_STRING(0x6B1A9691D8C032CCLL, m_preferences,
                      preferences, 11);
      break;
    case 13:
      HASH_SET_STRING(0x417BE58C8690F50DLL, m_mTarget,
                      mTarget, 7);
      break;
    case 17:
      HASH_SET_STRING(0x0CCD1A21600BA171LL, m_monthNames,
                      monthNames, 10);
      HASH_SET_STRING(0x755F8AFEAFB00311LL, m_rxMD,
                      rxMD, 4);
      break;
    case 24:
      HASH_SET_STRING(0x4E1148C557900378LL, m_pYears,
                      pYears, 6);
      break;
    case 29:
      HASH_SET_STRING(0x657321A3DE31AA3DLL, m_rxYMD,
                      rxYMD, 5);
      break;
    case 31:
      HASH_SET_STRING(0x4159348239CF623FLL, m_rxYDM,
                      rxYDM, 5);
      HASH_SET_STRING(0x3E939D7D980AE3DFLL, m_xMonths,
                      xMonths, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_dateformatter::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 2:
      HASH_RETURN_STRING(0x70659BCA13BC63A2LL, m_regexes,
                         regexes, 7);
      break;
    case 3:
      HASH_RETURN_STRING(0x64CD89056AE8C163LL, m_pDays,
                         pDays, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x57C56B91BFFBF664LL, m_mSource,
                         mSource, 7);
      break;
    case 5:
      HASH_RETURN_STRING(0x78D6949B972CFD25LL, m_rules,
                         rules, 5);
      break;
    case 6:
      HASH_RETURN_STRING(0x011256EA65DC1A26LL, m_rxMDY,
                         rxMDY, 5);
      break;
    case 9:
      HASH_RETURN_STRING(0x3391EC675F3EA929LL, m_rxDMY,
                         rxDMY, 5);
      break;
    case 11:
      HASH_RETURN_STRING(0x527EE98DA9631E8BLL, m_rxDM,
                         rxDM, 4);
      break;
    case 12:
      HASH_RETURN_STRING(0x226965538682896CLL, m_pMonths,
                         pMonths, 7);
      HASH_RETURN_STRING(0x6B1A9691D8C032CCLL, m_preferences,
                         preferences, 11);
      break;
    case 13:
      HASH_RETURN_STRING(0x417BE58C8690F50DLL, m_mTarget,
                         mTarget, 7);
      break;
    case 17:
      HASH_RETURN_STRING(0x0CCD1A21600BA171LL, m_monthNames,
                         monthNames, 10);
      HASH_RETURN_STRING(0x755F8AFEAFB00311LL, m_rxMD,
                         rxMD, 4);
      break;
    case 24:
      HASH_RETURN_STRING(0x4E1148C557900378LL, m_pYears,
                         pYears, 6);
      break;
    case 29:
      HASH_RETURN_STRING(0x657321A3DE31AA3DLL, m_rxYMD,
                         rxYMD, 5);
      break;
    case 31:
      HASH_RETURN_STRING(0x4159348239CF623FLL, m_rxYDM,
                         rxYDM, 5);
      HASH_RETURN_STRING(0x3E939D7D980AE3DFLL, m_xMonths,
                         xMonths, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_dateformatter::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 31) {
    case 0:
      HASH_RETURN(0x51EB28404597AB60LL, q_dateformatter_YMD, YMD);
      break;
    case 5:
      HASH_RETURN(0x6E546950BEAE4D65LL, q_dateformatter_MDY, MDY);
      break;
    case 14:
      HASH_RETURN(0x7C72130D53E8CB0ELL, q_dateformatter_MD, MD);
      break;
    case 15:
      HASH_RETURN(0x32A3371A2D165D0FLL, q_dateformatter_YDM, YDM);
      break;
    case 18:
      HASH_RETURN(0x72A8AAC51EE0B772LL, q_dateformatter_ISO1, ISO1);
      HASH_RETURN(0x1068F6ABAD043B32LL, q_dateformatter_LAST, LAST);
      break;
    case 19:
      HASH_RETURN(0x2EFDCA1922BFB273LL, q_dateformatter_NONE, NONE);
      break;
    case 24:
      HASH_RETURN(0x0C4F30470A9EEBF8LL, q_dateformatter_ALL, ALL);
      HASH_RETURN(0x4402D351F519F4D8LL, q_dateformatter_DMY, DMY);
      break;
    case 25:
      HASH_RETURN(0x6A2242B921F18079LL, q_dateformatter_LASTPREF, LASTPREF);
      break;
    case 30:
      HASH_RETURN(0x5BA9215FC910361ELL, q_dateformatter_ISO2, ISO2);
      break;
    case 31:
      HASH_RETURN(0x72C851D50775885FLL, q_dateformatter_DM, DM);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(dateformatter)
ObjectData *c_dateformatter::create() {
  init();
  t_dateformatter();
  return this;
}
ObjectData *c_dateformatter::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_dateformatter::dynConstruct(CArrRef params) {
  (t_dateformatter());
}
ObjectData *c_dateformatter::cloneImpl() {
  c_dateformatter *obj = NEW(c_dateformatter)();
  cloneSet(obj);
  return obj;
}
void c_dateformatter::cloneSet(c_dateformatter *clone) {
  clone->m_mSource = m_mSource.isReferenced() ? ref(m_mSource) : m_mSource;
  clone->m_mTarget = m_mTarget.isReferenced() ? ref(m_mTarget) : m_mTarget;
  clone->m_monthNames = m_monthNames.isReferenced() ? ref(m_monthNames) : m_monthNames;
  clone->m_rxDM = m_rxDM.isReferenced() ? ref(m_rxDM) : m_rxDM;
  clone->m_rxMD = m_rxMD.isReferenced() ? ref(m_rxMD) : m_rxMD;
  clone->m_rxDMY = m_rxDMY.isReferenced() ? ref(m_rxDMY) : m_rxDMY;
  clone->m_rxYDM = m_rxYDM.isReferenced() ? ref(m_rxYDM) : m_rxYDM;
  clone->m_rxMDY = m_rxMDY.isReferenced() ? ref(m_rxMDY) : m_rxMDY;
  clone->m_rxYMD = m_rxYMD.isReferenced() ? ref(m_rxYMD) : m_rxYMD;
  clone->m_regexes = m_regexes.isReferenced() ? ref(m_regexes) : m_regexes;
  clone->m_pDays = m_pDays.isReferenced() ? ref(m_pDays) : m_pDays;
  clone->m_pMonths = m_pMonths.isReferenced() ? ref(m_pMonths) : m_pMonths;
  clone->m_pYears = m_pYears.isReferenced() ? ref(m_pYears) : m_pYears;
  clone->m_rules = m_rules.isReferenced() ? ref(m_rules) : m_rules;
  clone->m_xMonths = m_xMonths.isReferenced() ? ref(m_xMonths) : m_xMonths;
  clone->m_preferences = m_preferences.isReferenced() ? ref(m_preferences) : m_preferences;
  ObjectData::cloneSet(clone);
}
Variant c_dateformatter::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x1CB8D83F9130E490LL, formatdate) {
        int count = params.size();
        if (count <= 1) return (t_formatdate(params.rvalAt(0)));
        return (t_formatdate(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 6:
      HASH_GUARD(0x59D9A29BB871E42ELL, reformat) {
        int count = params.size();
        if (count <= 2) return (t_reformat(params.rvalAt(0), params.rvalAt(1)));
        return (t_reformat(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      HASH_GUARD(0x696D931EC4A9598ELL, dateformatter) {
        return (t_dateformatter(), null);
      }
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (t_replace(params.rvalAt(0)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_dateformatter::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x1CB8D83F9130E490LL, formatdate) {
        if (count <= 1) return (t_formatdate(a0));
        return (t_formatdate(a0, a1));
      }
      break;
    case 6:
      HASH_GUARD(0x59D9A29BB871E42ELL, reformat) {
        if (count <= 2) return (t_reformat(a0, a1));
        return (t_reformat(a0, a1, a2));
      }
      HASH_GUARD(0x696D931EC4A9598ELL, dateformatter) {
        return (t_dateformatter(), null);
      }
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (t_replace(a0));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_dateformatter::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_dateformatter$os_get(const char *s) {
  return c_dateformatter::os_get(s, -1);
}
Variant &cw_dateformatter$os_lval(const char *s) {
  return c_dateformatter::os_lval(s, -1);
}
Variant cw_dateformatter$os_constant(const char *s) {
  return c_dateformatter::os_constant(s);
}
Variant cw_dateformatter$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_dateformatter::os_invoke(c, s, params, -1, fatal);
}
void c_dateformatter::init() {
  m_mSource = null;
  m_mTarget = null;
  m_monthNames = "";
  m_rxDM = null;
  m_rxMD = null;
  m_rxDMY = null;
  m_rxYDM = null;
  m_rxMDY = null;
  m_rxYMD = null;
  m_regexes = null;
  m_pDays = null;
  m_pMonths = null;
  m_pYears = null;
  m_rules = null;
  m_xMonths = null;
  m_preferences = null;
}
/* SRC: DateFormatter.php line 32 */
void c_dateformatter::t_dateformatter() {
  INSTANCE_METHOD_INJECTION(DateFormatter, DateFormatter::DateFormatter);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_i;

  (m_monthNames = LINE(35,t_getmonthregex()));
  {
    LOOP_COUNTER(1);
    for ((v_i = 1LL); not_more(v_i, 12LL); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        m_xMonths.set(LINE(37,gv_wgContLang.o_invoke_few_args("lc", 0x0191205BE4FF8F9CLL, 1, gv_wgContLang.o_invoke_few_args("getMonthName", 0x5DEDDA0E21068FE8LL, 1, v_i))), (v_i));
        m_xMonths.set(LINE(38,gv_wgContLang.o_invoke_few_args("lc", 0x0191205BE4FF8F9CLL, 1, gv_wgContLang.o_invoke_few_args("getMonthAbbreviation", 0x75A1001EE20C7275LL, 1, v_i))), (v_i));
      }
    }
  }
  (o_lval("regexTrail", 0x4AE8C8B9794CC251LL) = "(\?![a-z])/iu");
  (o_lval("prxDM", 0x0729C45821ADBF0ALL) = LINE(44,concat3("\\[\\[(\\d{1,2})[ _](", toString(m_monthNames), ")\\]\\]")));
  (o_lval("prxMD", 0x32CECC2DDFBDEA35LL) = LINE(45,concat3("\\[\\[(", toString(m_monthNames), ")[ _](\\d{1,2})\\]\\]")));
  (o_lval("prxY", 0x35FD7C92164DED11LL) = "\\[\\[(\\d{1,4}([ _]BC|))\\]\\]");
  (o_lval("prxISO1", 0x066E5D82C85246BFLL) = "\\[\\[(-\?\\d{4})]]-\\[\\[(\\d{2})-(\\d{2})\\]\\]");
  (o_lval("prxISO2", 0x4CA69F3E18F28138LL) = "\\[\\[(-\?\\d{4})-(\\d{2})-(\\d{2})\\]\\]");
  m_regexes.set(2LL /* dateformatter::DMY */, (LINE(51,concat5("/", toString(o_get("prxDM", 0x0729C45821ADBF0ALL)), "(\?: *, *| +)", toString(o_get("prxY", 0x35FD7C92164DED11LL)), toString(o_get("regexTrail", 0x4AE8C8B9794CC251LL))))), 0x486AFCC090D5F98CLL);
  m_regexes.set(6LL /* dateformatter::YDM */, (LINE(52,concat5("/", toString(o_get("prxY", 0x35FD7C92164DED11LL)), "(\?: *, *| +)", toString(o_get("prxDM", 0x0729C45821ADBF0ALL)), toString(o_get("regexTrail", 0x4AE8C8B9794CC251LL))))), 0x26BF47194D7E8E12LL);
  m_regexes.set(1LL /* dateformatter::MDY */, (LINE(53,concat5("/", toString(o_get("prxMD", 0x32CECC2DDFBDEA35LL)), "(\?: *, *| +)", toString(o_get("prxY", 0x35FD7C92164DED11LL)), toString(o_get("regexTrail", 0x4AE8C8B9794CC251LL))))), 0x5BCA7C69B794F8CELL);
  m_regexes.set(3LL /* dateformatter::YMD */, (LINE(54,concat5("/", toString(o_get("prxY", 0x35FD7C92164DED11LL)), "(\?: *, *| +)", toString(o_get("prxMD", 0x32CECC2DDFBDEA35LL)), toString(o_get("regexTrail", 0x4AE8C8B9794CC251LL))))), 0x135FDDF6A6BFBBDDLL);
  m_regexes.set(7LL /* dateformatter::DM */, (LINE(55,concat3("/", toString(o_get("prxDM", 0x0729C45821ADBF0ALL)), toString(o_get("regexTrail", 0x4AE8C8B9794CC251LL))))), 0x7D75B33B7AEB669DLL);
  m_regexes.set(8LL /* dateformatter::MD */, (LINE(56,concat3("/", toString(o_get("prxMD", 0x32CECC2DDFBDEA35LL)), toString(o_get("regexTrail", 0x4AE8C8B9794CC251LL))))), 0x21ABC084C3578135LL);
  m_regexes.set(4LL /* dateformatter::ISO1 */, (LINE(57,concat3("/", toString(o_get("prxISO1", 0x066E5D82C85246BFLL)), toString(o_get("regexTrail", 0x4AE8C8B9794CC251LL))))), 0x6F2A25235E544A31LL);
  m_regexes.set(5LL /* dateformatter::ISO2 */, (LINE(58,concat3("/", toString(o_get("prxISO2", 0x4CA69F3E18F28138LL)), toString(o_get("regexTrail", 0x4AE8C8B9794CC251LL))))), 0x350AEB726A15D700LL);
  lval(o_lval("keys", 0x654D31BC4825131CLL)).set(2LL /* dateformatter::DMY */, ("jFY"), 0x486AFCC090D5F98CLL);
  lval(o_lval("keys", 0x654D31BC4825131CLL)).set(6LL /* dateformatter::YDM */, ("Y jF"), 0x26BF47194D7E8E12LL);
  lval(o_lval("keys", 0x654D31BC4825131CLL)).set(1LL /* dateformatter::MDY */, ("FjY"), 0x5BCA7C69B794F8CELL);
  lval(o_lval("keys", 0x654D31BC4825131CLL)).set(3LL /* dateformatter::YMD */, ("Y Fj"), 0x135FDDF6A6BFBBDDLL);
  lval(o_lval("keys", 0x654D31BC4825131CLL)).set(7LL /* dateformatter::DM */, ("jF"), 0x7D75B33B7AEB669DLL);
  lval(o_lval("keys", 0x654D31BC4825131CLL)).set(8LL /* dateformatter::MD */, ("Fj"), 0x21ABC084C3578135LL);
  lval(o_lval("keys", 0x654D31BC4825131CLL)).set(4LL /* dateformatter::ISO1 */, ("ymd"), 0x6F2A25235E544A31LL);
  lval(o_lval("keys", 0x654D31BC4825131CLL)).set(5LL /* dateformatter::ISO2 */, ("ymd"), 0x350AEB726A15D700LL);
  lval(o_lval("targets", 0x15A9D30BFE3B6CC6LL)).set(2LL /* dateformatter::DMY */, ("[[F j|j F]] [[Y]]"), 0x486AFCC090D5F98CLL);
  lval(o_lval("targets", 0x15A9D30BFE3B6CC6LL)).set(6LL /* dateformatter::YDM */, ("[[Y]], [[F j|j F]]"), 0x26BF47194D7E8E12LL);
  lval(o_lval("targets", 0x15A9D30BFE3B6CC6LL)).set(1LL /* dateformatter::MDY */, ("[[F j]], [[Y]]"), 0x5BCA7C69B794F8CELL);
  lval(o_lval("targets", 0x15A9D30BFE3B6CC6LL)).set(3LL /* dateformatter::YMD */, ("[[Y]] [[F j]]"), 0x135FDDF6A6BFBBDDLL);
  lval(o_lval("targets", 0x15A9D30BFE3B6CC6LL)).set(7LL /* dateformatter::DM */, ("[[F j|j F]]"), 0x7D75B33B7AEB669DLL);
  lval(o_lval("targets", 0x15A9D30BFE3B6CC6LL)).set(8LL /* dateformatter::MD */, ("[[F j]]"), 0x21ABC084C3578135LL);
  lval(o_lval("targets", 0x15A9D30BFE3B6CC6LL)).set(4LL /* dateformatter::ISO1 */, ("[[Y|y]]-[[F j|m-d]]"), 0x6F2A25235E544A31LL);
  lval(o_lval("targets", 0x15A9D30BFE3B6CC6LL)).set(5LL /* dateformatter::ISO2 */, ("[[y-m-d]]"), 0x350AEB726A15D700LL);
  lval(m_rules.lvalAt(2LL /* dateformatter::DMY */, 0x486AFCC090D5F98CLL)).set(8LL /* dateformatter::MD */, (7LL /* dateformatter::DM */), 0x21ABC084C3578135LL);
  lval(m_rules.lvalAt(-1LL /* dateformatter::ALL */, 0x1F89206E3F8EC794LL)).set(8LL /* dateformatter::MD */, (8LL /* dateformatter::MD */), 0x21ABC084C3578135LL);
  lval(m_rules.lvalAt(1LL /* dateformatter::MDY */, 0x5BCA7C69B794F8CELL)).set(7LL /* dateformatter::DM */, (8LL /* dateformatter::MD */), 0x7D75B33B7AEB669DLL);
  lval(m_rules.lvalAt(-1LL /* dateformatter::ALL */, 0x1F89206E3F8EC794LL)).set(7LL /* dateformatter::DM */, (7LL /* dateformatter::DM */), 0x7D75B33B7AEB669DLL);
  lval(m_rules.lvalAt(0LL /* dateformatter::NONE */, 0x77CFA1EEF01BCA90LL)).set(5LL /* dateformatter::ISO2 */, (4LL /* dateformatter::ISO1 */), 0x350AEB726A15D700LL);
  (m_preferences = ScalarArrays::sa_[33]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: DateFormatter.php line 103 */
Variant c_dateformatter::ti_getinstance(const char* cls) {
  STATIC_METHOD_INJECTION(DateFormatter, DateFormatter::getInstance);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgMemc __attribute__((__unused__)) = g->GV(wgMemc);
  Variant &sv_dateFormatter __attribute__((__unused__)) = g->sv_dateformatter$$getinstance$$dateFormatter.lvalAt(cls);
  Variant &inited_sv_dateFormatter __attribute__((__unused__)) = g->inited_sv_dateformatter$$getinstance$$dateFormatter.lvalAt(cls);
  if (!inited_sv_dateFormatter) {
    (sv_dateFormatter = false);
    inited_sv_dateFormatter = true;
  }
  if (!(toBoolean(sv_dateFormatter))) {
    (sv_dateFormatter = LINE(107,gv_wgMemc.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, invoke_failed("wfmemckey", Array(ArrayInit(1).set(0, "dateformatter").create()), 0x0000000072AF623FLL))));
    if (!(toBoolean(sv_dateFormatter))) {
      (sv_dateFormatter = ((Object)(LINE(109,p_dateformatter(p_dateformatter(NEWOBJ(c_dateformatter)())->create())))));
      (assignCallTemp(eo_0, ref(LINE(110,invoke_failed("wfmemckey", Array(ArrayInit(1).set(0, "dateformatter").create()), 0x0000000072AF623FLL)))),assignCallTemp(eo_1, ref(sv_dateFormatter)),gv_wgMemc.o_invoke("set", Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, 3600LL).create()), 0x399A6427C2185621LL));
    }
  }
  return ref(sv_dateFormatter);
} /* function */
/* SRC: DateFormatter.php line 120 */
Variant c_dateformatter::t_reformat(Variant v_preference, Variant v_text, CVarRef v_options //  = ScalarArrays::sa_[34]
) {
  INSTANCE_METHOD_INJECTION(DateFormatter, DateFormatter::reformat);
  DECLARE_GLOBAL_VARIABLES(g);
  bool v_linked = false;
  bool v_match_whole = false;
  int64 v_i = 0;
  Variant v_regex;

  (v_linked = LINE(122,x_in_array("linked", v_options)));
  (v_match_whole = LINE(123,x_in_array("match-whole", v_options)));
  if (isset(m_preferences, v_preference)) {
    (v_preference = m_preferences.rvalAt(v_preference));
  }
  else {
    (v_preference = 0LL /* dateformatter::NONE */);
  }
  {
    LOOP_COUNTER(2);
    for ((v_i = 1LL); not_more(v_i, 8LL /* dateformatter::LAST */); v_i++) {
      LOOP_COUNTER_CHECK(2);
      {
        (m_mSource = v_i);
        if (isset(m_rules.rvalAt(v_preference), v_i)) {
          (m_mTarget = m_rules.rvalAt(v_preference).rvalAt(v_i));
        }
        else if (isset(m_rules.rvalAt(-1LL /* dateformatter::ALL */, 0x1F89206E3F8EC794LL), v_i)) {
          (m_mTarget = m_rules.rvalAt(-1LL /* dateformatter::ALL */, 0x1F89206E3F8EC794LL).rvalAt(v_i));
        }
        else if (toBoolean(v_preference)) {
          (m_mTarget = v_preference);
        }
        else {
          (m_mTarget = v_i);
        }
        (v_regex = m_regexes.rvalAt(v_i));
        if (!(v_linked)) {
          (v_regex = LINE(149,x_str_replace(ScalarArrays::sa_[35], "", v_regex)));
        }
        if (v_match_whole) {
          (v_regex = LINE(154,x_preg_replace("!^/!", "/^", v_regex)));
          (v_regex = LINE(156,x_str_replace(o_get("regexTrail", 0x4AE8C8B9794CC251LL), concat("$", toString(o_get("regexTrail", 0x4AE8C8B9794CC251LL))), v_regex)));
        }
        (o_lval("mLinked", 0x79F764A8DC3361BBLL) = v_linked);
        (v_text = LINE(161,x_preg_replace_callback(v_regex, Array(ArrayInit(2).setRef(0, ref(this)).set(1, "replace").create()), v_text)));
        t___unset("mLinked");
      }
    }
  }
  return v_text;
} /* function */
/* SRC: DateFormatter.php line 170 */
Variant c_dateformatter::t_replace(CVarRef v_matches) {
  INSTANCE_METHOD_INJECTION(DateFormatter, DateFormatter::replace);
  Variant v_linked;
  Variant v_bits;
  Variant v_key;
  int64 v_p = 0;

  (v_linked = true);
  if (t___isset("mLinked")) (v_linked = o_get("mLinked", 0x79F764A8DC3361BBLL));
  (v_bits = ScalarArrays::sa_[0]);
  (v_key = o_get("keys", 0x654D31BC4825131CLL).rvalAt(m_mSource));
  {
    LOOP_COUNTER(3);
    for ((v_p = 0LL); less(v_p, LINE(178,x_strlen(toString(v_key)))); v_p++) {
      LOOP_COUNTER_CHECK(3);
      {
        if (!equal(v_key.rvalAt(v_p), " ")) {
          v_bits.set(v_key.rvalAt(v_p), (v_matches.rvalAt(v_p + 1LL)));
        }
      }
    }
  }
  return LINE(184,t_formatdate(v_bits, v_linked));
} /* function */
/* SRC: DateFormatter.php line 187 */
Variant c_dateformatter::t_formatdate(Variant v_bits, CVarRef v_link //  = true
) {
  INSTANCE_METHOD_INJECTION(DateFormatter, DateFormatter::formatDate);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_format;
  Variant v_text;
  bool v_fail = false;
  Variant v_m;
  int64 v_p = 0;
  Variant v_char;
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_wgContLang;
  Variant v_matches;
  Array v_isoBits;
  String v_isoDate;

  (v_format = o_get("targets", 0x15A9D30BFE3B6CC6LL).rvalAt(m_mTarget));
  if (!(toBoolean(v_link))) {
    (v_format = LINE(192,x_preg_replace("/\\[\\[[^|]+\\|([^\\]]+)\\]\\]/", "$1", v_format)));
    (v_format = LINE(194,x_str_replace(ScalarArrays::sa_[36], "", v_format)));
  }
  (v_text = "");
  (v_fail = false);
  if (!(isset(v_bits, "y", 0x4F56B733A4DFC78ALL)) && isset(v_bits, "Y", 0x7F64BC1ECEF60CCCLL)) v_bits.set("y", (LINE(203,t_makeisoyear(v_bits.rvalAt("Y", 0x7F64BC1ECEF60CCCLL)))), 0x4F56B733A4DFC78ALL);
  if (!(isset(v_bits, "Y", 0x7F64BC1ECEF60CCCLL)) && isset(v_bits, "y", 0x4F56B733A4DFC78ALL)) v_bits.set("Y", (LINE(205,t_makenormalyear(v_bits.rvalAt("y", 0x4F56B733A4DFC78ALL)))), 0x7F64BC1ECEF60CCCLL);
  if (!(isset(v_bits, "m", 0x7A0C10E3609EA04BLL))) {
    (v_m = LINE(208,t_makeisomonth(v_bits.rvalAt("F", 0x286CE1C477560280LL))));
    if (!(toBoolean(v_m)) || equal(v_m, "00")) {
      (v_fail = true);
    }
    else {
      v_bits.set("m", (v_m), 0x7A0C10E3609EA04BLL);
    }
  }
  if (!(isset(v_bits, "d", 0x7A452383AA9BF7C4LL))) {
    v_bits.set("d", (LINE(217,x_sprintf(2, "%02d", Array(ArrayInit(1).set(0, v_bits.rvalAt("j", 0x4B27011F259D2446LL)).create())))), 0x7A452383AA9BF7C4LL);
  }
  {
    LOOP_COUNTER(4);
    for ((v_p = 0LL); less(v_p, LINE(220,x_strlen(toString(v_format)))); v_p++) {
      LOOP_COUNTER_CHECK(4);
      {
        (v_char = v_format.rvalAt(v_p));
        {
          Variant tmp6 = (v_char);
          int tmp7 = -1;
          if (equal(tmp6, ("d"))) {
            tmp7 = 0;
          } else if (equal(tmp6, ("m"))) {
            tmp7 = 1;
          } else if (equal(tmp6, ("y"))) {
            tmp7 = 2;
          } else if (equal(tmp6, ("j"))) {
            tmp7 = 3;
          } else if (equal(tmp6, ("F"))) {
            tmp7 = 4;
          } else if (equal(tmp6, ("Y"))) {
            tmp7 = 5;
          } else if (true) {
            tmp7 = 6;
          }
          switch (tmp7) {
          case 0:
            {
              concat_assign(v_text, toString(v_bits.rvalAt("d", 0x7A452383AA9BF7C4LL)));
              goto break5;
            }
          case 1:
            {
              concat_assign(v_text, toString(v_bits.rvalAt("m", 0x7A0C10E3609EA04BLL)));
              goto break5;
            }
          case 2:
            {
              concat_assign(v_text, toString(v_bits.rvalAt("y", 0x4F56B733A4DFC78ALL)));
              goto break5;
            }
          case 3:
            {
              if (!(isset(v_bits, "j", 0x4B27011F259D2446LL))) {
                concat_assign(v_text, toString(LINE(234,x_intval(v_bits.rvalAt("d", 0x7A452383AA9BF7C4LL)))));
              }
              else {
                concat_assign(v_text, toString(v_bits.rvalAt("j", 0x4B27011F259D2446LL)));
              }
              goto break5;
            }
          case 4:
            {
              if (!(isset(v_bits, "F", 0x286CE1C477560280LL))) {
                (v_m = LINE(241,x_intval(v_bits.rvalAt("m", 0x7A0C10E3609EA04BLL))));
                if (more(v_m, 12LL) || less(v_m, 1LL)) {
                  (v_fail = true);
                }
                else {
                  v_wgContLang = ref(g->GV(wgContLang));
                  concat_assign(v_text, toString(LINE(246,v_wgContLang.o_invoke_few_args("getMonthName", 0x5DEDDA0E21068FE8LL, 1, v_m))));
                }
              }
              else {
                concat_assign(v_text, LINE(249,x_ucfirst(toString(v_bits.rvalAt("F", 0x286CE1C477560280LL)))));
              }
              goto break5;
            }
          case 5:
            {
              concat_assign(v_text, toString(v_bits.rvalAt("Y", 0x7F64BC1ECEF60CCCLL)));
              goto break5;
            }
          case 6:
            {
              concat_assign(v_text, toString(v_char));
            }
          }
          break5:;
        }
      }
    }
  }
  if (v_fail) {
    (v_text = v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  }
  (v_isoBits = ScalarArrays::sa_[0]);
  if (isset(v_bits, "y", 0x4F56B733A4DFC78ALL)) v_isoBits.append((v_bits.rvalAt("y", 0x4F56B733A4DFC78ALL)));
  v_isoBits.append((v_bits.rvalAt("m", 0x7A0C10E3609EA04BLL)));
  v_isoBits.append((v_bits.rvalAt("d", 0x7A452383AA9BF7C4LL)));
  (v_isoDate = LINE(268,x_implode("-", v_isoBits)));
  (v_text = LINE(272,throw_fatal("unknown class html", ((void*)NULL))));
  return v_text;
} /* function */
/* SRC: DateFormatter.php line 280 */
String c_dateformatter::t_getmonthregex() {
  INSTANCE_METHOD_INJECTION(DateFormatter, DateFormatter::getMonthRegex);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Array v_names;
  Variant v_i;

  (v_names = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(8);
    for ((v_i = 1LL); not_more(v_i, 12LL); v_i++) {
      LOOP_COUNTER_CHECK(8);
      {
        v_names.append((LINE(284,gv_wgContLang.o_invoke_few_args("getMonthName", 0x5DEDDA0E21068FE8LL, 1, v_i))));
        v_names.append((LINE(285,gv_wgContLang.o_invoke_few_args("getMonthAbbreviation", 0x75A1001EE20C7275LL, 1, v_i))));
      }
    }
  }
  return LINE(287,x_implode("|", v_names));
} /* function */
/* SRC: DateFormatter.php line 295 */
String c_dateformatter::t_makeisomonth(Variant v_monthName) {
  INSTANCE_METHOD_INJECTION(DateFormatter, DateFormatter::makeIsoMonth);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_n;

  (v_n = m_xMonths.rvalAt(LINE(298,gv_wgContLang.o_invoke_few_args("lc", 0x0191205BE4FF8F9CLL, 1, v_monthName))));
  return LINE(299,x_sprintf(2, "%02d", Array(ArrayInit(1).set(0, v_n).create())));
} /* function */
/* SRC: DateFormatter.php line 307 */
String c_dateformatter::t_makeisoyear(CVarRef v_year) {
  INSTANCE_METHOD_INJECTION(DateFormatter, DateFormatter::makeIsoYear);
  int64 v_num = 0;
  String v_text;

  if (equal(LINE(309,x_substr(toString(v_year), toInt32(-2LL))), "BC")) {
    (v_num = LINE(310,x_intval(x_substr(toString(v_year), toInt32(0LL), toInt32(-3LL)))) - 1LL);
    (v_text = LINE(312,x_sprintf(2, "-%04d", Array(ArrayInit(1).set(0, v_num).create()))));
  }
  else {
    (v_text = LINE(315,x_sprintf(2, "%04d", Array(ArrayInit(1).set(0, v_year).create()))));
  }
  return v_text;
} /* function */
/* SRC: DateFormatter.php line 323 */
Variant c_dateformatter::t_makenormalyear(CVarRef v_iso) {
  INSTANCE_METHOD_INJECTION(DateFormatter, DateFormatter::makeNormalYear);
  Variant v_text;

  if (equal(v_iso.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), "-")) {
    (v_text = concat((toString(LINE(325,x_intval(x_substr(toString(v_iso), toInt32(1LL)))) + 1LL)), " BC"));
  }
  else {
    (v_text = LINE(327,x_intval(v_iso)));
  }
  return v_text;
} /* function */
Object co_dateformatter(CArrRef params, bool init /* = true */) {
  return Object(p_dateformatter(NEW(c_dateformatter)())->dynCreate(params, init));
}
Variant pm_php$DateFormatter_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::DateFormatter.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$DateFormatter_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
