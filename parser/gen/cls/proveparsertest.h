
#ifndef __GENERATED_cls_proveparsertest_h__
#define __GENERATED_cls_proveparsertest_h__

#include <cls/parsertest.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 45014 */
class c_proveparsertest : virtual public c_parsertest {
  BEGIN_CLASS_MAP(proveparsertest)
    PARENT_CLASS(parsertest)
  END_CLASS_MAP(proveparsertest)
  DECLARE_CLASS(proveparsertest, ProveParserTest, parsertest)
  void init();
  public: void t_showsuccess(CVarRef v_desc);
  public: void t_showfailure(CVarRef v_desc, CVarRef v_exp, CVarRef v_got);
  public: void t_showrunfile(CVarRef v_path);
  public: static void t_deletefiles(CArrRef v_files) { ti_deletefiles("proveparsertest", v_files); }
  public: static Variant t_getoptionvalue(CStrRef v_regex, CVarRef v_opts, CVarRef v_default) { return ti_getoptionvalue("proveparsertest", v_regex, v_opts, v_default); }
  public: static void t_deletedirs(CArrRef v_dirs) { ti_deletedirs("proveparsertest", v_dirs); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_proveparsertest_h__
