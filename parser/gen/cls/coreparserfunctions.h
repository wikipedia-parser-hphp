
#ifndef __GENERATED_cls_coreparserfunctions_h__
#define __GENERATED_cls_coreparserfunctions_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 32490 */
class c_coreparserfunctions : virtual public ObjectData {
  BEGIN_CLASS_MAP(coreparserfunctions)
  END_CLASS_MAP(coreparserfunctions)
  DECLARE_CLASS(coreparserfunctions, CoreParserFunctions, ObjectData)
  void init();
  public: static void ti_register(const char* cls, p_parser v_parser);
  public: static Variant ti_intfunction(const char* cls, int num_args, CVarRef v_parser, CVarRef v_part1 = "", Array args = Array());
  public: static Variant ti_ns(const char* cls, CVarRef v_parser, CVarRef v_part1 = "");
  public: static String ti_urlencode(const char* cls, CVarRef v_parser, CVarRef v_s = "");
  public: static Variant ti_lcfirst(const char* cls, CVarRef v_parser, Variant v_s = "");
  public: static Variant ti_ucfirst(const char* cls, CVarRef v_parser, Variant v_s = "");
  public: static Variant ti_lc(const char* cls, CVarRef v_parser, Variant v_s = "");
  public: static Variant ti_uc(const char* cls, CVarRef v_parser, Variant v_s = "");
  public: static Variant ti_localurl(const char* cls, CVarRef v_parser, CVarRef v_s = "", CVarRef v_arg = null_variant);
  public: static Variant ti_localurle(const char* cls, CVarRef v_parser, CVarRef v_s = "", CVarRef v_arg = null_variant);
  public: static Variant ti_fullurl(const char* cls, CVarRef v_parser, CVarRef v_s = "", CVarRef v_arg = null_variant);
  public: static Variant ti_fullurle(const char* cls, CVarRef v_parser, CVarRef v_s = "", CVarRef v_arg = null_variant);
  public: static Variant ti_urlfunction(const char* cls, CStrRef v_func, CVarRef v_s = "", Variant v_arg = null);
  public: static Variant ti_formatnum(const char* cls, CVarRef v_parser, Variant v_num = "", CVarRef v_raw = null_variant);
  public: static Variant ti_grammar(const char* cls, CVarRef v_parser, Variant v_case = "", Variant v_word = "");
  public: static Variant ti_gender(const char* cls, int num_args, CVarRef v_parser, Variant v_user, Array args = Array());
  public: static Variant ti_plural(const char* cls, int num_args, CVarRef v_parser, Variant v_text = "", Array args = Array());
  public: static String ti_displaytitle(const char* cls, CVarRef v_parser, Variant v_text = "");
  public: static Variant ti_israw(const char* cls, Variant v_param);
  public: static Variant ti_formatraw(const char* cls, Variant v_num, CVarRef v_raw);
  public: static Variant ti_numberofpages(const char* cls, CVarRef v_parser, CVarRef v_raw = null_variant);
  public: static Variant ti_numberofusers(const char* cls, CVarRef v_parser, CVarRef v_raw = null_variant);
  public: static Variant ti_numberofactiveusers(const char* cls, CVarRef v_parser, CVarRef v_raw = null_variant);
  public: static Variant ti_numberofarticles(const char* cls, CVarRef v_parser, CVarRef v_raw = null_variant);
  public: static Variant ti_numberoffiles(const char* cls, CVarRef v_parser, CVarRef v_raw = null_variant);
  public: static Variant ti_numberofadmins(const char* cls, CVarRef v_parser, CVarRef v_raw = null_variant);
  public: static Variant ti_numberofedits(const char* cls, CVarRef v_parser, CVarRef v_raw = null_variant);
  public: static Variant ti_numberofviews(const char* cls, CVarRef v_parser, CVarRef v_raw = null_variant);
  public: static Variant ti_pagesinnamespace(const char* cls, CVarRef v_parser, CVarRef v_namespace = 0LL, CVarRef v_raw = null_variant);
  public: static Variant ti_numberingroup(const char* cls, CVarRef v_parser, CVarRef v_name = "", CVarRef v_raw = null_variant);
  public: static Variant ti_namespace(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_namespacee(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_talkspace(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_talkspacee(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_subjectspace(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_subjectspacee(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_pagename(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_pagenamee(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_fullpagename(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_fullpagenamee(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_subpagename(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_subpagenamee(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_basepagename(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_basepagenamee(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_talkpagename(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_talkpagenamee(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_subjectpagename(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_subjectpagenamee(const char* cls, CVarRef v_parser, CVarRef v_title = null_variant);
  public: static Variant ti_pagesincategory(const char* cls, CVarRef v_parser, Variant v_name = "", CVarRef v_raw = null_variant);
  public: static Variant ti_pagesize(const char* cls, CVarRef v_parser, Variant v_page = "", CVarRef v_raw = null_variant);
  public: static String ti_protectionlevel(const char* cls, CVarRef v_parser, CVarRef v_type = "");
  public: static Variant ti_language(const char* cls, CVarRef v_parser, CVarRef v_arg = "");
  public: static Variant ti_pad(const char* cls, CVarRef v_string, Variant v_length, CVarRef v_padding = "0", int64 v_direction = 1LL /* STR_PAD_RIGHT */);
  public: static Variant ti_padleft(const char* cls, CVarRef v_parser, CVarRef v_string = "", CVarRef v_length = 0LL, CVarRef v_padding = "0");
  public: static Variant ti_padright(const char* cls, CVarRef v_parser, CVarRef v_string = "", CVarRef v_length = 0LL, CVarRef v_padding = "0");
  public: static Variant ti_anchorencode(const char* cls, CVarRef v_parser, CVarRef v_text);
  public: static Variant ti_special(const char* cls, CVarRef v_parser, CVarRef v_text);
  public: static Variant ti_defaultsort(const char* cls, CVarRef v_parser, Variant v_text);
  public: static Variant ti_filepath(const char* cls, CVarRef v_parser, CVarRef v_name = "", CVarRef v_option = "");
  public: static Variant ti_tagobj(const char* cls, CVarRef v_parser, Variant v_frame, Variant v_args);
  public: static Variant t_basepagenamee(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_basepagenamee("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_padleft(CVarRef v_parser, CVarRef v_string = "", CVarRef v_length = 0LL, CVarRef v_padding = "0") { return ti_padleft("coreparserfunctions", v_parser, v_string, v_length, v_padding); }
  public: static Variant t_numberofviews(CVarRef v_parser, CVarRef v_raw = null_variant) { return ti_numberofviews("coreparserfunctions", v_parser, v_raw); }
  public: static Variant t_talkpagenamee(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_talkpagenamee("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_defaultsort(CVarRef v_parser, CVarRef v_text) { return ti_defaultsort("coreparserfunctions", v_parser, v_text); }
  public: static Variant t_pagesincategory(CVarRef v_parser, CVarRef v_name = "", CVarRef v_raw = null_variant) { return ti_pagesincategory("coreparserfunctions", v_parser, v_name, v_raw); }
  public: static Variant t_fullpagename(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_fullpagename("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_talkspacee(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_talkspacee("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_subjectspace(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_subjectspace("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_lcfirst(CVarRef v_parser, CVarRef v_s = "") { return ti_lcfirst("coreparserfunctions", v_parser, v_s); }
  public: static Variant t_numberofusers(CVarRef v_parser, CVarRef v_raw = null_variant) { return ti_numberofusers("coreparserfunctions", v_parser, v_raw); }
  public: static Variant t_localurle(CVarRef v_parser, CVarRef v_s = "", CVarRef v_arg = null_variant) { return ti_localurle("coreparserfunctions", v_parser, v_s, v_arg); }
  public: static Variant t_fullpagenamee(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_fullpagenamee("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_urlfunction(CStrRef v_func, CVarRef v_s = "", CVarRef v_arg = null_variant) { return ti_urlfunction("coreparserfunctions", v_func, v_s, v_arg); }
  public: static Variant t_numberofedits(CVarRef v_parser, CVarRef v_raw = null_variant) { return ti_numberofedits("coreparserfunctions", v_parser, v_raw); }
  public: static Variant t_numberofarticles(CVarRef v_parser, CVarRef v_raw = null_variant) { return ti_numberofarticles("coreparserfunctions", v_parser, v_raw); }
  public: static Variant t_gender(int num_args, CVarRef v_parser, CVarRef v_user, Array args = Array()) { return ti_gender("coreparserfunctions", num_args, v_parser, v_user, args); }
  public: static Variant t_pagenamee(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_pagenamee("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_numberofpages(CVarRef v_parser, CVarRef v_raw = null_variant) { return ti_numberofpages("coreparserfunctions", v_parser, v_raw); }
  public: static Variant t_namespacee(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_namespacee("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_subjectpagenamee(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_subjectpagenamee("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_lc(CVarRef v_parser, CVarRef v_s = "") { return ti_lc("coreparserfunctions", v_parser, v_s); }
  public: static Variant t_fullurle(CVarRef v_parser, CVarRef v_s = "", CVarRef v_arg = null_variant) { return ti_fullurle("coreparserfunctions", v_parser, v_s, v_arg); }
  public: static Variant t_talkspace(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_talkspace("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_numberingroup(CVarRef v_parser, CVarRef v_name = "", CVarRef v_raw = null_variant) { return ti_numberingroup("coreparserfunctions", v_parser, v_name, v_raw); }
  public: static Variant t_numberofactiveusers(CVarRef v_parser, CVarRef v_raw = null_variant) { return ti_numberofactiveusers("coreparserfunctions", v_parser, v_raw); }
  public: static Variant t_pagesize(CVarRef v_parser, CVarRef v_page = "", CVarRef v_raw = null_variant) { return ti_pagesize("coreparserfunctions", v_parser, v_page, v_raw); }
  public: static Variant t_namespace(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_namespace("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_israw(CVarRef v_param) { return ti_israw("coreparserfunctions", v_param); }
  public: static Variant t_basepagename(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_basepagename("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_subpagenamee(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_subpagenamee("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_ns(CVarRef v_parser, CVarRef v_part1 = "") { return ti_ns("coreparserfunctions", v_parser, v_part1); }
  public: static Variant t_intfunction(int num_args, CVarRef v_parser, CVarRef v_part1 = "", Array args = Array()) { return ti_intfunction("coreparserfunctions", num_args, v_parser, v_part1, args); }
  public: static Variant t_grammar(CVarRef v_parser, CVarRef v_case = "", CVarRef v_word = "") { return ti_grammar("coreparserfunctions", v_parser, v_case, v_word); }
  public: static Variant t_tagobj(CVarRef v_parser, CVarRef v_frame, CVarRef v_args) { return ti_tagobj("coreparserfunctions", v_parser, v_frame, v_args); }
  public: static Variant t_pad(CVarRef v_string, CVarRef v_length, CVarRef v_padding = "0", int64 v_direction = 1LL /* STR_PAD_RIGHT */) { return ti_pad("coreparserfunctions", v_string, v_length, v_padding, v_direction); }
  public: static Variant t_uc(CVarRef v_parser, CVarRef v_s = "") { return ti_uc("coreparserfunctions", v_parser, v_s); }
  public: static Variant t_formatnum(CVarRef v_parser, CVarRef v_num = "", CVarRef v_raw = null_variant) { return ti_formatnum("coreparserfunctions", v_parser, v_num, v_raw); }
  public: static Variant t_fullurl(CVarRef v_parser, CVarRef v_s = "", CVarRef v_arg = null_variant) { return ti_fullurl("coreparserfunctions", v_parser, v_s, v_arg); }
  public: static Variant t_special(CVarRef v_parser, CVarRef v_text) { return ti_special("coreparserfunctions", v_parser, v_text); }
  public: static Variant t_formatraw(CVarRef v_num, CVarRef v_raw) { return ti_formatraw("coreparserfunctions", v_num, v_raw); }
  public: static String t_protectionlevel(CVarRef v_parser, CVarRef v_type = "") { return ti_protectionlevel("coreparserfunctions", v_parser, v_type); }
  public: static Variant t_anchorencode(CVarRef v_parser, CVarRef v_text) { return ti_anchorencode("coreparserfunctions", v_parser, v_text); }
  public: static Variant t_subjectspacee(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_subjectspacee("coreparserfunctions", v_parser, v_title); }
  public: static void t_register(p_parser v_parser) { ti_register("coreparserfunctions", v_parser); }
  public: static Variant t_subjectpagename(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_subjectpagename("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_subpagename(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_subpagename("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_numberoffiles(CVarRef v_parser, CVarRef v_raw = null_variant) { return ti_numberoffiles("coreparserfunctions", v_parser, v_raw); }
  public: static Variant t_numberofadmins(CVarRef v_parser, CVarRef v_raw = null_variant) { return ti_numberofadmins("coreparserfunctions", v_parser, v_raw); }
  public: static Variant t_localurl(CVarRef v_parser, CVarRef v_s = "", CVarRef v_arg = null_variant) { return ti_localurl("coreparserfunctions", v_parser, v_s, v_arg); }
  public: static Variant t_pagename(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_pagename("coreparserfunctions", v_parser, v_title); }
  public: static String t_displaytitle(CVarRef v_parser, CVarRef v_text = "") { return ti_displaytitle("coreparserfunctions", v_parser, v_text); }
  public: static Variant t_padright(CVarRef v_parser, CVarRef v_string = "", CVarRef v_length = 0LL, CVarRef v_padding = "0") { return ti_padright("coreparserfunctions", v_parser, v_string, v_length, v_padding); }
  public: static Variant t_language(CVarRef v_parser, CVarRef v_arg = "") { return ti_language("coreparserfunctions", v_parser, v_arg); }
  public: static Variant t_ucfirst(CVarRef v_parser, CVarRef v_s = "") { return ti_ucfirst("coreparserfunctions", v_parser, v_s); }
  public: static Variant t_plural(int num_args, CVarRef v_parser, CVarRef v_text = "", Array args = Array()) { return ti_plural("coreparserfunctions", num_args, v_parser, v_text, args); }
  public: static String t_urlencode(CVarRef v_parser, CVarRef v_s = "") { return ti_urlencode("coreparserfunctions", v_parser, v_s); }
  public: static Variant t_talkpagename(CVarRef v_parser, CVarRef v_title = null_variant) { return ti_talkpagename("coreparserfunctions", v_parser, v_title); }
  public: static Variant t_pagesinnamespace(CVarRef v_parser, CVarRef v_namespace = 0LL, CVarRef v_raw = null_variant) { return ti_pagesinnamespace("coreparserfunctions", v_parser, v_namespace, v_raw); }
  public: static Variant t_filepath(CVarRef v_parser, CVarRef v_name = "", CVarRef v_option = "") { return ti_filepath("coreparserfunctions", v_parser, v_name, v_option); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_coreparserfunctions_h__
