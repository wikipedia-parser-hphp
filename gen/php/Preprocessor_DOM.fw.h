
#ifndef __GENERATED_php_Preprocessor_DOM_fw_h__
#define __GENERATED_php_Preprocessor_DOM_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(ppdpart)
FORWARD_DECLARE_CLASS(ppdstackelement)
FORWARD_DECLARE_CLASS(ppnode_dom)
FORWARD_DECLARE_CLASS(ppdstack)
FORWARD_DECLARE_CLASS(preprocessor_dom)
extern const int64 q_preprocessor_dom_CACHE_VERSION;
FORWARD_DECLARE_CLASS(ppframe_dom)
FORWARD_DECLARE_CLASS(ppcustomframe_dom)
FORWARD_DECLARE_CLASS(pptemplateframe_dom)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/Parser.fw.h>
#include <php/Preprocessor.fw.h>

#endif // __GENERATED_php_Preprocessor_DOM_fw_h__
