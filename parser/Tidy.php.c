// BEGIN INCLUDED FILES
/* The difference between zend_symtable_X and zend_hash_X is that
 * the _symtable version will check if the key is a string of an
 * integer, and if so, use the int version instead. We can use the
 * zend_hash_X versions safely for symbol tables, since
 * variables cant be integer strings, but we cant safely use
 * zend_hash_X versions for hashtable lookups. Well-named, they are.
 */

/* An important note of zval*s and zval**s. Frequently, zval** are
 * fetched from arrays and symbol tables. The zval** will point into
 * the array, and so updating it will update the relevant array
 * entry. It is normally not the same to dereference it to a local
 * variable, and pass a reference to that.
 */



// Some common functions
#include "php.h"

/*
 * The runtime needs its own initialization and finalization.  phc is
 * responsible for generating calls to these functions.
 */

void
init_runtime ()
{
}

void
finalize_runtime ()
{
}

static void
ht_debug (HashTable * ht)
{
  printf ("\nHASH\n");
  if (ht == NULL)
    {
      printf ("NULL\n");
      return;
    }
  for (zend_hash_internal_pointer_reset (ht);
       zend_hash_has_more_elements (ht) == SUCCESS;
       zend_hash_move_forward (ht))
    {
      char *key;
      unsigned keylen;
      unsigned long idx;
      int type;
      zval **ppzval;
      zval *zvp;

      type = zend_hash_get_current_key_ex (ht, &key, &keylen, &idx, 0, NULL);
      zend_hash_get_current_data (ht, (void **) &ppzval);

      zvp = *ppzval;

      if (type == HASH_KEY_IS_STRING)
	{
	  printf ("%s", key);
	}
      else
	{
	  printf ("%ld", idx);
	}

      printf (": addr = %08lX, refcount = %d, is_ref = %d ",
	      (long unsigned int) (*ppzval), (*ppzval)->refcount,
	      (*ppzval)->is_ref);
      switch (Z_TYPE_P (zvp))
	{
	case IS_NULL:
	  printf ("(NULL)");
	  break;
	case IS_LONG:
	  printf ("(%ldL)", Z_LVAL_P (zvp));
	  break;
	case IS_DOUBLE:
	  printf ("(%lff)", Z_DVAL_P (zvp));
	  break;
	case IS_BOOL:
	  printf (Z_BVAL_P (zvp) ? "(true)" : "(false)");
	  break;
	case IS_ARRAY:
	  printf ("(array(%d))", Z_ARRVAL_P (zvp)->nNumOfElements);
	  break;
	case IS_OBJECT:
	  printf ("(Object)");
	  break;
	case IS_STRING:
	  printf ("(\"%s\")", Z_STRVAL_P (zvp));
	  break;
	case IS_RESOURCE:
	  printf ("(Resource)");
	  break;
	default:
	  printf ("(Invalid: %d)", Z_TYPE_P (zvp));
	}

      printf ("\n");

    }
  printf ("END HASH\n");
}

// Call ht_debug on the named var in the given symbol table
static void
ht_var_debug (HashTable * st, char *name)
{
  zval **p_zvp;
  if (zend_symtable_find (st, name, strlen (name) + 1,
			  (void **) &p_zvp) != SUCCESS)
    {
      printf ("VAR NOT IN SYMBOL TABLE: '%s'\n", name);
      return;
    }

  if (Z_TYPE_P (*p_zvp) != IS_ARRAY)
    {
      printf ("NOT HASH\n");
      return;
    }

  ht_debug ((*p_zvp)->value.ht);
}

static zval* counters;

static void init_counters ()
{
  ALLOC_INIT_ZVAL (counters);
  array_init (counters);
}

// Dump and cleanup memory
static void finalize_counters ()
{
  HashTable* ht = Z_ARRVAL_P (counters);
  for (zend_hash_internal_pointer_reset (ht);
       zend_hash_has_more_elements (ht) == SUCCESS;
       zend_hash_move_forward (ht))
    {
      char *key;
      zval **p_zvp;

      zend_hash_get_current_key_ex (ht, &key, NULL, NULL, 0, NULL);
      zend_hash_get_current_data (ht, (void **) &p_zvp);

      fprintf (stderr, "COUNTER:%s:%ld\n", key, Z_LVAL_P (*p_zvp));
    }

  zval_ptr_dtor (&counters);
}

static void increment_counter (char* name, int length, ulong hashval)
{
  zval** p_zvp;
  int success = zend_hash_quick_find (Z_ARRVAL_P (counters),
				      name,
				      length,
				      hashval,
				      (void **) &p_zvp);

  if (success == SUCCESS)
    {
      Z_LVAL_PP (p_zvp)++;
    }
  else
    {

      zval* new_val;
      ALLOC_INIT_ZVAL (new_val);
      ZVAL_LONG (new_val, 1);

      zend_hash_quick_add (Z_ARRVAL_P (counters),
			   name,
			   length,
			   hashval,
			   &new_val,
			   sizeof (zval *),
			   NULL);
    }
}



/* Make a copy of *P_ZVP, storing it in *P_ZVP. */
static zval *
zvp_clone_ex (zval * zvp)
{
  // TODO: use INIT_PZVAL_COPY
  zval *clone;
  MAKE_STD_ZVAL (clone);
  clone->value = zvp->value;
  clone->type = zvp->type;
  zval_copy_ctor (clone);
  return clone;
}


static inline int
in_copy_on_write (zval * zvp)
{
  return (zvp->refcount > 1 && !zvp->is_ref);
}

static inline int
in_change_on_write (zval * zvp)
{
  return (zvp->refcount > 1 && zvp->is_ref);
}

/* If *P_ZVP is in a copy-on-write set, separate it by overwriting
 * *P_ZVP with a clone of itself, and lowering the refcount on the
 * original. */
static void
sep_copy_on_write (zval ** p_zvp)
{
  if (!in_copy_on_write (*p_zvp))
    return;

  zval *old = *p_zvp;

  *p_zvp = zvp_clone_ex (*p_zvp);

  zval_ptr_dtor (&old);
}

/* If *P_ZVP is in a copy-on-write set, separate it by overwriting
 * *P_ZVP with a clone of itself, and lowering the refcount on the
 * original. */
static void
sep_change_on_write (zval ** p_zvp)
{
  assert (in_change_on_write (*p_zvp));

  zval *old = *p_zvp;

  *p_zvp = zvp_clone_ex (*p_zvp);

  zval_ptr_dtor (&old);
}

/* Assign RHS into LHS, by reference. After this, LHS will point to the same
 * zval* as RHS. */
static void
copy_into_ref (zval ** lhs, zval ** rhs)
{
  (*rhs)->is_ref = 1;
  (*rhs)->refcount++;
  zval_ptr_dtor (lhs);
  *lhs = *rhs;
}


// Overwrite one zval with another
static void
overwrite_lhs (zval * lhs, zval * rhs)
{
  // First, call the destructor to remove any data structures
  // associated with lhs that will now be overwritten
  zval_dtor (lhs);
  // Overwrite LHS
  lhs->value = rhs->value;
  lhs->type = rhs->type;
  zval_copy_ctor (lhs);
}

// Overwrite one zval with another
static void
overwrite_lhs_no_copy (zval * lhs, zval * rhs)
{
  // First, call the destructor to remove any data structures
  // associated with lhs that will now be overwritten
  zval_dtor (lhs);
  // Overwrite LHS
  lhs->value = rhs->value;
  lhs->type = rhs->type;
}

/* Write P_RHS into the symbol table as a variable named VAR_NAME. */
// NOTE: We do not alter p_rhs's refcount, unless p_lhs joins its
// Copy-on-write set.
// TODO: this is crying out to be inlined.
static void
write_var (zval ** p_lhs, zval * rhs)
{
  if (!(*p_lhs)->is_ref)
    {
      zval_ptr_dtor (p_lhs);
      // Take a copy of RHS for LHS.
      if (rhs->is_ref)
	{
	  *p_lhs = zvp_clone_ex (rhs);
	}
      else			// share a copy
	{
	  rhs->refcount++;
	  *p_lhs = rhs;
	}
    }
  else
    {
      overwrite_lhs (*p_lhs, rhs);
    }
}

// TODO: this functino does too much, and much might be redundant
static zval **
get_st_entry (HashTable * st, char *name, int length, ulong hashval TSRMLS_DC)
{
  zval **p_zvp;
  if (zend_hash_quick_find
      (st, name, length, hashval, (void **) &p_zvp) == SUCCESS)
    {
      assert (p_zvp != NULL);
      return p_zvp;
    }

  // If we dont find it, put EG (uninitialized_zval_ptr) into the
  // hashtable, and return a pointer to its container.
  EG (uninitialized_zval_ptr)->refcount++;
  int result = zend_hash_quick_add (st, name, length, hashval,
				    &EG (uninitialized_zval_ptr),
				    sizeof (zval *), (void **) &p_zvp);
  assert (result == SUCCESS);
  assert (p_zvp != NULL);

  return p_zvp;
}

/* Read the variable named VAR_NAME from the local symbol table and
 * return it. If the variable doent exist, a new one is created and
 * *IS_NEW is set.  */
static zval *
read_var (HashTable * st, char *name, int length, ulong hashval TSRMLS_DC)
{
  zval **p_zvp;
  if (zend_hash_quick_find
      (st, name, length, hashval, (void **) &p_zvp) == SUCCESS)
    return *p_zvp;

  return EG (uninitialized_zval_ptr);
}

static long
get_integer_index (zval * ind TSRMLS_DC)
{
  long index;
  switch (Z_TYPE_P (ind))
    {
    case IS_DOUBLE:
      return (long) Z_DVAL_P (ind);

    case IS_LONG:
    case IS_BOOL:
      return Z_LVAL_P (ind);

    case IS_NULL:
      return 0;

    default:
      php_error_docref (NULL TSRMLS_CC, E_WARNING, "Illegal offset type");
    }
}

static zval *
read_string_index (zval * var, zval * ind TSRMLS_DC)
{
  // This must always allocate memory, since we cant return the
  // passed string.
  assert (Z_TYPE_P (var) == IS_STRING);
  long index = get_integer_index (ind TSRMLS_CC);

  zval *result;
  ALLOC_INIT_ZVAL (result);

  if (index >= Z_STRLEN_P (var) || index < 0)
    {
      // this is 1 byte long, must be copied
      ZVAL_STRINGL (result, "", 0, 1);
    }
  else
    {
      char *string = Z_STRVAL_P (var);
      ZVAL_STRINGL (result, &string[index], 1, 1);
    }

  return result;
}

/* Given a string (p_lhs), write into it for $x[i] = $y; */
void
write_string_index (zval ** p_lhs, zval * ind, zval * rhs TSRMLS_DC)
{
  assert (Z_TYPE_P (*p_lhs) == IS_STRING);

  long index = get_integer_index (ind TSRMLS_CC);

  // Get the appropriate character
  char new_char;
  if (Z_TYPE_P (rhs) != IS_STRING)
    {
      // TODO: remove allocate
      zval *copy = zvp_clone_ex (rhs);
      convert_to_string (copy);
      new_char = Z_STRVAL_P (copy)[0];
      zval_ptr_dtor (&copy);
    }
  else
    {
      new_char = Z_STRVAL_P (rhs)[0];
    }

  // Bounds check
  if (index < 0)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Illegal string offset:  %ld", index);
      return;
    }

  // We overwrite if it's change-on-write
  sep_copy_on_write (p_lhs);

  if (index > Z_STRLEN_PP (p_lhs))
    {
      // Extend to fix new
      int len = Z_STRLEN_PP (p_lhs);
      int new_length = index + 1;	// space for the new character
      Z_STRVAL_PP (p_lhs) = erealloc (Z_STRVAL_PP (p_lhs), new_length + 1);

      // pad with ' '
      memset (&Z_STRVAL_PP (p_lhs)[len], ' ', index - len);

      // change the strlen
      Z_STRLEN_PP (p_lhs) = new_length;

      // add a null terminator
      Z_STRVAL_PP (p_lhs)[new_length] = '\0';
    }

  // write in the first character of the new value
  Z_STRVAL_PP (p_lhs)[index] = new_char;


  // index < 0: E_WARNING illegal string offset
}

// Extract the hashtable from a hash-valued zval
static HashTable *
extract_ht_ex (zval * arr TSRMLS_DC)
{
  // TODO: this likely should be inlined somewhere.
  assert (!in_copy_on_write (arr));
  if (Z_TYPE_P (arr) == IS_NULL)
    {
      array_init (arr);
    }
  else if (Z_TYPE_P (arr) != IS_ARRAY)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an array");
      array_init (arr);
    }
  return Z_ARRVAL_P (arr);
}


/* P_VAR points into a symbol table, at a variable which we wish to index as a hashtable. */
static HashTable *
extract_ht (zval ** p_var TSRMLS_DC)
{
  sep_copy_on_write (p_var);

  return extract_ht_ex (*p_var TSRMLS_CC);
}

/* Using IND as a key to HT, call the appropriate zend_index_X
 * function with data as a parameter, and return its result. This
 * updates the zval** pointed to by DATA. */
static int
ht_find (HashTable * ht, zval * ind, zval *** data)
{
  int result;
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      result = zend_hash_index_find (ht, Z_LVAL_P (ind), (void **) data);
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      result = zend_hash_index_find (ht, (long) Z_DVAL_P (ind),
				     (void **) data);
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      result = zend_hash_find (ht, "", sizeof (""), (void **)data);
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      result = zend_symtable_find (ht, Z_STRVAL_P (ind),
				   Z_STRLEN_P (ind) + 1, (void **) data);
    }
  else
    {
      // TODO: I believe this might need a warning.

      // TODO avoid alloc
      // use a string index for other types
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);

      result = zend_symtable_find (ht, Z_STRVAL_P (string_index),
				   Z_STRLEN_P (string_index) + 1,
				   (void **) data);
      zval_ptr_dtor (&string_index);
    }
  return result;
}


static int
check_array_index_type (zval * ind TSRMLS_DC)
{
  if (Z_TYPE_P (ind) == IS_OBJECT || Z_TYPE_P (ind) == IS_ARRAY)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING, "Illegal offset type");
      return 0;
    }

  return 1;
}

// Update a hashtable using a zval* index
static void
ht_update (HashTable * ht, zval * ind, zval * val, zval *** dest)
{
  int result;
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      result = zend_hash_index_update (ht, Z_LVAL_P (ind), &val,
				       sizeof (zval *), (void **) dest);
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      result = zend_hash_index_update (ht, (long) Z_DVAL_P (ind),
				       &val, sizeof (zval *), (void **) dest);
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      result = zend_hash_update (ht, "", sizeof (""), &val,
				 sizeof (zval *), (void **) dest);
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      result = zend_symtable_update (ht, Z_STRVAL_P (ind),
				     Z_STRLEN_P (ind) + 1,
				     &val, sizeof (zval *), (void **) dest);
    }
  else
    {
      // TODO avoid alloc
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);
      result = zend_symtable_update (ht, Z_STRVAL_P (string_index),
				     Z_STRLEN_P (string_index) + 1,
				     &val, sizeof (zval *), (void **) dest);

      zval_ptr_dtor (&string_index);
    }
  assert (result == SUCCESS);
}

// Delete from a hashtable using a zval* index
static void
ht_delete (HashTable * ht, zval * ind)
{
  // This may fail if the index doesnt exist, which is fine.
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      zend_hash_index_del (ht, Z_LVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      zend_hash_index_del (ht, (long) Z_DVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      zend_hash_del (ht, "", sizeof (""));
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      zend_hash_del (ht, Z_STRVAL_P (ind), Z_STRLEN_P (ind) + 1);
    }
  else
    {
      // TODO avoid alloc
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);
      zend_hash_del (ht, Z_STRVAL_P (string_index),
		     Z_STRLEN_P (string_index) + 1);

      zval_ptr_dtor (&string_index);
    }
}

// Check if a key exists in a hashtable 
static int
ht_exists (HashTable * ht, zval * ind)
{
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      return zend_hash_index_exists (ht, Z_LVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      return zend_hash_index_exists (ht, (long) Z_DVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      return zend_hash_exists (ht, "", sizeof (""));
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      return zend_hash_exists (ht, Z_STRVAL_P (ind), Z_STRLEN_P (ind) + 1);
    }
  else
    {
      // TODO avoid alloc
      int result;
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);
      result = zend_hash_exists (ht, Z_STRVAL_P (string_index),
				 Z_STRLEN_P (string_index) + 1);
      zval_ptr_dtor (&string_index);
      return result;
    }
  assert (0);
}

static zval **
get_ht_entry (zval ** p_var, zval * ind TSRMLS_DC)
{
  if (Z_TYPE_P (*p_var) == IS_STRING)
    {
      if (Z_STRLEN_PP (p_var) > 0)
	{
	  php_error_docref (NULL TSRMLS_CC, E_ERROR,
			    "Cannot create references to/from string offsets nor overloaded objects");
	}
    }

  if (Z_TYPE_P (*p_var) != IS_ARRAY)
    {
      zval_ptr_dtor (p_var);
      ALLOC_INIT_ZVAL (*p_var);
      array_init (*p_var);
    }

  HashTable *ht = extract_ht (p_var TSRMLS_CC);

  zval **data;
  if (ht_find (ht, ind, &data) == SUCCESS)
    {
      assert (data != NULL);
      return data;
    }

  // If we dont find it, put EG (uninitialized_zval_ptr) into the
  // hashtable, and return a pointer to its container.
  EG (uninitialized_zval_ptr)->refcount++;
  ht_update (ht, ind, EG (uninitialized_zval_ptr), &data);

  assert (data != NULL);

  return data;
}


// Like extract_ht_ex, but for objects 
static HashTable *
extract_field_ex (zval * obj TSRMLS_DC)
{
  // TODO: this likely should be inlined somewhere.
  assert (!in_copy_on_write (obj));
  if (Z_TYPE_P (obj) == IS_NULL)
    {
      assert (0);
      // TODO: implement initialization
    }
  else if (Z_TYPE_P (obj) != IS_OBJECT)
    {
      // TODO: test if this is the right error message
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an object");
      // TODO: implement initialization
      assert (0);
    }
  return Z_OBJPROP_P (obj);
}

// Like extract_ht, but for objects
static HashTable *
extract_field (zval ** p_var TSRMLS_DC)
{
  sep_copy_on_write (p_var);

  return extract_field_ex (*p_var TSRMLS_CC);
}

// Like get_ht_entry, but for objects
static zval **
get_field (zval ** p_var, char *ind TSRMLS_DC)
{
  if (Z_TYPE_P (*p_var) != IS_OBJECT)
    {
      // TODO: implement initialization
      assert (0);
    }

  HashTable *ht = extract_field (p_var TSRMLS_CC);

  zval **data;
  if (zend_symtable_find (ht, ind, strlen (ind) + 1, (void **) &data) ==
      SUCCESS)
    {
      assert (data != NULL);
      return data;
    }

  // If we dont find it, put EG (uninitialized_zval_ptr) into the
  // hashtable, and return a pointer to its container.
  EG (uninitialized_zval_ptr)->refcount++;
  zend_symtable_update (ht, ind, strlen (ind) + 1,
			&EG (uninitialized_zval_ptr), sizeof (zval *),
			(void **) &data);

  assert (data != NULL);

  return data;
}

void
read_array (zval ** result, zval * array, zval * ind TSRMLS_DC)
{
  // Memory can be allocated in read_string_index
  if (array == EG (uninitialized_zval_ptr))
    {
      *result = array;
      return;
    }

  // Since we know its an array, and we dont write to it, we dont need
  // to separate it.
  HashTable *ht = Z_ARRVAL_P (array);

  // find the result
  zval **p_result;
  if (ht_find (ht, ind, &p_result) == SUCCESS)
    {
      *result = *p_result;
      return;
    }

  *result = EG (uninitialized_zval_ptr);
}

/* If its not an array, convert it into an array. */
static void
check_array_type (zval ** p_var TSRMLS_DC)
{
  if ((Z_TYPE_P (*p_var) == IS_BOOL && !Z_BVAL_PP (p_var))
      || Z_TYPE_P (*p_var) == IS_NULL
      || (Z_TYPE_P (*p_var) == IS_STRING && Z_STRLEN_PP (p_var) == 0))
    {
      // Non ref use new values
      if (!PZVAL_IS_REF (*p_var))
	{
	  zval_ptr_dtor (p_var);
	  ALLOC_INIT_ZVAL (*p_var);
	}
      else
	// Refs are just replaced
	zval_dtor (*p_var);

      array_init (*p_var);
    }
  else if (Z_TYPE_PP (p_var) != IS_STRING && Z_TYPE_PP (p_var) != IS_ARRAY)
    {
      // TODO: why are these different types than pushing
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an array");
    }
}

/* If its not an array, convert it into an object. */
static void
check_object_type (zval ** p_var TSRMLS_DC)
{
  // TODO: implement
}

/* Push EG (uninitialized_zval_ptr) and return a pointer into the ht
 * for it */
/*
 * Converted to array automatically:
 *    ""
 *    NULL
 *    false
 *
 * Warning, no conversion:
 *    ints
 *    floats
 *    true
 *
 * Error, no conversion:
 *    strings other than ""
 */
// TODO: objects, resources, etc
static zval **
push_and_index_ht (zval ** p_var TSRMLS_DC)
{
  // Check for errors conditions

  if (Z_TYPE_P (*p_var) == IS_STRING && Z_STRLEN_PP (p_var) > 0)
    {
      php_error_docref (NULL TSRMLS_CC, E_ERROR,
			"[] operator not supported for strings");
      assert (0);		// unreachable
    }

  if (Z_TYPE_P (*p_var) == IS_BOOL && Z_BVAL_PP (p_var)
      || Z_TYPE_P (*p_var) == IS_LONG || Z_TYPE_P (*p_var) == IS_DOUBLE)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an array");
      return NULL;
    }

  if (Z_TYPE_P (*p_var) != IS_ARRAY)
    {
      zval_ptr_dtor (p_var);
      ALLOC_INIT_ZVAL (*p_var);
      array_init (*p_var);
    }

  // if its not an array, make it an array
  HashTable *ht = extract_ht (p_var TSRMLS_CC);
  zval **data;

  EG (uninitialized_zval_ptr)->refcount++;
  int result = zend_hash_next_index_insert (ht, &EG (uninitialized_zval_ptr),
					    sizeof (zval *), (void **) &data);
  assert (result == SUCCESS);

  assert (data);

  return data;
}


/*
 * isset
 */
static int
isset_var (HashTable * st, char *name, int length)
{
  return zend_hash_exists (st, name, length);
}

static int
isset_array (zval ** p_var, zval * ind)
{
  if (Z_TYPE_P (*p_var) == IS_STRING)
    {
      ind = zvp_clone_ex (ind);
      convert_to_long (ind);
      int result = (Z_LVAL_P (ind) >= 0
		    && Z_LVAL_P (ind) < Z_STRLEN_PP (p_var));
      assert (ind->refcount == 1);
      zval_ptr_dtor (&ind);
      return result;
    }

  // NO error required; return false
  if (Z_TYPE_P (*p_var) != IS_ARRAY)
    return 0;

  // if its not an array, make it an array
  HashTable *ht = Z_ARRVAL_P (*p_var);

  zval **data;
  if (ht_find (ht, ind, &data) == SUCCESS)
    {
      return !ZVAL_IS_NULL (*data);
    }
  else
    return 0;
}


static zval **
fetch_var_arg_by_ref (zval ** p_arg)
{
  // We are passing by reference
  sep_copy_on_write (p_arg);

  // We don't need to restore ->is_ref afterwards,
  // because the called function will reduce the
  // refcount of arg on return, and will reset is_ref to
  // 0 when refcount drops to 1.  If the refcount does
  // not drop to 1 when the function returns, but we did
  // set is_ref to 1 here, that means that is_ref must
  // already have been 1 to start with (since if it had
  // not, that means that the variable would have been
  // in a copy-on-write set, and would have been
  // seperated above).
  (*p_arg)->is_ref = 1;

  return p_arg;
}

/* Dont pass-by-ref */
static zval *
fetch_var_arg (zval * arg, int *is_arg_new)
{
  if (arg->is_ref)
    {
      // We dont separate since we don't own one of ARG's references.
      arg = zvp_clone_ex (arg);
      *is_arg_new = 1;

      // It seems we get incorrect refcounts without this.
      // TODO This decreases the refcount to zero, which seems wrong,
      // but gives the right answer. We should look at how zend does
      // this.

      arg->refcount--;
    }
  return arg;
}

// TODO dont overwrite line numbers if we're compiling an extension
static void
phc_setup_error (int init, char *filename, int line_number,
		 zend_function * function TSRMLS_DC)
{
  static int old_in_compilation;
  static int old_in_execution;
  static char *old_filename;
  static int old_lineno;
  static zend_function *old_function;
  if (init)
    {
      if (filename == NULL)
	filename = "[phc_compiled_file]";
      // Save old values
      old_in_compilation = CG (in_compilation);
      old_in_execution = EG (in_execution);
      old_filename = CG (compiled_filename);
      old_lineno = CG (zend_lineno);
      old_function = EG (function_state_ptr)->function;
      // Put in our values
      CG (in_compilation) = 1;
      EG (in_execution) = 1;
      CG (compiled_filename) = filename;
      CG (zend_lineno) = line_number;
      if (function)
	EG (function_state_ptr)->function = function;
    }
  else
    {
      CG (in_compilation) = old_in_compilation;
      EG (in_execution) = old_in_execution;
      CG (compiled_filename) = old_filename;
      CG (zend_lineno) = old_lineno;
      EG (function_state_ptr)->function = old_function;
    }
}

static void
initialize_function_call (zend_fcall_info * fci, zend_fcall_info_cache * fcic,
			  char *function_name, char *filename,
			  int line_number TSRMLS_DC)
{
  if (fcic->initialized)
    return;

  zval fn;
  INIT_PZVAL (&fn);
  ZVAL_STRING (&fn, function_name, 0);
  int result = zend_fcall_info_init (&fn, fci, fcic TSRMLS_CC);
  if (result != SUCCESS)
    {
      phc_setup_error (1, filename, line_number, NULL TSRMLS_CC);
      php_error_docref (NULL TSRMLS_CC, E_ERROR,
			"Call to undefined function %s()", function_name);
    }
}

/*
 * Initialize zend_fcall_info for a method lookup
 *
 * Implementation partly based on zend_call_method in Zend/zend_interfaces.c
 * Main difference is that we use Z_OBJ_HTT_PP(obj)->get_method to retrieve
 * the function handler for the method instead of looking it up directly;
 * this means that we correctly deal with __call.
 */

static void
initialize_method_call (zend_fcall_info * fci, zend_fcall_info_cache * fcic,
			zval ** obj, char *function_name,
			char *filename, int line_number TSRMLS_DC)
{
  if (fcic->initialized)
    return;

  zend_class_entry *obj_ce;
  obj_ce = Z_OBJCE_PP (obj);

  /*
   * we do not initialize fci.
   *   function_table  --  not initialized by zend_call_method
   *   function_name   --  zend_call_method initializes this to a pointer to
   *                       a zval 'z_fname', but does not initialize z_fname
   *                       in case of a method invocation
   *   retval_ptr_ptr  --  should be initialized by caller
   *   param_count     --  should be initialized by caller
   *   params          --  should be initialized by caller
   */
  fci->size = sizeof (*fci);
  fci->object_pp = obj;
  fci->no_separation = 1;
  fci->symbol_table = NULL;

  fcic->initialized = 1;
  fcic->calling_scope = obj_ce;
  fcic->object_pp = obj;
  fcic->function_handler
    = Z_OBJ_HT_PP (obj)->get_method (obj,
				     function_name,
				     strlen (function_name) TSRMLS_CC);

  if (fcic->function_handler == NULL)
    {
      phc_setup_error (1, filename, line_number, NULL TSRMLS_CC);
      php_error_docref (NULL TSRMLS_CC, E_ERROR,
			"Call to undefined method %s::%s",
			obj_ce->name, function_name);
    }
}

/*
 * Like initialize_method_call, but return 0 if no constructor is defined
 * rather than giving an error.
 */

static int 
initialize_constructor_call (zend_fcall_info * fci,
			     zend_fcall_info_cache * fcic, zval ** obj,
			     char *filename, int line_number TSRMLS_DC)
{
  if (fcic->initialized)
    return;

  zend_class_entry *obj_ce;
  obj_ce = Z_OBJCE_PP (obj);

  /*
   * we do not initialize fci.
   *   function_table  --  not initialized by zend_call_method
   *   function_name   --  zend_call_method initializes this to a pointer to
   *                       a zval 'z_fname', but does not initialize z_fname
   *                       in case of a method invocation
   *   retval_ptr_ptr  --  should be initialized by caller
   *   param_count     --  should be initialized by caller
   *   params          --  should be initialized by caller
   */
  fci->size = sizeof (*fci);
  fci->object_pp = obj;
  fci->no_separation = 1;
  fci->symbol_table = NULL;

  fcic->initialized = 1;
  fcic->calling_scope = obj_ce;
  fcic->object_pp = obj;
  fcic->function_handler
    = Z_OBJ_HT_PP (obj)->get_constructor (*obj TSRMLS_CC);

  return (fcic->function_handler != NULL);
}
// vi:set ts=8:

/*
 * Creates a copy of *in using persistent memory, optionally destroying *in
 *
 * Does not work for objects/resources and will loop on self-recursive arrays.
 */

zval *
persistent_clone (zval * in, int destroy_in TSRMLS_DC)
{
  zval *out = pemalloc (sizeof (zval), 1);
  *out = *in;

  switch (Z_TYPE_P (in))
    {
    case IS_NULL:
    case IS_LONG:
    case IS_DOUBLE:
    case IS_BOOL:
      /* nothing more to be done */
      break;
    case IS_STRING:
      Z_STRVAL_P (out) = pemalloc (Z_STRLEN_P (in) + 1, 1);
      memcpy (Z_STRVAL_P (out), Z_STRVAL_P (in), Z_STRLEN_P (in) + 1);
      break;
    case IS_ARRAY:
      {
	HashTable *old_arr = Z_ARRVAL_P (in);
	HashTable *new_arr = pemalloc (sizeof (HashTable), 1);
	zend_hash_init (new_arr, old_arr->nNumOfElements, NULL, ZVAL_PTR_DTOR,
			/* persistent */ 1);

	for (zend_hash_internal_pointer_reset (old_arr);
	     zend_hash_has_more_elements (old_arr) == SUCCESS;
	     zend_hash_move_forward (old_arr))
	  {
	    char *key;
	    uint keylen;
	    ulong idx;
	    int type;
	    zval **old_elem, *new_elem;

	    type =
	      zend_hash_get_current_key_ex (old_arr, &key, &keylen, &idx, 0,
					    NULL);
	    assert (zend_hash_get_current_data
		    (old_arr, (void **) &old_elem) == SUCCESS);

	    new_elem = persistent_clone (*old_elem, destroy_in TSRMLS_CC);

	    if (type == HASH_KEY_IS_STRING)
	      zend_hash_add (new_arr, key, keylen, &new_elem, sizeof (zval *),
			     NULL);
	    else
	      zend_hash_index_update (new_arr, idx, &new_elem,
				      sizeof (zval *), NULL);

	  }

	Z_ARRVAL_P (out) = new_arr;
      }
      break;
    default:
      /* other types are not supported */
      assert (0);
    }

  zval_ptr_dtor (&in);
  return out;
}

/*
 * Wrapper around zend_declare_property which 
 *
 * - Asserts that the ZEND_INTERNAL_CLASS flag is cleared
 *   (otherwise we cannot add complex (i.e., array) properties)
 * - Creates a persistent clone of the property to be added before
 *   calling zend_declare_property, since the memory for this property
 *   should only be deallocated when the module is shut down
 *   (and not when the request finishes)
 * - Cleans up after zend_declare_property by re-allocating the name of 
 *   the property using persistent memory, for much the same reason
 */

static int
phc_declare_property (zend_class_entry * ce, char *name, int name_length,
		      zval * property, int access_type TSRMLS_DC)
{
  assert (!(ce->type & ZEND_INTERNAL_CLASS));
  assert (zend_declare_property
	  (ce, name, name_length, persistent_clone (property, 1 TSRMLS_CC),
	   access_type TSRMLS_CC) == SUCCESS);

  zend_property_info *property_info;
  assert (zend_hash_find
	  (&ce->properties_info, name, name_length + 1,
	   (void **) &property_info) == SUCCESS);
  efree (property_info->name);
  property_info->name = name;

  return SUCCESS;
}

// vi:set ts=8:


static void
cast_var (zval ** p_zvp, int type)
{
  assert (type >= 0 && type <= 6);
  if ((*p_zvp)->type == type)
    return;

  sep_copy_on_write (p_zvp);
  zval *zvp = *p_zvp;

  switch (type)
    {
    case IS_NULL:
      convert_to_null (zvp);
      break;
    case IS_BOOL:
      convert_to_boolean (zvp);
      break;
    case IS_LONG:
      convert_to_long (zvp);
      break;
    case IS_DOUBLE:
      convert_to_double (zvp);
      break;
    case IS_STRING:
      convert_to_string (zvp);
      break;
    case IS_ARRAY:
      convert_to_array (zvp);
      break;
    case IS_OBJECT:
      convert_to_object (zvp);
      break;
    default:
      assert (0);		// TODO unimplemented
      break;
    }
}

/* Copies a constant into ZVP. Note that LENGTH does not include the NULL-terminating byte. */
static void
get_constant (char *name, int length, zval ** p_zvp TSRMLS_DC)
{
  MAKE_STD_ZVAL (*p_zvp);
  // zend_get_constant returns 1 for success, not SUCCESS
  int result = zend_get_constant (name, length, *p_zvp TSRMLS_CC);
  if (result == 0)
    ZVAL_STRINGL (*p_zvp, name, length, 1);
}

/* The function call mechanism deals specially with EG(uninitialize_zval_ptr)
 * (or sometime EG(uninitialize_zval)), so we need to use this too. This
 * particular zval can also be set, but there is an implicit guarantee 
 * of the information below.
 *
 * If assertions are off, this should be inlined to nothing.
 */
static void
phc_check_invariants (TSRMLS_D)
{
  assert (EG (uninitialized_zval_ptr) == &EG (uninitialized_zval));
  assert (EG (uninitialized_zval).refcount >= 1);
  assert (EG (uninitialized_zval).value.lval == 0);
  assert (EG (uninitialized_zval).type == IS_NULL);
  assert (EG (uninitialized_zval).is_ref == 0);
}


static int
check_unset_index_type (zval * ind TSRMLS_DC)
{
  if (Z_TYPE_P (ind) == IS_OBJECT || Z_TYPE_P (ind) == IS_ARRAY)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Illegal offset type in unset");
      return 0;
    }

  return 1;
}



/*
 * unset
 */

static void
unset_var (HashTable * st, char *name, int length)
{
  zend_hash_del (st, name, length);
}

static void
unset_array (zval ** p_var, zval * ind TSRMLS_DC)
{
  // NO error required
  if (Z_TYPE_PP (p_var) != IS_ARRAY)
    {
      if (Z_TYPE_PP (p_var) == IS_STRING)
	{
	  php_error_docref (NULL TSRMLS_CC, E_ERROR,
			    "Cannot unset string offsets");
	}
      else if (Z_TYPE_PP (p_var) != IS_NULL)
	{
	  php_error_docref (NULL TSRMLS_CC, E_WARNING,
			    "Cannot unset offsets in a non-array variable");
	}

      return;
    }

  // if its not an array, make it an array
  HashTable *ht = Z_ARRVAL_P (*p_var);

  ht_delete (ht, ind);
}

/*
 * Lookup variable whose name is var_var in st. We do not call
 * ht_find because ht_find uses zend_symtable_find to search for strings
 * rather than zend_hash_find. The difference is that zend_symtable_find
 * will convert strings to integers where possible: arrays are always
 * integer-indexed if at all possible. Variable names however should
 * _always_ be treated as strings.
 * 
 */

/*
 * If the parameter is a string, returns the parameter, with the refcount
 * incremented. If its not a string, returns a new zval, with a refcount of
 * 1. Either way, zval_dtor_ptr must be run by the caller on the return
 * value.
 */
zval*
get_string_val (zval* zvp)
{
  if (Z_TYPE_P (zvp) == IS_STRING)
    {
      zvp->refcount++;
      return zvp;
    }
  else
    {
      zval* clone = zvp_clone_ex (zvp);
      convert_to_string (clone);
      return clone;
    }
}

zval **
get_var_var (HashTable * st, zval * index TSRMLS_DC)
{
  zval* str_index = get_string_val (index);
  char* name = Z_STRVAL_P (str_index);
  int length = Z_STRLEN_P (str_index) + 1;
  unsigned long hash = zend_get_hash_value (name, length);

  zval** result = get_st_entry (st, name, length, hash TSRMLS_CC);
  zval_ptr_dtor (&str_index);
  return result;
}

/* 
 * Read the variable described by var_var from symbol table st
 * See comments for get_var_var
 */
zval *
read_var_var (HashTable * st, zval * index TSRMLS_DC)
{
  zval* str_index = get_string_val (index);
  char* name = Z_STRVAL_P (str_index);
  int length = Z_STRLEN_P (str_index) + 1;
  unsigned long hash = zend_get_hash_value (name, length);

  zval* result = read_var (st, name, length, hash TSRMLS_CC);
  zval_ptr_dtor (&str_index);
  return result;
}

static void
phc_builtin_eval (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  // If the user wrote "return ..", we need to store the
  // return value; however, in that case, zend_eval_string
  // will slap an extra "return" onto the front of the string,
  // so we must remove the "return" from the string the user
  // wrote. If the user did not write "return", he is not
  // interested in the return value, and we must pass NULL
  // instead or rhs to avoid zend_eval_string adding "return".

  // convert to a string
  // TODO avoid allocation
  zval *copy = zvp_clone_ex (arg);
  convert_to_string (copy);

  if (*p_result && !strncmp (Z_STRVAL_P (copy), "return ", 7))
    {
      zend_eval_string (Z_STRVAL_P (copy) + 7, *p_result,
			"eval'd code" TSRMLS_CC);
    }
  else
    {
      zend_eval_string (Z_STRVAL_P (copy), NULL, "eval'd code" TSRMLS_CC);
    }

  // cleanup
  assert (copy->refcount == 1);
  zval_ptr_dtor (&copy);
}

static void
phc_builtin_exit (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  if (Z_TYPE_P (arg) == IS_LONG)
    EG (exit_status) = Z_LVAL_P (arg);
  else
    zend_print_variable (arg);

  zend_bailout ();
}

static void
phc_builtin_die (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  phc_builtin_exit (arg, p_result, filename TSRMLS_CC);
}

static void
phc_builtin_echo (zval * arg, zval ** p_result TSRMLS_DC)
{
  assert (*p_result == NULL);
  zend_print_variable (arg);
}

static void
phc_builtin_print (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  zval *echo_arg = NULL;
  phc_builtin_echo (arg, &echo_arg TSRMLS_CC);

  if (*p_result)
    ZVAL_LONG (*p_result, 1);
}

// TODO is there a memory leak here is result has a value?
// TOOD isnt this just the same as isset
static void
phc_builtin_empty (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  assert (*p_result);
  ZVAL_BOOL (*p_result, !zend_is_true (arg));
}

// For require, include, require_once and include_once.

// Include:
//    return 1 for success
//    Warning, and return false for failure
// Require:
//    return 1 for success
//    Fail for failure
// Include_once
//    Return true if already included
//    Return 1 for success
//    Warning and return false for failure
// Require_once:
//    Return true if already included
//    return 1 for success
//    Fail for failure
//
static void
include_backend (zval * arg, zval ** p_result, char *filename, int type, int is_once, char* error, char* error_function TSRMLS_DC)
{
  // In the event that the Zend engine cannot find the file, after checking the
  // include path, it tries the current directory. It does this only if the
  // interpreter is executing, and it checks the interpreters opcodes for a
  // filename (see streams/plain_wrapper.c:1352)

  // An alternative is to add the directory to include_path, but its
  // semantically incorrect (get_included_path() would give the wrong answer),
  // and error prone (if people overwrite include_path).
  // TODO: though we could add it for this function only

  assert (EG (active_op_array) == NULL);
  assert (filename != NULL);

  zval *arg_file = arg;
  // Check we have a string
  if (Z_TYPE_P (arg_file) != IS_STRING)
    {
      arg_file = zvp_clone_ex (arg_file);
      convert_to_string (arg_file);
    }

  zend_file_handle handle;
  zend_op_array* new_op_array;
  zend_function zf;

  // Check the _ONCE varieties (based on zend_vm_def.h)
   if (is_once)
     {
       if (IS_ABSOLUTE_PATH (Z_STRVAL_P (arg_file), Z_STRLEN_P (arg_file)))
	 {
	   // Get the proper path name for require
	   cwd_state state;

	   state.cwd_length = 0;
	   state.cwd = malloc(1);
	   state.cwd[0] = 0;
	   int success = !virtual_file_ex(&state, Z_STRVAL_P(arg_file), NULL, 1)
	     && zend_hash_exists(&EG(included_files), state.cwd,
				 state.cwd_length+1);

	   free (state.cwd);

	   if (!success)
	     goto cleanup;
	 }
     }


   // Compile the file
   // Pretend the interpreter is running
   EG (in_execution) = 1;

   int success = zend_stream_open (Z_STRVAL_P (arg_file), &handle TSRMLS_CC);

   // Stop pretending
   EG (in_execution) = 0;
   EG (active_op_array) = NULL;

   if (success != SUCCESS)
     goto fail;


   if (is_once)
     {
       // Check it hadnt been included already
       int once_success = zend_hash_add_empty_element(&EG(included_files),
						      handle.opened_path,
						      strlen (handle.opened_path)+1);
       // Return true 
       if (once_success != SUCCESS)
	 {
	   ZVAL_BOOL (*p_result, 1);
	   goto cleanup;
	 }
     }

   if (!handle.opened_path)
     handle.opened_path = estrndup (Z_STRVAL_P(arg_file), Z_STRLEN_P (arg_file));

   // run it
   success = zend_execute_scripts (type TSRMLS_CC, p_result, 1, &handle);
   assert (success == SUCCESS);
   zend_stream_close (&handle);

   // Success
   if (*p_result)
       ZVAL_LONG (*p_result, 1);


   goto cleanup;


fail:

   php_error_docref (error_function
		     TSRMLS_CC, 
		     (type == ZEND_INCLUDE) ? E_WARNING : E_ERROR,
		     error,
		     php_strip_url_passwd (Z_STRVAL_P (arg_file)),
		     STR_PRINT (PG (include_path)));


   // Failure
   if (*p_result)
     ZVAL_BOOL (*p_result, 0);

cleanup:

   if (handle.opened_path)
     efree (handle.opened_path);
   zend_destroy_file_handle (&handle TSRMLS_CC);


  if (arg != arg_file)
    zval_ptr_dtor (&arg_file);
}

static void
phc_builtin_include (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_INCLUDE,
		    0,
		    "Failed opening '%s' for inclusion (include_path='%s')",
		    "function.include"
		    TSRMLS_CC);
}

static void
phc_builtin_require (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_REQUIRE,
		    0,
		    "Failed opening required '%s' (include_path='%s')",
		    "function.require"
		    TSRMLS_CC);
}

static void
phc_builtin_include_once (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_INCLUDE,
		    1,
		    "Failed opening '%s' for inclusion (include_path='%s')",
		    "function.include_once"
		    TSRMLS_CC);
}

static void
phc_builtin_require_once (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_REQUIRE,
		    1,
		    "Failed opening required '%s' (include_path='%s')",
		    "function.require_once"
		    TSRMLS_CC);
}

// END INCLUDED FILES
int saved_refcount;
static zend_fcall_info fclose_fci;
static zend_fcall_info_cache fclose_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info feof_fci;
static zend_fcall_info_cache feof_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info fgets_fci;
static zend_fcall_info_cache fgets_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info function_exists_fci;
static zend_fcall_info_cache function_exists_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info fwrite_fci;
static zend_fcall_info_cache fwrite_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info is_null_fci;
static zend_fcall_info_cache is_null_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info is_resource_fci;
static zend_fcall_info_cache is_resource_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info mwtidy_execexternaltidy_fci;
static zend_fcall_info_cache mwtidy_execexternaltidy_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info mwtidy_execinternaltidy_fci;
static zend_fcall_info_cache mwtidy_execinternaltidy_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info proc_close_fci;
static zend_fcall_info_cache proc_close_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info proc_open_fci;
static zend_fcall_info_cache proc_open_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info str_replace_fci;
static zend_fcall_info_cache str_replace_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info tidy_get_output_fci;
static zend_fcall_info_cache tidy_get_output_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info wfdebug_fci;
static zend_fcall_info_cache wfdebug_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info wfgetnull_fci;
static zend_fcall_info_cache wfgetnull_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info wfprofilein_fci;
static zend_fcall_info_cache wfprofilein_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info wfprofileout_fci;
static zend_fcall_info_cache wfprofileout_fcic = {0,NULL,NULL,NULL};
// class MWTidy
// {
// 	public static function tidy($text)
// 	{
// 		global $wgTidyInternal;
// 		$TLE11 = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><title>test</title></head><body>';
// 		$TLE12 = ($TLE11 . $text);
// 		$TLE13 = '</body></html>';
// 		$wrappedtext = ($TLE12 . $TLE13);
// 		$TLE14 = '	';
// 		$TLE15 = '&#9;';
// 		$wrappedtext = str_replace($TLE14, $TLE15, $wrappedtext);
// 		if (wgTidyInternal) goto L107 else goto L108;
// 	L107:
// 		$correctedtext = mwtidy::execinternaltidy($wrappedtext);
// 		goto L109;
// 	L108:
// 		$correctedtext = mwtidy::execexternaltidy($wrappedtext);
// 		goto L109;
// 	L109:
// 		$TLE16 = is_null($correctedtext);
// 		if (TLE16) goto L110 else goto L111;
// 	L110:
// 		$TLE17 = 'Tidy error detected!
// ';
// 		wfdebug($TLE17);
// 		$TLE18 = '
// <!-- Tidy found serious XHTML errors -->
// ';
// 		$TLE19 = ($text . $TLE18);
// 		return $TLE19;
// 		goto L112;
// 	L111:
// 		goto L112;
// 	L112:
// 		$TLE20 = '&#9;';
// 		$TLE21 = '	';
// 		$correctedtext = str_replace($TLE20, $TLE21, $correctedtext);
// 		return $correctedtext;
// 	}
// 	public static function checkerrors($text, &$errorStr = NULL)
// 	{
// 		global $wgTidyInternal;
// 		$retval = 0;
// 		if (wgTidyInternal) goto L113 else goto L114;
// 	L113:
// 		$TLE22 = True;
// 		$errorStr = mwtidy::execinternaltidy($text, $TLE22, $retval);
// 		goto L115;
// 	L114:
// 		$TLE23 = True;
// 		$errorStr = mwtidy::execexternaltidy($text, $TLE23, $retval);
// 		goto L115;
// 	L115:
// 		$TLE24 = 0;
// 		$TLE0 = ($retval < $TLE24);
// 		if (TLE0) goto L116 else goto L117;
// 	L116:
// 		$TLE25 = '';
// 		$TEF1 = ($errorStr == $TLE25);
// 		goto L118;
// 	L117:
// 		$TEF1 = $TLE0;
// 		goto L118;
// 	L118:
// 		$TLE2 = (bool) $TEF1;
// 		if (TLE2) goto L119 else goto L120;
// 	L119:
// 		$TEF3 = $TLE2;
// 		goto L121;
// 	L120:
// 		$TLE26 = 0;
// 		$TEF3 = ($retval == $TLE26);
// 		goto L121;
// 	L121:
// 		$TLE27 = (bool) $TEF3;
// 		return $TLE27;
// 	}
// 	private static function execexternaltidy($text, $stderr = False, &$retval = NULL)
// 	{
// 		global $wgTidyConf;
// 		global $wgTidyBin;
// 		global $wgTidyOpts;
// 		$TLE28 = 'MWTidy::execExternalTidy';
// 		wfprofilein($TLE28);
// 		$cleansource = '';
// 		$opts = ' -utf8';
// 		if (stderr) goto L122 else goto L123;
// 	L122:
// 		$TLE29 = 0;
// 		$TLE30 = 'pipe';
// 		$TLE31 = 'r';
// 		unset($TSa32);
// 		$TSa32 = (array) $TSa32;
// 		$TSa32[] = $TLE30;
// 		$TSa32[] = $TLE31;
// 		$TLE33 = 1;
// 		$TLE34 = 'file';
// 		$TLE35 = wfgetnull();
// 		$TLE36 = 'a';
// 		unset($TSa37);
// 		$TSa37 = (array) $TSa37;
// 		$TSa37[] = $TLE34;
// 		$TSa37[] = $TLE35;
// 		$TSa37[] = $TLE36;
// 		$TLE38 = 2;
// 		$TLE39 = 'pipe';
// 		$TLE40 = 'w';
// 		unset($TSa41);
// 		$TSa41 = (array) $TSa41;
// 		$TSa41[] = $TLE39;
// 		$TSa41[] = $TLE40;
// 		unset($TSa42);
// 		$TSa42 = (array) $TSa42;
// 		$TSa42[$TLE29] = $TSa32;
// 		$TSa42[$TLE33] = $TSa37;
// 		$TSa42[$TLE38] = $TSa41;
// 		$descriptorspec = $TSa42;
// 		goto L124;
// 	L123:
// 		$TLE43 = 0;
// 		$TLE44 = 'pipe';
// 		$TLE45 = 'r';
// 		unset($TSa46);
// 		$TSa46 = (array) $TSa46;
// 		$TSa46[] = $TLE44;
// 		$TSa46[] = $TLE45;
// 		$TLE47 = 1;
// 		$TLE48 = 'pipe';
// 		$TLE49 = 'w';
// 		unset($TSa50);
// 		$TSa50 = (array) $TSa50;
// 		$TSa50[] = $TLE48;
// 		$TSa50[] = $TLE49;
// 		$TLE51 = 2;
// 		$TLE52 = 'file';
// 		$TLE53 = wfgetnull();
// 		$TLE54 = 'a';
// 		unset($TSa55);
// 		$TSa55 = (array) $TSa55;
// 		$TSa55[] = $TLE52;
// 		$TSa55[] = $TLE53;
// 		$TSa55[] = $TLE54;
// 		unset($TSa56);
// 		$TSa56 = (array) $TSa56;
// 		$TSa56[$TLE43] = $TSa46;
// 		$TSa56[$TLE47] = $TSa50;
// 		$TSa56[$TLE51] = $TSa55;
// 		$descriptorspec = $TSa56;
// 		goto L124;
// 	L124:
// 		if (stderr) goto L125 else goto L126;
// 	L125:
// 		$TEF4 = 2;
// 		goto L127;
// 	L126:
// 		$TEF4 = 1;
// 		goto L127;
// 	L127:
// 		$readpipe = $TEF4;
// 		unset($TSa57);
// 		$TSa57 = (array) $TSa57;
// 		$pipes = $TSa57;
// 		$TLE58 = 'proc_open';
// 		$TLE59 = function_exists($TLE58);
// 		if (TLE59) goto L151 else goto L152;
// 	L151:
// 		$TLE60 = ' -config ';
// 		$TLE61 = ($wgTidyBin . $TLE60);
// 		$TLE62 = ($TLE61 . $wgTidyConf);
// 		$TLE63 = ' ';
// 		$TLE64 = ($TLE62 . $TLE63);
// 		$TLE65 = ($TLE64 . $wgTidyOpts);
// 		$TLE66 = ($TLE65 . $opts);
// 		$process = proc_open($TLE66, $descriptorspec, $pipes);
// 		$TLE67 = is_resource($process);
// 		if (TLE67) goto L148 else goto L149;
// 	L148:
// 		$TLE68 = 0;
// 		$TLE96 = param_is_ref (NULL, "fwrite", 0);
// 		;
// 		if (TLE96) goto L128 else goto L129;
// 	L128:
// 		$TMIi95 =& $pipes[$TLE68];
// 		goto L130;
// 	L129:
// 		$TMIi95 = $pipes[$TLE68];
// 		goto L130;
// 	L130:
// 		fwrite($TMIi95, $text);
// 		$TLE69 = 0;
// 		$TLE98 = param_is_ref (NULL, "fclose", 0);
// 		;
// 		if (TLE98) goto L131 else goto L132;
// 	L131:
// 		$TMIi97 =& $pipes[$TLE69];
// 		goto L133;
// 	L132:
// 		$TMIi97 = $pipes[$TLE69];
// 		goto L133;
// 	L133:
// 		fclose($TMIi97);
// 	L144:
// 		$TLE100 = param_is_ref (NULL, "feof", 0);
// 		;
// 		if (TLE100) goto L134 else goto L135;
// 	L134:
// 		$TMIi99 =& $pipes[$readpipe];
// 		goto L136;
// 	L135:
// 		$TMIi99 = $pipes[$readpipe];
// 		goto L136;
// 	L136:
// 		$TLE70 = feof($TMIi99);
// 		$TLE71 = !$TLE70;
// 		$TLE72 = !$TLE71;
// 		if (TLE72) goto L138 else goto L139;
// 	L138:
// 		goto L137;
// 		goto L140;
// 	L139:
// 		goto L140;
// 	L140:
// 		$TLE73 = 1024;
// 		$TLE102 = param_is_ref (NULL, "fgets", 0);
// 		;
// 		if (TLE102) goto L141 else goto L142;
// 	L141:
// 		$TMIi101 =& $pipes[$readpipe];
// 		goto L143;
// 	L142:
// 		$TMIi101 = $pipes[$readpipe];
// 		goto L143;
// 	L143:
// 		$TLE74 = fgets($TMIi101, $TLE73);
// 		$cleansource = ($cleansource . $TLE74);
// 		goto L144;
// 	L137:
// 		$TLE104 = param_is_ref (NULL, "fclose", 0);
// 		;
// 		if (TLE104) goto L145 else goto L146;
// 	L145:
// 		$TMIi103 =& $pipes[$readpipe];
// 		goto L147;
// 	L146:
// 		$TMIi103 = $pipes[$readpipe];
// 		goto L147;
// 	L147:
// 		fclose($TMIi103);
// 		$retval = proc_close($process);
// 		goto L150;
// 	L149:
// 		$retval = -1;
// 		goto L150;
// 	L150:
// 		goto L153;
// 	L152:
// 		$retval = -1;
// 		goto L153;
// 	L153:
// 		$TLE75 = 'MWTidy::execExternalTidy';
// 		wfprofileout($TLE75);
// 		$TLE5 = !$stderr;
// 		if (TLE5) goto L154 else goto L155;
// 	L154:
// 		$TLE76 = '';
// 		$TEF6 = ($cleansource == $TLE76);
// 		goto L156;
// 	L155:
// 		$TEF6 = $TLE5;
// 		goto L156;
// 	L156:
// 		$TLE7 = (bool) $TEF6;
// 		if (TLE7) goto L157 else goto L158;
// 	L157:
// 		$TLE77 = '';
// 		$TEF8 = ($text != $TLE77);
// 		goto L159;
// 	L158:
// 		$TEF8 = $TLE7;
// 		goto L159;
// 	L159:
// 		$TLE78 = (bool) $TEF8;
// 		if (TLE78) goto L160 else goto L161;
// 	L160:
// 		$TLE79 = NULL;
// 		return $TLE79;
// 		goto L162;
// 	L161:
// 		return $cleansource;
// 		goto L162;
// 	L162:
// 	}
// 	private static function execinternaltidy($text, $stderr = False, &$retval = NULL)
// 	{
// 		global $wgTidyConf;
// 		global $IP;
// 		global $wgDebugTidy;
// 		$TLE80 = 'MWTidy::execInternalTidy';
// 		wfprofilein($TLE80);
// 		$tidy = new tidy();
// 		$TLE81 = 'utf8';
// 		$tidy->parsestring($text, $wgTidyConf, $TLE81);
// 		if (stderr) goto L175 else goto L176;
// 	L175:
// 		$retval = $tidy->getstatus();
// 		$TSt82 = $tidy->errorBuffer;
// 		return $TSt82;
// 		goto L177;
// 	L176:
// 		$tidy->cleanrepair();
// 		$retval = $tidy->getstatus();
// 		$TLE83 = 2;
// 		$TLE84 = ($retval == $TLE83);
// 		if (TLE84) goto L163 else goto L164;
// 	L163:
// 		$cleansource = NULL;
// 		goto L165;
// 	L164:
// 		$cleansource = tidy_get_output($tidy);
// 		goto L165;
// 	L165:
// 		$TLE9 = $wgDebugTidy;
// 		if (TLE9) goto L166 else goto L167;
// 	L166:
// 		$TLE85 = 0;
// 		$TEF10 = ($TLE85 < $retval);
// 		goto L168;
// 	L167:
// 		$TEF10 = $TLE9;
// 		goto L168;
// 	L168:
// 		$TLE86 = (bool) $TEF10;
// 		if (TLE86) goto L172 else goto L173;
// 	L172:
// 		$TLE87 = '<!--
// Tidy reports:
// ';
// 		$TLE88 = '-->';
// 		$TLE89 = '--&gt;';
// 		$TLE106 = param_is_ref (NULL, "str_replace", 0);
// 		;
// 		if (TLE106) goto L169 else goto L170;
// 	L169:
// 		$TMIt105 =& $tidy->errorBuffer;
// 		goto L171;
// 	L170:
// 		$TMIt105 = $tidy->errorBuffer;
// 		goto L171;
// 	L171:
// 		$TLE90 = str_replace($TLE88, $TLE89, $TMIt105);
// 		$TLE91 = ($TLE87 . $TLE90);
// 		$TLE92 = '
// -->';
// 		$TLE93 = ($TLE91 . $TLE92);
// 		$cleansource = ($cleansource . $TLE93);
// 		goto L174;
// 	L173:
// 		goto L174;
// 	L174:
// 		$TLE94 = 'MWTidy::execInternalTidy';
// 		wfprofileout($TLE94);
// 		return $cleansource;
// 		goto L177;
// 	L177:
// 	}
// }
// public static function tidy($text)
// {
// 	global $wgTidyInternal;
// 	$TLE11 = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><title>test</title></head><body>';
// 	$TLE12 = ($TLE11 . $text);
// 	$TLE13 = '</body></html>';
// 	$wrappedtext = ($TLE12 . $TLE13);
// 	$TLE14 = '	';
// 	$TLE15 = '&#9;';
// 	$wrappedtext = str_replace($TLE14, $TLE15, $wrappedtext);
// 	if (wgTidyInternal) goto L107 else goto L108;
// L107:
// 	$correctedtext = mwtidy::execinternaltidy($wrappedtext);
// 	goto L109;
// L108:
// 	$correctedtext = mwtidy::execexternaltidy($wrappedtext);
// 	goto L109;
// L109:
// 	$TLE16 = is_null($correctedtext);
// 	if (TLE16) goto L110 else goto L111;
// L110:
// 	$TLE17 = 'Tidy error detected!
// ';
// 	wfdebug($TLE17);
// 	$TLE18 = '
// <!-- Tidy found serious XHTML errors -->
// ';
// 	$TLE19 = ($text . $TLE18);
// 	return $TLE19;
// 	goto L112;
// L111:
// 	goto L112;
// L112:
// 	$TLE20 = '&#9;';
// 	$TLE21 = '	';
// 	$correctedtext = str_replace($TLE20, $TLE21, $correctedtext);
// 	return $correctedtext;
// }
PHP_METHOD(MWTidy, tidy)
{
zval* local_TLE11 = NULL;
zval* local_TLE12 = NULL;
zval* local_TLE13 = NULL;
zval* local_TLE14 = NULL;
zval* local_TLE15 = NULL;
zval* local_TLE16 = NULL;
zval* local_TLE17 = NULL;
zval* local_TLE18 = NULL;
zval* local_TLE19 = NULL;
zval* local_TLE20 = NULL;
zval* local_TLE21 = NULL;
zval* local_correctedtext = NULL;
zval* local_text = NULL;
zval* local_wgTidyInternal = NULL;
zval* local_wrappedtext = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_text != NULL)
{
	zval_ptr_dtor (&local_text);
}
local_text = params[0];
}
// Function body
// global $wgTidyInternal;
{
     if (local_wgTidyInternal == NULL)
    {
      local_wgTidyInternal = EG (uninitialized_zval_ptr);
      local_wgTidyInternal->refcount++;
    }
  zval** p_local = &local_wgTidyInternal;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgTidyInternal", 14 + 1, 125132442u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// $TLE11 = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><title>test</title></head><body>';
{
        if (local_TLE11 == NULL)
    {
      local_TLE11 = EG (uninitialized_zval_ptr);
      local_TLE11->refcount++;
    }
  zval** p_lhs = &local_TLE11;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><title>test</title></head><body>", 165, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE12 = ($TLE11 . $text);
{
    if (local_TLE12 == NULL)
    {
      local_TLE12 = EG (uninitialized_zval_ptr);
      local_TLE12->refcount++;
    }
  zval** p_lhs = &local_TLE12;

    zval* left;
  if (local_TLE11 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE11;

    zval* right;
  if (local_text == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_text;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE13 = '</body></html>';
{
        if (local_TLE13 == NULL)
    {
      local_TLE13 = EG (uninitialized_zval_ptr);
      local_TLE13->refcount++;
    }
  zval** p_lhs = &local_TLE13;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "</body></html>", 14, 1);

phc_check_invariants (TSRMLS_C);
}
// $wrappedtext = ($TLE12 . $TLE13);
{
    if (local_wrappedtext == NULL)
    {
      local_wrappedtext = EG (uninitialized_zval_ptr);
      local_wrappedtext->refcount++;
    }
  zval** p_lhs = &local_wrappedtext;

    zval* left;
  if (local_TLE12 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE12;

    zval* right;
  if (local_TLE13 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE13;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE14 = '	';
{
        if (local_TLE14 == NULL)
    {
      local_TLE14 = EG (uninitialized_zval_ptr);
      local_TLE14->refcount++;
    }
  zval** p_lhs = &local_TLE14;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "\011", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE15 = '&#9;';
{
        if (local_TLE15 == NULL)
    {
      local_TLE15 = EG (uninitialized_zval_ptr);
      local_TLE15->refcount++;
    }
  zval** p_lhs = &local_TLE15;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "&#9;", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $wrappedtext = str_replace($TLE14, $TLE15, $wrappedtext);
{
   initialize_function_call (&str_replace_fci, &str_replace_fcic, "str_replace", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 30 TSRMLS_CC);
      zend_function* signature = str_replace_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE14 == NULL)
    {
      local_TLE14 = EG (uninitialized_zval_ptr);
      local_TLE14->refcount++;
    }
  zval** p_arg = &local_TLE14;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE14 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE14;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE15 == NULL)
    {
      local_TLE15 = EG (uninitialized_zval_ptr);
      local_TLE15->refcount++;
    }
  zval** p_arg = &local_TLE15;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE15 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE15;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_wrappedtext == NULL)
    {
      local_wrappedtext = EG (uninitialized_zval_ptr);
      local_wrappedtext->refcount++;
    }
  zval** p_arg = &local_wrappedtext;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_wrappedtext == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_wrappedtext;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 30, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = str_replace_fci.param_count;
   zval*** params_save = str_replace_fci.params;
   zval** retval_save = str_replace_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   str_replace_fci.params = args_ind;
   str_replace_fci.param_count = 3;
   str_replace_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&str_replace_fci, &str_replace_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   str_replace_fci.params = params_save;
   str_replace_fci.param_count = param_count_save;
   str_replace_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_wrappedtext == NULL)
    {
      local_wrappedtext = EG (uninitialized_zval_ptr);
      local_wrappedtext->refcount++;
    }
  zval** p_lhs = &local_wrappedtext;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// if (wgTidyInternal) goto L107 else goto L108;
{
     zval* p_cond;
  if (local_wgTidyInternal == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_wgTidyInternal;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L107;
   else
      goto L108;
phc_check_invariants (TSRMLS_C);
}
// L107:
L107:;
// $correctedtext = mwtidy::execinternaltidy($wrappedtext);
{
   initialize_function_call (&mwtidy_execinternaltidy_fci, &mwtidy_execinternaltidy_fcic, "mwtidy::execinternaltidy", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 33 TSRMLS_CC);
      zend_function* signature = mwtidy_execinternaltidy_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_wrappedtext == NULL)
    {
      local_wrappedtext = EG (uninitialized_zval_ptr);
      local_wrappedtext->refcount++;
    }
  zval** p_arg = &local_wrappedtext;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_wrappedtext == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_wrappedtext;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 33, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = mwtidy_execinternaltidy_fci.param_count;
   zval*** params_save = mwtidy_execinternaltidy_fci.params;
   zval** retval_save = mwtidy_execinternaltidy_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   mwtidy_execinternaltidy_fci.params = args_ind;
   mwtidy_execinternaltidy_fci.param_count = 1;
   mwtidy_execinternaltidy_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&mwtidy_execinternaltidy_fci, &mwtidy_execinternaltidy_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   mwtidy_execinternaltidy_fci.params = params_save;
   mwtidy_execinternaltidy_fci.param_count = param_count_save;
   mwtidy_execinternaltidy_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_correctedtext == NULL)
    {
      local_correctedtext = EG (uninitialized_zval_ptr);
      local_correctedtext->refcount++;
    }
  zval** p_lhs = &local_correctedtext;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// goto L109;
{
goto L109;
phc_check_invariants (TSRMLS_C);
}
// L108:
L108:;
// $correctedtext = mwtidy::execexternaltidy($wrappedtext);
{
   initialize_function_call (&mwtidy_execexternaltidy_fci, &mwtidy_execexternaltidy_fcic, "mwtidy::execexternaltidy", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 35 TSRMLS_CC);
      zend_function* signature = mwtidy_execexternaltidy_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_wrappedtext == NULL)
    {
      local_wrappedtext = EG (uninitialized_zval_ptr);
      local_wrappedtext->refcount++;
    }
  zval** p_arg = &local_wrappedtext;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_wrappedtext == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_wrappedtext;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 35, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = mwtidy_execexternaltidy_fci.param_count;
   zval*** params_save = mwtidy_execexternaltidy_fci.params;
   zval** retval_save = mwtidy_execexternaltidy_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   mwtidy_execexternaltidy_fci.params = args_ind;
   mwtidy_execexternaltidy_fci.param_count = 1;
   mwtidy_execexternaltidy_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&mwtidy_execexternaltidy_fci, &mwtidy_execexternaltidy_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   mwtidy_execexternaltidy_fci.params = params_save;
   mwtidy_execexternaltidy_fci.param_count = param_count_save;
   mwtidy_execexternaltidy_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_correctedtext == NULL)
    {
      local_correctedtext = EG (uninitialized_zval_ptr);
      local_correctedtext->refcount++;
    }
  zval** p_lhs = &local_correctedtext;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// goto L109;
{
goto L109;
phc_check_invariants (TSRMLS_C);
}
// L109:
L109:;
// $TLE16 = is_null($correctedtext);
{
   initialize_function_call (&is_null_fci, &is_null_fcic, "is_null", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 37 TSRMLS_CC);
      zend_function* signature = is_null_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_correctedtext == NULL)
    {
      local_correctedtext = EG (uninitialized_zval_ptr);
      local_correctedtext->refcount++;
    }
  zval** p_arg = &local_correctedtext;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_correctedtext == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_correctedtext;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 37, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = is_null_fci.param_count;
   zval*** params_save = is_null_fci.params;
   zval** retval_save = is_null_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   is_null_fci.params = args_ind;
   is_null_fci.param_count = 1;
   is_null_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&is_null_fci, &is_null_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   is_null_fci.params = params_save;
   is_null_fci.param_count = param_count_save;
   is_null_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE16 == NULL)
    {
      local_TLE16 = EG (uninitialized_zval_ptr);
      local_TLE16->refcount++;
    }
  zval** p_lhs = &local_TLE16;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// if (TLE16) goto L110 else goto L111;
{
     zval* p_cond;
  if (local_TLE16 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE16;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L110;
   else
      goto L111;
phc_check_invariants (TSRMLS_C);
}
// L110:
L110:;
// $TLE17 = 'Tidy error detected!
// ';
{
        if (local_TLE17 == NULL)
    {
      local_TLE17 = EG (uninitialized_zval_ptr);
      local_TLE17->refcount++;
    }
  zval** p_lhs = &local_TLE17;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "Tidy error detected!\012", 21, 1);

phc_check_invariants (TSRMLS_C);
}
// wfdebug($TLE17);
{
   initialize_function_call (&wfdebug_fci, &wfdebug_fcic, "wfdebug", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 38 TSRMLS_CC);
      zend_function* signature = wfdebug_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE17 == NULL)
    {
      local_TLE17 = EG (uninitialized_zval_ptr);
      local_TLE17->refcount++;
    }
  zval** p_arg = &local_TLE17;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE17 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE17;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 38, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfdebug_fci.param_count;
   zval*** params_save = wfdebug_fci.params;
   zval** retval_save = wfdebug_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfdebug_fci.params = args_ind;
   wfdebug_fci.param_count = 1;
   wfdebug_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfdebug_fci, &wfdebug_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfdebug_fci.params = params_save;
   wfdebug_fci.param_count = param_count_save;
   wfdebug_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE18 = '
// <!-- Tidy found serious XHTML errors -->
// ';
{
        if (local_TLE18 == NULL)
    {
      local_TLE18 = EG (uninitialized_zval_ptr);
      local_TLE18->refcount++;
    }
  zval** p_lhs = &local_TLE18;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "\012<!-- Tidy found serious XHTML errors -->\012", 42, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE19 = ($text . $TLE18);
{
    if (local_TLE19 == NULL)
    {
      local_TLE19 = EG (uninitialized_zval_ptr);
      local_TLE19->refcount++;
    }
  zval** p_lhs = &local_TLE19;

    zval* left;
  if (local_text == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_text;

    zval* right;
  if (local_TLE18 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE18;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// return $TLE19;
{
     zval* rhs;
  if (local_TLE19 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE19;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// goto L112;
{
goto L112;
phc_check_invariants (TSRMLS_C);
}
// L111:
L111:;
// goto L112;
{
goto L112;
phc_check_invariants (TSRMLS_C);
}
// L112:
L112:;
// $TLE20 = '&#9;';
{
        if (local_TLE20 == NULL)
    {
      local_TLE20 = EG (uninitialized_zval_ptr);
      local_TLE20->refcount++;
    }
  zval** p_lhs = &local_TLE20;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "&#9;", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE21 = '	';
{
        if (local_TLE21 == NULL)
    {
      local_TLE21 = EG (uninitialized_zval_ptr);
      local_TLE21->refcount++;
    }
  zval** p_lhs = &local_TLE21;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "\011", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $correctedtext = str_replace($TLE20, $TLE21, $correctedtext);
{
   initialize_function_call (&str_replace_fci, &str_replace_fcic, "str_replace", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 43 TSRMLS_CC);
      zend_function* signature = str_replace_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE20 == NULL)
    {
      local_TLE20 = EG (uninitialized_zval_ptr);
      local_TLE20->refcount++;
    }
  zval** p_arg = &local_TLE20;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE20 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE20;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE21 == NULL)
    {
      local_TLE21 = EG (uninitialized_zval_ptr);
      local_TLE21->refcount++;
    }
  zval** p_arg = &local_TLE21;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE21 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE21;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_correctedtext == NULL)
    {
      local_correctedtext = EG (uninitialized_zval_ptr);
      local_correctedtext->refcount++;
    }
  zval** p_arg = &local_correctedtext;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_correctedtext == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_correctedtext;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 43, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = str_replace_fci.param_count;
   zval*** params_save = str_replace_fci.params;
   zval** retval_save = str_replace_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   str_replace_fci.params = args_ind;
   str_replace_fci.param_count = 3;
   str_replace_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&str_replace_fci, &str_replace_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   str_replace_fci.params = params_save;
   str_replace_fci.param_count = param_count_save;
   str_replace_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_correctedtext == NULL)
    {
      local_correctedtext = EG (uninitialized_zval_ptr);
      local_correctedtext->refcount++;
    }
  zval** p_lhs = &local_correctedtext;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $correctedtext;
{
     zval* rhs;
  if (local_correctedtext == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_correctedtext;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE11 != NULL)
{
zval_ptr_dtor (&local_TLE11);
}
if (local_TLE12 != NULL)
{
zval_ptr_dtor (&local_TLE12);
}
if (local_TLE13 != NULL)
{
zval_ptr_dtor (&local_TLE13);
}
if (local_TLE14 != NULL)
{
zval_ptr_dtor (&local_TLE14);
}
if (local_TLE15 != NULL)
{
zval_ptr_dtor (&local_TLE15);
}
if (local_TLE16 != NULL)
{
zval_ptr_dtor (&local_TLE16);
}
if (local_TLE17 != NULL)
{
zval_ptr_dtor (&local_TLE17);
}
if (local_TLE18 != NULL)
{
zval_ptr_dtor (&local_TLE18);
}
if (local_TLE19 != NULL)
{
zval_ptr_dtor (&local_TLE19);
}
if (local_TLE20 != NULL)
{
zval_ptr_dtor (&local_TLE20);
}
if (local_TLE21 != NULL)
{
zval_ptr_dtor (&local_TLE21);
}
if (local_correctedtext != NULL)
{
zval_ptr_dtor (&local_correctedtext);
}
if (local_text != NULL)
{
zval_ptr_dtor (&local_text);
}
if (local_wgTidyInternal != NULL)
{
zval_ptr_dtor (&local_wgTidyInternal);
}
if (local_wrappedtext != NULL)
{
zval_ptr_dtor (&local_wrappedtext);
}
}
// public static function checkerrors($text, &$errorStr = NULL)
// {
// 	global $wgTidyInternal;
// 	$retval = 0;
// 	if (wgTidyInternal) goto L113 else goto L114;
// L113:
// 	$TLE22 = True;
// 	$errorStr = mwtidy::execinternaltidy($text, $TLE22, $retval);
// 	goto L115;
// L114:
// 	$TLE23 = True;
// 	$errorStr = mwtidy::execexternaltidy($text, $TLE23, $retval);
// 	goto L115;
// L115:
// 	$TLE24 = 0;
// 	$TLE0 = ($retval < $TLE24);
// 	if (TLE0) goto L116 else goto L117;
// L116:
// 	$TLE25 = '';
// 	$TEF1 = ($errorStr == $TLE25);
// 	goto L118;
// L117:
// 	$TEF1 = $TLE0;
// 	goto L118;
// L118:
// 	$TLE2 = (bool) $TEF1;
// 	if (TLE2) goto L119 else goto L120;
// L119:
// 	$TEF3 = $TLE2;
// 	goto L121;
// L120:
// 	$TLE26 = 0;
// 	$TEF3 = ($retval == $TLE26);
// 	goto L121;
// L121:
// 	$TLE27 = (bool) $TEF3;
// 	return $TLE27;
// }
PHP_METHOD(MWTidy, checkerrors)
{
zval* local_TEF1 = NULL;
zval* local_TEF3 = NULL;
zval* local_TLE0 = NULL;
zval* local_TLE2 = NULL;
zval* local_TLE22 = NULL;
zval* local_TLE23 = NULL;
zval* local_TLE24 = NULL;
zval* local_TLE25 = NULL;
zval* local_TLE26 = NULL;
zval* local_TLE27 = NULL;
zval* local_errorStr = NULL;
zval* local_retval = NULL;
zval* local_text = NULL;
zval* local_wgTidyInternal = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[2];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_text != NULL)
{
	zval_ptr_dtor (&local_text);
}
local_text = params[0];
// param 1
if (num_args <= 1)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[1] = default_value;
}
params[1]->refcount++;
if (local_errorStr != NULL)
{
	zval_ptr_dtor (&local_errorStr);
}
local_errorStr = params[1];
}
// Function body
// global $wgTidyInternal;
{
     if (local_wgTidyInternal == NULL)
    {
      local_wgTidyInternal = EG (uninitialized_zval_ptr);
      local_wgTidyInternal->refcount++;
    }
  zval** p_local = &local_wgTidyInternal;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgTidyInternal", 14 + 1, 125132442u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// $retval = 0;
{
        if (local_retval == NULL)
    {
      local_retval = EG (uninitialized_zval_ptr);
      local_retval->refcount++;
    }
  zval** p_lhs = &local_retval;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// if (wgTidyInternal) goto L113 else goto L114;
{
     zval* p_cond;
  if (local_wgTidyInternal == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_wgTidyInternal;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L113;
   else
      goto L114;
phc_check_invariants (TSRMLS_C);
}
// L113:
L113:;
// $TLE22 = True;
{
        if (local_TLE22 == NULL)
    {
      local_TLE22 = EG (uninitialized_zval_ptr);
      local_TLE22->refcount++;
    }
  zval** p_lhs = &local_TLE22;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $errorStr = mwtidy::execinternaltidy($text, $TLE22, $retval);
{
   initialize_function_call (&mwtidy_execinternaltidy_fci, &mwtidy_execinternaltidy_fcic, "mwtidy::execinternaltidy", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 59 TSRMLS_CC);
      zend_function* signature = mwtidy_execinternaltidy_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_text == NULL)
    {
      local_text = EG (uninitialized_zval_ptr);
      local_text->refcount++;
    }
  zval** p_arg = &local_text;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_text == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_text;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE22 == NULL)
    {
      local_TLE22 = EG (uninitialized_zval_ptr);
      local_TLE22->refcount++;
    }
  zval** p_arg = &local_TLE22;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE22 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE22;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_retval == NULL)
    {
      local_retval = EG (uninitialized_zval_ptr);
      local_retval->refcount++;
    }
  zval** p_arg = &local_retval;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_retval == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_retval;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 59, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = mwtidy_execinternaltidy_fci.param_count;
   zval*** params_save = mwtidy_execinternaltidy_fci.params;
   zval** retval_save = mwtidy_execinternaltidy_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   mwtidy_execinternaltidy_fci.params = args_ind;
   mwtidy_execinternaltidy_fci.param_count = 3;
   mwtidy_execinternaltidy_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&mwtidy_execinternaltidy_fci, &mwtidy_execinternaltidy_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   mwtidy_execinternaltidy_fci.params = params_save;
   mwtidy_execinternaltidy_fci.param_count = param_count_save;
   mwtidy_execinternaltidy_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_errorStr == NULL)
    {
      local_errorStr = EG (uninitialized_zval_ptr);
      local_errorStr->refcount++;
    }
  zval** p_lhs = &local_errorStr;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// goto L115;
{
goto L115;
phc_check_invariants (TSRMLS_C);
}
// L114:
L114:;
// $TLE23 = True;
{
        if (local_TLE23 == NULL)
    {
      local_TLE23 = EG (uninitialized_zval_ptr);
      local_TLE23->refcount++;
    }
  zval** p_lhs = &local_TLE23;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $errorStr = mwtidy::execexternaltidy($text, $TLE23, $retval);
{
   initialize_function_call (&mwtidy_execexternaltidy_fci, &mwtidy_execexternaltidy_fcic, "mwtidy::execexternaltidy", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 61 TSRMLS_CC);
      zend_function* signature = mwtidy_execexternaltidy_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_text == NULL)
    {
      local_text = EG (uninitialized_zval_ptr);
      local_text->refcount++;
    }
  zval** p_arg = &local_text;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_text == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_text;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE23 == NULL)
    {
      local_TLE23 = EG (uninitialized_zval_ptr);
      local_TLE23->refcount++;
    }
  zval** p_arg = &local_TLE23;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE23 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE23;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_retval == NULL)
    {
      local_retval = EG (uninitialized_zval_ptr);
      local_retval->refcount++;
    }
  zval** p_arg = &local_retval;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_retval == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_retval;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 61, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = mwtidy_execexternaltidy_fci.param_count;
   zval*** params_save = mwtidy_execexternaltidy_fci.params;
   zval** retval_save = mwtidy_execexternaltidy_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   mwtidy_execexternaltidy_fci.params = args_ind;
   mwtidy_execexternaltidy_fci.param_count = 3;
   mwtidy_execexternaltidy_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&mwtidy_execexternaltidy_fci, &mwtidy_execexternaltidy_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   mwtidy_execexternaltidy_fci.params = params_save;
   mwtidy_execexternaltidy_fci.param_count = param_count_save;
   mwtidy_execexternaltidy_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_errorStr == NULL)
    {
      local_errorStr = EG (uninitialized_zval_ptr);
      local_errorStr->refcount++;
    }
  zval** p_lhs = &local_errorStr;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// goto L115;
{
goto L115;
phc_check_invariants (TSRMLS_C);
}
// L115:
L115:;
// $TLE24 = 0;
{
        if (local_TLE24 == NULL)
    {
      local_TLE24 = EG (uninitialized_zval_ptr);
      local_TLE24->refcount++;
    }
  zval** p_lhs = &local_TLE24;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TLE0 = ($retval < $TLE24);
{
    if (local_TLE0 == NULL)
    {
      local_TLE0 = EG (uninitialized_zval_ptr);
      local_TLE0->refcount++;
    }
  zval** p_lhs = &local_TLE0;

    zval* left;
  if (local_retval == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_retval;

    zval* right;
  if (local_TLE24 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE24;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_smaller_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE0) goto L116 else goto L117;
{
     zval* p_cond;
  if (local_TLE0 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE0;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L116;
   else
      goto L117;
phc_check_invariants (TSRMLS_C);
}
// L116:
L116:;
// $TLE25 = '';
{
        if (local_TLE25 == NULL)
    {
      local_TLE25 = EG (uninitialized_zval_ptr);
      local_TLE25->refcount++;
    }
  zval** p_lhs = &local_TLE25;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
// $TEF1 = ($errorStr == $TLE25);
{
    if (local_TEF1 == NULL)
    {
      local_TEF1 = EG (uninitialized_zval_ptr);
      local_TEF1->refcount++;
    }
  zval** p_lhs = &local_TEF1;

    zval* left;
  if (local_errorStr == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_errorStr;

    zval* right;
  if (local_TLE25 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE25;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L118;
{
goto L118;
phc_check_invariants (TSRMLS_C);
}
// L117:
L117:;
// $TEF1 = $TLE0;
{
    if (local_TEF1 == NULL)
    {
      local_TEF1 = EG (uninitialized_zval_ptr);
      local_TEF1->refcount++;
    }
  zval** p_lhs = &local_TEF1;

    zval* rhs;
  if (local_TLE0 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE0;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L118;
{
goto L118;
phc_check_invariants (TSRMLS_C);
}
// L118:
L118:;
// $TLE2 = (bool) $TEF1;
{
      if (local_TLE2 == NULL)
    {
      local_TLE2 = EG (uninitialized_zval_ptr);
      local_TLE2->refcount++;
    }
  zval** p_lhs = &local_TLE2;

    zval* rhs;
  if (local_TEF1 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF1;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// if (TLE2) goto L119 else goto L120;
{
     zval* p_cond;
  if (local_TLE2 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE2;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L119;
   else
      goto L120;
phc_check_invariants (TSRMLS_C);
}
// L119:
L119:;
// $TEF3 = $TLE2;
{
    if (local_TEF3 == NULL)
    {
      local_TEF3 = EG (uninitialized_zval_ptr);
      local_TEF3->refcount++;
    }
  zval** p_lhs = &local_TEF3;

    zval* rhs;
  if (local_TLE2 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE2;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L121;
{
goto L121;
phc_check_invariants (TSRMLS_C);
}
// L120:
L120:;
// $TLE26 = 0;
{
        if (local_TLE26 == NULL)
    {
      local_TLE26 = EG (uninitialized_zval_ptr);
      local_TLE26->refcount++;
    }
  zval** p_lhs = &local_TLE26;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TEF3 = ($retval == $TLE26);
{
    if (local_TEF3 == NULL)
    {
      local_TEF3 = EG (uninitialized_zval_ptr);
      local_TEF3->refcount++;
    }
  zval** p_lhs = &local_TEF3;

    zval* left;
  if (local_retval == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_retval;

    zval* right;
  if (local_TLE26 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE26;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L121;
{
goto L121;
phc_check_invariants (TSRMLS_C);
}
// L121:
L121:;
// $TLE27 = (bool) $TEF3;
{
      if (local_TLE27 == NULL)
    {
      local_TLE27 = EG (uninitialized_zval_ptr);
      local_TLE27->refcount++;
    }
  zval** p_lhs = &local_TLE27;

    zval* rhs;
  if (local_TEF3 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF3;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// return $TLE27;
{
     zval* rhs;
  if (local_TLE27 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE27;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TEF1 != NULL)
{
zval_ptr_dtor (&local_TEF1);
}
if (local_TEF3 != NULL)
{
zval_ptr_dtor (&local_TEF3);
}
if (local_TLE0 != NULL)
{
zval_ptr_dtor (&local_TLE0);
}
if (local_TLE2 != NULL)
{
zval_ptr_dtor (&local_TLE2);
}
if (local_TLE22 != NULL)
{
zval_ptr_dtor (&local_TLE22);
}
if (local_TLE23 != NULL)
{
zval_ptr_dtor (&local_TLE23);
}
if (local_TLE24 != NULL)
{
zval_ptr_dtor (&local_TLE24);
}
if (local_TLE25 != NULL)
{
zval_ptr_dtor (&local_TLE25);
}
if (local_TLE26 != NULL)
{
zval_ptr_dtor (&local_TLE26);
}
if (local_TLE27 != NULL)
{
zval_ptr_dtor (&local_TLE27);
}
if (local_errorStr != NULL)
{
zval_ptr_dtor (&local_errorStr);
}
if (local_retval != NULL)
{
zval_ptr_dtor (&local_retval);
}
if (local_text != NULL)
{
zval_ptr_dtor (&local_text);
}
if (local_wgTidyInternal != NULL)
{
zval_ptr_dtor (&local_wgTidyInternal);
}
}
// private static function execexternaltidy($text, $stderr = False, &$retval = NULL)
// {
// 	global $wgTidyConf;
// 	global $wgTidyBin;
// 	global $wgTidyOpts;
// 	$TLE28 = 'MWTidy::execExternalTidy';
// 	wfprofilein($TLE28);
// 	$cleansource = '';
// 	$opts = ' -utf8';
// 	if (stderr) goto L122 else goto L123;
// L122:
// 	$TLE29 = 0;
// 	$TLE30 = 'pipe';
// 	$TLE31 = 'r';
// 	unset($TSa32);
// 	$TSa32 = (array) $TSa32;
// 	$TSa32[] = $TLE30;
// 	$TSa32[] = $TLE31;
// 	$TLE33 = 1;
// 	$TLE34 = 'file';
// 	$TLE35 = wfgetnull();
// 	$TLE36 = 'a';
// 	unset($TSa37);
// 	$TSa37 = (array) $TSa37;
// 	$TSa37[] = $TLE34;
// 	$TSa37[] = $TLE35;
// 	$TSa37[] = $TLE36;
// 	$TLE38 = 2;
// 	$TLE39 = 'pipe';
// 	$TLE40 = 'w';
// 	unset($TSa41);
// 	$TSa41 = (array) $TSa41;
// 	$TSa41[] = $TLE39;
// 	$TSa41[] = $TLE40;
// 	unset($TSa42);
// 	$TSa42 = (array) $TSa42;
// 	$TSa42[$TLE29] = $TSa32;
// 	$TSa42[$TLE33] = $TSa37;
// 	$TSa42[$TLE38] = $TSa41;
// 	$descriptorspec = $TSa42;
// 	goto L124;
// L123:
// 	$TLE43 = 0;
// 	$TLE44 = 'pipe';
// 	$TLE45 = 'r';
// 	unset($TSa46);
// 	$TSa46 = (array) $TSa46;
// 	$TSa46[] = $TLE44;
// 	$TSa46[] = $TLE45;
// 	$TLE47 = 1;
// 	$TLE48 = 'pipe';
// 	$TLE49 = 'w';
// 	unset($TSa50);
// 	$TSa50 = (array) $TSa50;
// 	$TSa50[] = $TLE48;
// 	$TSa50[] = $TLE49;
// 	$TLE51 = 2;
// 	$TLE52 = 'file';
// 	$TLE53 = wfgetnull();
// 	$TLE54 = 'a';
// 	unset($TSa55);
// 	$TSa55 = (array) $TSa55;
// 	$TSa55[] = $TLE52;
// 	$TSa55[] = $TLE53;
// 	$TSa55[] = $TLE54;
// 	unset($TSa56);
// 	$TSa56 = (array) $TSa56;
// 	$TSa56[$TLE43] = $TSa46;
// 	$TSa56[$TLE47] = $TSa50;
// 	$TSa56[$TLE51] = $TSa55;
// 	$descriptorspec = $TSa56;
// 	goto L124;
// L124:
// 	if (stderr) goto L125 else goto L126;
// L125:
// 	$TEF4 = 2;
// 	goto L127;
// L126:
// 	$TEF4 = 1;
// 	goto L127;
// L127:
// 	$readpipe = $TEF4;
// 	unset($TSa57);
// 	$TSa57 = (array) $TSa57;
// 	$pipes = $TSa57;
// 	$TLE58 = 'proc_open';
// 	$TLE59 = function_exists($TLE58);
// 	if (TLE59) goto L151 else goto L152;
// L151:
// 	$TLE60 = ' -config ';
// 	$TLE61 = ($wgTidyBin . $TLE60);
// 	$TLE62 = ($TLE61 . $wgTidyConf);
// 	$TLE63 = ' ';
// 	$TLE64 = ($TLE62 . $TLE63);
// 	$TLE65 = ($TLE64 . $wgTidyOpts);
// 	$TLE66 = ($TLE65 . $opts);
// 	$process = proc_open($TLE66, $descriptorspec, $pipes);
// 	$TLE67 = is_resource($process);
// 	if (TLE67) goto L148 else goto L149;
// L148:
// 	$TLE68 = 0;
// 	$TLE96 = param_is_ref (NULL, "fwrite", 0);
// 	;
// 	if (TLE96) goto L128 else goto L129;
// L128:
// 	$TMIi95 =& $pipes[$TLE68];
// 	goto L130;
// L129:
// 	$TMIi95 = $pipes[$TLE68];
// 	goto L130;
// L130:
// 	fwrite($TMIi95, $text);
// 	$TLE69 = 0;
// 	$TLE98 = param_is_ref (NULL, "fclose", 0);
// 	;
// 	if (TLE98) goto L131 else goto L132;
// L131:
// 	$TMIi97 =& $pipes[$TLE69];
// 	goto L133;
// L132:
// 	$TMIi97 = $pipes[$TLE69];
// 	goto L133;
// L133:
// 	fclose($TMIi97);
// L144:
// 	$TLE100 = param_is_ref (NULL, "feof", 0);
// 	;
// 	if (TLE100) goto L134 else goto L135;
// L134:
// 	$TMIi99 =& $pipes[$readpipe];
// 	goto L136;
// L135:
// 	$TMIi99 = $pipes[$readpipe];
// 	goto L136;
// L136:
// 	$TLE70 = feof($TMIi99);
// 	$TLE71 = !$TLE70;
// 	$TLE72 = !$TLE71;
// 	if (TLE72) goto L138 else goto L139;
// L138:
// 	goto L137;
// 	goto L140;
// L139:
// 	goto L140;
// L140:
// 	$TLE73 = 1024;
// 	$TLE102 = param_is_ref (NULL, "fgets", 0);
// 	;
// 	if (TLE102) goto L141 else goto L142;
// L141:
// 	$TMIi101 =& $pipes[$readpipe];
// 	goto L143;
// L142:
// 	$TMIi101 = $pipes[$readpipe];
// 	goto L143;
// L143:
// 	$TLE74 = fgets($TMIi101, $TLE73);
// 	$cleansource = ($cleansource . $TLE74);
// 	goto L144;
// L137:
// 	$TLE104 = param_is_ref (NULL, "fclose", 0);
// 	;
// 	if (TLE104) goto L145 else goto L146;
// L145:
// 	$TMIi103 =& $pipes[$readpipe];
// 	goto L147;
// L146:
// 	$TMIi103 = $pipes[$readpipe];
// 	goto L147;
// L147:
// 	fclose($TMIi103);
// 	$retval = proc_close($process);
// 	goto L150;
// L149:
// 	$retval = -1;
// 	goto L150;
// L150:
// 	goto L153;
// L152:
// 	$retval = -1;
// 	goto L153;
// L153:
// 	$TLE75 = 'MWTidy::execExternalTidy';
// 	wfprofileout($TLE75);
// 	$TLE5 = !$stderr;
// 	if (TLE5) goto L154 else goto L155;
// L154:
// 	$TLE76 = '';
// 	$TEF6 = ($cleansource == $TLE76);
// 	goto L156;
// L155:
// 	$TEF6 = $TLE5;
// 	goto L156;
// L156:
// 	$TLE7 = (bool) $TEF6;
// 	if (TLE7) goto L157 else goto L158;
// L157:
// 	$TLE77 = '';
// 	$TEF8 = ($text != $TLE77);
// 	goto L159;
// L158:
// 	$TEF8 = $TLE7;
// 	goto L159;
// L159:
// 	$TLE78 = (bool) $TEF8;
// 	if (TLE78) goto L160 else goto L161;
// L160:
// 	$TLE79 = NULL;
// 	return $TLE79;
// 	goto L162;
// L161:
// 	return $cleansource;
// 	goto L162;
// L162:
// }
PHP_METHOD(MWTidy, execexternaltidy)
{
zval* local_TEF4 = NULL;
zval* local_TEF6 = NULL;
zval* local_TEF8 = NULL;
zval* local_TLE100 = NULL;
zval* local_TLE102 = NULL;
zval* local_TLE104 = NULL;
zval* local_TLE28 = NULL;
zval* local_TLE29 = NULL;
zval* local_TLE30 = NULL;
zval* local_TLE31 = NULL;
zval* local_TLE33 = NULL;
zval* local_TLE34 = NULL;
zval* local_TLE35 = NULL;
zval* local_TLE36 = NULL;
zval* local_TLE38 = NULL;
zval* local_TLE39 = NULL;
zval* local_TLE40 = NULL;
zval* local_TLE43 = NULL;
zval* local_TLE44 = NULL;
zval* local_TLE45 = NULL;
zval* local_TLE47 = NULL;
zval* local_TLE48 = NULL;
zval* local_TLE49 = NULL;
zval* local_TLE5 = NULL;
zval* local_TLE51 = NULL;
zval* local_TLE52 = NULL;
zval* local_TLE53 = NULL;
zval* local_TLE54 = NULL;
zval* local_TLE58 = NULL;
zval* local_TLE59 = NULL;
zval* local_TLE60 = NULL;
zval* local_TLE61 = NULL;
zval* local_TLE62 = NULL;
zval* local_TLE63 = NULL;
zval* local_TLE64 = NULL;
zval* local_TLE65 = NULL;
zval* local_TLE66 = NULL;
zval* local_TLE67 = NULL;
zval* local_TLE68 = NULL;
zval* local_TLE69 = NULL;
zval* local_TLE7 = NULL;
zval* local_TLE70 = NULL;
zval* local_TLE71 = NULL;
zval* local_TLE72 = NULL;
zval* local_TLE73 = NULL;
zval* local_TLE74 = NULL;
zval* local_TLE75 = NULL;
zval* local_TLE76 = NULL;
zval* local_TLE77 = NULL;
zval* local_TLE78 = NULL;
zval* local_TLE79 = NULL;
zval* local_TLE96 = NULL;
zval* local_TLE98 = NULL;
zval* local_TMIi101 = NULL;
zval* local_TMIi103 = NULL;
zval* local_TMIi95 = NULL;
zval* local_TMIi97 = NULL;
zval* local_TMIi99 = NULL;
zval* local_TSa32 = NULL;
zval* local_TSa37 = NULL;
zval* local_TSa41 = NULL;
zval* local_TSa42 = NULL;
zval* local_TSa46 = NULL;
zval* local_TSa50 = NULL;
zval* local_TSa55 = NULL;
zval* local_TSa56 = NULL;
zval* local_TSa57 = NULL;
zval* local_cleansource = NULL;
zval* local_descriptorspec = NULL;
zval* local_opts = NULL;
zval* local_pipes = NULL;
zval* local_process = NULL;
zval* local_readpipe = NULL;
zval* local_retval = NULL;
zval* local_stderr = NULL;
zval* local_text = NULL;
zval* local_wgTidyBin = NULL;
zval* local_wgTidyConf = NULL;
zval* local_wgTidyOpts = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[3];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_text != NULL)
{
	zval_ptr_dtor (&local_text);
}
local_text = params[0];
// param 1
if (num_args <= 1)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = False;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[1] = default_value;
}
params[1]->refcount++;
if (local_stderr != NULL)
{
	zval_ptr_dtor (&local_stderr);
}
local_stderr = params[1];
// param 2
if (num_args <= 2)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[2] = default_value;
}
params[2]->refcount++;
if (local_retval != NULL)
{
	zval_ptr_dtor (&local_retval);
}
local_retval = params[2];
}
// Function body
// global $wgTidyConf;
{
     if (local_wgTidyConf == NULL)
    {
      local_wgTidyConf = EG (uninitialized_zval_ptr);
      local_wgTidyConf->refcount++;
    }
  zval** p_local = &local_wgTidyConf;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgTidyConf", 10 + 1, 3667061347u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgTidyBin;
{
     if (local_wgTidyBin == NULL)
    {
      local_wgTidyBin = EG (uninitialized_zval_ptr);
      local_wgTidyBin->refcount++;
    }
  zval** p_local = &local_wgTidyBin;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgTidyBin", 9 + 1, 4275897270u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgTidyOpts;
{
     if (local_wgTidyOpts == NULL)
    {
      local_wgTidyOpts = EG (uninitialized_zval_ptr);
      local_wgTidyOpts->refcount++;
    }
  zval** p_local = &local_wgTidyOpts;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgTidyOpts", 10 + 1, 3681335299u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// $TLE28 = 'MWTidy::execExternalTidy';
{
        if (local_TLE28 == NULL)
    {
      local_TLE28 = EG (uninitialized_zval_ptr);
      local_TLE28->refcount++;
    }
  zval** p_lhs = &local_TLE28;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "MWTidy::execExternalTidy", 24, 1);

phc_check_invariants (TSRMLS_C);
}
// wfprofilein($TLE28);
{
   initialize_function_call (&wfprofilein_fci, &wfprofilein_fcic, "wfprofilein", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 77 TSRMLS_CC);
      zend_function* signature = wfprofilein_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE28 == NULL)
    {
      local_TLE28 = EG (uninitialized_zval_ptr);
      local_TLE28->refcount++;
    }
  zval** p_arg = &local_TLE28;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE28 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE28;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 77, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfprofilein_fci.param_count;
   zval*** params_save = wfprofilein_fci.params;
   zval** retval_save = wfprofilein_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfprofilein_fci.params = args_ind;
   wfprofilein_fci.param_count = 1;
   wfprofilein_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfprofilein_fci, &wfprofilein_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfprofilein_fci.params = params_save;
   wfprofilein_fci.param_count = param_count_save;
   wfprofilein_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $cleansource = '';
{
        if (local_cleansource == NULL)
    {
      local_cleansource = EG (uninitialized_zval_ptr);
      local_cleansource->refcount++;
    }
  zval** p_lhs = &local_cleansource;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
// $opts = ' -utf8';
{
        if (local_opts == NULL)
    {
      local_opts = EG (uninitialized_zval_ptr);
      local_opts->refcount++;
    }
  zval** p_lhs = &local_opts;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, " -utf8", 6, 1);

phc_check_invariants (TSRMLS_C);
}
// if (stderr) goto L122 else goto L123;
{
     zval* p_cond;
  if (local_stderr == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_stderr;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L122;
   else
      goto L123;
phc_check_invariants (TSRMLS_C);
}
// L122:
L122:;
// $TLE29 = 0;
{
        if (local_TLE29 == NULL)
    {
      local_TLE29 = EG (uninitialized_zval_ptr);
      local_TLE29->refcount++;
    }
  zval** p_lhs = &local_TLE29;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TLE30 = 'pipe';
{
        if (local_TLE30 == NULL)
    {
      local_TLE30 = EG (uninitialized_zval_ptr);
      local_TLE30->refcount++;
    }
  zval** p_lhs = &local_TLE30;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "pipe", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE31 = 'r';
{
        if (local_TLE31 == NULL)
    {
      local_TLE31 = EG (uninitialized_zval_ptr);
      local_TLE31->refcount++;
    }
  zval** p_lhs = &local_TLE31;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "r", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// unset($TSa32);
{
if (local_TSa32 != NULL)
{
zval_ptr_dtor (&local_TSa32);
local_TSa32 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa32 = (array) $TSa32;
{
      if (local_TSa32 == NULL)
    {
      local_TSa32 = EG (uninitialized_zval_ptr);
      local_TSa32->refcount++;
    }
  zval** p_lhs = &local_TSa32;

    zval* rhs;
  if (local_TSa32 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa32;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa32[] = $TLE30;
{
     if (local_TSa32 == NULL)
    {
      local_TSa32 = EG (uninitialized_zval_ptr);
      local_TSa32->refcount++;
    }
  zval** p_array = &local_TSa32;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE30 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE30;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa32[] = $TLE31;
{
     if (local_TSa32 == NULL)
    {
      local_TSa32 = EG (uninitialized_zval_ptr);
      local_TSa32->refcount++;
    }
  zval** p_array = &local_TSa32;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE31 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE31;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TLE33 = 1;
{
        if (local_TLE33 == NULL)
    {
      local_TLE33 = EG (uninitialized_zval_ptr);
      local_TLE33->refcount++;
    }
  zval** p_lhs = &local_TLE33;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE34 = 'file';
{
        if (local_TLE34 == NULL)
    {
      local_TLE34 = EG (uninitialized_zval_ptr);
      local_TLE34->refcount++;
    }
  zval** p_lhs = &local_TLE34;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "file", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE35 = wfgetnull();
{
   initialize_function_call (&wfgetnull_fci, &wfgetnull_fcic, "wfgetnull", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 85 TSRMLS_CC);
      zend_function* signature = wfgetnull_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 85, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfgetnull_fci.param_count;
   zval*** params_save = wfgetnull_fci.params;
   zval** retval_save = wfgetnull_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfgetnull_fci.params = args_ind;
   wfgetnull_fci.param_count = 0;
   wfgetnull_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfgetnull_fci, &wfgetnull_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfgetnull_fci.params = params_save;
   wfgetnull_fci.param_count = param_count_save;
   wfgetnull_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE35 == NULL)
    {
      local_TLE35 = EG (uninitialized_zval_ptr);
      local_TLE35->refcount++;
    }
  zval** p_lhs = &local_TLE35;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE36 = 'a';
{
        if (local_TLE36 == NULL)
    {
      local_TLE36 = EG (uninitialized_zval_ptr);
      local_TLE36->refcount++;
    }
  zval** p_lhs = &local_TLE36;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "a", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// unset($TSa37);
{
if (local_TSa37 != NULL)
{
zval_ptr_dtor (&local_TSa37);
local_TSa37 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa37 = (array) $TSa37;
{
      if (local_TSa37 == NULL)
    {
      local_TSa37 = EG (uninitialized_zval_ptr);
      local_TSa37->refcount++;
    }
  zval** p_lhs = &local_TSa37;

    zval* rhs;
  if (local_TSa37 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa37;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa37[] = $TLE34;
{
     if (local_TSa37 == NULL)
    {
      local_TSa37 = EG (uninitialized_zval_ptr);
      local_TSa37->refcount++;
    }
  zval** p_array = &local_TSa37;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE34 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE34;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa37[] = $TLE35;
{
     if (local_TSa37 == NULL)
    {
      local_TSa37 = EG (uninitialized_zval_ptr);
      local_TSa37->refcount++;
    }
  zval** p_array = &local_TSa37;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE35 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE35;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa37[] = $TLE36;
{
     if (local_TSa37 == NULL)
    {
      local_TSa37 = EG (uninitialized_zval_ptr);
      local_TSa37->refcount++;
    }
  zval** p_array = &local_TSa37;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE36 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE36;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TLE38 = 2;
{
        if (local_TLE38 == NULL)
    {
      local_TLE38 = EG (uninitialized_zval_ptr);
      local_TLE38->refcount++;
    }
  zval** p_lhs = &local_TLE38;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 2);

phc_check_invariants (TSRMLS_C);
}
// $TLE39 = 'pipe';
{
        if (local_TLE39 == NULL)
    {
      local_TLE39 = EG (uninitialized_zval_ptr);
      local_TLE39->refcount++;
    }
  zval** p_lhs = &local_TLE39;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "pipe", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE40 = 'w';
{
        if (local_TLE40 == NULL)
    {
      local_TLE40 = EG (uninitialized_zval_ptr);
      local_TLE40->refcount++;
    }
  zval** p_lhs = &local_TLE40;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "w", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// unset($TSa41);
{
if (local_TSa41 != NULL)
{
zval_ptr_dtor (&local_TSa41);
local_TSa41 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa41 = (array) $TSa41;
{
      if (local_TSa41 == NULL)
    {
      local_TSa41 = EG (uninitialized_zval_ptr);
      local_TSa41->refcount++;
    }
  zval** p_lhs = &local_TSa41;

    zval* rhs;
  if (local_TSa41 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa41;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa41[] = $TLE39;
{
     if (local_TSa41 == NULL)
    {
      local_TSa41 = EG (uninitialized_zval_ptr);
      local_TSa41->refcount++;
    }
  zval** p_array = &local_TSa41;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE39 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE39;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa41[] = $TLE40;
{
     if (local_TSa41 == NULL)
    {
      local_TSa41 = EG (uninitialized_zval_ptr);
      local_TSa41->refcount++;
    }
  zval** p_array = &local_TSa41;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE40 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE40;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// unset($TSa42);
{
if (local_TSa42 != NULL)
{
zval_ptr_dtor (&local_TSa42);
local_TSa42 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa42 = (array) $TSa42;
{
      if (local_TSa42 == NULL)
    {
      local_TSa42 = EG (uninitialized_zval_ptr);
      local_TSa42->refcount++;
    }
  zval** p_lhs = &local_TSa42;

    zval* rhs;
  if (local_TSa42 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa42;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa42[$TLE29] = $TSa32;
{
     if (local_TSa42 == NULL)
    {
      local_TSa42 = EG (uninitialized_zval_ptr);
      local_TSa42->refcount++;
    }
  zval** p_array = &local_TSa42;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_TLE29 == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_TLE29;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TSa32 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa32;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TSa32 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa32;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// $TSa42[$TLE33] = $TSa37;
{
     if (local_TSa42 == NULL)
    {
      local_TSa42 = EG (uninitialized_zval_ptr);
      local_TSa42->refcount++;
    }
  zval** p_array = &local_TSa42;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_TLE33 == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_TLE33;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TSa37 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa37;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TSa37 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa37;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// $TSa42[$TLE38] = $TSa41;
{
     if (local_TSa42 == NULL)
    {
      local_TSa42 = EG (uninitialized_zval_ptr);
      local_TSa42->refcount++;
    }
  zval** p_array = &local_TSa42;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_TLE38 == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_TLE38;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TSa41 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa41;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TSa41 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa41;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// $descriptorspec = $TSa42;
{
    if (local_descriptorspec == NULL)
    {
      local_descriptorspec = EG (uninitialized_zval_ptr);
      local_descriptorspec->refcount++;
    }
  zval** p_lhs = &local_descriptorspec;

    zval* rhs;
  if (local_TSa42 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa42;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L124;
{
goto L124;
phc_check_invariants (TSRMLS_C);
}
// L123:
L123:;
// $TLE43 = 0;
{
        if (local_TLE43 == NULL)
    {
      local_TLE43 = EG (uninitialized_zval_ptr);
      local_TLE43->refcount++;
    }
  zval** p_lhs = &local_TLE43;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TLE44 = 'pipe';
{
        if (local_TLE44 == NULL)
    {
      local_TLE44 = EG (uninitialized_zval_ptr);
      local_TLE44->refcount++;
    }
  zval** p_lhs = &local_TLE44;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "pipe", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE45 = 'r';
{
        if (local_TLE45 == NULL)
    {
      local_TLE45 = EG (uninitialized_zval_ptr);
      local_TLE45->refcount++;
    }
  zval** p_lhs = &local_TLE45;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "r", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// unset($TSa46);
{
if (local_TSa46 != NULL)
{
zval_ptr_dtor (&local_TSa46);
local_TSa46 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa46 = (array) $TSa46;
{
      if (local_TSa46 == NULL)
    {
      local_TSa46 = EG (uninitialized_zval_ptr);
      local_TSa46->refcount++;
    }
  zval** p_lhs = &local_TSa46;

    zval* rhs;
  if (local_TSa46 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa46;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa46[] = $TLE44;
{
     if (local_TSa46 == NULL)
    {
      local_TSa46 = EG (uninitialized_zval_ptr);
      local_TSa46->refcount++;
    }
  zval** p_array = &local_TSa46;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE44 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE44;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa46[] = $TLE45;
{
     if (local_TSa46 == NULL)
    {
      local_TSa46 = EG (uninitialized_zval_ptr);
      local_TSa46->refcount++;
    }
  zval** p_array = &local_TSa46;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE45 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE45;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TLE47 = 1;
{
        if (local_TLE47 == NULL)
    {
      local_TLE47 = EG (uninitialized_zval_ptr);
      local_TLE47->refcount++;
    }
  zval** p_lhs = &local_TLE47;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE48 = 'pipe';
{
        if (local_TLE48 == NULL)
    {
      local_TLE48 = EG (uninitialized_zval_ptr);
      local_TLE48->refcount++;
    }
  zval** p_lhs = &local_TLE48;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "pipe", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE49 = 'w';
{
        if (local_TLE49 == NULL)
    {
      local_TLE49 = EG (uninitialized_zval_ptr);
      local_TLE49->refcount++;
    }
  zval** p_lhs = &local_TLE49;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "w", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// unset($TSa50);
{
if (local_TSa50 != NULL)
{
zval_ptr_dtor (&local_TSa50);
local_TSa50 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa50 = (array) $TSa50;
{
      if (local_TSa50 == NULL)
    {
      local_TSa50 = EG (uninitialized_zval_ptr);
      local_TSa50->refcount++;
    }
  zval** p_lhs = &local_TSa50;

    zval* rhs;
  if (local_TSa50 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa50;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa50[] = $TLE48;
{
     if (local_TSa50 == NULL)
    {
      local_TSa50 = EG (uninitialized_zval_ptr);
      local_TSa50->refcount++;
    }
  zval** p_array = &local_TSa50;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE48 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE48;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa50[] = $TLE49;
{
     if (local_TSa50 == NULL)
    {
      local_TSa50 = EG (uninitialized_zval_ptr);
      local_TSa50->refcount++;
    }
  zval** p_array = &local_TSa50;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE49 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE49;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TLE51 = 2;
{
        if (local_TLE51 == NULL)
    {
      local_TLE51 = EG (uninitialized_zval_ptr);
      local_TLE51->refcount++;
    }
  zval** p_lhs = &local_TLE51;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 2);

phc_check_invariants (TSRMLS_C);
}
// $TLE52 = 'file';
{
        if (local_TLE52 == NULL)
    {
      local_TLE52 = EG (uninitialized_zval_ptr);
      local_TLE52->refcount++;
    }
  zval** p_lhs = &local_TLE52;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "file", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE53 = wfgetnull();
{
   initialize_function_call (&wfgetnull_fci, &wfgetnull_fcic, "wfgetnull", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 92 TSRMLS_CC);
      zend_function* signature = wfgetnull_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 92, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfgetnull_fci.param_count;
   zval*** params_save = wfgetnull_fci.params;
   zval** retval_save = wfgetnull_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfgetnull_fci.params = args_ind;
   wfgetnull_fci.param_count = 0;
   wfgetnull_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfgetnull_fci, &wfgetnull_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfgetnull_fci.params = params_save;
   wfgetnull_fci.param_count = param_count_save;
   wfgetnull_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE53 == NULL)
    {
      local_TLE53 = EG (uninitialized_zval_ptr);
      local_TLE53->refcount++;
    }
  zval** p_lhs = &local_TLE53;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE54 = 'a';
{
        if (local_TLE54 == NULL)
    {
      local_TLE54 = EG (uninitialized_zval_ptr);
      local_TLE54->refcount++;
    }
  zval** p_lhs = &local_TLE54;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "a", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// unset($TSa55);
{
if (local_TSa55 != NULL)
{
zval_ptr_dtor (&local_TSa55);
local_TSa55 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa55 = (array) $TSa55;
{
      if (local_TSa55 == NULL)
    {
      local_TSa55 = EG (uninitialized_zval_ptr);
      local_TSa55->refcount++;
    }
  zval** p_lhs = &local_TSa55;

    zval* rhs;
  if (local_TSa55 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa55;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa55[] = $TLE52;
{
     if (local_TSa55 == NULL)
    {
      local_TSa55 = EG (uninitialized_zval_ptr);
      local_TSa55->refcount++;
    }
  zval** p_array = &local_TSa55;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE52 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE52;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa55[] = $TLE53;
{
     if (local_TSa55 == NULL)
    {
      local_TSa55 = EG (uninitialized_zval_ptr);
      local_TSa55->refcount++;
    }
  zval** p_array = &local_TSa55;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE53 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE53;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa55[] = $TLE54;
{
     if (local_TSa55 == NULL)
    {
      local_TSa55 = EG (uninitialized_zval_ptr);
      local_TSa55->refcount++;
    }
  zval** p_array = &local_TSa55;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE54 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE54;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// unset($TSa56);
{
if (local_TSa56 != NULL)
{
zval_ptr_dtor (&local_TSa56);
local_TSa56 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa56 = (array) $TSa56;
{
      if (local_TSa56 == NULL)
    {
      local_TSa56 = EG (uninitialized_zval_ptr);
      local_TSa56->refcount++;
    }
  zval** p_lhs = &local_TSa56;

    zval* rhs;
  if (local_TSa56 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa56;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa56[$TLE43] = $TSa46;
{
     if (local_TSa56 == NULL)
    {
      local_TSa56 = EG (uninitialized_zval_ptr);
      local_TSa56->refcount++;
    }
  zval** p_array = &local_TSa56;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_TLE43 == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_TLE43;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TSa46 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa46;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TSa46 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa46;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// $TSa56[$TLE47] = $TSa50;
{
     if (local_TSa56 == NULL)
    {
      local_TSa56 = EG (uninitialized_zval_ptr);
      local_TSa56->refcount++;
    }
  zval** p_array = &local_TSa56;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_TLE47 == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_TLE47;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TSa50 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa50;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TSa50 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa50;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// $TSa56[$TLE51] = $TSa55;
{
     if (local_TSa56 == NULL)
    {
      local_TSa56 = EG (uninitialized_zval_ptr);
      local_TSa56->refcount++;
    }
  zval** p_array = &local_TSa56;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_TLE51 == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_TLE51;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TSa55 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa55;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TSa55 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa55;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// $descriptorspec = $TSa56;
{
    if (local_descriptorspec == NULL)
    {
      local_descriptorspec = EG (uninitialized_zval_ptr);
      local_descriptorspec->refcount++;
    }
  zval** p_lhs = &local_descriptorspec;

    zval* rhs;
  if (local_TSa56 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa56;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L124;
{
goto L124;
phc_check_invariants (TSRMLS_C);
}
// L124:
L124:;
// if (stderr) goto L125 else goto L126;
{
     zval* p_cond;
  if (local_stderr == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_stderr;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L125;
   else
      goto L126;
phc_check_invariants (TSRMLS_C);
}
// L125:
L125:;
// $TEF4 = 2;
{
        if (local_TEF4 == NULL)
    {
      local_TEF4 = EG (uninitialized_zval_ptr);
      local_TEF4->refcount++;
    }
  zval** p_lhs = &local_TEF4;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 2);

phc_check_invariants (TSRMLS_C);
}
// goto L127;
{
goto L127;
phc_check_invariants (TSRMLS_C);
}
// L126:
L126:;
// $TEF4 = 1;
{
        if (local_TEF4 == NULL)
    {
      local_TEF4 = EG (uninitialized_zval_ptr);
      local_TEF4->refcount++;
    }
  zval** p_lhs = &local_TEF4;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// goto L127;
{
goto L127;
phc_check_invariants (TSRMLS_C);
}
// L127:
L127:;
// $readpipe = $TEF4;
{
    if (local_readpipe == NULL)
    {
      local_readpipe = EG (uninitialized_zval_ptr);
      local_readpipe->refcount++;
    }
  zval** p_lhs = &local_readpipe;

    zval* rhs;
  if (local_TEF4 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF4;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// unset($TSa57);
{
if (local_TSa57 != NULL)
{
zval_ptr_dtor (&local_TSa57);
local_TSa57 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa57 = (array) $TSa57;
{
      if (local_TSa57 == NULL)
    {
      local_TSa57 = EG (uninitialized_zval_ptr);
      local_TSa57->refcount++;
    }
  zval** p_lhs = &local_TSa57;

    zval* rhs;
  if (local_TSa57 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa57;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $pipes = $TSa57;
{
    if (local_pipes == NULL)
    {
      local_pipes = EG (uninitialized_zval_ptr);
      local_pipes->refcount++;
    }
  zval** p_lhs = &local_pipes;

    zval* rhs;
  if (local_TSa57 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa57;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// $TLE58 = 'proc_open';
{
        if (local_TLE58 == NULL)
    {
      local_TLE58 = EG (uninitialized_zval_ptr);
      local_TLE58->refcount++;
    }
  zval** p_lhs = &local_TLE58;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "proc_open", 9, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE59 = function_exists($TLE58);
{
   initialize_function_call (&function_exists_fci, &function_exists_fcic, "function_exists", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 98 TSRMLS_CC);
      zend_function* signature = function_exists_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE58 == NULL)
    {
      local_TLE58 = EG (uninitialized_zval_ptr);
      local_TLE58->refcount++;
    }
  zval** p_arg = &local_TLE58;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE58 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE58;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 98, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = function_exists_fci.param_count;
   zval*** params_save = function_exists_fci.params;
   zval** retval_save = function_exists_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   function_exists_fci.params = args_ind;
   function_exists_fci.param_count = 1;
   function_exists_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&function_exists_fci, &function_exists_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   function_exists_fci.params = params_save;
   function_exists_fci.param_count = param_count_save;
   function_exists_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE59 == NULL)
    {
      local_TLE59 = EG (uninitialized_zval_ptr);
      local_TLE59->refcount++;
    }
  zval** p_lhs = &local_TLE59;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// if (TLE59) goto L151 else goto L152;
{
     zval* p_cond;
  if (local_TLE59 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE59;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L151;
   else
      goto L152;
phc_check_invariants (TSRMLS_C);
}
// L151:
L151:;
// $TLE60 = ' -config ';
{
        if (local_TLE60 == NULL)
    {
      local_TLE60 = EG (uninitialized_zval_ptr);
      local_TLE60->refcount++;
    }
  zval** p_lhs = &local_TLE60;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, " -config ", 9, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE61 = ($wgTidyBin . $TLE60);
{
    if (local_TLE61 == NULL)
    {
      local_TLE61 = EG (uninitialized_zval_ptr);
      local_TLE61->refcount++;
    }
  zval** p_lhs = &local_TLE61;

    zval* left;
  if (local_wgTidyBin == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_wgTidyBin;

    zval* right;
  if (local_TLE60 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE60;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE62 = ($TLE61 . $wgTidyConf);
{
    if (local_TLE62 == NULL)
    {
      local_TLE62 = EG (uninitialized_zval_ptr);
      local_TLE62->refcount++;
    }
  zval** p_lhs = &local_TLE62;

    zval* left;
  if (local_TLE61 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE61;

    zval* right;
  if (local_wgTidyConf == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_wgTidyConf;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE63 = ' ';
{
        if (local_TLE63 == NULL)
    {
      local_TLE63 = EG (uninitialized_zval_ptr);
      local_TLE63->refcount++;
    }
  zval** p_lhs = &local_TLE63;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, " ", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE64 = ($TLE62 . $TLE63);
{
    if (local_TLE64 == NULL)
    {
      local_TLE64 = EG (uninitialized_zval_ptr);
      local_TLE64->refcount++;
    }
  zval** p_lhs = &local_TLE64;

    zval* left;
  if (local_TLE62 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE62;

    zval* right;
  if (local_TLE63 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE63;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE65 = ($TLE64 . $wgTidyOpts);
{
    if (local_TLE65 == NULL)
    {
      local_TLE65 = EG (uninitialized_zval_ptr);
      local_TLE65->refcount++;
    }
  zval** p_lhs = &local_TLE65;

    zval* left;
  if (local_TLE64 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE64;

    zval* right;
  if (local_wgTidyOpts == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_wgTidyOpts;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE66 = ($TLE65 . $opts);
{
    if (local_TLE66 == NULL)
    {
      local_TLE66 = EG (uninitialized_zval_ptr);
      local_TLE66->refcount++;
    }
  zval** p_lhs = &local_TLE66;

    zval* left;
  if (local_TLE65 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE65;

    zval* right;
  if (local_opts == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_opts;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $process = proc_open($TLE66, $descriptorspec, $pipes);
{
   initialize_function_call (&proc_open_fci, &proc_open_fcic, "proc_open", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 99 TSRMLS_CC);
      zend_function* signature = proc_open_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE66 == NULL)
    {
      local_TLE66 = EG (uninitialized_zval_ptr);
      local_TLE66->refcount++;
    }
  zval** p_arg = &local_TLE66;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE66 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE66;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_descriptorspec == NULL)
    {
      local_descriptorspec = EG (uninitialized_zval_ptr);
      local_descriptorspec->refcount++;
    }
  zval** p_arg = &local_descriptorspec;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_descriptorspec == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_descriptorspec;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_pipes == NULL)
    {
      local_pipes = EG (uninitialized_zval_ptr);
      local_pipes->refcount++;
    }
  zval** p_arg = &local_pipes;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_pipes == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_pipes;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 99, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = proc_open_fci.param_count;
   zval*** params_save = proc_open_fci.params;
   zval** retval_save = proc_open_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   proc_open_fci.params = args_ind;
   proc_open_fci.param_count = 3;
   proc_open_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&proc_open_fci, &proc_open_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   proc_open_fci.params = params_save;
   proc_open_fci.param_count = param_count_save;
   proc_open_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_process == NULL)
    {
      local_process = EG (uninitialized_zval_ptr);
      local_process->refcount++;
    }
  zval** p_lhs = &local_process;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE67 = is_resource($process);
{
   initialize_function_call (&is_resource_fci, &is_resource_fcic, "is_resource", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 100 TSRMLS_CC);
      zend_function* signature = is_resource_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_process == NULL)
    {
      local_process = EG (uninitialized_zval_ptr);
      local_process->refcount++;
    }
  zval** p_arg = &local_process;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_process == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_process;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 100, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = is_resource_fci.param_count;
   zval*** params_save = is_resource_fci.params;
   zval** retval_save = is_resource_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   is_resource_fci.params = args_ind;
   is_resource_fci.param_count = 1;
   is_resource_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&is_resource_fci, &is_resource_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   is_resource_fci.params = params_save;
   is_resource_fci.param_count = param_count_save;
   is_resource_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE67 == NULL)
    {
      local_TLE67 = EG (uninitialized_zval_ptr);
      local_TLE67->refcount++;
    }
  zval** p_lhs = &local_TLE67;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// if (TLE67) goto L148 else goto L149;
{
     zval* p_cond;
  if (local_TLE67 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE67;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L148;
   else
      goto L149;
phc_check_invariants (TSRMLS_C);
}
// L148:
L148:;
// $TLE68 = 0;
{
        if (local_TLE68 == NULL)
    {
      local_TLE68 = EG (uninitialized_zval_ptr);
      local_TLE68->refcount++;
    }
  zval** p_lhs = &local_TLE68;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TLE96 = param_is_ref (NULL, "fwrite", 0);
// ;
{
   initialize_function_call (&fwrite_fci, &fwrite_fcic, "fwrite", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = fwrite_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE96 == NULL)
    {
      local_TLE96 = EG (uninitialized_zval_ptr);
      local_TLE96->refcount++;
    }
  zval** p_lhs = &local_TLE96;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE96) goto L128 else goto L129;
{
     zval* p_cond;
  if (local_TLE96 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE96;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L128;
   else
      goto L129;
phc_check_invariants (TSRMLS_C);
}
// L128:
L128:;
// $TMIi95 =& $pipes[$TLE68];
{
     if (local_TMIi95 == NULL)
    {
      local_TMIi95 = EG (uninitialized_zval_ptr);
      local_TMIi95->refcount++;
    }
  zval** p_lhs = &local_TMIi95;

     if (local_pipes == NULL)
    {
      local_pipes = EG (uninitialized_zval_ptr);
      local_pipes->refcount++;
    }
  zval** p_r_array = &local_pipes;

     zval* r_index;
  if (local_TLE68 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE68;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L130;
{
goto L130;
phc_check_invariants (TSRMLS_C);
}
// L129:
L129:;
// $TMIi95 = $pipes[$TLE68];
{
     if (local_TMIi95 == NULL)
    {
      local_TMIi95 = EG (uninitialized_zval_ptr);
      local_TMIi95->refcount++;
    }
  zval** p_lhs = &local_TMIi95;

     zval* r_array;
  if (local_pipes == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_pipes;

     zval* r_index;
  if (local_TLE68 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE68;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L130;
{
goto L130;
phc_check_invariants (TSRMLS_C);
}
// L130:
L130:;
// fwrite($TMIi95, $text);
{
   initialize_function_call (&fwrite_fci, &fwrite_fcic, "fwrite", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 106 TSRMLS_CC);
      zend_function* signature = fwrite_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi95 == NULL)
    {
      local_TMIi95 = EG (uninitialized_zval_ptr);
      local_TMIi95->refcount++;
    }
  zval** p_arg = &local_TMIi95;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi95 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi95;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_text == NULL)
    {
      local_text = EG (uninitialized_zval_ptr);
      local_text->refcount++;
    }
  zval** p_arg = &local_text;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_text == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_text;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 106, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fwrite_fci.param_count;
   zval*** params_save = fwrite_fci.params;
   zval** retval_save = fwrite_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fwrite_fci.params = args_ind;
   fwrite_fci.param_count = 2;
   fwrite_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fwrite_fci, &fwrite_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fwrite_fci.params = params_save;
   fwrite_fci.param_count = param_count_save;
   fwrite_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE69 = 0;
{
        if (local_TLE69 == NULL)
    {
      local_TLE69 = EG (uninitialized_zval_ptr);
      local_TLE69->refcount++;
    }
  zval** p_lhs = &local_TLE69;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TLE98 = param_is_ref (NULL, "fclose", 0);
// ;
{
   initialize_function_call (&fclose_fci, &fclose_fcic, "fclose", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = fclose_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE98 == NULL)
    {
      local_TLE98 = EG (uninitialized_zval_ptr);
      local_TLE98->refcount++;
    }
  zval** p_lhs = &local_TLE98;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE98) goto L131 else goto L132;
{
     zval* p_cond;
  if (local_TLE98 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE98;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L131;
   else
      goto L132;
phc_check_invariants (TSRMLS_C);
}
// L131:
L131:;
// $TMIi97 =& $pipes[$TLE69];
{
     if (local_TMIi97 == NULL)
    {
      local_TMIi97 = EG (uninitialized_zval_ptr);
      local_TMIi97->refcount++;
    }
  zval** p_lhs = &local_TMIi97;

     if (local_pipes == NULL)
    {
      local_pipes = EG (uninitialized_zval_ptr);
      local_pipes->refcount++;
    }
  zval** p_r_array = &local_pipes;

     zval* r_index;
  if (local_TLE69 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE69;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L133;
{
goto L133;
phc_check_invariants (TSRMLS_C);
}
// L132:
L132:;
// $TMIi97 = $pipes[$TLE69];
{
     if (local_TMIi97 == NULL)
    {
      local_TMIi97 = EG (uninitialized_zval_ptr);
      local_TMIi97->refcount++;
    }
  zval** p_lhs = &local_TMIi97;

     zval* r_array;
  if (local_pipes == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_pipes;

     zval* r_index;
  if (local_TLE69 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE69;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L133;
{
goto L133;
phc_check_invariants (TSRMLS_C);
}
// L133:
L133:;
// fclose($TMIi97);
{
   initialize_function_call (&fclose_fci, &fclose_fcic, "fclose", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 107 TSRMLS_CC);
      zend_function* signature = fclose_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi97 == NULL)
    {
      local_TMIi97 = EG (uninitialized_zval_ptr);
      local_TMIi97->refcount++;
    }
  zval** p_arg = &local_TMIi97;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi97 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi97;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 107, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fclose_fci.param_count;
   zval*** params_save = fclose_fci.params;
   zval** retval_save = fclose_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fclose_fci.params = args_ind;
   fclose_fci.param_count = 1;
   fclose_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fclose_fci, &fclose_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fclose_fci.params = params_save;
   fclose_fci.param_count = param_count_save;
   fclose_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// L144:
L144:;
// $TLE100 = param_is_ref (NULL, "feof", 0);
// ;
{
   initialize_function_call (&feof_fci, &feof_fcic, "feof", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = feof_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE100 == NULL)
    {
      local_TLE100 = EG (uninitialized_zval_ptr);
      local_TLE100->refcount++;
    }
  zval** p_lhs = &local_TLE100;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE100) goto L134 else goto L135;
{
     zval* p_cond;
  if (local_TLE100 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE100;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L134;
   else
      goto L135;
phc_check_invariants (TSRMLS_C);
}
// L134:
L134:;
// $TMIi99 =& $pipes[$readpipe];
{
     if (local_TMIi99 == NULL)
    {
      local_TMIi99 = EG (uninitialized_zval_ptr);
      local_TMIi99->refcount++;
    }
  zval** p_lhs = &local_TMIi99;

     if (local_pipes == NULL)
    {
      local_pipes = EG (uninitialized_zval_ptr);
      local_pipes->refcount++;
    }
  zval** p_r_array = &local_pipes;

     zval* r_index;
  if (local_readpipe == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_readpipe;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L136;
{
goto L136;
phc_check_invariants (TSRMLS_C);
}
// L135:
L135:;
// $TMIi99 = $pipes[$readpipe];
{
     if (local_TMIi99 == NULL)
    {
      local_TMIi99 = EG (uninitialized_zval_ptr);
      local_TMIi99->refcount++;
    }
  zval** p_lhs = &local_TMIi99;

     zval* r_array;
  if (local_pipes == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_pipes;

     zval* r_index;
  if (local_readpipe == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_readpipe;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L136;
{
goto L136;
phc_check_invariants (TSRMLS_C);
}
// L136:
L136:;
// $TLE70 = feof($TMIi99);
{
   initialize_function_call (&feof_fci, &feof_fcic, "feof", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 108 TSRMLS_CC);
      zend_function* signature = feof_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi99 == NULL)
    {
      local_TMIi99 = EG (uninitialized_zval_ptr);
      local_TMIi99->refcount++;
    }
  zval** p_arg = &local_TMIi99;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi99 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi99;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 108, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = feof_fci.param_count;
   zval*** params_save = feof_fci.params;
   zval** retval_save = feof_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   feof_fci.params = args_ind;
   feof_fci.param_count = 1;
   feof_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&feof_fci, &feof_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   feof_fci.params = params_save;
   feof_fci.param_count = param_count_save;
   feof_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE70 == NULL)
    {
      local_TLE70 = EG (uninitialized_zval_ptr);
      local_TLE70->refcount++;
    }
  zval** p_lhs = &local_TLE70;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE71 = !$TLE70;
{
     if (local_TLE71 == NULL)
    {
      local_TLE71 = EG (uninitialized_zval_ptr);
      local_TLE71->refcount++;
    }
  zval** p_lhs = &local_TLE71;

     zval* rhs;
  if (local_TLE70 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE70;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE72 = !$TLE71;
{
     if (local_TLE72 == NULL)
    {
      local_TLE72 = EG (uninitialized_zval_ptr);
      local_TLE72->refcount++;
    }
  zval** p_lhs = &local_TLE72;

     zval* rhs;
  if (local_TLE71 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE71;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE72) goto L138 else goto L139;
{
     zval* p_cond;
  if (local_TLE72 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE72;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L138;
   else
      goto L139;
phc_check_invariants (TSRMLS_C);
}
// L138:
L138:;
// goto L137;
{
goto L137;
phc_check_invariants (TSRMLS_C);
}
// goto L140;
{
goto L140;
phc_check_invariants (TSRMLS_C);
}
// L139:
L139:;
// goto L140;
{
goto L140;
phc_check_invariants (TSRMLS_C);
}
// L140:
L140:;
// $TLE73 = 1024;
{
        if (local_TLE73 == NULL)
    {
      local_TLE73 = EG (uninitialized_zval_ptr);
      local_TLE73->refcount++;
    }
  zval** p_lhs = &local_TLE73;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1024);

phc_check_invariants (TSRMLS_C);
}
// $TLE102 = param_is_ref (NULL, "fgets", 0);
// ;
{
   initialize_function_call (&fgets_fci, &fgets_fcic, "fgets", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = fgets_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE102 == NULL)
    {
      local_TLE102 = EG (uninitialized_zval_ptr);
      local_TLE102->refcount++;
    }
  zval** p_lhs = &local_TLE102;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE102) goto L141 else goto L142;
{
     zval* p_cond;
  if (local_TLE102 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE102;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L141;
   else
      goto L142;
phc_check_invariants (TSRMLS_C);
}
// L141:
L141:;
// $TMIi101 =& $pipes[$readpipe];
{
     if (local_TMIi101 == NULL)
    {
      local_TMIi101 = EG (uninitialized_zval_ptr);
      local_TMIi101->refcount++;
    }
  zval** p_lhs = &local_TMIi101;

     if (local_pipes == NULL)
    {
      local_pipes = EG (uninitialized_zval_ptr);
      local_pipes->refcount++;
    }
  zval** p_r_array = &local_pipes;

     zval* r_index;
  if (local_readpipe == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_readpipe;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L143;
{
goto L143;
phc_check_invariants (TSRMLS_C);
}
// L142:
L142:;
// $TMIi101 = $pipes[$readpipe];
{
     if (local_TMIi101 == NULL)
    {
      local_TMIi101 = EG (uninitialized_zval_ptr);
      local_TMIi101->refcount++;
    }
  zval** p_lhs = &local_TMIi101;

     zval* r_array;
  if (local_pipes == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_pipes;

     zval* r_index;
  if (local_readpipe == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_readpipe;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L143;
{
goto L143;
phc_check_invariants (TSRMLS_C);
}
// L143:
L143:;
// $TLE74 = fgets($TMIi101, $TLE73);
{
   initialize_function_call (&fgets_fci, &fgets_fcic, "fgets", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 109 TSRMLS_CC);
      zend_function* signature = fgets_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi101 == NULL)
    {
      local_TMIi101 = EG (uninitialized_zval_ptr);
      local_TMIi101->refcount++;
    }
  zval** p_arg = &local_TMIi101;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi101 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi101;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE73 == NULL)
    {
      local_TLE73 = EG (uninitialized_zval_ptr);
      local_TLE73->refcount++;
    }
  zval** p_arg = &local_TLE73;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE73 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE73;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 109, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fgets_fci.param_count;
   zval*** params_save = fgets_fci.params;
   zval** retval_save = fgets_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fgets_fci.params = args_ind;
   fgets_fci.param_count = 2;
   fgets_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fgets_fci, &fgets_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fgets_fci.params = params_save;
   fgets_fci.param_count = param_count_save;
   fgets_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE74 == NULL)
    {
      local_TLE74 = EG (uninitialized_zval_ptr);
      local_TLE74->refcount++;
    }
  zval** p_lhs = &local_TLE74;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $cleansource = ($cleansource . $TLE74);
{
    if (local_cleansource == NULL)
    {
      local_cleansource = EG (uninitialized_zval_ptr);
      local_cleansource->refcount++;
    }
  zval** p_lhs = &local_cleansource;

    zval* left;
  if (local_cleansource == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_cleansource;

    zval* right;
  if (local_TLE74 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE74;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L144;
{
goto L144;
phc_check_invariants (TSRMLS_C);
}
// L137:
L137:;
// $TLE104 = param_is_ref (NULL, "fclose", 0);
// ;
{
   initialize_function_call (&fclose_fci, &fclose_fcic, "fclose", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = fclose_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE104 == NULL)
    {
      local_TLE104 = EG (uninitialized_zval_ptr);
      local_TLE104->refcount++;
    }
  zval** p_lhs = &local_TLE104;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE104) goto L145 else goto L146;
{
     zval* p_cond;
  if (local_TLE104 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE104;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L145;
   else
      goto L146;
phc_check_invariants (TSRMLS_C);
}
// L145:
L145:;
// $TMIi103 =& $pipes[$readpipe];
{
     if (local_TMIi103 == NULL)
    {
      local_TMIi103 = EG (uninitialized_zval_ptr);
      local_TMIi103->refcount++;
    }
  zval** p_lhs = &local_TMIi103;

     if (local_pipes == NULL)
    {
      local_pipes = EG (uninitialized_zval_ptr);
      local_pipes->refcount++;
    }
  zval** p_r_array = &local_pipes;

     zval* r_index;
  if (local_readpipe == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_readpipe;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L147;
{
goto L147;
phc_check_invariants (TSRMLS_C);
}
// L146:
L146:;
// $TMIi103 = $pipes[$readpipe];
{
     if (local_TMIi103 == NULL)
    {
      local_TMIi103 = EG (uninitialized_zval_ptr);
      local_TMIi103->refcount++;
    }
  zval** p_lhs = &local_TMIi103;

     zval* r_array;
  if (local_pipes == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_pipes;

     zval* r_index;
  if (local_readpipe == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_readpipe;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L147;
{
goto L147;
phc_check_invariants (TSRMLS_C);
}
// L147:
L147:;
// fclose($TMIi103);
{
   initialize_function_call (&fclose_fci, &fclose_fcic, "fclose", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 111 TSRMLS_CC);
      zend_function* signature = fclose_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi103 == NULL)
    {
      local_TMIi103 = EG (uninitialized_zval_ptr);
      local_TMIi103->refcount++;
    }
  zval** p_arg = &local_TMIi103;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi103 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi103;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 111, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fclose_fci.param_count;
   zval*** params_save = fclose_fci.params;
   zval** retval_save = fclose_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fclose_fci.params = args_ind;
   fclose_fci.param_count = 1;
   fclose_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fclose_fci, &fclose_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fclose_fci.params = params_save;
   fclose_fci.param_count = param_count_save;
   fclose_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $retval = proc_close($process);
{
   initialize_function_call (&proc_close_fci, &proc_close_fcic, "proc_close", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 112 TSRMLS_CC);
      zend_function* signature = proc_close_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_process == NULL)
    {
      local_process = EG (uninitialized_zval_ptr);
      local_process->refcount++;
    }
  zval** p_arg = &local_process;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_process == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_process;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 112, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = proc_close_fci.param_count;
   zval*** params_save = proc_close_fci.params;
   zval** retval_save = proc_close_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   proc_close_fci.params = args_ind;
   proc_close_fci.param_count = 1;
   proc_close_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&proc_close_fci, &proc_close_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   proc_close_fci.params = params_save;
   proc_close_fci.param_count = param_count_save;
   proc_close_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_retval == NULL)
    {
      local_retval = EG (uninitialized_zval_ptr);
      local_retval->refcount++;
    }
  zval** p_lhs = &local_retval;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// goto L150;
{
goto L150;
phc_check_invariants (TSRMLS_C);
}
// L149:
L149:;
// $retval = -1;
{
        if (local_retval == NULL)
    {
      local_retval = EG (uninitialized_zval_ptr);
      local_retval->refcount++;
    }
  zval** p_lhs = &local_retval;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, -1);

phc_check_invariants (TSRMLS_C);
}
// goto L150;
{
goto L150;
phc_check_invariants (TSRMLS_C);
}
// L150:
L150:;
// goto L153;
{
goto L153;
phc_check_invariants (TSRMLS_C);
}
// L152:
L152:;
// $retval = -1;
{
        if (local_retval == NULL)
    {
      local_retval = EG (uninitialized_zval_ptr);
      local_retval->refcount++;
    }
  zval** p_lhs = &local_retval;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, -1);

phc_check_invariants (TSRMLS_C);
}
// goto L153;
{
goto L153;
phc_check_invariants (TSRMLS_C);
}
// L153:
L153:;
// $TLE75 = 'MWTidy::execExternalTidy';
{
        if (local_TLE75 == NULL)
    {
      local_TLE75 = EG (uninitialized_zval_ptr);
      local_TLE75->refcount++;
    }
  zval** p_lhs = &local_TLE75;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "MWTidy::execExternalTidy", 24, 1);

phc_check_invariants (TSRMLS_C);
}
// wfprofileout($TLE75);
{
   initialize_function_call (&wfprofileout_fci, &wfprofileout_fcic, "wfprofileout", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 120 TSRMLS_CC);
      zend_function* signature = wfprofileout_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE75 == NULL)
    {
      local_TLE75 = EG (uninitialized_zval_ptr);
      local_TLE75->refcount++;
    }
  zval** p_arg = &local_TLE75;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE75 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE75;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 120, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfprofileout_fci.param_count;
   zval*** params_save = wfprofileout_fci.params;
   zval** retval_save = wfprofileout_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfprofileout_fci.params = args_ind;
   wfprofileout_fci.param_count = 1;
   wfprofileout_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfprofileout_fci, &wfprofileout_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfprofileout_fci.params = params_save;
   wfprofileout_fci.param_count = param_count_save;
   wfprofileout_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE5 = !$stderr;
{
     if (local_TLE5 == NULL)
    {
      local_TLE5 = EG (uninitialized_zval_ptr);
      local_TLE5->refcount++;
    }
  zval** p_lhs = &local_TLE5;

     zval* rhs;
  if (local_stderr == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_stderr;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE5) goto L154 else goto L155;
{
     zval* p_cond;
  if (local_TLE5 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE5;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L154;
   else
      goto L155;
phc_check_invariants (TSRMLS_C);
}
// L154:
L154:;
// $TLE76 = '';
{
        if (local_TLE76 == NULL)
    {
      local_TLE76 = EG (uninitialized_zval_ptr);
      local_TLE76->refcount++;
    }
  zval** p_lhs = &local_TLE76;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
// $TEF6 = ($cleansource == $TLE76);
{
    if (local_TEF6 == NULL)
    {
      local_TEF6 = EG (uninitialized_zval_ptr);
      local_TEF6->refcount++;
    }
  zval** p_lhs = &local_TEF6;

    zval* left;
  if (local_cleansource == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_cleansource;

    zval* right;
  if (local_TLE76 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE76;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L156;
{
goto L156;
phc_check_invariants (TSRMLS_C);
}
// L155:
L155:;
// $TEF6 = $TLE5;
{
    if (local_TEF6 == NULL)
    {
      local_TEF6 = EG (uninitialized_zval_ptr);
      local_TEF6->refcount++;
    }
  zval** p_lhs = &local_TEF6;

    zval* rhs;
  if (local_TLE5 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE5;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L156;
{
goto L156;
phc_check_invariants (TSRMLS_C);
}
// L156:
L156:;
// $TLE7 = (bool) $TEF6;
{
      if (local_TLE7 == NULL)
    {
      local_TLE7 = EG (uninitialized_zval_ptr);
      local_TLE7->refcount++;
    }
  zval** p_lhs = &local_TLE7;

    zval* rhs;
  if (local_TEF6 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF6;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// if (TLE7) goto L157 else goto L158;
{
     zval* p_cond;
  if (local_TLE7 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE7;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L157;
   else
      goto L158;
phc_check_invariants (TSRMLS_C);
}
// L157:
L157:;
// $TLE77 = '';
{
        if (local_TLE77 == NULL)
    {
      local_TLE77 = EG (uninitialized_zval_ptr);
      local_TLE77->refcount++;
    }
  zval** p_lhs = &local_TLE77;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
// $TEF8 = ($text != $TLE77);
{
    if (local_TEF8 == NULL)
    {
      local_TEF8 = EG (uninitialized_zval_ptr);
      local_TEF8->refcount++;
    }
  zval** p_lhs = &local_TEF8;

    zval* left;
  if (local_text == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_text;

    zval* right;
  if (local_TLE77 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE77;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_not_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L159;
{
goto L159;
phc_check_invariants (TSRMLS_C);
}
// L158:
L158:;
// $TEF8 = $TLE7;
{
    if (local_TEF8 == NULL)
    {
      local_TEF8 = EG (uninitialized_zval_ptr);
      local_TEF8->refcount++;
    }
  zval** p_lhs = &local_TEF8;

    zval* rhs;
  if (local_TLE7 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE7;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L159;
{
goto L159;
phc_check_invariants (TSRMLS_C);
}
// L159:
L159:;
// $TLE78 = (bool) $TEF8;
{
      if (local_TLE78 == NULL)
    {
      local_TLE78 = EG (uninitialized_zval_ptr);
      local_TLE78->refcount++;
    }
  zval** p_lhs = &local_TLE78;

    zval* rhs;
  if (local_TEF8 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF8;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// if (TLE78) goto L160 else goto L161;
{
     zval* p_cond;
  if (local_TLE78 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE78;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L160;
   else
      goto L161;
phc_check_invariants (TSRMLS_C);
}
// L160:
L160:;
// $TLE79 = NULL;
{
        if (local_TLE79 == NULL)
    {
      local_TLE79 = EG (uninitialized_zval_ptr);
      local_TLE79->refcount++;
    }
  zval** p_lhs = &local_TLE79;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
// return $TLE79;
{
     zval* rhs;
  if (local_TLE79 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE79;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// goto L162;
{
goto L162;
phc_check_invariants (TSRMLS_C);
}
// L161:
L161:;
// return $cleansource;
{
     zval* rhs;
  if (local_cleansource == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_cleansource;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// goto L162;
{
goto L162;
phc_check_invariants (TSRMLS_C);
}
// L162:
L162:;
// Method exit
end_of_function:__attribute__((unused));
if (local_TEF4 != NULL)
{
zval_ptr_dtor (&local_TEF4);
}
if (local_TEF6 != NULL)
{
zval_ptr_dtor (&local_TEF6);
}
if (local_TEF8 != NULL)
{
zval_ptr_dtor (&local_TEF8);
}
if (local_TLE100 != NULL)
{
zval_ptr_dtor (&local_TLE100);
}
if (local_TLE102 != NULL)
{
zval_ptr_dtor (&local_TLE102);
}
if (local_TLE104 != NULL)
{
zval_ptr_dtor (&local_TLE104);
}
if (local_TLE28 != NULL)
{
zval_ptr_dtor (&local_TLE28);
}
if (local_TLE29 != NULL)
{
zval_ptr_dtor (&local_TLE29);
}
if (local_TLE30 != NULL)
{
zval_ptr_dtor (&local_TLE30);
}
if (local_TLE31 != NULL)
{
zval_ptr_dtor (&local_TLE31);
}
if (local_TLE33 != NULL)
{
zval_ptr_dtor (&local_TLE33);
}
if (local_TLE34 != NULL)
{
zval_ptr_dtor (&local_TLE34);
}
if (local_TLE35 != NULL)
{
zval_ptr_dtor (&local_TLE35);
}
if (local_TLE36 != NULL)
{
zval_ptr_dtor (&local_TLE36);
}
if (local_TLE38 != NULL)
{
zval_ptr_dtor (&local_TLE38);
}
if (local_TLE39 != NULL)
{
zval_ptr_dtor (&local_TLE39);
}
if (local_TLE40 != NULL)
{
zval_ptr_dtor (&local_TLE40);
}
if (local_TLE43 != NULL)
{
zval_ptr_dtor (&local_TLE43);
}
if (local_TLE44 != NULL)
{
zval_ptr_dtor (&local_TLE44);
}
if (local_TLE45 != NULL)
{
zval_ptr_dtor (&local_TLE45);
}
if (local_TLE47 != NULL)
{
zval_ptr_dtor (&local_TLE47);
}
if (local_TLE48 != NULL)
{
zval_ptr_dtor (&local_TLE48);
}
if (local_TLE49 != NULL)
{
zval_ptr_dtor (&local_TLE49);
}
if (local_TLE5 != NULL)
{
zval_ptr_dtor (&local_TLE5);
}
if (local_TLE51 != NULL)
{
zval_ptr_dtor (&local_TLE51);
}
if (local_TLE52 != NULL)
{
zval_ptr_dtor (&local_TLE52);
}
if (local_TLE53 != NULL)
{
zval_ptr_dtor (&local_TLE53);
}
if (local_TLE54 != NULL)
{
zval_ptr_dtor (&local_TLE54);
}
if (local_TLE58 != NULL)
{
zval_ptr_dtor (&local_TLE58);
}
if (local_TLE59 != NULL)
{
zval_ptr_dtor (&local_TLE59);
}
if (local_TLE60 != NULL)
{
zval_ptr_dtor (&local_TLE60);
}
if (local_TLE61 != NULL)
{
zval_ptr_dtor (&local_TLE61);
}
if (local_TLE62 != NULL)
{
zval_ptr_dtor (&local_TLE62);
}
if (local_TLE63 != NULL)
{
zval_ptr_dtor (&local_TLE63);
}
if (local_TLE64 != NULL)
{
zval_ptr_dtor (&local_TLE64);
}
if (local_TLE65 != NULL)
{
zval_ptr_dtor (&local_TLE65);
}
if (local_TLE66 != NULL)
{
zval_ptr_dtor (&local_TLE66);
}
if (local_TLE67 != NULL)
{
zval_ptr_dtor (&local_TLE67);
}
if (local_TLE68 != NULL)
{
zval_ptr_dtor (&local_TLE68);
}
if (local_TLE69 != NULL)
{
zval_ptr_dtor (&local_TLE69);
}
if (local_TLE7 != NULL)
{
zval_ptr_dtor (&local_TLE7);
}
if (local_TLE70 != NULL)
{
zval_ptr_dtor (&local_TLE70);
}
if (local_TLE71 != NULL)
{
zval_ptr_dtor (&local_TLE71);
}
if (local_TLE72 != NULL)
{
zval_ptr_dtor (&local_TLE72);
}
if (local_TLE73 != NULL)
{
zval_ptr_dtor (&local_TLE73);
}
if (local_TLE74 != NULL)
{
zval_ptr_dtor (&local_TLE74);
}
if (local_TLE75 != NULL)
{
zval_ptr_dtor (&local_TLE75);
}
if (local_TLE76 != NULL)
{
zval_ptr_dtor (&local_TLE76);
}
if (local_TLE77 != NULL)
{
zval_ptr_dtor (&local_TLE77);
}
if (local_TLE78 != NULL)
{
zval_ptr_dtor (&local_TLE78);
}
if (local_TLE79 != NULL)
{
zval_ptr_dtor (&local_TLE79);
}
if (local_TLE96 != NULL)
{
zval_ptr_dtor (&local_TLE96);
}
if (local_TLE98 != NULL)
{
zval_ptr_dtor (&local_TLE98);
}
if (local_TMIi101 != NULL)
{
zval_ptr_dtor (&local_TMIi101);
}
if (local_TMIi103 != NULL)
{
zval_ptr_dtor (&local_TMIi103);
}
if (local_TMIi95 != NULL)
{
zval_ptr_dtor (&local_TMIi95);
}
if (local_TMIi97 != NULL)
{
zval_ptr_dtor (&local_TMIi97);
}
if (local_TMIi99 != NULL)
{
zval_ptr_dtor (&local_TMIi99);
}
if (local_TSa32 != NULL)
{
zval_ptr_dtor (&local_TSa32);
}
if (local_TSa37 != NULL)
{
zval_ptr_dtor (&local_TSa37);
}
if (local_TSa41 != NULL)
{
zval_ptr_dtor (&local_TSa41);
}
if (local_TSa42 != NULL)
{
zval_ptr_dtor (&local_TSa42);
}
if (local_TSa46 != NULL)
{
zval_ptr_dtor (&local_TSa46);
}
if (local_TSa50 != NULL)
{
zval_ptr_dtor (&local_TSa50);
}
if (local_TSa55 != NULL)
{
zval_ptr_dtor (&local_TSa55);
}
if (local_TSa56 != NULL)
{
zval_ptr_dtor (&local_TSa56);
}
if (local_TSa57 != NULL)
{
zval_ptr_dtor (&local_TSa57);
}
if (local_cleansource != NULL)
{
zval_ptr_dtor (&local_cleansource);
}
if (local_descriptorspec != NULL)
{
zval_ptr_dtor (&local_descriptorspec);
}
if (local_opts != NULL)
{
zval_ptr_dtor (&local_opts);
}
if (local_pipes != NULL)
{
zval_ptr_dtor (&local_pipes);
}
if (local_process != NULL)
{
zval_ptr_dtor (&local_process);
}
if (local_readpipe != NULL)
{
zval_ptr_dtor (&local_readpipe);
}
if (local_retval != NULL)
{
zval_ptr_dtor (&local_retval);
}
if (local_stderr != NULL)
{
zval_ptr_dtor (&local_stderr);
}
if (local_text != NULL)
{
zval_ptr_dtor (&local_text);
}
if (local_wgTidyBin != NULL)
{
zval_ptr_dtor (&local_wgTidyBin);
}
if (local_wgTidyConf != NULL)
{
zval_ptr_dtor (&local_wgTidyConf);
}
if (local_wgTidyOpts != NULL)
{
zval_ptr_dtor (&local_wgTidyOpts);
}
}
// private static function execinternaltidy($text, $stderr = False, &$retval = NULL)
// {
// 	global $wgTidyConf;
// 	global $IP;
// 	global $wgDebugTidy;
// 	$TLE80 = 'MWTidy::execInternalTidy';
// 	wfprofilein($TLE80);
// 	$tidy = new tidy();
// 	$TLE81 = 'utf8';
// 	$tidy->parsestring($text, $wgTidyConf, $TLE81);
// 	if (stderr) goto L175 else goto L176;
// L175:
// 	$retval = $tidy->getstatus();
// 	$TSt82 = $tidy->errorBuffer;
// 	return $TSt82;
// 	goto L177;
// L176:
// 	$tidy->cleanrepair();
// 	$retval = $tidy->getstatus();
// 	$TLE83 = 2;
// 	$TLE84 = ($retval == $TLE83);
// 	if (TLE84) goto L163 else goto L164;
// L163:
// 	$cleansource = NULL;
// 	goto L165;
// L164:
// 	$cleansource = tidy_get_output($tidy);
// 	goto L165;
// L165:
// 	$TLE9 = $wgDebugTidy;
// 	if (TLE9) goto L166 else goto L167;
// L166:
// 	$TLE85 = 0;
// 	$TEF10 = ($TLE85 < $retval);
// 	goto L168;
// L167:
// 	$TEF10 = $TLE9;
// 	goto L168;
// L168:
// 	$TLE86 = (bool) $TEF10;
// 	if (TLE86) goto L172 else goto L173;
// L172:
// 	$TLE87 = '<!--
// Tidy reports:
// ';
// 	$TLE88 = '-->';
// 	$TLE89 = '--&gt;';
// 	$TLE106 = param_is_ref (NULL, "str_replace", 0);
// 	;
// 	if (TLE106) goto L169 else goto L170;
// L169:
// 	$TMIt105 =& $tidy->errorBuffer;
// 	goto L171;
// L170:
// 	$TMIt105 = $tidy->errorBuffer;
// 	goto L171;
// L171:
// 	$TLE90 = str_replace($TLE88, $TLE89, $TMIt105);
// 	$TLE91 = ($TLE87 . $TLE90);
// 	$TLE92 = '
// -->';
// 	$TLE93 = ($TLE91 . $TLE92);
// 	$cleansource = ($cleansource . $TLE93);
// 	goto L174;
// L173:
// 	goto L174;
// L174:
// 	$TLE94 = 'MWTidy::execInternalTidy';
// 	wfprofileout($TLE94);
// 	return $cleansource;
// 	goto L177;
// L177:
// }
PHP_METHOD(MWTidy, execinternaltidy)
{
zval* local_IP = NULL;
zval* local_TEF10 = NULL;
zval* local_TLE106 = NULL;
zval* local_TLE80 = NULL;
zval* local_TLE81 = NULL;
zval* local_TLE83 = NULL;
zval* local_TLE84 = NULL;
zval* local_TLE85 = NULL;
zval* local_TLE86 = NULL;
zval* local_TLE87 = NULL;
zval* local_TLE88 = NULL;
zval* local_TLE89 = NULL;
zval* local_TLE9 = NULL;
zval* local_TLE90 = NULL;
zval* local_TLE91 = NULL;
zval* local_TLE92 = NULL;
zval* local_TLE93 = NULL;
zval* local_TLE94 = NULL;
zval* local_TMIt105 = NULL;
zval* local_TSt82 = NULL;
zval* local_cleansource = NULL;
zval* local_retval = NULL;
zval* local_stderr = NULL;
zval* local_text = NULL;
zval* local_tidy = NULL;
zval* local_wgDebugTidy = NULL;
zval* local_wgTidyConf = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[3];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_text != NULL)
{
	zval_ptr_dtor (&local_text);
}
local_text = params[0];
// param 1
if (num_args <= 1)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = False;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[1] = default_value;
}
params[1]->refcount++;
if (local_stderr != NULL)
{
	zval_ptr_dtor (&local_stderr);
}
local_stderr = params[1];
// param 2
if (num_args <= 2)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[2] = default_value;
}
params[2]->refcount++;
if (local_retval != NULL)
{
	zval_ptr_dtor (&local_retval);
}
local_retval = params[2];
}
// Function body
// global $wgTidyConf;
{
     if (local_wgTidyConf == NULL)
    {
      local_wgTidyConf = EG (uninitialized_zval_ptr);
      local_wgTidyConf->refcount++;
    }
  zval** p_local = &local_wgTidyConf;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgTidyConf", 10 + 1, 3667061347u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $IP;
{
     if (local_IP == NULL)
    {
      local_IP = EG (uninitialized_zval_ptr);
      local_IP->refcount++;
    }
  zval** p_local = &local_IP;

     zval** p_global = get_st_entry (&EG(symbol_table), "IP", 2 + 1, 193459134u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgDebugTidy;
{
     if (local_wgDebugTidy == NULL)
    {
      local_wgDebugTidy = EG (uninitialized_zval_ptr);
      local_wgDebugTidy->refcount++;
    }
  zval** p_local = &local_wgDebugTidy;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgDebugTidy", 11 + 1, 1610034724u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// $TLE80 = 'MWTidy::execInternalTidy';
{
        if (local_TLE80 == NULL)
    {
      local_TLE80 = EG (uninitialized_zval_ptr);
      local_TLE80->refcount++;
    }
  zval** p_lhs = &local_TLE80;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "MWTidy::execInternalTidy", 24, 1);

phc_check_invariants (TSRMLS_C);
}
// wfprofilein($TLE80);
{
   initialize_function_call (&wfprofilein_fci, &wfprofilein_fcic, "wfprofilein", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 139 TSRMLS_CC);
      zend_function* signature = wfprofilein_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE80 == NULL)
    {
      local_TLE80 = EG (uninitialized_zval_ptr);
      local_TLE80->refcount++;
    }
  zval** p_arg = &local_TLE80;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE80 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE80;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 139, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfprofilein_fci.param_count;
   zval*** params_save = wfprofilein_fci.params;
   zval** retval_save = wfprofilein_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfprofilein_fci.params = args_ind;
   wfprofilein_fci.param_count = 1;
   wfprofilein_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfprofilein_fci, &wfprofilein_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfprofilein_fci.params = params_save;
   wfprofilein_fci.param_count = param_count_save;
   wfprofilein_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $tidy = new tidy();
{
	     if (local_tidy == NULL)
    {
      local_tidy = EG (uninitialized_zval_ptr);
      local_tidy->refcount++;
    }
  zval** p_lhs = &local_tidy;

   zval* lhs;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     lhs = *p_lhs;
     zval_dtor (lhs);
   }
   else
   {
     ALLOC_INIT_ZVAL (lhs);
     zval_ptr_dtor (p_lhs);
     *p_lhs = lhs;
   }

	zend_class_entry* ce;
	ce = zend_fetch_class ("tidy", strlen("tidy"), ZEND_FETCH_CLASS_DEFAULT TSRMLS_CC);
	object_init_ex(lhs, ce); 
    if (local_tidy == NULL)
    {
      local_tidy = EG (uninitialized_zval_ptr);
      local_tidy->refcount++;
    }
  zval** p_obj = &local_tidy;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   int has_constructor = initialize_constructor_call (&fci_object, &fcic_object, p_obj, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 141 TSRMLS_CC);
   // TODO: We pass in __construct to call_function, but in general it may be a
   // a different name (the name of the class, for example).
   // However, \call_function does not actually use this name at all atm.
   if(has_constructor)
   {
        zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 141, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

   }
phc_check_invariants (TSRMLS_C);
}
// $TLE81 = 'utf8';
{
        if (local_TLE81 == NULL)
    {
      local_TLE81 = EG (uninitialized_zval_ptr);
      local_TLE81->refcount++;
    }
  zval** p_lhs = &local_TLE81;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "utf8", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $tidy->parsestring($text, $wgTidyConf, $TLE81);
{
     if (local_tidy == NULL)
    {
      local_tidy = EG (uninitialized_zval_ptr);
      local_tidy->refcount++;
    }
  zval** p_obj = &local_tidy;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "parsestring", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 142 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_text == NULL)
    {
      local_text = EG (uninitialized_zval_ptr);
      local_text->refcount++;
    }
  zval** p_arg = &local_text;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_text == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_text;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_wgTidyConf == NULL)
    {
      local_wgTidyConf = EG (uninitialized_zval_ptr);
      local_wgTidyConf->refcount++;
    }
  zval** p_arg = &local_wgTidyConf;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_wgTidyConf == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_wgTidyConf;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE81 == NULL)
    {
      local_TLE81 = EG (uninitialized_zval_ptr);
      local_TLE81->refcount++;
    }
  zval** p_arg = &local_TLE81;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE81 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE81;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 142, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 3;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// if (stderr) goto L175 else goto L176;
{
     zval* p_cond;
  if (local_stderr == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_stderr;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L175;
   else
      goto L176;
phc_check_invariants (TSRMLS_C);
}
// L175:
L175:;
// $retval = $tidy->getstatus();
{
     if (local_tidy == NULL)
    {
      local_tidy = EG (uninitialized_zval_ptr);
      local_tidy->refcount++;
    }
  zval** p_obj = &local_tidy;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getstatus", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 145 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 145, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_retval == NULL)
    {
      local_retval = EG (uninitialized_zval_ptr);
      local_retval->refcount++;
    }
  zval** p_lhs = &local_retval;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TSt82 = $tidy->errorBuffer;
{
	  if (local_tidy == NULL)
    {
      local_tidy = EG (uninitialized_zval_ptr);
      local_tidy->refcount++;
    }
  zval** p_obj = &local_tidy;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "errorBuffer", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt82 == NULL)
    {
      local_TSt82 = EG (uninitialized_zval_ptr);
      local_TSt82->refcount++;
    }
  zval** p_lhs = &local_TSt82;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt82;
{
     zval* rhs;
  if (local_TSt82 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt82;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// goto L177;
{
goto L177;
phc_check_invariants (TSRMLS_C);
}
// L176:
L176:;
// $tidy->cleanrepair();
{
     if (local_tidy == NULL)
    {
      local_tidy = EG (uninitialized_zval_ptr);
      local_tidy->refcount++;
    }
  zval** p_obj = &local_tidy;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "cleanrepair", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 148 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 148, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $retval = $tidy->getstatus();
{
     if (local_tidy == NULL)
    {
      local_tidy = EG (uninitialized_zval_ptr);
      local_tidy->refcount++;
    }
  zval** p_obj = &local_tidy;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getstatus", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 149 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 149, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_retval == NULL)
    {
      local_retval = EG (uninitialized_zval_ptr);
      local_retval->refcount++;
    }
  zval** p_lhs = &local_retval;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE83 = 2;
{
        if (local_TLE83 == NULL)
    {
      local_TLE83 = EG (uninitialized_zval_ptr);
      local_TLE83->refcount++;
    }
  zval** p_lhs = &local_TLE83;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 2);

phc_check_invariants (TSRMLS_C);
}
// $TLE84 = ($retval == $TLE83);
{
    if (local_TLE84 == NULL)
    {
      local_TLE84 = EG (uninitialized_zval_ptr);
      local_TLE84->refcount++;
    }
  zval** p_lhs = &local_TLE84;

    zval* left;
  if (local_retval == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_retval;

    zval* right;
  if (local_TLE83 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE83;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE84) goto L163 else goto L164;
{
     zval* p_cond;
  if (local_TLE84 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE84;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L163;
   else
      goto L164;
phc_check_invariants (TSRMLS_C);
}
// L163:
L163:;
// $cleansource = NULL;
{
        if (local_cleansource == NULL)
    {
      local_cleansource = EG (uninitialized_zval_ptr);
      local_cleansource->refcount++;
    }
  zval** p_lhs = &local_cleansource;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
// goto L165;
{
goto L165;
phc_check_invariants (TSRMLS_C);
}
// L164:
L164:;
// $cleansource = tidy_get_output($tidy);
{
   initialize_function_call (&tidy_get_output_fci, &tidy_get_output_fcic, "tidy_get_output", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 155 TSRMLS_CC);
      zend_function* signature = tidy_get_output_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_tidy == NULL)
    {
      local_tidy = EG (uninitialized_zval_ptr);
      local_tidy->refcount++;
    }
  zval** p_arg = &local_tidy;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_tidy == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_tidy;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 155, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = tidy_get_output_fci.param_count;
   zval*** params_save = tidy_get_output_fci.params;
   zval** retval_save = tidy_get_output_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   tidy_get_output_fci.params = args_ind;
   tidy_get_output_fci.param_count = 1;
   tidy_get_output_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&tidy_get_output_fci, &tidy_get_output_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   tidy_get_output_fci.params = params_save;
   tidy_get_output_fci.param_count = param_count_save;
   tidy_get_output_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_cleansource == NULL)
    {
      local_cleansource = EG (uninitialized_zval_ptr);
      local_cleansource->refcount++;
    }
  zval** p_lhs = &local_cleansource;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// goto L165;
{
goto L165;
phc_check_invariants (TSRMLS_C);
}
// L165:
L165:;
// $TLE9 = $wgDebugTidy;
{
    if (local_TLE9 == NULL)
    {
      local_TLE9 = EG (uninitialized_zval_ptr);
      local_TLE9->refcount++;
    }
  zval** p_lhs = &local_TLE9;

    zval* rhs;
  if (local_wgDebugTidy == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgDebugTidy;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// if (TLE9) goto L166 else goto L167;
{
     zval* p_cond;
  if (local_TLE9 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE9;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L166;
   else
      goto L167;
phc_check_invariants (TSRMLS_C);
}
// L166:
L166:;
// $TLE85 = 0;
{
        if (local_TLE85 == NULL)
    {
      local_TLE85 = EG (uninitialized_zval_ptr);
      local_TLE85->refcount++;
    }
  zval** p_lhs = &local_TLE85;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TEF10 = ($TLE85 < $retval);
{
    if (local_TEF10 == NULL)
    {
      local_TEF10 = EG (uninitialized_zval_ptr);
      local_TEF10->refcount++;
    }
  zval** p_lhs = &local_TEF10;

    zval* left;
  if (local_TLE85 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE85;

    zval* right;
  if (local_retval == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_retval;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_smaller_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L168;
{
goto L168;
phc_check_invariants (TSRMLS_C);
}
// L167:
L167:;
// $TEF10 = $TLE9;
{
    if (local_TEF10 == NULL)
    {
      local_TEF10 = EG (uninitialized_zval_ptr);
      local_TEF10->refcount++;
    }
  zval** p_lhs = &local_TEF10;

    zval* rhs;
  if (local_TLE9 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE9;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L168;
{
goto L168;
phc_check_invariants (TSRMLS_C);
}
// L168:
L168:;
// $TLE86 = (bool) $TEF10;
{
      if (local_TLE86 == NULL)
    {
      local_TLE86 = EG (uninitialized_zval_ptr);
      local_TLE86->refcount++;
    }
  zval** p_lhs = &local_TLE86;

    zval* rhs;
  if (local_TEF10 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF10;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// if (TLE86) goto L172 else goto L173;
{
     zval* p_cond;
  if (local_TLE86 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE86;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L172;
   else
      goto L173;
phc_check_invariants (TSRMLS_C);
}
// L172:
L172:;
// $TLE87 = '<!--
// Tidy reports:
// ';
{
        if (local_TLE87 == NULL)
    {
      local_TLE87 = EG (uninitialized_zval_ptr);
      local_TLE87->refcount++;
    }
  zval** p_lhs = &local_TLE87;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "<!--\012Tidy reports:\012", 19, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE88 = '-->';
{
        if (local_TLE88 == NULL)
    {
      local_TLE88 = EG (uninitialized_zval_ptr);
      local_TLE88->refcount++;
    }
  zval** p_lhs = &local_TLE88;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "-->", 3, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE89 = '--&gt;';
{
        if (local_TLE89 == NULL)
    {
      local_TLE89 = EG (uninitialized_zval_ptr);
      local_TLE89->refcount++;
    }
  zval** p_lhs = &local_TLE89;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "--&gt;", 6, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE106 = param_is_ref (NULL, "str_replace", 0);
// ;
{
   initialize_function_call (&str_replace_fci, &str_replace_fcic, "str_replace", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = str_replace_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE106 == NULL)
    {
      local_TLE106 = EG (uninitialized_zval_ptr);
      local_TLE106->refcount++;
    }
  zval** p_lhs = &local_TLE106;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE106) goto L169 else goto L170;
{
     zval* p_cond;
  if (local_TLE106 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE106;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L169;
   else
      goto L170;
phc_check_invariants (TSRMLS_C);
}
// L169:
L169:;
// $TMIt105 =& $tidy->errorBuffer;
{
	  if (local_tidy == NULL)
    {
      local_tidy = EG (uninitialized_zval_ptr);
      local_tidy->refcount++;
    }
  zval** p_obj = &local_tidy;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "errorBuffer", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt105 == NULL)
    {
      local_TMIt105 = EG (uninitialized_zval_ptr);
      local_TMIt105->refcount++;
    }
  zval** p_lhs = &local_TMIt105;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L171;
{
goto L171;
phc_check_invariants (TSRMLS_C);
}
// L170:
L170:;
// $TMIt105 = $tidy->errorBuffer;
{
	  if (local_tidy == NULL)
    {
      local_tidy = EG (uninitialized_zval_ptr);
      local_tidy->refcount++;
    }
  zval** p_obj = &local_tidy;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "errorBuffer", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt105 == NULL)
    {
      local_TMIt105 = EG (uninitialized_zval_ptr);
      local_TMIt105->refcount++;
    }
  zval** p_lhs = &local_TMIt105;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L171;
{
goto L171;
phc_check_invariants (TSRMLS_C);
}
// L171:
L171:;
// $TLE90 = str_replace($TLE88, $TLE89, $TMIt105);
{
   initialize_function_call (&str_replace_fci, &str_replace_fcic, "str_replace", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 159 TSRMLS_CC);
      zend_function* signature = str_replace_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE88 == NULL)
    {
      local_TLE88 = EG (uninitialized_zval_ptr);
      local_TLE88->refcount++;
    }
  zval** p_arg = &local_TLE88;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE88 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE88;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE89 == NULL)
    {
      local_TLE89 = EG (uninitialized_zval_ptr);
      local_TLE89->refcount++;
    }
  zval** p_arg = &local_TLE89;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE89 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE89;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt105 == NULL)
    {
      local_TMIt105 = EG (uninitialized_zval_ptr);
      local_TMIt105->refcount++;
    }
  zval** p_arg = &local_TMIt105;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt105 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt105;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 159, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = str_replace_fci.param_count;
   zval*** params_save = str_replace_fci.params;
   zval** retval_save = str_replace_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   str_replace_fci.params = args_ind;
   str_replace_fci.param_count = 3;
   str_replace_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&str_replace_fci, &str_replace_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   str_replace_fci.params = params_save;
   str_replace_fci.param_count = param_count_save;
   str_replace_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE90 == NULL)
    {
      local_TLE90 = EG (uninitialized_zval_ptr);
      local_TLE90->refcount++;
    }
  zval** p_lhs = &local_TLE90;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE91 = ($TLE87 . $TLE90);
{
    if (local_TLE91 == NULL)
    {
      local_TLE91 = EG (uninitialized_zval_ptr);
      local_TLE91->refcount++;
    }
  zval** p_lhs = &local_TLE91;

    zval* left;
  if (local_TLE87 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE87;

    zval* right;
  if (local_TLE90 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE90;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE92 = '
// -->';
{
        if (local_TLE92 == NULL)
    {
      local_TLE92 = EG (uninitialized_zval_ptr);
      local_TLE92->refcount++;
    }
  zval** p_lhs = &local_TLE92;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "\012-->", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE93 = ($TLE91 . $TLE92);
{
    if (local_TLE93 == NULL)
    {
      local_TLE93 = EG (uninitialized_zval_ptr);
      local_TLE93->refcount++;
    }
  zval** p_lhs = &local_TLE93;

    zval* left;
  if (local_TLE91 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE91;

    zval* right;
  if (local_TLE92 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE92;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $cleansource = ($cleansource . $TLE93);
{
    if (local_cleansource == NULL)
    {
      local_cleansource = EG (uninitialized_zval_ptr);
      local_cleansource->refcount++;
    }
  zval** p_lhs = &local_cleansource;

    zval* left;
  if (local_cleansource == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_cleansource;

    zval* right;
  if (local_TLE93 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE93;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L174;
{
goto L174;
phc_check_invariants (TSRMLS_C);
}
// L173:
L173:;
// goto L174;
{
goto L174;
phc_check_invariants (TSRMLS_C);
}
// L174:
L174:;
// $TLE94 = 'MWTidy::execInternalTidy';
{
        if (local_TLE94 == NULL)
    {
      local_TLE94 = EG (uninitialized_zval_ptr);
      local_TLE94->refcount++;
    }
  zval** p_lhs = &local_TLE94;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "MWTidy::execInternalTidy", 24, 1);

phc_check_invariants (TSRMLS_C);
}
// wfprofileout($TLE94);
{
   initialize_function_call (&wfprofileout_fci, &wfprofileout_fcic, "wfprofileout", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 162 TSRMLS_CC);
      zend_function* signature = wfprofileout_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE94 == NULL)
    {
      local_TLE94 = EG (uninitialized_zval_ptr);
      local_TLE94->refcount++;
    }
  zval** p_arg = &local_TLE94;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE94 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE94;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/Tidy.php", 162, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfprofileout_fci.param_count;
   zval*** params_save = wfprofileout_fci.params;
   zval** retval_save = wfprofileout_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfprofileout_fci.params = args_ind;
   wfprofileout_fci.param_count = 1;
   wfprofileout_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfprofileout_fci, &wfprofileout_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfprofileout_fci.params = params_save;
   wfprofileout_fci.param_count = param_count_save;
   wfprofileout_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $cleansource;
{
     zval* rhs;
  if (local_cleansource == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_cleansource;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// goto L177;
{
goto L177;
phc_check_invariants (TSRMLS_C);
}
// L177:
L177:;
// Method exit
end_of_function:__attribute__((unused));
if (local_IP != NULL)
{
zval_ptr_dtor (&local_IP);
}
if (local_TEF10 != NULL)
{
zval_ptr_dtor (&local_TEF10);
}
if (local_TLE106 != NULL)
{
zval_ptr_dtor (&local_TLE106);
}
if (local_TLE80 != NULL)
{
zval_ptr_dtor (&local_TLE80);
}
if (local_TLE81 != NULL)
{
zval_ptr_dtor (&local_TLE81);
}
if (local_TLE83 != NULL)
{
zval_ptr_dtor (&local_TLE83);
}
if (local_TLE84 != NULL)
{
zval_ptr_dtor (&local_TLE84);
}
if (local_TLE85 != NULL)
{
zval_ptr_dtor (&local_TLE85);
}
if (local_TLE86 != NULL)
{
zval_ptr_dtor (&local_TLE86);
}
if (local_TLE87 != NULL)
{
zval_ptr_dtor (&local_TLE87);
}
if (local_TLE88 != NULL)
{
zval_ptr_dtor (&local_TLE88);
}
if (local_TLE89 != NULL)
{
zval_ptr_dtor (&local_TLE89);
}
if (local_TLE9 != NULL)
{
zval_ptr_dtor (&local_TLE9);
}
if (local_TLE90 != NULL)
{
zval_ptr_dtor (&local_TLE90);
}
if (local_TLE91 != NULL)
{
zval_ptr_dtor (&local_TLE91);
}
if (local_TLE92 != NULL)
{
zval_ptr_dtor (&local_TLE92);
}
if (local_TLE93 != NULL)
{
zval_ptr_dtor (&local_TLE93);
}
if (local_TLE94 != NULL)
{
zval_ptr_dtor (&local_TLE94);
}
if (local_TMIt105 != NULL)
{
zval_ptr_dtor (&local_TMIt105);
}
if (local_TSt82 != NULL)
{
zval_ptr_dtor (&local_TSt82);
}
if (local_cleansource != NULL)
{
zval_ptr_dtor (&local_cleansource);
}
if (local_retval != NULL)
{
zval_ptr_dtor (&local_retval);
}
if (local_stderr != NULL)
{
zval_ptr_dtor (&local_stderr);
}
if (local_text != NULL)
{
zval_ptr_dtor (&local_text);
}
if (local_tidy != NULL)
{
zval_ptr_dtor (&local_tidy);
}
if (local_wgDebugTidy != NULL)
{
zval_ptr_dtor (&local_wgDebugTidy);
}
if (local_wgTidyConf != NULL)
{
zval_ptr_dtor (&local_wgTidyConf);
}
}
// ArgInfo structures (necessary to support compile time pass-by-reference)
ZEND_BEGIN_ARG_INFO_EX(MWTidy_tidy_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "text")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(MWTidy_checkerrors_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "text")
ZEND_ARG_INFO(1, "errorStr")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(MWTidy_execexternaltidy_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "text")
ZEND_ARG_INFO(0, "stderr")
ZEND_ARG_INFO(1, "retval")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(MWTidy_execinternaltidy_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "text")
ZEND_ARG_INFO(0, "stderr")
ZEND_ARG_INFO(1, "retval")
ZEND_END_ARG_INFO()

static function_entry MWTidy_functions[] = {
PHP_ME(MWTidy, tidy, MWTidy_tidy_arg_info, ZEND_ACC_PUBLIC | ZEND_ACC_STATIC)
PHP_ME(MWTidy, checkerrors, MWTidy_checkerrors_arg_info, ZEND_ACC_PUBLIC | ZEND_ACC_STATIC)
PHP_ME(MWTidy, execexternaltidy, MWTidy_execexternaltidy_arg_info, ZEND_ACC_PRIVATE | ZEND_ACC_STATIC)
PHP_ME(MWTidy, execinternaltidy, MWTidy_execinternaltidy_arg_info, ZEND_ACC_PRIVATE | ZEND_ACC_STATIC)
{ NULL, NULL, NULL }
};
// function __MAIN__()
// {
// }
PHP_FUNCTION(__MAIN__)
{
// Function body
// Method exit
end_of_function:__attribute__((unused));
}
// Module initialization
PHP_MINIT_FUNCTION(app)
{
{
zend_class_entry ce; // temp
zend_class_entry* ce_reg; // once registered, ce_ptr should be used
INIT_CLASS_ENTRY(ce, "MWTidy", MWTidy_functions);
ce_reg = zend_register_internal_class(&ce TSRMLS_CC);
ce_reg->type &= ~ZEND_INTERNAL_CLASS;
}return SUCCESS;}// ArgInfo structures (necessary to support compile time pass-by-reference)
ZEND_BEGIN_ARG_INFO_EX(app___MAIN___arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

static function_entry app_functions[] = {
PHP_FE(__MAIN__, app___MAIN___arg_info)
{ NULL, NULL, NULL }
};
// Register the module itself with PHP
zend_module_entry app_module_entry = {
STANDARD_MODULE_HEADER, 
"app",
app_functions,
PHP_MINIT(app), /* MINIT */
NULL, /* MSHUTDOWN */
NULL, /* RINIT */
NULL, /* RSHUTDOWN */
NULL, /* MINFO */
"1.0",
STANDARD_MODULE_PROPERTIES
};
#include <sapi/embed/php_embed.h>
#include <signal.h>

void sighandler(int signum)
{
	switch(signum)
	{
		case SIGABRT:
			printf("SIGABRT received!\n");
			break;
		case SIGSEGV:
			printf("SIGSEGV received!\n");
			break;
		default:
			printf("Unknown signal received!\n");
			break;
	}

	printf("This could be a bug in phc. If you suspect it is, please email\n");
	printf("a bug report to phc-general@phpcompiler.org.\n");
	exit(-1);
}

int
main (int argc, char* argv[])
{
   int phc_exit_status;
   signal(SIGABRT, sighandler);
   signal(SIGSEGV, sighandler);

   TSRMLS_D;
   int dealloc_pools = 1;
   php_embed_init (argc, argv PTSRMLS_CC);
   zend_first_try
   {

      // initialize the phc runtime
      init_runtime();

      // load the compiled extension
      zend_startup_module (&app_module_entry);

      zval main_name;
      ZVAL_STRING (&main_name, "__MAIN__", NULL);

      zval retval;

      // Use standard errors, on stdout
      zend_alter_ini_entry ("report_zend_debug", sizeof("report_zend_debug"), "0", sizeof("0") - 1, PHP_INI_ALL, PHP_INI_STAGE_RUNTIME);
      zend_alter_ini_entry ("display_startup_errors", sizeof("display_startup_errors"), "1", sizeof("1") - 1, PHP_INI_ALL, PHP_INI_STAGE_RUNTIME);

      // initialize all the constants
saved_refcount = 0;

      // call __MAIN__
      int success = call_user_function( 
                   EG (function_table),
                   NULL,
                   &main_name,
                   &retval,
                   0,
                   NULL
                   TSRMLS_CC);

      assert (success == SUCCESS);

      // finalize the runtime
      finalize_runtime();

   }
   zend_catch
   {
		dealloc_pools = 0;
   }
   zend_end_try ();
   if (dealloc_pools)
   {
   }
   phc_exit_status = EG(exit_status);
   php_embed_shutdown (TSRMLS_C);

  return phc_exit_status;
}
