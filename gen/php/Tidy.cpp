
#include <php/Tidy.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: Tidy.php line 12 */
Variant c_mwtidy::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_mwtidy::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_mwtidy::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_mwtidy::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_mwtidy::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_mwtidy::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_mwtidy::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_mwtidy::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(mwtidy)
ObjectData *c_mwtidy::cloneImpl() {
  c_mwtidy *obj = NEW(c_mwtidy)();
  cloneSet(obj);
  return obj;
}
void c_mwtidy::cloneSet(c_mwtidy *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_mwtidy::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_mwtidy::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_mwtidy::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_mwtidy$os_get(const char *s) {
  return c_mwtidy::os_get(s, -1);
}
Variant &cw_mwtidy$os_lval(const char *s) {
  return c_mwtidy::os_lval(s, -1);
}
Variant cw_mwtidy$os_constant(const char *s) {
  return c_mwtidy::os_constant(s);
}
Variant cw_mwtidy$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_mwtidy::os_invoke(c, s, params, -1, fatal);
}
void c_mwtidy::init() {
}
/* SRC: Tidy.php line 22 */
Variant c_mwtidy::ti_tidy(const char* cls, CVarRef v_text) {
  STATIC_METHOD_INJECTION(MWTidy, MWTidy::tidy);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgTidyInternal __attribute__((__unused__)) = g->GV(wgTidyInternal);
  Variant v_wrappedtext;
  Variant v_correctedtext;

  (v_wrappedtext = LINE(27,concat3("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><title>test</title></head><body>", toString(v_text), "</body></html>")));
  (v_wrappedtext = LINE(30,x_str_replace("\t", "&#9;", v_wrappedtext)));
  if (toBoolean(gv_wgTidyInternal)) {
    (v_correctedtext = LINE(33,c_mwtidy::t_execinternaltidy(v_wrappedtext)));
  }
  else {
    (v_correctedtext = LINE(35,c_mwtidy::t_execexternaltidy(v_wrappedtext)));
  }
  if (LINE(37,x_is_null(v_correctedtext))) {
    LINE(38,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Tidy error detected!\n").create()), 0x00000000E441E905LL));
    return concat(toString(v_text), "\n<!-- Tidy found serious XHTML errors -->\n");
  }
  (v_correctedtext = LINE(43,x_str_replace("&#9;", "\t", v_correctedtext)));
  return v_correctedtext;
} /* function */
/* SRC: Tidy.php line 55 */
bool c_mwtidy::ti_checkerrors(const char* cls, CVarRef v_text, Variant v_errorStr //  = null
) {
  STATIC_METHOD_INJECTION(MWTidy, MWTidy::checkErrors);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgTidyInternal __attribute__((__unused__)) = g->GV(wgTidyInternal);
  Variant v_retval;

  (v_retval = 0LL);
  if (toBoolean(gv_wgTidyInternal)) {
    (v_errorStr = LINE(60,c_mwtidy::t_execinternaltidy(v_text, true, ref(v_retval))));
  }
  else {
    (v_errorStr = LINE(62,c_mwtidy::t_execexternaltidy(v_text, true, ref(v_retval))));
  }
  return (less(v_retval, 0LL) && equal(v_errorStr, "")) || equal(v_retval, 0LL);
} /* function */
/* SRC: Tidy.php line 76 */
Variant c_mwtidy::ti_execexternaltidy(const char* cls, CVarRef v_text, bool v_stderr //  = false
, Variant v_retval //  = null
) {
  STATIC_METHOD_INJECTION(MWTidy, MWTidy::execExternalTidy);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgTidyConf __attribute__((__unused__)) = g->GV(wgTidyConf);
  Variant &gv_wgTidyBin __attribute__((__unused__)) = g->GV(wgTidyBin);
  Variant &gv_wgTidyOpts __attribute__((__unused__)) = g->GV(wgTidyOpts);
  String v_cleansource;
  String v_opts;
  Array v_descriptorspec;
  int64 v_readpipe = 0;
  Variant v_pipes;
  Variant v_process;

  {
  }
  LINE(78,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "MWTidy::execExternalTidy").create()), 0x0000000075359BAFLL));
  (v_cleansource = "");
  (v_opts = " -utf8");
  if (v_stderr) {
    (v_descriptorspec = Array(ArrayInit(3).set(0, 0LL, ScalarArrays::sa_[37], 0x77CFA1EEF01BCA90LL).set(1, 1LL, (assignCallTemp(eo_1, LINE(86,invoke_failed("wfgetnull", Array(), 0x0000000057801CCBLL))),Array(ArrayInit(3).set(0, "file").set(1, eo_1).set(2, "a").create())), 0x5BCA7C69B794F8CELL).set(2, 2LL, ScalarArrays::sa_[38], 0x486AFCC090D5F98CLL).create()));
  }
  else {
    (v_descriptorspec = Array(ArrayInit(3).set(0, 0LL, ScalarArrays::sa_[37], 0x77CFA1EEF01BCA90LL).set(1, 1LL, ScalarArrays::sa_[38], 0x5BCA7C69B794F8CELL).set(2, 2LL, (assignCallTemp(eo_1, LINE(93,invoke_failed("wfgetnull", Array(), 0x0000000057801CCBLL))),Array(ArrayInit(3).set(0, "file").set(1, eo_1).set(2, "a").create())), 0x486AFCC090D5F98CLL).create()));
  }
  (v_readpipe = v_stderr ? ((2LL)) : ((1LL)));
  (v_pipes = ScalarArrays::sa_[0]);
  {
    (v_process = LINE(101,(assignCallTemp(eo_0, concat6(toString(gv_wgTidyBin), " -config ", toString(gv_wgTidyConf), " ", toString(gv_wgTidyOpts), v_opts)),assignCallTemp(eo_1, v_descriptorspec),assignCallTemp(eo_2, ref(v_pipes)),x_proc_open(eo_0, eo_1, eo_2))));
    if (LINE(102,x_is_resource(v_process))) {
      LINE(108,x_fwrite(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toString(v_text)));
      LINE(109,x_fclose(toObject(v_pipes.rvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
      LOOP_COUNTER(1);
      {
        while (!(LINE(110,x_feof(toObject(v_pipes.rvalAt(v_readpipe)))))) {
          LOOP_COUNTER_CHECK(1);
          {
            concat_assign(v_cleansource, toString(LINE(111,x_fgets(toObject(v_pipes.rvalAt(v_readpipe)), 1024LL))));
          }
        }
      }
      LINE(113,x_fclose(toObject(v_pipes.rvalAt(v_readpipe))));
      (v_retval = LINE(114,x_proc_close(toObject(v_process))));
    }
    else {
      (v_retval = -1LL);
    }
  }
  LINE(122,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "MWTidy::execExternalTidy").create()), 0x00000000B599F276LL));
  if (!(v_stderr) && equal(v_cleansource, "") && !equal(v_text, "")) {
    return null;
  }
  else {
    return v_cleansource;
  }
  return null;
} /* function */
/* SRC: Tidy.php line 139 */
Variant c_mwtidy::ti_execinternaltidy(const char* cls, Variant v_text, bool v_stderr //  = false
, Variant v_retval //  = null
) {
  STATIC_METHOD_INJECTION(MWTidy, MWTidy::execInternalTidy);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgTidyConf __attribute__((__unused__)) = g->GV(wgTidyConf);
  Variant &gv_IP __attribute__((__unused__)) = g->GV(IP);
  Variant &gv_wgDebugTidy __attribute__((__unused__)) = g->GV(wgDebugTidy);
  Variant v_tidy;
  Variant v_cleansource;

  {
  }
  LINE(141,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "MWTidy::execInternalTidy").create()), 0x0000000075359BAFLL));
  (v_tidy = LINE(143,create_object("tidy", Array())));
  LINE(144,v_tidy.o_invoke_few_args("parseString", 0x612A19C517BCBA54LL, 3, v_text, gv_wgTidyConf, "utf8"));
  if (v_stderr) {
    (v_retval = LINE(147,v_tidy.o_invoke_few_args("getStatus", 0x289EEF4F945EB30CLL, 0)));
    return v_tidy.o_get("errorBuffer", 0x467F2F6CD0CD2BC2LL);
  }
  else {
    LINE(150,v_tidy.o_invoke_few_args("cleanRepair", 0x60D559FEBDDD2806LL, 0));
    (v_retval = LINE(151,v_tidy.o_invoke_few_args("getStatus", 0x289EEF4F945EB30CLL, 0)));
    if (equal(v_retval, 2LL)) {
      setNull(v_cleansource);
    }
    else {
      (v_cleansource = LINE(157,invoke_failed("tidy_get_output", Array(ArrayInit(1).set(0, ref(v_tidy)).create()), 0x000000008D6FC8B2LL)));
    }
    if (toBoolean(gv_wgDebugTidy) && more(v_retval, 0LL)) {
      concat_assign(v_cleansource, LINE(162,(assignCallTemp(eo_1, toString(LINE(161,x_str_replace("-->", "--&gt;", v_tidy.o_get("errorBuffer", 0x467F2F6CD0CD2BC2LL))))),concat3("<!--\nTidy reports:\n", eo_1, "\n-->"))));
    }
    LINE(165,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "MWTidy::execInternalTidy").create()), 0x00000000B599F276LL));
    return v_cleansource;
  }
  return null;
} /* function */
Object co_mwtidy(CArrRef params, bool init /* = true */) {
  return Object(p_mwtidy(NEW(c_mwtidy)())->dynCreate(params, init));
}
Variant pm_php$Tidy_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::Tidy.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$Tidy_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
