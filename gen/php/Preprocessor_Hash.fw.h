
#ifndef __GENERATED_php_Preprocessor_Hash_fw_h__
#define __GENERATED_php_Preprocessor_Hash_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(ppdstack_hash)
FORWARD_DECLARE_CLASS(ppnode_hash_text)
FORWARD_DECLARE_CLASS(ppdaccum_hash)
FORWARD_DECLARE_CLASS(ppnode_hash_tree)
FORWARD_DECLARE_CLASS(ppframe_hash)
FORWARD_DECLARE_CLASS(preprocessor_hash)
extern const int64 q_preprocessor_hash_CACHE_VERSION;
FORWARD_DECLARE_CLASS(ppdpart_hash)
FORWARD_DECLARE_CLASS(ppcustomframe_hash)
FORWARD_DECLARE_CLASS(ppnode_hash_attr)
FORWARD_DECLARE_CLASS(pptemplateframe_hash)
FORWARD_DECLARE_CLASS(ppnode_hash_array)
FORWARD_DECLARE_CLASS(ppdstackelement_hash)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/Parser.fw.h>
#include <php/Preprocessor.fw.h>
#include <php/Preprocessor_DOM.fw.h>

#endif // __GENERATED_php_Preprocessor_Hash_fw_h__
