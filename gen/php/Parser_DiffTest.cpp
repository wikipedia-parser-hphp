
#include <php/Parser.h>
#include <php/Parser_DiffTest.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: Parser_DiffTest.php line 6 */
Variant c_parser_difftest::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_parser_difftest::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_parser_difftest::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("parsers", m_parsers.isReferenced() ? ref(m_parsers) : m_parsers));
  props.push_back(NEW(ArrayElement)("conf", m_conf.isReferenced() ? ref(m_conf) : m_conf));
  props.push_back(NEW(ArrayElement)("shortOutput", m_shortOutput.isReferenced() ? ref(m_shortOutput) : m_shortOutput));
  props.push_back(NEW(ArrayElement)("dfUniqPrefix", m_dfUniqPrefix.isReferenced() ? ref(m_dfUniqPrefix) : m_dfUniqPrefix));
  c_ObjectData::o_get(props);
}
bool c_parser_difftest::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_EXISTS_STRING(0x7837CEB82412F651LL, dfUniqPrefix, 12);
      break;
    case 2:
      HASH_EXISTS_STRING(0x549C5B982C221C12LL, shortOutput, 11);
      break;
    case 3:
      HASH_EXISTS_STRING(0x3DD56525B556A463LL, parsers, 7);
      break;
    case 6:
      HASH_EXISTS_STRING(0x6B245112F4B75C96LL, conf, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_parser_difftest::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x7837CEB82412F651LL, m_dfUniqPrefix,
                         dfUniqPrefix, 12);
      break;
    case 2:
      HASH_RETURN_STRING(0x549C5B982C221C12LL, m_shortOutput,
                         shortOutput, 11);
      break;
    case 3:
      HASH_RETURN_STRING(0x3DD56525B556A463LL, m_parsers,
                         parsers, 7);
      break;
    case 6:
      HASH_RETURN_STRING(0x6B245112F4B75C96LL, m_conf,
                         conf, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_parser_difftest::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_SET_STRING(0x7837CEB82412F651LL, m_dfUniqPrefix,
                      dfUniqPrefix, 12);
      break;
    case 2:
      HASH_SET_STRING(0x549C5B982C221C12LL, m_shortOutput,
                      shortOutput, 11);
      break;
    case 3:
      HASH_SET_STRING(0x3DD56525B556A463LL, m_parsers,
                      parsers, 7);
      break;
    case 6:
      HASH_SET_STRING(0x6B245112F4B75C96LL, m_conf,
                      conf, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_parser_difftest::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x7837CEB82412F651LL, m_dfUniqPrefix,
                         dfUniqPrefix, 12);
      break;
    case 2:
      HASH_RETURN_STRING(0x549C5B982C221C12LL, m_shortOutput,
                         shortOutput, 11);
      break;
    case 3:
      HASH_RETURN_STRING(0x3DD56525B556A463LL, m_parsers,
                         parsers, 7);
      break;
    case 6:
      HASH_RETURN_STRING(0x6B245112F4B75C96LL, m_conf,
                         conf, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_parser_difftest::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(parser_difftest)
ObjectData *c_parser_difftest::create(Variant v_conf) {
  init();
  t___construct(v_conf);
  return this;
}
ObjectData *c_parser_difftest::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_parser_difftest::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_parser_difftest::cloneImpl() {
  c_parser_difftest *obj = NEW(c_parser_difftest)();
  cloneSet(obj);
  return obj;
}
void c_parser_difftest::cloneSet(c_parser_difftest *clone) {
  clone->m_parsers = m_parsers.isReferenced() ? ref(m_parsers) : m_parsers;
  clone->m_conf = m_conf.isReferenced() ? ref(m_conf) : m_conf;
  clone->m_shortOutput = m_shortOutput.isReferenced() ? ref(m_shortOutput) : m_shortOutput;
  clone->m_dfUniqPrefix = m_dfUniqPrefix.isReferenced() ? ref(m_dfUniqPrefix) : m_dfUniqPrefix;
  ObjectData::cloneSet(clone);
}
Variant c_parser_difftest::doCall(Variant v_name, Variant v_arguments, bool fatal) {
  return t___call(v_name, v_arguments);
}
Variant c_parser_difftest::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GUARD(0x5959ADC755897C13LL, onclearstate) {
        return (t_onclearstate(ref(const_cast<Array&>(params).lvalAt(0))));
      }
      break;
    case 4:
      HASH_GUARD(0x07B2DE5DC3BC16D4LL, setfunctionhook) {
        int count = params.size();
        if (count <= 2) return (t_setfunctionhook(params.rvalAt(0), params.rvalAt(1)), null);
        return (t_setfunctionhook(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_parser_difftest::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 3:
      HASH_GUARD(0x5959ADC755897C13LL, onclearstate) {
        return (t_onclearstate(ref(a0)));
      }
      break;
    case 4:
      HASH_GUARD(0x07B2DE5DC3BC16D4LL, setfunctionhook) {
        if (count <= 2) return (t_setfunctionhook(a0, a1), null);
        return (t_setfunctionhook(a0, a1, a2), null);
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parser_difftest::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parser_difftest$os_get(const char *s) {
  return c_parser_difftest::os_get(s, -1);
}
Variant &cw_parser_difftest$os_lval(const char *s) {
  return c_parser_difftest::os_lval(s, -1);
}
Variant cw_parser_difftest$os_constant(const char *s) {
  return c_parser_difftest::os_constant(s);
}
Variant cw_parser_difftest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parser_difftest::os_invoke(c, s, params, -1, fatal);
}
void c_parser_difftest::init() {
  m_parsers = null;
  m_conf = null;
  m_shortOutput = false;
  m_dfUniqPrefix = null;
}
/* SRC: Parser_DiffTest.php line 13 */
void c_parser_difftest::t___construct(Variant v_conf) {
  INSTANCE_METHOD_INJECTION(Parser_DiffTest, Parser_DiffTest::__construct);
  bool oldInCtor = gasInCtor(true);
  if (!(isset(v_conf, "parsers", 0x3DD56525B556A463LL))) {
    throw_exception(LINE(15,create_object("mwexception", Array(ArrayInit(1).set(0, "Parser_DiffTest::__construct: no parsers specified").create()))));
  }
  (m_conf = v_conf);
  (o_lval("dtUniqPrefix", 0x5BF7F09B4C03EDBCLL) = concat("\177UNIQ", LINE(18,c_parser::t_getrandomstring())));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Parser_DiffTest.php line 21 */
void c_parser_difftest::t_init() {
  INSTANCE_METHOD_INJECTION(Parser_DiffTest, Parser_DiffTest::init);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgHooks __attribute__((__unused__)) = g->GV(wgHooks);
  Variant &sv_doneHook __attribute__((__unused__)) = g->sv_parser_difftest$$init$$doneHook.lvalAt(this->o_getClassName());
  Variant &inited_sv_doneHook __attribute__((__unused__)) = g->inited_sv_parser_difftest$$init$$doneHook.lvalAt(this->o_getClassName());
  Primitive v_i = 0;
  Variant v_parserConf;
  Variant v_class;

  if (!(LINE(22,x_is_null(m_parsers)))) {
    return;
  }
  if (!inited_sv_doneHook) {
    (sv_doneHook = false);
    inited_sv_doneHook = true;
  }
  if (!(toBoolean(sv_doneHook))) {
    (sv_doneHook = true);
    lval(gv_wgHooks.lvalAt("ParserClearState", 0x33A5F12C1D6199E7LL)).append((Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "onClearState").create())));
  }
  if (isset(m_conf, "shortOutput", 0x549C5B982C221C12LL)) {
    (m_shortOutput = m_conf.rvalAt("shortOutput", 0x549C5B982C221C12LL));
  }
  {
    LOOP_COUNTER(1);
    Variant map2 = m_conf.rvalAt("parsers", 0x3DD56525B556A463LL);
    for (ArrayIterPtr iter3 = map2.begin("parser_difftest"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_parserConf = iter3->second();
      v_i = iter3->first();
      {
        if (!(LINE(37,x_is_array(v_parserConf)))) {
          (v_class = v_parserConf);
          (v_parserConf = Array(ArrayInit(1).set(0, "class", v_parserConf, 0x45397FE5C82DBD12LL).create()));
        }
        else {
          (v_class = v_parserConf.rvalAt("class", 0x45397FE5C82DBD12LL));
        }
        m_parsers.set(v_i, (LINE(43,create_object(toString(v_class), Array(ArrayInit(1).set(0, ref(v_parserConf)).create())))));
      }
    }
  }
} /* function */
/* SRC: Parser_DiffTest.php line 47 */
Variant c_parser_difftest::t___call(Variant v_name, Variant v_args) {
  INSTANCE_METHOD_INJECTION(Parser_DiffTest, Parser_DiffTest::__call);
  INCALL_HELPER(v_name);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant v_results;
  bool v_mismatch = false;
  Variant v_lastResult;
  bool v_first = false;
  Primitive v_i = 0;
  Variant v_parser;
  Variant v_currentResult;
  Variant v_resultsList;
  Variant v_diff;
  String v_msg;

  LINE(48,t_init());
  (v_results = ScalarArrays::sa_[0]);
  (v_mismatch = false);
  setNull(v_lastResult);
  (v_first = true);
  {
    LOOP_COUNTER(4);
    Variant map5 = m_parsers;
    for (ArrayIterPtr iter6 = map5.begin("parser_difftest"); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_parser = iter6->second();
      v_i = iter6->first();
      {
        (v_currentResult = LINE(54,x_call_user_func_array(Array(ArrayInit(2).setRef(0, ref(lval(m_parsers.lvalAt(v_i)))).set(1, v_name).create()), toArray(v_args))));
        if (v_first) {
          (v_first = false);
        }
        else {
          if (LINE(58,x_is_object(v_lastResult))) {
            if (!equal(v_lastResult, v_currentResult)) {
              (v_mismatch = true);
            }
          }
          else {
            if (!same(v_lastResult, v_currentResult)) {
              (v_mismatch = true);
            }
          }
        }
        v_results.set(v_i, (v_currentResult));
        (v_lastResult = v_currentResult);
      }
    }
  }
  if (v_mismatch) {
    if (equal(LINE(72,x_count(v_results)), 2LL)) {
      (v_resultsList = ScalarArrays::sa_[0]);
      {
        LOOP_COUNTER(7);
        Variant map8 = m_parsers;
        for (ArrayIterPtr iter9 = map8.begin("parser_difftest"); !iter9->end(); iter9->next()) {
          LOOP_COUNTER_CHECK(7);
          v_parser = iter9->second();
          v_i = iter9->first();
          {
            v_resultsList.append((LINE(75,x_var_export(v_results.rvalAt(v_i), true))));
          }
        }
      }
      (v_diff = LINE(77,invoke_failed("wfdiff", Array(ArrayInit(2).set(0, ref(v_resultsList.refvalAt(0LL, 0x77CFA1EEF01BCA90LL))).set(1, ref(v_resultsList.refvalAt(1LL, 0x5BCA7C69B794F8CELL))).create()), 0x00000000B1D6FC35LL)));
    }
    else {
      (v_diff = "[too many parsers]");
    }
    (v_msg = LINE(81,concat3("Parser_DiffTest: results mismatch on call to ", toString(v_name), "\n")));
    if (!(toBoolean(m_shortOutput))) {
      concat_assign(v_msg, LINE(83,(assignCallTemp(eo_1, toString(t_formatarray(v_args))),concat3("Arguments: ", eo_1, "\n"))));
    }
    concat_assign(v_msg, LINE(86,(assignCallTemp(eo_1, toString(LINE(85,t_formatarray(v_results)))),assignCallTemp(eo_3, LINE(86,concat3("Diff: ", toString(v_diff), "\n"))),concat4("Results: ", eo_1, "\n", eo_3))));
    throw_exception(LINE(87,create_object("mwexception", Array(ArrayInit(1).set(0, v_msg).create()))));
  }
  return v_lastResult;
} /* function */
/* SRC: Parser_DiffTest.php line 92 */
Variant c_parser_difftest::t_formatarray(Variant v_array) {
  INSTANCE_METHOD_INJECTION(Parser_DiffTest, Parser_DiffTest::formatArray);
  Primitive v_key = 0;
  Variant v_value;

  if (toBoolean(m_shortOutput)) {
    {
      LOOP_COUNTER(10);
      for (ArrayIterPtr iter12 = v_array.begin("parser_difftest"); !iter12->end(); iter12->next()) {
        LOOP_COUNTER_CHECK(10);
        v_value = iter12->second();
        v_key = iter12->first();
        {
          if (instanceOf(v_value, "ParserOutput")) {
            v_array.set(v_key, (toString("ParserOutput: ") + toString(LINE(96,v_value.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0)))));
          }
        }
      }
    }
  }
  return LINE(100,x_var_export(v_array, true));
} /* function */
/* SRC: Parser_DiffTest.php line 103 */
void c_parser_difftest::t_setfunctionhook(Variant v_id, Variant v_callback, Variant v_flags //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(Parser_DiffTest, Parser_DiffTest::setFunctionHook);
  Primitive v_i = 0;
  Variant v_parser;

  LINE(104,t_init());
  {
    LOOP_COUNTER(13);
    Variant map14 = m_parsers;
    for (ArrayIterPtr iter15 = map14.begin("parser_difftest"); !iter15->end(); iter15->next()) {
      LOOP_COUNTER_CHECK(13);
      v_parser = iter15->second();
      v_i = iter15->first();
      {
        LINE(106,v_parser.o_invoke_few_args("setFunctionHook", 0x07B2DE5DC3BC16D4LL, 3, v_id, v_callback, v_flags));
      }
    }
  }
} /* function */
/* SRC: Parser_DiffTest.php line 110 */
bool c_parser_difftest::t_onclearstate(Variant v_parser) {
  INSTANCE_METHOD_INJECTION(Parser_DiffTest, Parser_DiffTest::onClearState);
  (v_parser.o_lval("mUniqPrefix", 0x22D00FA247E8311DLL) = o_get("dtUniqPrefix", 0x5BF7F09B4C03EDBCLL));
  return true;
} /* function */
Object co_parser_difftest(CArrRef params, bool init /* = true */) {
  return Object(p_parser_difftest(NEW(c_parser_difftest)())->dynCreate(params, init));
}
Variant pm_php$Parser_DiffTest_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::Parser_DiffTest.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$Parser_DiffTest_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
