
#ifndef __GENERATED_cls_parsercache_h__
#define __GENERATED_cls_parsercache_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: ParserCache.php line 6 */
class c_parsercache : virtual public ObjectData {
  BEGIN_CLASS_MAP(parsercache)
  END_CLASS_MAP(parsercache)
  DECLARE_CLASS(parsercache, ParserCache, ObjectData)
  void init();
  public: static Variant ti_singleton(const char* cls);
  public: void t___construct(Variant v_memCached);
  public: ObjectData *create(Variant v_memCached);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_getkey(CVarRef v_article, Variant v_popts);
  public: String t_getetag(Object v_article, CVarRef v_popts);
  public: Variant t_getdirty(CVarRef v_article, CVarRef v_popts);
  public: Variant t_get(Variant v_article, CVarRef v_popts);
  public: void t_save(Variant v_parserOutput, Object v_article, CVarRef v_popts);
  public: static Variant t_singleton() { return ti_singleton("parsercache"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parsercache_h__
