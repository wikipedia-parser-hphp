
#ifndef __GENERATED_php_Preprocessor_Hash_h__
#define __GENERATED_php_Preprocessor_Hash_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/Preprocessor_Hash.fw.h>

// Declarations
#include <cls/ppdstack_hash.h>
#include <cls/ppnode_hash_text.h>
#include <cls/ppdaccum_hash.h>
#include <cls/ppnode_hash_tree.h>
#include <cls/ppframe_hash.h>
#include <cls/preprocessor_hash.h>
#include <cls/ppdpart_hash.h>
#include <cls/ppcustomframe_hash.h>
#include <cls/ppnode_hash_attr.h>
#include <cls/pptemplateframe_hash.h>
#include <cls/ppnode_hash_array.h>
#include <cls/ppdstackelement_hash.h>
#include <php/Parser.h>
#include <php/Preprocessor_DOM.h>
#include <php/Preprocessor.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$Preprocessor_Hash_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_ppdstack_hash(CArrRef params, bool init = true);
Object co_ppnode_hash_text(CArrRef params, bool init = true);
Object co_ppdaccum_hash(CArrRef params, bool init = true);
Object co_ppnode_hash_tree(CArrRef params, bool init = true);
Object co_ppframe_hash(CArrRef params, bool init = true);
Object co_preprocessor_hash(CArrRef params, bool init = true);
Object co_ppdpart_hash(CArrRef params, bool init = true);
Object co_ppcustomframe_hash(CArrRef params, bool init = true);
Object co_ppnode_hash_attr(CArrRef params, bool init = true);
Object co_pptemplateframe_hash(CArrRef params, bool init = true);
Object co_ppnode_hash_array(CArrRef params, bool init = true);
Object co_ppdstackelement_hash(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_Preprocessor_Hash_h__
