// BEGIN INCLUDED FILES
/* The difference between zend_symtable_X and zend_hash_X is that
 * the _symtable version will check if the key is a string of an
 * integer, and if so, use the int version instead. We can use the
 * zend_hash_X versions safely for symbol tables, since
 * variables cant be integer strings, but we cant safely use
 * zend_hash_X versions for hashtable lookups. Well-named, they are.
 */

/* An important note of zval*s and zval**s. Frequently, zval** are
 * fetched from arrays and symbol tables. The zval** will point into
 * the array, and so updating it will update the relevant array
 * entry. It is normally not the same to dereference it to a local
 * variable, and pass a reference to that.
 */



// Some common functions
#include "php.h"

/*
 * The runtime needs its own initialization and finalization.  phc is
 * responsible for generating calls to these functions.
 */

void
init_runtime ()
{
}

void
finalize_runtime ()
{
}

static void
ht_debug (HashTable * ht)
{
  printf ("\nHASH\n");
  if (ht == NULL)
    {
      printf ("NULL\n");
      return;
    }
  for (zend_hash_internal_pointer_reset (ht);
       zend_hash_has_more_elements (ht) == SUCCESS;
       zend_hash_move_forward (ht))
    {
      char *key;
      unsigned keylen;
      unsigned long idx;
      int type;
      zval **ppzval;
      zval *zvp;

      type = zend_hash_get_current_key_ex (ht, &key, &keylen, &idx, 0, NULL);
      zend_hash_get_current_data (ht, (void **) &ppzval);

      zvp = *ppzval;

      if (type == HASH_KEY_IS_STRING)
	{
	  printf ("%s", key);
	}
      else
	{
	  printf ("%ld", idx);
	}

      printf (": addr = %08lX, refcount = %d, is_ref = %d ",
	      (long unsigned int) (*ppzval), (*ppzval)->refcount,
	      (*ppzval)->is_ref);
      switch (Z_TYPE_P (zvp))
	{
	case IS_NULL:
	  printf ("(NULL)");
	  break;
	case IS_LONG:
	  printf ("(%ldL)", Z_LVAL_P (zvp));
	  break;
	case IS_DOUBLE:
	  printf ("(%lff)", Z_DVAL_P (zvp));
	  break;
	case IS_BOOL:
	  printf (Z_BVAL_P (zvp) ? "(true)" : "(false)");
	  break;
	case IS_ARRAY:
	  printf ("(array(%d))", Z_ARRVAL_P (zvp)->nNumOfElements);
	  break;
	case IS_OBJECT:
	  printf ("(Object)");
	  break;
	case IS_STRING:
	  printf ("(\"%s\")", Z_STRVAL_P (zvp));
	  break;
	case IS_RESOURCE:
	  printf ("(Resource)");
	  break;
	default:
	  printf ("(Invalid: %d)", Z_TYPE_P (zvp));
	}

      printf ("\n");

    }
  printf ("END HASH\n");
}

// Call ht_debug on the named var in the given symbol table
static void
ht_var_debug (HashTable * st, char *name)
{
  zval **p_zvp;
  if (zend_symtable_find (st, name, strlen (name) + 1,
			  (void **) &p_zvp) != SUCCESS)
    {
      printf ("VAR NOT IN SYMBOL TABLE: '%s'\n", name);
      return;
    }

  if (Z_TYPE_P (*p_zvp) != IS_ARRAY)
    {
      printf ("NOT HASH\n");
      return;
    }

  ht_debug ((*p_zvp)->value.ht);
}

static zval* counters;

static void init_counters ()
{
  ALLOC_INIT_ZVAL (counters);
  array_init (counters);
}

// Dump and cleanup memory
static void finalize_counters ()
{
  HashTable* ht = Z_ARRVAL_P (counters);
  for (zend_hash_internal_pointer_reset (ht);
       zend_hash_has_more_elements (ht) == SUCCESS;
       zend_hash_move_forward (ht))
    {
      char *key;
      zval **p_zvp;

      zend_hash_get_current_key_ex (ht, &key, NULL, NULL, 0, NULL);
      zend_hash_get_current_data (ht, (void **) &p_zvp);

      fprintf (stderr, "COUNTER:%s:%ld\n", key, Z_LVAL_P (*p_zvp));
    }

  zval_ptr_dtor (&counters);
}

static void increment_counter (char* name, int length, ulong hashval)
{
  zval** p_zvp;
  int success = zend_hash_quick_find (Z_ARRVAL_P (counters),
				      name,
				      length,
				      hashval,
				      (void **) &p_zvp);

  if (success == SUCCESS)
    {
      Z_LVAL_PP (p_zvp)++;
    }
  else
    {

      zval* new_val;
      ALLOC_INIT_ZVAL (new_val);
      ZVAL_LONG (new_val, 1);

      zend_hash_quick_add (Z_ARRVAL_P (counters),
			   name,
			   length,
			   hashval,
			   &new_val,
			   sizeof (zval *),
			   NULL);
    }
}



/* Make a copy of *P_ZVP, storing it in *P_ZVP. */
static zval *
zvp_clone_ex (zval * zvp)
{
  // TODO: use INIT_PZVAL_COPY
  zval *clone;
  MAKE_STD_ZVAL (clone);
  clone->value = zvp->value;
  clone->type = zvp->type;
  zval_copy_ctor (clone);
  return clone;
}


static inline int
in_copy_on_write (zval * zvp)
{
  return (zvp->refcount > 1 && !zvp->is_ref);
}

static inline int
in_change_on_write (zval * zvp)
{
  return (zvp->refcount > 1 && zvp->is_ref);
}

/* If *P_ZVP is in a copy-on-write set, separate it by overwriting
 * *P_ZVP with a clone of itself, and lowering the refcount on the
 * original. */
static void
sep_copy_on_write (zval ** p_zvp)
{
  if (!in_copy_on_write (*p_zvp))
    return;

  zval *old = *p_zvp;

  *p_zvp = zvp_clone_ex (*p_zvp);

  zval_ptr_dtor (&old);
}

/* If *P_ZVP is in a copy-on-write set, separate it by overwriting
 * *P_ZVP with a clone of itself, and lowering the refcount on the
 * original. */
static void
sep_change_on_write (zval ** p_zvp)
{
  assert (in_change_on_write (*p_zvp));

  zval *old = *p_zvp;

  *p_zvp = zvp_clone_ex (*p_zvp);

  zval_ptr_dtor (&old);
}

/* Assign RHS into LHS, by reference. After this, LHS will point to the same
 * zval* as RHS. */
static void
copy_into_ref (zval ** lhs, zval ** rhs)
{
  (*rhs)->is_ref = 1;
  (*rhs)->refcount++;
  zval_ptr_dtor (lhs);
  *lhs = *rhs;
}


// Overwrite one zval with another
static void
overwrite_lhs (zval * lhs, zval * rhs)
{
  // First, call the destructor to remove any data structures
  // associated with lhs that will now be overwritten
  zval_dtor (lhs);
  // Overwrite LHS
  lhs->value = rhs->value;
  lhs->type = rhs->type;
  zval_copy_ctor (lhs);
}

// Overwrite one zval with another
static void
overwrite_lhs_no_copy (zval * lhs, zval * rhs)
{
  // First, call the destructor to remove any data structures
  // associated with lhs that will now be overwritten
  zval_dtor (lhs);
  // Overwrite LHS
  lhs->value = rhs->value;
  lhs->type = rhs->type;
}

/* Write P_RHS into the symbol table as a variable named VAR_NAME. */
// NOTE: We do not alter p_rhs's refcount, unless p_lhs joins its
// Copy-on-write set.
// TODO: this is crying out to be inlined.
static void
write_var (zval ** p_lhs, zval * rhs)
{
  if (!(*p_lhs)->is_ref)
    {
      zval_ptr_dtor (p_lhs);
      // Take a copy of RHS for LHS.
      if (rhs->is_ref)
	{
	  *p_lhs = zvp_clone_ex (rhs);
	}
      else			// share a copy
	{
	  rhs->refcount++;
	  *p_lhs = rhs;
	}
    }
  else
    {
      overwrite_lhs (*p_lhs, rhs);
    }
}

// TODO: this functino does too much, and much might be redundant
static zval **
get_st_entry (HashTable * st, char *name, int length, ulong hashval TSRMLS_DC)
{
  zval **p_zvp;
  if (zend_hash_quick_find
      (st, name, length, hashval, (void **) &p_zvp) == SUCCESS)
    {
      assert (p_zvp != NULL);
      return p_zvp;
    }

  // If we dont find it, put EG (uninitialized_zval_ptr) into the
  // hashtable, and return a pointer to its container.
  EG (uninitialized_zval_ptr)->refcount++;
  int result = zend_hash_quick_add (st, name, length, hashval,
				    &EG (uninitialized_zval_ptr),
				    sizeof (zval *), (void **) &p_zvp);
  assert (result == SUCCESS);
  assert (p_zvp != NULL);

  return p_zvp;
}

/* Read the variable named VAR_NAME from the local symbol table and
 * return it. If the variable doent exist, a new one is created and
 * *IS_NEW is set.  */
static zval *
read_var (HashTable * st, char *name, int length, ulong hashval TSRMLS_DC)
{
  zval **p_zvp;
  if (zend_hash_quick_find
      (st, name, length, hashval, (void **) &p_zvp) == SUCCESS)
    return *p_zvp;

  return EG (uninitialized_zval_ptr);
}

static long
get_integer_index (zval * ind TSRMLS_DC)
{
  long index;
  switch (Z_TYPE_P (ind))
    {
    case IS_DOUBLE:
      return (long) Z_DVAL_P (ind);

    case IS_LONG:
    case IS_BOOL:
      return Z_LVAL_P (ind);

    case IS_NULL:
      return 0;

    default:
      php_error_docref (NULL TSRMLS_CC, E_WARNING, "Illegal offset type");
    }
}

static zval *
read_string_index (zval * var, zval * ind TSRMLS_DC)
{
  // This must always allocate memory, since we cant return the
  // passed string.
  assert (Z_TYPE_P (var) == IS_STRING);
  long index = get_integer_index (ind TSRMLS_CC);

  zval *result;
  ALLOC_INIT_ZVAL (result);

  if (index >= Z_STRLEN_P (var) || index < 0)
    {
      // this is 1 byte long, must be copied
      ZVAL_STRINGL (result, "", 0, 1);
    }
  else
    {
      char *string = Z_STRVAL_P (var);
      ZVAL_STRINGL (result, &string[index], 1, 1);
    }

  return result;
}

/* Given a string (p_lhs), write into it for $x[i] = $y; */
void
write_string_index (zval ** p_lhs, zval * ind, zval * rhs TSRMLS_DC)
{
  assert (Z_TYPE_P (*p_lhs) == IS_STRING);

  long index = get_integer_index (ind TSRMLS_CC);

  // Get the appropriate character
  char new_char;
  if (Z_TYPE_P (rhs) != IS_STRING)
    {
      // TODO: remove allocate
      zval *copy = zvp_clone_ex (rhs);
      convert_to_string (copy);
      new_char = Z_STRVAL_P (copy)[0];
      zval_ptr_dtor (&copy);
    }
  else
    {
      new_char = Z_STRVAL_P (rhs)[0];
    }

  // Bounds check
  if (index < 0)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Illegal string offset:  %ld", index);
      return;
    }

  // We overwrite if it's change-on-write
  sep_copy_on_write (p_lhs);

  if (index > Z_STRLEN_PP (p_lhs))
    {
      // Extend to fix new
      int len = Z_STRLEN_PP (p_lhs);
      int new_length = index + 1;	// space for the new character
      Z_STRVAL_PP (p_lhs) = erealloc (Z_STRVAL_PP (p_lhs), new_length + 1);

      // pad with ' '
      memset (&Z_STRVAL_PP (p_lhs)[len], ' ', index - len);

      // change the strlen
      Z_STRLEN_PP (p_lhs) = new_length;

      // add a null terminator
      Z_STRVAL_PP (p_lhs)[new_length] = '\0';
    }

  // write in the first character of the new value
  Z_STRVAL_PP (p_lhs)[index] = new_char;


  // index < 0: E_WARNING illegal string offset
}

// Extract the hashtable from a hash-valued zval
static HashTable *
extract_ht_ex (zval * arr TSRMLS_DC)
{
  // TODO: this likely should be inlined somewhere.
  assert (!in_copy_on_write (arr));
  if (Z_TYPE_P (arr) == IS_NULL)
    {
      array_init (arr);
    }
  else if (Z_TYPE_P (arr) != IS_ARRAY)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an array");
      array_init (arr);
    }
  return Z_ARRVAL_P (arr);
}


/* P_VAR points into a symbol table, at a variable which we wish to index as a hashtable. */
static HashTable *
extract_ht (zval ** p_var TSRMLS_DC)
{
  sep_copy_on_write (p_var);

  return extract_ht_ex (*p_var TSRMLS_CC);
}

/* Using IND as a key to HT, call the appropriate zend_index_X
 * function with data as a parameter, and return its result. This
 * updates the zval** pointed to by DATA. */
static int
ht_find (HashTable * ht, zval * ind, zval *** data)
{
  int result;
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      result = zend_hash_index_find (ht, Z_LVAL_P (ind), (void **) data);
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      result = zend_hash_index_find (ht, (long) Z_DVAL_P (ind),
				     (void **) data);
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      result = zend_hash_find (ht, "", sizeof (""), (void **)data);
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      result = zend_symtable_find (ht, Z_STRVAL_P (ind),
				   Z_STRLEN_P (ind) + 1, (void **) data);
    }
  else
    {
      // TODO: I believe this might need a warning.

      // TODO avoid alloc
      // use a string index for other types
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);

      result = zend_symtable_find (ht, Z_STRVAL_P (string_index),
				   Z_STRLEN_P (string_index) + 1,
				   (void **) data);
      zval_ptr_dtor (&string_index);
    }
  return result;
}


static int
check_array_index_type (zval * ind TSRMLS_DC)
{
  if (Z_TYPE_P (ind) == IS_OBJECT || Z_TYPE_P (ind) == IS_ARRAY)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING, "Illegal offset type");
      return 0;
    }

  return 1;
}

// Update a hashtable using a zval* index
static void
ht_update (HashTable * ht, zval * ind, zval * val, zval *** dest)
{
  int result;
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      result = zend_hash_index_update (ht, Z_LVAL_P (ind), &val,
				       sizeof (zval *), (void **) dest);
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      result = zend_hash_index_update (ht, (long) Z_DVAL_P (ind),
				       &val, sizeof (zval *), (void **) dest);
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      result = zend_hash_update (ht, "", sizeof (""), &val,
				 sizeof (zval *), (void **) dest);
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      result = zend_symtable_update (ht, Z_STRVAL_P (ind),
				     Z_STRLEN_P (ind) + 1,
				     &val, sizeof (zval *), (void **) dest);
    }
  else
    {
      // TODO avoid alloc
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);
      result = zend_symtable_update (ht, Z_STRVAL_P (string_index),
				     Z_STRLEN_P (string_index) + 1,
				     &val, sizeof (zval *), (void **) dest);

      zval_ptr_dtor (&string_index);
    }
  assert (result == SUCCESS);
}

// Delete from a hashtable using a zval* index
static void
ht_delete (HashTable * ht, zval * ind)
{
  // This may fail if the index doesnt exist, which is fine.
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      zend_hash_index_del (ht, Z_LVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      zend_hash_index_del (ht, (long) Z_DVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      zend_hash_del (ht, "", sizeof (""));
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      zend_hash_del (ht, Z_STRVAL_P (ind), Z_STRLEN_P (ind) + 1);
    }
  else
    {
      // TODO avoid alloc
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);
      zend_hash_del (ht, Z_STRVAL_P (string_index),
		     Z_STRLEN_P (string_index) + 1);

      zval_ptr_dtor (&string_index);
    }
}

// Check if a key exists in a hashtable 
static int
ht_exists (HashTable * ht, zval * ind)
{
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      return zend_hash_index_exists (ht, Z_LVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      return zend_hash_index_exists (ht, (long) Z_DVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      return zend_hash_exists (ht, "", sizeof (""));
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      return zend_hash_exists (ht, Z_STRVAL_P (ind), Z_STRLEN_P (ind) + 1);
    }
  else
    {
      // TODO avoid alloc
      int result;
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);
      result = zend_hash_exists (ht, Z_STRVAL_P (string_index),
				 Z_STRLEN_P (string_index) + 1);
      zval_ptr_dtor (&string_index);
      return result;
    }
  assert (0);
}

static zval **
get_ht_entry (zval ** p_var, zval * ind TSRMLS_DC)
{
  if (Z_TYPE_P (*p_var) == IS_STRING)
    {
      if (Z_STRLEN_PP (p_var) > 0)
	{
	  php_error_docref (NULL TSRMLS_CC, E_ERROR,
			    "Cannot create references to/from string offsets nor overloaded objects");
	}
    }

  if (Z_TYPE_P (*p_var) != IS_ARRAY)
    {
      zval_ptr_dtor (p_var);
      ALLOC_INIT_ZVAL (*p_var);
      array_init (*p_var);
    }

  HashTable *ht = extract_ht (p_var TSRMLS_CC);

  zval **data;
  if (ht_find (ht, ind, &data) == SUCCESS)
    {
      assert (data != NULL);
      return data;
    }

  // If we dont find it, put EG (uninitialized_zval_ptr) into the
  // hashtable, and return a pointer to its container.
  EG (uninitialized_zval_ptr)->refcount++;
  ht_update (ht, ind, EG (uninitialized_zval_ptr), &data);

  assert (data != NULL);

  return data;
}


// Like extract_ht_ex, but for objects 
static HashTable *
extract_field_ex (zval * obj TSRMLS_DC)
{
  // TODO: this likely should be inlined somewhere.
  assert (!in_copy_on_write (obj));
  if (Z_TYPE_P (obj) == IS_NULL)
    {
      assert (0);
      // TODO: implement initialization
    }
  else if (Z_TYPE_P (obj) != IS_OBJECT)
    {
      // TODO: test if this is the right error message
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an object");
      // TODO: implement initialization
      assert (0);
    }
  return Z_OBJPROP_P (obj);
}

// Like extract_ht, but for objects
static HashTable *
extract_field (zval ** p_var TSRMLS_DC)
{
  sep_copy_on_write (p_var);

  return extract_field_ex (*p_var TSRMLS_CC);
}

// Like get_ht_entry, but for objects
static zval **
get_field (zval ** p_var, char *ind TSRMLS_DC)
{
  if (Z_TYPE_P (*p_var) != IS_OBJECT)
    {
      // TODO: implement initialization
      assert (0);
    }

  HashTable *ht = extract_field (p_var TSRMLS_CC);

  zval **data;
  if (zend_symtable_find (ht, ind, strlen (ind) + 1, (void **) &data) ==
      SUCCESS)
    {
      assert (data != NULL);
      return data;
    }

  // If we dont find it, put EG (uninitialized_zval_ptr) into the
  // hashtable, and return a pointer to its container.
  EG (uninitialized_zval_ptr)->refcount++;
  zend_symtable_update (ht, ind, strlen (ind) + 1,
			&EG (uninitialized_zval_ptr), sizeof (zval *),
			(void **) &data);

  assert (data != NULL);

  return data;
}

void
read_array (zval ** result, zval * array, zval * ind TSRMLS_DC)
{
  // Memory can be allocated in read_string_index
  if (array == EG (uninitialized_zval_ptr))
    {
      *result = array;
      return;
    }

  // Since we know its an array, and we dont write to it, we dont need
  // to separate it.
  HashTable *ht = Z_ARRVAL_P (array);

  // find the result
  zval **p_result;
  if (ht_find (ht, ind, &p_result) == SUCCESS)
    {
      *result = *p_result;
      return;
    }

  *result = EG (uninitialized_zval_ptr);
}

/* If its not an array, convert it into an array. */
static void
check_array_type (zval ** p_var TSRMLS_DC)
{
  if ((Z_TYPE_P (*p_var) == IS_BOOL && !Z_BVAL_PP (p_var))
      || Z_TYPE_P (*p_var) == IS_NULL
      || (Z_TYPE_P (*p_var) == IS_STRING && Z_STRLEN_PP (p_var) == 0))
    {
      // Non ref use new values
      if (!PZVAL_IS_REF (*p_var))
	{
	  zval_ptr_dtor (p_var);
	  ALLOC_INIT_ZVAL (*p_var);
	}
      else
	// Refs are just replaced
	zval_dtor (*p_var);

      array_init (*p_var);
    }
  else if (Z_TYPE_PP (p_var) != IS_STRING && Z_TYPE_PP (p_var) != IS_ARRAY)
    {
      // TODO: why are these different types than pushing
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an array");
    }
}

/* If its not an array, convert it into an object. */
static void
check_object_type (zval ** p_var TSRMLS_DC)
{
  // TODO: implement
}

/* Push EG (uninitialized_zval_ptr) and return a pointer into the ht
 * for it */
/*
 * Converted to array automatically:
 *    ""
 *    NULL
 *    false
 *
 * Warning, no conversion:
 *    ints
 *    floats
 *    true
 *
 * Error, no conversion:
 *    strings other than ""
 */
// TODO: objects, resources, etc
static zval **
push_and_index_ht (zval ** p_var TSRMLS_DC)
{
  // Check for errors conditions

  if (Z_TYPE_P (*p_var) == IS_STRING && Z_STRLEN_PP (p_var) > 0)
    {
      php_error_docref (NULL TSRMLS_CC, E_ERROR,
			"[] operator not supported for strings");
      assert (0);		// unreachable
    }

  if (Z_TYPE_P (*p_var) == IS_BOOL && Z_BVAL_PP (p_var)
      || Z_TYPE_P (*p_var) == IS_LONG || Z_TYPE_P (*p_var) == IS_DOUBLE)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an array");
      return NULL;
    }

  if (Z_TYPE_P (*p_var) != IS_ARRAY)
    {
      zval_ptr_dtor (p_var);
      ALLOC_INIT_ZVAL (*p_var);
      array_init (*p_var);
    }

  // if its not an array, make it an array
  HashTable *ht = extract_ht (p_var TSRMLS_CC);
  zval **data;

  EG (uninitialized_zval_ptr)->refcount++;
  int result = zend_hash_next_index_insert (ht, &EG (uninitialized_zval_ptr),
					    sizeof (zval *), (void **) &data);
  assert (result == SUCCESS);

  assert (data);

  return data;
}


/*
 * isset
 */
static int
isset_var (HashTable * st, char *name, int length)
{
  return zend_hash_exists (st, name, length);
}

static int
isset_array (zval ** p_var, zval * ind)
{
  if (Z_TYPE_P (*p_var) == IS_STRING)
    {
      ind = zvp_clone_ex (ind);
      convert_to_long (ind);
      int result = (Z_LVAL_P (ind) >= 0
		    && Z_LVAL_P (ind) < Z_STRLEN_PP (p_var));
      assert (ind->refcount == 1);
      zval_ptr_dtor (&ind);
      return result;
    }

  // NO error required; return false
  if (Z_TYPE_P (*p_var) != IS_ARRAY)
    return 0;

  // if its not an array, make it an array
  HashTable *ht = Z_ARRVAL_P (*p_var);

  zval **data;
  if (ht_find (ht, ind, &data) == SUCCESS)
    {
      return !ZVAL_IS_NULL (*data);
    }
  else
    return 0;
}


static zval **
fetch_var_arg_by_ref (zval ** p_arg)
{
  // We are passing by reference
  sep_copy_on_write (p_arg);

  // We don't need to restore ->is_ref afterwards,
  // because the called function will reduce the
  // refcount of arg on return, and will reset is_ref to
  // 0 when refcount drops to 1.  If the refcount does
  // not drop to 1 when the function returns, but we did
  // set is_ref to 1 here, that means that is_ref must
  // already have been 1 to start with (since if it had
  // not, that means that the variable would have been
  // in a copy-on-write set, and would have been
  // seperated above).
  (*p_arg)->is_ref = 1;

  return p_arg;
}

/* Dont pass-by-ref */
static zval *
fetch_var_arg (zval * arg, int *is_arg_new)
{
  if (arg->is_ref)
    {
      // We dont separate since we don't own one of ARG's references.
      arg = zvp_clone_ex (arg);
      *is_arg_new = 1;

      // It seems we get incorrect refcounts without this.
      // TODO This decreases the refcount to zero, which seems wrong,
      // but gives the right answer. We should look at how zend does
      // this.

      arg->refcount--;
    }
  return arg;
}

// TODO dont overwrite line numbers if we're compiling an extension
static void
phc_setup_error (int init, char *filename, int line_number,
		 zend_function * function TSRMLS_DC)
{
  static int old_in_compilation;
  static int old_in_execution;
  static char *old_filename;
  static int old_lineno;
  static zend_function *old_function;
  if (init)
    {
      if (filename == NULL)
	filename = "[phc_compiled_file]";
      // Save old values
      old_in_compilation = CG (in_compilation);
      old_in_execution = EG (in_execution);
      old_filename = CG (compiled_filename);
      old_lineno = CG (zend_lineno);
      old_function = EG (function_state_ptr)->function;
      // Put in our values
      CG (in_compilation) = 1;
      EG (in_execution) = 1;
      CG (compiled_filename) = filename;
      CG (zend_lineno) = line_number;
      if (function)
	EG (function_state_ptr)->function = function;
    }
  else
    {
      CG (in_compilation) = old_in_compilation;
      EG (in_execution) = old_in_execution;
      CG (compiled_filename) = old_filename;
      CG (zend_lineno) = old_lineno;
      EG (function_state_ptr)->function = old_function;
    }
}

static void
initialize_function_call (zend_fcall_info * fci, zend_fcall_info_cache * fcic,
			  char *function_name, char *filename,
			  int line_number TSRMLS_DC)
{
  if (fcic->initialized)
    return;

  zval fn;
  INIT_PZVAL (&fn);
  ZVAL_STRING (&fn, function_name, 0);
  int result = zend_fcall_info_init (&fn, fci, fcic TSRMLS_CC);
  if (result != SUCCESS)
    {
      phc_setup_error (1, filename, line_number, NULL TSRMLS_CC);
      php_error_docref (NULL TSRMLS_CC, E_ERROR,
			"Call to undefined function %s()", function_name);
    }
}

/*
 * Initialize zend_fcall_info for a method lookup
 *
 * Implementation partly based on zend_call_method in Zend/zend_interfaces.c
 * Main difference is that we use Z_OBJ_HTT_PP(obj)->get_method to retrieve
 * the function handler for the method instead of looking it up directly;
 * this means that we correctly deal with __call.
 */

static void
initialize_method_call (zend_fcall_info * fci, zend_fcall_info_cache * fcic,
			zval ** obj, char *function_name,
			char *filename, int line_number TSRMLS_DC)
{
  if (fcic->initialized)
    return;

  zend_class_entry *obj_ce;
  obj_ce = Z_OBJCE_PP (obj);

  /*
   * we do not initialize fci.
   *   function_table  --  not initialized by zend_call_method
   *   function_name   --  zend_call_method initializes this to a pointer to
   *                       a zval 'z_fname', but does not initialize z_fname
   *                       in case of a method invocation
   *   retval_ptr_ptr  --  should be initialized by caller
   *   param_count     --  should be initialized by caller
   *   params          --  should be initialized by caller
   */
  fci->size = sizeof (*fci);
  fci->object_pp = obj;
  fci->no_separation = 1;
  fci->symbol_table = NULL;

  fcic->initialized = 1;
  fcic->calling_scope = obj_ce;
  fcic->object_pp = obj;
  fcic->function_handler
    = Z_OBJ_HT_PP (obj)->get_method (obj,
				     function_name,
				     strlen (function_name) TSRMLS_CC);

  if (fcic->function_handler == NULL)
    {
      phc_setup_error (1, filename, line_number, NULL TSRMLS_CC);
      php_error_docref (NULL TSRMLS_CC, E_ERROR,
			"Call to undefined method %s::%s",
			obj_ce->name, function_name);
    }
}

/*
 * Like initialize_method_call, but return 0 if no constructor is defined
 * rather than giving an error.
 */

static int 
initialize_constructor_call (zend_fcall_info * fci,
			     zend_fcall_info_cache * fcic, zval ** obj,
			     char *filename, int line_number TSRMLS_DC)
{
  if (fcic->initialized)
    return;

  zend_class_entry *obj_ce;
  obj_ce = Z_OBJCE_PP (obj);

  /*
   * we do not initialize fci.
   *   function_table  --  not initialized by zend_call_method
   *   function_name   --  zend_call_method initializes this to a pointer to
   *                       a zval 'z_fname', but does not initialize z_fname
   *                       in case of a method invocation
   *   retval_ptr_ptr  --  should be initialized by caller
   *   param_count     --  should be initialized by caller
   *   params          --  should be initialized by caller
   */
  fci->size = sizeof (*fci);
  fci->object_pp = obj;
  fci->no_separation = 1;
  fci->symbol_table = NULL;

  fcic->initialized = 1;
  fcic->calling_scope = obj_ce;
  fcic->object_pp = obj;
  fcic->function_handler
    = Z_OBJ_HT_PP (obj)->get_constructor (*obj TSRMLS_CC);

  return (fcic->function_handler != NULL);
}
// vi:set ts=8:

/*
 * Creates a copy of *in using persistent memory, optionally destroying *in
 *
 * Does not work for objects/resources and will loop on self-recursive arrays.
 */

zval *
persistent_clone (zval * in, int destroy_in TSRMLS_DC)
{
  zval *out = pemalloc (sizeof (zval), 1);
  *out = *in;

  switch (Z_TYPE_P (in))
    {
    case IS_NULL:
    case IS_LONG:
    case IS_DOUBLE:
    case IS_BOOL:
      /* nothing more to be done */
      break;
    case IS_STRING:
      Z_STRVAL_P (out) = pemalloc (Z_STRLEN_P (in) + 1, 1);
      memcpy (Z_STRVAL_P (out), Z_STRVAL_P (in), Z_STRLEN_P (in) + 1);
      break;
    case IS_ARRAY:
      {
	HashTable *old_arr = Z_ARRVAL_P (in);
	HashTable *new_arr = pemalloc (sizeof (HashTable), 1);
	zend_hash_init (new_arr, old_arr->nNumOfElements, NULL, ZVAL_PTR_DTOR,
			/* persistent */ 1);

	for (zend_hash_internal_pointer_reset (old_arr);
	     zend_hash_has_more_elements (old_arr) == SUCCESS;
	     zend_hash_move_forward (old_arr))
	  {
	    char *key;
	    uint keylen;
	    ulong idx;
	    int type;
	    zval **old_elem, *new_elem;

	    type =
	      zend_hash_get_current_key_ex (old_arr, &key, &keylen, &idx, 0,
					    NULL);
	    assert (zend_hash_get_current_data
		    (old_arr, (void **) &old_elem) == SUCCESS);

	    new_elem = persistent_clone (*old_elem, destroy_in TSRMLS_CC);

	    if (type == HASH_KEY_IS_STRING)
	      zend_hash_add (new_arr, key, keylen, &new_elem, sizeof (zval *),
			     NULL);
	    else
	      zend_hash_index_update (new_arr, idx, &new_elem,
				      sizeof (zval *), NULL);

	  }

	Z_ARRVAL_P (out) = new_arr;
      }
      break;
    default:
      /* other types are not supported */
      assert (0);
    }

  zval_ptr_dtor (&in);
  return out;
}

/*
 * Wrapper around zend_declare_property which 
 *
 * - Asserts that the ZEND_INTERNAL_CLASS flag is cleared
 *   (otherwise we cannot add complex (i.e., array) properties)
 * - Creates a persistent clone of the property to be added before
 *   calling zend_declare_property, since the memory for this property
 *   should only be deallocated when the module is shut down
 *   (and not when the request finishes)
 * - Cleans up after zend_declare_property by re-allocating the name of 
 *   the property using persistent memory, for much the same reason
 */

static int
phc_declare_property (zend_class_entry * ce, char *name, int name_length,
		      zval * property, int access_type TSRMLS_DC)
{
  assert (!(ce->type & ZEND_INTERNAL_CLASS));
  assert (zend_declare_property
	  (ce, name, name_length, persistent_clone (property, 1 TSRMLS_CC),
	   access_type TSRMLS_CC) == SUCCESS);

  zend_property_info *property_info;
  assert (zend_hash_find
	  (&ce->properties_info, name, name_length + 1,
	   (void **) &property_info) == SUCCESS);
  efree (property_info->name);
  property_info->name = name;

  return SUCCESS;
}

// vi:set ts=8:


static void
cast_var (zval ** p_zvp, int type)
{
  assert (type >= 0 && type <= 6);
  if ((*p_zvp)->type == type)
    return;

  sep_copy_on_write (p_zvp);
  zval *zvp = *p_zvp;

  switch (type)
    {
    case IS_NULL:
      convert_to_null (zvp);
      break;
    case IS_BOOL:
      convert_to_boolean (zvp);
      break;
    case IS_LONG:
      convert_to_long (zvp);
      break;
    case IS_DOUBLE:
      convert_to_double (zvp);
      break;
    case IS_STRING:
      convert_to_string (zvp);
      break;
    case IS_ARRAY:
      convert_to_array (zvp);
      break;
    case IS_OBJECT:
      convert_to_object (zvp);
      break;
    default:
      assert (0);		// TODO unimplemented
      break;
    }
}

/* Copies a constant into ZVP. Note that LENGTH does not include the NULL-terminating byte. */
static void
get_constant (char *name, int length, zval ** p_zvp TSRMLS_DC)
{
  MAKE_STD_ZVAL (*p_zvp);
  // zend_get_constant returns 1 for success, not SUCCESS
  int result = zend_get_constant (name, length, *p_zvp TSRMLS_CC);
  if (result == 0)
    ZVAL_STRINGL (*p_zvp, name, length, 1);
}

/* The function call mechanism deals specially with EG(uninitialize_zval_ptr)
 * (or sometime EG(uninitialize_zval)), so we need to use this too. This
 * particular zval can also be set, but there is an implicit guarantee 
 * of the information below.
 *
 * If assertions are off, this should be inlined to nothing.
 */
static void
phc_check_invariants (TSRMLS_D)
{
  assert (EG (uninitialized_zval_ptr) == &EG (uninitialized_zval));
  assert (EG (uninitialized_zval).refcount >= 1);
  assert (EG (uninitialized_zval).value.lval == 0);
  assert (EG (uninitialized_zval).type == IS_NULL);
  assert (EG (uninitialized_zval).is_ref == 0);
}


static int
check_unset_index_type (zval * ind TSRMLS_DC)
{
  if (Z_TYPE_P (ind) == IS_OBJECT || Z_TYPE_P (ind) == IS_ARRAY)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Illegal offset type in unset");
      return 0;
    }

  return 1;
}



/*
 * unset
 */

static void
unset_var (HashTable * st, char *name, int length)
{
  zend_hash_del (st, name, length);
}

static void
unset_array (zval ** p_var, zval * ind TSRMLS_DC)
{
  // NO error required
  if (Z_TYPE_PP (p_var) != IS_ARRAY)
    {
      if (Z_TYPE_PP (p_var) == IS_STRING)
	{
	  php_error_docref (NULL TSRMLS_CC, E_ERROR,
			    "Cannot unset string offsets");
	}
      else if (Z_TYPE_PP (p_var) != IS_NULL)
	{
	  php_error_docref (NULL TSRMLS_CC, E_WARNING,
			    "Cannot unset offsets in a non-array variable");
	}

      return;
    }

  // if its not an array, make it an array
  HashTable *ht = Z_ARRVAL_P (*p_var);

  ht_delete (ht, ind);
}

/*
 * Lookup variable whose name is var_var in st. We do not call
 * ht_find because ht_find uses zend_symtable_find to search for strings
 * rather than zend_hash_find. The difference is that zend_symtable_find
 * will convert strings to integers where possible: arrays are always
 * integer-indexed if at all possible. Variable names however should
 * _always_ be treated as strings.
 * 
 */

/*
 * If the parameter is a string, returns the parameter, with the refcount
 * incremented. If its not a string, returns a new zval, with a refcount of
 * 1. Either way, zval_dtor_ptr must be run by the caller on the return
 * value.
 */
zval*
get_string_val (zval* zvp)
{
  if (Z_TYPE_P (zvp) == IS_STRING)
    {
      zvp->refcount++;
      return zvp;
    }
  else
    {
      zval* clone = zvp_clone_ex (zvp);
      convert_to_string (clone);
      return clone;
    }
}

zval **
get_var_var (HashTable * st, zval * index TSRMLS_DC)
{
  zval* str_index = get_string_val (index);
  char* name = Z_STRVAL_P (str_index);
  int length = Z_STRLEN_P (str_index) + 1;
  unsigned long hash = zend_get_hash_value (name, length);

  zval** result = get_st_entry (st, name, length, hash TSRMLS_CC);
  zval_ptr_dtor (&str_index);
  return result;
}

/* 
 * Read the variable described by var_var from symbol table st
 * See comments for get_var_var
 */
zval *
read_var_var (HashTable * st, zval * index TSRMLS_DC)
{
  zval* str_index = get_string_val (index);
  char* name = Z_STRVAL_P (str_index);
  int length = Z_STRLEN_P (str_index) + 1;
  unsigned long hash = zend_get_hash_value (name, length);

  zval* result = read_var (st, name, length, hash TSRMLS_CC);
  zval_ptr_dtor (&str_index);
  return result;
}

static void
phc_builtin_eval (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  // If the user wrote "return ..", we need to store the
  // return value; however, in that case, zend_eval_string
  // will slap an extra "return" onto the front of the string,
  // so we must remove the "return" from the string the user
  // wrote. If the user did not write "return", he is not
  // interested in the return value, and we must pass NULL
  // instead or rhs to avoid zend_eval_string adding "return".

  // convert to a string
  // TODO avoid allocation
  zval *copy = zvp_clone_ex (arg);
  convert_to_string (copy);

  if (*p_result && !strncmp (Z_STRVAL_P (copy), "return ", 7))
    {
      zend_eval_string (Z_STRVAL_P (copy) + 7, *p_result,
			"eval'd code" TSRMLS_CC);
    }
  else
    {
      zend_eval_string (Z_STRVAL_P (copy), NULL, "eval'd code" TSRMLS_CC);
    }

  // cleanup
  assert (copy->refcount == 1);
  zval_ptr_dtor (&copy);
}

static void
phc_builtin_exit (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  if (Z_TYPE_P (arg) == IS_LONG)
    EG (exit_status) = Z_LVAL_P (arg);
  else
    zend_print_variable (arg);

  zend_bailout ();
}

static void
phc_builtin_die (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  phc_builtin_exit (arg, p_result, filename TSRMLS_CC);
}

static void
phc_builtin_echo (zval * arg, zval ** p_result TSRMLS_DC)
{
  assert (*p_result == NULL);
  zend_print_variable (arg);
}

static void
phc_builtin_print (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  zval *echo_arg = NULL;
  phc_builtin_echo (arg, &echo_arg TSRMLS_CC);

  if (*p_result)
    ZVAL_LONG (*p_result, 1);
}

// TODO is there a memory leak here is result has a value?
// TOOD isnt this just the same as isset
static void
phc_builtin_empty (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  assert (*p_result);
  ZVAL_BOOL (*p_result, !zend_is_true (arg));
}

// For require, include, require_once and include_once.

// Include:
//    return 1 for success
//    Warning, and return false for failure
// Require:
//    return 1 for success
//    Fail for failure
// Include_once
//    Return true if already included
//    Return 1 for success
//    Warning and return false for failure
// Require_once:
//    Return true if already included
//    return 1 for success
//    Fail for failure
//
static void
include_backend (zval * arg, zval ** p_result, char *filename, int type, int is_once, char* error, char* error_function TSRMLS_DC)
{
  // In the event that the Zend engine cannot find the file, after checking the
  // include path, it tries the current directory. It does this only if the
  // interpreter is executing, and it checks the interpreters opcodes for a
  // filename (see streams/plain_wrapper.c:1352)

  // An alternative is to add the directory to include_path, but its
  // semantically incorrect (get_included_path() would give the wrong answer),
  // and error prone (if people overwrite include_path).
  // TODO: though we could add it for this function only

  assert (EG (active_op_array) == NULL);
  assert (filename != NULL);

  zval *arg_file = arg;
  // Check we have a string
  if (Z_TYPE_P (arg_file) != IS_STRING)
    {
      arg_file = zvp_clone_ex (arg_file);
      convert_to_string (arg_file);
    }

  zend_file_handle handle;
  zend_op_array* new_op_array;
  zend_function zf;

  // Check the _ONCE varieties (based on zend_vm_def.h)
   if (is_once)
     {
       if (IS_ABSOLUTE_PATH (Z_STRVAL_P (arg_file), Z_STRLEN_P (arg_file)))
	 {
	   // Get the proper path name for require
	   cwd_state state;

	   state.cwd_length = 0;
	   state.cwd = malloc(1);
	   state.cwd[0] = 0;
	   int success = !virtual_file_ex(&state, Z_STRVAL_P(arg_file), NULL, 1)
	     && zend_hash_exists(&EG(included_files), state.cwd,
				 state.cwd_length+1);

	   free (state.cwd);

	   if (!success)
	     goto cleanup;
	 }
     }


   // Compile the file
   // Pretend the interpreter is running
   EG (in_execution) = 1;

   int success = zend_stream_open (Z_STRVAL_P (arg_file), &handle TSRMLS_CC);

   // Stop pretending
   EG (in_execution) = 0;
   EG (active_op_array) = NULL;

   if (success != SUCCESS)
     goto fail;


   if (is_once)
     {
       // Check it hadnt been included already
       int once_success = zend_hash_add_empty_element(&EG(included_files),
						      handle.opened_path,
						      strlen (handle.opened_path)+1);
       // Return true 
       if (once_success != SUCCESS)
	 {
	   ZVAL_BOOL (*p_result, 1);
	   goto cleanup;
	 }
     }

   if (!handle.opened_path)
     handle.opened_path = estrndup (Z_STRVAL_P(arg_file), Z_STRLEN_P (arg_file));

   // run it
   success = zend_execute_scripts (type TSRMLS_CC, p_result, 1, &handle);
   assert (success == SUCCESS);
   zend_stream_close (&handle);

   // Success
   if (*p_result)
       ZVAL_LONG (*p_result, 1);


   goto cleanup;


fail:

   php_error_docref (error_function
		     TSRMLS_CC, 
		     (type == ZEND_INCLUDE) ? E_WARNING : E_ERROR,
		     error,
		     php_strip_url_passwd (Z_STRVAL_P (arg_file)),
		     STR_PRINT (PG (include_path)));


   // Failure
   if (*p_result)
     ZVAL_BOOL (*p_result, 0);

cleanup:

   if (handle.opened_path)
     efree (handle.opened_path);
   zend_destroy_file_handle (&handle TSRMLS_CC);


  if (arg != arg_file)
    zval_ptr_dtor (&arg_file);
}

static void
phc_builtin_include (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_INCLUDE,
		    0,
		    "Failed opening '%s' for inclusion (include_path='%s')",
		    "function.include"
		    TSRMLS_CC);
}

static void
phc_builtin_require (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_REQUIRE,
		    0,
		    "Failed opening required '%s' (include_path='%s')",
		    "function.require"
		    TSRMLS_CC);
}

static void
phc_builtin_include_once (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_INCLUDE,
		    1,
		    "Failed opening '%s' for inclusion (include_path='%s')",
		    "function.include_once"
		    TSRMLS_CC);
}

static void
phc_builtin_require_once (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_REQUIRE,
		    1,
		    "Failed opening required '%s' (include_path='%s')",
		    "function.require_once"
		    TSRMLS_CC);
}

// END INCLUDED FILES
int saved_refcount;
static zend_fcall_info wfprofilein_fci;
static zend_fcall_info_cache wfprofilein_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info wfprofileout_fci;
static zend_fcall_info_cache wfprofileout_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info wfsetvar_fci;
static zend_fcall_info_cache wfsetvar_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info wftimestampnow_fci;
static zend_fcall_info_cache wftimestampnow_fcic = {0,NULL,NULL,NULL};
// class ParserOptions
// {
// 	var $mUseTeX = NULL;
// 	var $mUseDynamicDates = NULL;
// 	var $mInterwikiMagic = NULL;
// 	var $mAllowExternalImages = NULL;
// 	var $mAllowExternalImagesFrom = NULL;
// 	var $mEnableImageWhitelist = NULL;
// 	var $mSkin = NULL;
// 	var $mDateFormat = NULL;
// 	var $mEditSection = NULL;
// 	var $mNumberHeadings = NULL;
// 	var $mAllowSpecialInclusion = NULL;
// 	var $mTidy = NULL;
// 	var $mInterfaceMessage = NULL;
// 	var $mTargetLanguage = NULL;
// 	var $mMaxIncludeSize = NULL;
// 	var $mMaxPPNodeCount = NULL;
// 	var $mMaxPPExpandDepth = NULL;
// 	var $mMaxTemplateDepth = NULL;
// 	var $mRemoveComments = NULL;
// 	var $mTemplateCallback = NULL;
// 	var $mEnableLimitReport = NULL;
// 	var $mTimestamp = NULL;
// 	var $mExternalLinkTarget = NULL;
// 	var $mUser = NULL;
// 	var $mIsPreview = NULL;
// 	var $mIsSectionPreview = NULL;
// 	function getusetex()
// 	{
// 		$TSt0 = $this->mUseTeX;
// 		return $TSt0;
// 	}
// 	function getusedynamicdates()
// 	{
// 		$TSt1 = $this->mUseDynamicDates;
// 		return $TSt1;
// 	}
// 	function getinterwikimagic()
// 	{
// 		$TSt2 = $this->mInterwikiMagic;
// 		return $TSt2;
// 	}
// 	function getallowexternalimages()
// 	{
// 		$TSt3 = $this->mAllowExternalImages;
// 		return $TSt3;
// 	}
// 	function getallowexternalimagesfrom()
// 	{
// 		$TSt4 = $this->mAllowExternalImagesFrom;
// 		return $TSt4;
// 	}
// 	function getenableimagewhitelist()
// 	{
// 		$TSt5 = $this->mEnableImageWhitelist;
// 		return $TSt5;
// 	}
// 	function geteditsection()
// 	{
// 		$TSt6 = $this->mEditSection;
// 		return $TSt6;
// 	}
// 	function getnumberheadings()
// 	{
// 		$TSt7 = $this->mNumberHeadings;
// 		return $TSt7;
// 	}
// 	function getallowspecialinclusion()
// 	{
// 		$TSt8 = $this->mAllowSpecialInclusion;
// 		return $TSt8;
// 	}
// 	function gettidy()
// 	{
// 		$TSt9 = $this->mTidy;
// 		return $TSt9;
// 	}
// 	function getinterfacemessage()
// 	{
// 		$TSt10 = $this->mInterfaceMessage;
// 		return $TSt10;
// 	}
// 	function gettargetlanguage()
// 	{
// 		$TSt11 = $this->mTargetLanguage;
// 		return $TSt11;
// 	}
// 	function getmaxincludesize()
// 	{
// 		$TSt12 = $this->mMaxIncludeSize;
// 		return $TSt12;
// 	}
// 	function getmaxppnodecount()
// 	{
// 		$TSt13 = $this->mMaxPPNodeCount;
// 		return $TSt13;
// 	}
// 	function getmaxtemplatedepth()
// 	{
// 		$TSt14 = $this->mMaxTemplateDepth;
// 		return $TSt14;
// 	}
// 	function getremovecomments()
// 	{
// 		$TSt15 = $this->mRemoveComments;
// 		return $TSt15;
// 	}
// 	function gettemplatecallback()
// 	{
// 		$TSt16 = $this->mTemplateCallback;
// 		return $TSt16;
// 	}
// 	function getenablelimitreport()
// 	{
// 		$TSt17 = $this->mEnableLimitReport;
// 		return $TSt17;
// 	}
// 	function getcleansignatures()
// 	{
// 		$TSt18 = $this->mCleanSignatures;
// 		return $TSt18;
// 	}
// 	function getexternallinktarget()
// 	{
// 		$TSt19 = $this->mExternalLinkTarget;
// 		return $TSt19;
// 	}
// 	function getispreview()
// 	{
// 		$TSt20 = $this->mIsPreview;
// 		return $TSt20;
// 	}
// 	function getissectionpreview()
// 	{
// 		$TSt21 = $this->mIsSectionPreview;
// 		return $TSt21;
// 	}
// 	function getskin()
// 	{
// 		$TMIt80 = $this->mSkin;
// 		$TLE22 = isset($TMIt80);
// 		$TLE23 = !$TLE22;
// 		if (TLE23) goto L131 else goto L132;
// 	L131:
// 		$TSt24 = $this->mUser;
// 		$TLE25 = $TSt24->getskin();
// 		$this->mSkin = $TLE25;
// 		goto L133;
// 	L132:
// 		goto L133;
// 	L133:
// 		$TSt26 = $this->mSkin;
// 		return $TSt26;
// 	}
// 	function getdateformat()
// 	{
// 		$TMIt81 = $this->mDateFormat;
// 		$TLE27 = isset($TMIt81);
// 		$TLE28 = !$TLE27;
// 		if (TLE28) goto L134 else goto L135;
// 	L134:
// 		$TSt29 = $this->mUser;
// 		$TLE30 = $TSt29->getdatepreference();
// 		$this->mDateFormat = $TLE30;
// 		goto L136;
// 	L135:
// 		goto L136;
// 	L136:
// 		$TSt31 = $this->mDateFormat;
// 		return $TSt31;
// 	}
// 	function gettimestamp()
// 	{
// 		$TMIt82 = $this->mTimestamp;
// 		$TLE32 = isset($TMIt82);
// 		$TLE33 = !$TLE32;
// 		if (TLE33) goto L137 else goto L138;
// 	L137:
// 		$TLE34 = wftimestampnow();
// 		$this->mTimestamp = $TLE34;
// 		goto L139;
// 	L138:
// 		goto L139;
// 	L139:
// 		$TSt35 = $this->mTimestamp;
// 		return $TSt35;
// 	}
// 	function setusetex($x)
// 	{
// 		$TLE84 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE84) goto L140 else goto L141;
// 	L140:
// 		$TMIt83 =& $this->mUseTeX;
// 		goto L142;
// 	L141:
// 		$TMIt83 = $this->mUseTeX;
// 		goto L142;
// 	L142:
// 		$TLE36 = wfsetvar($TMIt83, $x);
// 		return $TLE36;
// 	}
// 	function setusedynamicdates($x)
// 	{
// 		$TLE86 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE86) goto L143 else goto L144;
// 	L143:
// 		$TMIt85 =& $this->mUseDynamicDates;
// 		goto L145;
// 	L144:
// 		$TMIt85 = $this->mUseDynamicDates;
// 		goto L145;
// 	L145:
// 		$TLE37 = wfsetvar($TMIt85, $x);
// 		return $TLE37;
// 	}
// 	function setinterwikimagic($x)
// 	{
// 		$TLE88 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE88) goto L146 else goto L147;
// 	L146:
// 		$TMIt87 =& $this->mInterwikiMagic;
// 		goto L148;
// 	L147:
// 		$TMIt87 = $this->mInterwikiMagic;
// 		goto L148;
// 	L148:
// 		$TLE38 = wfsetvar($TMIt87, $x);
// 		return $TLE38;
// 	}
// 	function setallowexternalimages($x)
// 	{
// 		$TLE90 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE90) goto L149 else goto L150;
// 	L149:
// 		$TMIt89 =& $this->mAllowExternalImages;
// 		goto L151;
// 	L150:
// 		$TMIt89 = $this->mAllowExternalImages;
// 		goto L151;
// 	L151:
// 		$TLE39 = wfsetvar($TMIt89, $x);
// 		return $TLE39;
// 	}
// 	function setallowexternalimagesfrom($x)
// 	{
// 		$TLE92 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE92) goto L152 else goto L153;
// 	L152:
// 		$TMIt91 =& $this->mAllowExternalImagesFrom;
// 		goto L154;
// 	L153:
// 		$TMIt91 = $this->mAllowExternalImagesFrom;
// 		goto L154;
// 	L154:
// 		$TLE40 = wfsetvar($TMIt91, $x);
// 		return $TLE40;
// 	}
// 	function setenableimagewhitelist($x)
// 	{
// 		$TLE94 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE94) goto L155 else goto L156;
// 	L155:
// 		$TMIt93 =& $this->mEnableImageWhitelist;
// 		goto L157;
// 	L156:
// 		$TMIt93 = $this->mEnableImageWhitelist;
// 		goto L157;
// 	L157:
// 		$TLE41 = wfsetvar($TMIt93, $x);
// 		return $TLE41;
// 	}
// 	function setdateformat($x)
// 	{
// 		$TLE96 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE96) goto L158 else goto L159;
// 	L158:
// 		$TMIt95 =& $this->mDateFormat;
// 		goto L160;
// 	L159:
// 		$TMIt95 = $this->mDateFormat;
// 		goto L160;
// 	L160:
// 		$TLE42 = wfsetvar($TMIt95, $x);
// 		return $TLE42;
// 	}
// 	function seteditsection($x)
// 	{
// 		$TLE98 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE98) goto L161 else goto L162;
// 	L161:
// 		$TMIt97 =& $this->mEditSection;
// 		goto L163;
// 	L162:
// 		$TMIt97 = $this->mEditSection;
// 		goto L163;
// 	L163:
// 		$TLE43 = wfsetvar($TMIt97, $x);
// 		return $TLE43;
// 	}
// 	function setnumberheadings($x)
// 	{
// 		$TLE100 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE100) goto L164 else goto L165;
// 	L164:
// 		$TMIt99 =& $this->mNumberHeadings;
// 		goto L166;
// 	L165:
// 		$TMIt99 = $this->mNumberHeadings;
// 		goto L166;
// 	L166:
// 		$TLE44 = wfsetvar($TMIt99, $x);
// 		return $TLE44;
// 	}
// 	function setallowspecialinclusion($x)
// 	{
// 		$TLE102 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE102) goto L167 else goto L168;
// 	L167:
// 		$TMIt101 =& $this->mAllowSpecialInclusion;
// 		goto L169;
// 	L168:
// 		$TMIt101 = $this->mAllowSpecialInclusion;
// 		goto L169;
// 	L169:
// 		$TLE45 = wfsetvar($TMIt101, $x);
// 		return $TLE45;
// 	}
// 	function settidy($x)
// 	{
// 		$TLE104 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE104) goto L170 else goto L171;
// 	L170:
// 		$TMIt103 =& $this->mTidy;
// 		goto L172;
// 	L171:
// 		$TMIt103 = $this->mTidy;
// 		goto L172;
// 	L172:
// 		$TLE46 = wfsetvar($TMIt103, $x);
// 		return $TLE46;
// 	}
// 	function setskin($x)
// 	{
// 		$this->mSkin = $x;
// 	}
// 	function setinterfacemessage($x)
// 	{
// 		$TLE106 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE106) goto L173 else goto L174;
// 	L173:
// 		$TMIt105 =& $this->mInterfaceMessage;
// 		goto L175;
// 	L174:
// 		$TMIt105 = $this->mInterfaceMessage;
// 		goto L175;
// 	L175:
// 		$TLE47 = wfsetvar($TMIt105, $x);
// 		return $TLE47;
// 	}
// 	function settargetlanguage($x)
// 	{
// 		$TLE108 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE108) goto L176 else goto L177;
// 	L176:
// 		$TMIt107 =& $this->mTargetLanguage;
// 		goto L178;
// 	L177:
// 		$TMIt107 = $this->mTargetLanguage;
// 		goto L178;
// 	L178:
// 		$TLE48 = wfsetvar($TMIt107, $x);
// 		return $TLE48;
// 	}
// 	function setmaxincludesize($x)
// 	{
// 		$TLE110 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE110) goto L179 else goto L180;
// 	L179:
// 		$TMIt109 =& $this->mMaxIncludeSize;
// 		goto L181;
// 	L180:
// 		$TMIt109 = $this->mMaxIncludeSize;
// 		goto L181;
// 	L181:
// 		$TLE49 = wfsetvar($TMIt109, $x);
// 		return $TLE49;
// 	}
// 	function setmaxppnodecount($x)
// 	{
// 		$TLE112 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE112) goto L182 else goto L183;
// 	L182:
// 		$TMIt111 =& $this->mMaxPPNodeCount;
// 		goto L184;
// 	L183:
// 		$TMIt111 = $this->mMaxPPNodeCount;
// 		goto L184;
// 	L184:
// 		$TLE50 = wfsetvar($TMIt111, $x);
// 		return $TLE50;
// 	}
// 	function setmaxtemplatedepth($x)
// 	{
// 		$TLE114 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE114) goto L185 else goto L186;
// 	L185:
// 		$TMIt113 =& $this->mMaxTemplateDepth;
// 		goto L187;
// 	L186:
// 		$TMIt113 = $this->mMaxTemplateDepth;
// 		goto L187;
// 	L187:
// 		$TLE51 = wfsetvar($TMIt113, $x);
// 		return $TLE51;
// 	}
// 	function setremovecomments($x)
// 	{
// 		$TLE116 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE116) goto L188 else goto L189;
// 	L188:
// 		$TMIt115 =& $this->mRemoveComments;
// 		goto L190;
// 	L189:
// 		$TMIt115 = $this->mRemoveComments;
// 		goto L190;
// 	L190:
// 		$TLE52 = wfsetvar($TMIt115, $x);
// 		return $TLE52;
// 	}
// 	function settemplatecallback($x)
// 	{
// 		$TLE118 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE118) goto L191 else goto L192;
// 	L191:
// 		$TMIt117 =& $this->mTemplateCallback;
// 		goto L193;
// 	L192:
// 		$TMIt117 = $this->mTemplateCallback;
// 		goto L193;
// 	L193:
// 		$TLE53 = wfsetvar($TMIt117, $x);
// 		return $TLE53;
// 	}
// 	function enablelimitreport($x = True)
// 	{
// 		$TLE120 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE120) goto L194 else goto L195;
// 	L194:
// 		$TMIt119 =& $this->mEnableLimitReport;
// 		goto L196;
// 	L195:
// 		$TMIt119 = $this->mEnableLimitReport;
// 		goto L196;
// 	L196:
// 		$TLE54 = wfsetvar($TMIt119, $x);
// 		return $TLE54;
// 	}
// 	function settimestamp($x)
// 	{
// 		$TLE122 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE122) goto L197 else goto L198;
// 	L197:
// 		$TMIt121 =& $this->mTimestamp;
// 		goto L199;
// 	L198:
// 		$TMIt121 = $this->mTimestamp;
// 		goto L199;
// 	L199:
// 		$TLE55 = wfsetvar($TMIt121, $x);
// 		return $TLE55;
// 	}
// 	function setcleansignatures($x)
// 	{
// 		$TLE124 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE124) goto L200 else goto L201;
// 	L200:
// 		$TMIt123 =& $this->mCleanSignatures;
// 		goto L202;
// 	L201:
// 		$TMIt123 = $this->mCleanSignatures;
// 		goto L202;
// 	L202:
// 		$TLE56 = wfsetvar($TMIt123, $x);
// 		return $TLE56;
// 	}
// 	function setexternallinktarget($x)
// 	{
// 		$TLE126 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE126) goto L203 else goto L204;
// 	L203:
// 		$TMIt125 =& $this->mExternalLinkTarget;
// 		goto L205;
// 	L204:
// 		$TMIt125 = $this->mExternalLinkTarget;
// 		goto L205;
// 	L205:
// 		$TLE57 = wfsetvar($TMIt125, $x);
// 		return $TLE57;
// 	}
// 	function setispreview($x)
// 	{
// 		$TLE128 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE128) goto L206 else goto L207;
// 	L206:
// 		$TMIt127 =& $this->mIsPreview;
// 		goto L208;
// 	L207:
// 		$TMIt127 = $this->mIsPreview;
// 		goto L208;
// 	L208:
// 		$TLE58 = wfsetvar($TMIt127, $x);
// 		return $TLE58;
// 	}
// 	function setissectionpreview($x)
// 	{
// 		$TLE130 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE130) goto L209 else goto L210;
// 	L209:
// 		$TMIt129 =& $this->mIsSectionPreview;
// 		goto L211;
// 	L210:
// 		$TMIt129 = $this->mIsSectionPreview;
// 		goto L211;
// 	L211:
// 		$TLE59 = wfsetvar($TMIt129, $x);
// 		return $TLE59;
// 	}
// 	function __construct($user = NULL)
// 	{
// 		$this->initialisefromuser($user);
// 	}
// 	static function newfromuser($user)
// 	{
// 		$TLE60 = new parseroptions($user);
// 		return $TLE60;
// 	}
// 	function initialisefromuser($userInput)
// 	{
// 		global $wgUseTeX;
// 		global $wgUseDynamicDates;
// 		global $wgInterwikiMagic;
// 		global $wgAllowExternalImages;
// 		global $wgAllowExternalImagesFrom;
// 		global $wgEnableImageWhitelist;
// 		global $wgAllowSpecialInclusion;
// 		global $wgMaxArticleSize;
// 		global $wgMaxPPNodeCount;
// 		global $wgMaxTemplateDepth;
// 		global $wgMaxPPExpandDepth;
// 		global $wgCleanSignatures;
// 		global $wgExternalLinkTarget;
// 		$fname = 'ParserOptions::initialiseFromUser';
// 		wfprofilein($fname);
// 		$TLE61 = !$userInput;
// 		if (TLE61) goto L215 else goto L216;
// 	L215:
// 		global $wgUser;
// 		$TLE62 = isset($wgUser);
// 		if (TLE62) goto L212 else goto L213;
// 	L212:
// 		$user = $wgUser;
// 		goto L214;
// 	L213:
// 		$user = new user();
// 		goto L214;
// 	L214:
// 		goto L217;
// 	L216:
// 		$user =& $userInput;
// 		goto L217;
// 	L217:
// 		$this->mUser = $user;
// 		$this->mUseTeX = $wgUseTeX;
// 		$this->mUseDynamicDates = $wgUseDynamicDates;
// 		$this->mInterwikiMagic = $wgInterwikiMagic;
// 		$this->mAllowExternalImages = $wgAllowExternalImages;
// 		$this->mAllowExternalImagesFrom = $wgAllowExternalImagesFrom;
// 		$this->mEnableImageWhitelist = $wgEnableImageWhitelist;
// 		$TLE63 = NULL;
// 		$this->mSkin = $TLE63;
// 		$TLE64 = NULL;
// 		$this->mDateFormat = $TLE64;
// 		$TLE65 = True;
// 		$this->mEditSection = $TLE65;
// 		$TLE66 = 'numberheadings';
// 		$TLE67 = $user->getoption($TLE66);
// 		$this->mNumberHeadings = $TLE67;
// 		$this->mAllowSpecialInclusion = $wgAllowSpecialInclusion;
// 		$TLE68 = False;
// 		$this->mTidy = $TLE68;
// 		$TLE69 = False;
// 		$this->mInterfaceMessage = $TLE69;
// 		$TLE70 = NULL;
// 		$this->mTargetLanguage = $TLE70;
// 		$TLE71 = 1024;
// 		$TLE72 = ($wgMaxArticleSize * $TLE71);
// 		$this->mMaxIncludeSize = $TLE72;
// 		$this->mMaxPPNodeCount = $wgMaxPPNodeCount;
// 		$this->mMaxPPExpandDepth = $wgMaxPPExpandDepth;
// 		$this->mMaxTemplateDepth = $wgMaxTemplateDepth;
// 		$TLE73 = True;
// 		$this->mRemoveComments = $TLE73;
// 		$TLE74 = 'Parser';
// 		$TLE75 = 'statelessFetchTemplate';
// 		unset($TSa76);
// 		$TSa76 = (array) $TSa76;
// 		$TSa76[] = $TLE74;
// 		$TSa76[] = $TLE75;
// 		$this->mTemplateCallback = $TSa76;
// 		$TLE77 = False;
// 		$this->mEnableLimitReport = $TLE77;
// 		$this->mCleanSignatures = $wgCleanSignatures;
// 		$this->mExternalLinkTarget = $wgExternalLinkTarget;
// 		$TLE78 = False;
// 		$this->mIsPreview = $TLE78;
// 		$TLE79 = False;
// 		$this->mIsSectionPreview = $TLE79;
// 		wfprofileout($fname);
// 	}
// }
// function getusetex()
// {
// 	$TSt0 = $this->mUseTeX;
// 	return $TSt0;
// }
PHP_METHOD(ParserOptions, getusetex)
{
zval* local_TSt0 = NULL;
zval* local_this = getThis();
// Function body
// $TSt0 = $this->mUseTeX;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUseTeX", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt0 == NULL)
    {
      local_TSt0 = EG (uninitialized_zval_ptr);
      local_TSt0->refcount++;
    }
  zval** p_lhs = &local_TSt0;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt0;
{
     zval* rhs;
  if (local_TSt0 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt0;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt0 != NULL)
{
zval_ptr_dtor (&local_TSt0);
}
}
// function getusedynamicdates()
// {
// 	$TSt1 = $this->mUseDynamicDates;
// 	return $TSt1;
// }
PHP_METHOD(ParserOptions, getusedynamicdates)
{
zval* local_TSt1 = NULL;
zval* local_this = getThis();
// Function body
// $TSt1 = $this->mUseDynamicDates;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUseDynamicDates", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt1 == NULL)
    {
      local_TSt1 = EG (uninitialized_zval_ptr);
      local_TSt1->refcount++;
    }
  zval** p_lhs = &local_TSt1;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt1;
{
     zval* rhs;
  if (local_TSt1 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt1;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt1 != NULL)
{
zval_ptr_dtor (&local_TSt1);
}
}
// function getinterwikimagic()
// {
// 	$TSt2 = $this->mInterwikiMagic;
// 	return $TSt2;
// }
PHP_METHOD(ParserOptions, getinterwikimagic)
{
zval* local_TSt2 = NULL;
zval* local_this = getThis();
// Function body
// $TSt2 = $this->mInterwikiMagic;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mInterwikiMagic", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt2 == NULL)
    {
      local_TSt2 = EG (uninitialized_zval_ptr);
      local_TSt2->refcount++;
    }
  zval** p_lhs = &local_TSt2;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt2;
{
     zval* rhs;
  if (local_TSt2 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt2;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt2 != NULL)
{
zval_ptr_dtor (&local_TSt2);
}
}
// function getallowexternalimages()
// {
// 	$TSt3 = $this->mAllowExternalImages;
// 	return $TSt3;
// }
PHP_METHOD(ParserOptions, getallowexternalimages)
{
zval* local_TSt3 = NULL;
zval* local_this = getThis();
// Function body
// $TSt3 = $this->mAllowExternalImages;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowExternalImages", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt3 == NULL)
    {
      local_TSt3 = EG (uninitialized_zval_ptr);
      local_TSt3->refcount++;
    }
  zval** p_lhs = &local_TSt3;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt3;
{
     zval* rhs;
  if (local_TSt3 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt3;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt3 != NULL)
{
zval_ptr_dtor (&local_TSt3);
}
}
// function getallowexternalimagesfrom()
// {
// 	$TSt4 = $this->mAllowExternalImagesFrom;
// 	return $TSt4;
// }
PHP_METHOD(ParserOptions, getallowexternalimagesfrom)
{
zval* local_TSt4 = NULL;
zval* local_this = getThis();
// Function body
// $TSt4 = $this->mAllowExternalImagesFrom;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowExternalImagesFrom", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt4 == NULL)
    {
      local_TSt4 = EG (uninitialized_zval_ptr);
      local_TSt4->refcount++;
    }
  zval** p_lhs = &local_TSt4;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt4;
{
     zval* rhs;
  if (local_TSt4 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt4;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt4 != NULL)
{
zval_ptr_dtor (&local_TSt4);
}
}
// function getenableimagewhitelist()
// {
// 	$TSt5 = $this->mEnableImageWhitelist;
// 	return $TSt5;
// }
PHP_METHOD(ParserOptions, getenableimagewhitelist)
{
zval* local_TSt5 = NULL;
zval* local_this = getThis();
// Function body
// $TSt5 = $this->mEnableImageWhitelist;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEnableImageWhitelist", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt5 == NULL)
    {
      local_TSt5 = EG (uninitialized_zval_ptr);
      local_TSt5->refcount++;
    }
  zval** p_lhs = &local_TSt5;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt5;
{
     zval* rhs;
  if (local_TSt5 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt5;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt5 != NULL)
{
zval_ptr_dtor (&local_TSt5);
}
}
// function geteditsection()
// {
// 	$TSt6 = $this->mEditSection;
// 	return $TSt6;
// }
PHP_METHOD(ParserOptions, geteditsection)
{
zval* local_TSt6 = NULL;
zval* local_this = getThis();
// Function body
// $TSt6 = $this->mEditSection;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEditSection", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt6 == NULL)
    {
      local_TSt6 = EG (uninitialized_zval_ptr);
      local_TSt6->refcount++;
    }
  zval** p_lhs = &local_TSt6;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt6;
{
     zval* rhs;
  if (local_TSt6 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt6;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt6 != NULL)
{
zval_ptr_dtor (&local_TSt6);
}
}
// function getnumberheadings()
// {
// 	$TSt7 = $this->mNumberHeadings;
// 	return $TSt7;
// }
PHP_METHOD(ParserOptions, getnumberheadings)
{
zval* local_TSt7 = NULL;
zval* local_this = getThis();
// Function body
// $TSt7 = $this->mNumberHeadings;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mNumberHeadings", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt7 == NULL)
    {
      local_TSt7 = EG (uninitialized_zval_ptr);
      local_TSt7->refcount++;
    }
  zval** p_lhs = &local_TSt7;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt7;
{
     zval* rhs;
  if (local_TSt7 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt7;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt7 != NULL)
{
zval_ptr_dtor (&local_TSt7);
}
}
// function getallowspecialinclusion()
// {
// 	$TSt8 = $this->mAllowSpecialInclusion;
// 	return $TSt8;
// }
PHP_METHOD(ParserOptions, getallowspecialinclusion)
{
zval* local_TSt8 = NULL;
zval* local_this = getThis();
// Function body
// $TSt8 = $this->mAllowSpecialInclusion;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowSpecialInclusion", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt8 == NULL)
    {
      local_TSt8 = EG (uninitialized_zval_ptr);
      local_TSt8->refcount++;
    }
  zval** p_lhs = &local_TSt8;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt8;
{
     zval* rhs;
  if (local_TSt8 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt8;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt8 != NULL)
{
zval_ptr_dtor (&local_TSt8);
}
}
// function gettidy()
// {
// 	$TSt9 = $this->mTidy;
// 	return $TSt9;
// }
PHP_METHOD(ParserOptions, gettidy)
{
zval* local_TSt9 = NULL;
zval* local_this = getThis();
// Function body
// $TSt9 = $this->mTidy;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTidy", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt9 == NULL)
    {
      local_TSt9 = EG (uninitialized_zval_ptr);
      local_TSt9->refcount++;
    }
  zval** p_lhs = &local_TSt9;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt9;
{
     zval* rhs;
  if (local_TSt9 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt9;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt9 != NULL)
{
zval_ptr_dtor (&local_TSt9);
}
}
// function getinterfacemessage()
// {
// 	$TSt10 = $this->mInterfaceMessage;
// 	return $TSt10;
// }
PHP_METHOD(ParserOptions, getinterfacemessage)
{
zval* local_TSt10 = NULL;
zval* local_this = getThis();
// Function body
// $TSt10 = $this->mInterfaceMessage;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mInterfaceMessage", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt10 == NULL)
    {
      local_TSt10 = EG (uninitialized_zval_ptr);
      local_TSt10->refcount++;
    }
  zval** p_lhs = &local_TSt10;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt10;
{
     zval* rhs;
  if (local_TSt10 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt10;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt10 != NULL)
{
zval_ptr_dtor (&local_TSt10);
}
}
// function gettargetlanguage()
// {
// 	$TSt11 = $this->mTargetLanguage;
// 	return $TSt11;
// }
PHP_METHOD(ParserOptions, gettargetlanguage)
{
zval* local_TSt11 = NULL;
zval* local_this = getThis();
// Function body
// $TSt11 = $this->mTargetLanguage;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTargetLanguage", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt11 == NULL)
    {
      local_TSt11 = EG (uninitialized_zval_ptr);
      local_TSt11->refcount++;
    }
  zval** p_lhs = &local_TSt11;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt11;
{
     zval* rhs;
  if (local_TSt11 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt11;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt11 != NULL)
{
zval_ptr_dtor (&local_TSt11);
}
}
// function getmaxincludesize()
// {
// 	$TSt12 = $this->mMaxIncludeSize;
// 	return $TSt12;
// }
PHP_METHOD(ParserOptions, getmaxincludesize)
{
zval* local_TSt12 = NULL;
zval* local_this = getThis();
// Function body
// $TSt12 = $this->mMaxIncludeSize;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxIncludeSize", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt12 == NULL)
    {
      local_TSt12 = EG (uninitialized_zval_ptr);
      local_TSt12->refcount++;
    }
  zval** p_lhs = &local_TSt12;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt12;
{
     zval* rhs;
  if (local_TSt12 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt12;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt12 != NULL)
{
zval_ptr_dtor (&local_TSt12);
}
}
// function getmaxppnodecount()
// {
// 	$TSt13 = $this->mMaxPPNodeCount;
// 	return $TSt13;
// }
PHP_METHOD(ParserOptions, getmaxppnodecount)
{
zval* local_TSt13 = NULL;
zval* local_this = getThis();
// Function body
// $TSt13 = $this->mMaxPPNodeCount;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxPPNodeCount", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt13 == NULL)
    {
      local_TSt13 = EG (uninitialized_zval_ptr);
      local_TSt13->refcount++;
    }
  zval** p_lhs = &local_TSt13;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt13;
{
     zval* rhs;
  if (local_TSt13 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt13;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt13 != NULL)
{
zval_ptr_dtor (&local_TSt13);
}
}
// function getmaxtemplatedepth()
// {
// 	$TSt14 = $this->mMaxTemplateDepth;
// 	return $TSt14;
// }
PHP_METHOD(ParserOptions, getmaxtemplatedepth)
{
zval* local_TSt14 = NULL;
zval* local_this = getThis();
// Function body
// $TSt14 = $this->mMaxTemplateDepth;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxTemplateDepth", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt14 == NULL)
    {
      local_TSt14 = EG (uninitialized_zval_ptr);
      local_TSt14->refcount++;
    }
  zval** p_lhs = &local_TSt14;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt14;
{
     zval* rhs;
  if (local_TSt14 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt14;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt14 != NULL)
{
zval_ptr_dtor (&local_TSt14);
}
}
// function getremovecomments()
// {
// 	$TSt15 = $this->mRemoveComments;
// 	return $TSt15;
// }
PHP_METHOD(ParserOptions, getremovecomments)
{
zval* local_TSt15 = NULL;
zval* local_this = getThis();
// Function body
// $TSt15 = $this->mRemoveComments;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mRemoveComments", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt15 == NULL)
    {
      local_TSt15 = EG (uninitialized_zval_ptr);
      local_TSt15->refcount++;
    }
  zval** p_lhs = &local_TSt15;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt15;
{
     zval* rhs;
  if (local_TSt15 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt15;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt15 != NULL)
{
zval_ptr_dtor (&local_TSt15);
}
}
// function gettemplatecallback()
// {
// 	$TSt16 = $this->mTemplateCallback;
// 	return $TSt16;
// }
PHP_METHOD(ParserOptions, gettemplatecallback)
{
zval* local_TSt16 = NULL;
zval* local_this = getThis();
// Function body
// $TSt16 = $this->mTemplateCallback;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplateCallback", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt16 == NULL)
    {
      local_TSt16 = EG (uninitialized_zval_ptr);
      local_TSt16->refcount++;
    }
  zval** p_lhs = &local_TSt16;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt16;
{
     zval* rhs;
  if (local_TSt16 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt16;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt16 != NULL)
{
zval_ptr_dtor (&local_TSt16);
}
}
// function getenablelimitreport()
// {
// 	$TSt17 = $this->mEnableLimitReport;
// 	return $TSt17;
// }
PHP_METHOD(ParserOptions, getenablelimitreport)
{
zval* local_TSt17 = NULL;
zval* local_this = getThis();
// Function body
// $TSt17 = $this->mEnableLimitReport;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEnableLimitReport", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt17 == NULL)
    {
      local_TSt17 = EG (uninitialized_zval_ptr);
      local_TSt17->refcount++;
    }
  zval** p_lhs = &local_TSt17;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt17;
{
     zval* rhs;
  if (local_TSt17 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt17;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt17 != NULL)
{
zval_ptr_dtor (&local_TSt17);
}
}
// function getcleansignatures()
// {
// 	$TSt18 = $this->mCleanSignatures;
// 	return $TSt18;
// }
PHP_METHOD(ParserOptions, getcleansignatures)
{
zval* local_TSt18 = NULL;
zval* local_this = getThis();
// Function body
// $TSt18 = $this->mCleanSignatures;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCleanSignatures", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt18 == NULL)
    {
      local_TSt18 = EG (uninitialized_zval_ptr);
      local_TSt18->refcount++;
    }
  zval** p_lhs = &local_TSt18;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt18;
{
     zval* rhs;
  if (local_TSt18 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt18;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt18 != NULL)
{
zval_ptr_dtor (&local_TSt18);
}
}
// function getexternallinktarget()
// {
// 	$TSt19 = $this->mExternalLinkTarget;
// 	return $TSt19;
// }
PHP_METHOD(ParserOptions, getexternallinktarget)
{
zval* local_TSt19 = NULL;
zval* local_this = getThis();
// Function body
// $TSt19 = $this->mExternalLinkTarget;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mExternalLinkTarget", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt19 == NULL)
    {
      local_TSt19 = EG (uninitialized_zval_ptr);
      local_TSt19->refcount++;
    }
  zval** p_lhs = &local_TSt19;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt19;
{
     zval* rhs;
  if (local_TSt19 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt19;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt19 != NULL)
{
zval_ptr_dtor (&local_TSt19);
}
}
// function getispreview()
// {
// 	$TSt20 = $this->mIsPreview;
// 	return $TSt20;
// }
PHP_METHOD(ParserOptions, getispreview)
{
zval* local_TSt20 = NULL;
zval* local_this = getThis();
// Function body
// $TSt20 = $this->mIsPreview;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIsPreview", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt20 == NULL)
    {
      local_TSt20 = EG (uninitialized_zval_ptr);
      local_TSt20->refcount++;
    }
  zval** p_lhs = &local_TSt20;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt20;
{
     zval* rhs;
  if (local_TSt20 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt20;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt20 != NULL)
{
zval_ptr_dtor (&local_TSt20);
}
}
// function getissectionpreview()
// {
// 	$TSt21 = $this->mIsSectionPreview;
// 	return $TSt21;
// }
PHP_METHOD(ParserOptions, getissectionpreview)
{
zval* local_TSt21 = NULL;
zval* local_this = getThis();
// Function body
// $TSt21 = $this->mIsSectionPreview;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIsSectionPreview", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt21 == NULL)
    {
      local_TSt21 = EG (uninitialized_zval_ptr);
      local_TSt21->refcount++;
    }
  zval** p_lhs = &local_TSt21;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt21;
{
     zval* rhs;
  if (local_TSt21 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt21;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt21 != NULL)
{
zval_ptr_dtor (&local_TSt21);
}
}
// function getskin()
// {
// 	$TMIt80 = $this->mSkin;
// 	$TLE22 = isset($TMIt80);
// 	$TLE23 = !$TLE22;
// 	if (TLE23) goto L131 else goto L132;
// L131:
// 	$TSt24 = $this->mUser;
// 	$TLE25 = $TSt24->getskin();
// 	$this->mSkin = $TLE25;
// 	goto L133;
// L132:
// 	goto L133;
// L133:
// 	$TSt26 = $this->mSkin;
// 	return $TSt26;
// }
PHP_METHOD(ParserOptions, getskin)
{
zval* local_TLE22 = NULL;
zval* local_TLE23 = NULL;
zval* local_TLE25 = NULL;
zval* local_TMIt80 = NULL;
zval* local_TSt24 = NULL;
zval* local_TSt26 = NULL;
zval* local_this = getThis();
// Function body
// $TMIt80 = $this->mSkin;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mSkin", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt80 == NULL)
    {
      local_TMIt80 = EG (uninitialized_zval_ptr);
      local_TMIt80->refcount++;
    }
  zval** p_lhs = &local_TMIt80;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE22 = isset($TMIt80);
{
if (local_TLE22 == NULL)
{
local_TLE22 = EG (uninitialized_zval_ptr);
local_TLE22->refcount++;
}
zval** p_lhs = &local_TLE22;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
ZVAL_BOOL(value, local_TMIt80 != NULL && !ZVAL_IS_NULL(local_TMIt80));
phc_check_invariants (TSRMLS_C);
}
// $TLE23 = !$TLE22;
{
     if (local_TLE23 == NULL)
    {
      local_TLE23 = EG (uninitialized_zval_ptr);
      local_TLE23->refcount++;
    }
  zval** p_lhs = &local_TLE23;

     zval* rhs;
  if (local_TLE22 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE22;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE23) goto L131 else goto L132;
{
     zval* p_cond;
  if (local_TLE23 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE23;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L131;
   else
      goto L132;
phc_check_invariants (TSRMLS_C);
}
// L131:
L131:;
// $TSt24 = $this->mUser;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUser", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt24 == NULL)
    {
      local_TSt24 = EG (uninitialized_zval_ptr);
      local_TSt24->refcount++;
    }
  zval** p_lhs = &local_TSt24;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE25 = $TSt24->getskin();
{
     if (local_TSt24 == NULL)
    {
      local_TSt24 = EG (uninitialized_zval_ptr);
      local_TSt24->refcount++;
    }
  zval** p_obj = &local_TSt24;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getskin", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 64 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 64, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE25 == NULL)
    {
      local_TLE25 = EG (uninitialized_zval_ptr);
      local_TLE25->refcount++;
    }
  zval** p_lhs = &local_TLE25;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $this->mSkin = $TLE25;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE25 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE25;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mSkin", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// goto L133;
{
goto L133;
phc_check_invariants (TSRMLS_C);
}
// L132:
L132:;
// goto L133;
{
goto L133;
phc_check_invariants (TSRMLS_C);
}
// L133:
L133:;
// $TSt26 = $this->mSkin;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mSkin", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt26 == NULL)
    {
      local_TSt26 = EG (uninitialized_zval_ptr);
      local_TSt26->refcount++;
    }
  zval** p_lhs = &local_TSt26;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt26;
{
     zval* rhs;
  if (local_TSt26 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt26;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE22 != NULL)
{
zval_ptr_dtor (&local_TLE22);
}
if (local_TLE23 != NULL)
{
zval_ptr_dtor (&local_TLE23);
}
if (local_TLE25 != NULL)
{
zval_ptr_dtor (&local_TLE25);
}
if (local_TMIt80 != NULL)
{
zval_ptr_dtor (&local_TMIt80);
}
if (local_TSt24 != NULL)
{
zval_ptr_dtor (&local_TSt24);
}
if (local_TSt26 != NULL)
{
zval_ptr_dtor (&local_TSt26);
}
}
// function getdateformat()
// {
// 	$TMIt81 = $this->mDateFormat;
// 	$TLE27 = isset($TMIt81);
// 	$TLE28 = !$TLE27;
// 	if (TLE28) goto L134 else goto L135;
// L134:
// 	$TSt29 = $this->mUser;
// 	$TLE30 = $TSt29->getdatepreference();
// 	$this->mDateFormat = $TLE30;
// 	goto L136;
// L135:
// 	goto L136;
// L136:
// 	$TSt31 = $this->mDateFormat;
// 	return $TSt31;
// }
PHP_METHOD(ParserOptions, getdateformat)
{
zval* local_TLE27 = NULL;
zval* local_TLE28 = NULL;
zval* local_TLE30 = NULL;
zval* local_TMIt81 = NULL;
zval* local_TSt29 = NULL;
zval* local_TSt31 = NULL;
zval* local_this = getThis();
// Function body
// $TMIt81 = $this->mDateFormat;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mDateFormat", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt81 == NULL)
    {
      local_TMIt81 = EG (uninitialized_zval_ptr);
      local_TMIt81->refcount++;
    }
  zval** p_lhs = &local_TMIt81;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE27 = isset($TMIt81);
{
if (local_TLE27 == NULL)
{
local_TLE27 = EG (uninitialized_zval_ptr);
local_TLE27->refcount++;
}
zval** p_lhs = &local_TLE27;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
ZVAL_BOOL(value, local_TMIt81 != NULL && !ZVAL_IS_NULL(local_TMIt81));
phc_check_invariants (TSRMLS_C);
}
// $TLE28 = !$TLE27;
{
     if (local_TLE28 == NULL)
    {
      local_TLE28 = EG (uninitialized_zval_ptr);
      local_TLE28->refcount++;
    }
  zval** p_lhs = &local_TLE28;

     zval* rhs;
  if (local_TLE27 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE27;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE28) goto L134 else goto L135;
{
     zval* p_cond;
  if (local_TLE28 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE28;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L134;
   else
      goto L135;
phc_check_invariants (TSRMLS_C);
}
// L134:
L134:;
// $TSt29 = $this->mUser;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUser", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt29 == NULL)
    {
      local_TSt29 = EG (uninitialized_zval_ptr);
      local_TSt29->refcount++;
    }
  zval** p_lhs = &local_TSt29;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE30 = $TSt29->getdatepreference();
{
     if (local_TSt29 == NULL)
    {
      local_TSt29 = EG (uninitialized_zval_ptr);
      local_TSt29->refcount++;
    }
  zval** p_obj = &local_TSt29;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getdatepreference", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 71 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 71, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE30 == NULL)
    {
      local_TLE30 = EG (uninitialized_zval_ptr);
      local_TLE30->refcount++;
    }
  zval** p_lhs = &local_TLE30;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $this->mDateFormat = $TLE30;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE30 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE30;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mDateFormat", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// goto L136;
{
goto L136;
phc_check_invariants (TSRMLS_C);
}
// L135:
L135:;
// goto L136;
{
goto L136;
phc_check_invariants (TSRMLS_C);
}
// L136:
L136:;
// $TSt31 = $this->mDateFormat;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mDateFormat", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt31 == NULL)
    {
      local_TSt31 = EG (uninitialized_zval_ptr);
      local_TSt31->refcount++;
    }
  zval** p_lhs = &local_TSt31;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt31;
{
     zval* rhs;
  if (local_TSt31 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt31;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE27 != NULL)
{
zval_ptr_dtor (&local_TLE27);
}
if (local_TLE28 != NULL)
{
zval_ptr_dtor (&local_TLE28);
}
if (local_TLE30 != NULL)
{
zval_ptr_dtor (&local_TLE30);
}
if (local_TMIt81 != NULL)
{
zval_ptr_dtor (&local_TMIt81);
}
if (local_TSt29 != NULL)
{
zval_ptr_dtor (&local_TSt29);
}
if (local_TSt31 != NULL)
{
zval_ptr_dtor (&local_TSt31);
}
}
// function gettimestamp()
// {
// 	$TMIt82 = $this->mTimestamp;
// 	$TLE32 = isset($TMIt82);
// 	$TLE33 = !$TLE32;
// 	if (TLE33) goto L137 else goto L138;
// L137:
// 	$TLE34 = wftimestampnow();
// 	$this->mTimestamp = $TLE34;
// 	goto L139;
// L138:
// 	goto L139;
// L139:
// 	$TSt35 = $this->mTimestamp;
// 	return $TSt35;
// }
PHP_METHOD(ParserOptions, gettimestamp)
{
zval* local_TLE32 = NULL;
zval* local_TLE33 = NULL;
zval* local_TLE34 = NULL;
zval* local_TMIt82 = NULL;
zval* local_TSt35 = NULL;
zval* local_this = getThis();
// Function body
// $TMIt82 = $this->mTimestamp;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTimestamp", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt82 == NULL)
    {
      local_TMIt82 = EG (uninitialized_zval_ptr);
      local_TMIt82->refcount++;
    }
  zval** p_lhs = &local_TMIt82;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE32 = isset($TMIt82);
{
if (local_TLE32 == NULL)
{
local_TLE32 = EG (uninitialized_zval_ptr);
local_TLE32->refcount++;
}
zval** p_lhs = &local_TLE32;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
ZVAL_BOOL(value, local_TMIt82 != NULL && !ZVAL_IS_NULL(local_TMIt82));
phc_check_invariants (TSRMLS_C);
}
// $TLE33 = !$TLE32;
{
     if (local_TLE33 == NULL)
    {
      local_TLE33 = EG (uninitialized_zval_ptr);
      local_TLE33->refcount++;
    }
  zval** p_lhs = &local_TLE33;

     zval* rhs;
  if (local_TLE32 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE32;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE33) goto L137 else goto L138;
{
     zval* p_cond;
  if (local_TLE33 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE33;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L137;
   else
      goto L138;
phc_check_invariants (TSRMLS_C);
}
// L137:
L137:;
// $TLE34 = wftimestampnow();
{
   initialize_function_call (&wftimestampnow_fci, &wftimestampnow_fcic, "wftimestampnow", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 78 TSRMLS_CC);
      zend_function* signature = wftimestampnow_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 78, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wftimestampnow_fci.param_count;
   zval*** params_save = wftimestampnow_fci.params;
   zval** retval_save = wftimestampnow_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wftimestampnow_fci.params = args_ind;
   wftimestampnow_fci.param_count = 0;
   wftimestampnow_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wftimestampnow_fci, &wftimestampnow_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wftimestampnow_fci.params = params_save;
   wftimestampnow_fci.param_count = param_count_save;
   wftimestampnow_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE34 == NULL)
    {
      local_TLE34 = EG (uninitialized_zval_ptr);
      local_TLE34->refcount++;
    }
  zval** p_lhs = &local_TLE34;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $this->mTimestamp = $TLE34;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE34 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE34;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTimestamp", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// goto L139;
{
goto L139;
phc_check_invariants (TSRMLS_C);
}
// L138:
L138:;
// goto L139;
{
goto L139;
phc_check_invariants (TSRMLS_C);
}
// L139:
L139:;
// $TSt35 = $this->mTimestamp;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTimestamp", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt35 == NULL)
    {
      local_TSt35 = EG (uninitialized_zval_ptr);
      local_TSt35->refcount++;
    }
  zval** p_lhs = &local_TSt35;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt35;
{
     zval* rhs;
  if (local_TSt35 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt35;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE32 != NULL)
{
zval_ptr_dtor (&local_TLE32);
}
if (local_TLE33 != NULL)
{
zval_ptr_dtor (&local_TLE33);
}
if (local_TLE34 != NULL)
{
zval_ptr_dtor (&local_TLE34);
}
if (local_TMIt82 != NULL)
{
zval_ptr_dtor (&local_TMIt82);
}
if (local_TSt35 != NULL)
{
zval_ptr_dtor (&local_TSt35);
}
}
// function setusetex($x)
// {
// 	$TLE84 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE84) goto L140 else goto L141;
// L140:
// 	$TMIt83 =& $this->mUseTeX;
// 	goto L142;
// L141:
// 	$TMIt83 = $this->mUseTeX;
// 	goto L142;
// L142:
// 	$TLE36 = wfsetvar($TMIt83, $x);
// 	return $TLE36;
// }
PHP_METHOD(ParserOptions, setusetex)
{
zval* local_TLE36 = NULL;
zval* local_TLE84 = NULL;
zval* local_TMIt83 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE84 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE84 == NULL)
    {
      local_TLE84 = EG (uninitialized_zval_ptr);
      local_TLE84->refcount++;
    }
  zval** p_lhs = &local_TLE84;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE84) goto L140 else goto L141;
{
     zval* p_cond;
  if (local_TLE84 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE84;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L140;
   else
      goto L141;
phc_check_invariants (TSRMLS_C);
}
// L140:
L140:;
// $TMIt83 =& $this->mUseTeX;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUseTeX", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt83 == NULL)
    {
      local_TMIt83 = EG (uninitialized_zval_ptr);
      local_TMIt83->refcount++;
    }
  zval** p_lhs = &local_TMIt83;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L142;
{
goto L142;
phc_check_invariants (TSRMLS_C);
}
// L141:
L141:;
// $TMIt83 = $this->mUseTeX;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUseTeX", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt83 == NULL)
    {
      local_TMIt83 = EG (uninitialized_zval_ptr);
      local_TMIt83->refcount++;
    }
  zval** p_lhs = &local_TMIt83;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L142;
{
goto L142;
phc_check_invariants (TSRMLS_C);
}
// L142:
L142:;
// $TLE36 = wfsetvar($TMIt83, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 83 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt83 == NULL)
    {
      local_TMIt83 = EG (uninitialized_zval_ptr);
      local_TMIt83->refcount++;
    }
  zval** p_arg = &local_TMIt83;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt83 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt83;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 83, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE36 == NULL)
    {
      local_TLE36 = EG (uninitialized_zval_ptr);
      local_TLE36->refcount++;
    }
  zval** p_lhs = &local_TLE36;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE36;
{
     zval* rhs;
  if (local_TLE36 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE36;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE36 != NULL)
{
zval_ptr_dtor (&local_TLE36);
}
if (local_TLE84 != NULL)
{
zval_ptr_dtor (&local_TLE84);
}
if (local_TMIt83 != NULL)
{
zval_ptr_dtor (&local_TMIt83);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setusedynamicdates($x)
// {
// 	$TLE86 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE86) goto L143 else goto L144;
// L143:
// 	$TMIt85 =& $this->mUseDynamicDates;
// 	goto L145;
// L144:
// 	$TMIt85 = $this->mUseDynamicDates;
// 	goto L145;
// L145:
// 	$TLE37 = wfsetvar($TMIt85, $x);
// 	return $TLE37;
// }
PHP_METHOD(ParserOptions, setusedynamicdates)
{
zval* local_TLE37 = NULL;
zval* local_TLE86 = NULL;
zval* local_TMIt85 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE86 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE86 == NULL)
    {
      local_TLE86 = EG (uninitialized_zval_ptr);
      local_TLE86->refcount++;
    }
  zval** p_lhs = &local_TLE86;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE86) goto L143 else goto L144;
{
     zval* p_cond;
  if (local_TLE86 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE86;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L143;
   else
      goto L144;
phc_check_invariants (TSRMLS_C);
}
// L143:
L143:;
// $TMIt85 =& $this->mUseDynamicDates;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUseDynamicDates", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt85 == NULL)
    {
      local_TMIt85 = EG (uninitialized_zval_ptr);
      local_TMIt85->refcount++;
    }
  zval** p_lhs = &local_TMIt85;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L145;
{
goto L145;
phc_check_invariants (TSRMLS_C);
}
// L144:
L144:;
// $TMIt85 = $this->mUseDynamicDates;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUseDynamicDates", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt85 == NULL)
    {
      local_TMIt85 = EG (uninitialized_zval_ptr);
      local_TMIt85->refcount++;
    }
  zval** p_lhs = &local_TMIt85;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L145;
{
goto L145;
phc_check_invariants (TSRMLS_C);
}
// L145:
L145:;
// $TLE37 = wfsetvar($TMIt85, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 84 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt85 == NULL)
    {
      local_TMIt85 = EG (uninitialized_zval_ptr);
      local_TMIt85->refcount++;
    }
  zval** p_arg = &local_TMIt85;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt85 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt85;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 84, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE37 == NULL)
    {
      local_TLE37 = EG (uninitialized_zval_ptr);
      local_TLE37->refcount++;
    }
  zval** p_lhs = &local_TLE37;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE37;
{
     zval* rhs;
  if (local_TLE37 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE37;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE37 != NULL)
{
zval_ptr_dtor (&local_TLE37);
}
if (local_TLE86 != NULL)
{
zval_ptr_dtor (&local_TLE86);
}
if (local_TMIt85 != NULL)
{
zval_ptr_dtor (&local_TMIt85);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setinterwikimagic($x)
// {
// 	$TLE88 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE88) goto L146 else goto L147;
// L146:
// 	$TMIt87 =& $this->mInterwikiMagic;
// 	goto L148;
// L147:
// 	$TMIt87 = $this->mInterwikiMagic;
// 	goto L148;
// L148:
// 	$TLE38 = wfsetvar($TMIt87, $x);
// 	return $TLE38;
// }
PHP_METHOD(ParserOptions, setinterwikimagic)
{
zval* local_TLE38 = NULL;
zval* local_TLE88 = NULL;
zval* local_TMIt87 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE88 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE88 == NULL)
    {
      local_TLE88 = EG (uninitialized_zval_ptr);
      local_TLE88->refcount++;
    }
  zval** p_lhs = &local_TLE88;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE88) goto L146 else goto L147;
{
     zval* p_cond;
  if (local_TLE88 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE88;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L146;
   else
      goto L147;
phc_check_invariants (TSRMLS_C);
}
// L146:
L146:;
// $TMIt87 =& $this->mInterwikiMagic;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mInterwikiMagic", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt87 == NULL)
    {
      local_TMIt87 = EG (uninitialized_zval_ptr);
      local_TMIt87->refcount++;
    }
  zval** p_lhs = &local_TMIt87;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L148;
{
goto L148;
phc_check_invariants (TSRMLS_C);
}
// L147:
L147:;
// $TMIt87 = $this->mInterwikiMagic;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mInterwikiMagic", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt87 == NULL)
    {
      local_TMIt87 = EG (uninitialized_zval_ptr);
      local_TMIt87->refcount++;
    }
  zval** p_lhs = &local_TMIt87;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L148;
{
goto L148;
phc_check_invariants (TSRMLS_C);
}
// L148:
L148:;
// $TLE38 = wfsetvar($TMIt87, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 85 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt87 == NULL)
    {
      local_TMIt87 = EG (uninitialized_zval_ptr);
      local_TMIt87->refcount++;
    }
  zval** p_arg = &local_TMIt87;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt87 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt87;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 85, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE38 == NULL)
    {
      local_TLE38 = EG (uninitialized_zval_ptr);
      local_TLE38->refcount++;
    }
  zval** p_lhs = &local_TLE38;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE38;
{
     zval* rhs;
  if (local_TLE38 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE38;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE38 != NULL)
{
zval_ptr_dtor (&local_TLE38);
}
if (local_TLE88 != NULL)
{
zval_ptr_dtor (&local_TLE88);
}
if (local_TMIt87 != NULL)
{
zval_ptr_dtor (&local_TMIt87);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setallowexternalimages($x)
// {
// 	$TLE90 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE90) goto L149 else goto L150;
// L149:
// 	$TMIt89 =& $this->mAllowExternalImages;
// 	goto L151;
// L150:
// 	$TMIt89 = $this->mAllowExternalImages;
// 	goto L151;
// L151:
// 	$TLE39 = wfsetvar($TMIt89, $x);
// 	return $TLE39;
// }
PHP_METHOD(ParserOptions, setallowexternalimages)
{
zval* local_TLE39 = NULL;
zval* local_TLE90 = NULL;
zval* local_TMIt89 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE90 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE90 == NULL)
    {
      local_TLE90 = EG (uninitialized_zval_ptr);
      local_TLE90->refcount++;
    }
  zval** p_lhs = &local_TLE90;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE90) goto L149 else goto L150;
{
     zval* p_cond;
  if (local_TLE90 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE90;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L149;
   else
      goto L150;
phc_check_invariants (TSRMLS_C);
}
// L149:
L149:;
// $TMIt89 =& $this->mAllowExternalImages;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowExternalImages", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt89 == NULL)
    {
      local_TMIt89 = EG (uninitialized_zval_ptr);
      local_TMIt89->refcount++;
    }
  zval** p_lhs = &local_TMIt89;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L151;
{
goto L151;
phc_check_invariants (TSRMLS_C);
}
// L150:
L150:;
// $TMIt89 = $this->mAllowExternalImages;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowExternalImages", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt89 == NULL)
    {
      local_TMIt89 = EG (uninitialized_zval_ptr);
      local_TMIt89->refcount++;
    }
  zval** p_lhs = &local_TMIt89;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L151;
{
goto L151;
phc_check_invariants (TSRMLS_C);
}
// L151:
L151:;
// $TLE39 = wfsetvar($TMIt89, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 86 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt89 == NULL)
    {
      local_TMIt89 = EG (uninitialized_zval_ptr);
      local_TMIt89->refcount++;
    }
  zval** p_arg = &local_TMIt89;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt89 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt89;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 86, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE39 == NULL)
    {
      local_TLE39 = EG (uninitialized_zval_ptr);
      local_TLE39->refcount++;
    }
  zval** p_lhs = &local_TLE39;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE39;
{
     zval* rhs;
  if (local_TLE39 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE39;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE39 != NULL)
{
zval_ptr_dtor (&local_TLE39);
}
if (local_TLE90 != NULL)
{
zval_ptr_dtor (&local_TLE90);
}
if (local_TMIt89 != NULL)
{
zval_ptr_dtor (&local_TMIt89);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setallowexternalimagesfrom($x)
// {
// 	$TLE92 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE92) goto L152 else goto L153;
// L152:
// 	$TMIt91 =& $this->mAllowExternalImagesFrom;
// 	goto L154;
// L153:
// 	$TMIt91 = $this->mAllowExternalImagesFrom;
// 	goto L154;
// L154:
// 	$TLE40 = wfsetvar($TMIt91, $x);
// 	return $TLE40;
// }
PHP_METHOD(ParserOptions, setallowexternalimagesfrom)
{
zval* local_TLE40 = NULL;
zval* local_TLE92 = NULL;
zval* local_TMIt91 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE92 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE92 == NULL)
    {
      local_TLE92 = EG (uninitialized_zval_ptr);
      local_TLE92->refcount++;
    }
  zval** p_lhs = &local_TLE92;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE92) goto L152 else goto L153;
{
     zval* p_cond;
  if (local_TLE92 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE92;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L152;
   else
      goto L153;
phc_check_invariants (TSRMLS_C);
}
// L152:
L152:;
// $TMIt91 =& $this->mAllowExternalImagesFrom;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowExternalImagesFrom", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt91 == NULL)
    {
      local_TMIt91 = EG (uninitialized_zval_ptr);
      local_TMIt91->refcount++;
    }
  zval** p_lhs = &local_TMIt91;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L154;
{
goto L154;
phc_check_invariants (TSRMLS_C);
}
// L153:
L153:;
// $TMIt91 = $this->mAllowExternalImagesFrom;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowExternalImagesFrom", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt91 == NULL)
    {
      local_TMIt91 = EG (uninitialized_zval_ptr);
      local_TMIt91->refcount++;
    }
  zval** p_lhs = &local_TMIt91;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L154;
{
goto L154;
phc_check_invariants (TSRMLS_C);
}
// L154:
L154:;
// $TLE40 = wfsetvar($TMIt91, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 87 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt91 == NULL)
    {
      local_TMIt91 = EG (uninitialized_zval_ptr);
      local_TMIt91->refcount++;
    }
  zval** p_arg = &local_TMIt91;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt91 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt91;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 87, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE40 == NULL)
    {
      local_TLE40 = EG (uninitialized_zval_ptr);
      local_TLE40->refcount++;
    }
  zval** p_lhs = &local_TLE40;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE40;
{
     zval* rhs;
  if (local_TLE40 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE40;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE40 != NULL)
{
zval_ptr_dtor (&local_TLE40);
}
if (local_TLE92 != NULL)
{
zval_ptr_dtor (&local_TLE92);
}
if (local_TMIt91 != NULL)
{
zval_ptr_dtor (&local_TMIt91);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setenableimagewhitelist($x)
// {
// 	$TLE94 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE94) goto L155 else goto L156;
// L155:
// 	$TMIt93 =& $this->mEnableImageWhitelist;
// 	goto L157;
// L156:
// 	$TMIt93 = $this->mEnableImageWhitelist;
// 	goto L157;
// L157:
// 	$TLE41 = wfsetvar($TMIt93, $x);
// 	return $TLE41;
// }
PHP_METHOD(ParserOptions, setenableimagewhitelist)
{
zval* local_TLE41 = NULL;
zval* local_TLE94 = NULL;
zval* local_TMIt93 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE94 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE94 == NULL)
    {
      local_TLE94 = EG (uninitialized_zval_ptr);
      local_TLE94->refcount++;
    }
  zval** p_lhs = &local_TLE94;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE94) goto L155 else goto L156;
{
     zval* p_cond;
  if (local_TLE94 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE94;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L155;
   else
      goto L156;
phc_check_invariants (TSRMLS_C);
}
// L155:
L155:;
// $TMIt93 =& $this->mEnableImageWhitelist;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEnableImageWhitelist", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt93 == NULL)
    {
      local_TMIt93 = EG (uninitialized_zval_ptr);
      local_TMIt93->refcount++;
    }
  zval** p_lhs = &local_TMIt93;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L157;
{
goto L157;
phc_check_invariants (TSRMLS_C);
}
// L156:
L156:;
// $TMIt93 = $this->mEnableImageWhitelist;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEnableImageWhitelist", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt93 == NULL)
    {
      local_TMIt93 = EG (uninitialized_zval_ptr);
      local_TMIt93->refcount++;
    }
  zval** p_lhs = &local_TMIt93;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L157;
{
goto L157;
phc_check_invariants (TSRMLS_C);
}
// L157:
L157:;
// $TLE41 = wfsetvar($TMIt93, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 88 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt93 == NULL)
    {
      local_TMIt93 = EG (uninitialized_zval_ptr);
      local_TMIt93->refcount++;
    }
  zval** p_arg = &local_TMIt93;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt93 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt93;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 88, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE41 == NULL)
    {
      local_TLE41 = EG (uninitialized_zval_ptr);
      local_TLE41->refcount++;
    }
  zval** p_lhs = &local_TLE41;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE41;
{
     zval* rhs;
  if (local_TLE41 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE41;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE41 != NULL)
{
zval_ptr_dtor (&local_TLE41);
}
if (local_TLE94 != NULL)
{
zval_ptr_dtor (&local_TLE94);
}
if (local_TMIt93 != NULL)
{
zval_ptr_dtor (&local_TMIt93);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setdateformat($x)
// {
// 	$TLE96 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE96) goto L158 else goto L159;
// L158:
// 	$TMIt95 =& $this->mDateFormat;
// 	goto L160;
// L159:
// 	$TMIt95 = $this->mDateFormat;
// 	goto L160;
// L160:
// 	$TLE42 = wfsetvar($TMIt95, $x);
// 	return $TLE42;
// }
PHP_METHOD(ParserOptions, setdateformat)
{
zval* local_TLE42 = NULL;
zval* local_TLE96 = NULL;
zval* local_TMIt95 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE96 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE96 == NULL)
    {
      local_TLE96 = EG (uninitialized_zval_ptr);
      local_TLE96->refcount++;
    }
  zval** p_lhs = &local_TLE96;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE96) goto L158 else goto L159;
{
     zval* p_cond;
  if (local_TLE96 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE96;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L158;
   else
      goto L159;
phc_check_invariants (TSRMLS_C);
}
// L158:
L158:;
// $TMIt95 =& $this->mDateFormat;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mDateFormat", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt95 == NULL)
    {
      local_TMIt95 = EG (uninitialized_zval_ptr);
      local_TMIt95->refcount++;
    }
  zval** p_lhs = &local_TMIt95;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L160;
{
goto L160;
phc_check_invariants (TSRMLS_C);
}
// L159:
L159:;
// $TMIt95 = $this->mDateFormat;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mDateFormat", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt95 == NULL)
    {
      local_TMIt95 = EG (uninitialized_zval_ptr);
      local_TMIt95->refcount++;
    }
  zval** p_lhs = &local_TMIt95;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L160;
{
goto L160;
phc_check_invariants (TSRMLS_C);
}
// L160:
L160:;
// $TLE42 = wfsetvar($TMIt95, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 89 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt95 == NULL)
    {
      local_TMIt95 = EG (uninitialized_zval_ptr);
      local_TMIt95->refcount++;
    }
  zval** p_arg = &local_TMIt95;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt95 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt95;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 89, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE42 == NULL)
    {
      local_TLE42 = EG (uninitialized_zval_ptr);
      local_TLE42->refcount++;
    }
  zval** p_lhs = &local_TLE42;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE42;
{
     zval* rhs;
  if (local_TLE42 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE42;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE42 != NULL)
{
zval_ptr_dtor (&local_TLE42);
}
if (local_TLE96 != NULL)
{
zval_ptr_dtor (&local_TLE96);
}
if (local_TMIt95 != NULL)
{
zval_ptr_dtor (&local_TMIt95);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function seteditsection($x)
// {
// 	$TLE98 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE98) goto L161 else goto L162;
// L161:
// 	$TMIt97 =& $this->mEditSection;
// 	goto L163;
// L162:
// 	$TMIt97 = $this->mEditSection;
// 	goto L163;
// L163:
// 	$TLE43 = wfsetvar($TMIt97, $x);
// 	return $TLE43;
// }
PHP_METHOD(ParserOptions, seteditsection)
{
zval* local_TLE43 = NULL;
zval* local_TLE98 = NULL;
zval* local_TMIt97 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE98 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE98 == NULL)
    {
      local_TLE98 = EG (uninitialized_zval_ptr);
      local_TLE98->refcount++;
    }
  zval** p_lhs = &local_TLE98;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE98) goto L161 else goto L162;
{
     zval* p_cond;
  if (local_TLE98 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE98;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L161;
   else
      goto L162;
phc_check_invariants (TSRMLS_C);
}
// L161:
L161:;
// $TMIt97 =& $this->mEditSection;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEditSection", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt97 == NULL)
    {
      local_TMIt97 = EG (uninitialized_zval_ptr);
      local_TMIt97->refcount++;
    }
  zval** p_lhs = &local_TMIt97;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L163;
{
goto L163;
phc_check_invariants (TSRMLS_C);
}
// L162:
L162:;
// $TMIt97 = $this->mEditSection;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEditSection", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt97 == NULL)
    {
      local_TMIt97 = EG (uninitialized_zval_ptr);
      local_TMIt97->refcount++;
    }
  zval** p_lhs = &local_TMIt97;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L163;
{
goto L163;
phc_check_invariants (TSRMLS_C);
}
// L163:
L163:;
// $TLE43 = wfsetvar($TMIt97, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 90 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt97 == NULL)
    {
      local_TMIt97 = EG (uninitialized_zval_ptr);
      local_TMIt97->refcount++;
    }
  zval** p_arg = &local_TMIt97;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt97 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt97;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 90, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE43 == NULL)
    {
      local_TLE43 = EG (uninitialized_zval_ptr);
      local_TLE43->refcount++;
    }
  zval** p_lhs = &local_TLE43;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE43;
{
     zval* rhs;
  if (local_TLE43 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE43;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE43 != NULL)
{
zval_ptr_dtor (&local_TLE43);
}
if (local_TLE98 != NULL)
{
zval_ptr_dtor (&local_TLE98);
}
if (local_TMIt97 != NULL)
{
zval_ptr_dtor (&local_TMIt97);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setnumberheadings($x)
// {
// 	$TLE100 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE100) goto L164 else goto L165;
// L164:
// 	$TMIt99 =& $this->mNumberHeadings;
// 	goto L166;
// L165:
// 	$TMIt99 = $this->mNumberHeadings;
// 	goto L166;
// L166:
// 	$TLE44 = wfsetvar($TMIt99, $x);
// 	return $TLE44;
// }
PHP_METHOD(ParserOptions, setnumberheadings)
{
zval* local_TLE100 = NULL;
zval* local_TLE44 = NULL;
zval* local_TMIt99 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE100 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE100 == NULL)
    {
      local_TLE100 = EG (uninitialized_zval_ptr);
      local_TLE100->refcount++;
    }
  zval** p_lhs = &local_TLE100;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE100) goto L164 else goto L165;
{
     zval* p_cond;
  if (local_TLE100 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE100;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L164;
   else
      goto L165;
phc_check_invariants (TSRMLS_C);
}
// L164:
L164:;
// $TMIt99 =& $this->mNumberHeadings;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mNumberHeadings", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt99 == NULL)
    {
      local_TMIt99 = EG (uninitialized_zval_ptr);
      local_TMIt99->refcount++;
    }
  zval** p_lhs = &local_TMIt99;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L166;
{
goto L166;
phc_check_invariants (TSRMLS_C);
}
// L165:
L165:;
// $TMIt99 = $this->mNumberHeadings;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mNumberHeadings", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt99 == NULL)
    {
      local_TMIt99 = EG (uninitialized_zval_ptr);
      local_TMIt99->refcount++;
    }
  zval** p_lhs = &local_TMIt99;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L166;
{
goto L166;
phc_check_invariants (TSRMLS_C);
}
// L166:
L166:;
// $TLE44 = wfsetvar($TMIt99, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 91 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt99 == NULL)
    {
      local_TMIt99 = EG (uninitialized_zval_ptr);
      local_TMIt99->refcount++;
    }
  zval** p_arg = &local_TMIt99;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt99 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt99;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 91, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE44 == NULL)
    {
      local_TLE44 = EG (uninitialized_zval_ptr);
      local_TLE44->refcount++;
    }
  zval** p_lhs = &local_TLE44;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE44;
{
     zval* rhs;
  if (local_TLE44 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE44;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE100 != NULL)
{
zval_ptr_dtor (&local_TLE100);
}
if (local_TLE44 != NULL)
{
zval_ptr_dtor (&local_TLE44);
}
if (local_TMIt99 != NULL)
{
zval_ptr_dtor (&local_TMIt99);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setallowspecialinclusion($x)
// {
// 	$TLE102 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE102) goto L167 else goto L168;
// L167:
// 	$TMIt101 =& $this->mAllowSpecialInclusion;
// 	goto L169;
// L168:
// 	$TMIt101 = $this->mAllowSpecialInclusion;
// 	goto L169;
// L169:
// 	$TLE45 = wfsetvar($TMIt101, $x);
// 	return $TLE45;
// }
PHP_METHOD(ParserOptions, setallowspecialinclusion)
{
zval* local_TLE102 = NULL;
zval* local_TLE45 = NULL;
zval* local_TMIt101 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE102 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE102 == NULL)
    {
      local_TLE102 = EG (uninitialized_zval_ptr);
      local_TLE102->refcount++;
    }
  zval** p_lhs = &local_TLE102;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE102) goto L167 else goto L168;
{
     zval* p_cond;
  if (local_TLE102 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE102;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L167;
   else
      goto L168;
phc_check_invariants (TSRMLS_C);
}
// L167:
L167:;
// $TMIt101 =& $this->mAllowSpecialInclusion;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowSpecialInclusion", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt101 == NULL)
    {
      local_TMIt101 = EG (uninitialized_zval_ptr);
      local_TMIt101->refcount++;
    }
  zval** p_lhs = &local_TMIt101;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L169;
{
goto L169;
phc_check_invariants (TSRMLS_C);
}
// L168:
L168:;
// $TMIt101 = $this->mAllowSpecialInclusion;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowSpecialInclusion", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt101 == NULL)
    {
      local_TMIt101 = EG (uninitialized_zval_ptr);
      local_TMIt101->refcount++;
    }
  zval** p_lhs = &local_TMIt101;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L169;
{
goto L169;
phc_check_invariants (TSRMLS_C);
}
// L169:
L169:;
// $TLE45 = wfsetvar($TMIt101, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 92 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt101 == NULL)
    {
      local_TMIt101 = EG (uninitialized_zval_ptr);
      local_TMIt101->refcount++;
    }
  zval** p_arg = &local_TMIt101;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt101 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt101;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 92, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE45 == NULL)
    {
      local_TLE45 = EG (uninitialized_zval_ptr);
      local_TLE45->refcount++;
    }
  zval** p_lhs = &local_TLE45;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE45;
{
     zval* rhs;
  if (local_TLE45 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE45;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE102 != NULL)
{
zval_ptr_dtor (&local_TLE102);
}
if (local_TLE45 != NULL)
{
zval_ptr_dtor (&local_TLE45);
}
if (local_TMIt101 != NULL)
{
zval_ptr_dtor (&local_TMIt101);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function settidy($x)
// {
// 	$TLE104 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE104) goto L170 else goto L171;
// L170:
// 	$TMIt103 =& $this->mTidy;
// 	goto L172;
// L171:
// 	$TMIt103 = $this->mTidy;
// 	goto L172;
// L172:
// 	$TLE46 = wfsetvar($TMIt103, $x);
// 	return $TLE46;
// }
PHP_METHOD(ParserOptions, settidy)
{
zval* local_TLE104 = NULL;
zval* local_TLE46 = NULL;
zval* local_TMIt103 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE104 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE104 == NULL)
    {
      local_TLE104 = EG (uninitialized_zval_ptr);
      local_TLE104->refcount++;
    }
  zval** p_lhs = &local_TLE104;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE104) goto L170 else goto L171;
{
     zval* p_cond;
  if (local_TLE104 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE104;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L170;
   else
      goto L171;
phc_check_invariants (TSRMLS_C);
}
// L170:
L170:;
// $TMIt103 =& $this->mTidy;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTidy", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt103 == NULL)
    {
      local_TMIt103 = EG (uninitialized_zval_ptr);
      local_TMIt103->refcount++;
    }
  zval** p_lhs = &local_TMIt103;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L172;
{
goto L172;
phc_check_invariants (TSRMLS_C);
}
// L171:
L171:;
// $TMIt103 = $this->mTidy;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTidy", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt103 == NULL)
    {
      local_TMIt103 = EG (uninitialized_zval_ptr);
      local_TMIt103->refcount++;
    }
  zval** p_lhs = &local_TMIt103;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L172;
{
goto L172;
phc_check_invariants (TSRMLS_C);
}
// L172:
L172:;
// $TLE46 = wfsetvar($TMIt103, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 93 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt103 == NULL)
    {
      local_TMIt103 = EG (uninitialized_zval_ptr);
      local_TMIt103->refcount++;
    }
  zval** p_arg = &local_TMIt103;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt103 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt103;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 93, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE46 == NULL)
    {
      local_TLE46 = EG (uninitialized_zval_ptr);
      local_TLE46->refcount++;
    }
  zval** p_lhs = &local_TLE46;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE46;
{
     zval* rhs;
  if (local_TLE46 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE46;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE104 != NULL)
{
zval_ptr_dtor (&local_TLE104);
}
if (local_TLE46 != NULL)
{
zval_ptr_dtor (&local_TLE46);
}
if (local_TMIt103 != NULL)
{
zval_ptr_dtor (&local_TMIt103);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setskin($x)
// {
// 	$this->mSkin = $x;
// }
PHP_METHOD(ParserOptions, setskin)
{
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $this->mSkin = $x;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_x == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_x;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mSkin", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setinterfacemessage($x)
// {
// 	$TLE106 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE106) goto L173 else goto L174;
// L173:
// 	$TMIt105 =& $this->mInterfaceMessage;
// 	goto L175;
// L174:
// 	$TMIt105 = $this->mInterfaceMessage;
// 	goto L175;
// L175:
// 	$TLE47 = wfsetvar($TMIt105, $x);
// 	return $TLE47;
// }
PHP_METHOD(ParserOptions, setinterfacemessage)
{
zval* local_TLE106 = NULL;
zval* local_TLE47 = NULL;
zval* local_TMIt105 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE106 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE106 == NULL)
    {
      local_TLE106 = EG (uninitialized_zval_ptr);
      local_TLE106->refcount++;
    }
  zval** p_lhs = &local_TLE106;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE106) goto L173 else goto L174;
{
     zval* p_cond;
  if (local_TLE106 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE106;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L173;
   else
      goto L174;
phc_check_invariants (TSRMLS_C);
}
// L173:
L173:;
// $TMIt105 =& $this->mInterfaceMessage;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mInterfaceMessage", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt105 == NULL)
    {
      local_TMIt105 = EG (uninitialized_zval_ptr);
      local_TMIt105->refcount++;
    }
  zval** p_lhs = &local_TMIt105;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L175;
{
goto L175;
phc_check_invariants (TSRMLS_C);
}
// L174:
L174:;
// $TMIt105 = $this->mInterfaceMessage;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mInterfaceMessage", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt105 == NULL)
    {
      local_TMIt105 = EG (uninitialized_zval_ptr);
      local_TMIt105->refcount++;
    }
  zval** p_lhs = &local_TMIt105;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L175;
{
goto L175;
phc_check_invariants (TSRMLS_C);
}
// L175:
L175:;
// $TLE47 = wfsetvar($TMIt105, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 95 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt105 == NULL)
    {
      local_TMIt105 = EG (uninitialized_zval_ptr);
      local_TMIt105->refcount++;
    }
  zval** p_arg = &local_TMIt105;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt105 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt105;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 95, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE47 == NULL)
    {
      local_TLE47 = EG (uninitialized_zval_ptr);
      local_TLE47->refcount++;
    }
  zval** p_lhs = &local_TLE47;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE47;
{
     zval* rhs;
  if (local_TLE47 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE47;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE106 != NULL)
{
zval_ptr_dtor (&local_TLE106);
}
if (local_TLE47 != NULL)
{
zval_ptr_dtor (&local_TLE47);
}
if (local_TMIt105 != NULL)
{
zval_ptr_dtor (&local_TMIt105);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function settargetlanguage($x)
// {
// 	$TLE108 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE108) goto L176 else goto L177;
// L176:
// 	$TMIt107 =& $this->mTargetLanguage;
// 	goto L178;
// L177:
// 	$TMIt107 = $this->mTargetLanguage;
// 	goto L178;
// L178:
// 	$TLE48 = wfsetvar($TMIt107, $x);
// 	return $TLE48;
// }
PHP_METHOD(ParserOptions, settargetlanguage)
{
zval* local_TLE108 = NULL;
zval* local_TLE48 = NULL;
zval* local_TMIt107 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE108 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE108 == NULL)
    {
      local_TLE108 = EG (uninitialized_zval_ptr);
      local_TLE108->refcount++;
    }
  zval** p_lhs = &local_TLE108;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE108) goto L176 else goto L177;
{
     zval* p_cond;
  if (local_TLE108 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE108;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L176;
   else
      goto L177;
phc_check_invariants (TSRMLS_C);
}
// L176:
L176:;
// $TMIt107 =& $this->mTargetLanguage;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTargetLanguage", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt107 == NULL)
    {
      local_TMIt107 = EG (uninitialized_zval_ptr);
      local_TMIt107->refcount++;
    }
  zval** p_lhs = &local_TMIt107;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L178;
{
goto L178;
phc_check_invariants (TSRMLS_C);
}
// L177:
L177:;
// $TMIt107 = $this->mTargetLanguage;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTargetLanguage", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt107 == NULL)
    {
      local_TMIt107 = EG (uninitialized_zval_ptr);
      local_TMIt107->refcount++;
    }
  zval** p_lhs = &local_TMIt107;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L178;
{
goto L178;
phc_check_invariants (TSRMLS_C);
}
// L178:
L178:;
// $TLE48 = wfsetvar($TMIt107, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 96 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt107 == NULL)
    {
      local_TMIt107 = EG (uninitialized_zval_ptr);
      local_TMIt107->refcount++;
    }
  zval** p_arg = &local_TMIt107;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt107 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt107;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 96, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE48 == NULL)
    {
      local_TLE48 = EG (uninitialized_zval_ptr);
      local_TLE48->refcount++;
    }
  zval** p_lhs = &local_TLE48;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE48;
{
     zval* rhs;
  if (local_TLE48 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE48;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE108 != NULL)
{
zval_ptr_dtor (&local_TLE108);
}
if (local_TLE48 != NULL)
{
zval_ptr_dtor (&local_TLE48);
}
if (local_TMIt107 != NULL)
{
zval_ptr_dtor (&local_TMIt107);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setmaxincludesize($x)
// {
// 	$TLE110 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE110) goto L179 else goto L180;
// L179:
// 	$TMIt109 =& $this->mMaxIncludeSize;
// 	goto L181;
// L180:
// 	$TMIt109 = $this->mMaxIncludeSize;
// 	goto L181;
// L181:
// 	$TLE49 = wfsetvar($TMIt109, $x);
// 	return $TLE49;
// }
PHP_METHOD(ParserOptions, setmaxincludesize)
{
zval* local_TLE110 = NULL;
zval* local_TLE49 = NULL;
zval* local_TMIt109 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE110 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE110 == NULL)
    {
      local_TLE110 = EG (uninitialized_zval_ptr);
      local_TLE110->refcount++;
    }
  zval** p_lhs = &local_TLE110;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE110) goto L179 else goto L180;
{
     zval* p_cond;
  if (local_TLE110 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE110;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L179;
   else
      goto L180;
phc_check_invariants (TSRMLS_C);
}
// L179:
L179:;
// $TMIt109 =& $this->mMaxIncludeSize;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxIncludeSize", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt109 == NULL)
    {
      local_TMIt109 = EG (uninitialized_zval_ptr);
      local_TMIt109->refcount++;
    }
  zval** p_lhs = &local_TMIt109;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L181;
{
goto L181;
phc_check_invariants (TSRMLS_C);
}
// L180:
L180:;
// $TMIt109 = $this->mMaxIncludeSize;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxIncludeSize", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt109 == NULL)
    {
      local_TMIt109 = EG (uninitialized_zval_ptr);
      local_TMIt109->refcount++;
    }
  zval** p_lhs = &local_TMIt109;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L181;
{
goto L181;
phc_check_invariants (TSRMLS_C);
}
// L181:
L181:;
// $TLE49 = wfsetvar($TMIt109, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 97 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt109 == NULL)
    {
      local_TMIt109 = EG (uninitialized_zval_ptr);
      local_TMIt109->refcount++;
    }
  zval** p_arg = &local_TMIt109;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt109 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt109;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 97, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE49 == NULL)
    {
      local_TLE49 = EG (uninitialized_zval_ptr);
      local_TLE49->refcount++;
    }
  zval** p_lhs = &local_TLE49;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE49;
{
     zval* rhs;
  if (local_TLE49 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE49;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE110 != NULL)
{
zval_ptr_dtor (&local_TLE110);
}
if (local_TLE49 != NULL)
{
zval_ptr_dtor (&local_TLE49);
}
if (local_TMIt109 != NULL)
{
zval_ptr_dtor (&local_TMIt109);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setmaxppnodecount($x)
// {
// 	$TLE112 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE112) goto L182 else goto L183;
// L182:
// 	$TMIt111 =& $this->mMaxPPNodeCount;
// 	goto L184;
// L183:
// 	$TMIt111 = $this->mMaxPPNodeCount;
// 	goto L184;
// L184:
// 	$TLE50 = wfsetvar($TMIt111, $x);
// 	return $TLE50;
// }
PHP_METHOD(ParserOptions, setmaxppnodecount)
{
zval* local_TLE112 = NULL;
zval* local_TLE50 = NULL;
zval* local_TMIt111 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE112 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE112 == NULL)
    {
      local_TLE112 = EG (uninitialized_zval_ptr);
      local_TLE112->refcount++;
    }
  zval** p_lhs = &local_TLE112;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE112) goto L182 else goto L183;
{
     zval* p_cond;
  if (local_TLE112 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE112;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L182;
   else
      goto L183;
phc_check_invariants (TSRMLS_C);
}
// L182:
L182:;
// $TMIt111 =& $this->mMaxPPNodeCount;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxPPNodeCount", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt111 == NULL)
    {
      local_TMIt111 = EG (uninitialized_zval_ptr);
      local_TMIt111->refcount++;
    }
  zval** p_lhs = &local_TMIt111;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L184;
{
goto L184;
phc_check_invariants (TSRMLS_C);
}
// L183:
L183:;
// $TMIt111 = $this->mMaxPPNodeCount;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxPPNodeCount", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt111 == NULL)
    {
      local_TMIt111 = EG (uninitialized_zval_ptr);
      local_TMIt111->refcount++;
    }
  zval** p_lhs = &local_TMIt111;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L184;
{
goto L184;
phc_check_invariants (TSRMLS_C);
}
// L184:
L184:;
// $TLE50 = wfsetvar($TMIt111, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 98 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt111 == NULL)
    {
      local_TMIt111 = EG (uninitialized_zval_ptr);
      local_TMIt111->refcount++;
    }
  zval** p_arg = &local_TMIt111;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt111 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt111;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 98, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE50 == NULL)
    {
      local_TLE50 = EG (uninitialized_zval_ptr);
      local_TLE50->refcount++;
    }
  zval** p_lhs = &local_TLE50;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE50;
{
     zval* rhs;
  if (local_TLE50 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE50;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE112 != NULL)
{
zval_ptr_dtor (&local_TLE112);
}
if (local_TLE50 != NULL)
{
zval_ptr_dtor (&local_TLE50);
}
if (local_TMIt111 != NULL)
{
zval_ptr_dtor (&local_TMIt111);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setmaxtemplatedepth($x)
// {
// 	$TLE114 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE114) goto L185 else goto L186;
// L185:
// 	$TMIt113 =& $this->mMaxTemplateDepth;
// 	goto L187;
// L186:
// 	$TMIt113 = $this->mMaxTemplateDepth;
// 	goto L187;
// L187:
// 	$TLE51 = wfsetvar($TMIt113, $x);
// 	return $TLE51;
// }
PHP_METHOD(ParserOptions, setmaxtemplatedepth)
{
zval* local_TLE114 = NULL;
zval* local_TLE51 = NULL;
zval* local_TMIt113 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE114 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE114 == NULL)
    {
      local_TLE114 = EG (uninitialized_zval_ptr);
      local_TLE114->refcount++;
    }
  zval** p_lhs = &local_TLE114;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE114) goto L185 else goto L186;
{
     zval* p_cond;
  if (local_TLE114 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE114;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L185;
   else
      goto L186;
phc_check_invariants (TSRMLS_C);
}
// L185:
L185:;
// $TMIt113 =& $this->mMaxTemplateDepth;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxTemplateDepth", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt113 == NULL)
    {
      local_TMIt113 = EG (uninitialized_zval_ptr);
      local_TMIt113->refcount++;
    }
  zval** p_lhs = &local_TMIt113;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L187;
{
goto L187;
phc_check_invariants (TSRMLS_C);
}
// L186:
L186:;
// $TMIt113 = $this->mMaxTemplateDepth;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxTemplateDepth", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt113 == NULL)
    {
      local_TMIt113 = EG (uninitialized_zval_ptr);
      local_TMIt113->refcount++;
    }
  zval** p_lhs = &local_TMIt113;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L187;
{
goto L187;
phc_check_invariants (TSRMLS_C);
}
// L187:
L187:;
// $TLE51 = wfsetvar($TMIt113, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 99 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt113 == NULL)
    {
      local_TMIt113 = EG (uninitialized_zval_ptr);
      local_TMIt113->refcount++;
    }
  zval** p_arg = &local_TMIt113;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt113 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt113;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 99, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE51 == NULL)
    {
      local_TLE51 = EG (uninitialized_zval_ptr);
      local_TLE51->refcount++;
    }
  zval** p_lhs = &local_TLE51;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE51;
{
     zval* rhs;
  if (local_TLE51 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE51;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE114 != NULL)
{
zval_ptr_dtor (&local_TLE114);
}
if (local_TLE51 != NULL)
{
zval_ptr_dtor (&local_TLE51);
}
if (local_TMIt113 != NULL)
{
zval_ptr_dtor (&local_TMIt113);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setremovecomments($x)
// {
// 	$TLE116 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE116) goto L188 else goto L189;
// L188:
// 	$TMIt115 =& $this->mRemoveComments;
// 	goto L190;
// L189:
// 	$TMIt115 = $this->mRemoveComments;
// 	goto L190;
// L190:
// 	$TLE52 = wfsetvar($TMIt115, $x);
// 	return $TLE52;
// }
PHP_METHOD(ParserOptions, setremovecomments)
{
zval* local_TLE116 = NULL;
zval* local_TLE52 = NULL;
zval* local_TMIt115 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE116 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE116 == NULL)
    {
      local_TLE116 = EG (uninitialized_zval_ptr);
      local_TLE116->refcount++;
    }
  zval** p_lhs = &local_TLE116;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE116) goto L188 else goto L189;
{
     zval* p_cond;
  if (local_TLE116 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE116;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L188;
   else
      goto L189;
phc_check_invariants (TSRMLS_C);
}
// L188:
L188:;
// $TMIt115 =& $this->mRemoveComments;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mRemoveComments", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt115 == NULL)
    {
      local_TMIt115 = EG (uninitialized_zval_ptr);
      local_TMIt115->refcount++;
    }
  zval** p_lhs = &local_TMIt115;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L190;
{
goto L190;
phc_check_invariants (TSRMLS_C);
}
// L189:
L189:;
// $TMIt115 = $this->mRemoveComments;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mRemoveComments", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt115 == NULL)
    {
      local_TMIt115 = EG (uninitialized_zval_ptr);
      local_TMIt115->refcount++;
    }
  zval** p_lhs = &local_TMIt115;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L190;
{
goto L190;
phc_check_invariants (TSRMLS_C);
}
// L190:
L190:;
// $TLE52 = wfsetvar($TMIt115, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 100 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt115 == NULL)
    {
      local_TMIt115 = EG (uninitialized_zval_ptr);
      local_TMIt115->refcount++;
    }
  zval** p_arg = &local_TMIt115;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt115 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt115;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 100, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE52 == NULL)
    {
      local_TLE52 = EG (uninitialized_zval_ptr);
      local_TLE52->refcount++;
    }
  zval** p_lhs = &local_TLE52;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE52;
{
     zval* rhs;
  if (local_TLE52 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE52;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE116 != NULL)
{
zval_ptr_dtor (&local_TLE116);
}
if (local_TLE52 != NULL)
{
zval_ptr_dtor (&local_TLE52);
}
if (local_TMIt115 != NULL)
{
zval_ptr_dtor (&local_TMIt115);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function settemplatecallback($x)
// {
// 	$TLE118 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE118) goto L191 else goto L192;
// L191:
// 	$TMIt117 =& $this->mTemplateCallback;
// 	goto L193;
// L192:
// 	$TMIt117 = $this->mTemplateCallback;
// 	goto L193;
// L193:
// 	$TLE53 = wfsetvar($TMIt117, $x);
// 	return $TLE53;
// }
PHP_METHOD(ParserOptions, settemplatecallback)
{
zval* local_TLE118 = NULL;
zval* local_TLE53 = NULL;
zval* local_TMIt117 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE118 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE118 == NULL)
    {
      local_TLE118 = EG (uninitialized_zval_ptr);
      local_TLE118->refcount++;
    }
  zval** p_lhs = &local_TLE118;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE118) goto L191 else goto L192;
{
     zval* p_cond;
  if (local_TLE118 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE118;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L191;
   else
      goto L192;
phc_check_invariants (TSRMLS_C);
}
// L191:
L191:;
// $TMIt117 =& $this->mTemplateCallback;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplateCallback", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt117 == NULL)
    {
      local_TMIt117 = EG (uninitialized_zval_ptr);
      local_TMIt117->refcount++;
    }
  zval** p_lhs = &local_TMIt117;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L193;
{
goto L193;
phc_check_invariants (TSRMLS_C);
}
// L192:
L192:;
// $TMIt117 = $this->mTemplateCallback;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplateCallback", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt117 == NULL)
    {
      local_TMIt117 = EG (uninitialized_zval_ptr);
      local_TMIt117->refcount++;
    }
  zval** p_lhs = &local_TMIt117;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L193;
{
goto L193;
phc_check_invariants (TSRMLS_C);
}
// L193:
L193:;
// $TLE53 = wfsetvar($TMIt117, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 101 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt117 == NULL)
    {
      local_TMIt117 = EG (uninitialized_zval_ptr);
      local_TMIt117->refcount++;
    }
  zval** p_arg = &local_TMIt117;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt117 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt117;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 101, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE53 == NULL)
    {
      local_TLE53 = EG (uninitialized_zval_ptr);
      local_TLE53->refcount++;
    }
  zval** p_lhs = &local_TLE53;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE53;
{
     zval* rhs;
  if (local_TLE53 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE53;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE118 != NULL)
{
zval_ptr_dtor (&local_TLE118);
}
if (local_TLE53 != NULL)
{
zval_ptr_dtor (&local_TLE53);
}
if (local_TMIt117 != NULL)
{
zval_ptr_dtor (&local_TMIt117);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function enablelimitreport($x = True)
// {
// 	$TLE120 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE120) goto L194 else goto L195;
// L194:
// 	$TMIt119 =& $this->mEnableLimitReport;
// 	goto L196;
// L195:
// 	$TMIt119 = $this->mEnableLimitReport;
// 	goto L196;
// L196:
// 	$TLE54 = wfsetvar($TMIt119, $x);
// 	return $TLE54;
// }
PHP_METHOD(ParserOptions, enablelimitreport)
{
zval* local_TLE120 = NULL;
zval* local_TLE54 = NULL;
zval* local_TMIt119 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
if (num_args <= 0)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = True;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 1);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[0] = default_value;
}
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE120 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE120 == NULL)
    {
      local_TLE120 = EG (uninitialized_zval_ptr);
      local_TLE120->refcount++;
    }
  zval** p_lhs = &local_TLE120;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE120) goto L194 else goto L195;
{
     zval* p_cond;
  if (local_TLE120 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE120;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L194;
   else
      goto L195;
phc_check_invariants (TSRMLS_C);
}
// L194:
L194:;
// $TMIt119 =& $this->mEnableLimitReport;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEnableLimitReport", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt119 == NULL)
    {
      local_TMIt119 = EG (uninitialized_zval_ptr);
      local_TMIt119->refcount++;
    }
  zval** p_lhs = &local_TMIt119;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L196;
{
goto L196;
phc_check_invariants (TSRMLS_C);
}
// L195:
L195:;
// $TMIt119 = $this->mEnableLimitReport;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEnableLimitReport", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt119 == NULL)
    {
      local_TMIt119 = EG (uninitialized_zval_ptr);
      local_TMIt119->refcount++;
    }
  zval** p_lhs = &local_TMIt119;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L196;
{
goto L196;
phc_check_invariants (TSRMLS_C);
}
// L196:
L196:;
// $TLE54 = wfsetvar($TMIt119, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 102 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt119 == NULL)
    {
      local_TMIt119 = EG (uninitialized_zval_ptr);
      local_TMIt119->refcount++;
    }
  zval** p_arg = &local_TMIt119;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt119 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt119;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 102, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE54 == NULL)
    {
      local_TLE54 = EG (uninitialized_zval_ptr);
      local_TLE54->refcount++;
    }
  zval** p_lhs = &local_TLE54;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE54;
{
     zval* rhs;
  if (local_TLE54 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE54;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE120 != NULL)
{
zval_ptr_dtor (&local_TLE120);
}
if (local_TLE54 != NULL)
{
zval_ptr_dtor (&local_TLE54);
}
if (local_TMIt119 != NULL)
{
zval_ptr_dtor (&local_TMIt119);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function settimestamp($x)
// {
// 	$TLE122 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE122) goto L197 else goto L198;
// L197:
// 	$TMIt121 =& $this->mTimestamp;
// 	goto L199;
// L198:
// 	$TMIt121 = $this->mTimestamp;
// 	goto L199;
// L199:
// 	$TLE55 = wfsetvar($TMIt121, $x);
// 	return $TLE55;
// }
PHP_METHOD(ParserOptions, settimestamp)
{
zval* local_TLE122 = NULL;
zval* local_TLE55 = NULL;
zval* local_TMIt121 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE122 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE122 == NULL)
    {
      local_TLE122 = EG (uninitialized_zval_ptr);
      local_TLE122->refcount++;
    }
  zval** p_lhs = &local_TLE122;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE122) goto L197 else goto L198;
{
     zval* p_cond;
  if (local_TLE122 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE122;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L197;
   else
      goto L198;
phc_check_invariants (TSRMLS_C);
}
// L197:
L197:;
// $TMIt121 =& $this->mTimestamp;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTimestamp", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt121 == NULL)
    {
      local_TMIt121 = EG (uninitialized_zval_ptr);
      local_TMIt121->refcount++;
    }
  zval** p_lhs = &local_TMIt121;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L199;
{
goto L199;
phc_check_invariants (TSRMLS_C);
}
// L198:
L198:;
// $TMIt121 = $this->mTimestamp;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTimestamp", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt121 == NULL)
    {
      local_TMIt121 = EG (uninitialized_zval_ptr);
      local_TMIt121->refcount++;
    }
  zval** p_lhs = &local_TMIt121;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L199;
{
goto L199;
phc_check_invariants (TSRMLS_C);
}
// L199:
L199:;
// $TLE55 = wfsetvar($TMIt121, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 103 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt121 == NULL)
    {
      local_TMIt121 = EG (uninitialized_zval_ptr);
      local_TMIt121->refcount++;
    }
  zval** p_arg = &local_TMIt121;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt121 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt121;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 103, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE55 == NULL)
    {
      local_TLE55 = EG (uninitialized_zval_ptr);
      local_TLE55->refcount++;
    }
  zval** p_lhs = &local_TLE55;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE55;
{
     zval* rhs;
  if (local_TLE55 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE55;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE122 != NULL)
{
zval_ptr_dtor (&local_TLE122);
}
if (local_TLE55 != NULL)
{
zval_ptr_dtor (&local_TLE55);
}
if (local_TMIt121 != NULL)
{
zval_ptr_dtor (&local_TMIt121);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setcleansignatures($x)
// {
// 	$TLE124 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE124) goto L200 else goto L201;
// L200:
// 	$TMIt123 =& $this->mCleanSignatures;
// 	goto L202;
// L201:
// 	$TMIt123 = $this->mCleanSignatures;
// 	goto L202;
// L202:
// 	$TLE56 = wfsetvar($TMIt123, $x);
// 	return $TLE56;
// }
PHP_METHOD(ParserOptions, setcleansignatures)
{
zval* local_TLE124 = NULL;
zval* local_TLE56 = NULL;
zval* local_TMIt123 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE124 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE124 == NULL)
    {
      local_TLE124 = EG (uninitialized_zval_ptr);
      local_TLE124->refcount++;
    }
  zval** p_lhs = &local_TLE124;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE124) goto L200 else goto L201;
{
     zval* p_cond;
  if (local_TLE124 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE124;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L200;
   else
      goto L201;
phc_check_invariants (TSRMLS_C);
}
// L200:
L200:;
// $TMIt123 =& $this->mCleanSignatures;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCleanSignatures", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt123 == NULL)
    {
      local_TMIt123 = EG (uninitialized_zval_ptr);
      local_TMIt123->refcount++;
    }
  zval** p_lhs = &local_TMIt123;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L202;
{
goto L202;
phc_check_invariants (TSRMLS_C);
}
// L201:
L201:;
// $TMIt123 = $this->mCleanSignatures;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCleanSignatures", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt123 == NULL)
    {
      local_TMIt123 = EG (uninitialized_zval_ptr);
      local_TMIt123->refcount++;
    }
  zval** p_lhs = &local_TMIt123;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L202;
{
goto L202;
phc_check_invariants (TSRMLS_C);
}
// L202:
L202:;
// $TLE56 = wfsetvar($TMIt123, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 104 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt123 == NULL)
    {
      local_TMIt123 = EG (uninitialized_zval_ptr);
      local_TMIt123->refcount++;
    }
  zval** p_arg = &local_TMIt123;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt123 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt123;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 104, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE56 == NULL)
    {
      local_TLE56 = EG (uninitialized_zval_ptr);
      local_TLE56->refcount++;
    }
  zval** p_lhs = &local_TLE56;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE56;
{
     zval* rhs;
  if (local_TLE56 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE56;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE124 != NULL)
{
zval_ptr_dtor (&local_TLE124);
}
if (local_TLE56 != NULL)
{
zval_ptr_dtor (&local_TLE56);
}
if (local_TMIt123 != NULL)
{
zval_ptr_dtor (&local_TMIt123);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setexternallinktarget($x)
// {
// 	$TLE126 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE126) goto L203 else goto L204;
// L203:
// 	$TMIt125 =& $this->mExternalLinkTarget;
// 	goto L205;
// L204:
// 	$TMIt125 = $this->mExternalLinkTarget;
// 	goto L205;
// L205:
// 	$TLE57 = wfsetvar($TMIt125, $x);
// 	return $TLE57;
// }
PHP_METHOD(ParserOptions, setexternallinktarget)
{
zval* local_TLE126 = NULL;
zval* local_TLE57 = NULL;
zval* local_TMIt125 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE126 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE126 == NULL)
    {
      local_TLE126 = EG (uninitialized_zval_ptr);
      local_TLE126->refcount++;
    }
  zval** p_lhs = &local_TLE126;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE126) goto L203 else goto L204;
{
     zval* p_cond;
  if (local_TLE126 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE126;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L203;
   else
      goto L204;
phc_check_invariants (TSRMLS_C);
}
// L203:
L203:;
// $TMIt125 =& $this->mExternalLinkTarget;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mExternalLinkTarget", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt125 == NULL)
    {
      local_TMIt125 = EG (uninitialized_zval_ptr);
      local_TMIt125->refcount++;
    }
  zval** p_lhs = &local_TMIt125;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L205;
{
goto L205;
phc_check_invariants (TSRMLS_C);
}
// L204:
L204:;
// $TMIt125 = $this->mExternalLinkTarget;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mExternalLinkTarget", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt125 == NULL)
    {
      local_TMIt125 = EG (uninitialized_zval_ptr);
      local_TMIt125->refcount++;
    }
  zval** p_lhs = &local_TMIt125;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L205;
{
goto L205;
phc_check_invariants (TSRMLS_C);
}
// L205:
L205:;
// $TLE57 = wfsetvar($TMIt125, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 105 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt125 == NULL)
    {
      local_TMIt125 = EG (uninitialized_zval_ptr);
      local_TMIt125->refcount++;
    }
  zval** p_arg = &local_TMIt125;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt125 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt125;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 105, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE57 == NULL)
    {
      local_TLE57 = EG (uninitialized_zval_ptr);
      local_TLE57->refcount++;
    }
  zval** p_lhs = &local_TLE57;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE57;
{
     zval* rhs;
  if (local_TLE57 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE57;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE126 != NULL)
{
zval_ptr_dtor (&local_TLE126);
}
if (local_TLE57 != NULL)
{
zval_ptr_dtor (&local_TLE57);
}
if (local_TMIt125 != NULL)
{
zval_ptr_dtor (&local_TMIt125);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setispreview($x)
// {
// 	$TLE128 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE128) goto L206 else goto L207;
// L206:
// 	$TMIt127 =& $this->mIsPreview;
// 	goto L208;
// L207:
// 	$TMIt127 = $this->mIsPreview;
// 	goto L208;
// L208:
// 	$TLE58 = wfsetvar($TMIt127, $x);
// 	return $TLE58;
// }
PHP_METHOD(ParserOptions, setispreview)
{
zval* local_TLE128 = NULL;
zval* local_TLE58 = NULL;
zval* local_TMIt127 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE128 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE128 == NULL)
    {
      local_TLE128 = EG (uninitialized_zval_ptr);
      local_TLE128->refcount++;
    }
  zval** p_lhs = &local_TLE128;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE128) goto L206 else goto L207;
{
     zval* p_cond;
  if (local_TLE128 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE128;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L206;
   else
      goto L207;
phc_check_invariants (TSRMLS_C);
}
// L206:
L206:;
// $TMIt127 =& $this->mIsPreview;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIsPreview", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt127 == NULL)
    {
      local_TMIt127 = EG (uninitialized_zval_ptr);
      local_TMIt127->refcount++;
    }
  zval** p_lhs = &local_TMIt127;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L208;
{
goto L208;
phc_check_invariants (TSRMLS_C);
}
// L207:
L207:;
// $TMIt127 = $this->mIsPreview;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIsPreview", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt127 == NULL)
    {
      local_TMIt127 = EG (uninitialized_zval_ptr);
      local_TMIt127->refcount++;
    }
  zval** p_lhs = &local_TMIt127;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L208;
{
goto L208;
phc_check_invariants (TSRMLS_C);
}
// L208:
L208:;
// $TLE58 = wfsetvar($TMIt127, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 106 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt127 == NULL)
    {
      local_TMIt127 = EG (uninitialized_zval_ptr);
      local_TMIt127->refcount++;
    }
  zval** p_arg = &local_TMIt127;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt127 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt127;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 106, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE58 == NULL)
    {
      local_TLE58 = EG (uninitialized_zval_ptr);
      local_TLE58->refcount++;
    }
  zval** p_lhs = &local_TLE58;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE58;
{
     zval* rhs;
  if (local_TLE58 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE58;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE128 != NULL)
{
zval_ptr_dtor (&local_TLE128);
}
if (local_TLE58 != NULL)
{
zval_ptr_dtor (&local_TLE58);
}
if (local_TMIt127 != NULL)
{
zval_ptr_dtor (&local_TMIt127);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function setissectionpreview($x)
// {
// 	$TLE130 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE130) goto L209 else goto L210;
// L209:
// 	$TMIt129 =& $this->mIsSectionPreview;
// 	goto L211;
// L210:
// 	$TMIt129 = $this->mIsSectionPreview;
// 	goto L211;
// L211:
// 	$TLE59 = wfsetvar($TMIt129, $x);
// 	return $TLE59;
// }
PHP_METHOD(ParserOptions, setissectionpreview)
{
zval* local_TLE130 = NULL;
zval* local_TLE59 = NULL;
zval* local_TMIt129 = NULL;
zval* local_this = getThis();
zval* local_x = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_x != NULL)
{
	zval_ptr_dtor (&local_x);
}
local_x = params[0];
}
// Function body
// $TLE130 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE130 == NULL)
    {
      local_TLE130 = EG (uninitialized_zval_ptr);
      local_TLE130->refcount++;
    }
  zval** p_lhs = &local_TLE130;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE130) goto L209 else goto L210;
{
     zval* p_cond;
  if (local_TLE130 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE130;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L209;
   else
      goto L210;
phc_check_invariants (TSRMLS_C);
}
// L209:
L209:;
// $TMIt129 =& $this->mIsSectionPreview;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIsSectionPreview", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt129 == NULL)
    {
      local_TMIt129 = EG (uninitialized_zval_ptr);
      local_TMIt129->refcount++;
    }
  zval** p_lhs = &local_TMIt129;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L211;
{
goto L211;
phc_check_invariants (TSRMLS_C);
}
// L210:
L210:;
// $TMIt129 = $this->mIsSectionPreview;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIsSectionPreview", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt129 == NULL)
    {
      local_TMIt129 = EG (uninitialized_zval_ptr);
      local_TMIt129->refcount++;
    }
  zval** p_lhs = &local_TMIt129;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L211;
{
goto L211;
phc_check_invariants (TSRMLS_C);
}
// L211:
L211:;
// $TLE59 = wfsetvar($TMIt129, $x);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 107 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt129 == NULL)
    {
      local_TMIt129 = EG (uninitialized_zval_ptr);
      local_TMIt129->refcount++;
    }
  zval** p_arg = &local_TMIt129;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt129 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt129;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_x == NULL)
    {
      local_x = EG (uninitialized_zval_ptr);
      local_x->refcount++;
    }
  zval** p_arg = &local_x;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_x == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_x;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 107, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE59 == NULL)
    {
      local_TLE59 = EG (uninitialized_zval_ptr);
      local_TLE59->refcount++;
    }
  zval** p_lhs = &local_TLE59;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE59;
{
     zval* rhs;
  if (local_TLE59 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE59;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE130 != NULL)
{
zval_ptr_dtor (&local_TLE130);
}
if (local_TLE59 != NULL)
{
zval_ptr_dtor (&local_TLE59);
}
if (local_TMIt129 != NULL)
{
zval_ptr_dtor (&local_TMIt129);
}
if (local_x != NULL)
{
zval_ptr_dtor (&local_x);
}
}
// function __construct($user = NULL)
// {
// 	$this->initialisefromuser($user);
// }
PHP_METHOD(ParserOptions, __construct)
{
zval* local_this = getThis();
zval* local_user = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
if (num_args <= 0)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[0] = default_value;
}
params[0]->refcount++;
if (local_user != NULL)
{
	zval_ptr_dtor (&local_user);
}
local_user = params[0];
}
// Function body
// $this->initialisefromuser($user);
{
     if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "initialisefromuser", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 110 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_user == NULL)
    {
      local_user = EG (uninitialized_zval_ptr);
      local_user->refcount++;
    }
  zval** p_arg = &local_user;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_user == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_user;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 110, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 1;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_user != NULL)
{
zval_ptr_dtor (&local_user);
}
}
// static function newfromuser($user)
// {
// 	$TLE60 = new parseroptions($user);
// 	return $TLE60;
// }
PHP_METHOD(ParserOptions, newfromuser)
{
zval* local_TLE60 = NULL;
zval* local_user = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_user != NULL)
{
	zval_ptr_dtor (&local_user);
}
local_user = params[0];
}
// Function body
// $TLE60 = new parseroptions($user);
{
	     if (local_TLE60 == NULL)
    {
      local_TLE60 = EG (uninitialized_zval_ptr);
      local_TLE60->refcount++;
    }
  zval** p_lhs = &local_TLE60;

   zval* lhs;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     lhs = *p_lhs;
     zval_dtor (lhs);
   }
   else
   {
     ALLOC_INIT_ZVAL (lhs);
     zval_ptr_dtor (p_lhs);
     *p_lhs = lhs;
   }

	zend_class_entry* ce;
	ce = zend_fetch_class ("parseroptions", strlen("parseroptions"), ZEND_FETCH_CLASS_DEFAULT TSRMLS_CC);
	object_init_ex(lhs, ce); 
    if (local_TLE60 == NULL)
    {
      local_TLE60 = EG (uninitialized_zval_ptr);
      local_TLE60->refcount++;
    }
  zval** p_obj = &local_TLE60;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   int has_constructor = initialize_constructor_call (&fci_object, &fcic_object, p_obj, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 118 TSRMLS_CC);
   // TODO: We pass in __construct to call_function, but in general it may be a
   // a different name (the name of the class, for example).
   // However, \call_function does not actually use this name at all atm.
   if(has_constructor)
   {
        zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_user == NULL)
    {
      local_user = EG (uninitialized_zval_ptr);
      local_user->refcount++;
    }
  zval** p_arg = &local_user;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_user == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_user;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 118, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 1;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

   }
phc_check_invariants (TSRMLS_C);
}
// return $TLE60;
{
     zval* rhs;
  if (local_TLE60 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE60;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE60 != NULL)
{
zval_ptr_dtor (&local_TLE60);
}
if (local_user != NULL)
{
zval_ptr_dtor (&local_user);
}
}
// function initialisefromuser($userInput)
// {
// 	global $wgUseTeX;
// 	global $wgUseDynamicDates;
// 	global $wgInterwikiMagic;
// 	global $wgAllowExternalImages;
// 	global $wgAllowExternalImagesFrom;
// 	global $wgEnableImageWhitelist;
// 	global $wgAllowSpecialInclusion;
// 	global $wgMaxArticleSize;
// 	global $wgMaxPPNodeCount;
// 	global $wgMaxTemplateDepth;
// 	global $wgMaxPPExpandDepth;
// 	global $wgCleanSignatures;
// 	global $wgExternalLinkTarget;
// 	$fname = 'ParserOptions::initialiseFromUser';
// 	wfprofilein($fname);
// 	$TLE61 = !$userInput;
// 	if (TLE61) goto L215 else goto L216;
// L215:
// 	global $wgUser;
// 	$TLE62 = isset($wgUser);
// 	if (TLE62) goto L212 else goto L213;
// L212:
// 	$user = $wgUser;
// 	goto L214;
// L213:
// 	$user = new user();
// 	goto L214;
// L214:
// 	goto L217;
// L216:
// 	$user =& $userInput;
// 	goto L217;
// L217:
// 	$this->mUser = $user;
// 	$this->mUseTeX = $wgUseTeX;
// 	$this->mUseDynamicDates = $wgUseDynamicDates;
// 	$this->mInterwikiMagic = $wgInterwikiMagic;
// 	$this->mAllowExternalImages = $wgAllowExternalImages;
// 	$this->mAllowExternalImagesFrom = $wgAllowExternalImagesFrom;
// 	$this->mEnableImageWhitelist = $wgEnableImageWhitelist;
// 	$TLE63 = NULL;
// 	$this->mSkin = $TLE63;
// 	$TLE64 = NULL;
// 	$this->mDateFormat = $TLE64;
// 	$TLE65 = True;
// 	$this->mEditSection = $TLE65;
// 	$TLE66 = 'numberheadings';
// 	$TLE67 = $user->getoption($TLE66);
// 	$this->mNumberHeadings = $TLE67;
// 	$this->mAllowSpecialInclusion = $wgAllowSpecialInclusion;
// 	$TLE68 = False;
// 	$this->mTidy = $TLE68;
// 	$TLE69 = False;
// 	$this->mInterfaceMessage = $TLE69;
// 	$TLE70 = NULL;
// 	$this->mTargetLanguage = $TLE70;
// 	$TLE71 = 1024;
// 	$TLE72 = ($wgMaxArticleSize * $TLE71);
// 	$this->mMaxIncludeSize = $TLE72;
// 	$this->mMaxPPNodeCount = $wgMaxPPNodeCount;
// 	$this->mMaxPPExpandDepth = $wgMaxPPExpandDepth;
// 	$this->mMaxTemplateDepth = $wgMaxTemplateDepth;
// 	$TLE73 = True;
// 	$this->mRemoveComments = $TLE73;
// 	$TLE74 = 'Parser';
// 	$TLE75 = 'statelessFetchTemplate';
// 	unset($TSa76);
// 	$TSa76 = (array) $TSa76;
// 	$TSa76[] = $TLE74;
// 	$TSa76[] = $TLE75;
// 	$this->mTemplateCallback = $TSa76;
// 	$TLE77 = False;
// 	$this->mEnableLimitReport = $TLE77;
// 	$this->mCleanSignatures = $wgCleanSignatures;
// 	$this->mExternalLinkTarget = $wgExternalLinkTarget;
// 	$TLE78 = False;
// 	$this->mIsPreview = $TLE78;
// 	$TLE79 = False;
// 	$this->mIsSectionPreview = $TLE79;
// 	wfprofileout($fname);
// }
PHP_METHOD(ParserOptions, initialisefromuser)
{
zval* local_TLE61 = NULL;
zval* local_TLE62 = NULL;
zval* local_TLE63 = NULL;
zval* local_TLE64 = NULL;
zval* local_TLE65 = NULL;
zval* local_TLE66 = NULL;
zval* local_TLE67 = NULL;
zval* local_TLE68 = NULL;
zval* local_TLE69 = NULL;
zval* local_TLE70 = NULL;
zval* local_TLE71 = NULL;
zval* local_TLE72 = NULL;
zval* local_TLE73 = NULL;
zval* local_TLE74 = NULL;
zval* local_TLE75 = NULL;
zval* local_TLE77 = NULL;
zval* local_TLE78 = NULL;
zval* local_TLE79 = NULL;
zval* local_TSa76 = NULL;
zval* local_fname = NULL;
zval* local_this = getThis();
zval* local_user = NULL;
zval* local_userInput = NULL;
zval* local_wgAllowExternalImages = NULL;
zval* local_wgAllowExternalImagesFrom = NULL;
zval* local_wgAllowSpecialInclusion = NULL;
zval* local_wgCleanSignatures = NULL;
zval* local_wgEnableImageWhitelist = NULL;
zval* local_wgExternalLinkTarget = NULL;
zval* local_wgInterwikiMagic = NULL;
zval* local_wgMaxArticleSize = NULL;
zval* local_wgMaxPPExpandDepth = NULL;
zval* local_wgMaxPPNodeCount = NULL;
zval* local_wgMaxTemplateDepth = NULL;
zval* local_wgUseDynamicDates = NULL;
zval* local_wgUseTeX = NULL;
zval* local_wgUser = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_userInput != NULL)
{
	zval_ptr_dtor (&local_userInput);
}
local_userInput = params[0];
}
// Function body
// global $wgUseTeX;
{
     if (local_wgUseTeX == NULL)
    {
      local_wgUseTeX = EG (uninitialized_zval_ptr);
      local_wgUseTeX->refcount++;
    }
  zval** p_local = &local_wgUseTeX;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgUseTeX", 8 + 1, 3374094529u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgUseDynamicDates;
{
     if (local_wgUseDynamicDates == NULL)
    {
      local_wgUseDynamicDates = EG (uninitialized_zval_ptr);
      local_wgUseDynamicDates->refcount++;
    }
  zval** p_local = &local_wgUseDynamicDates;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgUseDynamicDates", 17 + 1, 3762790118u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgInterwikiMagic;
{
     if (local_wgInterwikiMagic == NULL)
    {
      local_wgInterwikiMagic = EG (uninitialized_zval_ptr);
      local_wgInterwikiMagic->refcount++;
    }
  zval** p_local = &local_wgInterwikiMagic;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgInterwikiMagic", 16 + 1, 134681658u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgAllowExternalImages;
{
     if (local_wgAllowExternalImages == NULL)
    {
      local_wgAllowExternalImages = EG (uninitialized_zval_ptr);
      local_wgAllowExternalImages->refcount++;
    }
  zval** p_local = &local_wgAllowExternalImages;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgAllowExternalImages", 21 + 1, 1826481435u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgAllowExternalImagesFrom;
{
     if (local_wgAllowExternalImagesFrom == NULL)
    {
      local_wgAllowExternalImagesFrom = EG (uninitialized_zval_ptr);
      local_wgAllowExternalImagesFrom->refcount++;
    }
  zval** p_local = &local_wgAllowExternalImagesFrom;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgAllowExternalImagesFrom", 25 + 1, 3395557199u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgEnableImageWhitelist;
{
     if (local_wgEnableImageWhitelist == NULL)
    {
      local_wgEnableImageWhitelist = EG (uninitialized_zval_ptr);
      local_wgEnableImageWhitelist->refcount++;
    }
  zval** p_local = &local_wgEnableImageWhitelist;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgEnableImageWhitelist", 22 + 1, 2100864554u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgAllowSpecialInclusion;
{
     if (local_wgAllowSpecialInclusion == NULL)
    {
      local_wgAllowSpecialInclusion = EG (uninitialized_zval_ptr);
      local_wgAllowSpecialInclusion->refcount++;
    }
  zval** p_local = &local_wgAllowSpecialInclusion;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgAllowSpecialInclusion", 23 + 1, 2904579383u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgMaxArticleSize;
{
     if (local_wgMaxArticleSize == NULL)
    {
      local_wgMaxArticleSize = EG (uninitialized_zval_ptr);
      local_wgMaxArticleSize->refcount++;
    }
  zval** p_local = &local_wgMaxArticleSize;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgMaxArticleSize", 16 + 1, 2649036712u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgMaxPPNodeCount;
{
     if (local_wgMaxPPNodeCount == NULL)
    {
      local_wgMaxPPNodeCount = EG (uninitialized_zval_ptr);
      local_wgMaxPPNodeCount->refcount++;
    }
  zval** p_local = &local_wgMaxPPNodeCount;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgMaxPPNodeCount", 16 + 1, 1800171672u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgMaxTemplateDepth;
{
     if (local_wgMaxTemplateDepth == NULL)
    {
      local_wgMaxTemplateDepth = EG (uninitialized_zval_ptr);
      local_wgMaxTemplateDepth->refcount++;
    }
  zval** p_local = &local_wgMaxTemplateDepth;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgMaxTemplateDepth", 18 + 1, 1695740730u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgMaxPPExpandDepth;
{
     if (local_wgMaxPPExpandDepth == NULL)
    {
      local_wgMaxPPExpandDepth = EG (uninitialized_zval_ptr);
      local_wgMaxPPExpandDepth->refcount++;
    }
  zval** p_local = &local_wgMaxPPExpandDepth;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgMaxPPExpandDepth", 18 + 1, 3016385150u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgCleanSignatures;
{
     if (local_wgCleanSignatures == NULL)
    {
      local_wgCleanSignatures = EG (uninitialized_zval_ptr);
      local_wgCleanSignatures->refcount++;
    }
  zval** p_local = &local_wgCleanSignatures;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgCleanSignatures", 17 + 1, 977135819u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// global $wgExternalLinkTarget;
{
     if (local_wgExternalLinkTarget == NULL)
    {
      local_wgExternalLinkTarget = EG (uninitialized_zval_ptr);
      local_wgExternalLinkTarget->refcount++;
    }
  zval** p_local = &local_wgExternalLinkTarget;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgExternalLinkTarget", 20 + 1, 4023362555u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// $fname = 'ParserOptions::initialiseFromUser';
{
        if (local_fname == NULL)
    {
      local_fname = EG (uninitialized_zval_ptr);
      local_fname->refcount++;
    }
  zval** p_lhs = &local_fname;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "ParserOptions::initialiseFromUser", 33, 1);

phc_check_invariants (TSRMLS_C);
}
// wfprofilein($fname);
{
   initialize_function_call (&wfprofilein_fci, &wfprofilein_fcic, "wfprofilein", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 128 TSRMLS_CC);
      zend_function* signature = wfprofilein_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_fname == NULL)
    {
      local_fname = EG (uninitialized_zval_ptr);
      local_fname->refcount++;
    }
  zval** p_arg = &local_fname;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_fname == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_fname;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 128, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfprofilein_fci.param_count;
   zval*** params_save = wfprofilein_fci.params;
   zval** retval_save = wfprofilein_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfprofilein_fci.params = args_ind;
   wfprofilein_fci.param_count = 1;
   wfprofilein_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfprofilein_fci, &wfprofilein_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfprofilein_fci.params = params_save;
   wfprofilein_fci.param_count = param_count_save;
   wfprofilein_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE61 = !$userInput;
{
     if (local_TLE61 == NULL)
    {
      local_TLE61 = EG (uninitialized_zval_ptr);
      local_TLE61->refcount++;
    }
  zval** p_lhs = &local_TLE61;

     zval* rhs;
  if (local_userInput == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_userInput;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE61) goto L215 else goto L216;
{
     zval* p_cond;
  if (local_TLE61 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE61;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L215;
   else
      goto L216;
phc_check_invariants (TSRMLS_C);
}
// L215:
L215:;
// global $wgUser;
{
     if (local_wgUser == NULL)
    {
      local_wgUser = EG (uninitialized_zval_ptr);
      local_wgUser->refcount++;
    }
  zval** p_local = &local_wgUser;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgUser", 6 + 1, 3264750242u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// $TLE62 = isset($wgUser);
{
if (local_TLE62 == NULL)
{
local_TLE62 = EG (uninitialized_zval_ptr);
local_TLE62->refcount++;
}
zval** p_lhs = &local_TLE62;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
ZVAL_BOOL(value, local_wgUser != NULL && !ZVAL_IS_NULL(local_wgUser));
phc_check_invariants (TSRMLS_C);
}
// if (TLE62) goto L212 else goto L213;
{
     zval* p_cond;
  if (local_TLE62 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE62;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L212;
   else
      goto L213;
phc_check_invariants (TSRMLS_C);
}
// L212:
L212:;
// $user = $wgUser;
{
    if (local_user == NULL)
    {
      local_user = EG (uninitialized_zval_ptr);
      local_user->refcount++;
    }
  zval** p_lhs = &local_user;

    zval* rhs;
  if (local_wgUser == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgUser;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L214;
{
goto L214;
phc_check_invariants (TSRMLS_C);
}
// L213:
L213:;
// $user = new user();
{
	     if (local_user == NULL)
    {
      local_user = EG (uninitialized_zval_ptr);
      local_user->refcount++;
    }
  zval** p_lhs = &local_user;

   zval* lhs;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     lhs = *p_lhs;
     zval_dtor (lhs);
   }
   else
   {
     ALLOC_INIT_ZVAL (lhs);
     zval_ptr_dtor (p_lhs);
     *p_lhs = lhs;
   }

	zend_class_entry* ce;
	ce = zend_fetch_class ("user", strlen("user"), ZEND_FETCH_CLASS_DEFAULT TSRMLS_CC);
	object_init_ex(lhs, ce); 
    if (local_user == NULL)
    {
      local_user = EG (uninitialized_zval_ptr);
      local_user->refcount++;
    }
  zval** p_obj = &local_user;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   int has_constructor = initialize_constructor_call (&fci_object, &fcic_object, p_obj, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 134 TSRMLS_CC);
   // TODO: We pass in __construct to call_function, but in general it may be a
   // a different name (the name of the class, for example).
   // However, \call_function does not actually use this name at all atm.
   if(has_constructor)
   {
        zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 134, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

   }
phc_check_invariants (TSRMLS_C);
}
// goto L214;
{
goto L214;
phc_check_invariants (TSRMLS_C);
}
// L214:
L214:;
// goto L217;
{
goto L217;
phc_check_invariants (TSRMLS_C);
}
// L216:
L216:;
// $user =& $userInput;
{
    if (local_user == NULL)
    {
      local_user = EG (uninitialized_zval_ptr);
      local_user->refcount++;
    }
  zval** p_lhs = &local_user;

    if (local_userInput == NULL)
    {
      local_userInput = EG (uninitialized_zval_ptr);
      local_userInput->refcount++;
    }
  zval** p_rhs = &local_userInput;

  sep_copy_on_write (p_rhs);
  copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L217;
{
goto L217;
phc_check_invariants (TSRMLS_C);
}
// L217:
L217:;
// $this->mUser = $user;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_user == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_user;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUser", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mUseTeX = $wgUseTeX;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgUseTeX == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgUseTeX;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUseTeX", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mUseDynamicDates = $wgUseDynamicDates;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgUseDynamicDates == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgUseDynamicDates;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mUseDynamicDates", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mInterwikiMagic = $wgInterwikiMagic;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgInterwikiMagic == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgInterwikiMagic;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mInterwikiMagic", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mAllowExternalImages = $wgAllowExternalImages;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgAllowExternalImages == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgAllowExternalImages;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowExternalImages", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mAllowExternalImagesFrom = $wgAllowExternalImagesFrom;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgAllowExternalImagesFrom == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgAllowExternalImagesFrom;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowExternalImagesFrom", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mEnableImageWhitelist = $wgEnableImageWhitelist;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgEnableImageWhitelist == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgEnableImageWhitelist;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEnableImageWhitelist", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE63 = NULL;
{
        if (local_TLE63 == NULL)
    {
      local_TLE63 = EG (uninitialized_zval_ptr);
      local_TLE63->refcount++;
    }
  zval** p_lhs = &local_TLE63;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
// $this->mSkin = $TLE63;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE63 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE63;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mSkin", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE64 = NULL;
{
        if (local_TLE64 == NULL)
    {
      local_TLE64 = EG (uninitialized_zval_ptr);
      local_TLE64->refcount++;
    }
  zval** p_lhs = &local_TLE64;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
// $this->mDateFormat = $TLE64;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE64 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE64;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mDateFormat", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE65 = True;
{
        if (local_TLE65 == NULL)
    {
      local_TLE65 = EG (uninitialized_zval_ptr);
      local_TLE65->refcount++;
    }
  zval** p_lhs = &local_TLE65;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $this->mEditSection = $TLE65;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE65 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE65;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEditSection", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE66 = 'numberheadings';
{
        if (local_TLE66 == NULL)
    {
      local_TLE66 = EG (uninitialized_zval_ptr);
      local_TLE66->refcount++;
    }
  zval** p_lhs = &local_TLE66;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "numberheadings", 14, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE67 = $user->getoption($TLE66);
{
     if (local_user == NULL)
    {
      local_user = EG (uninitialized_zval_ptr);
      local_user->refcount++;
    }
  zval** p_obj = &local_user;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getoption", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 151 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE66 == NULL)
    {
      local_TLE66 = EG (uninitialized_zval_ptr);
      local_TLE66->refcount++;
    }
  zval** p_arg = &local_TLE66;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE66 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE66;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 151, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 1;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE67 == NULL)
    {
      local_TLE67 = EG (uninitialized_zval_ptr);
      local_TLE67->refcount++;
    }
  zval** p_lhs = &local_TLE67;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $this->mNumberHeadings = $TLE67;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE67 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE67;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mNumberHeadings", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mAllowSpecialInclusion = $wgAllowSpecialInclusion;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgAllowSpecialInclusion == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgAllowSpecialInclusion;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mAllowSpecialInclusion", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE68 = False;
{
        if (local_TLE68 == NULL)
    {
      local_TLE68 = EG (uninitialized_zval_ptr);
      local_TLE68->refcount++;
    }
  zval** p_lhs = &local_TLE68;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $this->mTidy = $TLE68;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE68 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE68;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTidy", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE69 = False;
{
        if (local_TLE69 == NULL)
    {
      local_TLE69 = EG (uninitialized_zval_ptr);
      local_TLE69->refcount++;
    }
  zval** p_lhs = &local_TLE69;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $this->mInterfaceMessage = $TLE69;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE69 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE69;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mInterfaceMessage", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE70 = NULL;
{
        if (local_TLE70 == NULL)
    {
      local_TLE70 = EG (uninitialized_zval_ptr);
      local_TLE70->refcount++;
    }
  zval** p_lhs = &local_TLE70;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
// $this->mTargetLanguage = $TLE70;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE70 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE70;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTargetLanguage", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE71 = 1024;
{
        if (local_TLE71 == NULL)
    {
      local_TLE71 = EG (uninitialized_zval_ptr);
      local_TLE71->refcount++;
    }
  zval** p_lhs = &local_TLE71;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1024);

phc_check_invariants (TSRMLS_C);
}
// $TLE72 = ($wgMaxArticleSize * $TLE71);
{
    if (local_TLE72 == NULL)
    {
      local_TLE72 = EG (uninitialized_zval_ptr);
      local_TLE72->refcount++;
    }
  zval** p_lhs = &local_TLE72;

    zval* left;
  if (local_wgMaxArticleSize == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_wgMaxArticleSize;

    zval* right;
  if (local_TLE71 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE71;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  mul_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $this->mMaxIncludeSize = $TLE72;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE72 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE72;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxIncludeSize", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mMaxPPNodeCount = $wgMaxPPNodeCount;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgMaxPPNodeCount == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgMaxPPNodeCount;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxPPNodeCount", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mMaxPPExpandDepth = $wgMaxPPExpandDepth;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgMaxPPExpandDepth == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgMaxPPExpandDepth;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxPPExpandDepth", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mMaxTemplateDepth = $wgMaxTemplateDepth;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgMaxTemplateDepth == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgMaxTemplateDepth;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mMaxTemplateDepth", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE73 = True;
{
        if (local_TLE73 == NULL)
    {
      local_TLE73 = EG (uninitialized_zval_ptr);
      local_TLE73->refcount++;
    }
  zval** p_lhs = &local_TLE73;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $this->mRemoveComments = $TLE73;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE73 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE73;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mRemoveComments", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE74 = 'Parser';
{
        if (local_TLE74 == NULL)
    {
      local_TLE74 = EG (uninitialized_zval_ptr);
      local_TLE74->refcount++;
    }
  zval** p_lhs = &local_TLE74;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "Parser", 6, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE75 = 'statelessFetchTemplate';
{
        if (local_TLE75 == NULL)
    {
      local_TLE75 = EG (uninitialized_zval_ptr);
      local_TLE75->refcount++;
    }
  zval** p_lhs = &local_TLE75;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "statelessFetchTemplate", 22, 1);

phc_check_invariants (TSRMLS_C);
}
// unset($TSa76);
{
if (local_TSa76 != NULL)
{
zval_ptr_dtor (&local_TSa76);
local_TSa76 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa76 = (array) $TSa76;
{
      if (local_TSa76 == NULL)
    {
      local_TSa76 = EG (uninitialized_zval_ptr);
      local_TSa76->refcount++;
    }
  zval** p_lhs = &local_TSa76;

    zval* rhs;
  if (local_TSa76 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa76;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa76[] = $TLE74;
{
     if (local_TSa76 == NULL)
    {
      local_TSa76 = EG (uninitialized_zval_ptr);
      local_TSa76->refcount++;
    }
  zval** p_array = &local_TSa76;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE74 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE74;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa76[] = $TLE75;
{
     if (local_TSa76 == NULL)
    {
      local_TSa76 = EG (uninitialized_zval_ptr);
      local_TSa76->refcount++;
    }
  zval** p_array = &local_TSa76;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE75 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE75;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $this->mTemplateCallback = $TSa76;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TSa76 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa76;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplateCallback", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE77 = False;
{
        if (local_TLE77 == NULL)
    {
      local_TLE77 = EG (uninitialized_zval_ptr);
      local_TLE77->refcount++;
    }
  zval** p_lhs = &local_TLE77;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $this->mEnableLimitReport = $TLE77;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE77 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE77;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mEnableLimitReport", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mCleanSignatures = $wgCleanSignatures;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgCleanSignatures == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgCleanSignatures;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCleanSignatures", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mExternalLinkTarget = $wgExternalLinkTarget;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_wgExternalLinkTarget == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_wgExternalLinkTarget;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mExternalLinkTarget", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE78 = False;
{
        if (local_TLE78 == NULL)
    {
      local_TLE78 = EG (uninitialized_zval_ptr);
      local_TLE78->refcount++;
    }
  zval** p_lhs = &local_TLE78;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $this->mIsPreview = $TLE78;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE78 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE78;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIsPreview", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $TLE79 = False;
{
        if (local_TLE79 == NULL)
    {
      local_TLE79 = EG (uninitialized_zval_ptr);
      local_TLE79->refcount++;
    }
  zval** p_lhs = &local_TLE79;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $this->mIsSectionPreview = $TLE79;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE79 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE79;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIsSectionPreview", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// wfprofileout($fname);
{
   initialize_function_call (&wfprofileout_fci, &wfprofileout_fcic, "wfprofileout", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 167 TSRMLS_CC);
      zend_function* signature = wfprofileout_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_fname == NULL)
    {
      local_fname = EG (uninitialized_zval_ptr);
      local_fname->refcount++;
    }
  zval** p_arg = &local_fname;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_fname == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_fname;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOptions.php", 167, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfprofileout_fci.param_count;
   zval*** params_save = wfprofileout_fci.params;
   zval** retval_save = wfprofileout_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfprofileout_fci.params = args_ind;
   wfprofileout_fci.param_count = 1;
   wfprofileout_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfprofileout_fci, &wfprofileout_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfprofileout_fci.params = params_save;
   wfprofileout_fci.param_count = param_count_save;
   wfprofileout_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

   

   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE61 != NULL)
{
zval_ptr_dtor (&local_TLE61);
}
if (local_TLE62 != NULL)
{
zval_ptr_dtor (&local_TLE62);
}
if (local_TLE63 != NULL)
{
zval_ptr_dtor (&local_TLE63);
}
if (local_TLE64 != NULL)
{
zval_ptr_dtor (&local_TLE64);
}
if (local_TLE65 != NULL)
{
zval_ptr_dtor (&local_TLE65);
}
if (local_TLE66 != NULL)
{
zval_ptr_dtor (&local_TLE66);
}
if (local_TLE67 != NULL)
{
zval_ptr_dtor (&local_TLE67);
}
if (local_TLE68 != NULL)
{
zval_ptr_dtor (&local_TLE68);
}
if (local_TLE69 != NULL)
{
zval_ptr_dtor (&local_TLE69);
}
if (local_TLE70 != NULL)
{
zval_ptr_dtor (&local_TLE70);
}
if (local_TLE71 != NULL)
{
zval_ptr_dtor (&local_TLE71);
}
if (local_TLE72 != NULL)
{
zval_ptr_dtor (&local_TLE72);
}
if (local_TLE73 != NULL)
{
zval_ptr_dtor (&local_TLE73);
}
if (local_TLE74 != NULL)
{
zval_ptr_dtor (&local_TLE74);
}
if (local_TLE75 != NULL)
{
zval_ptr_dtor (&local_TLE75);
}
if (local_TLE77 != NULL)
{
zval_ptr_dtor (&local_TLE77);
}
if (local_TLE78 != NULL)
{
zval_ptr_dtor (&local_TLE78);
}
if (local_TLE79 != NULL)
{
zval_ptr_dtor (&local_TLE79);
}
if (local_TSa76 != NULL)
{
zval_ptr_dtor (&local_TSa76);
}
if (local_fname != NULL)
{
zval_ptr_dtor (&local_fname);
}
if (local_user != NULL)
{
zval_ptr_dtor (&local_user);
}
if (local_userInput != NULL)
{
zval_ptr_dtor (&local_userInput);
}
if (local_wgAllowExternalImages != NULL)
{
zval_ptr_dtor (&local_wgAllowExternalImages);
}
if (local_wgAllowExternalImagesFrom != NULL)
{
zval_ptr_dtor (&local_wgAllowExternalImagesFrom);
}
if (local_wgAllowSpecialInclusion != NULL)
{
zval_ptr_dtor (&local_wgAllowSpecialInclusion);
}
if (local_wgCleanSignatures != NULL)
{
zval_ptr_dtor (&local_wgCleanSignatures);
}
if (local_wgEnableImageWhitelist != NULL)
{
zval_ptr_dtor (&local_wgEnableImageWhitelist);
}
if (local_wgExternalLinkTarget != NULL)
{
zval_ptr_dtor (&local_wgExternalLinkTarget);
}
if (local_wgInterwikiMagic != NULL)
{
zval_ptr_dtor (&local_wgInterwikiMagic);
}
if (local_wgMaxArticleSize != NULL)
{
zval_ptr_dtor (&local_wgMaxArticleSize);
}
if (local_wgMaxPPExpandDepth != NULL)
{
zval_ptr_dtor (&local_wgMaxPPExpandDepth);
}
if (local_wgMaxPPNodeCount != NULL)
{
zval_ptr_dtor (&local_wgMaxPPNodeCount);
}
if (local_wgMaxTemplateDepth != NULL)
{
zval_ptr_dtor (&local_wgMaxTemplateDepth);
}
if (local_wgUseDynamicDates != NULL)
{
zval_ptr_dtor (&local_wgUseDynamicDates);
}
if (local_wgUseTeX != NULL)
{
zval_ptr_dtor (&local_wgUseTeX);
}
if (local_wgUser != NULL)
{
zval_ptr_dtor (&local_wgUser);
}
}
// ArgInfo structures (necessary to support compile time pass-by-reference)
ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getusetex_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getusedynamicdates_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getinterwikimagic_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getallowexternalimages_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getallowexternalimagesfrom_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getenableimagewhitelist_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_geteditsection_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getnumberheadings_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getallowspecialinclusion_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_gettidy_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getinterfacemessage_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_gettargetlanguage_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getmaxincludesize_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getmaxppnodecount_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getmaxtemplatedepth_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getremovecomments_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_gettemplatecallback_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getenablelimitreport_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getcleansignatures_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getexternallinktarget_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getispreview_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getissectionpreview_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getskin_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_getdateformat_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_gettimestamp_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setusetex_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setusedynamicdates_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setinterwikimagic_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setallowexternalimages_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setallowexternalimagesfrom_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setenableimagewhitelist_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setdateformat_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_seteditsection_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setnumberheadings_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setallowspecialinclusion_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_settidy_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setskin_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setinterfacemessage_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_settargetlanguage_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setmaxincludesize_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setmaxppnodecount_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setmaxtemplatedepth_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setremovecomments_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_settemplatecallback_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_enablelimitreport_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_settimestamp_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setcleansignatures_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setexternallinktarget_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setispreview_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_setissectionpreview_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "x")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions___construct_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "user")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_newfromuser_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "user")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOptions_initialisefromuser_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "userInput")
ZEND_END_ARG_INFO()

static function_entry ParserOptions_functions[] = {
PHP_ME(ParserOptions, getusetex, ParserOptions_getusetex_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getusedynamicdates, ParserOptions_getusedynamicdates_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getinterwikimagic, ParserOptions_getinterwikimagic_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getallowexternalimages, ParserOptions_getallowexternalimages_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getallowexternalimagesfrom, ParserOptions_getallowexternalimagesfrom_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getenableimagewhitelist, ParserOptions_getenableimagewhitelist_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, geteditsection, ParserOptions_geteditsection_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getnumberheadings, ParserOptions_getnumberheadings_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getallowspecialinclusion, ParserOptions_getallowspecialinclusion_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, gettidy, ParserOptions_gettidy_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getinterfacemessage, ParserOptions_getinterfacemessage_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, gettargetlanguage, ParserOptions_gettargetlanguage_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getmaxincludesize, ParserOptions_getmaxincludesize_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getmaxppnodecount, ParserOptions_getmaxppnodecount_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getmaxtemplatedepth, ParserOptions_getmaxtemplatedepth_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getremovecomments, ParserOptions_getremovecomments_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, gettemplatecallback, ParserOptions_gettemplatecallback_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getenablelimitreport, ParserOptions_getenablelimitreport_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getcleansignatures, ParserOptions_getcleansignatures_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getexternallinktarget, ParserOptions_getexternallinktarget_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getispreview, ParserOptions_getispreview_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getissectionpreview, ParserOptions_getissectionpreview_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getskin, ParserOptions_getskin_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, getdateformat, ParserOptions_getdateformat_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, gettimestamp, ParserOptions_gettimestamp_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setusetex, ParserOptions_setusetex_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setusedynamicdates, ParserOptions_setusedynamicdates_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setinterwikimagic, ParserOptions_setinterwikimagic_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setallowexternalimages, ParserOptions_setallowexternalimages_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setallowexternalimagesfrom, ParserOptions_setallowexternalimagesfrom_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setenableimagewhitelist, ParserOptions_setenableimagewhitelist_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setdateformat, ParserOptions_setdateformat_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, seteditsection, ParserOptions_seteditsection_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setnumberheadings, ParserOptions_setnumberheadings_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setallowspecialinclusion, ParserOptions_setallowspecialinclusion_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, settidy, ParserOptions_settidy_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setskin, ParserOptions_setskin_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setinterfacemessage, ParserOptions_setinterfacemessage_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, settargetlanguage, ParserOptions_settargetlanguage_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setmaxincludesize, ParserOptions_setmaxincludesize_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setmaxppnodecount, ParserOptions_setmaxppnodecount_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setmaxtemplatedepth, ParserOptions_setmaxtemplatedepth_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setremovecomments, ParserOptions_setremovecomments_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, settemplatecallback, ParserOptions_settemplatecallback_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, enablelimitreport, ParserOptions_enablelimitreport_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, settimestamp, ParserOptions_settimestamp_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setcleansignatures, ParserOptions_setcleansignatures_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setexternallinktarget, ParserOptions_setexternallinktarget_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setispreview, ParserOptions_setispreview_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, setissectionpreview, ParserOptions_setissectionpreview_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, __construct, ParserOptions___construct_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOptions, newfromuser, ParserOptions_newfromuser_arg_info, ZEND_ACC_PUBLIC | ZEND_ACC_STATIC)
PHP_ME(ParserOptions, initialisefromuser, ParserOptions_initialisefromuser_arg_info, ZEND_ACC_PUBLIC)
{ NULL, NULL, NULL }
};
// function __MAIN__()
// {
// }
PHP_FUNCTION(__MAIN__)
{
// Function body
// Method exit
end_of_function:__attribute__((unused));
}
// Module initialization
PHP_MINIT_FUNCTION(app)
{
{
zend_class_entry ce; // temp
zend_class_entry* ce_reg; // once registered, ce_ptr should be used
INIT_CLASS_ENTRY(ce, "ParserOptions", ParserOptions_functions);
ce_reg = zend_register_internal_class(&ce TSRMLS_CC);
ce_reg->type &= ~ZEND_INTERNAL_CLASS;
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mUseTeX", 7, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mUseDynamicDates", 16, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mInterwikiMagic", 15, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mAllowExternalImages", 20, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mAllowExternalImagesFrom", 24, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mEnableImageWhitelist", 21, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mSkin", 5, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mDateFormat", 11, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mEditSection", 12, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mNumberHeadings", 15, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mAllowSpecialInclusion", 22, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mTidy", 5, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mInterfaceMessage", 17, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mTargetLanguage", 15, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mMaxIncludeSize", 15, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mMaxPPNodeCount", 15, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mMaxPPExpandDepth", 17, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mMaxTemplateDepth", 17, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mRemoveComments", 15, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mTemplateCallback", 17, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mEnableLimitReport", 18, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mTimestamp", 10, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mExternalLinkTarget", 19, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mUser", 5, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mIsPreview", 10, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mIsSectionPreview", 17, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}}return SUCCESS;}// ArgInfo structures (necessary to support compile time pass-by-reference)
ZEND_BEGIN_ARG_INFO_EX(app___MAIN___arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

static function_entry app_functions[] = {
PHP_FE(__MAIN__, app___MAIN___arg_info)
{ NULL, NULL, NULL }
};
// Register the module itself with PHP
zend_module_entry app_module_entry = {
STANDARD_MODULE_HEADER, 
"app",
app_functions,
PHP_MINIT(app), /* MINIT */
NULL, /* MSHUTDOWN */
NULL, /* RINIT */
NULL, /* RSHUTDOWN */
NULL, /* MINFO */
"1.0",
STANDARD_MODULE_PROPERTIES
};
#include <sapi/embed/php_embed.h>
#include <signal.h>

void sighandler(int signum)
{
	switch(signum)
	{
		case SIGABRT:
			printf("SIGABRT received!\n");
			break;
		case SIGSEGV:
			printf("SIGSEGV received!\n");
			break;
		default:
			printf("Unknown signal received!\n");
			break;
	}

	printf("This could be a bug in phc. If you suspect it is, please email\n");
	printf("a bug report to phc-general@phpcompiler.org.\n");
	exit(-1);
}

int
main (int argc, char* argv[])
{
   int phc_exit_status;
   signal(SIGABRT, sighandler);
   signal(SIGSEGV, sighandler);

   TSRMLS_D;
   int dealloc_pools = 1;
   php_embed_init (argc, argv PTSRMLS_CC);
   zend_first_try
   {

      // initialize the phc runtime
      init_runtime();

      // load the compiled extension
      zend_startup_module (&app_module_entry);

      zval main_name;
      ZVAL_STRING (&main_name, "__MAIN__", NULL);

      zval retval;

      // Use standard errors, on stdout
      zend_alter_ini_entry ("report_zend_debug", sizeof("report_zend_debug"), "0", sizeof("0") - 1, PHP_INI_ALL, PHP_INI_STAGE_RUNTIME);
      zend_alter_ini_entry ("display_startup_errors", sizeof("display_startup_errors"), "1", sizeof("1") - 1, PHP_INI_ALL, PHP_INI_STAGE_RUNTIME);

      // initialize all the constants
saved_refcount = 0;

      // call __MAIN__
      int success = call_user_function( 
                   EG (function_table),
                   NULL,
                   &main_name,
                   &retval,
                   0,
                   NULL
                   TSRMLS_CC);

      assert (success == SUCCESS);

      // finalize the runtime
      finalize_runtime();

   }
   zend_catch
   {
		dealloc_pools = 0;
   }
   zend_end_try ();
   if (dealloc_pools)
   {
   }
   phc_exit_status = EG(exit_status);
   php_embed_shutdown (TSRMLS_C);

  return phc_exit_status;
}
