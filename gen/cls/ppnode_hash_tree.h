
#ifndef __GENERATED_cls_ppnode_hash_tree_h__
#define __GENERATED_cls_ppnode_hash_tree_h__

#include <cls/ppnode.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_Hash.php line 1336 */
class c_ppnode_hash_tree : virtual public c_ppnode {
  BEGIN_CLASS_MAP(ppnode_hash_tree)
    PARENT_CLASS(ppnode)
  END_CLASS_MAP(ppnode_hash_tree)
  DECLARE_CLASS(ppnode_hash_tree, PPNode_Hash_Tree, ObjectData)
  void init();
  public: Variant m_name;
  public: Variant m_firstChild;
  public: Variant m_lastChild;
  public: Variant m_nextSibling;
  public: void t___construct(Variant v_name);
  public: ObjectData *create(Variant v_name);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t___tostring();
  public: static Object ti_newwithtext(const char* cls, CVarRef v_name, CVarRef v_text);
  public: void t_addchild(CVarRef v_node);
  public: p_ppnode_hash_array t_getchildren();
  public: Variant t_getfirstchild();
  public: Variant t_getnextsibling();
  public: Array t_getchildrenoftype(CVarRef v_name);
  public: bool t_getlength();
  public: bool t_item(CVarRef v_i);
  public: Variant t_getname();
  public: Variant t_splitarg();
  public: Variant t_splitext();
  public: Variant t_splitheading();
  public: Variant t_splittemplate();
  public: static Object t_newwithtext(CVarRef v_name, CVarRef v_text) { return ti_newwithtext("ppnode_hash_tree", v_name, v_text); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppnode_hash_tree_h__
