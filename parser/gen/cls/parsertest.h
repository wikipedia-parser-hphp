
#ifndef __GENERATED_cls_parsertest_h__
#define __GENERATED_cls_parsertest_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 43602 */
class c_parsertest : virtual public ObjectData {
  BEGIN_CLASS_MAP(parsertest)
  END_CLASS_MAP(parsertest)
  DECLARE_CLASS(parsertest, ParserTest, ObjectData)
  void init();
  public: Variant m_color;
  public: Variant m_showOutput;
  public: Variant m_useTemporaryTables;
  public: Variant m_databaseSetupDone;
  public: Variant m_oldTablePrefix;
  public: Variant m_maxFuzzTestLength;
  public: Variant m_fuzzSeed;
  public: Variant m_memoryLimit;
  public: void t_parsertest();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_chomp(CVarRef v_s);
  public: void t_fuzztest(CVarRef v_filenames);
  public: String t_getfuzzinput(CVarRef v_filenames);
  public: Variant t_getmemorybreakdown();
  public: void t_abort();
  public: bool t_runtestsfromfiles(CVarRef v_filenames);
  public: bool t_runfile(CVarRef v_filename);
  public: Variant t_getparser();
  public: Variant t_runtest(Variant v_desc, Variant v_input, Variant v_result, CVarRef v_opts);
  public: static Variant ti_getoptionvalue(const char* cls, CStrRef v_regex, CVarRef v_opts, CVarRef v_default);
  public: void t_setupglobals(CVarRef v_opts = "");
  public: Variant t_listtables();
  public: void t_setupdatabase();
  public: void t_changeprefix(CVarRef v_prefix);
  public: void t_changelbprefix(Object v_lb, CVarRef v_prefix);
  public: void t_changedbprefix(CVarRef v_db, Variant v_prefix);
  public: void t_teardowndatabase();
  public: String t_setupuploaddir();
  public: void t_teardownglobals();
  public: void t_teardownuploaddir(CVarRef v_dir);
  public: static void ti_deletefiles(const char* cls, CArrRef v_files);
  public: static void ti_deletedirs(const char* cls, CArrRef v_dirs);
  public: void t_showtesting(CVarRef v_desc);
  public: bool t_showsuccess(CVarRef v_desc);
  public: bool t_showfailure(CVarRef v_desc, CVarRef v_result, CVarRef v_html);
  public: Variant t_quickdiff(CVarRef v_input, CVarRef v_output, CStrRef v_inFileTail = "expected", CStrRef v_outFileTail = "actual");
  public: void t_dumptofile(CVarRef v_data, CStrRef v_filename);
  public: Variant t_colordiff(CStrRef v_text);
  public: void t_showrunfile(CVarRef v_path);
  public: void t_addarticle(CVarRef v_name, CVarRef v_text, int64 v_line);
  public: void t_requirehook(CVarRef v_name);
  public: void t_requirefunctionhook(CVarRef v_name);
  public: Variant t_tidy(Variant v_text);
  public: bool t_wellformed(CVarRef v_text);
  public: String t_extractfragment(CStrRef v_text, int v_position);
  public: static void t_deletefiles(CArrRef v_files) { ti_deletefiles("parsertest", v_files); }
  public: static Variant t_getoptionvalue(CStrRef v_regex, CVarRef v_opts, CVarRef v_default) { return ti_getoptionvalue("parsertest", v_regex, v_opts, v_default); }
  public: static void t_deletedirs(CArrRef v_dirs) { ti_deletedirs("parsertest", v_dirs); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parsertest_h__
