
#ifndef __GENERATED_cls_ppdstack_h__
#define __GENERATED_cls_ppdstack_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_DOM.php line 642 */
class c_ppdstack : virtual public ObjectData {
  BEGIN_CLASS_MAP(ppdstack)
  END_CLASS_MAP(ppdstack)
  DECLARE_CLASS(ppdstack, PPDStack, ObjectData)
  void init();
  public: Variant m_stack;
  public: Variant m_rootAccum;
  public: Variant m_top;
  public: Variant m_out;
  public: Variant m_elementClass;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: int t_count();
  public: Variant t_getaccum();
  public: Variant t_getcurrentpart();
  public: void t_push(Variant v_data);
  public: Variant t_pop();
  public: void t_addpart(Variant v_s = "");
  public: Variant t_getflags();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppdstack_h__
