
#ifndef __GENERATED_cls_ppdstackelement_h__
#define __GENERATED_cls_ppdstackelement_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 34398 */
class c_ppdstackelement : virtual public ObjectData {
  BEGIN_CLASS_MAP(ppdstackelement)
  END_CLASS_MAP(ppdstackelement)
  DECLARE_CLASS(ppdstackelement, PPDStackElement, ObjectData)
  void init();
  public: Variant m_open;
  public: Variant m_close;
  public: Variant m_count;
  public: Variant m_parts;
  public: Variant m_lineStart;
  public: Variant m_partClass;
  public: void t___construct(Variant v_data = ScalarArrays::sa_[0]);
  public: ObjectData *create(Variant v_data = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_getaccum();
  public: void t_addpart(Variant v_s = "");
  public: Variant t_getcurrentpart();
  public: Array t_getflags();
  public: Variant t_breaksyntax(Variant v_openingCount = false);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppdstackelement_h__
