
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "wgServer",
    "wgRegisterInternalExternals",
    "wgCacheEpoch",
    "wgUseTeX",
    "wgUseDynamicDates",
    "wgInterwikiMagic",
    "wgAllowExternalImages",
    "wgAllowExternalImagesFrom",
    "wgEnableImageWhitelist",
    "wgAllowSpecialInclusion",
    "wgMaxArticleSize",
    "wgMaxPPNodeCount",
    "wgMaxTemplateDepth",
    "wgMaxPPExpandDepth",
    "wgCleanSignatures",
    "wgExternalLinkTarget",
    "wgUser",
    "wgHooks",
    "wgMemc",
    "wgPreprocessorCacheThreshold",
    "wgContLang",
    "wgLinkHolderBatchSize",
    "wgUseTidy",
    "wgAlwaysUseTidy",
    "wgExpensiveParserFunctionLimit",
    "wgLang",
    "wgRawHtml",
    "wgNoFollowLinks",
    "wgNoFollowNsExceptions",
    "wgNoFollowDomainExceptions",
    "wgSitename",
    "wgServerName",
    "wgScriptPath",
    "wgStylePath",
    "wgLocaltimezone",
    "wgContLanguageCode",
    "wgNonincludableNamespaces",
    "wgEnableScaryTranscluding",
    "wgTranscludeCacheExpiry",
    "wgMaxTocLevel",
    "wgEnforceHtmlIds",
    "wgLegalTitleChars",
    "wgMaxSigChars",
    "wgTitle",
    "wgCategoryPrefixedDefaultSortkey",
    "parserMemc",
    "wgRequest",
    "wgParserCacheExpireTime",
    "wgTidyInternal",
    "wgTidyConf",
    "wgTidyBin",
    "wgTidyOpts",
    "IP",
    "wgDebugTidy",
    "wgAllowDisplayTitle",
    "wgAllowSlowParserFunctions",
    "wgRestrictDisplayTitle",
  };
  if (idx >= 0 && idx < 69) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(wgServer);
    case 13: return GV(wgRegisterInternalExternals);
    case 14: return GV(wgCacheEpoch);
    case 15: return GV(wgUseTeX);
    case 16: return GV(wgUseDynamicDates);
    case 17: return GV(wgInterwikiMagic);
    case 18: return GV(wgAllowExternalImages);
    case 19: return GV(wgAllowExternalImagesFrom);
    case 20: return GV(wgEnableImageWhitelist);
    case 21: return GV(wgAllowSpecialInclusion);
    case 22: return GV(wgMaxArticleSize);
    case 23: return GV(wgMaxPPNodeCount);
    case 24: return GV(wgMaxTemplateDepth);
    case 25: return GV(wgMaxPPExpandDepth);
    case 26: return GV(wgCleanSignatures);
    case 27: return GV(wgExternalLinkTarget);
    case 28: return GV(wgUser);
    case 29: return GV(wgHooks);
    case 30: return GV(wgMemc);
    case 31: return GV(wgPreprocessorCacheThreshold);
    case 32: return GV(wgContLang);
    case 33: return GV(wgLinkHolderBatchSize);
    case 34: return GV(wgUseTidy);
    case 35: return GV(wgAlwaysUseTidy);
    case 36: return GV(wgExpensiveParserFunctionLimit);
    case 37: return GV(wgLang);
    case 38: return GV(wgRawHtml);
    case 39: return GV(wgNoFollowLinks);
    case 40: return GV(wgNoFollowNsExceptions);
    case 41: return GV(wgNoFollowDomainExceptions);
    case 42: return GV(wgSitename);
    case 43: return GV(wgServerName);
    case 44: return GV(wgScriptPath);
    case 45: return GV(wgStylePath);
    case 46: return GV(wgLocaltimezone);
    case 47: return GV(wgContLanguageCode);
    case 48: return GV(wgNonincludableNamespaces);
    case 49: return GV(wgEnableScaryTranscluding);
    case 50: return GV(wgTranscludeCacheExpiry);
    case 51: return GV(wgMaxTocLevel);
    case 52: return GV(wgEnforceHtmlIds);
    case 53: return GV(wgLegalTitleChars);
    case 54: return GV(wgMaxSigChars);
    case 55: return GV(wgTitle);
    case 56: return GV(wgCategoryPrefixedDefaultSortkey);
    case 57: return GV(parserMemc);
    case 58: return GV(wgRequest);
    case 59: return GV(wgParserCacheExpireTime);
    case 60: return GV(wgTidyInternal);
    case 61: return GV(wgTidyConf);
    case 62: return GV(wgTidyBin);
    case 63: return GV(wgTidyOpts);
    case 64: return GV(IP);
    case 65: return GV(wgDebugTidy);
    case 66: return GV(wgAllowDisplayTitle);
    case 67: return GV(wgAllowSlowParserFunctions);
    case 68: return GV(wgRestrictDisplayTitle);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
