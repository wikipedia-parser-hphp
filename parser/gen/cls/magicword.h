
#ifndef __GENERATED_cls_magicword_h__
#define __GENERATED_cls_magicword_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 24690 */
class c_magicword : virtual public ObjectData {
  BEGIN_CLASS_MAP(magicword)
  END_CLASS_MAP(magicword)
  DECLARE_CLASS(magicword, MagicWord, ObjectData)
  void init();
  public: Variant m_mId;
  public: Variant m_mSynonyms;
  public: Variant m_mCaseSensitive;
  public: Variant m_mRegex;
  public: Variant m_mRegexStart;
  public: Variant m_mBaseRegex;
  public: Variant m_mVariableRegex;
  public: Variant m_mModified;
  public: Variant m_mFound;
  public: void t___construct(Variant v_id = 0LL, Variant v_syn = "", Variant v_cs = false);
  public: ObjectData *create(Variant v_id = 0LL, Variant v_syn = "", Variant v_cs = false);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static Variant ti_get(const char* cls, CVarRef v_id);
  public: static Variant ti_getvariableids(const char* cls);
  public: static Variant ti_getcachettl(const char* cls, CVarRef v_id);
  public: static Variant ti_getdoubleunderscorearray(const char* cls);
  public: void t_load(CVarRef v_id);
  public: void t_initregex();
  public: Variant t_getregex();
  public: Variant t_getregexcase();
  public: Variant t_getregexstart();
  public: Variant t_getbaseregex();
  public: Variant t_match(CVarRef v_text);
  public: Variant t_matchstart(CVarRef v_text);
  public: Variant t_matchvariablestarttoend(CVarRef v_text);
  public: Variant t_matchandremove(Variant v_text);
  public: Variant t_matchstartandremove(Variant v_text);
  public: String t_pregremoveandrecord();
  public: Variant t_replace(CVarRef v_replacement, CVarRef v_subject, CVarRef v_limit = -1LL);
  public: Variant t_substitutecallback(CVarRef v_text, CVarRef v_callback);
  public: Variant t_getvariableregex();
  public: Variant t_getvariablestarttoendregex();
  public: Variant t_getsynonym(CVarRef v_i);
  public: Variant t_getsynonyms();
  public: Variant t_getwasmodified();
  public: bool t_replacemultiple(CArrRef v_magicarr, CVarRef v_subject, Variant v_result);
  public: void t_addtoarray(Variant v_array, CVarRef v_value);
  public: Variant t_iscasesensitive();
  public: Variant t_getid();
  public: static Variant t_getdoubleunderscorearray() { return ti_getdoubleunderscorearray("magicword"); }
  public: static Variant t_getcachettl(CVarRef v_id) { return ti_getcachettl("magicword", v_id); }
  public: static Variant t_getvariableids() { return ti_getvariableids("magicword"); }
  public: static Variant t_get(CVarRef v_id) { return ti_get("magicword", v_id); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_magicword_h__
