
#ifndef __GENERATED_cls_utfnormal_h__
#define __GENERATED_cls_utfnormal_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 16075 */
class c_utfnormal : virtual public ObjectData {
  BEGIN_CLASS_MAP(utfnormal)
  END_CLASS_MAP(utfnormal)
  DECLARE_CLASS(utfnormal, UtfNormal, ObjectData)
  void init();
  public: static Variant ti_cleanup(const char* cls, Variant v_string);
  public: static Variant ti_tonfc(const char* cls, CVarRef v_string);
  public: static Variant ti_tonfd(const char* cls, CVarRef v_string);
  public: static Variant ti_tonfkc(const char* cls, CVarRef v_string);
  public: static Variant ti_tonfkd(const char* cls, CVarRef v_string);
  public: static void ti_loaddata(const char* cls);
  public: static bool ti_quickisnfc(const char* cls, CVarRef v_string);
  public: static bool ti_quickisnfcverify(const char* cls, Variant v_string);
  public: static String ti_nfc(const char* cls, CVarRef v_string);
  public: static Variant ti_nfd(const char* cls, CVarRef v_string);
  public: static String ti_nfkc(const char* cls, CVarRef v_string);
  public: static Variant ti_nfkd(const char* cls, CVarRef v_string);
  public: static Variant ti_fastdecompose(const char* cls, CVarRef v_string, CVarRef v_map);
  public: static Variant ti_fastcombiningsort(const char* cls, CVarRef v_string);
  public: static String ti_fastcompose(const char* cls, CVarRef v_string);
  public: static String ti_placebo(const char* cls, CVarRef v_string);
  public: static String t_fastcompose(CVarRef v_string) { return ti_fastcompose("utfnormal", v_string); }
  public: static void t_loaddata() { ti_loaddata("utfnormal"); }
  public: static Variant t_fastcombiningsort(CVarRef v_string) { return ti_fastcombiningsort("utfnormal", v_string); }
  public: static Variant t_cleanup(CVarRef v_string) { return ti_cleanup("utfnormal", v_string); }
  public: static String t_nfc(CVarRef v_string) { return ti_nfc("utfnormal", v_string); }
  public: static Variant t_nfd(CVarRef v_string) { return ti_nfd("utfnormal", v_string); }
  public: static Variant t_tonfkc(CVarRef v_string) { return ti_tonfkc("utfnormal", v_string); }
  public: static Variant t_tonfkd(CVarRef v_string) { return ti_tonfkd("utfnormal", v_string); }
  public: static String t_placebo(CVarRef v_string) { return ti_placebo("utfnormal", v_string); }
  public: static String t_nfkc(CVarRef v_string) { return ti_nfkc("utfnormal", v_string); }
  public: static Variant t_nfkd(CVarRef v_string) { return ti_nfkd("utfnormal", v_string); }
  public: static bool t_quickisnfc(CVarRef v_string) { return ti_quickisnfc("utfnormal", v_string); }
  public: static Variant t_fastdecompose(CVarRef v_string, CVarRef v_map) { return ti_fastdecompose("utfnormal", v_string, v_map); }
  public: static bool t_quickisnfcverify(CVarRef v_string) { return ti_quickisnfcverify("utfnormal", v_string); }
  public: static Variant t_tonfc(CVarRef v_string) { return ti_tonfc("utfnormal", v_string); }
  public: static Variant t_tonfd(CVarRef v_string) { return ti_tonfd("utfnormal", v_string); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_utfnormal_h__
