
#ifndef __GENERATED_php_DateFormatter_h__
#define __GENERATED_php_DateFormatter_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/DateFormatter.fw.h>

// Declarations
#include <cls/dateformatter.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$DateFormatter_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_dateformatter(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_DateFormatter_h__
