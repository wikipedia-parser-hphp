
#include <php/ParserCache.h>
#include <php/ParserOptions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: ParserCache.php line 6 */
Variant c_parsercache::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_parsercache::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_parsercache::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_parsercache::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_parsercache::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_parsercache::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_parsercache::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_parsercache::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(parsercache)
ObjectData *c_parsercache::create(Variant v_memCached) {
  init();
  t___construct(v_memCached);
  return this;
}
ObjectData *c_parsercache::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_parsercache::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_parsercache::cloneImpl() {
  c_parsercache *obj = NEW(c_parsercache)();
  cloneSet(obj);
  return obj;
}
void c_parsercache::cloneSet(c_parsercache *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_parsercache::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_parsercache::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x25DCCC35D69AD828LL, get) {
        return (t_get(a0, a1));
      }
      break;
    case 3:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parsercache::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parsercache$os_get(const char *s) {
  return c_parsercache::os_get(s, -1);
}
Variant &cw_parsercache$os_lval(const char *s) {
  return c_parsercache::os_lval(s, -1);
}
Variant cw_parsercache$os_constant(const char *s) {
  return c_parsercache::os_constant(s);
}
Variant cw_parsercache$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parsercache::os_invoke(c, s, params, -1, fatal);
}
void c_parsercache::init() {
}
/* SRC: ParserCache.php line 10 */
Variant c_parsercache::ti_singleton(const char* cls) {
  STATIC_METHOD_INJECTION(ParserCache, ParserCache::singleton);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_instance __attribute__((__unused__)) = g->sv_parsercache$$singleton$$instance.lvalAt(cls);
  Variant &inited_sv_instance __attribute__((__unused__)) = g->inited_sv_parsercache$$singleton$$instance.lvalAt(cls);
  Variant &gv_parserMemc __attribute__((__unused__)) = g->GV(parserMemc);
  Variant v_parserMemc;

  if (!inited_sv_instance) {
    (sv_instance = null);
    inited_sv_instance = true;
  }
  if (!(isset(sv_instance))) {
    v_parserMemc = ref(g->GV(parserMemc));
    (sv_instance = ((Object)(LINE(14,p_parsercache(p_parsercache(NEWOBJ(c_parsercache)())->create(v_parserMemc))))));
  }
  return sv_instance;
} /* function */
/* SRC: ParserCache.php line 25 */
void c_parsercache::t___construct(Variant v_memCached) {
  INSTANCE_METHOD_INJECTION(ParserCache, ParserCache::__construct);
  bool oldInCtor = gasInCtor(true);
  (o_lval("mMemc", 0x745C56F2F4808026LL) = v_memCached);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: ParserCache.php line 29 */
Variant c_parsercache::t_getkey(CVarRef v_article, Variant v_popts) {
  INSTANCE_METHOD_INJECTION(ParserCache, ParserCache::getKey);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgRequest __attribute__((__unused__)) = g->GV(wgRequest);
  Variant v_user;
  Variant v_printable;
  Variant v_hash;
  String v_edit;
  Variant v_pageid;
  int64 v_renderkey = 0;
  Variant v_key;

  if (instanceOf(v_popts, "User")) (v_popts = ((Object)(LINE(33,c_parseroptions::t_newfromuser(v_popts)))));
  (v_user = v_popts.o_get("mUser", 0x316BC0F2B0E26180LL));
  (v_printable = (toBoolean(LINE(36,v_popts.o_invoke_few_args("getIsPrintable", 0x6256ABE618956C3DLL, 0)))) ? (("!printable=1")) : (("")));
  (v_hash = LINE(37,v_user.o_invoke_few_args("getPageRenderingHash", 0x08F98BEA09BD8F62LL, 0)));
  if (!(toBoolean(LINE(38,toObject(v_article).o_get("mTitle", 0x4C804C46307BF0EALL).o_invoke_few_args("quickUserCan", 0x643A7B8592BE5D21LL, 1, "edit"))))) {
    (v_edit = "!edit=0");
  }
  else {
    (v_edit = "");
  }
  (v_pageid = LINE(44,toObject(v_article)->o_invoke_few_args("getID", 0x5ADBB1D3A7BA92B8LL, 0)));
  (v_renderkey = toInt64((equal(LINE(45,gv_wgRequest.o_invoke_few_args("getVal", 0x7D658F691378C5ACLL, 1, "action")), "render"))));
  (v_key = LINE(46,(assignCallTemp(eo_2, concat(toString(v_pageid), concat6("-", toString(v_renderkey), "!", toString(v_hash), v_edit, toString(v_printable)))),invoke_failed("wfmemckey", Array(ArrayInit(3).set(0, "pcache").set(1, "idhash").set(2, eo_2).create()), 0x0000000072AF623FLL))));
  return v_key;
} /* function */
/* SRC: ParserCache.php line 50 */
String c_parsercache::t_getetag(Object v_article, CVarRef v_popts) {
  INSTANCE_METHOD_INJECTION(ParserCache, ParserCache::getETag);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  return LINE(51,(assignCallTemp(eo_1, toString(t_getkey(v_article, v_popts))),assignCallTemp(eo_3, toString(v_article.o_get("mTouched", 0x204FE32FF55B259DLL))),concat5("W/\"", eo_1, "--", eo_3, "\"")));
} /* function */
/* SRC: ParserCache.php line 54 */
Variant c_parsercache::t_getdirty(CVarRef v_article, CVarRef v_popts) {
  INSTANCE_METHOD_INJECTION(ParserCache, ParserCache::getDirty);
  Variant v_key;
  Variant v_value;

  (v_key = LINE(55,t_getkey(v_article, v_popts)));
  LINE(56,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, concat3("Trying parser cache ", toString(v_key), "\n")).create()), 0x00000000E441E905LL));
  (v_value = LINE(57,o_get("mMemc", 0x745C56F2F4808026LL).o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_key)));
  return LINE(58,x_is_object(v_value)) ? ((Variant)(v_value)) : ((Variant)(false));
} /* function */
/* SRC: ParserCache.php line 61 */
Variant c_parsercache::t_get(Variant v_article, CVarRef v_popts) {
  INSTANCE_METHOD_INJECTION(ParserCache, ParserCache::get);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgCacheEpoch __attribute__((__unused__)) = g->GV(wgCacheEpoch);
  Variant v_value;
  Variant v_canCache;
  Variant v_cacheTime;
  Variant v_touched;

  LINE(63,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "ParserCache::get").create()), 0x0000000075359BAFLL));
  (v_value = LINE(65,t_getdirty(v_article, v_popts)));
  if (!(toBoolean(v_value))) {
    LINE(67,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser cache miss.\n").create()), 0x00000000E441E905LL));
    LINE(68,invoke_failed("wfincrstats", Array(ArrayInit(1).set(0, "pcache_miss_absent").create()), 0x000000001E6250DBLL));
    LINE(69,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "ParserCache::get").create()), 0x00000000B599F276LL));
    return false;
  }
  LINE(73,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Found.\n").create()), 0x00000000E441E905LL));
  (v_canCache = LINE(75,v_article.o_invoke_few_args("checkTouched", 0x792C1A084E953102LL, 0)));
  (v_cacheTime = LINE(76,v_value.o_invoke_few_args("getCacheTime", 0x5C8BDF93ED656C68LL, 0)));
  (v_touched = v_article.o_get("mTouched", 0x204FE32FF55B259DLL));
  if (!(toBoolean(v_canCache)) || toBoolean(LINE(78,v_value.o_invoke_few_args("expired", 0x1DEB2F58AAA8A419LL, 1, v_touched)))) {
    if (!(toBoolean(v_canCache))) {
      LINE(80,invoke_failed("wfincrstats", Array(ArrayInit(1).set(0, "pcache_miss_invalid").create()), 0x000000001E6250DBLL));
      LINE(81,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, concat("Invalid cached redirect, touched ", concat6(toString(v_touched), ", epoch ", toString(gv_wgCacheEpoch), ", cached ", toString(v_cacheTime), "\n"))).create()), 0x00000000E441E905LL));
    }
    else {
      LINE(83,invoke_failed("wfincrstats", Array(ArrayInit(1).set(0, "pcache_miss_expired").create()), 0x000000001E6250DBLL));
      LINE(84,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, concat("Key expired, touched ", concat6(toString(v_touched), ", epoch ", toString(gv_wgCacheEpoch), ", cached ", toString(v_cacheTime), "\n"))).create()), 0x00000000E441E905LL));
    }
    (v_value = false);
  }
  else {
    if (toObject(v_value)->t___isset("mTimestamp")) {
      (v_article.o_lval("mTimestamp", 0x78CCE3041490DE7ALL) = v_value.o_get("mTimestamp", 0x78CCE3041490DE7ALL));
    }
    LINE(91,invoke_failed("wfincrstats", Array(ArrayInit(1).set(0, "pcache_hit").create()), 0x000000001E6250DBLL));
  }
  LINE(94,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "ParserCache::get").create()), 0x00000000B599F276LL));
  return v_value;
} /* function */
/* SRC: ParserCache.php line 98 */
void c_parsercache::t_save(Variant v_parserOutput, Object v_article, CVarRef v_popts) {
  INSTANCE_METHOD_INJECTION(ParserCache, ParserCache::save);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgParserCacheExpireTime __attribute__((__unused__)) = g->GV(wgParserCacheExpireTime);
  Variant v_key;
  Variant v_now;
  Variant v_expire;

  (v_key = LINE(100,t_getkey(v_article, v_popts)));
  if (!equal(LINE(102,v_parserOutput.o_invoke_few_args("getCacheTime", 0x5C8BDF93ED656C68LL, 0)), -1LL)) {
    (v_now = LINE(104,invoke_failed("wftimestampnow", Array(), 0x00000000151D8950LL)));
    LINE(105,v_parserOutput.o_invoke_few_args("setCacheTime", 0x2983A8B8675FE955LL, 1, v_now));
    (v_parserOutput.o_lval("mTimestamp", 0x78CCE3041490DE7ALL) = LINE(108,v_article->o_invoke_few_args("getTimestamp", 0x7533D7D1FB463C81LL, 0)));
    concat_assign(v_parserOutput.o_lval("mText", 0x69302AA34BFECFE3LL), LINE(110,concat5("\n<!-- Saved in parser cache with key ", toString(v_key), " and timestamp ", toString(v_now), " -->\n")));
    LINE(111,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, concat5("Saved in parser cache with key ", toString(v_key), " and timestamp ", toString(v_now), "\n")).create()), 0x00000000E441E905LL));
    if (toBoolean(LINE(113,v_parserOutput.o_invoke_few_args("containsOldMagic", 0x0B5494D93581FFA5LL, 0)))) {
      (v_expire = 3600LL);
    }
    else {
      (v_expire = gv_wgParserCacheExpireTime);
    }
    LINE(118,o_get("mMemc", 0x745C56F2F4808026LL).o_invoke_few_args("set", 0x399A6427C2185621LL, 3, v_key, v_parserOutput, v_expire));
  }
  else {
    LINE(121,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser output was marked as uncacheable and has not been saved.\n").create()), 0x00000000E441E905LL));
  }
} /* function */
Object co_parsercache(CArrRef params, bool init /* = true */) {
  return Object(p_parsercache(NEW(c_parsercache)())->dynCreate(params, init));
}
Variant pm_php$ParserCache_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::ParserCache.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$ParserCache_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
