
#include <cpp/base/hphp.h>
#include <cpp/base/array/zend_array.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void csi_title();
void csi_magicword();
void csi_ppdstack();
void csi_language();
void csi_user();

IMPLEMENT_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
GlobalVariables::GlobalVariables() : dummy(false),
  inited_sv_title$$secureandsplit$$rxTc(),
  inited_sv_wfout$$lineStarted(false),
  inited_sv_wfshellexec$$disabled(false),
  inited_sv_wfincrstats$$socket(false),
  inited_sv_sanitizer$$removehtmltags$$htmlsingleallowed(),
  inited_sv_language$$getcasemaps$$wikiUpperChars(),
  inited_sv_wfabruptexit$$called(false),
  inited_sv_parser$$replaceinternallinks2$$tc(),
  inited_sv_skin$$getskinnames$$skinsInitialised(),
  inited_sv_wfsuppresswarnings$$originalLevel(false),
  inited_sv_sanitizer$$removehtmltags$$staticInitialised(),
  inited_sv_sanitizer$$removehtmltags$$htmlnest(),
  inited_sv_language$$romannumeral$$table(),
  inited_sv_stubobject$$_unstub$$recursionLevel(),
  inited_sv_language$$hebrewnumeral$$table(),
  inited_sv_sanitizer$$removehtmltags$$htmlsingleonly(),
  inited_sv_utfnormal$$quickisnfcverify$$checkit(),
  inited_sv_wfloadextensionmessages$$loaded(false),
  inited_sv_wfinitshelllocale$$done(false),
  inited_sv_ip$$ispublic$$privateRanges(),
  inited_sv_sanitizer$$removehtmltags$$htmlpairs(),
  inited_sv_language$$getcasemaps$$wikiLowerChars(),
  inited_sv_block$$formatexpiry$$msg(),
  inited_sv_sanitizer$$removehtmltags$$htmllist(),
  inited_sv_coreparserfunctions$$pagesincategory$$cache(),
  inited_sv_ppframe_dom$$expand$$expansionDepth(),
  inited_sv_parser$$replaceinternallinks2$$e1(),
  inited_sv_language$$getfallbackfor$$cache(),
  inited_sv_coreparserfunctions$$israw$$mwRaw(),
  inited_sv_coreparserfunctions$$pagesize$$cache(),
  inited_sv_linkcache$$singleton$$instance(),
  inited_sv_sanitizer$$removehtmltags$$tabletags(),
  inited_sv_user$$getmaxid$$res(),
  inited_sv_parser$$getimageparams$$internalParamNames(),
  inited_sv_sanitizer$$attributewhitelist$$list(),
  inited_sv_linker$$splittrail$$regex(),
  inited_sv_parser$$getimageparams$$internalParamMap(),
  inited_sv_parser$$extracttagsandparams$$n(),
  inited_sv_parser$$replaceinternallinks2$$e1_img(),
  inited_sv_language$$newfromcode$$recursionLevel(),
  inited_sv_wfdebug$$cache(false),
  inited_sv_user$$isusablename$$reservedUsernames(),
  inited_sv_utfnormal$$quickisnfcverify$$tailBytes(),
  inited_sv_wfhostname$$host(false),
  inited_sv_backlinkcache$$getprefix$$prefixes(),
  inited_sv_title$$newfromtext$$cachedcount(),
  inited_sv_language$$loadlocalisation$$recursionGuard(),
  inited_sv_article$$tryfilecache$$called(),
  inited_sv_sanitizer$$removehtmltags$$htmlelements(),
  inited_sv_sanitizer$$removehtmltags$$listtags(),
  inited_sv_sanitizer$$removehtmltags$$htmlsingle(),
  inited_sv_utfnormal$$quickisnfcverify$$utfCheckOrCombining(),
  inited_sv_wfsuppresswarnings$$suppressCount(false),
  inited_sv_mwnamespace$$getcanonicalindex$$xNamespaces(),
  inited_sv_sanitizer$$escapeid$$replace(),
  inited_sv_parser$$transformmsg$$executing(),
  inited_sv_wfdebug$$recursion(false),
  run_pm_php$Parser2_php(false) {

  // Dynamic Constants
  k_MW_DB = "MW_DB";
  k_MW_PREFIX = "MW_PREFIX";
  k_MW_ATTRIBS_REGEX = "MW_ATTRIBS_REGEX";
  k_MEDIAWIKI = "MEDIAWIKI";
  k_TESTWIKI = "TESTWIKI";

  // Primitive Function/Method Static Variables
  sv_wfout$$lineStarted = false;
  sv_wfabruptexit$$called = false;
  sv_wfinitshelllocale$$done = false;
  sv_wfsuppresswarnings$$suppressCount = 0;
  sv_wfdebug$$recursion = 0;

  // Primitive Class Static Variables

  // Redeclared Functions

  // Redeclared Classes
}

void GlobalVariables::initialize() {
  SystemGlobals::initialize();
  csi_language();
  csi_magicword();
  csi_ppdstack();
  csi_title();
  csi_user();
}

void init_static_variables() { ScalarArrays::initialize();}
static ThreadLocalSingleton<GlobalVariables> g_variables;
static IMPLEMENT_THREAD_LOCAL(GlobalArrayWrapper, g_array_wrapper);
GlobalVariables *get_global_variables() {
  return g_variables.get();
}
void init_global_variables() { GlobalVariables::initialize();}
void free_global_variables() {
  g_variables.reset();
  g_array_wrapper.reset();
}
LVariableTable *get_variable_table() { return (LVariableTable*)get_global_variables();}
Globals *get_globals() { return (Globals*)get_global_variables();}
SystemGlobals *get_system_globals() { return (SystemGlobals*)get_global_variables();}
Array get_global_array_wrapper(){ return g_array_wrapper.get();}

///////////////////////////////////////////////////////////////////////////////
}

#ifndef HPHP_BUILD_LIBRARY
int main(int argc, char** argv) {
  return HPHP::execute_program(argc, argv);
}
#endif
