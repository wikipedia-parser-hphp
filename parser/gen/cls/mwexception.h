
#ifndef __GENERATED_cls_mwexception_h__
#define __GENERATED_cls_mwexception_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 14672 */
class c_mwexception : virtual public c_exception {
  BEGIN_CLASS_MAP(mwexception)
    PARENT_CLASS(exception)
  END_CLASS_MAP(mwexception)
  DECLARE_CLASS(mwexception, MWException, exception)
  void init();
  public: bool t_useoutputpage();
  public: bool t_usemessagecache();
  public: Variant t_runhooks(CVarRef v_name, CArrRef v_args = ScalarArrays::sa_[0]);
  public: Variant t_msg(int num_args, CVarRef v_key, CVarRef v_fallback, Array args = Array());
  public: Variant t_gethtml();
  public: Variant t_gettext();
  public: Variant t_getpagetitle();
  public: String t_getlogmessage();
  public: void t_reporthtml();
  public: void t_report();
  public: void t_htmlheader();
  public: void t_htmlfooter();
  public: static bool ti_iscommandline(const char* cls);
  public: static bool t_iscommandline() { return ti_iscommandline("mwexception"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_mwexception_h__
