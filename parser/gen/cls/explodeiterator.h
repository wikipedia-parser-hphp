
#ifndef __GENERATED_cls_explodeiterator_h__
#define __GENERATED_cls_explodeiterator_h__

#include <cls/citerator.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 40355 */
class c_explodeiterator : virtual public c_citerator {
  BEGIN_CLASS_MAP(explodeiterator)
    PARENT_CLASS(ctraversable)
    PARENT_CLASS(citerator)
  END_CLASS_MAP(explodeiterator)
  DECLARE_CLASS(explodeiterator, ExplodeIterator, citerator)
  void init();
  public: Variant m_subject;
  public: Variant m_subjectLength;
  public: Variant m_delim;
  public: Variant m_delimLength;
  public: Variant m_curPos;
  public: Variant m_endPos;
  public: Variant m_current;
  public: void t___construct(Variant v_delim, Variant v_s);
  public: ObjectData *create(Variant v_delim, Variant v_s);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_rewind();
  public: void t_refreshcurrent();
  public: Variant t_current();
  public: Variant t_key();
  public: Variant t_next();
  public: bool t_valid();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_explodeiterator_h__
