#!/bin/sh
##
## runxlst.sh
## 
## Made by Michael DuPont
## Login   <mdupont@mdupontdesktop2>
## 
## Started on  Wed Feb 11 18:38:47 2009 Michael DuPont
## Last update Wed Feb 11 18:38:47 2009 Michael DuPont
##

xsltproc --verbose ../../../../../../parrot/languages/pipp/src/phc/phc_xml_to_past_xml.xsl ../Parser.php.xml  > xsltproc.out 2>xsltproc.log

xsltproc --verbose ../../../../../../parrot/languages/pipp/src/phc/past_xml_to_past_nqp.xsl xsltproc.out > past.out 2>past.log