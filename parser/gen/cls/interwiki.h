
#ifndef __GENERATED_cls_interwiki_h__
#define __GENERATED_cls_interwiki_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 80 */
class c_interwiki : virtual public ObjectData {
  BEGIN_CLASS_MAP(interwiki)
  END_CLASS_MAP(interwiki)
  DECLARE_CLASS(interwiki, Interwiki, ObjectData)
  void init();
  public: static void ti_fetch(const char* cls);
  public: static void t_fetch() { ti_fetch("interwiki"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_interwiki_h__
