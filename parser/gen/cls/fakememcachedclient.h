
#ifndef __GENERATED_cls_fakememcachedclient_h__
#define __GENERATED_cls_fakememcachedclient_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 43482 */
class c_fakememcachedclient : virtual public ObjectData {
  BEGIN_CLASS_MAP(fakememcachedclient)
  END_CLASS_MAP(fakememcachedclient)
  DECLARE_CLASS(fakememcachedclient, FakeMemCachedClient, ObjectData)
  void init();
  public: bool t_add(CVarRef v_key, CVarRef v_val, CVarRef v_exp = 0LL);
  public: Variant t_decr(CVarRef v_key, int64 v_amt = 1LL);
  public: bool t_delete(CVarRef v_key, CVarRef v_time = 0LL);
  public: void t_disconnect_all();
  public: void t_enable_compress(CVarRef v_enable);
  public: void t_forget_dead_hosts();
  public: Variant t_get(CVarRef v_key);
  public: Variant t_get_multi(CVarRef v_keys);
  public: Variant t_incr(CVarRef v_key, CVarRef v_amt = 1LL);
  public: bool t_replace(CVarRef v_key, CVarRef v_value, CVarRef v_exp = 0LL);
  public: Variant t_run_command(CVarRef v_sock, CVarRef v_cmd);
  public: bool t_set(CVarRef v_key, CVarRef v_value, CVarRef v_exp = 0LL);
  public: void t_set_compress_threshold(CVarRef v_thresh);
  public: void t_set_debug(CVarRef v_dbg);
  public: void t_set_servers(CVarRef v_list);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fakememcachedclient_h__
