#include "StandardHeader.h"

namespace RCH
{
  template <class C> class MethodCall
  {
  public:
    C theObject; // we store the object
    
  };
};

/***
    get this is called in the beginning of the function
    we want to pass the object context in from outside.
*/
zvalP getThis(){

  static zval ObjectContext;
  return &ObjectContext;
}


STDRET ZEND_NUM_ARGS(){};
STDRET::operator int(){};
template <> STDRET zend_get_parameters_array<int, int, zval**>(int, int, zval**){};
zvalC2 & zvalC2::operator++(int){};
template <> STDRET zval_ptr_dtor<zval*>(zval**){};
template <> SmartRet<NULLT2> EG<NULLT2>(NULLT2&){};
template <> SmartRet<NULLT2>::operator zval*(){};
template <> SmartRet<symbol_tableT> EG<symbol_tableT>(symbol_tableT&){};
//template <> SmartRet<symbol_tableT>::operator&(){};
template <> STDRET get_st_entry<symbol_tableT*, char const*, int, unsigned int>(symbol_tableT*, char const*, int, unsigned int){};
template <> STDRET::operator zval**(){};
void sep_copy_on_write(zval**){};
void copy_into_ref(zval**, zval**){};
STDRET phc_check_invariants(){};
zvalR2::operator bool(){};
template <> STDRET zval_dtor<zval>(zval*){};
template <> STDRET ALLOC_INIT_ZVAL<zval>(zval*){};
template <> STDRET ZVAL_STRINGL<zval*, char const*, int, int>(zval*, char const*, int, int){};
template <> STDRET get_constant<char const*, int, zval**>(char const*, int, zval**){};
void overwrite_lhs_no_copy(zval*, zval*){};
template <> STDRET safe_free_zval_ptr<zval>(zval*){};
template <> STDRET initialize_function_call<zend_fcall_info*, _zend_fcall_info_cache*, char const*, char const*, int>(zend_fcall_info*, _zend_fcall_info_cache*, char const*, char const*, int){};


template <> zend_functionCArg::operator zend_arg_info*(){};
zend_arg_infoR::operator int(){};
zend_functionCPass::operator int(){};

void fetch_var_arg_by_ref(){};
int in_copy_on_write(zval*){};
assert::assert(int){};
zval* fetch_var_arg(zval*, int*){};
template <> STDRET phc_setup_error<int, char const*, int, NULLT>(int, char const*, int, NULLT){};
 zend_functionPC::operator int(){};
template <> zend_functionPS::operator zval***(){};
template <>  zend_functionR::operator zval**(){};
template <> zend_functionPS::zend_functionPS(zval** ){};
zend_functionPC::zend_functionPC(int){};
template <> zend_functionR::zend_functionR(zval** const){};


template <> STDRETT<int> zend_call_function(zend_fcall_info *,_zend_fcall_info_cache *)
{
}

template <> STDRETT<int>::operator int()
{

}


// NULLT::operator int() const{};
template <> zend_functionPS::zend_functionPS(zval***){};

template <> STDRET phc_setup_error<int, NULLT, int, NULLT>(int, NULLT, int, NULLT){};
assert::assert(){};
zend_functionCReturn::operator int(){};
template <>  bool zvalT2::operator!=<NULLT>(NULLT){};
zvalR2 & zvalR2::operator=(int)
{
  return *this;
};
template <>  zvalC2& zvalC2::operator=<int>(int&){};
void write_var(zval**, zval*){};

zval::zval(zval const&){};
template <> STDRET concat_function<zval*, zval*, zval*>(zval*, zval*, zval*){};
zval::~zval(){};
zend_bool::zend_bool(int){};
template <> STDRET initialize_method_call<zend_fcall_info*, _zend_fcall_info_cache*, zval**, char const*, char const*, int>(zend_fcall_info*, _zend_fcall_info_cache*, zval**, char const*, char const*, int){};
template <> STDRET ZVAL_LONG<zval*, int>(zval*, int){};
template <> STDRET Z_TYPE_P<zval>(zval*){};
zval* read_string_index(zval*, zval*){};
int check_array_index_type(zval*){};
void read_array(zval**, zval*, zval*){};
void overwrite_lhs(zval*, zval*){};
zval * zvp_clone_ex(zval*){};
template <> zend_arg_infoR::zend_arg_infoR(zend_arg_infoR&){};
template <> STDRET ZVAL_BOOL/*<zval*, zend_arg_infoR>*/(zval*, zend_arg_infoR){};
template <> zend_functionCPass::zend_functionCPass/*<zend_functionCPass>*/(zend_functionCPass&){};
template <> STDRET ZVAL_BOOL/*<zval*, zend_functionCPass>*/(zval*, zend_functionCPass){};
template <> STDRET zend_is_true/*<zval>*/(zval*){};
template <>  STDRET::operator zend_bool/*<zend_bool>*/(){};
zend_bool::operator int(){};
void check_array_type(zval**){};
zval** get_ht_entry(zval**, zval*){};
template <> STDRET ZVAL_BOOL/*<zval*, int>*/(zval*, int){};
template <> STDRET is_not_identical_function/*<zval*, zval*, zval*>*/(zval*, zval*, zval*){};
template <> bool zvalT2::operator!=/*<int>*/(int){};
template <> STDRET convert_to_array/*<zval>*/(zval*){};
zval** push_and_index_ht(zval**){};
template <> zend_functionPS::zend_functionPS/*<zval** [3]>*/(zval** (&) [3]){};
template <> STDRET Z_TYPE_PP/*<zval*>*/(zval**){};
template <> STDRET Z_STRLEN_PP/*<zval*>*/(zval**){};
bool STDRET::operator>(int){};
void write_string_index(zval**, zval*, zval*){};
template <> STDRET is_not_equal_function/*<zval*, zval*, zval*>*/(zval*, zval*, zval*){};
template <> zend_functionPS::zend_functionPS/*<zval** [0]>*/(zval** (&) [0]){};
int isset_array(zval**, zval*){};
template <> STDRET is_equal_function<zval*, zval*, zval*>(zval*, zval*, zval*){};
template <> STDRET convert_to_boolean<zval>(zval*){};
//template <>  zend_functionPS::zend_functionPS/*<zval** [4]>*/(zval** ){};
template <>  return_valueV& return_valueV::operator=<zvalV2>(zvalV2&){};
template <>  return_valueV& return_valueT::operator=<zvalT2>(zvalT2&){};
template <> STDRET zval_copy_ctor<return_valueT2>(return_valueT2*){};
//NULLT::NULLT(){};
NULLT2::NULLT2(){};
symbol_tableT::symbol_tableT(){};
 class_table_t::class_table_t(){};
 function_table_t::function_table_t(){};
 zend_fcall_infoB::zend_fcall_infoB(){};
 zend_functionPC::zend_functionPC(){};
 zend_functionPS::zend_functionPS(){};
 zend_functionR::zend_functionR(){};

//void __ZNK5NULLTcvPT_I4zvalEEv{};
template <> symbol_tableT * SmartRet<symbol_tableT>::operator&(){};
zval** fetch_var_arg_by_ref(zval**){};
//void __ZNK5NULLTcvPT_I13zend_functionEEv{};
//void __ZNK5NULLTcvPT_I16zend_class_entryEEv{};
//void __ZNK5NULLTcvPT_IP4zvalEEv{};
 zval::zval(){};
//void _WinMain@16{};


void MyThrowImp(zval* vakz)
{
  // TODO 
}
void * MyTypeOf(void * Variable)
{
}

void * MyInstanceof(void * Instance,void * Class)
{
}

void * MyVariable(void * Variable)
{
}
void * MyArray_next(void * Variable)
{
}
void * MyMIRClassName(void * Name=0)
{
}

void * MyStringImp(void * Name)
{
}

void MyStaticImp(zval* vakz)
{
  // TODO 
}

NULLT2 uninitialized_zval_ptr;
symbol_tableT symbol_table;
int saved_refcount;
return_valueT2 * return_value;


/**
 * Formats wiki links and media links in text; all other wiki formatting
 * is ignored
 *
 * @fixme doesn't handle sub-links as in image thumb texts like the main parser
 * @param string $comment Text to format links in
 * @return string

 public function formatLinksInComment( $comment ) {
 return preg_replace_callback(
 '/\[\[:?(.*?)(\|(.*?))*\]\]([^[]*)/',
 array( $this, 'formatLinksInCommentCallback' ),
 $comment );
 }

*/
void Linkerformatlinksincommentcallback ();

int main(int argc, char ** argv)
{
  //
  Linkerformatlinksincommentcallback();
}



/**
   function specific code...
   
*/

template <> 
STDRET initialize_function_call
/*
<zend_fcall_infoT
<mwnamespace_getcanonicalname>
*, _zend_fcall_info_cache*, char cons
t*, char const*, int>
*/
(
 zend_fcall_infoT<mwnamespace_getcanonicalname>*, 
 _zend_fcall_info_cache*, 
 char const*, 
 char const*
 , int){
  
};

/*
for assigning the parameters that were saved.
template <>
zend_functionPST<mwnamespace_getcanonicalname> & 
zend_functionPST<mwnamespace_getcanonicalname>::operator=(zval***)
{

};
/*
/*
set the return type
move back into header
template <> 
zend_functionRT<mwnamespace_getcanonicalname> & 
zend_functionRT<mwnamespace_getcanonicalname>::operator=(const zvalPP & rVale)
{

};
*/

template <> 
STDRETT<int> zend_call_function
//<zend_fcall_infoT<mwnamespace_getcanonicalname>, _zend_fcall_info_cache>
(zend_fcall_infoT<mwnamespace_getcanonicalname>*, 
 _zend_fcall_info_cache*)
{

};


/////

zvalP::operator zval*(){};
zvalPP::zvalPP(zval**){};
void sep_copy_on_write(zvalPP){};
void copy_into_ref(zvalPP, zvalPP){};
zvalP::zvalP(zval*){};
void overwrite_lhs_no_copy(zvalP, zvalP){};
zvalPP fetch_var_arg_by_ref(zvalPP){};
zvalPP::operator zval**(){};
int in_copy_on_write(zvalP){};
zvalP fetch_var_arg(zvalP, int*){};
void write_var(zvalPP, zvalP){};
zvalP read_string_index(zvalP, zvalP){};
int check_array_index_type(zvalP){};
void read_array(zvalPP, zvalP, zvalP){};
void overwrite_lhs(zvalP, zvalP){};
zvalP zvp_clone_ex(zvalP){};
void check_array_type(zvalPP){};
zvalPP get_ht_entry(zvalPP, zvalP){};
zvalPP push_and_index_ht(zvalPP){};
void write_string_index(zvalPP, zvalP, zvalP){};
int isset_array(zvalPP, zvalP){};

template <> bool zvalP::operator!=(SmartRet<NULLT2>)
{

};

zval* zvalP::operator->(){};

template <class T> STDRET zval_ptr_dtor /*<zvalPReturn<mwnamespace_getcanonicalname> >*/
(zvalPReturn<mwnamespace_getcanonicalname>*){
};

zvalP::zvalP(){};

template <>
STDRET zval_ptr_dtor
/*<zvalPReturn<mwnamespace_getcanonicalname> >*/
(zvalPReturn<mwnamespace_getcanonicalname>*){};

/*
template <>
void zvalPReturn<mwnamespace_getcanonicalname>::setReturnTypeDynamic
(zend_functionRT<mwnamespace_getcanonicalname>*) const{};
*/

/*
<zend_functionRT<mwnamespace_getcanonicalname>*
>*/


template<>
STDRET initialize_function_call
//<zend_fcall_infoT<mwnamespace_getcanonicalname>*, zend_fcall_info_cacheT<mwnamespace_getcanonicalname>*, char const*, char const*, int>
(zend_fcall_infoT<mwnamespace_getcanonicalname>*, zend_fcall_info_cacheT<mwnamespace_getcanonicalname>*, char const*, char const*, int){};

template<>
STDRETT<int> zend_call_function
//<zend_fcall_infoT<mwnamespace_getcanonicalname>, zend_fcall_info_cacheT<mwnamespace_getcanonicalname> >
(zend_fcall_infoT<mwnamespace_getcanonicalname>*, zend_fcall_info_cacheT<mwnamespace_getcanonicalname>*){};

zend_bool::zend_bool(){};



/// new functions
template <>
zend_functionPS::operator 
//zvalParamsT<mwnamespace_getcanonicalname><
zvalParamsT<mwnamespace_getcanonicalname> 
//>
(){}
;


/*
template <> zend_functionPST<mwnamespace_getcanonicalname>&
zend_functionPST<mwnamespace_getcanonicalname>::operator=

(zval***){};
*/
//<Zvalx***>

// the new template parameter
 /*
template <> zend_functionPST<mwnamespace_getcanonicalname>&
zend_functionPST<mwnamespace_getcanonicalname>::operator=
//<zvalParamsT<mwnamespace_getcanonicalname> >
(zvalParamsT<mwnamespace_getcanonicalname>){};
 */



 /*<zend_functionRT
   <mwnamespace_getcanonicalname>*>

*/
  /*
 template <>
 void zvalPReturn<mwnamespace_getcanonicalname>
 ::setReturnTypeDynamic(zend_functionRT<mwnamespace_getcanonicalname>*) const
 {}*/
  //template <> zval* MakeNullPtr(){};

  //template <>zval* MakeNullPtr<zval>(){}


//zend_functionPST<mwnamespace_getcanonicalname>& zend_functionPST<mwnamespace_getcanonicalname>::operator=<zval***>(zval***)'

/*template <>
zend_functionPST<mwnamespace_getcanonicalname>& 
zend_functionPST<mwnamespace_getcanonicalname>::operator=
//<zvalParamsT<mwnamespace_getcanonicalname> >
(zvalParamsT<mwnamespace_getcanonicalname>)
{

}

//operator=<zvalParamsT<mwnamespace_getcanonicalname> >

zend_functionPST<mwnamespace_getcanonicalname>& 
zend_functionPST<mwnamespace_getcanonicalname>::operator=(
zvalParamsT<mwnamespace_getcanonicalname>
)
{

}
*/

//zend_function* MakeNullPtr<zend_function>()
//zend_class_entry* MakeNullPtr<zend_class_entry>()'
//zval** MakeNullPtrPtr<zval>()
//zval* MakeNullPtr<zval>()'
//

//template <> zend_function* MakeNullPtr<zend_function>(){}
///template <> zend_function* MakeNullPtr<zend_function>()'

//template <> zend_function* MakeNullPtr(){};
//template <> zend_class_entry* MakeNullPtr(){};
//template <> zval** MakeNullPtrPtr(){};


template <>
void DoAssign
//<zend_functionPST<mwnamespace_getcanonicalname>, zval***>
(zend_functionPST<mwnamespace_getcanonicalname>, zval***)
{

}

//zend_function* MakeNullPtr<zend_function*>()

//template <> zend_function* MakeNullPtr(){}

void DoAssign
//<zend_functionPST<mwnamespace_getcanonicalname>,
//zvalParamsT<mwnamespace_getcanonicalname> >
(
 zend_functionPST<mwnamespace_getcanonicalname>, 
 zvalParamsT<mwnamespace_getcanonicalname>
 )
{

}

template <>
void DoAssign
//<zend_functionPST<mwnamespace_getcanonicalname>, 
//zvalParamsT<mwnamespace_getcanonicalname> >
(
 zend_functionPST<mwnamespace_getcanonicalname>, 
 zvalParamsT<mwnamespace_getcanonicalname>
 )
{

}
