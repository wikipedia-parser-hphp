#!/bin/sh
##
## extract.sh
## 
## Made by Michael DuPont
## Login   <mdupont@mdupontdesktop2>
## 
## Started on  Wed Feb 11 18:13:16 2009 Michael DuPont
## Last update Wed Feb 11 18:13:16 2009 Michael DuPont
##

# first we remove the content to create a model, we leave the ordering
xmllint --debug ../Parser.php.xml  | grep -v content | grep -v TEXT > structure.txt

# now we remove the whitespaces to get the structure
cat structure.txt | perl  -n  -e's/\s+/ /g;print "$_\n";'  | sort -u > types.txt

# now we have a list of the structural elements
# we create now rules for each element
for x in `grep ELEMENT types.txt |cut "-d " -f3 `; do echo  $x; done > elements.txt

