
#ifndef __GENERATED_php_Preprocessor_DOM_h__
#define __GENERATED_php_Preprocessor_DOM_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/Preprocessor_DOM.fw.h>

// Declarations
#include <cls/ppdpart.h>
#include <cls/ppdstackelement.h>
#include <cls/ppnode_dom.h>
#include <cls/ppdstack.h>
#include <cls/preprocessor_dom.h>
#include <cls/ppframe_dom.h>
#include <cls/ppcustomframe_dom.h>
#include <cls/pptemplateframe_dom.h>
#include <php/Parser.h>
#include <php/Preprocessor.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$Preprocessor_DOM_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_ppdpart(CArrRef params, bool init = true);
Object co_ppdstackelement(CArrRef params, bool init = true);
Object co_ppnode_dom(CArrRef params, bool init = true);
Object co_ppdstack(CArrRef params, bool init = true);
Object co_preprocessor_dom(CArrRef params, bool init = true);
Object co_ppframe_dom(CArrRef params, bool init = true);
Object co_ppcustomframe_dom(CArrRef params, bool init = true);
Object co_pptemplateframe_dom(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_Preprocessor_DOM_h__
