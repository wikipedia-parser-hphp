
#ifndef __GENERATED_cls_doublereplacer_h__
#define __GENERATED_cls_doublereplacer_h__

#include <cls/replacer.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 40219 */
class c_doublereplacer : virtual public c_replacer {
  BEGIN_CLASS_MAP(doublereplacer)
    PARENT_CLASS(replacer)
  END_CLASS_MAP(doublereplacer)
  DECLARE_CLASS(doublereplacer, DoubleReplacer, replacer)
  void init();
  public: void t___construct(Variant v_from, Variant v_to, Variant v_index = 0LL);
  public: ObjectData *create(Variant v_from, Variant v_to, Variant v_index = 0LL);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_replace(CVarRef v_matches);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_doublereplacer_h__
