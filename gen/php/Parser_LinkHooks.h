
#ifndef __GENERATED_php_Parser_LinkHooks_h__
#define __GENERATED_php_Parser_LinkHooks_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/Parser_LinkHooks.fw.h>

// Declarations
#include <cls/parser_linkhooks.h>
#include <cls/linkmarkerreplacer.h>
#include <php/CoreLinkFunctions.h>
#include <php/CoreParserFunctions.h>
#include <php/LinkHolderArray.h>
#include <php/Parser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$Parser_LinkHooks_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_parser_linkhooks(CArrRef params, bool init = true);
Object co_linkmarkerreplacer(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_Parser_LinkHooks_h__
