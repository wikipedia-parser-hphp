
#ifndef __GENERATED_cls_outputpage_h__
#define __GENERATED_cls_outputpage_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 98 */
class c_outputpage : virtual public ObjectData {
  BEGIN_CLASS_MAP(outputpage)
  END_CLASS_MAP(outputpage)
  DECLARE_CLASS(outputpage, OutputPage, ObjectData)
  void init();
  public: void t_redirect();
  public: void t_addcategorylinks();
  public: void t_getcategorylinks();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_outputpage_h__
