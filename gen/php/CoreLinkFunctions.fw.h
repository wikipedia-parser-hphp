
#ifndef __GENERATED_php_CoreLinkFunctions_fw_h__
#define __GENERATED_php_CoreLinkFunctions_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(corelinkfunctions)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/LinkHolderArray.fw.h>
#include <php/Parser_LinkHooks.fw.h>

#endif // __GENERATED_php_CoreLinkFunctions_fw_h__
