
#ifndef __GENERATED_cls_ppnode_dom_h__
#define __GENERATED_cls_ppnode_dom_h__

#include <cls/ppnode.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 35057 */
class c_ppnode_dom : virtual public c_ppnode {
  BEGIN_CLASS_MAP(ppnode_dom)
    PARENT_CLASS(ppnode)
  END_CLASS_MAP(ppnode_dom)
  DECLARE_CLASS(ppnode_dom, PPNode_DOM, ppnode)
  void init();
  public: Variant m_node;
  public: void t___construct(Variant v_node, Variant v_xpath = false);
  public: ObjectData *create(Variant v_node, Variant v_xpath = false);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t___get(Variant v_name);
  public: Variant &___lval(Variant v_name);
  public: String t___tostring();
  public: Variant t_getchildren();
  public: Variant t_getfirstchild();
  public: Variant t_getnextsibling();
  public: p_ppnode_dom t_getchildrenoftype(Variant v_type);
  public: Variant t_getlength();
  public: Variant t_item(Variant v_i);
  public: Variant t_getname();
  public: Array t_splitarg();
  public: Variant t_splitext();
  public: Array t_splitheading();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppnode_dom_h__
