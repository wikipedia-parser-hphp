
#ifndef __GENERATED_cls_linkbatch_h__
#define __GENERATED_cls_linkbatch_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 1228 */
class c_linkbatch : virtual public ObjectData {
  BEGIN_CLASS_MAP(linkbatch)
  END_CLASS_MAP(linkbatch)
  DECLARE_CLASS(linkbatch, LinkBatch, ObjectData)
  void init();
  public: Variant m_data;
  public: void t___construct(Variant v_arr = ScalarArrays::sa_[0]);
  public: ObjectData *create(Variant v_arr = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_addobj(CVarRef v_title);
  public: void t_add(CVarRef v_ns, CVarRef v_dbkey);
  public: void t_setarray(CVarRef v_array);
  public: bool t_isempty();
  public: int t_getsize();
  public: Variant t_execute();
  public: Variant t_executeinto(Variant v_cache);
  public: Variant t_addresulttocache(CVarRef v_cache, CVarRef v_res);
  public: Variant t_doquery();
  public: Variant t_constructset(CStrRef v_prefix, Variant v_db);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_linkbatch_h__
