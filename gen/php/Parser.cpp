
#include <php/CoreParserFunctions.h>
#include <php/DateFormatter.h>
#include <php/LinkHolderArray.h>
#include <php/Parser.h>
#include <php/ParserOptions.h>
#include <php/ParserOutput.h>
#include <php/Preprocessor.h>
#include <php/Tidy.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_DB_MASTER = "DB_MASTER";
const StaticString k_NS_MEDIAWIKI = "NS_MEDIAWIKI";
const StaticString k_NS_TEMPLATE = "NS_TEMPLATE";
const StaticString k_TS_MW = "TS_MW";
const StaticString k_TS_UNIX = "TS_UNIX";
const StaticString k_SFH_OBJECT_ARGS = "SFH_OBJECT_ARGS";
const StaticString k_SFH_NO_HASH = "SFH_NO_HASH";

/* preface starts */
inline Variant df_lambda_6(CVarRef aa, Variant &p0, Variant &p1, Variant &p2, Variant &p3) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  p2 = a0.rvalAt(2);
  p3 = a0.rvalAt(3);
  return aa;
  
  }
inline Variant df_lambda_7(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_8(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_9(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_10(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_11(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_12(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_13(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_14(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_15(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_16(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_17(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_18(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: Parser.php line 5183 */
Variant c_stripstate::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_stripstate::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_stripstate::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("general", m_general.isReferenced() ? ref(m_general) : m_general));
  props.push_back(NEW(ArrayElement)("nowiki", m_nowiki.isReferenced() ? ref(m_nowiki) : m_nowiki));
  c_ObjectData::o_get(props);
}
bool c_stripstate::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x78258C7EF69CF55DLL, nowiki, 6);
      break;
    case 3:
      HASH_EXISTS_STRING(0x661B15A9C00F7127LL, general, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_stripstate::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x78258C7EF69CF55DLL, m_nowiki,
                         nowiki, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x661B15A9C00F7127LL, m_general,
                         general, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_stripstate::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x78258C7EF69CF55DLL, m_nowiki,
                      nowiki, 6);
      break;
    case 3:
      HASH_SET_STRING(0x661B15A9C00F7127LL, m_general,
                      general, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_stripstate::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x78258C7EF69CF55DLL, m_nowiki,
                         nowiki, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x661B15A9C00F7127LL, m_general,
                         general, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_stripstate::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(stripstate)
ObjectData *c_stripstate::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_stripstate::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_stripstate::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_stripstate::cloneImpl() {
  c_stripstate *obj = NEW(c_stripstate)();
  cloneSet(obj);
  return obj;
}
void c_stripstate::cloneSet(c_stripstate *clone) {
  clone->m_general = m_general.isReferenced() ? ref(m_general) : m_general;
  clone->m_nowiki = m_nowiki.isReferenced() ? ref(m_nowiki) : m_nowiki;
  ObjectData::cloneSet(clone);
}
Variant c_stripstate::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x2374179DF576FCA9LL, unstripnowiki) {
        return (t_unstripnowiki(params.rvalAt(0)));
      }
      HASH_GUARD(0x695FA349798CF769LL, unstripgeneral) {
        return (t_unstripgeneral(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x540C7C6FAA3D937BLL, unstripboth) {
        return (t_unstripboth(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_stripstate::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 1:
      HASH_GUARD(0x2374179DF576FCA9LL, unstripnowiki) {
        return (t_unstripnowiki(a0));
      }
      HASH_GUARD(0x695FA349798CF769LL, unstripgeneral) {
        return (t_unstripgeneral(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x540C7C6FAA3D937BLL, unstripboth) {
        return (t_unstripboth(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_stripstate::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_stripstate$os_get(const char *s) {
  return c_stripstate::os_get(s, -1);
}
Variant &cw_stripstate$os_lval(const char *s) {
  return c_stripstate::os_lval(s, -1);
}
Variant cw_stripstate$os_constant(const char *s) {
  return c_stripstate::os_constant(s);
}
Variant cw_stripstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_stripstate::os_invoke(c, s, params, -1, fatal);
}
void c_stripstate::init() {
  m_general = null;
  m_nowiki = null;
}
/* SRC: Parser.php line 5186 */
void c_stripstate::t___construct() {
  INSTANCE_METHOD_INJECTION(StripState, StripState::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_general = LINE(5187,create_object("replacementarray", Array())));
  (m_nowiki = LINE(5188,create_object("replacementarray", Array())));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Parser.php line 5191 */
Variant c_stripstate::t_unstripgeneral(Variant v_text) {
  INSTANCE_METHOD_INJECTION(StripState, StripState::unstripGeneral);
  Variant v_oldText;

  LINE(5192,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "StripState::unstripGeneral").create()), 0x0000000075359BAFLL));
  {
    LOOP_COUNTER(1);
    do {
      LOOP_COUNTER_CHECK(1);
      {
        (v_oldText = v_text);
        (v_text = LINE(5195,m_general.o_invoke_few_args("replace", 0x42FAC655280A6146LL, 1, v_text)));
      }
    } while (!same(v_text, v_oldText));
  }
  LINE(5197,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "StripState::unstripGeneral").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 5201 */
Variant c_stripstate::t_unstripnowiki(Variant v_text) {
  INSTANCE_METHOD_INJECTION(StripState, StripState::unstripNoWiki);
  Variant v_oldText;

  LINE(5202,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "StripState::unstripNoWiki").create()), 0x0000000075359BAFLL));
  {
    LOOP_COUNTER(2);
    do {
      LOOP_COUNTER_CHECK(2);
      {
        (v_oldText = v_text);
        (v_text = LINE(5205,m_nowiki.o_invoke_few_args("replace", 0x42FAC655280A6146LL, 1, v_text)));
      }
    } while (!same(v_text, v_oldText));
  }
  LINE(5207,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "StripState::unstripNoWiki").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 5211 */
Variant c_stripstate::t_unstripboth(Variant v_text) {
  INSTANCE_METHOD_INJECTION(StripState, StripState::unstripBoth);
  Variant v_oldText;

  LINE(5212,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "StripState::unstripBoth").create()), 0x0000000075359BAFLL));
  {
    LOOP_COUNTER(3);
    do {
      LOOP_COUNTER_CHECK(3);
      {
        (v_oldText = v_text);
        (v_text = LINE(5215,m_general.o_invoke_few_args("replace", 0x42FAC655280A6146LL, 1, v_text)));
        (v_text = LINE(5216,m_nowiki.o_invoke_few_args("replace", 0x42FAC655280A6146LL, 1, v_text)));
      }
    } while (!same(v_text, v_oldText));
  }
  LINE(5218,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "StripState::unstripBoth").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 46 */
const StaticString q_parser_VERSION = "1.6.4";
const int64 q_parser_SFH_NO_HASH = 1LL;
const int64 q_parser_SFH_OBJECT_ARGS = 2LL;
const StaticString q_parser_EXT_LINK_URL_CLASS = "[^][<>\"\\x00-\\x20\\x7F]";
const StaticString q_parser_EXT_IMAGE_REGEX = "/^(http:\\/\\/|https:\\/\\/)([^][<>\"\\x00-\\x20\\x7F]+)\n\t\t\\/([A-Za-z0-9_.,~%\\-+&;#*\?!=()@\\x80-\\xFF]+)\\.((\?i)gif|png|jpg|jpeg)$/Sx";
const int64 q_parser_COLON_STATE_TEXT = 0LL;
const int64 q_parser_COLON_STATE_TAG = 1LL;
const int64 q_parser_COLON_STATE_TAGSTART = 2LL;
const int64 q_parser_COLON_STATE_CLOSETAG = 3LL;
const int64 q_parser_COLON_STATE_TAGSLASH = 4LL;
const int64 q_parser_COLON_STATE_COMMENT = 5LL;
const int64 q_parser_COLON_STATE_COMMENTDASH = 6LL;
const int64 q_parser_COLON_STATE_COMMENTDASHDASH = 7LL;
const int64 q_parser_PTD_FOR_INCLUSION = 1LL;
const int64 q_parser_OT_HTML = 1LL;
const int64 q_parser_OT_WIKI = 2LL;
const int64 q_parser_OT_PREPROCESS = 3LL;
const int64 q_parser_OT_MSG = 3LL;
const StaticString q_parser_MARKER_SUFFIX = "-QINU\177";
Variant c_parser::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_parser::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_parser::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("mTagHooks", m_mTagHooks.isReferenced() ? ref(m_mTagHooks) : m_mTagHooks));
  props.push_back(NEW(ArrayElement)("mTransparentTagHooks", m_mTransparentTagHooks.isReferenced() ? ref(m_mTransparentTagHooks) : m_mTransparentTagHooks));
  props.push_back(NEW(ArrayElement)("mFunctionHooks", m_mFunctionHooks.isReferenced() ? ref(m_mFunctionHooks) : m_mFunctionHooks));
  props.push_back(NEW(ArrayElement)("mFunctionSynonyms", m_mFunctionSynonyms.isReferenced() ? ref(m_mFunctionSynonyms) : m_mFunctionSynonyms));
  props.push_back(NEW(ArrayElement)("mVariables", m_mVariables.isReferenced() ? ref(m_mVariables) : m_mVariables));
  props.push_back(NEW(ArrayElement)("mImageParams", m_mImageParams.isReferenced() ? ref(m_mImageParams) : m_mImageParams));
  props.push_back(NEW(ArrayElement)("mImageParamsMagicArray", m_mImageParamsMagicArray.isReferenced() ? ref(m_mImageParamsMagicArray) : m_mImageParamsMagicArray));
  props.push_back(NEW(ArrayElement)("mStripList", m_mStripList.isReferenced() ? ref(m_mStripList) : m_mStripList));
  props.push_back(NEW(ArrayElement)("mMarkerIndex", m_mMarkerIndex.isReferenced() ? ref(m_mMarkerIndex) : m_mMarkerIndex));
  props.push_back(NEW(ArrayElement)("mPreprocessor", m_mPreprocessor.isReferenced() ? ref(m_mPreprocessor) : m_mPreprocessor));
  props.push_back(NEW(ArrayElement)("mExtLinkBracketedRegex", m_mExtLinkBracketedRegex.isReferenced() ? ref(m_mExtLinkBracketedRegex) : m_mExtLinkBracketedRegex));
  props.push_back(NEW(ArrayElement)("mUrlProtocols", m_mUrlProtocols.isReferenced() ? ref(m_mUrlProtocols) : m_mUrlProtocols));
  props.push_back(NEW(ArrayElement)("mDefaultStripList", m_mDefaultStripList.isReferenced() ? ref(m_mDefaultStripList) : m_mDefaultStripList));
  props.push_back(NEW(ArrayElement)("mVarCache", m_mVarCache.isReferenced() ? ref(m_mVarCache) : m_mVarCache));
  props.push_back(NEW(ArrayElement)("mConf", m_mConf.isReferenced() ? ref(m_mConf) : m_mConf));
  props.push_back(NEW(ArrayElement)("mFunctionTagHooks", m_mFunctionTagHooks.isReferenced() ? ref(m_mFunctionTagHooks) : m_mFunctionTagHooks));
  props.push_back(NEW(ArrayElement)("mOutput", m_mOutput.isReferenced() ? ref(m_mOutput) : m_mOutput));
  props.push_back(NEW(ArrayElement)("mAutonumber", m_mAutonumber.isReferenced() ? ref(m_mAutonumber) : m_mAutonumber));
  props.push_back(NEW(ArrayElement)("mDTopen", m_mDTopen.isReferenced() ? ref(m_mDTopen) : m_mDTopen));
  props.push_back(NEW(ArrayElement)("mStripState", m_mStripState.isReferenced() ? ref(m_mStripState) : m_mStripState));
  props.push_back(NEW(ArrayElement)("mIncludeCount", m_mIncludeCount.isReferenced() ? ref(m_mIncludeCount) : m_mIncludeCount));
  props.push_back(NEW(ArrayElement)("mArgStack", m_mArgStack.isReferenced() ? ref(m_mArgStack) : m_mArgStack));
  props.push_back(NEW(ArrayElement)("mLastSection", m_mLastSection.isReferenced() ? ref(m_mLastSection) : m_mLastSection));
  props.push_back(NEW(ArrayElement)("mInPre", m_mInPre.isReferenced() ? ref(m_mInPre) : m_mInPre));
  props.push_back(NEW(ArrayElement)("mLinkHolders", m_mLinkHolders.isReferenced() ? ref(m_mLinkHolders) : m_mLinkHolders));
  props.push_back(NEW(ArrayElement)("mLinkID", m_mLinkID.isReferenced() ? ref(m_mLinkID) : m_mLinkID));
  props.push_back(NEW(ArrayElement)("mIncludeSizes", m_mIncludeSizes.isReferenced() ? ref(m_mIncludeSizes) : m_mIncludeSizes));
  props.push_back(NEW(ArrayElement)("mPPNodeCount", m_mPPNodeCount.isReferenced() ? ref(m_mPPNodeCount) : m_mPPNodeCount));
  props.push_back(NEW(ArrayElement)("mDefaultSort", m_mDefaultSort.isReferenced() ? ref(m_mDefaultSort) : m_mDefaultSort));
  props.push_back(NEW(ArrayElement)("mTplExpandCache", m_mTplExpandCache.isReferenced() ? ref(m_mTplExpandCache) : m_mTplExpandCache));
  props.push_back(NEW(ArrayElement)("mTplRedirCache", m_mTplRedirCache.isReferenced() ? ref(m_mTplRedirCache) : m_mTplRedirCache));
  props.push_back(NEW(ArrayElement)("mTplDomCache", m_mTplDomCache.isReferenced() ? ref(m_mTplDomCache) : m_mTplDomCache));
  props.push_back(NEW(ArrayElement)("mHeadings", m_mHeadings.isReferenced() ? ref(m_mHeadings) : m_mHeadings));
  props.push_back(NEW(ArrayElement)("mDoubleUnderscores", m_mDoubleUnderscores.isReferenced() ? ref(m_mDoubleUnderscores) : m_mDoubleUnderscores));
  props.push_back(NEW(ArrayElement)("mExpensiveFunctionCount", m_mExpensiveFunctionCount.isReferenced() ? ref(m_mExpensiveFunctionCount) : m_mExpensiveFunctionCount));
  props.push_back(NEW(ArrayElement)("mOptions", m_mOptions.isReferenced() ? ref(m_mOptions) : m_mOptions));
  props.push_back(NEW(ArrayElement)("mTitle", m_mTitle.isReferenced() ? ref(m_mTitle) : m_mTitle));
  props.push_back(NEW(ArrayElement)("mOutputType", m_mOutputType.isReferenced() ? ref(m_mOutputType) : m_mOutputType));
  props.push_back(NEW(ArrayElement)("ot", m_ot.isReferenced() ? ref(m_ot) : m_ot));
  props.push_back(NEW(ArrayElement)("mRevisionId", m_mRevisionId.isReferenced() ? ref(m_mRevisionId) : m_mRevisionId));
  props.push_back(NEW(ArrayElement)("mRevisionTimestamp", m_mRevisionTimestamp.isReferenced() ? ref(m_mRevisionTimestamp) : m_mRevisionTimestamp));
  props.push_back(NEW(ArrayElement)("mRevIdForTs", m_mRevIdForTs.isReferenced() ? ref(m_mRevIdForTs) : m_mRevIdForTs));
  c_ObjectData::o_get(props);
}
bool c_parser::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 1:
      HASH_EXISTS_STRING(0x17B591196693E481LL, mPPNodeCount, 12);
      break;
    case 6:
      HASH_EXISTS_STRING(0x29A48DB6C6A9A306LL, mTransparentTagHooks, 20);
      break;
    case 13:
      HASH_EXISTS_STRING(0x3BE32D09F769180DLL, mIncludeSizes, 13);
      break;
    case 14:
      HASH_EXISTS_STRING(0x61D533DDE0E29D8ELL, mConf, 5);
      break;
    case 15:
      HASH_EXISTS_STRING(0x078BB50DFD81B50FLL, mVariables, 10);
      break;
    case 18:
      HASH_EXISTS_STRING(0x54CDA5D215104C92LL, mImageParamsMagicArray, 22);
      break;
    case 23:
      HASH_EXISTS_STRING(0x79B0EDF6D034C797LL, mFunctionSynonyms, 17);
      break;
    case 30:
      HASH_EXISTS_STRING(0x268656C76979C11ELL, mImageParams, 12);
      break;
    case 32:
      HASH_EXISTS_STRING(0x1F6884F6D1BD5420LL, mTplDomCache, 12);
      break;
    case 36:
      HASH_EXISTS_STRING(0x2D440400444AAAA4LL, mTagHooks, 9);
      break;
    case 37:
      HASH_EXISTS_STRING(0x51106CF09F297525LL, mIncludeCount, 13);
      break;
    case 43:
      HASH_EXISTS_STRING(0x0E51B7CA30E388ABLL, mDTopen, 7);
      break;
    case 47:
      HASH_EXISTS_STRING(0x3A5EAF7D0A0A62AFLL, mOutputType, 11);
      break;
    case 48:
      HASH_EXISTS_STRING(0x6017CB24F50048B0LL, mInPre, 6);
      break;
    case 49:
      HASH_EXISTS_STRING(0x246A9B9AF3CF9F31LL, mMarkerIndex, 12);
      break;
    case 51:
      HASH_EXISTS_STRING(0x23ADD91853701033LL, mFunctionHooks, 14);
      break;
    case 53:
      HASH_EXISTS_STRING(0x0BF9A1BCA38BE1B5LL, mTplExpandCache, 15);
      break;
    case 65:
      HASH_EXISTS_STRING(0x4DDE8DD427CE5241LL, mRevisionId, 11);
      break;
    case 69:
      HASH_EXISTS_STRING(0x32D18CCCD8EDEA45LL, mDoubleUnderscores, 18);
      break;
    case 70:
      HASH_EXISTS_STRING(0x040877553522A8C6LL, mStripList, 10);
      break;
    case 77:
      HASH_EXISTS_STRING(0x48663D83DD673D4DLL, ot, 2);
      break;
    case 78:
      HASH_EXISTS_STRING(0x65F5C1EF7BB90DCELL, mRevisionTimestamp, 18);
      break;
    case 79:
      HASH_EXISTS_STRING(0x0521FE0F0419AF4FLL, mLinkHolders, 12);
      break;
    case 80:
      HASH_EXISTS_STRING(0x7F7FE020D521DBD0LL, mOutput, 7);
      break;
    case 92:
      HASH_EXISTS_STRING(0x3231E133A048DBDCLL, mExpensiveFunctionCount, 23);
      break;
    case 94:
      HASH_EXISTS_STRING(0x22557F1A510D09DELL, mUrlProtocols, 13);
      break;
    case 97:
      HASH_EXISTS_STRING(0x0ACBB98FE340E8E1LL, mPreprocessor, 13);
      break;
    case 98:
      HASH_EXISTS_STRING(0x1AA92728432688E2LL, mAutonumber, 11);
      break;
    case 102:
      HASH_EXISTS_STRING(0x62355D375852DB66LL, mLastSection, 12);
      break;
    case 105:
      HASH_EXISTS_STRING(0x7207EA9A5BBFBF69LL, mStripState, 11);
      HASH_EXISTS_STRING(0x79AC63B0E96DC6E9LL, mOptions, 8);
      break;
    case 106:
      HASH_EXISTS_STRING(0x68120084F0DA35EALL, mFunctionTagHooks, 17);
      HASH_EXISTS_STRING(0x4C804C46307BF0EALL, mTitle, 6);
      break;
    case 107:
      HASH_EXISTS_STRING(0x1B2FB1E1A358AEEBLL, mVarCache, 9);
      HASH_EXISTS_STRING(0x5EB895444FBC1E6BLL, mHeadings, 9);
      break;
    case 108:
      HASH_EXISTS_STRING(0x00533A290681F3ECLL, mRevIdForTs, 11);
      break;
    case 114:
      HASH_EXISTS_STRING(0x1C236AA02D253072LL, mArgStack, 9);
      break;
    case 115:
      HASH_EXISTS_STRING(0x60D43FEB822D1C73LL, mDefaultStripList, 17);
      HASH_EXISTS_STRING(0x2033A5C7AD350773LL, mDefaultSort, 12);
      break;
    case 117:
      HASH_EXISTS_STRING(0x69D407F2FBC52275LL, mLinkID, 7);
      break;
    case 119:
      HASH_EXISTS_STRING(0x627C20A98F61F877LL, mExtLinkBracketedRegex, 22);
      break;
    case 125:
      HASH_EXISTS_STRING(0x43595FF2293B047DLL, mTplRedirCache, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_parser::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 1:
      HASH_RETURN_STRING(0x17B591196693E481LL, m_mPPNodeCount,
                         mPPNodeCount, 12);
      break;
    case 6:
      HASH_RETURN_STRING(0x29A48DB6C6A9A306LL, m_mTransparentTagHooks,
                         mTransparentTagHooks, 20);
      break;
    case 13:
      HASH_RETURN_STRING(0x3BE32D09F769180DLL, m_mIncludeSizes,
                         mIncludeSizes, 13);
      break;
    case 14:
      HASH_RETURN_STRING(0x61D533DDE0E29D8ELL, m_mConf,
                         mConf, 5);
      break;
    case 15:
      HASH_RETURN_STRING(0x078BB50DFD81B50FLL, m_mVariables,
                         mVariables, 10);
      break;
    case 18:
      HASH_RETURN_STRING(0x54CDA5D215104C92LL, m_mImageParamsMagicArray,
                         mImageParamsMagicArray, 22);
      break;
    case 23:
      HASH_RETURN_STRING(0x79B0EDF6D034C797LL, m_mFunctionSynonyms,
                         mFunctionSynonyms, 17);
      break;
    case 30:
      HASH_RETURN_STRING(0x268656C76979C11ELL, m_mImageParams,
                         mImageParams, 12);
      break;
    case 32:
      HASH_RETURN_STRING(0x1F6884F6D1BD5420LL, m_mTplDomCache,
                         mTplDomCache, 12);
      break;
    case 36:
      HASH_RETURN_STRING(0x2D440400444AAAA4LL, m_mTagHooks,
                         mTagHooks, 9);
      break;
    case 37:
      HASH_RETURN_STRING(0x51106CF09F297525LL, m_mIncludeCount,
                         mIncludeCount, 13);
      break;
    case 43:
      HASH_RETURN_STRING(0x0E51B7CA30E388ABLL, m_mDTopen,
                         mDTopen, 7);
      break;
    case 47:
      HASH_RETURN_STRING(0x3A5EAF7D0A0A62AFLL, m_mOutputType,
                         mOutputType, 11);
      break;
    case 48:
      HASH_RETURN_STRING(0x6017CB24F50048B0LL, m_mInPre,
                         mInPre, 6);
      break;
    case 49:
      HASH_RETURN_STRING(0x246A9B9AF3CF9F31LL, m_mMarkerIndex,
                         mMarkerIndex, 12);
      break;
    case 51:
      HASH_RETURN_STRING(0x23ADD91853701033LL, m_mFunctionHooks,
                         mFunctionHooks, 14);
      break;
    case 53:
      HASH_RETURN_STRING(0x0BF9A1BCA38BE1B5LL, m_mTplExpandCache,
                         mTplExpandCache, 15);
      break;
    case 65:
      HASH_RETURN_STRING(0x4DDE8DD427CE5241LL, m_mRevisionId,
                         mRevisionId, 11);
      break;
    case 69:
      HASH_RETURN_STRING(0x32D18CCCD8EDEA45LL, m_mDoubleUnderscores,
                         mDoubleUnderscores, 18);
      break;
    case 70:
      HASH_RETURN_STRING(0x040877553522A8C6LL, m_mStripList,
                         mStripList, 10);
      break;
    case 77:
      HASH_RETURN_STRING(0x48663D83DD673D4DLL, m_ot,
                         ot, 2);
      break;
    case 78:
      HASH_RETURN_STRING(0x65F5C1EF7BB90DCELL, m_mRevisionTimestamp,
                         mRevisionTimestamp, 18);
      break;
    case 79:
      HASH_RETURN_STRING(0x0521FE0F0419AF4FLL, m_mLinkHolders,
                         mLinkHolders, 12);
      break;
    case 80:
      HASH_RETURN_STRING(0x7F7FE020D521DBD0LL, m_mOutput,
                         mOutput, 7);
      break;
    case 92:
      HASH_RETURN_STRING(0x3231E133A048DBDCLL, m_mExpensiveFunctionCount,
                         mExpensiveFunctionCount, 23);
      break;
    case 94:
      HASH_RETURN_STRING(0x22557F1A510D09DELL, m_mUrlProtocols,
                         mUrlProtocols, 13);
      break;
    case 97:
      HASH_RETURN_STRING(0x0ACBB98FE340E8E1LL, m_mPreprocessor,
                         mPreprocessor, 13);
      break;
    case 98:
      HASH_RETURN_STRING(0x1AA92728432688E2LL, m_mAutonumber,
                         mAutonumber, 11);
      break;
    case 102:
      HASH_RETURN_STRING(0x62355D375852DB66LL, m_mLastSection,
                         mLastSection, 12);
      break;
    case 105:
      HASH_RETURN_STRING(0x7207EA9A5BBFBF69LL, m_mStripState,
                         mStripState, 11);
      HASH_RETURN_STRING(0x79AC63B0E96DC6E9LL, m_mOptions,
                         mOptions, 8);
      break;
    case 106:
      HASH_RETURN_STRING(0x68120084F0DA35EALL, m_mFunctionTagHooks,
                         mFunctionTagHooks, 17);
      HASH_RETURN_STRING(0x4C804C46307BF0EALL, m_mTitle,
                         mTitle, 6);
      break;
    case 107:
      HASH_RETURN_STRING(0x1B2FB1E1A358AEEBLL, m_mVarCache,
                         mVarCache, 9);
      HASH_RETURN_STRING(0x5EB895444FBC1E6BLL, m_mHeadings,
                         mHeadings, 9);
      break;
    case 108:
      HASH_RETURN_STRING(0x00533A290681F3ECLL, m_mRevIdForTs,
                         mRevIdForTs, 11);
      break;
    case 114:
      HASH_RETURN_STRING(0x1C236AA02D253072LL, m_mArgStack,
                         mArgStack, 9);
      break;
    case 115:
      HASH_RETURN_STRING(0x60D43FEB822D1C73LL, m_mDefaultStripList,
                         mDefaultStripList, 17);
      HASH_RETURN_STRING(0x2033A5C7AD350773LL, m_mDefaultSort,
                         mDefaultSort, 12);
      break;
    case 117:
      HASH_RETURN_STRING(0x69D407F2FBC52275LL, m_mLinkID,
                         mLinkID, 7);
      break;
    case 119:
      HASH_RETURN_STRING(0x627C20A98F61F877LL, m_mExtLinkBracketedRegex,
                         mExtLinkBracketedRegex, 22);
      break;
    case 125:
      HASH_RETURN_STRING(0x43595FF2293B047DLL, m_mTplRedirCache,
                         mTplRedirCache, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_parser::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 1:
      HASH_SET_STRING(0x17B591196693E481LL, m_mPPNodeCount,
                      mPPNodeCount, 12);
      break;
    case 6:
      HASH_SET_STRING(0x29A48DB6C6A9A306LL, m_mTransparentTagHooks,
                      mTransparentTagHooks, 20);
      break;
    case 13:
      HASH_SET_STRING(0x3BE32D09F769180DLL, m_mIncludeSizes,
                      mIncludeSizes, 13);
      break;
    case 14:
      HASH_SET_STRING(0x61D533DDE0E29D8ELL, m_mConf,
                      mConf, 5);
      break;
    case 15:
      HASH_SET_STRING(0x078BB50DFD81B50FLL, m_mVariables,
                      mVariables, 10);
      break;
    case 18:
      HASH_SET_STRING(0x54CDA5D215104C92LL, m_mImageParamsMagicArray,
                      mImageParamsMagicArray, 22);
      break;
    case 23:
      HASH_SET_STRING(0x79B0EDF6D034C797LL, m_mFunctionSynonyms,
                      mFunctionSynonyms, 17);
      break;
    case 30:
      HASH_SET_STRING(0x268656C76979C11ELL, m_mImageParams,
                      mImageParams, 12);
      break;
    case 32:
      HASH_SET_STRING(0x1F6884F6D1BD5420LL, m_mTplDomCache,
                      mTplDomCache, 12);
      break;
    case 36:
      HASH_SET_STRING(0x2D440400444AAAA4LL, m_mTagHooks,
                      mTagHooks, 9);
      break;
    case 37:
      HASH_SET_STRING(0x51106CF09F297525LL, m_mIncludeCount,
                      mIncludeCount, 13);
      break;
    case 43:
      HASH_SET_STRING(0x0E51B7CA30E388ABLL, m_mDTopen,
                      mDTopen, 7);
      break;
    case 47:
      HASH_SET_STRING(0x3A5EAF7D0A0A62AFLL, m_mOutputType,
                      mOutputType, 11);
      break;
    case 48:
      HASH_SET_STRING(0x6017CB24F50048B0LL, m_mInPre,
                      mInPre, 6);
      break;
    case 49:
      HASH_SET_STRING(0x246A9B9AF3CF9F31LL, m_mMarkerIndex,
                      mMarkerIndex, 12);
      break;
    case 51:
      HASH_SET_STRING(0x23ADD91853701033LL, m_mFunctionHooks,
                      mFunctionHooks, 14);
      break;
    case 53:
      HASH_SET_STRING(0x0BF9A1BCA38BE1B5LL, m_mTplExpandCache,
                      mTplExpandCache, 15);
      break;
    case 65:
      HASH_SET_STRING(0x4DDE8DD427CE5241LL, m_mRevisionId,
                      mRevisionId, 11);
      break;
    case 69:
      HASH_SET_STRING(0x32D18CCCD8EDEA45LL, m_mDoubleUnderscores,
                      mDoubleUnderscores, 18);
      break;
    case 70:
      HASH_SET_STRING(0x040877553522A8C6LL, m_mStripList,
                      mStripList, 10);
      break;
    case 77:
      HASH_SET_STRING(0x48663D83DD673D4DLL, m_ot,
                      ot, 2);
      break;
    case 78:
      HASH_SET_STRING(0x65F5C1EF7BB90DCELL, m_mRevisionTimestamp,
                      mRevisionTimestamp, 18);
      break;
    case 79:
      HASH_SET_STRING(0x0521FE0F0419AF4FLL, m_mLinkHolders,
                      mLinkHolders, 12);
      break;
    case 80:
      HASH_SET_STRING(0x7F7FE020D521DBD0LL, m_mOutput,
                      mOutput, 7);
      break;
    case 92:
      HASH_SET_STRING(0x3231E133A048DBDCLL, m_mExpensiveFunctionCount,
                      mExpensiveFunctionCount, 23);
      break;
    case 94:
      HASH_SET_STRING(0x22557F1A510D09DELL, m_mUrlProtocols,
                      mUrlProtocols, 13);
      break;
    case 97:
      HASH_SET_STRING(0x0ACBB98FE340E8E1LL, m_mPreprocessor,
                      mPreprocessor, 13);
      break;
    case 98:
      HASH_SET_STRING(0x1AA92728432688E2LL, m_mAutonumber,
                      mAutonumber, 11);
      break;
    case 102:
      HASH_SET_STRING(0x62355D375852DB66LL, m_mLastSection,
                      mLastSection, 12);
      break;
    case 105:
      HASH_SET_STRING(0x7207EA9A5BBFBF69LL, m_mStripState,
                      mStripState, 11);
      HASH_SET_STRING(0x79AC63B0E96DC6E9LL, m_mOptions,
                      mOptions, 8);
      break;
    case 106:
      HASH_SET_STRING(0x68120084F0DA35EALL, m_mFunctionTagHooks,
                      mFunctionTagHooks, 17);
      HASH_SET_STRING(0x4C804C46307BF0EALL, m_mTitle,
                      mTitle, 6);
      break;
    case 107:
      HASH_SET_STRING(0x1B2FB1E1A358AEEBLL, m_mVarCache,
                      mVarCache, 9);
      HASH_SET_STRING(0x5EB895444FBC1E6BLL, m_mHeadings,
                      mHeadings, 9);
      break;
    case 108:
      HASH_SET_STRING(0x00533A290681F3ECLL, m_mRevIdForTs,
                      mRevIdForTs, 11);
      break;
    case 114:
      HASH_SET_STRING(0x1C236AA02D253072LL, m_mArgStack,
                      mArgStack, 9);
      break;
    case 115:
      HASH_SET_STRING(0x60D43FEB822D1C73LL, m_mDefaultStripList,
                      mDefaultStripList, 17);
      HASH_SET_STRING(0x2033A5C7AD350773LL, m_mDefaultSort,
                      mDefaultSort, 12);
      break;
    case 117:
      HASH_SET_STRING(0x69D407F2FBC52275LL, m_mLinkID,
                      mLinkID, 7);
      break;
    case 119:
      HASH_SET_STRING(0x627C20A98F61F877LL, m_mExtLinkBracketedRegex,
                      mExtLinkBracketedRegex, 22);
      break;
    case 125:
      HASH_SET_STRING(0x43595FF2293B047DLL, m_mTplRedirCache,
                      mTplRedirCache, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_parser::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 127) {
    case 1:
      HASH_RETURN_STRING(0x17B591196693E481LL, m_mPPNodeCount,
                         mPPNodeCount, 12);
      break;
    case 6:
      HASH_RETURN_STRING(0x29A48DB6C6A9A306LL, m_mTransparentTagHooks,
                         mTransparentTagHooks, 20);
      break;
    case 13:
      HASH_RETURN_STRING(0x3BE32D09F769180DLL, m_mIncludeSizes,
                         mIncludeSizes, 13);
      break;
    case 14:
      HASH_RETURN_STRING(0x61D533DDE0E29D8ELL, m_mConf,
                         mConf, 5);
      break;
    case 15:
      HASH_RETURN_STRING(0x078BB50DFD81B50FLL, m_mVariables,
                         mVariables, 10);
      break;
    case 18:
      HASH_RETURN_STRING(0x54CDA5D215104C92LL, m_mImageParamsMagicArray,
                         mImageParamsMagicArray, 22);
      break;
    case 23:
      HASH_RETURN_STRING(0x79B0EDF6D034C797LL, m_mFunctionSynonyms,
                         mFunctionSynonyms, 17);
      break;
    case 30:
      HASH_RETURN_STRING(0x268656C76979C11ELL, m_mImageParams,
                         mImageParams, 12);
      break;
    case 32:
      HASH_RETURN_STRING(0x1F6884F6D1BD5420LL, m_mTplDomCache,
                         mTplDomCache, 12);
      break;
    case 36:
      HASH_RETURN_STRING(0x2D440400444AAAA4LL, m_mTagHooks,
                         mTagHooks, 9);
      break;
    case 37:
      HASH_RETURN_STRING(0x51106CF09F297525LL, m_mIncludeCount,
                         mIncludeCount, 13);
      break;
    case 43:
      HASH_RETURN_STRING(0x0E51B7CA30E388ABLL, m_mDTopen,
                         mDTopen, 7);
      break;
    case 47:
      HASH_RETURN_STRING(0x3A5EAF7D0A0A62AFLL, m_mOutputType,
                         mOutputType, 11);
      break;
    case 48:
      HASH_RETURN_STRING(0x6017CB24F50048B0LL, m_mInPre,
                         mInPre, 6);
      break;
    case 49:
      HASH_RETURN_STRING(0x246A9B9AF3CF9F31LL, m_mMarkerIndex,
                         mMarkerIndex, 12);
      break;
    case 51:
      HASH_RETURN_STRING(0x23ADD91853701033LL, m_mFunctionHooks,
                         mFunctionHooks, 14);
      break;
    case 53:
      HASH_RETURN_STRING(0x0BF9A1BCA38BE1B5LL, m_mTplExpandCache,
                         mTplExpandCache, 15);
      break;
    case 65:
      HASH_RETURN_STRING(0x4DDE8DD427CE5241LL, m_mRevisionId,
                         mRevisionId, 11);
      break;
    case 69:
      HASH_RETURN_STRING(0x32D18CCCD8EDEA45LL, m_mDoubleUnderscores,
                         mDoubleUnderscores, 18);
      break;
    case 70:
      HASH_RETURN_STRING(0x040877553522A8C6LL, m_mStripList,
                         mStripList, 10);
      break;
    case 77:
      HASH_RETURN_STRING(0x48663D83DD673D4DLL, m_ot,
                         ot, 2);
      break;
    case 78:
      HASH_RETURN_STRING(0x65F5C1EF7BB90DCELL, m_mRevisionTimestamp,
                         mRevisionTimestamp, 18);
      break;
    case 79:
      HASH_RETURN_STRING(0x0521FE0F0419AF4FLL, m_mLinkHolders,
                         mLinkHolders, 12);
      break;
    case 80:
      HASH_RETURN_STRING(0x7F7FE020D521DBD0LL, m_mOutput,
                         mOutput, 7);
      break;
    case 92:
      HASH_RETURN_STRING(0x3231E133A048DBDCLL, m_mExpensiveFunctionCount,
                         mExpensiveFunctionCount, 23);
      break;
    case 94:
      HASH_RETURN_STRING(0x22557F1A510D09DELL, m_mUrlProtocols,
                         mUrlProtocols, 13);
      break;
    case 97:
      HASH_RETURN_STRING(0x0ACBB98FE340E8E1LL, m_mPreprocessor,
                         mPreprocessor, 13);
      break;
    case 98:
      HASH_RETURN_STRING(0x1AA92728432688E2LL, m_mAutonumber,
                         mAutonumber, 11);
      break;
    case 102:
      HASH_RETURN_STRING(0x62355D375852DB66LL, m_mLastSection,
                         mLastSection, 12);
      break;
    case 105:
      HASH_RETURN_STRING(0x7207EA9A5BBFBF69LL, m_mStripState,
                         mStripState, 11);
      HASH_RETURN_STRING(0x79AC63B0E96DC6E9LL, m_mOptions,
                         mOptions, 8);
      break;
    case 106:
      HASH_RETURN_STRING(0x68120084F0DA35EALL, m_mFunctionTagHooks,
                         mFunctionTagHooks, 17);
      HASH_RETURN_STRING(0x4C804C46307BF0EALL, m_mTitle,
                         mTitle, 6);
      break;
    case 107:
      HASH_RETURN_STRING(0x1B2FB1E1A358AEEBLL, m_mVarCache,
                         mVarCache, 9);
      HASH_RETURN_STRING(0x5EB895444FBC1E6BLL, m_mHeadings,
                         mHeadings, 9);
      break;
    case 108:
      HASH_RETURN_STRING(0x00533A290681F3ECLL, m_mRevIdForTs,
                         mRevIdForTs, 11);
      break;
    case 114:
      HASH_RETURN_STRING(0x1C236AA02D253072LL, m_mArgStack,
                         mArgStack, 9);
      break;
    case 115:
      HASH_RETURN_STRING(0x60D43FEB822D1C73LL, m_mDefaultStripList,
                         mDefaultStripList, 17);
      HASH_RETURN_STRING(0x2033A5C7AD350773LL, m_mDefaultSort,
                         mDefaultSort, 12);
      break;
    case 117:
      HASH_RETURN_STRING(0x69D407F2FBC52275LL, m_mLinkID,
                         mLinkID, 7);
      break;
    case 119:
      HASH_RETURN_STRING(0x627C20A98F61F877LL, m_mExtLinkBracketedRegex,
                         mExtLinkBracketedRegex, 22);
      break;
    case 125:
      HASH_RETURN_STRING(0x43595FF2293B047DLL, m_mTplRedirCache,
                         mTplRedirCache, 14);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_parser::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 0:
      HASH_RETURN(0x7A3DEE794AAE0080LL, q_parser_COLON_STATE_COMMENTDASHDASH, COLON_STATE_COMMENTDASHDASH);
      break;
    case 3:
      HASH_RETURN(0x39454943C9F77183LL, q_parser_SFH_NO_HASH, SFH_NO_HASH);
      break;
    case 8:
      HASH_RETURN(0x53D3618CFDD02A88LL, q_parser_COLON_STATE_CLOSETAG, COLON_STATE_CLOSETAG);
      HASH_RETURN(0x6183A19A97D054C8LL, q_parser_PTD_FOR_INCLUSION, PTD_FOR_INCLUSION);
      break;
    case 12:
      HASH_RETURN(0x7C7C96A77F65158CLL, q_parser_SFH_OBJECT_ARGS, SFH_OBJECT_ARGS);
      break;
    case 13:
      HASH_RETURN(0x63DD546A4381040DLL, q_parser_OT_MSG, OT_MSG);
      break;
    case 21:
      HASH_RETURN(0x44687B3BB0AA9915LL, q_parser_COLON_STATE_TAGSTART, COLON_STATE_TAGSTART);
      break;
    case 24:
      HASH_RETURN(0x5E2B4F68FF786898LL, q_parser_OT_WIKI, OT_WIKI);
      break;
    case 25:
      HASH_RETURN(0x3299493D3953AF19LL, q_parser_MARKER_SUFFIX, MARKER_SUFFIX);
      break;
    case 27:
      HASH_RETURN(0x4835CF75ECA1675BLL, q_parser_COLON_STATE_TAG, COLON_STATE_TAG);
      break;
    case 38:
      HASH_RETURN(0x4585610E25B87326LL, q_parser_COLON_STATE_TAGSLASH, COLON_STATE_TAGSLASH);
      break;
    case 40:
      HASH_RETURN(0x753F5E8DD64BF868LL, q_parser_COLON_STATE_COMMENTDASH, COLON_STATE_COMMENTDASH);
      break;
    case 42:
      HASH_RETURN(0x5AE41239FF63D86ALL, q_parser_VERSION, VERSION);
      HASH_RETURN(0x53D1BB8BE4436E6ALL, q_parser_EXT_IMAGE_REGEX, EXT_IMAGE_REGEX);
      break;
    case 45:
      HASH_RETURN(0x5DADFDAB77C589ADLL, q_parser_COLON_STATE_COMMENT, COLON_STATE_COMMENT);
      break;
    case 48:
      HASH_RETURN(0x4C1D0C36632BDF70LL, q_parser_OT_HTML, OT_HTML);
      break;
    case 50:
      HASH_RETURN(0x38F0CBBBEA7F23F2LL, q_parser_COLON_STATE_TEXT, COLON_STATE_TEXT);
      break;
    case 56:
      HASH_RETURN(0x63F95DCAC9867D78LL, q_parser_EXT_LINK_URL_CLASS, EXT_LINK_URL_CLASS);
      break;
    case 58:
      HASH_RETURN(0x45EBC8EBC4C81F3ALL, q_parser_OT_PREPROCESS, OT_PREPROCESS);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(parser)
ObjectData *c_parser::create(Variant v_conf //  = ScalarArrays::sa_[0]
) {
  init();
  t___construct(v_conf);
  return this;
}
ObjectData *c_parser::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_parser::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
void c_parser::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_parser::cloneImpl() {
  c_parser *obj = NEW(c_parser)();
  cloneSet(obj);
  return obj;
}
void c_parser::cloneSet(c_parser *clone) {
  clone->m_mTagHooks = m_mTagHooks.isReferenced() ? ref(m_mTagHooks) : m_mTagHooks;
  clone->m_mTransparentTagHooks = m_mTransparentTagHooks.isReferenced() ? ref(m_mTransparentTagHooks) : m_mTransparentTagHooks;
  clone->m_mFunctionHooks = m_mFunctionHooks.isReferenced() ? ref(m_mFunctionHooks) : m_mFunctionHooks;
  clone->m_mFunctionSynonyms = m_mFunctionSynonyms.isReferenced() ? ref(m_mFunctionSynonyms) : m_mFunctionSynonyms;
  clone->m_mVariables = m_mVariables.isReferenced() ? ref(m_mVariables) : m_mVariables;
  clone->m_mImageParams = m_mImageParams.isReferenced() ? ref(m_mImageParams) : m_mImageParams;
  clone->m_mImageParamsMagicArray = m_mImageParamsMagicArray.isReferenced() ? ref(m_mImageParamsMagicArray) : m_mImageParamsMagicArray;
  clone->m_mStripList = m_mStripList.isReferenced() ? ref(m_mStripList) : m_mStripList;
  clone->m_mMarkerIndex = m_mMarkerIndex.isReferenced() ? ref(m_mMarkerIndex) : m_mMarkerIndex;
  clone->m_mPreprocessor = m_mPreprocessor.isReferenced() ? ref(m_mPreprocessor) : m_mPreprocessor;
  clone->m_mExtLinkBracketedRegex = m_mExtLinkBracketedRegex.isReferenced() ? ref(m_mExtLinkBracketedRegex) : m_mExtLinkBracketedRegex;
  clone->m_mUrlProtocols = m_mUrlProtocols.isReferenced() ? ref(m_mUrlProtocols) : m_mUrlProtocols;
  clone->m_mDefaultStripList = m_mDefaultStripList.isReferenced() ? ref(m_mDefaultStripList) : m_mDefaultStripList;
  clone->m_mVarCache = m_mVarCache.isReferenced() ? ref(m_mVarCache) : m_mVarCache;
  clone->m_mConf = m_mConf.isReferenced() ? ref(m_mConf) : m_mConf;
  clone->m_mFunctionTagHooks = m_mFunctionTagHooks.isReferenced() ? ref(m_mFunctionTagHooks) : m_mFunctionTagHooks;
  clone->m_mOutput = m_mOutput.isReferenced() ? ref(m_mOutput) : m_mOutput;
  clone->m_mAutonumber = m_mAutonumber.isReferenced() ? ref(m_mAutonumber) : m_mAutonumber;
  clone->m_mDTopen = m_mDTopen.isReferenced() ? ref(m_mDTopen) : m_mDTopen;
  clone->m_mStripState = m_mStripState.isReferenced() ? ref(m_mStripState) : m_mStripState;
  clone->m_mIncludeCount = m_mIncludeCount.isReferenced() ? ref(m_mIncludeCount) : m_mIncludeCount;
  clone->m_mArgStack = m_mArgStack.isReferenced() ? ref(m_mArgStack) : m_mArgStack;
  clone->m_mLastSection = m_mLastSection.isReferenced() ? ref(m_mLastSection) : m_mLastSection;
  clone->m_mInPre = m_mInPre.isReferenced() ? ref(m_mInPre) : m_mInPre;
  clone->m_mLinkHolders = m_mLinkHolders.isReferenced() ? ref(m_mLinkHolders) : m_mLinkHolders;
  clone->m_mLinkID = m_mLinkID.isReferenced() ? ref(m_mLinkID) : m_mLinkID;
  clone->m_mIncludeSizes = m_mIncludeSizes.isReferenced() ? ref(m_mIncludeSizes) : m_mIncludeSizes;
  clone->m_mPPNodeCount = m_mPPNodeCount.isReferenced() ? ref(m_mPPNodeCount) : m_mPPNodeCount;
  clone->m_mDefaultSort = m_mDefaultSort.isReferenced() ? ref(m_mDefaultSort) : m_mDefaultSort;
  clone->m_mTplExpandCache = m_mTplExpandCache.isReferenced() ? ref(m_mTplExpandCache) : m_mTplExpandCache;
  clone->m_mTplRedirCache = m_mTplRedirCache.isReferenced() ? ref(m_mTplRedirCache) : m_mTplRedirCache;
  clone->m_mTplDomCache = m_mTplDomCache.isReferenced() ? ref(m_mTplDomCache) : m_mTplDomCache;
  clone->m_mHeadings = m_mHeadings.isReferenced() ? ref(m_mHeadings) : m_mHeadings;
  clone->m_mDoubleUnderscores = m_mDoubleUnderscores.isReferenced() ? ref(m_mDoubleUnderscores) : m_mDoubleUnderscores;
  clone->m_mExpensiveFunctionCount = m_mExpensiveFunctionCount.isReferenced() ? ref(m_mExpensiveFunctionCount) : m_mExpensiveFunctionCount;
  clone->m_mOptions = m_mOptions.isReferenced() ? ref(m_mOptions) : m_mOptions;
  clone->m_mTitle = m_mTitle.isReferenced() ? ref(m_mTitle) : m_mTitle;
  clone->m_mOutputType = m_mOutputType.isReferenced() ? ref(m_mOutputType) : m_mOutputType;
  clone->m_ot = m_ot.isReferenced() ? ref(m_ot) : m_ot;
  clone->m_mRevisionId = m_mRevisionId.isReferenced() ? ref(m_mRevisionId) : m_mRevisionId;
  clone->m_mRevisionTimestamp = m_mRevisionTimestamp.isReferenced() ? ref(m_mRevisionTimestamp) : m_mRevisionTimestamp;
  clone->m_mRevIdForTs = m_mRevIdForTs.isReferenced() ? ref(m_mRevIdForTs) : m_mRevIdForTs;
  ObjectData::cloneSet(clone);
}
Variant c_parser::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 5:
      HASH_GUARD(0x04CA531DD3FB2705LL, attributestripcallback) {
        int count = params.size();
        if (count <= 1) return (t_attributestripcallback(ref(const_cast<Array&>(params).lvalAt(0))));
        return (t_attributestripcallback(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)));
      }
      HASH_GUARD(0x298D74DE5F3B8B45LL, magiclinkcallback) {
        return (t_magiclinkcallback(params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x333DE406BD9436D1LL, argsubstitution) {
        return (t_argsubstitution(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 19:
      HASH_GUARD(0x6159C1C3A582E4D3LL, incrementexpensivefunctioncount) {
        return (t_incrementexpensivefunctioncount());
      }
      break;
    case 20:
      HASH_GUARD(0x07B2DE5DC3BC16D4LL, setfunctionhook) {
        int count = params.size();
        if (count <= 2) return (t_setfunctionhook(params.rvalAt(0), params.rvalAt(1)));
        return (t_setfunctionhook(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      HASH_GUARD(0x4AC114BB40027BD4LL, renderpretag) {
        return (t_renderpretag(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 23:
      HASH_GUARD(0x7519C1FAF33D7857LL, getoutput) {
        return (t_getoutput());
      }
      break;
    case 24:
      HASH_GUARD(0x1919E33A3EB87298LL, markerskipcallback) {
        return (t_markerskipcallback(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 28:
      HASH_GUARD(0x6F53A1945EB13C5CLL, replaceunusualescapescallback) {
        return (ti_replaceunusualescapescallback(o_getClassName(), params.rvalAt(0)));
      }
      break;
    case 30:
      HASH_GUARD(0x565D9E5A4801A09ELL, getcustomdefaultsort) {
        return (t_getcustomdefaultsort());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    case 34:
      HASH_GUARD(0x0A4CA37F020D70E2LL, insertstripitem) {
        return (t_insertstripitem(params.rvalAt(0)));
      }
      break;
    case 37:
      HASH_GUARD(0x443229B8642123A5LL, statelessfetchtemplate) {
        int count = params.size();
        if (count <= 1) return (ti_statelessfetchtemplate(o_getClassName(), params.rvalAt(0)));
        return (ti_statelessfetchtemplate(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 39:
      HASH_GUARD(0x6EFD1FF8956DBC27LL, nextlinkid) {
        return (t_nextlinkid());
      }
      break;
    case 41:
      HASH_GUARD(0x7D371E851A4798E9LL, extensionsubstitution) {
        return (t_extensionsubstitution(params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x2374179DF576FCA9LL, unstripnowiki) {
        return (t_unstripnowiki(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 43:
      HASH_GUARD(0x132FD54E0D0A99EBLL, title) {
        int count = params.size();
        if (count <= 0) return (t_title());
        return (t_title(params.rvalAt(0)));
      }
      break;
    case 47:
      HASH_GUARD(0x3BBB4BCA49D08AEFLL, getfunctionlang) {
        return (t_getfunctionlang());
      }
      HASH_GUARD(0x7E4180EEBEB7A62FLL, doquotes) {
        return (t_doquotes(params.rvalAt(0)));
      }
      break;
    case 50:
      HASH_GUARD(0x0FE91E46B43AEF32LL, setdefaultsort) {
        return (t_setdefaultsort(params.rvalAt(0)), null);
      }
      break;
    case 51:
      HASH_GUARD(0x7F974836AACC1EF3LL, __destruct) {
        return (t___destruct());
      }
      break;
    case 53:
      HASH_GUARD(0x151B6C9D84BBC1F5LL, getstriplist) {
        return (t_getstriplist());
      }
      HASH_GUARD(0x241A6011A10C7EF5LL, firstcallinit) {
        return (t_firstcallinit(), null);
      }
      break;
    case 54:
      HASH_GUARD(0x37AA0C66F5C36136LL, replaceinternallinks2) {
        return (t_replaceinternallinks2(ref(const_cast<Array&>(params).lvalAt(0))));
      }
      break;
    case 55:
      HASH_GUARD(0x392E383BDD5B10F7LL, bracesubstitution) {
        return (t_bracesubstitution(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 57:
      HASH_GUARD(0x2040B36587CE56F9LL, getoptions) {
        return (t_getoptions());
      }
      break;
    case 58:
      HASH_GUARD(0x655F4A045CA8303ALL, replacevariables) {
        int count = params.size();
        if (count <= 1) return (t_replacevariables(params.rvalAt(0)));
        if (count == 2) return (t_replacevariables(params.rvalAt(0), params.rvalAt(1)));
        return (t_replacevariables(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      HASH_GUARD(0x39ADF3C1E6A55D3ALL, getdefaultsort) {
        return (t_getdefaultsort());
      }
      HASH_GUARD(0x524D522D7D9607FALL, uniqprefix) {
        return (t_uniqprefix());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_parser::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 5:
      HASH_GUARD(0x04CA531DD3FB2705LL, attributestripcallback) {
        if (count <= 1) return (t_attributestripcallback(ref(a0)));
        return (t_attributestripcallback(ref(a0), a1));
      }
      HASH_GUARD(0x298D74DE5F3B8B45LL, magiclinkcallback) {
        return (t_magiclinkcallback(a0));
      }
      break;
    case 17:
      HASH_GUARD(0x333DE406BD9436D1LL, argsubstitution) {
        return (t_argsubstitution(a0, a1));
      }
      break;
    case 19:
      HASH_GUARD(0x6159C1C3A582E4D3LL, incrementexpensivefunctioncount) {
        return (t_incrementexpensivefunctioncount());
      }
      break;
    case 20:
      HASH_GUARD(0x07B2DE5DC3BC16D4LL, setfunctionhook) {
        if (count <= 2) return (t_setfunctionhook(a0, a1));
        return (t_setfunctionhook(a0, a1, a2));
      }
      HASH_GUARD(0x4AC114BB40027BD4LL, renderpretag) {
        return (t_renderpretag(a0, a1));
      }
      break;
    case 23:
      HASH_GUARD(0x7519C1FAF33D7857LL, getoutput) {
        return (t_getoutput());
      }
      break;
    case 24:
      HASH_GUARD(0x1919E33A3EB87298LL, markerskipcallback) {
        return (t_markerskipcallback(a0, a1));
      }
      break;
    case 28:
      HASH_GUARD(0x6F53A1945EB13C5CLL, replaceunusualescapescallback) {
        return (ti_replaceunusualescapescallback(o_getClassName(), a0));
      }
      break;
    case 30:
      HASH_GUARD(0x565D9E5A4801A09ELL, getcustomdefaultsort) {
        return (t_getcustomdefaultsort());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    case 34:
      HASH_GUARD(0x0A4CA37F020D70E2LL, insertstripitem) {
        return (t_insertstripitem(a0));
      }
      break;
    case 37:
      HASH_GUARD(0x443229B8642123A5LL, statelessfetchtemplate) {
        if (count <= 1) return (ti_statelessfetchtemplate(o_getClassName(), a0));
        return (ti_statelessfetchtemplate(o_getClassName(), a0, a1));
      }
      break;
    case 39:
      HASH_GUARD(0x6EFD1FF8956DBC27LL, nextlinkid) {
        return (t_nextlinkid());
      }
      break;
    case 41:
      HASH_GUARD(0x7D371E851A4798E9LL, extensionsubstitution) {
        return (t_extensionsubstitution(a0, a1));
      }
      HASH_GUARD(0x2374179DF576FCA9LL, unstripnowiki) {
        return (t_unstripnowiki(a0, a1));
      }
      break;
    case 43:
      HASH_GUARD(0x132FD54E0D0A99EBLL, title) {
        if (count <= 0) return (t_title());
        return (t_title(a0));
      }
      break;
    case 47:
      HASH_GUARD(0x3BBB4BCA49D08AEFLL, getfunctionlang) {
        return (t_getfunctionlang());
      }
      HASH_GUARD(0x7E4180EEBEB7A62FLL, doquotes) {
        return (t_doquotes(a0));
      }
      break;
    case 50:
      HASH_GUARD(0x0FE91E46B43AEF32LL, setdefaultsort) {
        return (t_setdefaultsort(a0), null);
      }
      break;
    case 51:
      HASH_GUARD(0x7F974836AACC1EF3LL, __destruct) {
        return (t___destruct());
      }
      break;
    case 53:
      HASH_GUARD(0x151B6C9D84BBC1F5LL, getstriplist) {
        return (t_getstriplist());
      }
      HASH_GUARD(0x241A6011A10C7EF5LL, firstcallinit) {
        return (t_firstcallinit(), null);
      }
      break;
    case 54:
      HASH_GUARD(0x37AA0C66F5C36136LL, replaceinternallinks2) {
        return (t_replaceinternallinks2(ref(a0)));
      }
      break;
    case 55:
      HASH_GUARD(0x392E383BDD5B10F7LL, bracesubstitution) {
        return (t_bracesubstitution(a0, a1));
      }
      break;
    case 57:
      HASH_GUARD(0x2040B36587CE56F9LL, getoptions) {
        return (t_getoptions());
      }
      break;
    case 58:
      HASH_GUARD(0x655F4A045CA8303ALL, replacevariables) {
        if (count <= 1) return (t_replacevariables(a0));
        if (count == 2) return (t_replacevariables(a0, a1));
        return (t_replacevariables(a0, a1, a2));
      }
      HASH_GUARD(0x39ADF3C1E6A55D3ALL, getdefaultsort) {
        return (t_getdefaultsort());
      }
      HASH_GUARD(0x524D522D7D9607FALL, uniqprefix) {
        return (t_uniqprefix());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parser::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x6F53A1945EB13C5CLL, replaceunusualescapescallback) {
        return (ti_replaceunusualescapescallback(c, params.rvalAt(0)));
      }
      break;
    case 1:
      HASH_GUARD(0x443229B8642123A5LL, statelessfetchtemplate) {
        int count = params.size();
        if (count <= 1) return (ti_statelessfetchtemplate(c, params.rvalAt(0)));
        return (ti_statelessfetchtemplate(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parser$os_get(const char *s) {
  return c_parser::os_get(s, -1);
}
Variant &cw_parser$os_lval(const char *s) {
  return c_parser::os_lval(s, -1);
}
Variant cw_parser$os_constant(const char *s) {
  return c_parser::os_constant(s);
}
Variant cw_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parser::os_invoke(c, s, params, -1, fatal);
}
void c_parser::init() {
  m_mTagHooks = null;
  m_mTransparentTagHooks = null;
  m_mFunctionHooks = null;
  m_mFunctionSynonyms = null;
  m_mVariables = null;
  m_mImageParams = null;
  m_mImageParamsMagicArray = null;
  m_mStripList = null;
  m_mMarkerIndex = null;
  m_mPreprocessor = null;
  m_mExtLinkBracketedRegex = null;
  m_mUrlProtocols = null;
  m_mDefaultStripList = null;
  m_mVarCache = null;
  m_mConf = null;
  m_mFunctionTagHooks = null;
  m_mOutput = null;
  m_mAutonumber = null;
  m_mDTopen = null;
  m_mStripState = null;
  m_mIncludeCount = null;
  m_mArgStack = null;
  m_mLastSection = null;
  m_mInPre = null;
  m_mLinkHolders = null;
  m_mLinkID = null;
  m_mIncludeSizes = null;
  m_mPPNodeCount = null;
  m_mDefaultSort = null;
  m_mTplExpandCache = null;
  m_mTplRedirCache = null;
  m_mTplDomCache = null;
  m_mHeadings = null;
  m_mDoubleUnderscores = null;
  m_mExpensiveFunctionCount = null;
  m_mOptions = null;
  m_mTitle = null;
  m_mOutputType = null;
  m_ot = null;
  m_mRevisionId = null;
  m_mRevisionTimestamp = null;
  m_mRevIdForTs = null;
}
/* SRC: Parser.php line 125 */
void c_parser::t___construct(Variant v_conf //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  (m_mConf = v_conf);
  (m_mTagHooks = ScalarArrays::sa_[0]);
  (m_mTransparentTagHooks = ScalarArrays::sa_[0]);
  (m_mFunctionHooks = ScalarArrays::sa_[0]);
  (m_mFunctionTagHooks = ScalarArrays::sa_[0]);
  (m_mFunctionSynonyms = ScalarArrays::sa_[14]);
  (m_mDefaultStripList = (m_mStripList = ScalarArrays::sa_[15]));
  (m_mUrlProtocols = LINE(133,invoke_failed("wfurlprotocols", Array(), 0x00000000BD4BD4FELL)));
  (m_mExtLinkBracketedRegex = LINE(135,(assignCallTemp(eo_1, toString(LINE(134,invoke_failed("wfurlprotocols", Array(), 0x00000000BD4BD4FELL)))),concat3("/\\[(\\b(", eo_1, ")[^][<>\"\\x00-\\x20\\x7F]+) *([^\\]\\x0a\\x0d]*\?)\\]/S"))));
  (m_mVarCache = ScalarArrays::sa_[0]);
  if (isset(v_conf, "preprocessorClass", 0x6C85AB6061598171LL)) {
    (o_lval("mPreprocessorClass", 0x6E5AA590C9CC679ALL) = v_conf.rvalAt("preprocessorClass", 0x6C85AB6061598171LL));
  }
  else if (LINE(139,x_extension_loaded("domxml"))) {
    LINE(141,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Warning: you have the obsolete domxml extension for PHP. Please remove it!\n").create()), 0x00000000E441E905LL));
    (o_lval("mPreprocessorClass", 0x6E5AA590C9CC679ALL) = "Preprocessor_Hash");
  }
  else if (LINE(143,x_extension_loaded("dom"))) {
    (o_lval("mPreprocessorClass", 0x6E5AA590C9CC679ALL) = "Preprocessor_DOM");
  }
  else {
    (o_lval("mPreprocessorClass", 0x6E5AA590C9CC679ALL) = "Preprocessor_Hash");
  }
  (m_mMarkerIndex = 0LL);
  (o_lval("mFirstCall", 0x017DA31DE741B5E9LL) = true);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Parser.php line 155 */
Variant c_parser::t___destruct() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::__destruct);
  setInDtor();
  DECLARE_GLOBAL_VARIABLES(g);
  Primitive v_name = 0;
  Variant v_value;

  if (t___isset("mLinkHolders")) {
    LINE(157,m_mLinkHolders.o_invoke_few_args("__destruct", 0x7F974836AACC1EF3LL, 0));
  }
  {
    LOOP_COUNTER(4);
    Variant map5 = ((Object)(this));
    for (ArrayIterPtr iter6 = map5.begin("parser"); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_value = iter6->second();
      v_name = iter6->first();
      {
        t___unset(toString(v_name));
      }
    }
  }
  return null;
} /* function */
/* SRC: Parser.php line 167 */
void c_parser::t_firstcallinit() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::firstCallInit);
  if (!(toBoolean(o_get("mFirstCall", 0x017DA31DE741B5E9LL)))) {
    return;
  }
  (o_lval("mFirstCall", 0x017DA31DE741B5E9LL) = false);
  LINE(173,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::firstCallInit").create()), 0x0000000075359BAFLL));
  LINE(175,t_sethook("pre", Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "renderPreTag").create())));
  LINE(176,c_coreparserfunctions::t_register(((Object)(this))));
  LINE(177,t_initialisevariables());
  LINE(179,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserFirstCallInit").set(1, Array(ArrayInit(1).setRef(0, ref(this)).create())).create()), 0x00000000787A96B2LL));
  LINE(180,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::firstCallInit").create()), 0x00000000B599F276LL));
} /* function */
/* SRC: Parser.php line 188 */
void c_parser::t_clearstate() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::clearState);
  LINE(189,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::clearState").create()), 0x0000000075359BAFLL));
  if (toBoolean(o_get("mFirstCall", 0x017DA31DE741B5E9LL))) {
    LINE(191,o_root_invoke_few_args("firstCallInit", 0x241A6011A10C7EF5LL, 0));
  }
  (m_mOutput = ((Object)(LINE(193,p_parseroutput(p_parseroutput(NEWOBJ(c_parseroutput)())->create())))));
  (m_mAutonumber = 0LL);
  (m_mLastSection = "");
  (m_mDTopen = false);
  (m_mIncludeCount = ScalarArrays::sa_[0]);
  (m_mStripState = ((Object)(LINE(198,p_stripstate(p_stripstate(NEWOBJ(c_stripstate)())->create())))));
  (m_mArgStack = false);
  (m_mInPre = false);
  (m_mLinkHolders = ((Object)(LINE(201,p_linkholderarray(p_linkholderarray(NEWOBJ(c_linkholderarray)())->create(((Object)(this))))))));
  (m_mLinkID = 0LL);
  (m_mRevisionTimestamp = (m_mRevisionId = null));
  (m_mVarCache = ScalarArrays::sa_[0]);
  (o_lval("mUniqPrefix", 0x22D00FA247E8311DLL) = concat("\177UNIQ", LINE(218,c_parser::t_getrandomstring())));
  (m_mTplExpandCache = (m_mTplRedirCache = (m_mTplDomCache = ScalarArrays::sa_[0])));
  (o_lval("mShowToc", 0x673E52F417D0192CLL) = true);
  (o_lval("mForceTocPosition", 0x00520DD93AF13A69LL) = false);
  (m_mIncludeSizes = ScalarArrays::sa_[16]);
  (m_mPPNodeCount = 0LL);
  (m_mDefaultSort = false);
  (m_mHeadings = ScalarArrays::sa_[0]);
  (m_mDoubleUnderscores = ScalarArrays::sa_[0]);
  (m_mExpensiveFunctionCount = 0LL);
  if (t___isset("mPreprocessor") && !same(m_mPreprocessor.o_get("parser", 0x18A8B9D71D4F2D02LL), ((Object)(this)))) {
    (m_mPreprocessor = null);
  }
  LINE(241,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserClearState").set(1, Array(ArrayInit(1).setRef(0, ref(this)).create())).create()), 0x00000000787A96B2LL));
  LINE(242,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::clearState").create()), 0x00000000B599F276LL));
} /* function */
/* SRC: Parser.php line 245 */
void c_parser::t_setoutputtype(CVarRef v_ot) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::setOutputType);
  (m_mOutputType = v_ot);
  (m_ot = Array(ArrayInit(3).set(0, "html", equal(v_ot, 1LL /* parser::OT_HTML */), 0x5CA91C812230855DLL).set(1, "wiki", equal(v_ot, 2LL /* parser::OT_WIKI */), 0x24648D2BCD794D21LL).set(2, "pre", equal(v_ot, 3LL /* parser::OT_PREPROCESS */), 0x633242EDD179FBACLL).create()));
} /* function */
/* SRC: Parser.php line 258 */
void c_parser::t_settitle(Variant v_t) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::setTitle);
  if (!(toBoolean(v_t)) || instanceOf(v_t, "FakeTitle")) {
    (v_t = LINE(260,throw_fatal("unknown class title", ((void*)NULL))));
  }
  if (!same(LINE(262,x_strval(v_t.o_invoke_few_args("getFragment", 0x2354C40909546D77LL, 0))), "")) {
    (m_mTitle = f_clone(toObject(v_t)));
    LINE(265,m_mTitle.o_invoke_few_args("setFragment", 0x11122CE04748A038LL, 1, ""));
  }
  else {
    (m_mTitle = v_t);
  }
} /* function */
/* SRC: Parser.php line 276 */
Variant c_parser::t_uniqprefix() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::uniqPrefix);
  if (!(t___isset("mUniqPrefix"))) {
    return "";
  }
  return o_get("mUniqPrefix", 0x22D00FA247E8311DLL);
} /* function */
/* SRC: Parser.php line 301 */
Variant c_parser::t_parse(Variant v_text, CVarRef v_title, p_parseroptions v_options, bool v_linestart //  = true
, bool v_clearState //  = true
, CVarRef v_revid //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::parse);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgUseTidy __attribute__((__unused__)) = g->GV(wgUseTidy);
  Variant &gv_wgAlwaysUseTidy __attribute__((__unused__)) = g->GV(wgAlwaysUseTidy);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_fname;
  Variant v_oldRevisionId;
  Variant v_oldRevisionTimestamp;
  Array v_fixtags;
  Variant v_uniq_prefix;
  Variant v_matches;
  Variant v_elements;
  Variant v_marker;
  Variant v_data;
  Variant v_element;
  Variant v_content;
  Variant v_params;
  Variant v_tag;
  String v_tagName;
  Variant v_output;
  Array v_tidyregs;
  Variant &gv_wgExpensiveParserFunctionLimit __attribute__((__unused__)) = g->GV(wgExpensiveParserFunctionLimit);
  Variant v_wgExpensiveParserFunctionLimit;
  Variant v_max;
  String v_PFreport;
  Variant v_limitReport;

  {
  }
  (v_fname = concat("Parser::parse-", toString(LINE(308,invoke_failed("wfgetcaller", Array(), 0x000000006DA04B19LL)))));
  LINE(309,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::parse").create()), 0x0000000075359BAFLL));
  LINE(310,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, ref(v_fname)).create()), 0x0000000075359BAFLL));
  if (v_clearState) {
    LINE(313,t_clearstate());
  }
  (m_mOptions = ((Object)(v_options)));
  LINE(317,t_settitle(v_title));
  (v_oldRevisionId = m_mRevisionId);
  (v_oldRevisionTimestamp = m_mRevisionTimestamp);
  if (!same(v_revid, null)) {
    (m_mRevisionId = v_revid);
    (m_mRevisionTimestamp = null);
  }
  LINE(324,t_setoutputtype(1LL /* parser::OT_HTML */));
  LINE(325,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserBeforeStrip").set(1, Array(ArrayInit(3).setRef(0, ref(this)).setRef(1, ref(v_text)).setRef(2, ref(lval(m_mStripState))).create())).create()), 0x00000000787A96B2LL));
  LINE(327,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserAfterStrip").set(1, Array(ArrayInit(3).setRef(0, ref(this)).setRef(1, ref(v_text)).setRef(2, ref(lval(m_mStripState))).create())).create()), 0x00000000787A96B2LL));
  (v_text = LINE(328,t_internalparse(v_text)));
  (v_text = LINE(329,m_mStripState.o_invoke_few_args("unstripGeneral", 0x695FA349798CF769LL, 1, v_text)));
  (v_fixtags = ScalarArrays::sa_[17]);
  (v_text = LINE(340,(assignCallTemp(eo_0, x_array_keys(v_fixtags)),assignCallTemp(eo_1, x_array_values(v_fixtags)),assignCallTemp(eo_2, v_text),x_preg_replace(eo_0, eo_1, eo_2))));
  (v_text = LINE(342,t_doblocklevels(v_text, v_linestart)));
  LINE(344,t_replacelinkholders(ref(v_text)));
  (v_text = LINE(350,gv_wgContLang.o_invoke_few_args("parserConvert", 0x3E3277B648A84B8BLL, 2, v_text, this)));
  (v_text = LINE(352,m_mStripState.o_invoke_few_args("unstripNoWiki", 0x2374179DF576FCA9LL, 1, v_text)));
  LINE(354,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserBeforeTidy").set(1, Array(ArrayInit(2).setRef(0, ref(this)).setRef(1, ref(v_text)).create())).create()), 0x00000000787A96B2LL));
  (v_uniq_prefix = o_get("mUniqPrefix", 0x22D00FA247E8311DLL));
  (v_matches = ScalarArrays::sa_[0]);
  (v_elements = LINE(360,x_array_keys(m_mTransparentTagHooks)));
  (v_text = LINE(361,c_parser::t_extracttagsandparams(v_elements, v_text, ref(v_matches), v_uniq_prefix)));
  {
    LOOP_COUNTER(7);
    for (ArrayIterPtr iter9 = v_matches.begin("parser"); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_data = iter9->second();
      v_marker = iter9->first();
      {
        df_lambda_6(v_data, v_element, v_content, v_params, v_tag);
        (v_tagName = LINE(365,x_strtolower(toString(v_element))));
        if (isset(m_mTransparentTagHooks, v_tagName)) {
          (v_output = LINE(368,x_call_user_func_array(m_mTransparentTagHooks.rvalAt(v_tagName), Array(ArrayInit(3).set(0, v_content).set(1, v_params).set(2, ((Object)(this))).create()))));
        }
        else {
          (v_output = v_tag);
        }
        LINE(372,m_mStripState.o_get("general", 0x661B15A9C00F7127LL).o_invoke_few_args("setPair", 0x1CA93325C3BDD490LL, 2, v_marker, v_output));
      }
    }
  }
  (v_text = LINE(374,m_mStripState.o_invoke_few_args("unstripGeneral", 0x695FA349798CF769LL, 1, v_text)));
  (v_text = LINE(376,throw_fatal("unknown class sanitizer", ((void*)NULL))));
  if ((toBoolean(gv_wgUseTidy) && toBoolean(m_mOptions.o_get("mTidy", 0x46AADA01E8915747LL))) || toBoolean(gv_wgAlwaysUseTidy)) {
    (v_text = LINE(379,c_mwtidy::t_tidy(v_text)));
  }
  else {
    (v_tidyregs = ScalarArrays::sa_[18]);
    (v_text = LINE(405,(assignCallTemp(eo_0, LINE(403,x_array_keys(v_tidyregs))),assignCallTemp(eo_1, LINE(404,x_array_values(v_tidyregs))),assignCallTemp(eo_2, v_text),x_preg_replace(eo_0, eo_1, eo_2))));
  }
  v_wgExpensiveParserFunctionLimit = ref(g->GV(wgExpensiveParserFunctionLimit));
  if (more(m_mExpensiveFunctionCount, v_wgExpensiveParserFunctionLimit)) {
    LINE(409,t_limitationwarn("expensive-parserfunction", m_mExpensiveFunctionCount, v_wgExpensiveParserFunctionLimit));
  }
  LINE(412,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserAfterTidy").set(1, Array(ArrayInit(2).setRef(0, ref(this)).setRef(1, ref(v_text)).create())).create()), 0x00000000787A96B2LL));
  if (toBoolean(LINE(415,m_mOptions.o_invoke_few_args("getEnableLimitReport", 0x22B0D101E5B6C279LL, 0)))) {
    v_wgExpensiveParserFunctionLimit = ref(g->GV(wgExpensiveParserFunctionLimit));
    (v_max = LINE(417,m_mOptions.o_invoke_few_args("getMaxIncludeSize", 0x5356D47AAA28FDF5LL, 0)));
    (v_PFreport = LINE(418,concat5("Expensive parser function count: ", toString(m_mExpensiveFunctionCount), "/", toString(v_wgExpensiveParserFunctionLimit), "\n")));
    (v_limitReport = LINE(424,(assignCallTemp(eo_1, LINE(421,concat5("Preprocessor node count: ", toString(m_mPPNodeCount), "/", toString(m_mOptions.o_get("mMaxPPNodeCount", 0x3DD526BE5013BD55LL)), "\n"))),assignCallTemp(eo_2, LINE(422,concat5("Post-expand include size: ", toString(m_mIncludeSizes.rvalAt("post-expand", 0x0A9E26E2CC92B3CCLL)), "/", toString(v_max), " bytes\n"))),assignCallTemp(eo_3, LINE(423,concat5("Template argument size: ", toString(m_mIncludeSizes.rvalAt("arg", 0x72E1EE0D651E56D8LL)), "/", toString(v_max), " bytes\n"))),assignCallTemp(eo_4, v_PFreport),concat5("NewPP limit report\n", eo_1, eo_2, eo_3, eo_4))));
    LINE(425,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserLimitReport").set(1, Array(ArrayInit(2).set(0, ((Object)(this))).setRef(1, ref(v_limitReport)).create())).create()), 0x00000000787A96B2LL));
    concat_assign(v_text, LINE(426,concat3("\n<!-- \n", toString(v_limitReport), "-->\n")));
  }
  LINE(428,m_mOutput.o_invoke_few_args("setText", 0x5C75DB446C2096A6LL, 1, v_text));
  (m_mRevisionId = v_oldRevisionId);
  (m_mRevisionTimestamp = v_oldRevisionTimestamp);
  LINE(431,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, ref(v_fname)).create()), 0x00000000B599F276LL));
  LINE(432,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::parse").create()), 0x00000000B599F276LL));
  return m_mOutput;
} /* function */
/* SRC: Parser.php line 446 */
Variant c_parser::t_recursivetagparse(Variant v_text, CVarRef v_frame //  = false
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::recursiveTagParse);
  LINE(447,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::recursiveTagParse").create()), 0x0000000075359BAFLL));
  LINE(448,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserBeforeStrip").set(1, Array(ArrayInit(3).setRef(0, ref(this)).setRef(1, ref(v_text)).setRef(2, ref(lval(m_mStripState))).create())).create()), 0x00000000787A96B2LL));
  LINE(449,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserAfterStrip").set(1, Array(ArrayInit(3).setRef(0, ref(this)).setRef(1, ref(v_text)).setRef(2, ref(lval(m_mStripState))).create())).create()), 0x00000000787A96B2LL));
  (v_text = LINE(450,t_internalparse(v_text, false, v_frame)));
  LINE(451,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::recursiveTagParse").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 459 */
Variant c_parser::t_preprocess(Variant v_text, CVarRef v_title, CVarRef v_options, CVarRef v_revid //  = null_variant
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::preprocess);
  LINE(460,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::preprocess").create()), 0x0000000075359BAFLL));
  LINE(461,t_clearstate());
  LINE(462,t_setoutputtype(3LL /* parser::OT_PREPROCESS */));
  (m_mOptions = v_options);
  LINE(464,t_settitle(v_title));
  if (!same(v_revid, null)) {
    (m_mRevisionId = v_revid);
  }
  LINE(468,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserBeforeStrip").set(1, Array(ArrayInit(3).setRef(0, ref(this)).setRef(1, ref(v_text)).setRef(2, ref(lval(m_mStripState))).create())).create()), 0x00000000787A96B2LL));
  LINE(469,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserAfterStrip").set(1, Array(ArrayInit(3).setRef(0, ref(this)).setRef(1, ref(v_text)).setRef(2, ref(lval(m_mStripState))).create())).create()), 0x00000000787A96B2LL));
  (v_text = LINE(470,t_replacevariables(v_text)));
  (v_text = LINE(471,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_text)));
  LINE(472,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::preprocess").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 482 */
String c_parser::ti_getrandomstring(const char* cls) {
  STATIC_METHOD_INJECTION(Parser, Parser::getRandomString);
  return concat_rev(LINE(483,x_dechex(x_mt_rand(0LL, 2147483647LL))), x_dechex(x_mt_rand(0LL, 2147483647LL)));
} /* function */
/* SRC: Parser.php line 486 */
Variant c_parser::t_gettitle() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getTitle);
  return ref(lval(m_mTitle));
} /* function */
/* SRC: Parser.php line 487 */
Variant c_parser::t_getoptions() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getOptions);
  return m_mOptions;
} /* function */
/* SRC: Parser.php line 488 */
Variant c_parser::t_getrevisionid() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getRevisionId);
  return m_mRevisionId;
} /* function */
/* SRC: Parser.php line 489 */
Variant c_parser::t_getoutput() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getOutput);
  return m_mOutput;
} /* function */
/* SRC: Parser.php line 490 */
Primitive c_parser::t_nextlinkid() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::nextLinkID);
  return m_mLinkID++;
} /* function */
/* SRC: Parser.php line 492 */
Variant c_parser::t_getfunctionlang() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getFunctionLang);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgLang __attribute__((__unused__)) = g->GV(wgLang);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_target;

  {
  }
  (v_target = LINE(495,m_mOptions.o_invoke_few_args("getTargetLanguage", 0x66650D0ADB29FEAALL, 0)));
  if (!same(v_target, null)) {
    return v_target;
  }
  else {
    return toBoolean(LINE(499,m_mOptions.o_invoke_few_args("getInterfaceMessage", 0x47CB1D8AB54E1850LL, 0))) ? ((Variant)(gv_wgLang)) : ((Variant)(gv_wgContLang));
  }
  return null;
} /* function */
/* SRC: Parser.php line 506 */
Variant c_parser::t_getpreprocessor() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getPreprocessor);
  Variant v_class;

  if (!(t___isset("mPreprocessor"))) {
    (v_class = o_get("mPreprocessorClass", 0x6E5AA590C9CC679ALL));
    (m_mPreprocessor = LINE(509,create_object(toString(v_class), Array(ArrayInit(1).set(0, ref(this)).create()))));
  }
  return m_mPreprocessor;
} /* function */
/* SRC: Parser.php line 532 */
String c_parser::t_extracttagsandparams(CVarRef v_elements, Variant v_text, Variant v_matches, CVarRef v_uniq_prefix //  = ""
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::extractTagsAndParams);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_n __attribute__((__unused__)) = g->sv_parser$$extracttagsandparams$$n.lvalAt(this->o_getClassName());
  Variant &inited_sv_n __attribute__((__unused__)) = g->inited_sv_parser$$extracttagsandparams$$n.lvalAt(this->o_getClassName());
  String v_stripped;
  String v_taglist;
  String v_start;
  Variant v_p;
  Variant v_element;
  Variant v_attributes;
  Variant v_close;
  Variant v_inside;
  String v_marker;
  Variant v_content;
  Variant v_tail;
  String v_end;
  Variant v_q;

  if (!inited_sv_n) {
    (sv_n = 1LL);
    inited_sv_n = true;
  }
  (v_stripped = "");
  (v_matches = ScalarArrays::sa_[0]);
  (v_taglist = LINE(537,x_implode("|", v_elements)));
  (v_start = LINE(538,concat3("/<(", v_taglist, ")(\\s+[^>]*\?|\\s*\?)(\\/\?>)|<(!--)/i")));
  LOOP_COUNTER(10);
  {
    while (!equal("", v_text)) {
      LOOP_COUNTER_CHECK(10);
      {
        (v_p = LINE(541,x_preg_split(v_start, v_text, toInt32(2LL), toInt32(2LL) /* PREG_SPLIT_DELIM_CAPTURE */)));
        concat_assign(v_stripped, toString(v_p.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
        if (less(LINE(543,x_count(v_p)), 5LL)) {
          break;
        }
        if (more(LINE(546,x_count(v_p)), 5LL)) {
          (v_element = v_p.rvalAt(4LL, 0x6F2A25235E544A31LL));
          (v_attributes = "");
          (v_close = "");
          (v_inside = v_p.rvalAt(5LL, 0x350AEB726A15D700LL));
        }
        else {
          (v_element = v_p.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
          (v_attributes = v_p.rvalAt(2LL, 0x486AFCC090D5F98CLL));
          (v_close = v_p.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL));
          (v_inside = v_p.rvalAt(4LL, 0x6F2A25235E544A31LL));
        }
        (v_marker = LINE(560,(assignCallTemp(eo_0, toString(v_uniq_prefix)),assignCallTemp(eo_2, toString(v_element)),assignCallTemp(eo_4, (assignCallTemp(eo_7, sv_n++),x_sprintf(2, "%08X", Array(ArrayInit(1).set(0, eo_7).create())))),concat6(eo_0, "-", eo_2, "-", eo_4, "-QINU\177" /* parser::MARKER_SUFFIX */))));
        concat_assign(v_stripped, v_marker);
        if (same(v_close, "/>")) {
          setNull(v_content);
          (v_text = v_inside);
          setNull(v_tail);
        }
        else {
          if (same(v_element, "!--")) {
            (v_end = "/(-->)/");
          }
          else {
            (v_end = LINE(572,concat3("/(<\\/", toString(v_element), "\\s*>)/i")));
          }
          (v_q = LINE(574,x_preg_split(v_end, v_inside, toInt32(2LL), toInt32(2LL) /* PREG_SPLIT_DELIM_CAPTURE */)));
          (v_content = v_q.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
          if (less(LINE(576,x_count(v_q)), 3LL)) {
            (v_tail = "");
            (v_text = "");
          }
          else {
            (v_tail = v_q.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
            (v_text = v_q.rvalAt(2LL, 0x486AFCC090D5F98CLL));
          }
        }
        v_matches.set(v_marker, ((assignCallTemp(eo_0, v_element),assignCallTemp(eo_1, v_content),assignCallTemp(eo_2, LINE(588,throw_fatal("unknown class sanitizer", ((void*)NULL)))),assignCallTemp(eo_3, LINE(589,concat6("<", toString(v_element), toString(v_attributes), toString(v_close), toString(v_content), toString(v_tail)))),Array(ArrayInit(4).set(0, eo_0).set(1, eo_1).set(2, eo_2).set(3, eo_3).create()))));
      }
    }
  }
  return v_stripped;
} /* function */
/* SRC: Parser.php line 597 */
Variant c_parser::t_getstriplist() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getStripList);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgRawHtml __attribute__((__unused__)) = g->GV(wgRawHtml);
  Variant v_elements;

  (v_elements = m_mStripList);
  if (toBoolean(gv_wgRawHtml)) {
    v_elements.append(("html"));
  }
  if (toBoolean(LINE(603,m_mOptions.o_invoke_few_args("getUseTeX", 0x2C2CE4CD607BE9D3LL, 0)))) {
    v_elements.append(("math"));
  }
  return v_elements;
} /* function */
/* SRC: Parser.php line 612 */
Variant c_parser::t_strip(CVarRef v_text, CVarRef v_state, bool v_stripcomments //  = false
, CArrRef v_dontstrip //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::strip);
  return v_text;
} /* function */
/* SRC: Parser.php line 623 */
Variant c_parser::t_unstrip(Variant v_text, Object v_state) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::unstrip);
  return LINE(624,v_state->o_invoke_few_args("unstripGeneral", 0x695FA349798CF769LL, 1, v_text));
} /* function */
/* SRC: Parser.php line 633 */
Variant c_parser::t_unstripnowiki(Variant v_text, CVarRef v_state) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::unstripNoWiki);
  return LINE(634,toObject(v_state)->o_invoke_few_args("unstripNoWiki", 0x2374179DF576FCA9LL, 1, v_text));
} /* function */
/* SRC: Parser.php line 640 */
Variant c_parser::t_unstripforhtml(Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::unstripForHTML);
  return LINE(641,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_text));
} /* function */
/* SRC: Parser.php line 651 */
Variant c_parser::t_insertstripitem(Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::insertStripItem);
  Variant v_rnd;

  (v_rnd = LINE(652,concat4(toString(o_get("mUniqPrefix", 0x22D00FA247E8311DLL)), "-item-", toString(m_mMarkerIndex), "--QINU\177")));
  m_mMarkerIndex++;
  LINE(654,m_mStripState.o_get("general", 0x661B15A9C00F7127LL).o_invoke_few_args("setPair", 0x1CA93325C3BDD490LL, 2, v_rnd, v_text));
  return v_rnd;
} /* function */
/* SRC: Parser.php line 662 */
Variant c_parser::ti_tidy(const char* cls, CVarRef v_text) {
  STATIC_METHOD_INJECTION(Parser, Parser::tidy);
  LINE(663,invoke_failed("wfdeprecated", Array(ArrayInit(1).set(0, "Parser::tidy").create()), 0x000000004033CBCALL));
  return LINE(664,c_mwtidy::t_tidy(v_text));
} /* function */
/* SRC: Parser.php line 672 */
Variant c_parser::t_dotablestuff(CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::doTableStuff);
  Variant v_lines;
  Variant v_out;
  Variant v_td_history;
  Variant v_last_tag_history;
  Variant v_tr_history;
  Variant v_tr_attributes;
  Variant v_has_opened_tr;
  int64 v_indent_level = 0;
  Variant v_outLine;
  Variant v_line;
  Variant v_first_character;
  Variant v_matches;
  Variant v_attributes;
  Variant v_last_tag;
  Variant v_cells;
  Variant v_cell;
  String v_previous;
  Variant v_tr_after;
  Variant v_cell_data;

  LINE(673,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::doTableStuff").create()), 0x0000000075359BAFLL));
  (v_lines = LINE(675,throw_fatal("unknown class stringutils", ((void*)NULL))));
  (v_out = "");
  (v_td_history = ScalarArrays::sa_[0]);
  (v_last_tag_history = ScalarArrays::sa_[0]);
  (v_tr_history = ScalarArrays::sa_[0]);
  (v_tr_attributes = ScalarArrays::sa_[0]);
  (v_has_opened_tr = ScalarArrays::sa_[0]);
  (v_indent_level = 0LL);
  {
    LOOP_COUNTER(11);
    for (ArrayIterPtr iter13 = v_lines.begin("parser"); !iter13->end(); iter13->next()) {
      LOOP_COUNTER_CHECK(11);
      v_outLine = iter13->second();
      {
        (v_line = LINE(685,x_trim(toString(v_outLine))));
        if (equal(v_line, "")) {
          concat_assign(v_out, concat(toString(v_outLine), "\n"));
          continue;
        }
        (v_first_character = v_line.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
        (v_matches = ScalarArrays::sa_[0]);
        if (toBoolean(LINE(694,x_preg_match("/^(:*)\\{\\|(.*)$/", toString(v_line), ref(v_matches))))) {
          (v_indent_level = toInt64(LINE(696,x_strlen(toString(v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL))))));
          (v_attributes = LINE(698,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_matches.refvalAt(2LL, 0x486AFCC090D5F98CLL))));
          (v_attributes = LINE(699,throw_fatal("unknown class sanitizer", ((void*)NULL))));
          (v_outLine = concat_rev(LINE(701,concat3("<table", toString(v_attributes), ">")), x_str_repeat("<dl><dd>", toInt32(v_indent_level))));
          LINE(702,x_array_push(2, ref(v_td_history), false));
          LINE(703,x_array_push(2, ref(v_last_tag_history), ""));
          LINE(704,x_array_push(2, ref(v_tr_history), false));
          LINE(705,x_array_push(2, ref(v_tr_attributes), ""));
          LINE(706,x_array_push(2, ref(v_has_opened_tr), false));
        }
        else if (equal(LINE(707,x_count(v_td_history)), 0LL)) {
          concat_assign(v_out, concat(toString(v_outLine), "\n"));
          continue;
        }
        else if (same(LINE(711,x_substr(toString(v_line), toInt32(0LL), toInt32(2LL))), "|}")) {
          (v_line = concat("</table>", toString(LINE(713,x_substr(toString(v_line), toInt32(2LL))))));
          (v_last_tag = LINE(714,x_array_pop(ref(v_last_tag_history))));
          if (!(toBoolean(LINE(716,x_array_pop(ref(v_has_opened_tr)))))) {
            (v_line = toString("<tr><td></td></tr>") + toString(v_line));
          }
          if (toBoolean(LINE(720,x_array_pop(ref(v_tr_history))))) {
            (v_line = toString("</tr>") + toString(v_line));
          }
          if (toBoolean(LINE(724,x_array_pop(ref(v_td_history))))) {
            (v_line = LINE(725,concat4("</", toString(v_last_tag), ">", toString(v_line))));
          }
          LINE(727,x_array_pop(ref(v_tr_attributes)));
          (v_outLine = concat(toString(v_line), LINE(728,x_str_repeat("</dd></dl>", toInt32(v_indent_level)))));
        }
        else if (same(LINE(729,x_substr(toString(v_line), toInt32(0LL), toInt32(2LL))), "|-")) {
          (v_line = LINE(731,x_preg_replace("#^\\|-+#", "", v_line)));
          (v_attributes = LINE(734,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_line)));
          (v_attributes = LINE(735,throw_fatal("unknown class sanitizer", ((void*)NULL))));
          LINE(736,x_array_pop(ref(v_tr_attributes)));
          LINE(737,x_array_push(2, ref(v_tr_attributes), v_attributes));
          (v_line = "");
          (v_last_tag = LINE(740,x_array_pop(ref(v_last_tag_history))));
          LINE(741,x_array_pop(ref(v_has_opened_tr)));
          LINE(742,x_array_push(2, ref(v_has_opened_tr), true));
          if (toBoolean(LINE(744,x_array_pop(ref(v_tr_history))))) {
            (v_line = "</tr>");
          }
          if (toBoolean(LINE(748,x_array_pop(ref(v_td_history))))) {
            (v_line = LINE(749,concat4("</", toString(v_last_tag), ">", toString(v_line))));
          }
          (v_outLine = v_line);
          LINE(753,x_array_push(2, ref(v_tr_history), false));
          LINE(754,x_array_push(2, ref(v_td_history), false));
          LINE(755,x_array_push(2, ref(v_last_tag_history), ""));
        }
        else if (same(v_first_character, "|") || same(v_first_character, "!") || same(LINE(757,x_substr(toString(v_line), toInt32(0LL), toInt32(2LL))), "|+")) {
          if (same(LINE(759,x_substr(toString(v_line), toInt32(0LL), toInt32(2LL))), "|+")) {
            (v_first_character = "+");
            (v_line = LINE(761,x_substr(toString(v_line), toInt32(1LL))));
          }
          (v_line = LINE(764,x_substr(toString(v_line), toInt32(1LL))));
          if (same(v_first_character, "!")) {
            (v_line = LINE(767,x_str_replace("!!", "||", v_line)));
          }
          (v_cells = LINE(774,throw_fatal("unknown class stringutils", ((void*)NULL))));
          (v_outLine = "");
          {
            LOOP_COUNTER(14);
            for (ArrayIterPtr iter16 = v_cells.begin("parser"); !iter16->end(); iter16->next()) {
              LOOP_COUNTER_CHECK(14);
              v_cell = iter16->second();
              {
                (v_previous = "");
                if (!same(v_first_character, "+")) {
                  (v_tr_after = LINE(784,x_array_pop(ref(v_tr_attributes))));
                  if (!(toBoolean(LINE(785,x_array_pop(ref(v_tr_history)))))) {
                    (v_previous = LINE(786,concat3("<tr", toString(v_tr_after), ">\n")));
                  }
                  LINE(788,x_array_push(2, ref(v_tr_history), true));
                  LINE(789,x_array_push(2, ref(v_tr_attributes), ""));
                  LINE(790,x_array_pop(ref(v_has_opened_tr)));
                  LINE(791,x_array_push(2, ref(v_has_opened_tr), true));
                }
                (v_last_tag = LINE(794,x_array_pop(ref(v_last_tag_history))));
                if (toBoolean(LINE(796,x_array_pop(ref(v_td_history))))) {
                  (v_previous = LINE(797,concat4("</", toString(v_last_tag), ">", v_previous)));
                }
                if (same(v_first_character, "|")) {
                  (v_last_tag = "td");
                }
                else if (same(v_first_character, "!")) {
                  (v_last_tag = "th");
                }
                else if (same(v_first_character, "+")) {
                  (v_last_tag = "caption");
                }
                else {
                  (v_last_tag = "");
                }
                LINE(810,x_array_push(2, ref(v_last_tag_history), v_last_tag));
                (v_cell_data = LINE(813,x_explode("|", toString(v_cell), toInt32(2LL))));
                if (!same(LINE(817,x_strpos(toString(v_cell_data.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "[[")), false)) {
                  (v_cell = LINE(818,concat5(v_previous, "<", toString(v_last_tag), ">", toString(v_cell))));
                }
                else if (equal(LINE(819,x_count(v_cell_data)), 1LL)) (v_cell = LINE(820,concat5(v_previous, "<", toString(v_last_tag), ">", toString(v_cell_data.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
                else {
                  (v_attributes = LINE(822,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_cell_data.refvalAt(0LL, 0x77CFA1EEF01BCA90LL))));
                  (v_attributes = LINE(823,throw_fatal("unknown class sanitizer", ((void*)NULL))));
                  (v_cell = LINE(824,concat6(v_previous, "<", toString(v_last_tag), toString(v_attributes), ">", toString(v_cell_data.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
                }
                concat_assign(v_outLine, toString(v_cell));
                LINE(828,x_array_push(2, ref(v_td_history), true));
              }
            }
          }
        }
        concat_assign(v_out, concat(toString(v_outLine), "\n"));
      }
    }
  }
  LOOP_COUNTER(17);
  {
    while (more(LINE(835,x_count(v_td_history)), 0LL)) {
      LOOP_COUNTER_CHECK(17);
      {
        if (toBoolean(LINE(837,x_array_pop(ref(v_td_history))))) {
          concat_assign(v_out, "</td>\n");
        }
        if (toBoolean(LINE(840,x_array_pop(ref(v_tr_history))))) {
          concat_assign(v_out, "</tr>\n");
        }
        if (!(toBoolean(LINE(843,x_array_pop(ref(v_has_opened_tr)))))) {
          concat_assign(v_out, "<tr><td></td></tr>\n");
        }
        concat_assign(v_out, "</table>\n");
      }
    }
  }
  if (same(LINE(851,x_substr(toString(v_out), toInt32(-1LL))), "\n")) {
    (v_out = LINE(852,x_substr(toString(v_out), toInt32(0LL), toInt32(-1LL))));
  }
  if (same(v_out, "<table>\n<tr><td></td></tr>\n</table>")) {
    (v_out = "");
  }
  LINE(860,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::doTableStuff").create()), 0x00000000B599F276LL));
  return v_out;
} /* function */
/* SRC: Parser.php line 871 */
Variant c_parser::t_internalparse(Variant v_text, bool v_isMain //  = true
, CVarRef v_frame //  = false
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::internalParse);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant v_origText;
  int64 v_flag = 0;
  Variant v_dom;
  Variant v_df;

  LINE(872,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::internalParse").create()), 0x0000000075359BAFLL));
  (v_origText = v_text);
  if (!(toBoolean(LINE(877,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserBeforeInternalParse").set(1, Array(ArrayInit(3).setRef(0, ref(this)).setRef(1, ref(v_text)).setRef(2, ref(lval(m_mStripState))).create())).create()), 0x00000000787A96B2LL))))) {
    LINE(878,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::internalParse").create()), 0x00000000B599F276LL));
    return v_text;
  }
  if (toBoolean(v_frame)) {
    if (!(toBoolean(toObject(v_frame).o_get("depth", 0x1AE700EECE6274C7LL)))) (v_flag = 0LL);
    else (v_flag = 1LL /* parser::PTD_FOR_INCLUSION */);
    (v_dom = LINE(890,t_preprocesstodom(v_text, v_flag)));
    (v_text = LINE(891,toObject(v_frame)->o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_dom)));
  }
  else {
    (v_text = LINE(895,t_replacevariables(v_text)));
  }
  (v_text = LINE(898,(assignCallTemp(eo_0, v_text),assignCallTemp(eo_1, Array(ArrayInit(2).setRef(0, ref(this)).set(1, "attributeStripCallback").create())),assignCallTemp(eo_3, x_array_keys(m_mTransparentTagHooks)),throw_fatal("unknown class sanitizer", (x_array_keys(m_mTransparentTagHooks), (void*)NULL)))));
  LINE(899,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "InternalParseBeforeLinks").set(1, Array(ArrayInit(3).setRef(0, ref(this)).setRef(1, ref(v_text)).setRef(2, ref(lval(m_mStripState))).create())).create()), 0x00000000787A96B2LL));
  (v_text = LINE(905,t_dotablestuff(v_text)));
  (v_text = LINE(907,x_preg_replace("/(^|\\n)-----*/", "\\1<hr />", v_text)));
  (v_text = LINE(909,t_dodoubleunderscore(v_text)));
  (v_text = LINE(910,t_doheadings(v_text)));
  if (toBoolean(LINE(911,m_mOptions.o_invoke_few_args("getUseDynamicDates", 0x01659C897F5E99C1LL, 0)))) {
    (v_df = LINE(912,c_dateformatter::t_getinstance()));
    (v_text = (assignCallTemp(eo_0, ref(LINE(913,m_mOptions.o_invoke_few_args("getDateFormat", 0x6EF12DB3679FD689LL, 0)))),assignCallTemp(eo_1, ref(v_text)),v_df.o_invoke("reformat", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x59D9A29BB871E42ELL)));
  }
  (v_text = LINE(915,t_doallquotes(v_text)));
  (v_text = LINE(916,t_replaceinternallinks(v_text)));
  (v_text = LINE(917,t_replaceexternallinks(v_text)));
  (v_text = LINE(921,x_str_replace(concat(toString(o_get("mUniqPrefix", 0x22D00FA247E8311DLL)), "NOPARSE"), "", v_text)));
  (v_text = LINE(923,t_domagiclinks(v_text)));
  (v_text = LINE(924,t_formatheadings(v_text, v_origText, v_isMain)));
  LINE(926,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::internalParse").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 937 */
Variant c_parser::t_domagiclinks(Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::doMagicLinks);
  Variant v_prots;
  String v_urlChar;

  LINE(938,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::doMagicLinks").create()), 0x0000000075359BAFLL));
  (v_prots = m_mUrlProtocols);
  (v_urlChar = "[^][<>\"\\x00-\\x20\\x7F]" /* parser::EXT_LINK_URL_CLASS */);
  (v_text = LINE(952,x_preg_replace_callback(concat("!(\?:                           # Start cases\n\t\t\t\t(<a.*\?</a>) |               # m[1]: Skip link text \n\t\t\t\t(<.*\?>) |                   # m[2]: Skip stuff inside HTML elements", concat5("\n\t\t\t\t(\\b(\?:", toString(v_prots), ")", v_urlChar, "+) |  # m[3]: Free external links\n\t\t\t\t(\?:RFC|PMID)\\s+([0-9]+) |   # m[4]: RFC or PMID, capture number\n\t\t\t\tISBN\\s+(\\b                  # m[5]: ISBN, capture number\n\t\t\t\t    (\?: 97[89] [\\ \\-]\? )\?   # optional 13-digit ISBN prefix\n\t\t\t\t    (\?: [0-9]  [\\ \\-]\? ){9} # 9 digits with opt. delimiters\n\t\t\t\t    [0-9Xx]                 # check digit\n\t\t\t\t    \\b)\n\t\t\t)!x")), Array(ArrayInit(2).setRef(0, ref(this)).set(1, "magicLinkCallback").create()), v_text)));
  LINE(953,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::doMagicLinks").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 957 */
Variant c_parser::t_magiclinkcallback(CVarRef v_m) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::magicLinkCallback);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_CssClass;
  String v_keyword;
  Variant v_urlmsg;
  Variant v_id;
  Variant v_url;
  Variant v_sk;
  Variant v_la;
  Variant v_isbn;
  String v_num;
  Variant v_titleObj;

  if (isset(v_m, 1LL, 0x5BCA7C69B794F8CELL) && !same(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL), "")) {
    return v_m.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
  }
  else if (isset(v_m, 2LL, 0x486AFCC090D5F98CLL) && !same(v_m.rvalAt(2LL, 0x486AFCC090D5F98CLL), "")) {
    return v_m.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
  }
  else if (isset(v_m, 3LL, 0x135FDDF6A6BFBBDDLL) && !same(v_m.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL), "")) {
    return LINE(966,t_makefreeexternallink(v_m.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)));
  }
  else if (isset(v_m, 4LL, 0x6F2A25235E544A31LL) && !same(v_m.rvalAt(4LL, 0x6F2A25235E544A31LL), "")) {
    (v_CssClass = "");
    if (same(LINE(970,x_substr(toString(v_m.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toInt32(0LL), toInt32(3LL))), "RFC")) {
      (v_keyword = "RFC");
      (v_urlmsg = "rfcurl");
      (v_CssClass = "mw-magiclink-rfc");
      (v_id = v_m.rvalAt(4LL, 0x6F2A25235E544A31LL));
    }
    else if (same(LINE(975,x_substr(toString(v_m.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toInt32(0LL), toInt32(4LL))), "PMID")) {
      (v_keyword = "PMID");
      (v_urlmsg = "pubmedurl");
      (v_CssClass = "mw-magiclink-pmid");
      (v_id = v_m.rvalAt(4LL, 0x6F2A25235E544A31LL));
    }
    else {
      throw_exception(LINE(982,create_object("mwexception", Array(ArrayInit(1).set(0, (assignCallTemp(eo_1, toString(x_substr(toString(v_m.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), toInt32(0LL), toInt32(20LL)))),concat3("Parser::magicLinkCallback: unrecognised match type \"", eo_1, "\""))).create()))));
    }
    (v_url = LINE(984,invoke_failed("wfmsg", Array(ArrayInit(2).set(0, ref(v_urlmsg)).set(1, ref(v_id)).create()), 0x0000000002E29FB3LL)));
    (v_sk = LINE(985,m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
    (v_la = LINE(986,v_sk.o_invoke_few_args("getExternalLinkAttributes", 0x14ACE0578A57FD66LL, 1, toString("external ") + v_CssClass)));
    return concat_rev(LINE(987,concat6(toString(v_la), ">", v_keyword, " ", toString(v_id), "</a>")), concat3("<a href=\"", toString(v_url), "\""));
  }
  else if (isset(v_m, 5LL, 0x350AEB726A15D700LL) && !same(v_m.rvalAt(5LL, 0x350AEB726A15D700LL), "")) {
    (v_isbn = v_m.rvalAt(5LL, 0x350AEB726A15D700LL));
    (v_num = LINE(995,x_strtr(toString(v_isbn), ScalarArrays::sa_[19])));
    (v_titleObj = LINE(996,throw_fatal("unknown class specialpage", ((void*)NULL))));
    return LINE(999,(assignCallTemp(eo_1, toString(LINE(998,v_titleObj.o_invoke_few_args("escapeLocalUrl", 0x0A4E2A99089C4842LL, 0)))),assignCallTemp(eo_2, LINE(999,concat3("\" class=\"internal mw-magiclink-isbn\">ISBN ", toString(v_isbn), "</a>"))),concat3("<a href=\"", eo_1, eo_2)));
  }
  else {
    return v_m.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
  }
  return null;
} /* function */
/* SRC: Parser.php line 1010 */
String c_parser::t_makefreeexternallink(Variant v_url) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::makeFreeExternalLink);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_sk;
  String v_trail;
  Variant v_m2;
  String v_sep;
  Variant v_numSepChars;
  Variant v_text;
  Variant v_pasteurized;

  LINE(1012,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::makeFreeExternalLink").create()), 0x0000000075359BAFLL));
  (v_sk = LINE(1014,m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_trail = "");
  (v_m2 = ScalarArrays::sa_[0]);
  if (toBoolean(LINE(1021,x_preg_match("/&(lt|gt);/", toString(v_url), ref(v_m2), toInt32(256LL) /* PREG_OFFSET_CAPTURE */)))) {
    (v_trail = concat(toString(LINE(1022,x_substr(toString(v_url), toInt32(v_m2.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL))))), v_trail));
    (v_url = LINE(1023,x_substr(toString(v_url), toInt32(0LL), toInt32(v_m2.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
  }
  (v_sep = ",;\\.:!\?");
  if (same(LINE(1029,x_strpos(toString(v_url), "(")), false)) {
    concat_assign(v_sep, ")");
  }
  (v_numSepChars = LINE(1033,(assignCallTemp(eo_0, x_strrev(toString(v_url))),assignCallTemp(eo_1, v_sep),x_strspn(eo_0, eo_1))));
  if (toBoolean(v_numSepChars)) {
    (v_trail = concat(toString(LINE(1035,x_substr(toString(v_url), toInt32(negate(v_numSepChars))))), v_trail));
    (v_url = LINE(1036,x_substr(toString(v_url), toInt32(0LL), toInt32(negate(v_numSepChars)))));
  }
  (v_url = LINE(1039,throw_fatal("unknown class sanitizer", ((void*)NULL))));
  (v_text = LINE(1042,t_maybemakeexternalimage(v_url)));
  if (same(v_text, false)) {
    (v_text = (assignCallTemp(eo_0, toObject(v_sk)),(assignCallTemp(eo_1, ref(v_url)),assignCallTemp(eo_2, ref(LINE(1045,gv_wgContLang.o_invoke_few_args("markNoConversion", 0x3AC6A984BC83E71BLL, 1, v_url)))),assignCallTemp(eo_5, ref(LINE(1046,t_getexternallinkattribs(v_url)))),eo_0.o_invoke("makeExternalLink", Array(ArrayInit(5).set(0, eo_1).set(1, eo_2).set(2, true).set(3, "free").set(4, eo_5).create()), 0x29CE649D13E77D3BLL))));
    (v_pasteurized = LINE(1049,c_parser::t_replaceunusualescapes(v_url)));
    LINE(1050,m_mOutput.o_invoke_few_args("addExternalLink", 0x3FE22FCE3F0B40E8LL, 1, v_pasteurized));
  }
  LINE(1052,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::makeFreeExternalLink").create()), 0x00000000B599F276LL));
  return concat(toString(v_text), v_trail);
} /* function */
/* SRC: Parser.php line 1062 */
Variant c_parser::t_doheadings(Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::doHeadings);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  int64 v_i = 0;
  String v_h;

  LINE(1063,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::doHeadings").create()), 0x0000000075359BAFLL));
  {
    LOOP_COUNTER(18);
    for ((v_i = 6LL); not_less(v_i, 1LL); --v_i) {
      LOOP_COUNTER_CHECK(18);
      {
        (v_h = LINE(1065,x_str_repeat("=", toInt32(v_i))));
        (v_text = LINE(1067,(assignCallTemp(eo_0, LINE(1066,concat5("/^", v_h, "(.+)", v_h, "\\s*$/m"))),assignCallTemp(eo_1, LINE(1067,concat5("<h", toString(v_i), ">\\1</h", toString(v_i), ">"))),assignCallTemp(eo_2, v_text),x_preg_replace(eo_0, eo_1, eo_2))));
      }
    }
  }
  LINE(1069,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::doHeadings").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 1078 */
Variant c_parser::t_doallquotes(CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::doAllQuotes);
  Variant v_outtext;
  Variant v_lines;
  Variant v_line;

  LINE(1079,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::doAllQuotes").create()), 0x0000000075359BAFLL));
  (v_outtext = "");
  (v_lines = LINE(1081,throw_fatal("unknown class stringutils", ((void*)NULL))));
  {
    LOOP_COUNTER(19);
    for (ArrayIterPtr iter21 = v_lines.begin("parser"); !iter21->end(); iter21->next()) {
      LOOP_COUNTER_CHECK(19);
      v_line = iter21->second();
      {
        concat_assign(v_outtext, concat(toString(LINE(1083,t_doquotes(v_line))), "\n"));
      }
    }
  }
  (v_outtext = LINE(1085,x_substr(toString(v_outtext), toInt32(0LL), toInt32(-1LL))));
  LINE(1086,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::doAllQuotes").create()), 0x00000000B599F276LL));
  return v_outtext;
} /* function */
/* SRC: Parser.php line 1093 */
Variant c_parser::t_doquotes(CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::doQuotes);
  Variant eo_0;
  Variant eo_1;
  Variant v_arr;
  int64 v_i = 0;
  int64 v_numbold = 0;
  int64 v_numitalics = 0;
  Variant v_r;
  int64 v_firstsingleletterword = 0;
  int64 v_firstmultiletterword = 0;
  int64 v_firstspace = 0;
  Variant v_x1;
  Variant v_x2;
  Variant v_output;
  Variant v_buffer;
  String v_state;

  (v_arr = LINE(1094,x_preg_split("/(''+)/", v_text, toInt32(-1LL), toInt32(2LL) /* PREG_SPLIT_DELIM_CAPTURE */)));
  if (equal(LINE(1095,x_count(v_arr)), 1LL)) return v_text;
  else {
    (v_i = 0LL);
    (v_numbold = 0LL);
    (v_numitalics = 0LL);
    {
      LOOP_COUNTER(22);
      for (ArrayIterPtr iter24 = v_arr.begin("parser"); !iter24->end(); iter24->next()) {
        LOOP_COUNTER_CHECK(22);
        v_r = iter24->second();
        {
          if (equal((modulo(v_i, 2LL)), 1LL)) {
            if (equal(LINE(1111,x_strlen(toString(v_arr.rvalAt(v_i)))), 4LL)) {
              concat_assign(lval(v_arr.lvalAt(v_i - 1LL)), "'");
              v_arr.set(v_i, ("'''"));
            }
            else if (more(LINE(1118,x_strlen(toString(v_arr.rvalAt(v_i)))), 5LL)) {
              concat_assign(lval(v_arr.lvalAt(v_i - 1LL)), LINE(1120,(assignCallTemp(eo_1, toInt32(x_strlen(toString(v_arr.rvalAt(v_i))) - 5LL)),x_str_repeat("'", eo_1))));
              v_arr.set(v_i, ("'''''"));
            }
            if (equal(LINE(1125,x_strlen(toString(v_arr.rvalAt(v_i)))), 2LL)) {
              v_numitalics++;
            }
            else if (equal(LINE(1126,x_strlen(toString(v_arr.rvalAt(v_i)))), 3LL)) {
              v_numbold++;
            }
            else if (equal(LINE(1127,x_strlen(toString(v_arr.rvalAt(v_i)))), 5LL)) {
              v_numitalics++;
              v_numbold++;
            }
          }
          v_i++;
        }
      }
    }
    if ((equal(modulo(v_numbold, 2LL), 1LL)) && (equal(modulo(v_numitalics, 2LL), 1LL))) {
      (v_i = 0LL);
      (v_firstsingleletterword = -1LL);
      (v_firstmultiletterword = -1LL);
      (v_firstspace = -1LL);
      {
        LOOP_COUNTER(25);
        for (ArrayIterPtr iter27 = v_arr.begin("parser"); !iter27->end(); iter27->next()) {
          LOOP_COUNTER_CHECK(25);
          v_r = iter27->second();
          {
            if (((equal(modulo(v_i, 2LL), 1LL))) && ((equal(LINE(1144,x_strlen(toString(v_r))), 3LL)))) {
              (v_x1 = LINE(1146,x_substr(toString(v_arr.rvalAt(v_i - 1LL)), toInt32(-1LL))));
              (v_x2 = LINE(1147,x_substr(toString(v_arr.rvalAt(v_i - 1LL)), toInt32(-2LL), toInt32(1LL))));
              if (same(v_x1, " ")) {
                if (equal(v_firstspace, -1LL)) (v_firstspace = v_i);
              }
              else if (same(v_x2, " ")) {
                if (equal(v_firstsingleletterword, -1LL)) (v_firstsingleletterword = v_i);
              }
              else {
                if (equal(v_firstmultiletterword, -1LL)) (v_firstmultiletterword = v_i);
              }
            }
            v_i++;
          }
        }
      }
      if (more(v_firstsingleletterword, -1LL)) {
        v_arr.set(v_firstsingleletterword, ("''"));
        concat_assign(lval(v_arr.lvalAt(v_firstsingleletterword - 1LL)), "'");
      }
      else if (more(v_firstmultiletterword, -1LL)) {
        v_arr.set(v_firstmultiletterword, ("''"));
        concat_assign(lval(v_arr.lvalAt(v_firstmultiletterword - 1LL)), "'");
      }
      else if (more(v_firstspace, -1LL)) {
        v_arr.set(v_firstspace, ("''"));
        concat_assign(lval(v_arr.lvalAt(v_firstspace - 1LL)), "'");
      }
    }
    (v_output = "");
    (v_buffer = "");
    (v_state = "");
    (v_i = 0LL);
    {
      LOOP_COUNTER(28);
      for (ArrayIterPtr iter30 = v_arr.begin("parser"); !iter30->end(); iter30->next()) {
        LOOP_COUNTER_CHECK(28);
        v_r = iter30->second();
        {
          if (equal((modulo(v_i, 2LL)), 0LL)) {
            if (same(v_state, "both")) concat_assign(v_buffer, toString(v_r));
            else concat_assign(v_output, toString(v_r));
          }
          else {
            if (equal(LINE(1197,x_strlen(toString(v_r))), 2LL)) {
              if (same(v_state, "i")) {
                concat_assign(v_output, "</i>");
                (v_state = "");
              }
              else if (same(v_state, "bi")) {
                concat_assign(v_output, "</i>");
                (v_state = "b");
              }
              else if (same(v_state, "ib")) {
                concat_assign(v_output, "</b></i><b>");
                (v_state = "b");
              }
              else if (same(v_state, "both")) {
                concat_assign(v_output, LINE(1206,concat3("<b><i>", toString(v_buffer), "</i>")));
                (v_state = "b");
              }
              else {
                concat_assign(v_output, "<i>");
                concat_assign(v_state, "i");
              }
            }
            else if (equal(LINE(1210,x_strlen(toString(v_r))), 3LL)) {
              if (same(v_state, "b")) {
                concat_assign(v_output, "</b>");
                (v_state = "");
              }
              else if (same(v_state, "bi")) {
                concat_assign(v_output, "</i></b><i>");
                (v_state = "i");
              }
              else if (same(v_state, "ib")) {
                concat_assign(v_output, "</b>");
                (v_state = "i");
              }
              else if (same(v_state, "both")) {
                concat_assign(v_output, LINE(1219,concat3("<i><b>", toString(v_buffer), "</b>")));
                (v_state = "i");
              }
              else {
                concat_assign(v_output, "<b>");
                concat_assign(v_state, "b");
              }
            }
            else if (equal(LINE(1223,x_strlen(toString(v_r))), 5LL)) {
              if (same(v_state, "b")) {
                concat_assign(v_output, "</b><i>");
                (v_state = "i");
              }
              else if (same(v_state, "i")) {
                concat_assign(v_output, "</i><b>");
                (v_state = "b");
              }
              else if (same(v_state, "bi")) {
                concat_assign(v_output, "</i></b>");
                (v_state = "");
              }
              else if (same(v_state, "ib")) {
                concat_assign(v_output, "</b></i>");
                (v_state = "");
              }
              else if (same(v_state, "both")) {
                concat_assign(v_output, LINE(1234,concat3("<i><b>", toString(v_buffer), "</b></i>")));
                (v_state = "");
              }
              else {
                (v_buffer = "");
                (v_state = "both");
              }
            }
          }
          v_i++;
        }
      }
    }
    if (same(v_state, "b") || same(v_state, "ib")) concat_assign(v_output, "</b>");
    if (same(v_state, "i") || same(v_state, "bi") || same(v_state, "ib")) concat_assign(v_output, "</i>");
    if (same(v_state, "bi")) concat_assign(v_output, "</b>");
    if (same(v_state, "both") && toBoolean(v_buffer)) concat_assign(v_output, LINE(1250,concat3("<b><i>", toString(v_buffer), "</i></b>")));
    return v_output;
  }
  return null;
} /* function */
/* SRC: Parser.php line 1263 */
Variant c_parser::t_replaceexternallinks(Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::replaceExternalLinks);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  Variant eo_8;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_sk;
  Variant v_bits;
  Variant v_s;
  int64 v_i = 0;
  Variant v_url;
  Variant v_protocol;
  Variant v_trail;
  Variant v_m2;
  Variant v_img;
  Variant v_dtrail;
  Variant v_linktype;
  Variant v_langObj;
  Variant v_pasteurized;

  LINE(1265,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceExternalLinks").create()), 0x0000000075359BAFLL));
  (v_sk = LINE(1267,m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_bits = LINE(1269,x_preg_split(m_mExtLinkBracketedRegex, v_text, toInt32(-1LL), toInt32(2LL) /* PREG_SPLIT_DELIM_CAPTURE */)));
  (v_s = LINE(1270,x_array_shift(ref(v_bits))));
  (v_i = 0LL);
  LOOP_COUNTER(31);
  {
    while (less(v_i, LINE(1273,x_count(v_bits)))) {
      LOOP_COUNTER_CHECK(31);
      {
        (v_url = v_bits.rvalAt(v_i++));
        (v_protocol = v_bits.rvalAt(v_i++));
        (v_text = v_bits.rvalAt(v_i++));
        (v_trail = v_bits.rvalAt(v_i++));
        (v_m2 = ScalarArrays::sa_[0]);
        if (toBoolean(LINE(1283,x_preg_match("/&(lt|gt);/", toString(v_url), ref(v_m2), toInt32(256LL) /* PREG_OFFSET_CAPTURE */)))) {
          (v_text = LINE(1284,(assignCallTemp(eo_0, toString(x_substr(toString(v_url), toInt32(v_m2.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL))))),assignCallTemp(eo_2, toString(v_text)),concat3(eo_0, " ", eo_2))));
          (v_url = LINE(1285,x_substr(toString(v_url), toInt32(0LL), toInt32(v_m2.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
        }
        (v_img = LINE(1290,t_maybemakeexternalimage(v_text)));
        if (!same(v_img, false)) {
          (v_text = v_img);
        }
        (v_dtrail = "");
        (v_linktype = (same(v_text, v_url)) ? (("free")) : (("text")));
        if (equal(v_text, "")) {
          if (!same(LINE(1303,(assignCallTemp(eo_0, toString(invoke_failed("wfurlprotocols", Array(), 0x00000000BD4BD4FELL))),assignCallTemp(eo_1, (assignCallTemp(eo_2, toString(v_protocol)),assignCallTemp(eo_4, toInt32(x_strpos(toString(v_protocol), ":"))),x_substr(eo_2, toInt32(0LL), eo_4))),x_strpos(eo_0, eo_1))), false)) {
            (v_langObj = LINE(1304,t_getfunctionlang()));
            (v_text = LINE(1305,(assignCallTemp(eo_1, toString(v_langObj.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, ++m_mAutonumber))),concat3("[", eo_1, "]"))));
            (v_linktype = "autonumber");
          }
          else {
            (v_text = LINE(1309,x_htmlspecialchars(toString(v_url))));
            (v_linktype = "free");
          }
        }
        else {
          df_lambda_7(LINE(1315,throw_fatal("unknown class linker", ((void*)NULL))), v_dtrail, v_trail);
        }
        (v_text = LINE(1318,gv_wgContLang.o_invoke_few_args("markNoConversion", 0x3AC6A984BC83E71BLL, 1, v_text)));
        (v_url = LINE(1320,throw_fatal("unknown class sanitizer", ((void*)NULL))));
        concat_assign(v_s, LINE(1327,(assignCallTemp(eo_0, toString((assignCallTemp(eo_3, toObject(v_sk)),(assignCallTemp(eo_4, ref(v_url)),assignCallTemp(eo_5, ref(v_text)),assignCallTemp(eo_7, ref(v_linktype)),assignCallTemp(eo_8, ref(t_getexternallinkattribs(v_url))),eo_3.o_invoke("makeExternalLink", Array(ArrayInit(5).set(0, eo_4).set(1, eo_5).set(2, false).set(3, eo_7).set(4, eo_8).create()), 0x29CE649D13E77D3BLL))))),assignCallTemp(eo_1, toString(v_dtrail)),assignCallTemp(eo_2, toString(v_trail)),concat3(eo_0, eo_1, eo_2))));
        (v_pasteurized = LINE(1332,c_parser::t_replaceunusualescapes(v_url)));
        LINE(1333,m_mOutput.o_invoke_few_args("addExternalLink", 0x3FE22FCE3F0B40E8LL, 1, v_pasteurized));
      }
    }
  }
  LINE(1336,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceExternalLinks").create()), 0x00000000B599F276LL));
  return v_s;
} /* function */
/* SRC: Parser.php line 1350 */
Variant c_parser::t_getexternallinkattribs(Variant v_url //  = false
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getExternalLinkAttribs);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_attribs;
  Variant &gv_wgNoFollowLinks __attribute__((__unused__)) = g->GV(wgNoFollowLinks);
  Variant v_wgNoFollowLinks;
  Variant &gv_wgNoFollowNsExceptions __attribute__((__unused__)) = g->GV(wgNoFollowNsExceptions);
  Variant v_wgNoFollowNsExceptions;
  Variant v_ns;
  Variant &gv_wgNoFollowDomainExceptions __attribute__((__unused__)) = g->GV(wgNoFollowDomainExceptions);
  Variant v_wgNoFollowDomainExceptions;
  Variant v_bits;
  Variant v_domain;

  (v_attribs = ScalarArrays::sa_[0]);
  {
    v_wgNoFollowLinks = ref(g->GV(wgNoFollowLinks));
    v_wgNoFollowNsExceptions = ref(g->GV(wgNoFollowNsExceptions));
  }
  (v_ns = LINE(1353,m_mTitle.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)));
  if (toBoolean(v_wgNoFollowLinks) && !(LINE(1354,x_in_array(v_ns, v_wgNoFollowNsExceptions)))) {
    v_attribs.set("rel", ("nofollow"), 0x234BFF7AE368CA91LL);
    v_wgNoFollowDomainExceptions = ref(g->GV(wgNoFollowDomainExceptions));
    if (toBoolean(v_wgNoFollowDomainExceptions)) {
      (v_bits = LINE(1359,invoke_failed("wfparseurl", Array(ArrayInit(1).set(0, ref(v_url)).create()), 0x00000000B00BED86LL)));
      if (LINE(1360,x_is_array(v_bits)) && isset(v_bits, "host", 0x5FFAEBBF2B227E19LL)) {
        {
          LOOP_COUNTER(32);
          for (ArrayIterPtr iter34 = v_wgNoFollowDomainExceptions.begin("parser"); !iter34->end(); iter34->next()) {
            LOOP_COUNTER_CHECK(32);
            v_domain = iter34->second();
            {
              if (equal(LINE(1362,x_substr(toString(v_bits.rvalAt("host", 0x5FFAEBBF2B227E19LL)), toInt32(negate(x_strlen(toString(v_domain)))))), v_domain)) {
                v_attribs.weakRemove("rel", 0x234BFF7AE368CA91LL);
                break;
              }
            }
          }
        }
      }
    }
  }
  if (toBoolean(LINE(1371,m_mOptions.o_invoke_few_args("getExternalLinkTarget", 0x3CCD270F1E382832LL, 0)))) {
    v_attribs.set("target", (LINE(1372,m_mOptions.o_invoke_few_args("getExternalLinkTarget", 0x3CCD270F1E382832LL, 0))), 0x22F784C030DE5CB0LL);
  }
  return v_attribs;
} /* function */
/* SRC: Parser.php line 1388 */
Variant c_parser::ti_replaceunusualescapes(const char* cls, CVarRef v_url) {
  STATIC_METHOD_INJECTION(Parser, Parser::replaceUnusualEscapes);
  return LINE(1390,x_preg_replace_callback("/%[0-9A-Fa-f]{2}/", ScalarArrays::sa_[20], v_url));
} /* function */
/* SRC: Parser.php line 1399 */
Variant c_parser::ti_replaceunusualescapescallback(const char* cls, CVarRef v_matches) {
  STATIC_METHOD_INJECTION(Parser, Parser::replaceUnusualEscapesCallback);
  String v_char;
  int64 v_ord = 0;

  (v_char = LINE(1400,x_urldecode(toString(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
  (v_ord = LINE(1401,x_ord(v_char)));
  if (more(v_ord, 32LL) && less(v_ord, 127LL) && same(LINE(1403,x_strpos("<>\"#{}|\\^~[]`;/\?", v_char)), false)) {
    return v_char;
  }
  else {
    return v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
  }
  return null;
} /* function */
/* SRC: Parser.php line 1417 */
Variant c_parser::t_maybemakeexternalimage(Variant v_url) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::maybeMakeExternalImage);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant v_sk;
  Variant v_imagesfrom;
  bool v_imagesexception = false;
  Variant v_text;
  bool v_imagematch = false;
  Variant v_match;
  Array v_whitelist;
  Variant v_entry;

  (v_sk = LINE(1418,m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_imagesfrom = LINE(1419,m_mOptions.o_invoke_few_args("getAllowExternalImagesFrom", 0x328637508AD8D9BELL, 0)));
  (v_imagesexception = !(empty(v_imagesfrom)));
  (v_text = false);
  if (v_imagesexception && LINE(1423,x_is_array(v_imagesfrom))) {
    (v_imagematch = false);
    {
      LOOP_COUNTER(35);
      for (ArrayIterPtr iter37 = v_imagesfrom.begin("parser"); !iter37->end(); iter37->next()) {
        LOOP_COUNTER_CHECK(35);
        v_match = iter37->second();
        {
          if (same(LINE(1426,x_strpos(toString(v_url), v_match)), 0LL)) {
            (v_imagematch = true);
            break;
          }
        }
      }
    }
  }
  else if (v_imagesexception) {
    (v_imagematch = (same(LINE(1432,x_strpos(toString(v_url), v_imagesfrom)), 0LL)));
  }
  else {
    (v_imagematch = false);
  }
  if (toBoolean(LINE(1436,m_mOptions.o_invoke_few_args("getAllowExternalImages", 0x09A86C9F3335CAB7LL, 0))) || (v_imagesexception && v_imagematch)) {
    if (toBoolean(LINE(1438,x_preg_match("/^(http:\\/\\/|https:\\/\\/)([^][<>\"\\x00-\\x20\\x7F]+)\n\t\t\\/([A-Za-z0-9_.,~%\\-+&;#*\?!=()@\\x80-\\xFF]+)\\.((\?i)gif|png|jpg|jpeg)$/Sx" /* parser::EXT_IMAGE_REGEX */, toString(v_url))))) {
      (v_text = LINE(1440,v_sk.o_invoke_few_args("makeExternalImage", 0x5E12DD62300C6EFELL, 1, v_url)));
    }
  }
  if (!(toBoolean(v_text)) && toBoolean(LINE(1443,m_mOptions.o_invoke_few_args("getEnableImageWhitelist", 0x500FFF199D084BF7LL, 0))) && toBoolean(LINE(1444,x_preg_match("/^(http:\\/\\/|https:\\/\\/)([^][<>\"\\x00-\\x20\\x7F]+)\n\t\t\\/([A-Za-z0-9_.,~%\\-+&;#*\?!=()@\\x80-\\xFF]+)\\.((\?i)gif|png|jpg|jpeg)$/Sx" /* parser::EXT_IMAGE_REGEX */, toString(v_url))))) {
    (v_whitelist = LINE(1445,(assignCallTemp(eo_1, toString(invoke_failed("wfmsgforcontent", Array(ArrayInit(1).set(0, "external_image_whitelist").create()), 0x0000000033607CCALL))),x_explode("\n", eo_1))));
    {
      LOOP_COUNTER(38);
      for (ArrayIter iter40 = v_whitelist.begin("parser"); !iter40.end(); ++iter40) {
        LOOP_COUNTER_CHECK(38);
        v_entry = iter40.second();
        {
          if (same(LINE(1448,x_strpos(toString(v_entry), "#")), 0LL) || same(v_entry, "")) continue;
          if (toBoolean(LINE(1450,(assignCallTemp(eo_0, (assignCallTemp(eo_3, toString(x_str_replace("/", "\\/", v_entry))),concat3("/", eo_3, "/i"))),assignCallTemp(eo_1, toString(v_url)),x_preg_match(eo_0, eo_1))))) {
            (v_text = LINE(1452,v_sk.o_invoke_few_args("makeExternalImage", 0x5E12DD62300C6EFELL, 1, v_url)));
            break;
          }
        }
      }
    }
  }
  return v_text;
} /* function */
/* SRC: Parser.php line 1466 */
Variant c_parser::t_replaceinternallinks(Variant v_s) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::replaceInternalLinks);
  LINE(1467,m_mLinkHolders.o_invoke_few_args("merge", 0x1C2E89C2A927FF7BLL, 1, o_root_invoke_few_args("replaceInternalLinks2", 0x37AA0C66F5C36136LL, 1, v_s)));
  return v_s;
} /* function */
/* SRC: Parser.php line 1477 */
p_linkholderarray c_parser::t_replaceinternallinks2(Variant v_s) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::replaceInternalLinks2);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant &sv_tc __attribute__((__unused__)) = g->sv_parser$$replaceinternallinks2$$tc.lvalAt(this->o_getClassName());
  Variant &inited_sv_tc __attribute__((__unused__)) = g->inited_sv_parser$$replaceinternallinks2$$tc.lvalAt(this->o_getClassName());
  Variant &sv_e1 __attribute__((__unused__)) = g->sv_parser$$replaceinternallinks2$$e1.lvalAt(this->o_getClassName());
  Variant &inited_sv_e1 __attribute__((__unused__)) = g->inited_sv_parser$$replaceinternallinks2$$e1.lvalAt(this->o_getClassName());
  Variant &sv_e1_img __attribute__((__unused__)) = g->sv_parser$$replaceinternallinks2$$e1_img.lvalAt(this->o_getClassName());
  Variant &inited_sv_e1_img __attribute__((__unused__)) = g->inited_sv_parser$$replaceinternallinks2$$e1_img.lvalAt(this->o_getClassName());
  Variant v_sk;
  p_linkholderarray v_holders;
  Variant v_a;
  Variant v_line;
  Variant v_useLinkPrefixExtension;
  Variant v_e2;
  bool v_nottalk = false;
  Variant v_m;
  Variant v_first_prefix;
  Variant v_prefix;
  Variant v_selflink;
  Variant v_useSubpages;
  bool v_might_be_img = false;
  Variant v_text;
  Variant v_trail;
  Variant v_link;
  bool v_noforce = false;
  Variant v_nt;
  Variant v_ns;
  Variant v_iw;
  bool v_found = false;
  Variant v_next_line;
  bool v_wasblank = false;
  Variant v_sortkey;
  Variant v_time;
  Variant v_skip;

  LINE(1480,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2").create()), 0x0000000075359BAFLL));
  LINE(1482,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-setup").create()), 0x0000000075359BAFLL));
  {
    if (!inited_sv_tc) {
      (sv_tc = false);
      inited_sv_tc = true;
    }
    if (!inited_sv_e1) {
      (sv_e1 = null);
      inited_sv_e1 = true;
    }
    if (!inited_sv_e1_img) {
      (sv_e1_img = null);
      inited_sv_e1_img = true;
    }
  }
  if (!(toBoolean(sv_tc))) {
    (sv_tc = concat(toString(LINE(1486,throw_fatal("unknown class title", ((void*)NULL)))), "#%"));
    (sv_e1 = LINE(1488,concat3("/^([", toString(sv_tc), "]+)(\?:\\|(.+\?))\?]](.*)$/sD")));
    (sv_e1_img = LINE(1490,concat3("/^([", toString(sv_tc), "]+)\\|(.*)$/sD")));
  }
  (v_sk = LINE(1493,m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  ((Object)((v_holders = ((Object)(LINE(1494,p_linkholderarray(p_linkholderarray(NEWOBJ(c_linkholderarray)())->create(((Object)(this))))))))));
  (v_a = LINE(1497,throw_fatal("unknown class stringutils", ((void*)NULL))));
  (v_s = LINE(1499,v_a.o_invoke_few_args("current", 0x5B3A4A72846B21DCLL, 0)));
  LINE(1500,v_a.o_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0));
  (v_line = LINE(1501,v_a.o_invoke_few_args("current", 0x5B3A4A72846B21DCLL, 0)));
  (v_s = LINE(1502,x_substr(toString(v_s), toInt32(1LL))));
  (v_useLinkPrefixExtension = LINE(1504,gv_wgContLang.o_invoke_few_args("linkPrefixExtension", 0x015F50DA8CF83919LL, 0)));
  setNull(v_e2);
  if (toBoolean(v_useLinkPrefixExtension)) {
    (v_e2 = LINE(1509,invoke_failed("wfmsgforcontent", Array(ArrayInit(1).set(0, "linkprefix").create()), 0x0000000033607CCALL)));
  }
  if (LINE(1512,x_is_null(m_mTitle))) {
    LINE(1513,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-setup").create()), 0x00000000B599F276LL));
    LINE(1514,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2").create()), 0x00000000B599F276LL));
    throw_exception(LINE(1515,create_object("mwexception", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2: $this->mTitle is null\n").create()))));
  }
  (v_nottalk = !(toBoolean(LINE(1517,m_mTitle.o_invoke_few_args("isTalkPage", 0x761349917BE980CFLL, 0)))));
  if (toBoolean(v_useLinkPrefixExtension)) {
    (v_m = ScalarArrays::sa_[0]);
    if (toBoolean(LINE(1521,x_preg_match(toString(v_e2), toString(v_s), ref(v_m))))) {
      (v_first_prefix = v_m.rvalAt(2LL, 0x486AFCC090D5F98CLL));
    }
    else {
      (v_first_prefix = false);
    }
  }
  else {
    (v_prefix = "");
  }
  if (toBoolean(LINE(1530,gv_wgContLang.o_invoke_few_args("hasVariants", 0x5A1FD2E20BD8DBBBLL, 0)))) {
    (v_selflink = LINE(1531,gv_wgContLang.o_invoke_few_args("convertLinkToAllVariants", 0x276E1379FA1C5660LL, 1, m_mTitle.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0))));
  }
  else {
    (v_selflink = Array(ArrayInit(1).set(0, LINE(1533,m_mTitle.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0))).create()));
  }
  (v_useSubpages = LINE(1535,t_aresubpagesallowed()));
  LINE(1536,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-setup").create()), 0x00000000B599F276LL));
  {
    LOOP_COUNTER(41);
    for (; !same(v_line, false) && !same(v_line, null); LINE(1539,v_a.o_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0)), (v_line = v_a.o_invoke_few_args("current", 0x5B3A4A72846B21DCLL, 0))) {
      LOOP_COUNTER_CHECK(41);
      {
        if (LINE(1541,v_holders->t_isbig())) {
          LINE(1544,v_holders->t_replace(ref(v_s)));
          LINE(1545,v_holders->t_clear());
        }
        if (toBoolean(v_useLinkPrefixExtension)) {
          LINE(1549,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-prefixhandling").create()), 0x0000000075359BAFLL));
          if (toBoolean(LINE(1550,x_preg_match(toString(v_e2), toString(v_s), ref(v_m))))) {
            (v_prefix = v_m.rvalAt(2LL, 0x486AFCC090D5F98CLL));
            (v_s = v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
          }
          else {
            (v_prefix = "");
          }
          if (toBoolean(v_first_prefix)) {
            (v_prefix = v_first_prefix);
            (v_first_prefix = false);
          }
          LINE(1561,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-prefixhandling").create()), 0x00000000B599F276LL));
        }
        (v_might_be_img = false);
        LINE(1566,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-e1").create()), 0x0000000075359BAFLL));
        if (toBoolean(LINE(1567,x_preg_match(toString(sv_e1), toString(v_line), ref(v_m))))) {
          (v_text = v_m.rvalAt(2LL, 0x486AFCC090D5F98CLL));
          if (!same(v_text, "") && same(LINE(1578,x_substr(toString(v_m.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), toInt32(0LL), toInt32(1LL))), "]") && !same(LINE(1579,x_strpos(toString(v_text), "[")), false)) {
            concat_assign(v_text, "]");
            v_m.set(3LL, (LINE(1583,x_substr(toString(v_m.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL)), toInt32(1LL)))), 0x135FDDF6A6BFBBDDLL);
          }
          if (!same(LINE(1586,x_strpos(toString(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), "%")), false)) {
            v_m.set(1LL, (LINE(1588,(assignCallTemp(eo_2, x_urldecode(toString(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))),x_str_replace(ScalarArrays::sa_[21], ScalarArrays::sa_[22], eo_2)))), 0x5BCA7C69B794F8CELL);
          }
          (v_trail = v_m.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL));
        }
        else if (toBoolean(LINE(1591,x_preg_match(toString(sv_e1_img), toString(v_line), ref(v_m))))) {
          (v_might_be_img = true);
          (v_text = v_m.rvalAt(2LL, 0x486AFCC090D5F98CLL));
          if (!same(LINE(1594,x_strpos(toString(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), "%")), false)) {
            v_m.set(1LL, (LINE(1595,x_urldecode(toString(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL))))), 0x5BCA7C69B794F8CELL);
          }
          (v_trail = "");
        }
        else {
          concat_assign(v_s, LINE(1599,concat3(toString(v_prefix), "[[", toString(v_line))));
          LINE(1600,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-e1").create()), 0x00000000B599F276LL));
          continue;
        }
        LINE(1603,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-e1").create()), 0x00000000B599F276LL));
        LINE(1604,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-misc").create()), 0x0000000075359BAFLL));
        if (toBoolean(LINE(1609,(assignCallTemp(eo_0, (assignCallTemp(eo_3, toString(invoke_failed("wfurlprotocols", Array(), 0x00000000BD4BD4FELL))),concat3("/^\\b(\?:", eo_3, ")/"))),assignCallTemp(eo_1, toString(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL))),x_preg_match(eo_0, eo_1))))) {
          concat_assign(v_s, LINE(1610,concat3(toString(v_prefix), "[[", toString(v_line))));
          LINE(1611,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-misc").create()), 0x00000000B599F276LL));
          continue;
        }
        if (toBoolean(v_useSubpages)) {
          (v_link = LINE(1617,t_maybedosubpagelink(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL), ref(v_text))));
        }
        else {
          (v_link = v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
        }
        (v_noforce = (!same(LINE(1622,x_substr(toString(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toInt32(0LL), toInt32(1LL))), ":")));
        if (!(v_noforce)) {
          (v_link = LINE(1625,x_substr(toString(v_link), toInt32(1LL))));
        }
        LINE(1628,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-misc").create()), 0x00000000B599F276LL));
        LINE(1629,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-title").create()), 0x0000000075359BAFLL));
        (v_nt = LINE(1630,throw_fatal("unknown class title", (m_mStripState.o_invoke_few_args("unstripNoWiki", 0x2374179DF576FCA9LL, 1, v_link), (void*)NULL))));
        if (same(v_nt, null)) {
          concat_assign(v_s, LINE(1632,concat3(toString(v_prefix), "[[", toString(v_line))));
          LINE(1633,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-title").create()), 0x00000000B599F276LL));
          continue;
        }
        (v_ns = LINE(1637,v_nt.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)));
        (v_iw = LINE(1638,v_nt.o_invoke_few_args("getInterWiki", 0x04849D85488D825ELL, 0)));
        LINE(1639,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-title").create()), 0x00000000B599F276LL));
        if (v_might_be_img) {
          LINE(1642,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-might_be_img").create()), 0x0000000075359BAFLL));
          if (equal(v_ns, k_NS_FILE) && v_noforce) {
            (v_found = false);
            LOOP_COUNTER(42);
            {
              while (true) {
                LOOP_COUNTER_CHECK(42);
                {
                  LINE(1647,v_a.o_invoke_few_args("next", 0x3C6D50F3BB8102B8LL, 0));
                  (v_next_line = LINE(1648,v_a.o_invoke_few_args("current", 0x5B3A4A72846B21DCLL, 0)));
                  if (same(v_next_line, false) || same(v_next_line, null)) {
                    break;
                  }
                  (v_m = LINE(1652,x_explode("]]", toString(v_next_line), toInt32(3LL))));
                  if (equal(LINE(1653,x_count(v_m)), 3LL)) {
                    (v_found = true);
                    concat_assign(v_text, LINE(1656,concat4("[[", toString(v_m.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "]]", toString(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
                    (v_trail = v_m.rvalAt(2LL, 0x486AFCC090D5F98CLL));
                    break;
                  }
                  else if (equal(LINE(1659,x_count(v_m)), 2LL)) {
                    concat_assign(v_text, LINE(1661,concat4("[[", toString(v_m.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "]]", toString(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL)))));
                  }
                  else {
                    concat_assign(v_text, concat("[[", toString(v_next_line)));
                    break;
                  }
                }
              }
            }
            if (!(v_found)) {
              LINE(1671,v_holders->t_merge(o_root_invoke_few_args("replaceInternalLinks2", 0x37AA0C66F5C36136LL, 1, v_text)));
              concat_assign(v_s, LINE(1672,concat5(toString(v_prefix), "[[", toString(v_link), "|", toString(v_text))));
              LINE(1674,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-might_be_img").create()), 0x00000000B599F276LL));
              continue;
            }
          }
          else {
            concat_assign(v_s, LINE(1678,concat5(toString(v_prefix), "[[", toString(v_link), "|", toString(v_text))));
            LINE(1680,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-might_be_img").create()), 0x00000000B599F276LL));
            continue;
          }
          LINE(1683,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-might_be_img").create()), 0x00000000B599F276LL));
        }
        (v_wasblank = (equal("", v_text)));
        if (v_wasblank) (v_text = v_link);
        if (v_noforce) {
          LINE(1693,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-interwiki").create()), 0x0000000075359BAFLL));
          if (toBoolean(v_iw) && toBoolean(LINE(1694,m_mOptions.o_invoke_few_args("getInterwikiMagic", 0x269DAD3410FFF8C6LL, 0))) && v_nottalk && toBoolean(gv_wgContLang.o_invoke_few_args("getLanguageName", 0x3D24C2035027A7DELL, 1, v_iw))) {
            LINE(1695,m_mOutput.o_invoke_few_args("addLanguageLink", 0x657AED0066D1BEECLL, 1, v_nt.o_invoke_few_args("getFullText", 0x3200D2E195D7162ALL, 0)));
            (v_s = LINE(1696,x_rtrim(concat(toString(v_s), toString(v_prefix)))));
            concat_assign(v_s, toString(equal(LINE(1697,x_trim(toString(v_trail), "\n")), "") ? ((Variant)("")) : ((Variant)(concat(toString(v_prefix), toString(v_trail))))));
            LINE(1698,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-interwiki").create()), 0x00000000B599F276LL));
            continue;
          }
          LINE(1701,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-interwiki").create()), 0x00000000B599F276LL));
          if (equal(v_ns, k_NS_FILE)) {
            LINE(1704,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-image").create()), 0x0000000075359BAFLL));
            if (!(toBoolean(LINE(1705,(assignCallTemp(eo_0, ref(v_nt.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0))),assignCallTemp(eo_1, ref(m_mTitle)),invoke_failed("wfisbadimage", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x00000000F72DE574LL)))))) {
              if (v_wasblank) {
                (v_text = "");
              }
              else {
                (v_text = LINE(1716,t_replaceexternallinks(v_text)));
                LINE(1717,v_holders->t_merge(o_root_invoke_few_args("replaceInternalLinks2", 0x37AA0C66F5C36136LL, 1, v_text)));
              }
              concat_assign(v_s, LINE(1720,(assignCallTemp(eo_0, toString(v_prefix)),assignCallTemp(eo_1, toString(t_armorlinks(t_makeimage(v_nt, v_text, ((Object)(v_holders)))))),assignCallTemp(eo_2, toString(v_trail)),concat3(eo_0, eo_1, eo_2))));
            }
            LINE(1722,m_mOutput.o_invoke_few_args("addImage", 0x1026F251B81BD927LL, 1, v_nt.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)));
            LINE(1723,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-image").create()), 0x00000000B599F276LL));
            continue;
          }
          if (equal(v_ns, k_NS_CATEGORY)) {
            LINE(1729,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-category").create()), 0x0000000075359BAFLL));
            (v_s = LINE(1730,x_rtrim(concat(toString(v_s), "\n"))));
            if (v_wasblank) {
              (v_sortkey = LINE(1733,t_getdefaultsort()));
            }
            else {
              (v_sortkey = v_text);
            }
            (v_sortkey = LINE(1737,throw_fatal("unknown class sanitizer", ((void*)NULL))));
            (v_sortkey = LINE(1738,x_str_replace("\n", "", v_sortkey)));
            (v_sortkey = LINE(1739,gv_wgContLang.o_invoke_few_args("convertCategoryKey", 0x11AD19C1DBDF452ALL, 1, v_sortkey)));
            (assignCallTemp(eo_0, ref(LINE(1740,v_nt.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)))),assignCallTemp(eo_1, ref(v_sortkey)),m_mOutput.o_invoke("addCategory", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x6DBAA9816B039793LL));
            concat_assign(v_s, toString(equal(LINE(1746,x_trim(concat(toString(v_prefix), toString(v_trail)), "\n")), "") ? ((Variant)("")) : ((Variant)(concat(toString(v_prefix), toString(v_trail))))));
            LINE(1748,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-category").create()), 0x00000000B599F276LL));
            continue;
          }
        }
        if (same(LINE(1754,v_nt.o_invoke_few_args("getFragment", 0x2354C40909546D77LL, 0)), "") && !equal(v_ns, k_NS_SPECIAL)) {
          if (LINE(1755,(assignCallTemp(eo_0, v_nt.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0)),assignCallTemp(eo_1, v_selflink),x_in_array(eo_0, eo_1, true)))) {
            concat_assign(v_s, concat(toString(v_prefix), toString(LINE(1756,v_sk.o_invoke_few_args("makeSelfLinkObj", 0x4D117586D20EE5FCLL, 4, v_nt, v_text, "", v_trail)))));
            continue;
          }
        }
        if (equal(v_ns, k_NS_MEDIA)) {
          LINE(1764,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-media").create()), 0x0000000075359BAFLL));
          (v_skip = (v_time = false));
          LINE(1767,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "BeforeParserMakeImageLinkObj").set(1, Array(ArrayInit(4).setRef(0, ref(this)).setRef(1, ref(v_nt)).setRef(2, ref(v_skip)).setRef(3, ref(v_time)).create())).create()), 0x00000000787A96B2LL));
          if (toBoolean(v_skip)) {
            (v_link = LINE(1769,v_sk.o_invoke_few_args("link", 0x230FE1D6EC599525LL, 1, v_nt)));
          }
          else {
            (v_link = LINE(1771,v_sk.o_invoke_few_args("makeMediaLinkObj", 0x3781EB5896E49D3ELL, 3, v_nt, v_text, v_time)));
          }
          concat_assign(v_s, LINE(1774,(assignCallTemp(eo_0, toString(v_prefix)),assignCallTemp(eo_1, toString(t_armorlinks(v_link))),assignCallTemp(eo_2, toString(v_trail)),concat3(eo_0, eo_1, eo_2))));
          LINE(1775,m_mOutput.o_invoke_few_args("addImage", 0x1026F251B81BD927LL, 1, v_nt.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)));
          LINE(1776,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-media").create()), 0x00000000B599F276LL));
          continue;
        }
        LINE(1780,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-always_known").create()), 0x0000000075359BAFLL));
        if (equal(v_iw, "") && toBoolean(LINE(1786,v_nt.o_invoke_few_args("isAlwaysKnown", 0x32DB35272B7DDB90LL, 0)))) {
          LINE(1787,m_mOutput.o_invoke_few_args("addLink", 0x5D10AC6ADCA895ACLL, 1, v_nt));
          concat_assign(v_s, LINE(1788,t_makeknownlinkholder(v_nt, v_text, "", v_trail, v_prefix)));
        }
        else {
          concat_assign(v_s, LINE(1791,v_holders->t_makeholder(v_nt, v_text, "", v_trail, v_prefix)));
        }
        LINE(1793,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2-always_known").create()), 0x00000000B599F276LL));
      }
    }
  }
  LINE(1795,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceInternalLinks2").create()), 0x00000000B599F276LL));
  return ((Object)(v_holders));
} /* function */
/* SRC: Parser.php line 1807 */
Variant c_parser::t_makelinkholder(Variant v_nt, Variant v_text //  = ""
, Variant v_query //  = ""
, Variant v_trail //  = ""
, Variant v_prefix //  = ""
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::makeLinkHolder);
  return LINE(1808,m_mLinkHolders.o_invoke_few_args("makeHolder", 0x0B6C33D73053B0C5LL, 5, v_nt, v_text, v_query, v_trail, v_prefix));
} /* function */
/* SRC: Parser.php line 1825 */
String c_parser::t_makeknownlinkholder(Variant v_nt, Variant v_text //  = ""
, Variant v_query //  = ""
, Variant v_trail //  = ""
, Variant v_prefix //  = ""
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::makeKnownLinkHolder);
  Variant v_inside;
  Variant v_sk;
  Variant v_link;

  df_lambda_8(LINE(1826,throw_fatal("unknown class linker", ((void*)NULL))), v_inside, v_trail);
  (v_sk = LINE(1827,m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_link = LINE(1829,v_sk.o_invoke_few_args("makeKnownLinkObj", 0x1C18E0A6D7DBDF99LL, 5, v_nt, v_text, v_query, v_inside, v_prefix)));
  return concat(toString(LINE(1830,t_armorlinks(v_link))), toString(v_trail));
} /* function */
/* SRC: Parser.php line 1843 */
Variant c_parser::t_armorlinks(CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::armorLinks);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  return LINE(1845,(assignCallTemp(eo_0, LINE(1844,(assignCallTemp(eo_4, toString(invoke_failed("wfurlprotocols", Array(), 0x00000000BD4BD4FELL))),concat3("/\\b(", eo_4, ")/")))),assignCallTemp(eo_1, toString(o_get("mUniqPrefix", 0x22D00FA247E8311DLL)) + toString("NOPARSE$1")),assignCallTemp(eo_2, v_text),x_preg_replace(eo_0, eo_1, eo_2)));
} /* function */
/* SRC: Parser.php line 1852 */
Variant c_parser::t_aresubpagesallowed() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::areSubpagesAllowed);
  return LINE(1854,throw_fatal("unknown class mwnamespace", (m_mTitle.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0), (void*)NULL)));
} /* function */
/* SRC: Parser.php line 1864 */
Variant c_parser::t_maybedosubpagelink(CVarRef v_target, Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::maybeDoSubpageLink);
  return LINE(1865,throw_fatal("unknown class linker", ((void*)NULL)));
} /* function */
/* SRC: Parser.php line 1872 */
String c_parser::t_closeparagraph() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::closeParagraph);
  String v_result;

  (v_result = "");
  if (!equal("", m_mLastSection)) {
    (v_result = LINE(1875,concat3("</", toString(m_mLastSection), ">\n")));
  }
  (m_mInPre = false);
  (m_mLastSection = "");
  return v_result;
} /* function */
/* SRC: Parser.php line 1884 */
int64 c_parser::t_getcommon(CVarRef v_st1, CVarRef v_st2) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getCommon);
  int v_fl = 0;
  int v_shorter = 0;
  int64 v_i = 0;

  (v_fl = LINE(1885,x_strlen(toString(v_st1))));
  (v_shorter = LINE(1886,x_strlen(toString(v_st2))));
  if (less(v_fl, v_shorter)) {
    (v_shorter = v_fl);
  }
  {
    LOOP_COUNTER(43);
    for ((v_i = 0LL); less(v_i, v_shorter); ++v_i) {
      LOOP_COUNTER_CHECK(43);
      {
        if (!equal(v_st1.rvalAt(v_i), v_st2.rvalAt(v_i))) {
          break;
        }
      }
    }
  }
  return v_i;
} /* function */
/* SRC: Parser.php line 1897 */
String c_parser::t_openlist(CVarRef v_char) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::openList);
  String v_result;

  (v_result = LINE(1898,t_closeparagraph()));
  if (same("*", v_char)) {
    concat_assign(v_result, "<ul><li>");
  }
  else if (same("#", v_char)) {
    concat_assign(v_result, "<ol><li>");
  }
  else if (same(":", v_char)) {
    concat_assign(v_result, "<dl><dd>");
  }
  else if (same(";", v_char)) {
    concat_assign(v_result, "<dl><dt>");
    (m_mDTopen = true);
  }
  else {
    (v_result = "<!-- ERR 1 -->");
  }
  return v_result;
} /* function */
/* SRC: Parser.php line 1912 */
String c_parser::t_nextitem(CVarRef v_char) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::nextItem);
  String v_close;

  if (same("*", v_char) || same("#", v_char)) {
    return "</li><li>";
  }
  else if (same(":", v_char) || same(";", v_char)) {
    (v_close = "</dd>");
    if (toBoolean(m_mDTopen)) {
      (v_close = "</dt>");
    }
    if (same(";", v_char)) {
      (m_mDTopen = true);
      return concat(v_close, "<dt>");
    }
    else {
      (m_mDTopen = false);
      return concat(v_close, "<dd>");
    }
  }
  return "<!-- ERR 2 -->";
} /* function */
/* SRC: Parser.php line 1928 */
String c_parser::t_closelist(CVarRef v_char) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::closeList);
  String v_text;

  if (same("*", v_char)) {
    (v_text = "</li></ul>");
  }
  else if (same("#", v_char)) {
    (v_text = "</li></ol>");
  }
  else if (same(":", v_char)) {
    if (toBoolean(m_mDTopen)) {
      (m_mDTopen = false);
      (v_text = "</dt></dl>");
    }
    else {
      (v_text = "</dd></dl>");
    }
  }
  else {
    return "<!-- ERR 3 -->";
  }
  return concat(v_text, "\n");
} /* function */
/* SRC: Parser.php line 1951 */
Variant c_parser::t_doblocklevels(CVarRef v_text, bool v_linestart) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::doBlockLevels);
  Variant eo_0;
  Variant eo_1;
  Variant v_textLines;
  Variant v_output;
  Variant v_lastPrefix;
  bool v_inBlockElem = false;
  Variant v_prefixLength;
  Variant v_paragraphStack;
  Variant v_oLine;
  int v_lastPrefixLength = 0;
  Variant v_preCloseMatch;
  Variant v_preOpenMatch;
  Variant v_prefix;
  Variant v_prefix2;
  Variant v_t;
  Variant v_t2;
  Variant v_term;
  int64 v_commonPrefixLength = 0;
  Variant v_char;
  Variant v_openmatch;
  Variant v_closematch;

  LINE(1952,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::doBlockLevels").create()), 0x0000000075359BAFLL));
  (v_textLines = LINE(1958,throw_fatal("unknown class stringutils", ((void*)NULL))));
  (v_lastPrefix = (v_output = ""));
  (m_mDTopen = (v_inBlockElem = false));
  (v_prefixLength = 0LL);
  (v_paragraphStack = false);
  {
    LOOP_COUNTER(44);
    for (ArrayIterPtr iter46 = v_textLines.begin("parser"); !iter46->end(); iter46->next()) {
      LOOP_COUNTER_CHECK(44);
      v_oLine = iter46->second();
      {
        if (!(v_linestart)) {
          concat_assign(v_output, toString(v_oLine));
          (v_linestart = true);
          continue;
        }
        (v_lastPrefixLength = LINE(1977,x_strlen(toString(v_lastPrefix))));
        (v_preCloseMatch = LINE(1978,x_preg_match("/<\\/pre/i", toString(v_oLine))));
        (v_preOpenMatch = LINE(1979,x_preg_match("/<pre/i", toString(v_oLine))));
        if (!(toBoolean(m_mInPre))) {
          (v_prefixLength = LINE(1983,x_strspn(toString(v_oLine), "*#:;")));
          (v_prefix = LINE(1984,x_substr(toString(v_oLine), toInt32(0LL), toInt32(v_prefixLength))));
          (v_prefix2 = LINE(1990,x_str_replace(";", ":", v_prefix)));
          (v_t = LINE(1991,x_substr(toString(v_oLine), toInt32(v_prefixLength))));
          (m_mInPre = toBoolean(v_preOpenMatch));
        }
        else {
          (v_prefixLength = 0LL);
          (v_prefix = (v_prefix2 = ""));
          (v_t = v_oLine);
        }
        if (toBoolean(v_prefixLength) && same(v_lastPrefix, v_prefix2)) {
          concat_assign(v_output, LINE(2003,t_nextitem(x_substr(toString(v_prefix), toInt32(-1LL)))));
          (v_paragraphStack = false);
          if (same(LINE(2006,x_substr(toString(v_prefix), toInt32(-1LL))), ";")) {
            (v_term = (v_t2 = ""));
            if (!same(LINE(2012,t_findcolonnolinks(v_t, ref(v_term), ref(v_t2))), false)) {
              (v_t = v_t2);
              concat_assign(v_output, concat(toString(v_term), LINE(2014,t_nextitem(":"))));
            }
          }
        }
        else if (toBoolean(v_prefixLength) || toBoolean(v_lastPrefixLength)) {
          (v_commonPrefixLength = LINE(2021,t_getcommon(v_prefix, v_lastPrefix)));
          (v_paragraphStack = false);
          LOOP_COUNTER(47);
          {
            while (less(v_commonPrefixLength, v_lastPrefixLength)) {
              LOOP_COUNTER_CHECK(47);
              {
                concat_assign(v_output, LINE(2026,t_closelist(v_lastPrefix.rvalAt(v_lastPrefixLength - 1LL))));
                --v_lastPrefixLength;
              }
            }
          }
          if (not_more(v_prefixLength, v_commonPrefixLength) && more(v_commonPrefixLength, 0LL)) {
            concat_assign(v_output, LINE(2032,t_nextitem(v_prefix.rvalAt(v_commonPrefixLength - 1LL))));
          }
          LOOP_COUNTER(48);
          {
            while (more(v_prefixLength, v_commonPrefixLength)) {
              LOOP_COUNTER_CHECK(48);
              {
                (v_char = LINE(2037,x_substr(toString(v_prefix), toInt32(v_commonPrefixLength), toInt32(1LL))));
                concat_assign(v_output, LINE(2038,t_openlist(v_char)));
                if (same(";", v_char)) {
                  if (!same(LINE(2042,t_findcolonnolinks(v_t, ref(v_term), ref(v_t2))), false)) {
                    (v_t = v_t2);
                    concat_assign(v_output, concat(toString(v_term), LINE(2044,t_nextitem(":"))));
                  }
                }
                ++v_commonPrefixLength;
              }
            }
          }
          (v_lastPrefix = v_prefix2);
        }
        if (equal(0LL, v_prefixLength)) {
          LINE(2054,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::doBlockLevels-paragraph").create()), 0x0000000075359BAFLL));
          (v_openmatch = LINE(2057,x_preg_match("/(\?:<table|<blockquote|<h1|<h2|<h3|<h4|<h5|<h6|<pre|<tr|<p|<ul|<ol|<li|<\\/tr|<\\/td|<\\/th)/iS", toString(v_t))));
          (v_closematch = LINE(2060,(assignCallTemp(eo_0, concat3("/(\?:<\\/table|<\\/blockquote|<\\/h1|<\\/h2|<\\/h3|<\\/h4|<\\/h5|<\\/h6|<td|<th|<\\/\?div|<hr|<\\/pre|<\\/p|", toString(o_get("mUniqPrefix", 0x22D00FA247E8311DLL)), "-pre|<\\/li|<\\/ul|<\\/ol|<\\/\?center)/iS")),assignCallTemp(eo_1, toString(v_t)),x_preg_match(eo_0, eo_1))));
          if ((toBoolean(v_openmatch)) || (toBoolean(v_closematch))) {
            (v_paragraphStack = false);
            concat_assign(v_output, LINE(2064,t_closeparagraph()));
            if ((toBoolean(v_preOpenMatch)) && (!(toBoolean(v_preCloseMatch)))) {
              (m_mInPre = true);
            }
            if (toBoolean(v_closematch)) {
              (v_inBlockElem = false);
            }
            else {
              (v_inBlockElem = true);
            }
          }
          else if (!(v_inBlockElem) && !(toBoolean(m_mInPre))) {
            if ((equal(" ", LINE(2074,x_substr(toString(v_t), toInt32(0LL), toInt32(1LL))))) && (((same(m_mLastSection, "pre")) || (!equal(x_trim(toString(v_t)), ""))))) {
              if (!same(m_mLastSection, "pre")) {
                (v_paragraphStack = false);
                concat_assign(v_output, concat(LINE(2078,t_closeparagraph()), "<pre>"));
                (m_mLastSection = "pre");
              }
              (v_t = LINE(2081,x_substr(toString(v_t), toInt32(1LL))));
            }
            else {
              if (equal("", LINE(2084,x_trim(toString(v_t))))) {
                if (toBoolean(v_paragraphStack)) {
                  concat_assign(v_output, concat(toString(v_paragraphStack), "<br />"));
                  (v_paragraphStack = false);
                  (m_mLastSection = "p");
                }
                else {
                  if (!same(m_mLastSection, "p")) {
                    concat_assign(v_output, LINE(2091,t_closeparagraph()));
                    (m_mLastSection = "");
                    (v_paragraphStack = "<p>");
                  }
                  else {
                    (v_paragraphStack = "</p><p>");
                  }
                }
              }
              else {
                if (toBoolean(v_paragraphStack)) {
                  concat_assign(v_output, toString(v_paragraphStack));
                  (v_paragraphStack = false);
                  (m_mLastSection = "p");
                }
                else if (!same(m_mLastSection, "p")) {
                  concat_assign(v_output, concat(LINE(2104,t_closeparagraph()), "<p>"));
                  (m_mLastSection = "p");
                }
              }
            }
          }
          LINE(2110,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::doBlockLevels-paragraph").create()), 0x00000000B599F276LL));
        }
        if (toBoolean(v_preCloseMatch) && toBoolean(m_mInPre)) {
          (m_mInPre = false);
        }
        if (same(v_paragraphStack, false)) {
          concat_assign(v_output, concat(toString(v_t), "\n"));
        }
      }
    }
  }
  LOOP_COUNTER(49);
  {
    while (toBoolean(v_prefixLength)) {
      LOOP_COUNTER_CHECK(49);
      {
        concat_assign(v_output, LINE(2121,t_closelist(v_prefix2.rvalAt(v_prefixLength - 1LL))));
        --v_prefixLength;
      }
    }
  }
  if (!equal("", m_mLastSection)) {
    concat_assign(v_output, LINE(2125,concat3("</", toString(m_mLastSection), ">")));
    (m_mLastSection = "");
  }
  LINE(2129,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::doBlockLevels").create()), 0x00000000B599F276LL));
  return v_output;
} /* function */
/* SRC: Parser.php line 2141 */
Variant c_parser::t_findcolonnolinks(CVarRef v_str, Variant v_before, Variant v_after) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::findColonNoLinks);
  Variant v_pos;
  Variant v_lt;
  int64 v_state = 0;
  int64 v_stack = 0;
  int v_len = 0;
  Variant v_i;
  Variant v_c;
  Variant v_colon;

  LINE(2142,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::findColonNoLinks").create()), 0x0000000075359BAFLL));
  (v_pos = LINE(2144,x_strpos(toString(v_str), ":")));
  if (same(v_pos, false)) {
    LINE(2147,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::findColonNoLinks").create()), 0x00000000B599F276LL));
    return false;
  }
  (v_lt = LINE(2151,x_strpos(toString(v_str), "<")));
  if (same(v_lt, false) || more(v_lt, v_pos)) {
    (v_before = LINE(2154,x_substr(toString(v_str), toInt32(0LL), toInt32(v_pos))));
    (v_after = LINE(2155,x_substr(toString(v_str), toInt32(v_pos + 1LL))));
    LINE(2156,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::findColonNoLinks").create()), 0x00000000B599F276LL));
    return v_pos;
  }
  (v_state = 0LL /* parser::COLON_STATE_TEXT */);
  (v_stack = 0LL);
  (v_len = LINE(2163,x_strlen(toString(v_str))));
  {
    LOOP_COUNTER(50);
    for ((v_i = 0LL); less(v_i, v_len); v_i++) {
      LOOP_COUNTER_CHECK(50);
      {
        (v_c = v_str.rvalAt(v_i));
        {
          switch (v_state) {
          case 0LL:
            {
              {
                Variant tmp53 = (v_c);
                int tmp54 = -1;
                if (equal(tmp53, ("<"))) {
                  tmp54 = 0;
                } else if (equal(tmp53, (":"))) {
                  tmp54 = 1;
                } else if (true) {
                  tmp54 = 2;
                }
                switch (tmp54) {
                case 0:
                  {
                    (v_state = 2LL /* parser::COLON_STATE_TAGSTART */);
                    goto break52;
                  }
                case 1:
                  {
                    if (equal(v_stack, 0LL)) {
                      (v_before = LINE(2178,x_substr(toString(v_str), toInt32(0LL), toInt32(v_i))));
                      (v_after = LINE(2179,x_substr(toString(v_str), toInt32(v_i + 1LL))));
                      LINE(2180,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::findColonNoLinks").create()), 0x00000000B599F276LL));
                      return v_i;
                    }
                    goto break52;
                  }
                case 2:
                  {
                    (v_colon = LINE(2187,x_strpos(toString(v_str), ":", toInt32(v_i))));
                    if (same(v_colon, false)) {
                      LINE(2190,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::findColonNoLinks").create()), 0x00000000B599F276LL));
                      return false;
                    }
                    (v_lt = LINE(2193,x_strpos(toString(v_str), "<", toInt32(v_i))));
                    if (same(v_stack, 0LL)) {
                      if (same(v_lt, false) || less(v_colon, v_lt)) {
                        (v_before = LINE(2197,x_substr(toString(v_str), toInt32(0LL), toInt32(v_colon))));
                        (v_after = LINE(2198,x_substr(toString(v_str), toInt32(v_colon + 1LL))));
                        LINE(2199,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::findColonNoLinks").create()), 0x00000000B599F276LL));
                        return v_i;
                      }
                    }
                    if (same(v_lt, false)) {
                      goto break51;
                    }
                    (v_i = v_lt);
                    (v_state = 2LL /* parser::COLON_STATE_TAGSTART */);
                  }
                }
                break52:;
              }
              break;
            }
          case 1LL:
            {
              {
                Variant tmp56 = (v_c);
                int tmp57 = -1;
                if (equal(tmp56, (">"))) {
                  tmp57 = 0;
                } else if (equal(tmp56, ("/"))) {
                  tmp57 = 1;
                } else if (true) {
                  tmp57 = 2;
                }
                switch (tmp57) {
                case 0:
                  {
                    v_stack++;
                    (v_state = 0LL /* parser::COLON_STATE_TEXT */);
                    goto break55;
                  }
                case 1:
                  {
                    (v_state = 4LL /* parser::COLON_STATE_TAGSLASH */);
                    goto break55;
                  }
                case 2:
                  {
                  }
                }
                break55:;
              }
              break;
            }
          case 2LL:
            {
              {
                Variant tmp59 = (v_c);
                int tmp60 = -1;
                if (equal(tmp59, ("/"))) {
                  tmp60 = 0;
                } else if (equal(tmp59, ("!"))) {
                  tmp60 = 1;
                } else if (equal(tmp59, (">"))) {
                  tmp60 = 2;
                } else if (true) {
                  tmp60 = 3;
                }
                switch (tmp60) {
                case 0:
                  {
                    (v_state = 3LL /* parser::COLON_STATE_CLOSETAG */);
                    goto break58;
                  }
                case 1:
                  {
                    (v_state = 5LL /* parser::COLON_STATE_COMMENT */);
                    goto break58;
                  }
                case 2:
                  {
                    (v_state = 0LL /* parser::COLON_STATE_TEXT */);
                    goto break58;
                  }
                case 3:
                  {
                    (v_state = 1LL /* parser::COLON_STATE_TAG */);
                  }
                }
                break58:;
              }
              break;
            }
          case 3LL:
            {
              if (same(v_c, ">")) {
                v_stack--;
                if (less(v_stack, 0LL)) {
                  LINE(2249,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser::findColonNoLinks: Invalid input; too many close tags\n").create()), 0x00000000E441E905LL));
                  LINE(2250,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::findColonNoLinks").create()), 0x00000000B599F276LL));
                  return false;
                }
                (v_state = 0LL /* parser::COLON_STATE_TEXT */);
              }
              break;
            }
          case 4LL:
            {
              if (same(v_c, ">")) {
                (v_state = 0LL /* parser::COLON_STATE_TEXT */);
              }
              else {
                (v_state = 1LL /* parser::COLON_STATE_TAG */);
              }
              break;
            }
          case 5LL:
            {
              if (same(v_c, "-")) {
                (v_state = 6LL /* parser::COLON_STATE_COMMENTDASH */);
              }
              break;
            }
          case 6LL:
            {
              if (same(v_c, "-")) {
                (v_state = 7LL /* parser::COLON_STATE_COMMENTDASHDASH */);
              }
              else {
                (v_state = 5LL /* parser::COLON_STATE_COMMENT */);
              }
              break;
            }
          case 7LL:
            {
              if (same(v_c, ">")) {
                (v_state = 0LL /* parser::COLON_STATE_TEXT */);
              }
              else {
                (v_state = 5LL /* parser::COLON_STATE_COMMENT */);
              }
              break;
            }
          default:
            {
              throw_exception(LINE(2285,create_object("mwexception", Array(ArrayInit(1).set(0, "State machine error in Parser::findColonNoLinks").create()))));
            }
          }
          break51:;
        }
      }
    }
  }
  if (more(v_stack, 0LL)) {
    LINE(2289,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, concat("Parser::findColonNoLinks", concat5(": Invalid input; not enough close tags (stack ", toString(v_stack), ", state ", toString(v_state), ")\n"))).create()), 0x00000000E441E905LL));
    return false;
  }
  LINE(2292,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::findColonNoLinks").create()), 0x00000000B599F276LL));
  return false;
} /* function */
/* SRC: Parser.php line 2301 */
Variant c_parser::t_getvariablevalue(Variant v_index, Variant v_frame //  = false
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getVariableValue);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant &gv_wgSitename __attribute__((__unused__)) = g->GV(wgSitename);
  Variant &gv_wgServer __attribute__((__unused__)) = g->GV(wgServer);
  Variant &gv_wgServerName __attribute__((__unused__)) = g->GV(wgServerName);
  Variant &gv_wgScriptPath __attribute__((__unused__)) = g->GV(wgScriptPath);
  Variant &gv_wgStylePath __attribute__((__unused__)) = g->GV(wgStylePath);
  Variant v_ts;
  Variant &gv_wgLocaltimezone __attribute__((__unused__)) = g->GV(wgLocaltimezone);
  Variant v_oldtz;
  Variant v_localTimestamp;
  Variant v_localMonth;
  Variant v_localMonth1;
  Variant v_localMonthName;
  Variant v_localDay;
  Variant v_localDay2;
  Variant v_localDayOfWeek;
  String v_localWeek;
  Variant v_localYear;
  Variant v_localHour;
  Variant v_value;
  Variant v_talkPage;
  Variant v_subjPage;
  Variant &gv_wgContLanguageCode __attribute__((__unused__)) = g->GV(wgContLanguageCode);
  Variant v_wgContLanguageCode;
  Variant v_ret;

  {
  }
  {
  }
  if (toBoolean(LINE(2309,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserGetVariableValueVarCache").set(1, Array(ArrayInit(2).setRef(0, ref(this)).setRef(1, ref(lval(m_mVarCache))).create())).create()), 0x00000000787A96B2LL)))) {
    if (isset(m_mVarCache, v_index)) {
      return m_mVarCache.rvalAt(v_index);
    }
  }
  (v_ts = LINE(2315,(assignCallTemp(eo_0, k_TS_UNIX),assignCallTemp(eo_1, ref(m_mOptions.o_invoke_few_args("getTimestamp", 0x7533D7D1FB463C81LL, 0))),invoke_failed("wftimestamp", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x00000000AADC1C20LL))));
  LINE(2316,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserGetVariableValueTs").set(1, Array(ArrayInit(2).setRef(0, ref(this)).setRef(1, ref(v_ts)).create())).create()), 0x00000000787A96B2LL));
  if (isset(gv_wgLocaltimezone)) {
    (v_oldtz = LINE(2321,x_getenv("TZ")));
    LINE(2322,x_putenv(concat("TZ=", toString(gv_wgLocaltimezone))));
  }
  LINE(2325,invoke_failed("wfsuppresswarnings", Array(), 0x00000000FCF68A6CLL));
  (v_localTimestamp = LINE(2326,x_date("YmdHis", toInt64(v_ts))));
  (v_localMonth = LINE(2327,x_date("m", toInt64(v_ts))));
  (v_localMonth1 = LINE(2328,x_date("n", toInt64(v_ts))));
  (v_localMonthName = LINE(2329,x_date("n", toInt64(v_ts))));
  (v_localDay = LINE(2330,x_date("j", toInt64(v_ts))));
  (v_localDay2 = LINE(2331,x_date("d", toInt64(v_ts))));
  (v_localDayOfWeek = LINE(2332,x_date("w", toInt64(v_ts))));
  (v_localWeek = LINE(2333,x_date("W", toInt64(v_ts))));
  (v_localYear = LINE(2334,x_date("Y", toInt64(v_ts))));
  (v_localHour = LINE(2335,x_date("H", toInt64(v_ts))));
  if (isset(gv_wgLocaltimezone)) {
    LINE(2337,x_putenv(concat("TZ=", toString(v_oldtz))));
  }
  LINE(2339,invoke_failed("wfrestorewarnings", Array(), 0x000000001BD025E4LL));
  {
    Variant tmp62 = (v_index);
    int tmp63 = -1;
    if (equal(tmp62, ("currentmonth"))) {
      tmp63 = 0;
    } else if (equal(tmp62, ("currentmonth1"))) {
      tmp63 = 1;
    } else if (equal(tmp62, ("currentmonthname"))) {
      tmp63 = 2;
    } else if (equal(tmp62, ("currentmonthnamegen"))) {
      tmp63 = 3;
    } else if (equal(tmp62, ("currentmonthabbrev"))) {
      tmp63 = 4;
    } else if (equal(tmp62, ("currentday"))) {
      tmp63 = 5;
    } else if (equal(tmp62, ("currentday2"))) {
      tmp63 = 6;
    } else if (equal(tmp62, ("localmonth"))) {
      tmp63 = 7;
    } else if (equal(tmp62, ("localmonth1"))) {
      tmp63 = 8;
    } else if (equal(tmp62, ("localmonthname"))) {
      tmp63 = 9;
    } else if (equal(tmp62, ("localmonthnamegen"))) {
      tmp63 = 10;
    } else if (equal(tmp62, ("localmonthabbrev"))) {
      tmp63 = 11;
    } else if (equal(tmp62, ("localday"))) {
      tmp63 = 12;
    } else if (equal(tmp62, ("localday2"))) {
      tmp63 = 13;
    } else if (equal(tmp62, ("pagename"))) {
      tmp63 = 14;
    } else if (equal(tmp62, ("pagenamee"))) {
      tmp63 = 15;
    } else if (equal(tmp62, ("fullpagename"))) {
      tmp63 = 16;
    } else if (equal(tmp62, ("fullpagenamee"))) {
      tmp63 = 17;
    } else if (equal(tmp62, ("subpagename"))) {
      tmp63 = 18;
    } else if (equal(tmp62, ("subpagenamee"))) {
      tmp63 = 19;
    } else if (equal(tmp62, ("basepagename"))) {
      tmp63 = 20;
    } else if (equal(tmp62, ("basepagenamee"))) {
      tmp63 = 21;
    } else if (equal(tmp62, ("talkpagename"))) {
      tmp63 = 22;
    } else if (equal(tmp62, ("talkpagenamee"))) {
      tmp63 = 23;
    } else if (equal(tmp62, ("subjectpagename"))) {
      tmp63 = 24;
    } else if (equal(tmp62, ("subjectpagenamee"))) {
      tmp63 = 25;
    } else if (equal(tmp62, ("revisionid"))) {
      tmp63 = 26;
    } else if (equal(tmp62, ("revisionday"))) {
      tmp63 = 27;
    } else if (equal(tmp62, ("revisionday2"))) {
      tmp63 = 28;
    } else if (equal(tmp62, ("revisionmonth"))) {
      tmp63 = 29;
    } else if (equal(tmp62, ("revisionyear"))) {
      tmp63 = 30;
    } else if (equal(tmp62, ("revisiontimestamp"))) {
      tmp63 = 31;
    } else if (equal(tmp62, ("revisionuser"))) {
      tmp63 = 32;
    } else if (equal(tmp62, ("namespace"))) {
      tmp63 = 33;
    } else if (equal(tmp62, ("namespacee"))) {
      tmp63 = 34;
    } else if (equal(tmp62, ("talkspace"))) {
      tmp63 = 35;
    } else if (equal(tmp62, ("talkspacee"))) {
      tmp63 = 36;
    } else if (equal(tmp62, ("subjectspace"))) {
      tmp63 = 37;
    } else if (equal(tmp62, ("subjectspacee"))) {
      tmp63 = 38;
    } else if (equal(tmp62, ("currentdayname"))) {
      tmp63 = 39;
    } else if (equal(tmp62, ("currentyear"))) {
      tmp63 = 40;
    } else if (equal(tmp62, ("currenttime"))) {
      tmp63 = 41;
    } else if (equal(tmp62, ("currenthour"))) {
      tmp63 = 42;
    } else if (equal(tmp62, ("currentweek"))) {
      tmp63 = 43;
    } else if (equal(tmp62, ("currentdow"))) {
      tmp63 = 44;
    } else if (equal(tmp62, ("localdayname"))) {
      tmp63 = 45;
    } else if (equal(tmp62, ("localyear"))) {
      tmp63 = 46;
    } else if (equal(tmp62, ("localtime"))) {
      tmp63 = 47;
    } else if (equal(tmp62, ("localhour"))) {
      tmp63 = 48;
    } else if (equal(tmp62, ("localweek"))) {
      tmp63 = 49;
    } else if (equal(tmp62, ("localdow"))) {
      tmp63 = 50;
    } else if (equal(tmp62, ("numberofarticles"))) {
      tmp63 = 51;
    } else if (equal(tmp62, ("numberoffiles"))) {
      tmp63 = 52;
    } else if (equal(tmp62, ("numberofusers"))) {
      tmp63 = 53;
    } else if (equal(tmp62, ("numberofactiveusers"))) {
      tmp63 = 54;
    } else if (equal(tmp62, ("numberofpages"))) {
      tmp63 = 55;
    } else if (equal(tmp62, ("numberofadmins"))) {
      tmp63 = 56;
    } else if (equal(tmp62, ("numberofedits"))) {
      tmp63 = 57;
    } else if (equal(tmp62, ("numberofviews"))) {
      tmp63 = 58;
    } else if (equal(tmp62, ("currenttimestamp"))) {
      tmp63 = 59;
    } else if (equal(tmp62, ("localtimestamp"))) {
      tmp63 = 60;
    } else if (equal(tmp62, ("currentversion"))) {
      tmp63 = 61;
    } else if (equal(tmp62, ("sitename"))) {
      tmp63 = 62;
    } else if (equal(tmp62, ("server"))) {
      tmp63 = 63;
    } else if (equal(tmp62, ("servername"))) {
      tmp63 = 64;
    } else if (equal(tmp62, ("scriptpath"))) {
      tmp63 = 65;
    } else if (equal(tmp62, ("directionmark"))) {
      tmp63 = 66;
    } else if (equal(tmp62, ("contentlanguage"))) {
      tmp63 = 67;
    } else if (true) {
      tmp63 = 68;
    }
    switch (tmp63) {
    case 0:
      {
        (v_value = LINE(2343,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, x_gmdate("m", toInt64(v_ts)))));
        goto break61;
      }
    case 1:
      {
        (v_value = LINE(2346,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, x_gmdate("n", toInt64(v_ts)))));
        goto break61;
      }
    case 2:
      {
        (v_value = LINE(2349,gv_wgContLang.o_invoke_few_args("getMonthName", 0x5DEDDA0E21068FE8LL, 1, x_gmdate("n", toInt64(v_ts)))));
        goto break61;
      }
    case 3:
      {
        (v_value = LINE(2352,gv_wgContLang.o_invoke_few_args("getMonthNameGen", 0x4A5939DD128D7FF8LL, 1, x_gmdate("n", toInt64(v_ts)))));
        goto break61;
      }
    case 4:
      {
        (v_value = LINE(2355,gv_wgContLang.o_invoke_few_args("getMonthAbbreviation", 0x75A1001EE20C7275LL, 1, x_gmdate("n", toInt64(v_ts)))));
        goto break61;
      }
    case 5:
      {
        (v_value = LINE(2358,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, x_gmdate("j", toInt64(v_ts)))));
        goto break61;
      }
    case 6:
      {
        (v_value = LINE(2361,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, x_gmdate("d", toInt64(v_ts)))));
        goto break61;
      }
    case 7:
      {
        (v_value = LINE(2364,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, v_localMonth)));
        goto break61;
      }
    case 8:
      {
        (v_value = LINE(2367,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, v_localMonth1)));
        goto break61;
      }
    case 9:
      {
        (v_value = LINE(2370,gv_wgContLang.o_invoke_few_args("getMonthName", 0x5DEDDA0E21068FE8LL, 1, v_localMonthName)));
        goto break61;
      }
    case 10:
      {
        (v_value = LINE(2373,gv_wgContLang.o_invoke_few_args("getMonthNameGen", 0x4A5939DD128D7FF8LL, 1, v_localMonthName)));
        goto break61;
      }
    case 11:
      {
        (v_value = LINE(2376,gv_wgContLang.o_invoke_few_args("getMonthAbbreviation", 0x75A1001EE20C7275LL, 1, v_localMonthName)));
        goto break61;
      }
    case 12:
      {
        (v_value = LINE(2379,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, v_localDay)));
        goto break61;
      }
    case 13:
      {
        (v_value = LINE(2382,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, v_localDay2)));
        goto break61;
      }
    case 14:
      {
        (v_value = LINE(2385,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(m_mTitle.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0))).create()), 0x00000000B3382909LL)));
        goto break61;
      }
    case 15:
      {
        (v_value = LINE(2388,m_mTitle.o_invoke_few_args("getPartialURL", 0x174317C1190941C3LL, 0)));
        goto break61;
      }
    case 16:
      {
        (v_value = LINE(2391,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(m_mTitle.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0))).create()), 0x00000000B3382909LL)));
        goto break61;
      }
    case 17:
      {
        (v_value = LINE(2394,m_mTitle.o_invoke_few_args("getPrefixedURL", 0x087B6C631A0C440BLL, 0)));
        goto break61;
      }
    case 18:
      {
        (v_value = LINE(2397,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(m_mTitle.o_invoke_few_args("getSubpageText", 0x01F4FD5FE8105897LL, 0))).create()), 0x00000000B3382909LL)));
        goto break61;
      }
    case 19:
      {
        (v_value = LINE(2400,m_mTitle.o_invoke_few_args("getSubpageUrlForm", 0x491DC72710364585LL, 0)));
        goto break61;
      }
    case 20:
      {
        (v_value = LINE(2403,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(m_mTitle.o_invoke_few_args("getBaseText", 0x23DB8A9B7829E674LL, 0))).create()), 0x00000000B3382909LL)));
        goto break61;
      }
    case 21:
      {
        (v_value = LINE(2406,invoke_failed("wfurlencode", Array(ArrayInit(1).set(0, ref((assignCallTemp(eo_2, m_mTitle.o_invoke_few_args("getBaseText", 0x23DB8A9B7829E674LL, 0)),x_str_replace(" ", "_", eo_2)))).create()), 0x00000000BFE709E0LL)));
        goto break61;
      }
    case 22:
      {
        if (toBoolean(LINE(2409,m_mTitle.o_invoke_few_args("canTalk", 0x6F6C6847DA1689A2LL, 0)))) {
          (v_talkPage = LINE(2410,m_mTitle.o_invoke_few_args("getTalkPage", 0x58258DF57A9BDD12LL, 0)));
          (v_value = LINE(2411,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(v_talkPage.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0))).create()), 0x00000000B3382909LL)));
        }
        else {
          (v_value = "");
        }
        goto break61;
      }
    case 23:
      {
        if (toBoolean(LINE(2417,m_mTitle.o_invoke_few_args("canTalk", 0x6F6C6847DA1689A2LL, 0)))) {
          (v_talkPage = LINE(2418,m_mTitle.o_invoke_few_args("getTalkPage", 0x58258DF57A9BDD12LL, 0)));
          (v_value = LINE(2419,v_talkPage.o_invoke_few_args("getPrefixedUrl", 0x087B6C631A0C440BLL, 0)));
        }
        else {
          (v_value = "");
        }
        goto break61;
      }
    case 24:
      {
        (v_subjPage = LINE(2425,m_mTitle.o_invoke_few_args("getSubjectPage", 0x366024925AE201F8LL, 0)));
        (v_value = LINE(2426,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(v_subjPage.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0))).create()), 0x00000000B3382909LL)));
        goto break61;
      }
    case 25:
      {
        (v_subjPage = LINE(2429,m_mTitle.o_invoke_few_args("getSubjectPage", 0x366024925AE201F8LL, 0)));
        (v_value = LINE(2430,v_subjPage.o_invoke_few_args("getPrefixedUrl", 0x087B6C631A0C440BLL, 0)));
        goto break61;
      }
    case 26:
      {
        LINE(2435,m_mOutput.o_invoke_few_args("setFlag", 0x17EB983A0D55AFA3LL, 1, "vary-revision"));
        LINE(2436,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser::getVariableValue: {{REVISIONID}} used, setting vary-revision...\n").create()), 0x00000000E441E905LL));
        (v_value = m_mRevisionId);
        goto break61;
      }
    case 27:
      {
        LINE(2442,m_mOutput.o_invoke_few_args("setFlag", 0x17EB983A0D55AFA3LL, 1, "vary-revision"));
        LINE(2443,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser::getVariableValue: {{REVISIONDAY}} used, setting vary-revision...\n").create()), 0x00000000E441E905LL));
        (v_value = LINE(2444,x_intval((assignCallTemp(eo_0, toString(t_getrevisiontimestamp())),x_substr(eo_0, toInt32(6LL), toInt32(2LL))))));
        goto break61;
      }
    case 28:
      {
        LINE(2449,m_mOutput.o_invoke_few_args("setFlag", 0x17EB983A0D55AFA3LL, 1, "vary-revision"));
        LINE(2450,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser::getVariableValue: {{REVISIONDAY2}} used, setting vary-revision...\n").create()), 0x00000000E441E905LL));
        (v_value = LINE(2451,(assignCallTemp(eo_0, toString(t_getrevisiontimestamp())),x_substr(eo_0, toInt32(6LL), toInt32(2LL)))));
        goto break61;
      }
    case 29:
      {
        LINE(2456,m_mOutput.o_invoke_few_args("setFlag", 0x17EB983A0D55AFA3LL, 1, "vary-revision"));
        LINE(2457,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser::getVariableValue: {{REVISIONMONTH}} used, setting vary-revision...\n").create()), 0x00000000E441E905LL));
        (v_value = LINE(2458,x_intval((assignCallTemp(eo_0, toString(t_getrevisiontimestamp())),x_substr(eo_0, toInt32(4LL), toInt32(2LL))))));
        goto break61;
      }
    case 30:
      {
        LINE(2463,m_mOutput.o_invoke_few_args("setFlag", 0x17EB983A0D55AFA3LL, 1, "vary-revision"));
        LINE(2464,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser::getVariableValue: {{REVISIONYEAR}} used, setting vary-revision...\n").create()), 0x00000000E441E905LL));
        (v_value = LINE(2465,(assignCallTemp(eo_0, toString(t_getrevisiontimestamp())),x_substr(eo_0, toInt32(0LL), toInt32(4LL)))));
        goto break61;
      }
    case 31:
      {
        LINE(2470,m_mOutput.o_invoke_few_args("setFlag", 0x17EB983A0D55AFA3LL, 1, "vary-revision"));
        LINE(2471,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser::getVariableValue: {{REVISIONTIMESTAMP}} used, setting vary-revision...\n").create()), 0x00000000E441E905LL));
        (v_value = LINE(2472,t_getrevisiontimestamp()));
        goto break61;
      }
    case 32:
      {
        LINE(2477,m_mOutput.o_invoke_few_args("setFlag", 0x17EB983A0D55AFA3LL, 1, "vary-revision"));
        LINE(2478,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser::getVariableValue: {{REVISIONUSER}} used, setting vary-revision...\n").create()), 0x00000000E441E905LL));
        (v_value = LINE(2479,t_getrevisionuser()));
        goto break61;
      }
    case 33:
      {
        (v_value = LINE(2482,(assignCallTemp(eo_2, gv_wgContLang.o_invoke_few_args("getNsText", 0x3A9D9935C2A87562LL, 1, m_mTitle.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0))),x_str_replace("_", " ", eo_2))));
        goto break61;
      }
    case 34:
      {
        (v_value = LINE(2485,invoke_failed("wfurlencode", Array(ArrayInit(1).set(0, ref(gv_wgContLang.o_invoke_few_args("getNsText", 0x3A9D9935C2A87562LL, 1, m_mTitle.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)))).create()), 0x00000000BFE709E0LL)));
        goto break61;
      }
    case 35:
      {
        (v_value = toBoolean(LINE(2488,m_mTitle.o_invoke_few_args("canTalk", 0x6F6C6847DA1689A2LL, 0))) ? ((Variant)((assignCallTemp(eo_2, m_mTitle.o_invoke_few_args("getTalkNsText", 0x51ABF4A6AFECC524LL, 0)),x_str_replace("_", " ", eo_2)))) : ((Variant)("")));
        goto break61;
      }
    case 36:
      {
        (v_value = toBoolean(LINE(2491,m_mTitle.o_invoke_few_args("canTalk", 0x6F6C6847DA1689A2LL, 0))) ? ((Variant)(invoke_failed("wfurlencode", Array(ArrayInit(1).set(0, ref(m_mTitle.o_invoke_few_args("getTalkNsText", 0x51ABF4A6AFECC524LL, 0))).create()), 0x00000000BFE709E0LL))) : ((Variant)("")));
        goto break61;
      }
    case 37:
      {
        (v_value = LINE(2494,m_mTitle.o_invoke_few_args("getSubjectNsText", 0x601B23155AB888BDLL, 0)));
        goto break61;
      }
    case 38:
      {
        (v_value = (LINE(2497,invoke_failed("wfurlencode", Array(ArrayInit(1).set(0, ref(m_mTitle.o_invoke_few_args("getSubjectNsText", 0x601B23155AB888BDLL, 0))).create()), 0x00000000BFE709E0LL))));
        goto break61;
      }
    case 39:
      {
        (v_value = LINE(2500,gv_wgContLang.o_invoke_few_args("getWeekdayName", 0x0DEF9E6D61D4F31FLL, 1, (Variant)(x_gmdate("w", toInt64(v_ts))) + 1LL)));
        goto break61;
      }
    case 40:
      {
        (v_value = (assignCallTemp(eo_0, ref(LINE(2503,x_gmdate("Y", toInt64(v_ts))))),gv_wgContLang.o_invoke("formatNum", Array(ArrayInit(2).set(0, eo_0).set(1, true).create()), 0x0FEB8F5DDB8EBB5ALL)));
        goto break61;
      }
    case 41:
      {
        (v_value = (assignCallTemp(eo_0, ref(LINE(2506,invoke_failed("wftimestamp", Array(ArrayInit(2).set(0, k_TS_MW).set(1, ref(v_ts)).create()), 0x00000000AADC1C20LL)))),gv_wgContLang.o_invoke("time", Array(ArrayInit(3).set(0, eo_0).set(1, false).set(2, false).create()), 0x74ABB4A1E10BBBF7LL)));
        goto break61;
      }
    case 42:
      {
        (v_value = (assignCallTemp(eo_0, ref(LINE(2509,x_gmdate("H", toInt64(v_ts))))),gv_wgContLang.o_invoke("formatNum", Array(ArrayInit(2).set(0, eo_0).set(1, true).create()), 0x0FEB8F5DDB8EBB5ALL)));
        goto break61;
      }
    case 43:
      {
        (v_value = LINE(2514,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, toInt64(x_gmdate("W", toInt64(v_ts))))));
        goto break61;
      }
    case 44:
      {
        (v_value = LINE(2517,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, x_gmdate("w", toInt64(v_ts)))));
        goto break61;
      }
    case 45:
      {
        (v_value = LINE(2520,gv_wgContLang.o_invoke_few_args("getWeekdayName", 0x0DEF9E6D61D4F31FLL, 1, v_localDayOfWeek + 1LL)));
        goto break61;
      }
    case 46:
      {
        (v_value = LINE(2523,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 2, v_localYear, true)));
        goto break61;
      }
    case 47:
      {
        (v_value = LINE(2526,gv_wgContLang.o_invoke_few_args("time", 0x74ABB4A1E10BBBF7LL, 3, v_localTimestamp, false, false)));
        goto break61;
      }
    case 48:
      {
        (v_value = LINE(2529,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 2, v_localHour, true)));
        goto break61;
      }
    case 49:
      {
        (v_value = LINE(2534,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, toInt64(v_localWeek))));
        goto break61;
      }
    case 50:
      {
        (v_value = LINE(2537,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, v_localDayOfWeek)));
        goto break61;
      }
    case 51:
      {
        (v_value = LINE(2540,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, throw_fatal("unknown class sitestats", ((void*)NULL)))));
        goto break61;
      }
    case 52:
      {
        (v_value = LINE(2543,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, throw_fatal("unknown class sitestats", ((void*)NULL)))));
        goto break61;
      }
    case 53:
      {
        (v_value = LINE(2546,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, throw_fatal("unknown class sitestats", ((void*)NULL)))));
        goto break61;
      }
    case 54:
      {
        (v_value = LINE(2549,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, throw_fatal("unknown class sitestats", ((void*)NULL)))));
        goto break61;
      }
    case 55:
      {
        (v_value = LINE(2552,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, throw_fatal("unknown class sitestats", ((void*)NULL)))));
        goto break61;
      }
    case 56:
      {
        (v_value = LINE(2555,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, throw_fatal("unknown class sitestats", ((void*)NULL)))));
        goto break61;
      }
    case 57:
      {
        (v_value = LINE(2558,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, throw_fatal("unknown class sitestats", ((void*)NULL)))));
        goto break61;
      }
    case 58:
      {
        (v_value = LINE(2561,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, throw_fatal("unknown class sitestats", ((void*)NULL)))));
        goto break61;
      }
    case 59:
      {
        (v_value = LINE(2564,invoke_failed("wftimestamp", Array(ArrayInit(2).set(0, k_TS_MW).set(1, ref(v_ts)).create()), 0x00000000AADC1C20LL)));
        goto break61;
      }
    case 60:
      {
        (v_value = v_localTimestamp);
        goto break61;
      }
    case 61:
      {
        (v_value = LINE(2570,throw_fatal("unknown class specialversion", ((void*)NULL))));
        goto break61;
      }
    case 62:
      {
        return gv_wgSitename;
      }
    case 63:
      {
        return gv_wgServer;
      }
    case 64:
      {
        return gv_wgServerName;
      }
    case 65:
      {
        return gv_wgScriptPath;
      }
    case 66:
      {
        return LINE(2581,gv_wgContLang.o_invoke_few_args("getDirMark", 0x5BF4F4487797223BLL, 0));
      }
    case 67:
      {
        v_wgContLanguageCode = ref(g->GV(wgContLanguageCode));
        return v_wgContLanguageCode;
      }
    case 68:
      {
        setNull(v_ret);
        if (toBoolean(LINE(2587,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserGetVariableValueSwitch").set(1, Array(ArrayInit(5).setRef(0, ref(this)).setRef(1, ref(lval(m_mVarCache))).setRef(2, ref(v_index)).setRef(3, ref(v_ret)).setRef(4, ref(v_frame)).create())).create()), 0x00000000787A96B2LL)))) return v_ret;
        else return null;
      }
    }
    break61:;
  }
  if (toBoolean(v_index)) m_mVarCache.set(v_index, (v_value));
  return v_value;
} /* function */
/* SRC: Parser.php line 2604 */
void c_parser::t_initialisevariables() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::initialiseVariables);
  Variant v_variableIDs;

  LINE(2605,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::initialiseVariables").create()), 0x0000000075359BAFLL));
  (v_variableIDs = LINE(2606,throw_fatal("unknown class magicword", ((void*)NULL))));
  (m_mVariables = LINE(2608,create_object("magicwordarray", Array(ArrayInit(1).set(0, v_variableIDs).create()))));
  LINE(2609,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::initialiseVariables").create()), 0x00000000B599F276LL));
} /* function */
/* SRC: Parser.php line 2634 */
Variant c_parser::t_preprocesstodom(Variant v_text, Variant v_flags //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::preprocessToDom);
  Variant eo_0;
  Variant v_dom;

  (v_dom = (assignCallTemp(eo_0, toObject(LINE(2635,t_getpreprocessor()))),eo_0.o_invoke_few_args("preprocessToObj", 0x0BA55A9913645813LL, 2, v_text, v_flags)));
  return v_dom;
} /* function */
/* SRC: Parser.php line 2642 */
Array c_parser::ti_splitwhitespace(const char* cls, CVarRef v_s) {
  STATIC_METHOD_INJECTION(Parser, Parser::splitWhitespace);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_ltrimmed;
  Variant v_w1;
  String v_trimmed;
  Numeric v_diff = 0;
  Variant v_w2;

  (v_ltrimmed = LINE(2643,x_ltrim(toString(v_s))));
  (v_w1 = LINE(2644,(assignCallTemp(eo_0, toString(v_s)),assignCallTemp(eo_2, toInt32(minus_rev(x_strlen(v_ltrimmed), x_strlen(toString(v_s))))),x_substr(eo_0, toInt32(0LL), eo_2))));
  (v_trimmed = LINE(2645,x_rtrim(v_ltrimmed)));
  (v_diff = minus_rev(LINE(2646,x_strlen(v_trimmed)), x_strlen(v_ltrimmed)));
  if (more(v_diff, 0LL)) {
    (v_w2 = LINE(2648,x_substr(v_ltrimmed, toInt32(negate(v_diff)))));
  }
  else {
    (v_w2 = "");
  }
  return Array(ArrayInit(3).set(0, v_w1).set(1, v_trimmed).set(2, v_w2).create());
} /* function */
/* SRC: Parser.php line 2672 */
Variant c_parser::t_replacevariables(Variant v_text, Variant v_frame //  = false
, CVarRef v_argsOnly //  = false
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::replaceVariables);
  Variant eo_0;
  Variant eo_1;
  Variant v_dom;
  Variant v_flags;

  if (less(LINE(2674,x_strlen(toString(v_text))), 1LL) || more_rev(m_mOptions.o_invoke_few_args("getMaxIncludeSize", 0x5356D47AAA28FDF5LL, 0), x_strlen(toString(v_text)))) {
    return v_text;
  }
  LINE(2677,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::replaceVariables").create()), 0x0000000075359BAFLL));
  if (same(v_frame, false)) {
    (v_frame = (assignCallTemp(eo_0, toObject(LINE(2680,t_getpreprocessor()))),eo_0.o_invoke_few_args("newFrame", 0x787B6141D7721675LL, 0)));
  }
  else if (!((instanceOf(v_frame, "PPFrame")))) {
    LINE(2682,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser::replaceVariables called using plain parameters instead of a PPFrame instance. Creating custom frame.\n").create()), 0x00000000E441E905LL));
    (v_frame = (assignCallTemp(eo_1, toObject(LINE(2683,t_getpreprocessor()))),eo_1.o_invoke_few_args("newCustomFrame", 0x5A11FE5B629AF7A0LL, 1, v_frame)));
  }
  (v_dom = LINE(2686,t_preprocesstodom(v_text)));
  (v_flags = toBoolean(v_argsOnly) ? ((2LL /* ppframe::NO_TEMPLATES */)) : ((0LL)));
  (v_text = LINE(2688,v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_dom, v_flags)));
  LINE(2690,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::replaceVariables").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 2695 */
Variant c_parser::ti_createassocargs(const char* cls, CArrRef v_args) {
  STATIC_METHOD_INJECTION(Parser, Parser::createAssocArgs);
  Variant v_assocArgs;
  int64 v_index = 0;
  Variant v_arg;
  Variant v_eqpos;
  String v_name;
  String v_value;

  (v_assocArgs = ScalarArrays::sa_[0]);
  (v_index = 1LL);
  {
    LOOP_COUNTER(64);
    for (ArrayIter iter66 = v_args.begin("parser"); !iter66.end(); ++iter66) {
      LOOP_COUNTER_CHECK(64);
      v_arg = iter66.second();
      {
        (v_eqpos = LINE(2699,x_strpos(toString(v_arg), "=")));
        if (same(v_eqpos, false)) {
          v_assocArgs.set(v_index++, (v_arg));
        }
        else {
          (v_name = LINE(2703,x_trim(toString(x_substr(toString(v_arg), toInt32(0LL), toInt32(v_eqpos))))));
          (v_value = LINE(2704,x_trim(toString(x_substr(toString(v_arg), toInt32(v_eqpos + 1LL))))));
          if (same(v_value, false)) {
            (v_value = "");
          }
          if (!same(v_name, false)) {
            v_assocArgs.set(v_name, (v_value));
          }
        }
      }
    }
  }
  return v_assocArgs;
} /* function */
/* SRC: Parser.php line 2728 */
void c_parser::t_limitationwarn(CStrRef v_limitationType, Variant v_current //  = null
, Variant v_max //  = null
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::limitationWarn);
  Variant v_warning;

  (v_warning = LINE(2730,invoke_failed("wfmsgext", Array(ArrayInit(4).set(0, v_limitationType + toString("-warning")).set(1, Array(ArrayInit(2).set(0, "parsemag").set(1, "escape").create())).set(2, ref(v_current)).set(3, ref(v_max)).create()), 0x000000001BB87306LL)));
  LINE(2731,m_mOutput.o_invoke_few_args("addWarning", 0x1E1ACEDC035034C5LL, 1, v_warning));
  LINE(2732,t_addtrackingcategory(v_limitationType + toString("-category")));
} /* function */
/* SRC: Parser.php line 2747 */
Variant c_parser::t_bracesubstitution(Variant v_piece, Variant v_frame) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::braceSubstitution);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_wgContLang;
  Variant &gv_wgNonincludableNamespaces __attribute__((__unused__)) = g->GV(wgNonincludableNamespaces);
  Variant v_wgNonincludableNamespaces;
  Variant v_found;
  Variant v_nowiki;
  Variant v_isHTML;
  Variant v_forceRawInterwiki;
  Variant v_isChildObj;
  Variant v_isLocalObj;
  Variant v_title;
  Variant v_titleWithSpaces;
  Variant v_part1;
  Variant v_titleText;
  Variant v_originalTitle;
  Variant v_args;
  Variant v_mwSubst;
  Variant v_text;
  Variant v_id;
  Variant v_mwMsgnw;
  Variant v_mwMsg;
  Variant v_mwRaw;
  Variant v_colonPos;
  Variant v_function;
  Variant v_callback;
  Variant v_flags;
  Variant v_initialArgs;
  Variant v_funcArgs;
  Variant v_allArgs;
  Variant v_i;
  Variant v_result;
  Variant v_noparse;
  Variant v_preprocessFlags;
  Variant v_ns;
  Variant v_subpage;
  Variant v_limit;
  Variant v_newFrame;
  Variant v_ret;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_piece; Variant &v_frame; Variant &v_wgContLang; Variant &v_wgNonincludableNamespaces; Variant &v_found; Variant &v_nowiki; Variant &v_isHTML; Variant &v_forceRawInterwiki; Variant &v_isChildObj; Variant &v_isLocalObj; Variant &v_title; Variant &v_titleWithSpaces; Variant &v_part1; Variant &v_titleText; Variant &v_originalTitle; Variant &v_args; Variant &v_mwSubst; Variant &v_text; Variant &v_id; Variant &v_mwMsgnw; Variant &v_mwMsg; Variant &v_mwRaw; Variant &v_colonPos; Variant &v_function; Variant &v_callback; Variant &v_flags; Variant &v_initialArgs; Variant &v_funcArgs; Variant &v_allArgs; Variant &v_i; Variant &v_result; Variant &v_noparse; Variant &v_preprocessFlags; Variant &v_ns; Variant &v_subpage; Variant &v_limit; Variant &v_newFrame; Variant &v_ret;
    VariableTable(Variant &r_piece, Variant &r_frame, Variant &r_wgContLang, Variant &r_wgNonincludableNamespaces, Variant &r_found, Variant &r_nowiki, Variant &r_isHTML, Variant &r_forceRawInterwiki, Variant &r_isChildObj, Variant &r_isLocalObj, Variant &r_title, Variant &r_titleWithSpaces, Variant &r_part1, Variant &r_titleText, Variant &r_originalTitle, Variant &r_args, Variant &r_mwSubst, Variant &r_text, Variant &r_id, Variant &r_mwMsgnw, Variant &r_mwMsg, Variant &r_mwRaw, Variant &r_colonPos, Variant &r_function, Variant &r_callback, Variant &r_flags, Variant &r_initialArgs, Variant &r_funcArgs, Variant &r_allArgs, Variant &r_i, Variant &r_result, Variant &r_noparse, Variant &r_preprocessFlags, Variant &r_ns, Variant &r_subpage, Variant &r_limit, Variant &r_newFrame, Variant &r_ret) : v_piece(r_piece), v_frame(r_frame), v_wgContLang(r_wgContLang), v_wgNonincludableNamespaces(r_wgNonincludableNamespaces), v_found(r_found), v_nowiki(r_nowiki), v_isHTML(r_isHTML), v_forceRawInterwiki(r_forceRawInterwiki), v_isChildObj(r_isChildObj), v_isLocalObj(r_isLocalObj), v_title(r_title), v_titleWithSpaces(r_titleWithSpaces), v_part1(r_part1), v_titleText(r_titleText), v_originalTitle(r_originalTitle), v_args(r_args), v_mwSubst(r_mwSubst), v_text(r_text), v_id(r_id), v_mwMsgnw(r_mwMsgnw), v_mwMsg(r_mwMsg), v_mwRaw(r_mwRaw), v_colonPos(r_colonPos), v_function(r_function), v_callback(r_callback), v_flags(r_flags), v_initialArgs(r_initialArgs), v_funcArgs(r_funcArgs), v_allArgs(r_allArgs), v_i(r_i), v_result(r_result), v_noparse(r_noparse), v_preprocessFlags(r_preprocessFlags), v_ns(r_ns), v_subpage(r_subpage), v_limit(r_limit), v_newFrame(r_newFrame), v_ret(r_ret) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 127) {
        case 2:
          HASH_RETURN(0x692E742321F80F02LL, v_mwSubst,
                      mwSubst);
          break;
        case 4:
          HASH_RETURN(0x6D135A1F775A3C04LL, v_titleWithSpaces,
                      titleWithSpaces);
          break;
        case 13:
          HASH_RETURN(0x6AFDA85728FAE70DLL, v_flags,
                      flags);
          break;
        case 22:
          HASH_RETURN(0x49465865E679EB16LL, v_limit,
                      limit);
          break;
        case 24:
          HASH_RETURN(0x0EB22EDA95766E98LL, v_i,
                      i);
          break;
        case 30:
          HASH_RETURN(0x4AF7CD17F976719ELL, v_args,
                      args);
          break;
        case 31:
          HASH_RETURN(0x2D577BEBE8CB9D1FLL, v_frame,
                      frame);
          HASH_RETURN(0x5ABEE6C41B223D9FLL, v_isLocalObj,
                      isLocalObj);
          break;
        case 37:
          HASH_RETURN(0x657E33B2AED298A5LL, v_newFrame,
                      newFrame);
          break;
        case 49:
          HASH_RETURN(0x736D912A52403931LL, v_function,
                      function);
          HASH_RETURN(0x334D179C1FC4A0B1LL, v_noparse,
                      noparse);
          break;
        case 53:
          HASH_RETURN(0x35F1C44CD9562BB5LL, v_mwMsg,
                      mwMsg);
          break;
        case 55:
          HASH_RETURN(0x665662C0C91B8BB7LL, v_part1,
                      part1);
          break;
        case 59:
          HASH_RETURN(0x540FC99D08D7253BLL, v_forceRawInterwiki,
                      forceRawInterwiki);
          break;
        case 67:
          HASH_RETURN(0x2A28A0084DD3A743LL, v_text,
                      text);
          HASH_RETURN(0x59478C0CAAAA0643LL, v_allArgs,
                      allArgs);
          break;
        case 70:
          HASH_RETURN(0x1E7DD8DF6E98F4C6LL, v_callback,
                      callback);
          break;
        case 72:
          HASH_RETURN(0x599E3F67E2599248LL, v_result,
                      result);
          break;
        case 74:
          HASH_RETURN(0x323EAC15D211984ALL, v_wgContLang,
                      wgContLang);
          HASH_RETURN(0x753C6234141C8C4ALL, v_colonPos,
                      colonPos);
          break;
        case 78:
          HASH_RETURN(0x41CC31743A0270CELL, v_found,
                      found);
          break;
        case 79:
          HASH_RETURN(0x4EF072AC8484C74FLL, v_preprocessFlags,
                      preprocessFlags);
          break;
        case 80:
          HASH_RETURN(0x7635B625A9ED35D0LL, v_subpage,
                      subpage);
          break;
        case 82:
          HASH_RETURN(0x0B33F381563FAED2LL, v_isHTML,
                      isHTML);
          break;
        case 85:
          HASH_RETURN(0x1341D4F88CD79F55LL, v_originalTitle,
                      originalTitle);
          break;
        case 91:
          HASH_RETURN(0x7132B63767EAFEDBLL, v_piece,
                      piece);
          break;
        case 93:
          HASH_RETURN(0x78258C7EF69CF55DLL, v_nowiki,
                      nowiki);
          break;
        case 94:
          HASH_RETURN(0x402070269FCD7CDELL, v_isChildObj,
                      isChildObj);
          break;
        case 96:
          HASH_RETURN(0x740962C1F3DF2EE0LL, v_initialArgs,
                      initialArgs);
          break;
        case 98:
          HASH_RETURN(0x028B9FE0C4522BE2LL, v_id,
                      id);
          break;
        case 108:
          HASH_RETURN(0x1DA25769DEC7D2ECLL, v_ns,
                      ns);
          break;
        case 115:
          HASH_RETURN(0x1C01B949AD4C00F3LL, v_ret,
                      ret);
          break;
        case 117:
          HASH_RETURN(0x43FBB2C4FE020BF5LL, v_wgNonincludableNamespaces,
                      wgNonincludableNamespaces);
          break;
        case 118:
          HASH_RETURN(0x176B222D8CCA9476LL, v_mwMsgnw,
                      mwMsgnw);
          HASH_RETURN(0x37B21F02C50936F6LL, v_funcArgs,
                      funcArgs);
          break;
        case 120:
          HASH_RETURN(0x461172FD01925F78LL, v_titleText,
                      titleText);
          break;
        case 122:
          HASH_RETURN(0x63C1D632E67611FALL, v_mwRaw,
                      mwRaw);
          break;
        case 126:
          HASH_RETURN(0x66AD900A2301E2FELL, v_title,
                      title);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
    virtual bool exists(const char *s, int64 hash /* = -1 */) const {
      if (hash < 0) hash = hash_string(s);
      switch (hash & 127) {
        case 2:
          HASH_INITIALIZED(0x692E742321F80F02LL, v_mwSubst,
                           mwSubst);
          break;
        case 4:
          HASH_INITIALIZED(0x6D135A1F775A3C04LL, v_titleWithSpaces,
                           titleWithSpaces);
          break;
        case 13:
          HASH_INITIALIZED(0x6AFDA85728FAE70DLL, v_flags,
                           flags);
          break;
        case 22:
          HASH_INITIALIZED(0x49465865E679EB16LL, v_limit,
                           limit);
          break;
        case 24:
          HASH_INITIALIZED(0x0EB22EDA95766E98LL, v_i,
                           i);
          break;
        case 30:
          HASH_INITIALIZED(0x4AF7CD17F976719ELL, v_args,
                           args);
          break;
        case 31:
          HASH_INITIALIZED(0x2D577BEBE8CB9D1FLL, v_frame,
                           frame);
          HASH_INITIALIZED(0x5ABEE6C41B223D9FLL, v_isLocalObj,
                           isLocalObj);
          break;
        case 37:
          HASH_INITIALIZED(0x657E33B2AED298A5LL, v_newFrame,
                           newFrame);
          break;
        case 49:
          HASH_INITIALIZED(0x736D912A52403931LL, v_function,
                           function);
          HASH_INITIALIZED(0x334D179C1FC4A0B1LL, v_noparse,
                           noparse);
          break;
        case 53:
          HASH_INITIALIZED(0x35F1C44CD9562BB5LL, v_mwMsg,
                           mwMsg);
          break;
        case 55:
          HASH_INITIALIZED(0x665662C0C91B8BB7LL, v_part1,
                           part1);
          break;
        case 59:
          HASH_INITIALIZED(0x540FC99D08D7253BLL, v_forceRawInterwiki,
                           forceRawInterwiki);
          break;
        case 67:
          HASH_INITIALIZED(0x2A28A0084DD3A743LL, v_text,
                           text);
          HASH_INITIALIZED(0x59478C0CAAAA0643LL, v_allArgs,
                           allArgs);
          break;
        case 70:
          HASH_INITIALIZED(0x1E7DD8DF6E98F4C6LL, v_callback,
                           callback);
          break;
        case 72:
          HASH_INITIALIZED(0x599E3F67E2599248LL, v_result,
                           result);
          break;
        case 74:
          HASH_INITIALIZED(0x323EAC15D211984ALL, v_wgContLang,
                           wgContLang);
          HASH_INITIALIZED(0x753C6234141C8C4ALL, v_colonPos,
                           colonPos);
          break;
        case 78:
          HASH_INITIALIZED(0x41CC31743A0270CELL, v_found,
                           found);
          break;
        case 79:
          HASH_INITIALIZED(0x4EF072AC8484C74FLL, v_preprocessFlags,
                           preprocessFlags);
          break;
        case 80:
          HASH_INITIALIZED(0x7635B625A9ED35D0LL, v_subpage,
                           subpage);
          break;
        case 82:
          HASH_INITIALIZED(0x0B33F381563FAED2LL, v_isHTML,
                           isHTML);
          break;
        case 85:
          HASH_INITIALIZED(0x1341D4F88CD79F55LL, v_originalTitle,
                           originalTitle);
          break;
        case 91:
          HASH_INITIALIZED(0x7132B63767EAFEDBLL, v_piece,
                           piece);
          break;
        case 93:
          HASH_INITIALIZED(0x78258C7EF69CF55DLL, v_nowiki,
                           nowiki);
          break;
        case 94:
          HASH_INITIALIZED(0x402070269FCD7CDELL, v_isChildObj,
                           isChildObj);
          break;
        case 96:
          HASH_INITIALIZED(0x740962C1F3DF2EE0LL, v_initialArgs,
                           initialArgs);
          break;
        case 98:
          HASH_INITIALIZED(0x028B9FE0C4522BE2LL, v_id,
                           id);
          break;
        case 108:
          HASH_INITIALIZED(0x1DA25769DEC7D2ECLL, v_ns,
                           ns);
          break;
        case 115:
          HASH_INITIALIZED(0x1C01B949AD4C00F3LL, v_ret,
                           ret);
          break;
        case 117:
          HASH_INITIALIZED(0x43FBB2C4FE020BF5LL, v_wgNonincludableNamespaces,
                           wgNonincludableNamespaces);
          break;
        case 118:
          HASH_INITIALIZED(0x176B222D8CCA9476LL, v_mwMsgnw,
                           mwMsgnw);
          HASH_INITIALIZED(0x37B21F02C50936F6LL, v_funcArgs,
                           funcArgs);
          break;
        case 120:
          HASH_INITIALIZED(0x461172FD01925F78LL, v_titleText,
                           titleText);
          break;
        case 122:
          HASH_INITIALIZED(0x63C1D632E67611FALL, v_mwRaw,
                           mwRaw);
          break;
        case 126:
          HASH_INITIALIZED(0x66AD900A2301E2FELL, v_title,
                           title);
          break;
        default:
          break;
      }
      return LVariableTable::exists(s, hash);
    }
  } variableTable(v_piece, v_frame, v_wgContLang, v_wgNonincludableNamespaces, v_found, v_nowiki, v_isHTML, v_forceRawInterwiki, v_isChildObj, v_isLocalObj, v_title, v_titleWithSpaces, v_part1, v_titleText, v_originalTitle, v_args, v_mwSubst, v_text, v_id, v_mwMsgnw, v_mwMsg, v_mwRaw, v_colonPos, v_function, v_callback, v_flags, v_initialArgs, v_funcArgs, v_allArgs, v_i, v_result, v_noparse, v_preprocessFlags, v_ns, v_subpage, v_limit, v_newFrame, v_ret);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  {
    v_wgContLang = ref(g->GV(wgContLang));
    v_wgNonincludableNamespaces = ref(g->GV(wgNonincludableNamespaces));
  }
  LINE(2749,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::braceSubstitution").create()), 0x0000000075359BAFLL));
  LINE(2750,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::braceSubstitution-setup").create()), 0x0000000075359BAFLL));
  (v_found = false);
  (v_nowiki = false);
  (v_isHTML = false);
  (v_forceRawInterwiki = false);
  (v_isChildObj = false);
  (v_isLocalObj = false);
  setNull(v_title);
  (v_titleWithSpaces = LINE(2765,v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_piece.refvalAt("title", 0x66AD900A2301E2FELL))));
  (v_part1 = LINE(2766,x_trim(toString(v_titleWithSpaces))));
  (v_titleText = false);
  (v_originalTitle = v_part1);
  (v_args = (equal(null, v_piece.rvalAt("parts", 0x2708FDA74562AD8DLL))) ? ((Variant)(ScalarArrays::sa_[0])) : ((Variant)(v_piece.rvalAt("parts", 0x2708FDA74562AD8DLL))));
  LINE(2774,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::braceSubstitution-setup").create()), 0x00000000B599F276LL));
  LINE(2777,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::braceSubstitution-modifiers").create()), 0x0000000075359BAFLL));
  if (!(toBoolean(v_found))) {
    (v_mwSubst = LINE(2779,throw_fatal("unknown class magicword", ((void*)NULL))));
    if (logical_xor(toBoolean(LINE(2780,v_mwSubst.o_invoke_few_args("matchStartAndRemove", 0x2B81946BBA5E8685LL, 1, v_part1))), toBoolean(m_ot.rvalAt("wiki", 0x24648D2BCD794D21LL)))) {
      (v_text = LINE(2785,v_frame.o_invoke_few_args("virtualBracketedImplode", 0x409618A98590618BLL, 5, "{{", "|", "}}", v_titleWithSpaces, v_args)));
      (v_isLocalObj = true);
      (v_found = true);
    }
  }
  if (!(toBoolean(v_found)) && equal(LINE(2792,v_args.o_invoke_few_args("getLength", 0x41EB078EF92C44D4LL, 0)), 0LL)) {
    (v_id = LINE(2793,m_mVariables.o_invoke_few_args("matchStartToEnd", 0x4F70AC0C69CFEB19LL, 1, v_part1)));
    if (!same(v_id, false)) {
      (v_text = LINE(2795,t_getvariablevalue(v_id, v_frame)));
      if (more(LINE(2796,throw_fatal("unknown class magicword", ((void*)NULL))), -1LL)) (m_mOutput.o_lval("mContainsOldMagic", 0x517EDA759E438B97LL) = true);
      (v_found = true);
    }
  }
  if (!(toBoolean(v_found))) {
    (v_mwMsgnw = LINE(2805,throw_fatal("unknown class magicword", ((void*)NULL))));
    if (toBoolean(LINE(2806,v_mwMsgnw.o_invoke_few_args("matchStartAndRemove", 0x2B81946BBA5E8685LL, 1, v_part1)))) {
      (v_nowiki = true);
    }
    else {
      (v_mwMsg = LINE(2810,throw_fatal("unknown class magicword", ((void*)NULL))));
      LINE(2811,v_mwMsg.o_invoke_few_args("matchStartAndRemove", 0x2B81946BBA5E8685LL, 1, v_part1));
    }
    (v_mwRaw = LINE(2815,throw_fatal("unknown class magicword", ((void*)NULL))));
    if (toBoolean(LINE(2816,v_mwRaw.o_invoke_few_args("matchStartAndRemove", 0x2B81946BBA5E8685LL, 1, v_part1)))) {
      (v_forceRawInterwiki = true);
    }
  }
  LINE(2820,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::braceSubstitution-modifiers").create()), 0x00000000B599F276LL));
  if (!(toBoolean(v_found))) {
    LINE(2824,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::braceSubstitution-pfunc").create()), 0x0000000075359BAFLL));
    (v_colonPos = LINE(2826,x_strpos(toString(v_part1), ":")));
    if (!same(v_colonPos, false)) {
      (v_function = LINE(2829,x_substr(toString(v_part1), toInt32(0LL), toInt32(v_colonPos))));
      if (isset(m_mFunctionSynonyms.rvalAt(1LL, 0x5BCA7C69B794F8CELL), v_function)) {
        (v_function = m_mFunctionSynonyms.rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(v_function));
      }
      else {
        (v_function = LINE(2834,v_wgContLang.o_invoke_few_args("lc", 0x0191205BE4FF8F9CLL, 1, v_function)));
        if (isset(m_mFunctionSynonyms.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), v_function)) {
          (v_function = m_mFunctionSynonyms.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(v_function));
        }
        else {
          (v_function = false);
        }
      }
      if (toBoolean(v_function)) {
        df_lambda_9(m_mFunctionHooks.rvalAt(v_function), v_callback, v_flags);
        (v_initialArgs = Array(ArrayInit(1).setRef(0, ref(this)).create()));
        (v_funcArgs = Array(ArrayInit(1).set(0, LINE(2844,x_trim(toString(x_substr(toString(v_part1), toInt32(v_colonPos + 1LL)))))).create()));
        if (toBoolean(bitwise_and(v_flags, k_SFH_OBJECT_ARGS))) {
          (v_allArgs = v_initialArgs);
          v_allArgs.append((v_frame));
          {
            LOOP_COUNTER(67);
            for ((v_i = 0LL); less(v_i, LINE(2849,v_args.o_invoke_few_args("getLength", 0x41EB078EF92C44D4LL, 0))); v_i++) {
              LOOP_COUNTER_CHECK(67);
              {
                v_funcArgs.append((LINE(2850,v_args.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, v_i))));
              }
            }
          }
          v_allArgs.append((v_funcArgs));
        }
        else {
          {
            LOOP_COUNTER(68);
            for ((v_i = 0LL); less(v_i, LINE(2855,v_args.o_invoke_few_args("getLength", 0x41EB078EF92C44D4LL, 0))); v_i++) {
              LOOP_COUNTER_CHECK(68);
              {
                v_funcArgs.append((LINE(2856,x_trim(toString(v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_args.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, v_i)))))));
              }
            }
          }
          (v_allArgs = LINE(2858,x_array_merge(2, v_initialArgs, Array(ArrayInit(1).set(0, v_funcArgs).create()))));
        }
        if (!(LINE(2862,x_is_callable(v_callback)))) {
          LINE(2863,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::braceSubstitution-pfunc").create()), 0x00000000B599F276LL));
          LINE(2864,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::braceSubstitution").create()), 0x00000000B599F276LL));
          throw_exception(LINE(2865,create_object("mwexception", Array(ArrayInit(1).set(0, concat3("Tag hook for ", toString(v_function), " is not callable\n")).create()))));
        }
        (v_result = LINE(2867,x_call_user_func_array(v_callback, toArray(v_allArgs))));
        (v_found = true);
        (v_noparse = true);
        (v_preprocessFlags = 0LL);
        if (LINE(2872,x_is_array(v_result))) {
          if (isset(v_result, 0LL, 0x77CFA1EEF01BCA90LL)) {
            (v_text = v_result.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
            v_result.weakRemove(0LL, 0x77CFA1EEF01BCA90LL);
          }
          LINE(2880,extract(variables, toArray(v_result)));
        }
        else {
          (v_text = v_result);
        }
        if (!(toBoolean(v_noparse))) {
          (v_text = LINE(2885,t_preprocesstodom(v_text, v_preprocessFlags)));
          (v_isChildObj = true);
        }
      }
    }
    LINE(2890,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::braceSubstitution-pfunc").create()), 0x00000000B599F276LL));
  }
  if (!(toBoolean(v_found))) {
    (v_ns = k_NS_TEMPLATE);
    (v_subpage = "");
    (v_part1 = LINE(2899,t_maybedosubpagelink(v_part1, ref(v_subpage))));
    if (!same(v_subpage, "")) {
      (v_ns = LINE(2901,m_mTitle.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)));
    }
    (v_title = LINE(2903,throw_fatal("unknown class title", ((void*)NULL))));
    if (toBoolean(v_title)) {
      (v_titleText = LINE(2905,v_title.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0)));
      if (toBoolean(LINE(2907,v_wgContLang.o_invoke_few_args("hasVariants", 0x5A1FD2E20BD8DBBBLL, 0))) && equal(v_title.o_invoke_few_args("getArticleID", 0x094F8EE5A02782D6LL, 0), 0LL)) {
        LINE(2908,v_wgContLang.o_invoke_few_args("findVariantLink", 0x49C38CD942068A0ELL, 3, v_part1, v_title, true));
      }
      (v_limit = LINE(2911,m_mOptions.o_invoke_few_args("getMaxTemplateDepth", 0x4C54B0BC426A6EE3LL, 0)));
      if (not_less(v_frame.o_get("depth", 0x1AE700EECE6274C7LL), v_limit)) {
        (v_found = true);
        (v_text = LINE(2914,(assignCallTemp(eo_1, toString(invoke_failed("wfmsgforcontent", Array(ArrayInit(2).set(0, "parser-template-recursion-depth-warning").set(1, ref(v_limit)).create()), 0x0000000033607CCALL))),concat3("<span class=\"error\">", eo_1, "</span>"))));
      }
    }
  }
  if (!(toBoolean(v_found)) && toBoolean(v_title)) {
    LINE(2921,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::braceSubstitution-loadtpl").create()), 0x0000000075359BAFLL));
    if (!(toBoolean(LINE(2922,v_title.o_invoke_few_args("isExternal", 0x16F7C0178CF6E138LL, 0))))) {
      if (equal(LINE(2923,v_title.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)), k_NS_SPECIAL) && toBoolean(m_mOptions.o_invoke_few_args("getAllowSpecialInclusion", 0x6C9C5936F7FFEA1CLL, 0)) && toBoolean(m_ot.rvalAt("html", 0x5CA91C812230855DLL))) {
        (v_text = LINE(2924,throw_fatal("unknown class specialpage", ((void*)NULL))));
        if (LINE(2925,x_is_string(v_text))) {
          (v_found = true);
          (v_isHTML = true);
          LINE(2928,t_disablecache());
        }
      }
      else if (toBoolean(v_wgNonincludableNamespaces) && LINE(2930,(assignCallTemp(eo_0, v_title.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)),assignCallTemp(eo_1, v_wgNonincludableNamespaces),x_in_array(eo_0, eo_1)))) {
        (v_found = false);
        LINE(2932,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, concat("Parser::braceSubstitution: template inclusion denied for ", toString(v_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)))).create()), 0x00000000E441E905LL));
      }
      else {
        df_lambda_10(LINE(2934,t_gettemplatedom(v_title)), v_text, v_title);
        if (!same(v_text, false)) {
          (v_found = true);
          (v_isChildObj = true);
        }
      }
      if (!(toBoolean(v_found)) && (toBoolean(m_ot.rvalAt("html", 0x5CA91C812230855DLL)) || toBoolean(m_ot.rvalAt("pre", 0x633242EDD179FBACLL)))) {
        (v_text = LINE(2943,concat3("[[:", toString(v_titleText), "]]")));
        (v_found = true);
      }
    }
    else if (toBoolean(LINE(2946,v_title.o_invoke_few_args("isTrans", 0x509FD18EA479DF87LL, 0)))) {
      if (toBoolean(m_ot.rvalAt("html", 0x5CA91C812230855DLL)) && !(toBoolean(v_forceRawInterwiki))) {
        (v_text = LINE(2949,t_interwikitransclude(v_title, "render")));
        (v_isHTML = true);
      }
      else {
        (v_text = LINE(2952,t_interwikitransclude(v_title, "raw")));
        (v_text = LINE(2954,t_preprocesstodom(v_text, 1LL /* parser::PTD_FOR_INCLUSION */)));
        (v_isChildObj = true);
      }
      (v_found = true);
    }
    if (!(toBoolean(LINE(2962,v_frame.o_invoke_few_args("loopCheck", 0x7BA9911BE4A4175FLL, 1, v_title))))) {
      (v_found = true);
      (v_text = LINE(2964,(assignCallTemp(eo_1, toString(invoke_failed("wfmsgforcontent", Array(ArrayInit(2).set(0, "parser-template-loop-warning").set(1, ref(v_titleText)).create()), 0x0000000033607CCALL))),concat3("<span class=\"error\">", eo_1, "</span>"))));
      LINE(2965,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, concat("Parser::braceSubstitution", concat3(": template loop broken at '", toString(v_titleText), "'\n"))).create()), 0x00000000E441E905LL));
    }
    LINE(2967,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::braceSubstitution-loadtpl").create()), 0x00000000B599F276LL));
  }
  if (!(toBoolean(v_found))) {
    (v_text = LINE(2973,v_frame.o_invoke_few_args("virtualBracketedImplode", 0x409618A98590618BLL, 5, "{{", "|", "}}", v_titleWithSpaces, v_args)));
    LINE(2974,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::braceSubstitution").create()), 0x00000000B599F276LL));
    return Array(ArrayInit(1).set(0, "object", v_text, 0x7F30BC4E222B1861LL).create());
  }
  if (toBoolean(v_isChildObj)) {
    (v_newFrame = LINE(2981,v_frame.o_invoke_few_args("newChild", 0x7F4AB942B9CDE1BCLL, 2, v_args, v_title)));
    if (toBoolean(v_nowiki)) {
      (v_text = LINE(2984,v_newFrame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_text, 27LL /* ppframe::RECOVER_ORIG */)));
    }
    else if (!same(v_titleText, false) && toBoolean(LINE(2985,v_newFrame.o_invoke_few_args("isEmpty", 0x6359F42D5FC265E8LL, 0)))) {
      if (isset(m_mTplExpandCache, v_titleText)) {
        (v_text = m_mTplExpandCache.rvalAt(v_titleText));
      }
      else {
        (v_text = LINE(2990,v_newFrame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_text)));
        m_mTplExpandCache.set(v_titleText, (v_text));
      }
    }
    else {
      (v_text = LINE(2995,v_newFrame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_text)));
    }
  }
  if (toBoolean(v_isLocalObj) && toBoolean(v_nowiki)) {
    (v_text = LINE(2999,v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_text, 27LL /* ppframe::RECOVER_ORIG */)));
    (v_isLocalObj = false);
  }
  if (toBoolean(v_isHTML)) {
    (v_text = concat("\n\n", toString(LINE(3007,t_insertstripitem(v_text)))));
  }
  else if (toBoolean(v_nowiki) && (toBoolean(m_ot.rvalAt("html", 0x5CA91C812230855DLL)) || toBoolean(m_ot.rvalAt("pre", 0x633242EDD179FBACLL)))) {
    (v_text = LINE(3011,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(v_text)).create()), 0x00000000B3382909LL)));
  }
  else if (LINE(3016,x_is_string(v_text)) && !(toBoolean(v_piece.rvalAt("lineStart", 0x0F84CB175598D0A5LL))) && toBoolean(x_preg_match("/^(\?:{\\||:|;|#|\\*)/", toString(v_text)))) {
    (v_text = concat("\n", toString(v_text)));
  }
  if (LINE(3020,x_is_string(v_text)) && !(toBoolean((assignCallTemp(eo_1, x_strlen(toString(v_text))),t_incrementincludesize("post-expand", eo_1))))) {
    (v_text = LINE(3023,(assignCallTemp(eo_1, toString(v_originalTitle)),assignCallTemp(eo_3, toString(t_insertstripitem("<!-- WARNING: template omitted, post-expand include size too large -->"))),concat4("[[", eo_1, "]]", eo_3))));
    LINE(3024,t_limitationwarn("post-expand-template-inclusion"));
  }
  if (toBoolean(v_isLocalObj)) {
    (v_ret = Array(ArrayInit(1).set(0, "object", v_text, 0x7F30BC4E222B1861LL).create()));
  }
  else {
    (v_ret = Array(ArrayInit(1).set(0, "text", v_text, 0x2A28A0084DD3A743LL).create()));
  }
  LINE(3033,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::braceSubstitution").create()), 0x00000000B599F276LL));
  return v_ret;
} /* function */
/* SRC: Parser.php line 3041 */
Array c_parser::t_gettemplatedom(Variant v_title) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getTemplateDom);
  Variant eo_0;
  Variant eo_1;
  Variant v_cacheTitle;
  Variant v_titleText;
  Variant v_ns;
  Variant v_dbk;
  Variant v_text;
  Variant v_dom;
  Variant v_cdb;

  (v_cacheTitle = v_title);
  (v_titleText = LINE(3043,v_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)));
  if (isset(m_mTplRedirCache, v_titleText)) {
    df_lambda_11(m_mTplRedirCache.rvalAt(v_titleText), v_ns, v_dbk);
    (v_title = LINE(3047,throw_fatal("unknown class title", ((void*)NULL))));
    (v_titleText = LINE(3048,v_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)));
  }
  if (isset(m_mTplDomCache, v_titleText)) {
    return Array(ArrayInit(2).set(0, m_mTplDomCache.rvalAt(v_titleText)).set(1, v_title).create());
  }
  df_lambda_12(LINE(3055,t_fetchtemplateandtitle(v_title)), v_text, v_title);
  if (same(v_text, false)) {
    m_mTplDomCache.set(v_titleText, (false));
    return Array(ArrayInit(2).set(0, false).set(1, v_title).create());
  }
  (v_dom = LINE(3062,t_preprocesstodom(v_text, 1LL /* parser::PTD_FOR_INCLUSION */)));
  m_mTplDomCache.set(v_titleText, (v_dom));
  if (!(toBoolean(LINE(3065,v_title.o_invoke_few_args("equals", 0x6E9F97FFEE61A498LL, 1, v_cacheTitle))))) {
    m_mTplRedirCache.set(LINE(3066,v_cacheTitle.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)), ((assignCallTemp(eo_0, LINE(3067,v_title.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0))),assignCallTemp(eo_1, (v_cdb = v_title.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0))),Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()))));
  }
  return Array(ArrayInit(2).set(0, v_dom).set(1, v_title).create());
} /* function */
/* SRC: Parser.php line 3076 */
Array c_parser::t_fetchtemplateandtitle(CVarRef v_title) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::fetchTemplateAndTitle);
  Variant v_templateCb;
  Variant v_stuff;
  Variant v_text;
  Variant v_finalTitle;
  Variant v_dep;

  (v_templateCb = LINE(3077,m_mOptions.o_invoke_few_args("getTemplateCallback", 0x4EDD9E12C4CE34F5LL, 0)));
  (v_stuff = LINE(3078,x_call_user_func(3, v_templateCb, Array(ArrayInit(2).set(0, v_title).set(1, ((Object)(this))).create()))));
  (v_text = v_stuff.rvalAt("text", 0x2A28A0084DD3A743LL));
  (v_finalTitle = isset(v_stuff, "finalTitle", 0x55CA175D2B13753ELL) ? ((Variant)(v_stuff.rvalAt("finalTitle", 0x55CA175D2B13753ELL))) : ((Variant)(v_title)));
  if (isset(v_stuff, "deps", 0x460EC09F3DCB8AC5LL)) {
    {
      LOOP_COUNTER(69);
      Variant map70 = v_stuff.rvalAt("deps", 0x460EC09F3DCB8AC5LL);
      for (ArrayIterPtr iter71 = map70.begin("parser"); !iter71->end(); iter71->next()) {
        LOOP_COUNTER_CHECK(69);
        v_dep = iter71->second();
        {
          LINE(3083,m_mOutput.o_invoke_few_args("addTemplate", 0x668299C8203F7926LL, 3, v_dep.refvalAt("title", 0x66AD900A2301E2FELL), v_dep.refvalAt("page_id", 0x1B27A324993FB16CLL), v_dep.refvalAt("rev_id", 0x60BB6026437DAFD3LL)));
        }
      }
    }
  }
  return Array(ArrayInit(2).set(0, v_text).set(1, v_finalTitle).create());
} /* function */
/* SRC: Parser.php line 3089 */
Variant c_parser::t_fetchtemplate(CVarRef v_title) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::fetchTemplate);
  Array v_rv;

  (v_rv = LINE(3090,t_fetchtemplateandtitle(v_title)));
  return v_rv.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: Parser.php line 3098 */
Array c_parser::ti_statelessfetchtemplate(const char* cls, Variant v_title, CVarRef v_parser //  = false
) {
  STATIC_METHOD_INJECTION(Parser, Parser::statelessFetchTemplate);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_skip;
  Variant v_text;
  Variant v_finalTitle;
  Array v_deps;
  int64 v_i = 0;
  Variant v_id;
  Variant v_rev;
  Variant v_rev_id;
  Variant v_linkCache;
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_wgContLang;
  Variant v_message;

  (v_text = (v_skip = false));
  (v_finalTitle = v_title);
  (v_deps = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(72);
    for ((v_i = 0LL); less(v_i, 2LL) && LINE(3104,x_is_object(v_title)); v_i++) {
      LOOP_COUNTER_CHECK(72);
      {
        (v_id = false);
        LINE(3107,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "BeforeParserFetchTemplateAndtitle").set(1, Array(ArrayInit(4).set(0, v_parser).setRef(1, ref(v_title)).setRef(2, ref(v_skip)).setRef(3, ref(v_id)).create())).create()), 0x00000000787A96B2LL));
        if (toBoolean(v_skip)) {
          (v_text = false);
          v_deps.append(((assignCallTemp(eo_0, v_title),assignCallTemp(eo_1, LINE(3113,v_title.o_invoke_few_args("getArticleID", 0x094F8EE5A02782D6LL, 0))),Array(ArrayInit(3).set(0, "title", eo_0, 0x66AD900A2301E2FELL).set(1, "page_id", eo_1, 0x1B27A324993FB16CLL).set(2, "rev_id", null, 0x60BB6026437DAFD3LL).create()))));
          break;
        }
        (v_rev = toBoolean(v_id) ? ((Variant)(LINE(3117,throw_fatal("unknown class revision", ((void*)NULL))))) : ((Variant)(throw_fatal("unknown class revision", ((void*)NULL)))));
        (v_rev_id = toBoolean(v_rev) ? ((Variant)(LINE(3118,v_rev.o_invoke_few_args("getId", 0x5ADBB1D3A7BA92B8LL, 0)))) : ((Variant)(0LL)));
        if (same(v_id, false) && !(toBoolean(v_rev))) {
          (v_linkCache = LINE(3121,throw_fatal("unknown class linkcache", ((void*)NULL))));
          LINE(3122,v_linkCache.o_invoke_few_args("addBadLinkObj", 0x6D7C22FD99A36EDBLL, 1, v_title));
        }
        v_deps.append(((assignCallTemp(eo_0, v_title),assignCallTemp(eo_1, LINE(3127,v_title.o_invoke_few_args("getArticleID", 0x094F8EE5A02782D6LL, 0))),assignCallTemp(eo_2, v_rev_id),Array(ArrayInit(3).set(0, "title", eo_0, 0x66AD900A2301E2FELL).set(1, "page_id", eo_1, 0x1B27A324993FB16CLL).set(2, "rev_id", eo_2, 0x60BB6026437DAFD3LL).create()))));
        if (toBoolean(v_rev)) {
          (v_text = LINE(3131,v_rev.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0)));
        }
        else if (equal(LINE(3132,v_title.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)), k_NS_MEDIAWIKI)) {
          v_wgContLang = ref(g->GV(wgContLang));
          (v_message = LINE(3134,v_wgContLang.o_invoke_few_args("lcfirst", 0x7448B5BAF0B032B3LL, 1, v_title.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0))));
          (v_text = LINE(3135,invoke_failed("wfmsgforcontentnotrans", Array(ArrayInit(1).set(0, ref(v_message)).create()), 0x00000000EBECF191LL)));
          if (toBoolean(LINE(3136,invoke_failed("wfemptymsg", Array(ArrayInit(2).set(0, ref(v_message)).set(1, ref(v_text)).create()), 0x00000000ACF08022LL)))) {
            (v_text = false);
            break;
          }
        }
        else {
          break;
        }
        if (same(v_text, false)) {
          break;
        }
        (v_finalTitle = v_title);
        (v_title = LINE(3148,throw_fatal("unknown class title", ((void*)NULL))));
      }
    }
  }
  return Array(ArrayInit(3).set(0, "text", v_text, 0x2A28A0084DD3A743LL).set(1, "finalTitle", v_finalTitle, 0x55CA175D2B13753ELL).set(2, "deps", v_deps, 0x460EC09F3DCB8AC5LL).create());
} /* function */
/* SRC: Parser.php line 3159 */
Variant c_parser::t_interwikitransclude(CVarRef v_title, CStrRef v_action) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::interwikiTransclude);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgEnableScaryTranscluding __attribute__((__unused__)) = g->GV(wgEnableScaryTranscluding);
  Variant v_url;

  if (!(toBoolean(gv_wgEnableScaryTranscluding))) return LINE(3163,invoke_failed("wfmsg", Array(ArrayInit(1).set(0, "scarytranscludedisabled").create()), 0x0000000002E29FB3LL));
  (v_url = LINE(3165,toObject(v_title)->o_invoke_few_args("getFullUrl", 0x4D03E4B69713A33BLL, 1, toString("action=") + v_action)));
  if (more(LINE(3167,x_strlen(toString(v_url))), 255LL)) return LINE(3168,invoke_failed("wfmsg", Array(ArrayInit(1).set(0, "scarytranscludetoolong").create()), 0x0000000002E29FB3LL));
  return LINE(3169,t_fetchscarytemplatemaybefromcache(v_url));
} /* function */
/* SRC: Parser.php line 3172 */
Variant c_parser::t_fetchscarytemplatemaybefromcache(Variant v_url) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::fetchScaryTemplateMaybeFromCache);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgTranscludeCacheExpiry __attribute__((__unused__)) = g->GV(wgTranscludeCacheExpiry);
  Variant v_dbr;
  Variant v_tsCond;
  Variant v_obj;
  Variant v_text;
  Variant v_dbw;

  (v_dbr = LINE(3174,invoke_failed("wfgetdb", Array(ArrayInit(1).set(0, k_DB_SLAVE).create()), 0x000000002DA3FF23LL)));
  (v_tsCond = LINE(3175,v_dbr.o_invoke_few_args("timestamp", 0x04F76359D187BC06LL, 1, x_time() - gv_wgTranscludeCacheExpiry)));
  (v_obj = (assignCallTemp(eo_0, toObject(v_dbr)),LINE(3177,eo_0.o_invoke_few_args("selectRow", 0x7AA6FDEB2182F653LL, 3, "transcache", Array(ArrayInit(2).set(0, "tc_time").set(1, "tc_contents").create()), (assignCallTemp(eo_1, v_url),assignCallTemp(eo_2, concat("tc_time >= ", toString(v_dbr.o_invoke_few_args("addQuotes", 0x3D76A2E50E13EE4CLL, 1, v_tsCond)))),Array(ArrayInit(2).set(0, "tc_url", eo_1, 0x6AAC1850B24C76E9LL).set(1, eo_2).create()))))));
  if (toBoolean(v_obj)) {
    return v_obj.o_get("tc_contents", 0x0B5478C26AE4342DLL);
  }
  (v_text = LINE(3182,throw_fatal("unknown class http", ((void*)NULL))));
  if (!(toBoolean(v_text))) return LINE(3184,invoke_failed("wfmsg", Array(ArrayInit(2).set(0, "scarytranscludefailed").set(1, ref(v_url)).create()), 0x0000000002E29FB3LL));
  (v_dbw = LINE(3186,invoke_failed("wfgetdb", Array(ArrayInit(1).set(0, k_DB_MASTER).create()), 0x000000002DA3FF23LL)));
  (assignCallTemp(eo_1, toObject(v_dbw)),LINE(3190,eo_1.o_invoke_few_args("replace", 0x42FAC655280A6146LL, 3, "transcache", Array(ArrayInit(1).set(0, "tc_url").create()), (assignCallTemp(eo_2, v_url),assignCallTemp(eo_3, LINE(3189,v_dbw.o_invoke_few_args("timestamp", 0x04F76359D187BC06LL, 1, x_time()))),assignCallTemp(eo_4, v_text),Array(ArrayInit(3).set(0, "tc_url", eo_2, 0x6AAC1850B24C76E9LL).set(1, "tc_time", eo_3, 0x0D1D3E8C72685FEALL).set(2, "tc_contents", eo_4, 0x0B5478C26AE4342DLL).create())))));
  return v_text;
} /* function */
/* SRC: Parser.php line 3199 */
Array c_parser::t_argsubstitution(Variant v_piece, CVarRef v_frame) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::argSubstitution);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_error;
  Variant v_parts;
  Variant v_nameWithSpaces;
  Variant v_argName;
  Variant v_object;
  Variant v_text;
  Array v_ret;

  LINE(3200,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::argSubstitution").create()), 0x0000000075359BAFLL));
  (v_error = false);
  (v_parts = v_piece.rvalAt("parts", 0x2708FDA74562AD8DLL));
  (v_nameWithSpaces = LINE(3204,toObject(v_frame)->o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_piece.refvalAt("title", 0x66AD900A2301E2FELL))));
  (v_argName = LINE(3205,x_trim(toString(v_nameWithSpaces))));
  (v_object = false);
  (v_text = LINE(3207,toObject(v_frame)->o_invoke_few_args("getArgument", 0x0A95A4780D41F7B8LL, 1, v_argName)));
  if (same(v_text, false) && more(LINE(3208,v_parts.o_invoke_few_args("getLength", 0x41EB078EF92C44D4LL, 0)), 0LL) && (toBoolean(m_ot.rvalAt("html", 0x5CA91C812230855DLL)) || toBoolean(m_ot.rvalAt("pre", 0x633242EDD179FBACLL)) || (toBoolean(m_ot.rvalAt("wiki", 0x24648D2BCD794D21LL)) && toBoolean(LINE(3212,toObject(v_frame)->o_invoke_few_args("isTemplate", 0x7CBE9654ADB6CFB9LL, 0)))))) {
    (v_object = (assignCallTemp(eo_0, toObject(LINE(3216,v_parts.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, 0LL)))),eo_0.o_invoke_few_args("getChildren", 0x732EC1BDA8EC520FLL, 0)));
  }
  if (!(toBoolean((assignCallTemp(eo_2, LINE(3218,x_strlen(toString(v_text)))),t_incrementincludesize("arg", eo_2))))) {
    (v_error = "<!-- WARNING: argument omitted, expansion size too large -->");
    LINE(3220,t_limitationwarn("post-expand-template-argument"));
  }
  if (same(v_text, false) && same(v_object, false)) {
    (v_object = LINE(3225,toObject(v_frame)->o_invoke_few_args("virtualBracketedImplode", 0x409618A98590618BLL, 5, "{{{", "|", "}}}", v_nameWithSpaces, v_parts)));
  }
  if (!same(v_error, false)) {
    concat_assign(v_text, toString(v_error));
  }
  if (!same(v_object, false)) {
    (v_ret = Array(ArrayInit(1).set(0, "object", v_object, 0x7F30BC4E222B1861LL).create()));
  }
  else {
    (v_ret = Array(ArrayInit(1).set(0, "text", v_text, 0x2A28A0084DD3A743LL).create()));
  }
  LINE(3236,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::argSubstitution").create()), 0x00000000B599F276LL));
  return v_ret;
} /* function */
/* SRC: Parser.php line 3252 */
Variant c_parser::t_extensionsubstitution(Variant v_params, CVarRef v_frame) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::extensionSubstitution);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgRawHtml __attribute__((__unused__)) = g->GV(wgRawHtml);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_name;
  Variant v_attrText;
  Variant v_content;
  Variant v_marker;
  bool v_isFunctionTag = false;
  Variant v_attributes;
  Variant v_output;
  Variant v_callback;
  Variant v_flags;
  Primitive v_attrName = 0;
  Variant v_attrValue;
  Variant v_close;

  {
  }
  (v_name = LINE(3255,toObject(v_frame)->o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_params.refvalAt("name", 0x0BCDB293DC3CBDDCLL))));
  (v_attrText = !(isset(v_params, "attr", 0x64311A2C8443755DLL)) ? ((Variant)(null)) : ((Variant)(LINE(3256,toObject(v_frame)->o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_params.refvalAt("attr", 0x64311A2C8443755DLL))))));
  (v_content = !(isset(v_params, "inner", 0x4F20D07BB803C1FELL)) ? ((Variant)(null)) : ((Variant)(LINE(3257,toObject(v_frame)->o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_params.refvalAt("inner", 0x4F20D07BB803C1FELL))))));
  (v_marker = LINE(3259,(assignCallTemp(eo_0, toString(o_get("mUniqPrefix", 0x22D00FA247E8311DLL))),assignCallTemp(eo_2, toString(v_name)),assignCallTemp(eo_4, (assignCallTemp(eo_7, m_mMarkerIndex++),x_sprintf(2, "%08X", Array(ArrayInit(1).set(0, eo_7).create())))),concat6(eo_0, "-", eo_2, "-", eo_4, "-QINU\177" /* parser::MARKER_SUFFIX */))));
  (v_isFunctionTag = isset(m_mFunctionTagHooks, LINE(3261,x_strtolower(toString(v_name)))) && (toBoolean(m_ot.rvalAt("html", 0x5CA91C812230855DLL)) || toBoolean(m_ot.rvalAt("pre", 0x633242EDD179FBACLL))));
  if (toBoolean(m_ot.rvalAt("html", 0x5CA91C812230855DLL)) || v_isFunctionTag) {
    (v_name = LINE(3264,x_strtolower(toString(v_name))));
    (v_attributes = LINE(3265,throw_fatal("unknown class sanitizer", ((void*)NULL))));
    if (isset(v_params, "attributes", 0x3E5975BA0FC37E03LL)) {
      (v_attributes = v_attributes + v_params.rvalAt("attributes", 0x3E5975BA0FC37E03LL));
    }
    {
      Variant tmp74 = (v_name);
      int tmp75 = -1;
      if (equal(tmp74, ("html"))) {
        tmp75 = 0;
      } else if (equal(tmp74, ("nowiki"))) {
        tmp75 = 1;
      } else if (equal(tmp74, ("gallery"))) {
        tmp75 = 2;
      } else if (equal(tmp74, ("a"))) {
        tmp75 = 3;
      } else if (equal(tmp74, ("math"))) {
        tmp75 = 4;
      } else if (true) {
        tmp75 = 5;
      }
      switch (tmp75) {
      case 0:
        {
          if (toBoolean(gv_wgRawHtml)) {
            (v_output = v_content);
            goto break73;
          }
          else {
            throw_exception(LINE(3275,create_object("mwexception", Array(ArrayInit(1).set(0, "<html> extension tag encountered unexpectedly").create()))));
          }
        }
      case 1:
        {
          (v_content = LINE(3278,x_strtr(toString(v_content), ScalarArrays::sa_[23])));
          (v_output = LINE(3279,throw_fatal("unknown class xml", ((void*)NULL))));
          goto break73;
        }
      case 2:
        {
          (v_output = LINE(3282,t_renderimagegallery(v_content, v_attributes)));
          goto break73;
        }
      case 3:
        {
          (v_output = LINE(3285,t_renderhyperlink(v_content, v_attributes, v_frame)));
          goto break73;
        }
      case 4:
        {
          if (toBoolean(LINE(3288,m_mOptions.o_invoke_few_args("getUseTeX", 0x2C2CE4CD607BE9D3LL, 0)))) {
            (v_output = (assignCallTemp(eo_0, toObject(gv_wgContLang)),LINE(3290,eo_0.o_invoke_few_args("armourMath", 0x56A51C7B197A1253LL, 1, throw_fatal("unknown class mathrenderer", ((void*)NULL))))));
            goto break73;
          }
        }
      case 5:
        {
          if (isset(m_mTagHooks, v_name)) {
            if (!(LINE(3297,x_is_callable(m_mTagHooks.rvalAt(v_name))))) {
              throw_exception(LINE(3298,create_object("mwexception", Array(ArrayInit(1).set(0, concat3("Tag hook for ", toString(v_name), " is not callable\n")).create()))));
            }
            (v_output = LINE(3301,x_call_user_func_array(m_mTagHooks.rvalAt(v_name), Array(ArrayInit(4).set(0, v_content).set(1, v_attributes).set(2, ((Object)(this))).set(3, v_frame).create()))));
          }
          else if (isset(m_mFunctionTagHooks, v_name)) {
            df_lambda_13(m_mFunctionTagHooks.rvalAt(v_name), v_callback, v_flags);
            if (!(LINE(3304,x_is_callable(v_callback)))) throw_exception(LINE(3305,create_object("mwexception", Array(ArrayInit(1).set(0, concat3("Tag hook for ", toString(v_name), " is not callable\n")).create()))));
            (v_output = LINE(3308,x_call_user_func_array(v_callback, Array(ArrayInit(4).setRef(0, ref(this)).set(1, v_frame).set(2, v_content).set(3, v_attributes).create()))));
          }
          else {
            (v_output = LINE(3311,(assignCallTemp(eo_2, x_htmlspecialchars(toString(v_name))),concat3("<span class=\"error\">Invalid tag extension name: ", eo_2, "</span>"))));
          }
        }
      }
      break73:;
    }
  }
  else {
    if (LINE(3315,x_is_null(v_attrText))) {
      (v_attrText = "");
    }
    if (isset(v_params, "attributes", 0x3E5975BA0FC37E03LL)) {
      {
        LOOP_COUNTER(76);
        Variant map77 = v_params.rvalAt("attributes", 0x3E5975BA0FC37E03LL);
        for (ArrayIterPtr iter78 = map77.begin("parser"); !iter78->end(); iter78->next()) {
          LOOP_COUNTER_CHECK(76);
          v_attrValue = iter78->second();
          v_attrName = iter78->first();
          {
            concat_assign(v_attrText, concat(concat_rev(LINE(3321,x_htmlspecialchars(toString(v_attrValue))), LINE(3320,(assignCallTemp(eo_2, x_htmlspecialchars(toString(v_attrName))),concat3(" ", eo_2, "=\"")))), "\""));
          }
        }
      }
    }
    if (same(v_content, null)) {
      (v_output = LINE(3325,concat4("<", toString(v_name), toString(v_attrText), "/>")));
    }
    else {
      (v_close = LINE(3327,x_is_null(v_params.rvalAt("close", 0x36B43082F052EC6BLL))) ? ((Variant)("")) : ((Variant)(toObject(v_frame)->o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_params.refvalAt("close", 0x36B43082F052EC6BLL)))));
      (v_output = LINE(3328,concat6("<", toString(v_name), toString(v_attrText), ">", toString(v_content), toString(v_close))));
    }
  }
  if (v_isFunctionTag) {
    return v_output;
  }
  else if (same(v_name, "html") || same(v_name, "nowiki")) {
    LINE(3335,m_mStripState.o_get("nowiki", 0x78258C7EF69CF55DLL).o_invoke_few_args("setPair", 0x1CA93325C3BDD490LL, 2, v_marker, v_output));
  }
  else {
    LINE(3337,m_mStripState.o_get("general", 0x661B15A9C00F7127LL).o_invoke_few_args("setPair", 0x1CA93325C3BDD490LL, 2, v_marker, v_output));
  }
  return v_marker;
} /* function */
/* SRC: Parser.php line 3349 */
Variant c_parser::t_incrementincludesize(Variant v_type, int v_size) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::incrementIncludeSize);
  if (more(m_mIncludeSizes.rvalAt(v_type) + v_size, LINE(3350,m_mOptions.o_invoke_few_args("getMaxIncludeSize", 0x5356D47AAA28FDF5LL, 1, v_type)))) {
    return false;
  }
  else {
    lval(m_mIncludeSizes.lvalAt(v_type)) += v_size;
    return true;
  }
  return null;
} /* function */
/* SRC: Parser.php line 3363 */
bool c_parser::t_incrementexpensivefunctioncount() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::incrementExpensiveFunctionCount);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgExpensiveParserFunctionLimit __attribute__((__unused__)) = g->GV(wgExpensiveParserFunctionLimit);
  m_mExpensiveFunctionCount++;
  if (not_more(m_mExpensiveFunctionCount, gv_wgExpensiveParserFunctionLimit)) {
    return true;
  }
  return false;
} /* function */
/* SRC: Parser.php line 3376 */
Variant c_parser::t_dodoubleunderscore(Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::doDoubleUnderscore);
  Variant v_mw;
  Variant v_mwa;

  LINE(3377,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::doDoubleUnderscore").create()), 0x0000000075359BAFLL));
  (v_mw = LINE(3379,throw_fatal("unknown class magicword", ((void*)NULL))));
  if (toBoolean(LINE(3380,v_mw.o_invoke_few_args("match", 0x2B8782BF7206E093LL, 1, v_text)))) {
    (o_lval("mShowToc", 0x673E52F417D0192CLL) = true);
    (o_lval("mForceTocPosition", 0x00520DD93AF13A69LL) = true);
    (v_text = LINE(3385,v_mw.o_invoke_few_args("replace", 0x42FAC655280A6146LL, 3, "<!--MWTOC-->", v_text, 1LL)));
    (v_text = LINE(3388,v_mw.o_invoke_few_args("replace", 0x42FAC655280A6146LL, 2, "", v_text)));
  }
  (v_mwa = LINE(3392,throw_fatal("unknown class magicword", ((void*)NULL))));
  (m_mDoubleUnderscores = LINE(3393,v_mwa.o_invoke_few_args("matchAndRemove", 0x5D764C79A01CBB8ALL, 1, v_text)));
  if (isset(m_mDoubleUnderscores, "nogallery", 0x6F33D1D1CA81CAF8LL)) {
    (m_mOutput.o_lval("mNoGallery", 0x1A3DE12EE5312AF7LL) = true);
  }
  if (isset(m_mDoubleUnderscores, "notoc", 0x5A0DBCCD1B31033ELL) && !(toBoolean(o_get("mForceTocPosition", 0x00520DD93AF13A69LL)))) {
    (o_lval("mShowToc", 0x673E52F417D0192CLL) = false);
  }
  if (isset(m_mDoubleUnderscores, "hiddencat", 0x2C5B4188322C4B9DLL) && equal(LINE(3401,m_mTitle.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)), k_NS_CATEGORY)) {
    LINE(3402,m_mOutput.o_invoke_few_args("setProperty", 0x71FC9AA7F05A6BD3LL, 2, "hiddencat", "y"));
    LINE(3403,t_addtrackingcategory("hidden-category-category"));
  }
  if (isset(m_mDoubleUnderscores, "noindex", 0x79A36E5D0F4C89CDLL) && toBoolean(LINE(3409,m_mTitle.o_invoke_few_args("canUseNoindex", 0x424D16BD430D23E2LL, 0)))) {
    LINE(3410,m_mOutput.o_invoke_few_args("setIndexPolicy", 0x78B79F97AFD45D1BLL, 1, "noindex"));
    LINE(3411,t_addtrackingcategory("noindex-category"));
  }
  if (isset(m_mDoubleUnderscores, "index", 0x440D5888C0FF3081LL) && toBoolean(LINE(3413,m_mTitle.o_invoke_few_args("canUseNoindex", 0x424D16BD430D23E2LL, 0)))) {
    LINE(3414,m_mOutput.o_invoke_few_args("setIndexPolicy", 0x78B79F97AFD45D1BLL, 1, "index"));
    LINE(3415,t_addtrackingcategory("index-category"));
  }
  LINE(3417,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::doDoubleUnderscore").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 3427 */
Variant c_parser::t_addtrackingcategory(Variant v_msg) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::addTrackingCategory);
  Variant eo_0;
  Variant eo_1;
  Variant v_cat;
  Variant v_containerCategory;

  (v_cat = LINE(3428,invoke_failed("wfmsgforcontent", Array(ArrayInit(1).set(0, ref(v_msg)).create()), 0x0000000033607CCALL)));
  if (same(v_cat, "-")) return false;
  (v_containerCategory = LINE(3433,throw_fatal("unknown class title", ((void*)NULL))));
  if (toBoolean(v_containerCategory)) {
    (assignCallTemp(eo_0, ref(LINE(3435,v_containerCategory.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)))),assignCallTemp(eo_1, ref(t_getdefaultsort())),m_mOutput.o_invoke("addCategory", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x6DBAA9816B039793LL));
    return true;
  }
  else {
    LINE(3438,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, concat("Parser::addTrackingCategory", concat3(": [[MediaWiki:", toString(v_msg), "]] is not a valid title!\n"))).create()), 0x00000000E441E905LL));
    return false;
  }
  return null;
} /* function */
/* SRC: Parser.php line 3458 */
Variant c_parser::t_formatheadings(CVarRef v_text, CVarRef v_origText, bool v_isMain //  = true
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::formatHeadings);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgMaxTocLevel __attribute__((__unused__)) = g->GV(wgMaxTocLevel);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant &gv_wgEnforceHtmlIds __attribute__((__unused__)) = g->GV(wgEnforceHtmlIds);
  Variant v_doNumberHeadings;
  Variant v_showEditLink;
  Variant v_matches;
  Variant v_numMatches;
  bool v_enoughToc = false;
  Variant v_sk;
  int64 v_headlineCount = 0;
  int64 v_numVisible = 0;
  Variant v_toc;
  Variant v_full;
  Variant v_head;
  Variant v_sublevelCount;
  Variant v_levelCount;
  Variant v_toclevel;
  Variant v_level;
  Variant v_prevlevel;
  Variant v_prevtoclevel;
  String v_markerRegex;
  Variant v_baseTitleText;
  Variant v_oldType;
  Variant v_frame;
  Variant v_root;
  Variant v_node;
  Variant v_byteOffset;
  Variant v_tocraw;
  Variant v_headline;
  bool v_isTemplate = false;
  Variant v_titleText;
  Variant v_sectionIndex;
  Variant v_numbering;
  Variant v_markerMatches;
  Variant v_serial;
  Variant v_i;
  int64 v_dot = 0;
  Variant v_safeHeadline;
  Variant v_tocline;
  Variant v_headlineHint;
  Variant v_legacyHeadline;
  String v_arrayKey;
  Variant v_legacyArrayKey;
  Variant v_refers;
  Variant v_anchor;
  Variant v_legacyAnchor;
  Variant v_bits;
  Variant v_editlink;
  Variant v_blocks;
  Variant v_block;

  {
  }
  (v_doNumberHeadings = LINE(3461,m_mOptions.o_invoke_few_args("getNumberHeadings", 0x24F0B1CAFFCAC26ELL, 0)));
  (v_showEditLink = LINE(3462,m_mOptions.o_invoke_few_args("getEditSection", 0x5F1B74D4BEBB6CD5LL, 0)));
  if (toBoolean(v_showEditLink) && !(toBoolean(LINE(3465,m_mTitle.o_invoke_few_args("quickUserCan", 0x643A7B8592BE5D21LL, 1, "edit"))))) {
    (v_showEditLink = 0LL);
  }
  if (isset(m_mDoubleUnderscores, "noeditsection", 0x1801E39E12ED2434LL) || toBoolean(LINE(3470,m_mOptions.o_invoke_few_args("getIsPrintable", 0x6256ABE618956C3DLL, 0)))) {
    (v_showEditLink = 0LL);
  }
  (v_matches = ScalarArrays::sa_[0]);
  (v_numMatches = LINE(3477,x_preg_match_all("/<H(\?P<level>[1-6])(\?P<attrib>.*\?>)(\?P<header>.*\?)<\\/H[1-6] *>/i", toString(v_text), ref(v_matches))));
  (v_enoughToc = toBoolean(o_get("mShowToc", 0x673E52F417D0192CLL)) && ((not_less(v_numMatches, 4LL)) || toBoolean(o_get("mForceTocPosition", 0x00520DD93AF13A69LL))));
  if (isset(m_mDoubleUnderscores, "newsectionlink", 0x101E6B0BB687547ELL)) {
    LINE(3487,m_mOutput.o_invoke_few_args("setNewSection", 0x7AAA4B3474D6F233LL, 1, true));
  }
  if (isset(m_mDoubleUnderscores, "nonewsectionlink", 0x200499187756CDE0LL)) {
    LINE(3493,m_mOutput.o_invoke_few_args("hideNewSection", 0x3F7198F790CA3D5CLL, 1, true));
  }
  if (isset(m_mDoubleUnderscores, "forcetoc", 0x51B4F107BB3551E3LL)) {
    (o_lval("mShowToc", 0x673E52F417D0192CLL) = true);
    (v_enoughToc = true);
  }
  (v_sk = LINE(3504,m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_headlineCount = 0LL);
  (v_numVisible = 0LL);
  (v_toc = "");
  (v_full = "");
  (v_head = ScalarArrays::sa_[0]);
  (v_sublevelCount = ScalarArrays::sa_[0]);
  (v_levelCount = ScalarArrays::sa_[0]);
  (v_toclevel = 0LL);
  (v_level = 0LL);
  (v_prevlevel = 0LL);
  (v_toclevel = 0LL);
  (v_prevtoclevel = 0LL);
  (v_markerRegex = concat(toString(o_get("mUniqPrefix", 0x22D00FA247E8311DLL)) + toString("-h-(\\d+)-"), "-QINU\177" /* parser::MARKER_SUFFIX */));
  (v_baseTitleText = LINE(3523,m_mTitle.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)));
  (v_oldType = m_mOutputType);
  LINE(3525,t_setoutputtype(2LL /* parser::OT_WIKI */));
  (v_frame = (assignCallTemp(eo_0, toObject(LINE(3526,t_getpreprocessor()))),eo_0.o_invoke_few_args("newFrame", 0x787B6141D7721675LL, 0)));
  (v_root = LINE(3527,t_preprocesstodom(v_origText)));
  (v_node = LINE(3528,v_root.o_invoke_few_args("getFirstChild", 0x050681239965E069LL, 0)));
  (v_byteOffset = 0LL);
  (v_tocraw = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(79);
    Variant map80 = v_matches.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL);
    for (ArrayIterPtr iter81 = map80.begin("parser"); !iter81->end(); iter81->next()) {
      LOOP_COUNTER_CHECK(79);
      v_headline = iter81->second();
      {
        (v_isTemplate = false);
        (v_titleText = false);
        (v_sectionIndex = false);
        (v_numbering = "");
        (v_markerMatches = ScalarArrays::sa_[0]);
        if (toBoolean(LINE(3538,(assignCallTemp(eo_1, concat3("/^", v_markerRegex, "/")),assignCallTemp(eo_2, toString(v_headline)),assignCallTemp(eo_3, ref(v_markerMatches)),x_preg_match(eo_1, eo_2, eo_3))))) {
          (v_serial = v_markerMatches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
          df_lambda_14(m_mHeadings.rvalAt(v_serial), v_titleText, v_sectionIndex);
          (v_isTemplate = (!equal(v_titleText, v_baseTitleText)));
          (v_headline = LINE(3542,(assignCallTemp(eo_1, concat3("/^", v_markerRegex, "/")),assignCallTemp(eo_3, v_headline),x_preg_replace(eo_1, "", eo_3))));
        }
        if (toBoolean(v_toclevel)) {
          (v_prevlevel = v_level);
          (v_prevtoclevel = v_toclevel);
        }
        (v_level = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL).rvalAt(v_headlineCount));
        if (more(v_level, v_prevlevel)) {
          v_toclevel++;
          v_sublevelCount.set(v_toclevel, (0LL));
          if (less(v_toclevel, gv_wgMaxTocLevel)) {
            (v_prevtoclevel = v_toclevel);
            concat_assign(v_toc, toString(LINE(3557,v_sk.o_invoke_few_args("tocIndent", 0x2FA99265082DB35BLL, 0))));
            v_numVisible++;
          }
        }
        else if (less(v_level, v_prevlevel) && more(v_toclevel, 1LL)) {
          {
            LOOP_COUNTER(82);
            for ((v_i = v_toclevel); more(v_i, 0LL); v_i--) {
              LOOP_COUNTER_CHECK(82);
              {
                if (equal(v_levelCount.rvalAt(v_i), v_level)) {
                  (v_toclevel = v_i);
                  break;
                }
                else if (less(v_levelCount.rvalAt(v_i), v_level)) {
                  (v_toclevel = v_i + 1LL);
                  break;
                }
              }
            }
          }
          if (equal(v_i, 0LL)) (v_toclevel = 1LL);
          if (less(v_toclevel, gv_wgMaxTocLevel)) {
            if (less(v_prevtoclevel, gv_wgMaxTocLevel)) {
              concat_assign(v_toc, toString(LINE(3580,v_sk.o_invoke_few_args("tocUnindent", 0x093C3E54C9882291LL, 1, v_prevtoclevel - v_toclevel))));
              (v_prevtoclevel = v_toclevel);
            }
            else {
              concat_assign(v_toc, toString(LINE(3583,v_sk.o_invoke_few_args("tocLineEnd", 0x797D6BBFCB435B39LL, 0))));
            }
          }
        }
        else {
          if (less(v_toclevel, gv_wgMaxTocLevel)) {
            concat_assign(v_toc, toString(LINE(3590,v_sk.o_invoke_few_args("tocLineEnd", 0x797D6BBFCB435B39LL, 0))));
          }
        }
        v_levelCount.set(v_toclevel, (v_level));
        (silenceInc(), silenceDec(lval(v_sublevelCount.lvalAt(v_toclevel))++));
        (v_dot = 0LL);
        {
          LOOP_COUNTER(83);
          for ((v_i = 1LL); not_more(v_i, v_toclevel); v_i++) {
            LOOP_COUNTER_CHECK(83);
            {
              if (!(empty(v_sublevelCount, v_i))) {
                if (toBoolean(v_dot)) {
                  concat_assign(v_numbering, ".");
                }
                concat_assign(v_numbering, toString(LINE(3604,gv_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, v_sublevelCount.refvalAt(v_i)))));
                (v_dot = 1LL);
              }
            }
          }
        }
        (v_safeHeadline = LINE(3611,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_headline)));
        (v_safeHeadline = LINE(3617,t_replacelinkholderstext(v_safeHeadline)));
        (v_tocline = LINE(3624,x_preg_replace(ScalarArrays::sa_[24], ScalarArrays::sa_[25], v_safeHeadline)));
        (v_tocline = LINE(3625,x_trim(toString(v_tocline))));
        (v_safeHeadline = LINE(3628,x_preg_replace("/<.*\?>/", "", v_safeHeadline)));
        (v_safeHeadline = LINE(3629,x_preg_replace("/[ _]+/", " ", v_safeHeadline)));
        (v_safeHeadline = LINE(3630,x_trim(toString(v_safeHeadline))));
        (v_headlineHint = v_safeHeadline);
        if (toBoolean(gv_wgEnforceHtmlIds)) {
          (v_legacyHeadline = false);
          (v_safeHeadline = LINE(3638,throw_fatal("unknown class sanitizer", ((void*)NULL))));
        }
        else {
          (v_legacyHeadline = LINE(3651,throw_fatal("unknown class sanitizer", ((void*)NULL))));
          (v_safeHeadline = LINE(3652,throw_fatal("unknown class sanitizer", ((void*)NULL))));
          if (equal(v_legacyHeadline, v_safeHeadline)) {
            (v_legacyHeadline = false);
          }
          else if (!equal(v_legacyHeadline, LINE(3658,throw_fatal("unknown class sanitizer", ((void*)NULL))))) {
            (v_legacyHeadline = false);
          }
        }
        (v_arrayKey = LINE(3670,x_strtolower(toString(v_safeHeadline))));
        if (same(v_legacyHeadline, false)) {
          (v_legacyArrayKey = false);
        }
        else {
          (v_legacyArrayKey = LINE(3674,x_strtolower(toString(v_legacyHeadline))));
        }
        if (isset(v_refers, v_arrayKey)) {
          lval(v_refers.lvalAt(v_arrayKey))++;
        }
        else {
          v_refers.set(v_arrayKey, (1LL));
        }
        if (isset(v_refers, v_legacyArrayKey)) {
          lval(v_refers.lvalAt(v_legacyArrayKey))++;
        }
        else {
          v_refers.set(v_legacyArrayKey, (1LL));
        }
        if (toBoolean(v_doNumberHeadings) && more(LINE(3690,x_count(v_matches.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL))), 1LL)) {
          (v_headline = LINE(3692,concat3(toString(v_numbering), " ", toString(v_headline))));
        }
        (v_anchor = v_safeHeadline);
        (v_legacyAnchor = v_legacyHeadline);
        if (more(v_refers.rvalAt(v_arrayKey), 1LL)) {
          concat_assign(v_anchor, concat("_", toString(v_refers.rvalAt(v_arrayKey))));
        }
        if (!same(v_legacyHeadline, false) && more(v_refers.rvalAt(v_legacyArrayKey), 1LL)) {
          concat_assign(v_legacyAnchor, concat("_", toString(v_refers.rvalAt(v_legacyArrayKey))));
        }
        if (v_enoughToc && (!(isset(gv_wgMaxTocLevel)) || less(v_toclevel, gv_wgMaxTocLevel))) {
          concat_assign(v_toc, toString((assignCallTemp(eo_1, toObject(v_sk)),LINE(3706,eo_1.o_invoke_few_args("tocLine", 0x452A70789058599DLL, 5, v_anchor, v_tocline, v_numbering, v_toclevel, (v_isTemplate ? ((Variant)(false)) : ((Variant)(v_sectionIndex))))))));
        }
        LOOP_COUNTER(84);
        {
          while (toBoolean(v_node) && !(v_isTemplate)) {
            LOOP_COUNTER_CHECK(84);
            {
              if (same(LINE(3712,v_node.o_invoke_few_args("getName", 0x23F51CDECC198965LL, 0)), "h")) {
                (v_bits = LINE(3713,v_node.o_invoke_few_args("splitHeading", 0x0621AE2D22A1922DLL, 0)));
                if (equal(v_bits.rvalAt("i", 0x0EB22EDA95766E98LL), v_sectionIndex)) break;
              }
              v_byteOffset += LINE(3718,x_mb_strlen(toString((assignCallTemp(eo_2, toObject(m_mStripState)),eo_2.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_node, 27LL /* ppframe::RECOVER_ORIG */))))));
              (v_node = LINE(3719,v_node.o_invoke_few_args("getNextSibling", 0x4CB333CF2D880119LL, 0)));
            }
          }
        }
        v_tocraw.append((Array(ArrayInit(8).set(0, "toclevel", v_toclevel, 0x5CD76FFB746050EDLL).set(1, "level", v_level, 0x26B910814038EBA3LL).set(2, "line", v_tocline, 0x21093C71DDF8728CLL).set(3, "number", v_numbering, 0x2E2E8BE57817A743LL).set(4, "index", concat((toString(v_isTemplate ? (("T-")) : (("")))), toString(v_sectionIndex)), 0x440D5888C0FF3081LL).set(5, "fromtitle", v_titleText, 0x3EDEEBB22A230184LL).set(6, "byteoffset", (v_isTemplate ? ((Variant)(null)) : ((Variant)(v_byteOffset))), 0x6C263FBE5174D9CDLL).set(7, "anchor", v_anchor, 0x07FC0405478F294FLL).create())));
        if (toBoolean(v_showEditLink) && !same(v_sectionIndex, false)) {
          if (v_isTemplate) {
            (v_editlink = (assignCallTemp(eo_3, ref(LINE(3737,throw_fatal("unknown class title", ((void*)NULL))))),assignCallTemp(eo_4, toString("T-") + toString(v_sectionIndex)),v_sk.o_invoke("doEditSectionLink", Array(ArrayInit(2).set(0, eo_3).set(1, eo_4).create()), 0x2F1D4ACEF0431135LL)));
          }
          else {
            (v_editlink = LINE(3739,v_sk.o_invoke_few_args("doEditSectionLink", 0x2F1D4ACEF0431135LL, 3, m_mTitle, v_sectionIndex, v_headlineHint)));
          }
        }
        else {
          (v_editlink = "");
        }
        v_head.set(v_headlineCount, ((assignCallTemp(eo_3, toObject(v_sk)),LINE(3746,eo_3.o_invoke_few_args("makeHeadline", 0x10AB357B787963CALL, 6, v_level, lval(v_matches.lvalAt("attrib", 0x66693EBBD88E0435LL)).refvalAt(v_headlineCount), v_anchor, v_headline, v_editlink, v_legacyAnchor)))));
        v_headlineCount++;
      }
    }
  }
  LINE(3751,t_setoutputtype(v_oldType));
  if (less(v_numVisible, 1LL)) {
    (v_enoughToc = false);
  }
  if (v_enoughToc) {
    if (more(v_prevtoclevel, 0LL) && less(v_prevtoclevel, gv_wgMaxTocLevel)) {
      concat_assign(v_toc, toString(LINE(3760,v_sk.o_invoke_few_args("tocUnindent", 0x093C3E54C9882291LL, 1, v_prevtoclevel - 1LL))));
    }
    (v_toc = LINE(3762,v_sk.o_invoke_few_args("tocList", 0x505D07AA0C1375D5LL, 1, v_toc)));
    LINE(3763,m_mOutput.o_invoke_few_args("setTOCHTML", 0x7207F5C225B9E59CLL, 1, v_toc));
  }
  if (v_isMain) {
    LINE(3767,m_mOutput.o_invoke_few_args("setSections", 0x6A6A6F940F0C36F5LL, 1, v_tocraw));
  }
  (v_blocks = LINE(3772,x_preg_split("/<H[1-6].*\?>.*\?<\\/H[1-6]>/i", v_text)));
  (v_i = 0LL);
  {
    LOOP_COUNTER(85);
    for (ArrayIterPtr iter87 = v_blocks.begin("parser"); !iter87->end(); iter87->next()) {
      LOOP_COUNTER_CHECK(85);
      v_block = iter87->second();
      {
        if (toBoolean(v_showEditLink) && more(v_headlineCount, 0LL) && equal(v_i, 0LL) && !same(v_block, "\n")) {
        }
        concat_assign(v_full, toString(v_block));
        if (v_enoughToc && !(toBoolean(v_i)) && v_isMain && !(toBoolean(o_get("mForceTocPosition", 0x00520DD93AF13A69LL)))) {
          (v_full = concat(toString(v_full), toString(v_toc)));
        }
        if (!(empty(v_head, v_i))) {
          concat_assign(v_full, toString(v_head.rvalAt(v_i)));
        }
        v_i++;
      }
    }
  }
  if (toBoolean(o_get("mForceTocPosition", 0x00520DD93AF13A69LL))) {
    return LINE(3796,x_str_replace("<!--MWTOC-->", v_toc, v_full));
  }
  else {
    return v_full;
  }
  return null;
} /* function */
/* SRC: Parser.php line 3814 */
Array c_parser::ti_mergesectiontrees(const char* cls, CArrRef v_tree1, CArrRef v_tree2, CVarRef v_section, Object v_title, CVarRef v_len2) {
  STATIC_METHOD_INJECTION(Parser, Parser::mergeSectionTrees);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Array v_newTree;
  Variant v_targetLevel;
  bool v_merged = false;
  Variant v_lastLevel;
  int64 v_nextIndex = 0;
  Variant v_numbering;
  Variant v_titletext;
  Variant v_s;
  Variant v_s2;

  (v_newTree = ScalarArrays::sa_[0]);
  (v_targetLevel = false);
  (v_merged = false);
  (v_lastLevel = 1LL);
  (v_nextIndex = 1LL);
  (v_numbering = ScalarArrays::sa_[26]);
  (v_titletext = LINE(3822,v_title->o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)));
  {
    LOOP_COUNTER(88);
    for (ArrayIter iter90 = v_tree1.begin("parser"); !iter90.end(); ++iter90) {
      LOOP_COUNTER_CHECK(88);
      v_s = iter90.second();
      {
        if (!same(v_targetLevel, false)) {
          if (not_more(v_s.rvalAt("level", 0x26B910814038EBA3LL), v_targetLevel)) (v_targetLevel = false);
          else continue;
        }
        if (!equal(v_s.rvalAt("index", 0x440D5888C0FF3081LL), v_section) || !equal(v_s.rvalAt("fromtitle", 0x3EDEEBB22A230184LL), v_titletext)) {
          LINE(3834,c_parser::t_incrementnumbering(ref(v_numbering), v_s.rvalAt("toclevel", 0x5CD76FFB746050EDLL), v_lastLevel));
          if (equal(v_s.rvalAt("fromtitle", 0x3EDEEBB22A230184LL), v_titletext)) {
            v_s.set("index", (v_nextIndex++), 0x440D5888C0FF3081LL);
            if (v_merged) lval(v_s.lvalAt("byteoffset", 0x6C263FBE5174D9CDLL)) += v_len2;
          }
          v_s.set("number", (LINE(3844,(assignCallTemp(eo_1, x_array_map(2, Array(ArrayInit(2).set(0, gv_wgContLang).set(1, "formatnum").create()), v_numbering)),x_implode(".", eo_1)))), 0x2E2E8BE57817A743LL);
          (v_lastLevel = v_s.rvalAt("toclevel", 0x5CD76FFB746050EDLL));
          v_newTree.append((v_s));
        }
        else {
          {
            LOOP_COUNTER(91);
            for (ArrayIter iter93 = v_tree2.begin("parser"); !iter93.end(); ++iter93) {
              LOOP_COUNTER_CHECK(91);
              v_s2 = iter93.second();
              {
                lval(v_s2.lvalAt("toclevel", 0x5CD76FFB746050EDLL)) += v_s.rvalAt("toclevel", 0x5CD76FFB746050EDLL) - 1LL;
                lval(v_s2.lvalAt("level", 0x26B910814038EBA3LL)) += v_s.rvalAt("level", 0x26B910814038EBA3LL) - 1LL;
                v_s2.set("index", (v_nextIndex++), 0x440D5888C0FF3081LL);
                lval(v_s2.lvalAt("byteoffset", 0x6C263FBE5174D9CDLL)) += v_s.rvalAt("byteoffset", 0x6C263FBE5174D9CDLL);
                LINE(3859,c_parser::t_incrementnumbering(ref(v_numbering), v_s2.rvalAt("toclevel", 0x5CD76FFB746050EDLL), v_lastLevel));
                v_s2.set("number", (LINE(3862,(assignCallTemp(eo_1, x_array_map(2, Array(ArrayInit(2).set(0, gv_wgContLang).set(1, "formatnum").create()), v_numbering)),x_implode(".", eo_1)))), 0x2E2E8BE57817A743LL);
                (v_lastLevel = v_s2.rvalAt("toclevel", 0x5CD76FFB746050EDLL));
                v_newTree.append((v_s2));
              }
            }
          }
          (v_targetLevel = v_s.rvalAt("level", 0x26B910814038EBA3LL));
          (v_merged = true);
        }
      }
    }
  }
  return v_newTree;
} /* function */
/* SRC: Parser.php line 3880 */
void c_parser::ti_incrementnumbering(const char* cls, Variant v_number, CVarRef v_level, CVarRef v_lastLevel) {
  STATIC_METHOD_INJECTION(Parser, Parser::incrementNumbering);
  DECLARE_GLOBAL_VARIABLES(g);
  Primitive v_key = 0;
  Variant v_unused;

  if (more(v_level, v_lastLevel)) v_number.set(v_level - 1LL, (1LL));
  else if (less(v_level, v_lastLevel)) {
    {
      LOOP_COUNTER(94);
      for (ArrayIterPtr iter96 = v_number.begin("parser"); !iter96->end(); iter96->next()) {
        LOOP_COUNTER_CHECK(94);
        v_unused = iter96->second();
        v_key = iter96->first();
        if (not_less(v_key, v_level)) v_number.weakRemove(v_key);
      }
    }
    lval(v_number.lvalAt(v_level - 1LL))++;
  }
  else lval(v_number.lvalAt(v_level - 1LL))++;
} /* function */
/* SRC: Parser.php line 3904 */
Variant c_parser::t_presavetransform(Variant v_text, CVarRef v_title, CVarRef v_user, CVarRef v_options, bool v_clearState //  = true
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::preSaveTransform);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Array v_pairs;

  (m_mOptions = v_options);
  LINE(3906,t_settitle(v_title));
  LINE(3907,t_setoutputtype(2LL /* parser::OT_WIKI */));
  if (v_clearState) {
    LINE(3910,t_clearstate());
  }
  (v_pairs = ScalarArrays::sa_[27]);
  (v_text = LINE(3916,(assignCallTemp(eo_0, x_array_keys(v_pairs)),assignCallTemp(eo_1, x_array_values(v_pairs)),assignCallTemp(eo_2, v_text),x_str_replace(eo_0, eo_1, eo_2))));
  (v_text = LINE(3917,t_pstpass2(v_text, v_user)));
  (v_text = LINE(3918,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_text)));
  return v_text;
} /* function */
/* SRC: Parser.php line 3926 */
Variant c_parser::t_pstpass2(Variant v_text, Variant v_user) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::pstPass2);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant &gv_wgLocaltimezone __attribute__((__unused__)) = g->GV(wgLocaltimezone);
  Variant v_ts;
  Variant v_tz;
  Variant v_unixts;
  Variant v_oldtz;
  Variant v_key;
  Variant v_value;
  String v_d;
  Variant v_sigText;
  Variant &gv_wgLegalTitleChars __attribute__((__unused__)) = g->GV(wgLegalTitleChars);
  String v_tc;
  String v_nc;
  String v_p1;
  String v_p4;
  String v_p3;
  String v_p2;
  Variant v_t;
  Variant v_m;

  {
  }
  (v_ts = LINE(3936,m_mOptions.o_invoke_few_args("getTimestamp", 0x7533D7D1FB463C81LL, 0)));
  (v_tz = LINE(3937,invoke_failed("wfmsgforcontent", Array(ArrayInit(1).set(0, "timezone-utc").create()), 0x0000000033607CCALL)));
  if (isset(gv_wgLocaltimezone)) {
    (v_unixts = LINE(3939,invoke_failed("wftimestamp", Array(ArrayInit(2).set(0, k_TS_UNIX).set(1, ref(v_ts)).create()), 0x00000000AADC1C20LL)));
    (v_oldtz = LINE(3940,x_getenv("TZ")));
    LINE(3941,x_putenv(concat("TZ=", toString(gv_wgLocaltimezone))));
    (v_ts = LINE(3942,x_date("YmdHis", toInt64(v_unixts))));
    (v_tz = LINE(3943,x_date("T", toInt64(v_unixts))));
    (v_key = concat("timezone-", LINE(3949,x_strtolower(x_trim(toString(v_tz))))));
    (v_value = LINE(3950,invoke_failed("wfmsgforcontent", Array(ArrayInit(1).set(0, ref(v_key)).create()), 0x0000000033607CCALL)));
    if (!(toBoolean(LINE(3951,invoke_failed("wfemptymsg", Array(ArrayInit(2).set(0, ref(v_key)).set(1, ref(v_value)).create()), 0x00000000ACF08022LL))))) (v_tz = v_value);
    LINE(3953,x_putenv(concat("TZ=", toString(v_oldtz))));
  }
  (v_d = concat_rev(LINE(3956,concat3(" (", toString(v_tz), ")")), toString(gv_wgContLang.o_invoke_few_args("timeanddate", 0x7A1040DE131E934BLL, 3, v_ts, false, false))));
  (v_text = LINE(3960,t_replacevariables(v_text)));
  (v_sigText = LINE(3963,t_getusersig(ref(v_user))));
  (v_text = LINE(3968,x_strtr(toString(v_text), (assignCallTemp(eo_0, v_d),assignCallTemp(eo_1, LINE(3966,concat3(toString(v_sigText), " ", v_d))),assignCallTemp(eo_2, v_sigText),Array(ArrayInit(3).set(0, "~~~~~", eo_0, 0x3D5058E7395417B2LL).set(1, "~~~~", eo_1, 0x691A9EDFAB83F804LL).set(2, "~~~", eo_2, 0x3C75BFED7FEA008DLL).create())))));
  (v_tc = LINE(3973,concat3("[", toString(gv_wgLegalTitleChars), "]")));
  (v_nc = "[ _0-9A-Za-z\\x80-\\xff-]");
  (v_p1 = concat("/\\[\\[(:\?", LINE(3976,concat6(v_nc, "+:|:|)(", v_tc, "+\?)( \\(", v_tc, "+\\))\\|]]/"))));
  (v_p4 = concat("/\\[\\[(:\?", LINE(3977,concat6(v_nc, "+:|:|)(", v_tc, "+\?)(\357\274\210", v_tc, "+\357\274\211)\\|]]/"))));
  (v_p3 = concat_rev(LINE(3978,concat6(v_tc, "+\?)( \\(", v_tc, "+\\)|)(, ", v_tc, "+|)\\|]]/")), concat3("/\\[\\[(:\?", v_nc, "+:|:|)(")));
  (v_p2 = LINE(3979,concat3("/\\[\\[\\|(", v_tc, "+)]]/")));
  (v_text = LINE(3982,x_preg_replace(v_p1, "[[\\1\\2\\3|\\2]]", v_text)));
  (v_text = LINE(3983,x_preg_replace(v_p4, "[[\\1\\2\\3|\\2]]", v_text)));
  (v_text = LINE(3984,x_preg_replace(v_p3, "[[\\1\\2\\3\\4|\\2]]", v_text)));
  (v_t = LINE(3986,m_mTitle.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0)));
  (v_m = ScalarArrays::sa_[0]);
  if (toBoolean(LINE(3988,(assignCallTemp(eo_0, concat("/^(", concat6(v_nc, "+:|)", v_tc, "+\?( \\(", v_tc, "+\\))$/"))),assignCallTemp(eo_1, toString(v_t)),assignCallTemp(eo_2, ref(v_m)),x_preg_match(eo_0, eo_1, eo_2))))) {
    (v_text = LINE(3989,(assignCallTemp(eo_0, v_p2),assignCallTemp(eo_1, concat5("[[", toString(v_m.rvalAt(1, 0x5BCA7C69B794F8CELL)), "\\1", toString(v_m.rvalAt(2, 0x486AFCC090D5F98CLL)), "|\\1]]")),assignCallTemp(eo_2, v_text),x_preg_replace(eo_0, eo_1, eo_2))));
  }
  else if (toBoolean(LINE(3990,(assignCallTemp(eo_0, concat("/^(", concat6(v_nc, "+:|)", v_tc, "+\?(, ", v_tc, "+|)$/"))),assignCallTemp(eo_1, toString(v_t)),assignCallTemp(eo_2, ref(v_m)),x_preg_match(eo_0, eo_1, eo_2)))) && !equal("", toString(v_m.rvalAt(1, 0x5BCA7C69B794F8CELL)) + toString(v_m.rvalAt(2, 0x486AFCC090D5F98CLL)))) {
    (v_text = LINE(3991,(assignCallTemp(eo_0, v_p2),assignCallTemp(eo_1, concat5("[[", toString(v_m.rvalAt(1, 0x5BCA7C69B794F8CELL)), "\\1", toString(v_m.rvalAt(2, 0x486AFCC090D5F98CLL)), "|\\1]]")),assignCallTemp(eo_2, v_text),x_preg_replace(eo_0, eo_1, eo_2))));
  }
  else {
    (v_text = LINE(3994,x_preg_replace(v_p2, "[[\\1]]", v_text)));
  }
  (v_text = LINE(3998,x_rtrim(toString(v_text))));
  return v_text;
} /* function */
/* SRC: Parser.php line 4012 */
Variant c_parser::t_getusersig(Variant v_user, Variant v_nickname //  = false
, Variant v_fancySig //  = null
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getUserSig);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgMaxSigChars __attribute__((__unused__)) = g->GV(wgMaxSigChars);
  Variant v_username;
  Variant v_userText;
  Variant v_nickText;

  (v_username = LINE(4015,toObject(v_user)->o_invoke_few_args("getName", 0x23F51CDECC198965LL, 0)));
  if (same(v_nickname, false)) (v_nickname = LINE(4019,toObject(v_user)->o_invoke_few_args("getOption", 0x402DD1A85CAEA6C0LL, 1, "nickname")));
  if (LINE(4021,x_is_null(v_fancySig))) (v_fancySig = LINE(4022,toObject(v_user)->o_invoke_few_args("getBoolOption", 0x78BE72AD8722ADA5LL, 1, "fancysig")));
  (v_nickname = equal(v_nickname, null) ? ((Variant)(v_username)) : ((Variant)(v_nickname)));
  if (more(LINE(4026,x_mb_strlen(toString(v_nickname))), gv_wgMaxSigChars)) {
    (v_nickname = v_username);
    LINE(4028,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, concat("Parser::getUserSig", concat3(": ", toString(v_username), " has overlong signature.\n"))).create()), 0x00000000E441E905LL));
  }
  else if (!same(v_fancySig, false)) {
    if (!same(LINE(4031,t_validatesig(v_nickname)), false)) {
      return LINE(4033,t_cleansig(v_nickname, true));
    }
    else {
      (v_nickname = v_username);
      LINE(4037,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, concat("Parser::getUserSig", concat3(": ", toString(v_username), " has bad XML tags in signature.\n"))).create()), 0x00000000E441E905LL));
    }
  }
  (v_nickname = LINE(4042,t_cleansiginsig(v_nickname)));
  (v_userText = LINE(4045,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(v_username)).create()), 0x00000000B3382909LL)));
  (v_nickText = LINE(4046,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(v_nickname)).create()), 0x00000000B3382909LL)));
  if (toBoolean(LINE(4047,toObject(v_user)->o_invoke_few_args("isAnon", 0x1F729AB8C6F7FE1ALL, 0)))) {
    return LINE(4048,invoke_failed("wfmsgext", Array(ArrayInit(4).set(0, "signature-anon").set(1, Array(ArrayInit(2).set(0, "content").set(1, "parsemag").create())).set(2, ref(v_userText)).set(3, ref(v_nickText)).create()), 0x000000001BB87306LL));
  }
  else {
    return LINE(4050,invoke_failed("wfmsgext", Array(ArrayInit(4).set(0, "signature").set(1, Array(ArrayInit(2).set(0, "content").set(1, "parsemag").create())).set(2, ref(v_userText)).set(3, ref(v_nickText)).create()), 0x000000001BB87306LL));
  }
  return null;
} /* function */
/* SRC: Parser.php line 4060 */
Variant c_parser::t_validatesig(CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::validateSig);
  return (toBoolean(LINE(4061,throw_fatal("unknown class xml", ((void*)NULL)))) ? ((Variant)(v_text)) : ((Variant)(false)));
} /* function */
/* SRC: Parser.php line 4074 */
Variant c_parser::t_cleansig(Variant v_text, bool v_parsing //  = false
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::cleanSig);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgTitle __attribute__((__unused__)) = g->GV(wgTitle);
  Variant v_wgTitle;
  Variant v_substWord;
  String v_substRegex;
  String v_substText;
  Variant v_dom;
  Variant v_frame;

  if (!(v_parsing)) {
    v_wgTitle = ref(g->GV(wgTitle));
    LINE(4077,t_clearstate());
    LINE(4078,t_settitle(v_wgTitle));
    (m_mOptions = ((Object)(LINE(4079,p_parseroptions(p_parseroptions(NEWOBJ(c_parseroptions)())->create())))));
    (o_lval("setOutputType", 0x6AC9CEC755620241LL) = 3LL /* parser::OT_PREPROCESS */);
  }
  if (!(toBoolean(LINE(4084,m_mOptions.o_invoke_few_args("getCleanSignatures", 0x49A4385734AC6E73LL, 0))))) {
    return v_text;
  }
  (v_substWord = LINE(4090,throw_fatal("unknown class magicword", ((void*)NULL))));
  (v_substRegex = concat_rev(toString(LINE(4091,v_substWord.o_invoke_few_args("getRegexCase", 0x06D2F946031AE77ALL, 0))), (assignCallTemp(eo_1, toString(v_substWord.o_invoke_few_args("getBaseRegex", 0x784B3CC2A5690212LL, 0))),concat3("/\\{\\{(\?!(\?:", eo_1, "))/x"))));
  (v_substText = concat("{{", toString(LINE(4092,v_substWord.o_invoke_few_args("getSynonym", 0x389984D2586DEE52LL, 1, 0LL)))));
  (v_text = LINE(4094,x_preg_replace(v_substRegex, v_substText, v_text)));
  (v_text = LINE(4095,t_cleansiginsig(v_text)));
  (v_dom = LINE(4096,t_preprocesstodom(v_text)));
  (v_frame = (assignCallTemp(eo_0, toObject(LINE(4097,t_getpreprocessor()))),eo_0.o_invoke_few_args("newFrame", 0x787B6141D7721675LL, 0)));
  (v_text = LINE(4098,v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_dom)));
  if (!(v_parsing)) {
    (v_text = LINE(4101,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_text)));
  }
  return v_text;
} /* function */
/* SRC: Parser.php line 4112 */
Variant c_parser::t_cleansiginsig(Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::cleanSigInSig);
  (v_text = LINE(4113,x_preg_replace("/~{3,5}/", "", v_text)));
  return v_text;
} /* function */
/* SRC: Parser.php line 4122 */
void c_parser::t_startexternalparse(Variant v_title, CVarRef v_options, CVarRef v_outputType, bool v_clearState //  = true
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::startExternalParse);
  LINE(4123,t_settitle(v_title));
  (m_mOptions = v_options);
  LINE(4125,t_setoutputtype(v_outputType));
  if (v_clearState) {
    LINE(4127,t_clearstate());
  }
} /* function */
/* SRC: Parser.php line 4139 */
Variant c_parser::t_transformmsg(Variant v_text, CVarRef v_options) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::transformMsg);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgTitle __attribute__((__unused__)) = g->GV(wgTitle);
  Variant &sv_executing __attribute__((__unused__)) = g->sv_parser$$transformmsg$$executing.lvalAt(this->o_getClassName());
  Variant &inited_sv_executing __attribute__((__unused__)) = g->inited_sv_parser$$transformmsg$$executing.lvalAt(this->o_getClassName());
  if (!inited_sv_executing) {
    (sv_executing = false);
    inited_sv_executing = true;
  }
  if (toBoolean(sv_executing)) {
    return v_text;
  }
  (sv_executing = true);
  LINE(4149,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::transformMsg").create()), 0x0000000075359BAFLL));
  (v_text = LINE(4150,t_preprocess(v_text, gv_wgTitle, v_options)));
  (sv_executing = false);
  LINE(4153,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::transformMsg").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: Parser.php line 4172 */
Variant c_parser::t_sethook(String v_tag, CArrRef v_callback) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::setHook);
  Variant v_oldVal;

  (v_tag = LINE(4173,x_strtolower(v_tag)));
  (v_oldVal = isset(m_mTagHooks, v_tag) ? ((Variant)(m_mTagHooks.rvalAt(v_tag))) : ((Variant)(null)));
  m_mTagHooks.set(v_tag, (v_callback));
  if (!(LINE(4176,x_in_array(v_tag, m_mStripList)))) {
    m_mStripList.append((v_tag));
  }
  return v_oldVal;
} /* function */
/* SRC: Parser.php line 4183 */
Variant c_parser::t_settransparenttaghook(String v_tag, CVarRef v_callback) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::setTransparentTagHook);
  Variant v_oldVal;

  (v_tag = LINE(4184,x_strtolower(v_tag)));
  (v_oldVal = isset(m_mTransparentTagHooks, v_tag) ? ((Variant)(m_mTransparentTagHooks.rvalAt(v_tag))) : ((Variant)(null)));
  m_mTransparentTagHooks.set(v_tag, (v_callback));
  return v_oldVal;
} /* function */
/* SRC: Parser.php line 4194 */
void c_parser::t_cleartaghooks() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::clearTagHooks);
  (m_mTagHooks = ScalarArrays::sa_[0]);
  (m_mStripList = m_mDefaultStripList);
} /* function */
/* SRC: Parser.php line 4243 */
Variant c_parser::t_setfunctionhook(CVarRef v_id, CVarRef v_callback, CVarRef v_flags //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::setFunctionHook);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_oldVal;
  Variant v_mw;
  Variant v_synonyms;
  int64 v_sensitive = 0;
  Variant v_syn;

  (v_oldVal = isset(m_mFunctionHooks, v_id) ? ((Variant)(m_mFunctionHooks.rvalAt(v_id).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) : ((Variant)(null)));
  m_mFunctionHooks.set(v_id, (Array(ArrayInit(2).set(0, v_callback).set(1, v_flags).create())));
  (v_mw = LINE(4250,throw_fatal("unknown class magicword", ((void*)NULL))));
  if (!(toBoolean(v_mw))) throw_exception(LINE(4252,create_object("mwexception", Array(ArrayInit(1).set(0, "Parser::setFunctionHook() expecting a magic word identifier.").create()))));
  (v_synonyms = LINE(4254,v_mw.o_invoke_few_args("getSynonyms", 0x44E19B4C6E0A744FLL, 0)));
  (v_sensitive = LINE(4255,x_intval(v_mw.o_invoke_few_args("isCaseSensitive", 0x6BB73460F5ADBF0DLL, 0))));
  {
    LOOP_COUNTER(97);
    for (ArrayIterPtr iter99 = v_synonyms.begin("parser"); !iter99->end(); iter99->next()) {
      LOOP_COUNTER_CHECK(97);
      v_syn = iter99->second();
      {
        if (!(toBoolean(v_sensitive))) {
          (v_syn = LINE(4260,gv_wgContLang.o_invoke_few_args("lc", 0x0191205BE4FF8F9CLL, 1, v_syn)));
        }
        if (!((toBoolean(bitwise_and(v_flags, k_SFH_NO_HASH))))) {
          (v_syn = concat("#", toString(v_syn)));
        }
        if (same(LINE(4267,x_substr(toString(v_syn), toInt32(-1LL), toInt32(1LL))), ":")) {
          (v_syn = LINE(4268,x_substr(toString(v_syn), toInt32(0LL), toInt32(-1LL))));
        }
        lval(m_mFunctionSynonyms.lvalAt(v_sensitive)).set(v_syn, (v_id));
      }
    }
  }
  return v_oldVal;
} /* function */
/* SRC: Parser.php line 4280 */
Variant c_parser::t_getfunctionhooks() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getFunctionHooks);
  return LINE(4281,x_array_keys(m_mFunctionHooks));
} /* function */
/* SRC: Parser.php line 4289 */
Variant c_parser::t_setfunctiontaghook(String v_tag, CVarRef v_callback, CVarRef v_flags) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::setFunctionTagHook);
  Variant v_old;

  (v_tag = LINE(4290,x_strtolower(v_tag)));
  (v_old = isset(m_mFunctionTagHooks, v_tag) ? ((Variant)(m_mFunctionTagHooks.rvalAt(v_tag))) : ((Variant)(null)));
  m_mFunctionTagHooks.set(v_tag, (Array(ArrayInit(2).set(0, v_callback).set(1, v_flags).create())));
  if (!(LINE(4295,x_in_array(v_tag, m_mStripList)))) {
    m_mStripList.append((v_tag));
  }
  return v_old;
} /* function */
/* SRC: Parser.php line 4308 */
Variant c_parser::t_replacelinkholders(Variant v_text, int64 v_options //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::replaceLinkHolders);
  return LINE(4309,m_mLinkHolders.o_invoke_few_args("replace", 0x42FAC655280A6146LL, 1, v_text));
} /* function */
/* SRC: Parser.php line 4318 */
Variant c_parser::t_replacelinkholderstext(Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::replaceLinkHoldersText);
  return LINE(4319,m_mLinkHolders.o_invoke_few_args("replaceText", 0x316E7B92ECA74146LL, 1, v_text));
} /* function */
/* SRC: Parser.php line 4325 */
String c_parser::t_renderpretag(CVarRef v_text, Variant v_attribs) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::renderPreTag);
  Variant v_content;

  (v_content = LINE(4327,throw_fatal("unknown class stringutils", ((void*)NULL))));
  (v_attribs = LINE(4329,throw_fatal("unknown class sanitizer", ((void*)NULL))));
  return concat(concat_rev(toString(LINE(4331,throw_fatal("unknown class xml", ((void*)NULL)))), toString(LINE(4330,throw_fatal("unknown class xml", ((void*)NULL))))), "</pre>");
} /* function */
/* SRC: Parser.php line 4339 */
Variant c_parser::t_renderhyperlink(CVarRef v_text, Variant v_params, CVarRef v_frame //  = false
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::renderHyperlink);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Primitive v_name = 0;
  Variant v_value;
  Variant v_whitelist;
  Variant v_content;
  Variant v_href;
  Variant v_sk;
  Variant v_html;

  {
    LOOP_COUNTER(100);
    for (ArrayIterPtr iter102 = v_params.begin("parser"); !iter102->end(); iter102->next()) {
      LOOP_COUNTER_CHECK(100);
      v_value = iter102->second();
      v_name = iter102->first();
      {
        v_params.set(v_name, (LINE(4341,t_replacevariables(v_value, v_frame))));
      }
    }
  }
  (v_whitelist = LINE(4344,throw_fatal("unknown class sanitizer", ((void*)NULL))));
  (v_params = LINE(4345,throw_fatal("unknown class sanitizer", ((void*)NULL))));
  (v_content = (assignCallTemp(eo_0, LINE(4347,x_trim(toString(v_text)))),assignCallTemp(eo_1, v_frame),t_recursivetagparse(eo_0, eo_1)));
  if (isset(v_params, "href", 0x6268F8493692A594LL)) {
    (v_href = v_params.rvalAt("href", 0x6268F8493692A594LL));
    LINE(4351,m_mOutput.o_invoke_few_args("addExternalLink", 0x3FE22FCE3F0B40E8LL, 1, v_href));
    v_params.weakRemove("href", 0x6268F8493692A594LL);
  }
  else {
    return concat_rev(toString(LINE(4355,throw_fatal("unknown class xml", ((void*)NULL)))), concat(toString(throw_fatal("unknown class xml", ((void*)NULL))), toString(v_content)));
  }
  (v_sk = LINE(4358,m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_html = LINE(4359,v_sk.o_invoke_few_args("makeExternalLink", 0x29CE649D13E77D3BLL, 5, v_href, v_content, false, "", v_params)));
  return v_html;
} /* function */
/* SRC: Parser.php line 4373 */
Variant c_parser::t_renderimagegallery(CVarRef v_text, Variant v_params) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::renderImageGallery);
  Variant v_ig;
  Variant v_caption;
  Variant v_lines;
  Variant v_line;
  Variant v_matches;
  Variant v_tp;
  Variant v_nt;
  Variant v_label;
  Variant v_html;

  (v_ig = LINE(4374,create_object("imagegallery", Array())));
  LINE(4375,v_ig.o_invoke_few_args("setContextTitle", 0x715C3E5638D42EC2LL, 1, m_mTitle));
  LINE(4376,v_ig.o_invoke_few_args("setShowBytes", 0x0E49F4BE615F8AAELL, 1, false));
  LINE(4377,v_ig.o_invoke_few_args("setShowFilename", 0x51C3E2A67C228FB2LL, 1, false));
  LINE(4378,v_ig.o_invoke_few_args("setParser", 0x7C0461AE60A73E32LL, 1, this));
  LINE(4379,v_ig.o_invoke_few_args("setHideBadImages", 0x2748FD6B960E0216LL, 0));
  LINE(4380,v_ig.o_invoke_few_args("setAttributes", 0x4D430E2CD85B314FLL, 1, throw_fatal("unknown class sanitizer", ((void*)NULL))));
  LINE(4381,v_ig.o_invoke_few_args("useSkin", 0x7BB4997C486DBF00LL, 1, m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_ig.o_lval("mRevisionId", 0x4DDE8DD427CE5241LL) = m_mRevisionId);
  if (isset(v_params, "caption", 0x10826816B41AADFDLL)) {
    (v_caption = v_params.rvalAt("caption", 0x10826816B41AADFDLL));
    (v_caption = LINE(4386,x_htmlspecialchars(toString(v_caption))));
    (v_caption = LINE(4387,t_replaceinternallinks(v_caption)));
    LINE(4388,v_ig.o_invoke_few_args("setCaptionHtml", 0x289F9CA74CEDB630LL, 1, v_caption));
  }
  if (isset(v_params, "perrow", 0x539812D4451C4CAALL)) {
    LINE(4391,v_ig.o_invoke_few_args("setPerRow", 0x528ABEBB0D8573D0LL, 1, v_params.refvalAt("perrow", 0x539812D4451C4CAALL)));
  }
  if (isset(v_params, "widths", 0x5BB134DE7AC6735FLL)) {
    LINE(4394,v_ig.o_invoke_few_args("setWidths", 0x654010992C45CD8FLL, 1, v_params.refvalAt("widths", 0x5BB134DE7AC6735FLL)));
  }
  if (isset(v_params, "heights", 0x3FD1AEF3EFB8D2BELL)) {
    LINE(4397,v_ig.o_invoke_few_args("setHeights", 0x39021A7987133C70LL, 1, v_params.refvalAt("heights", 0x3FD1AEF3EFB8D2BELL)));
  }
  LINE(4400,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "BeforeParserrenderImageGallery").set(1, Array(ArrayInit(2).setRef(0, ref(this)).setRef(1, ref(v_ig)).create())).create()), 0x00000000787A96B2LL));
  (v_lines = LINE(4402,throw_fatal("unknown class stringutils", ((void*)NULL))));
  {
    LOOP_COUNTER(103);
    for (ArrayIterPtr iter105 = v_lines.begin("parser"); !iter105->end(); iter105->next()) {
      LOOP_COUNTER_CHECK(103);
      v_line = iter105->second();
      {
        (v_matches = ScalarArrays::sa_[0]);
        LINE(4407,x_preg_match("/^([^|]+)(\\|(.*))\?$/", toString(v_line), ref(v_matches)));
        if (equal(LINE(4409,x_count(v_matches)), 0LL)) {
          continue;
        }
        if (!same(LINE(4413,x_strpos(toString(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "%")), false)) v_matches.set(1LL, (LINE(4414,x_urldecode(toString(v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL))))), 0x5BCA7C69B794F8CELL);
        (v_tp = LINE(4415,throw_fatal("unknown class title", ((void*)NULL))));
        (v_nt = ref(v_tp));
        if (LINE(4417,x_is_null(v_nt))) {
          continue;
        }
        if (isset(v_matches, 3LL, 0x135FDDF6A6BFBBDDLL)) {
          (v_label = v_matches.rvalAt(3LL, 0x135FDDF6A6BFBBDDLL));
        }
        else {
          (v_label = "");
        }
        (v_html = LINE(4427,t_recursivetagparse(x_trim(toString(v_label)))));
        LINE(4429,v_ig.o_invoke_few_args("add", 0x15D34462FC79458BLL, 2, v_nt, v_html));
        if (equal(LINE(4432,v_nt.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)), k_NS_FILE)) {
          LINE(4433,m_mOutput.o_invoke_few_args("addImage", 0x1026F251B81BD927LL, 1, v_nt.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)));
        }
      }
    }
  }
  return LINE(4436,v_ig.o_invoke_few_args("toHTML", 0x112305CB9B1971A9LL, 0));
} /* function */
/* SRC: Parser.php line 4439 */
Array c_parser::t_getimageparams(CVarRef v_handler) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getImageParams);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_handlerClass;
  Variant &sv_internalParamNames __attribute__((__unused__)) = g->sv_parser$$getimageparams$$internalParamNames.lvalAt(this->o_getClassName());
  Variant &inited_sv_internalParamNames __attribute__((__unused__)) = g->inited_sv_parser$$getimageparams$$internalParamNames.lvalAt(this->o_getClassName());
  Variant v_internalParamNames;
  Variant &sv_internalParamMap __attribute__((__unused__)) = g->sv_parser$$getimageparams$$internalParamMap.lvalAt(this->o_getClassName());
  Variant &inited_sv_internalParamMap __attribute__((__unused__)) = g->inited_sv_parser$$getimageparams$$internalParamMap.lvalAt(this->o_getClassName());
  Variant v_internalParamMap;
  Primitive v_type = 0;
  Variant v_names;
  Variant v_name;
  Variant v_magicName;
  Variant v_paramMap;
  Variant v_handlerParamMap;
  Primitive v_magic = 0;
  Variant v_paramName;

  if (toBoolean(v_handler)) {
    (v_handlerClass = LINE(4441,x_get_class(v_handler)));
  }
  else {
    (v_handlerClass = "");
  }
  if (!(isset(m_mImageParams, v_handlerClass))) {
    v_internalParamNames = ref(sv_internalParamNames);
    if (!inited_sv_internalParamNames) {
      (v_internalParamNames = ScalarArrays::sa_[28]);
      inited_sv_internalParamNames = true;
    }
    v_internalParamMap = ref(sv_internalParamMap);
    if (!inited_sv_internalParamMap) {
      (v_internalParamMap = null);
      inited_sv_internalParamMap = true;
    }
    if (!(toBoolean(v_internalParamMap))) {
      (v_internalParamMap = ScalarArrays::sa_[0]);
      {
        LOOP_COUNTER(106);
        for (ArrayIterPtr iter108 = v_internalParamNames.begin("parser"); !iter108->end(); iter108->next()) {
          LOOP_COUNTER_CHECK(106);
          v_names = iter108->second();
          v_type = iter108->first();
          {
            {
              LOOP_COUNTER(109);
              for (ArrayIterPtr iter111 = v_names.begin("parser"); !iter111->end(); iter111->next()) {
                LOOP_COUNTER_CHECK(109);
                v_name = iter111->second();
                {
                  (v_magicName = LINE(4459,x_str_replace("-", "_", toString("img_") + toString(v_name))));
                  v_internalParamMap.set(v_magicName, (Array(ArrayInit(2).set(0, v_type).set(1, v_name).create())));
                }
              }
            }
          }
        }
      }
    }
    (v_paramMap = v_internalParamMap);
    if (toBoolean(v_handler)) {
      (v_handlerParamMap = LINE(4468,toObject(v_handler)->o_invoke_few_args("getParamMap", 0x28D8A47A2E38229ALL, 0)));
      {
        LOOP_COUNTER(112);
        for (ArrayIterPtr iter114 = v_handlerParamMap.begin("parser"); !iter114->end(); iter114->next()) {
          LOOP_COUNTER_CHECK(112);
          v_paramName = iter114->second();
          v_magic = iter114->first();
          {
            v_paramMap.set(v_magic, (Array(ArrayInit(2).set(0, "handler").set(1, v_paramName).create())));
          }
        }
      }
    }
    m_mImageParams.set(v_handlerClass, (v_paramMap));
    m_mImageParamsMagicArray.set(v_handlerClass, (LINE(4474,create_object("magicwordarray", Array(ArrayInit(1).set(0, x_array_keys(v_paramMap)).create())))));
  }
  return Array(ArrayInit(2).set(0, m_mImageParams.rvalAt(v_handlerClass)).set(1, m_mImageParamsMagicArray.rvalAt(v_handlerClass)).create());
} /* function */
/* SRC: Parser.php line 4485 */
Variant c_parser::t_makeimage(Variant v_title, CVarRef v_options, CVarRef v_holders //  = false
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::makeImage);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_parts;
  Variant v_sk;
  Variant v_descQuery;
  Variant v_time;
  Variant v_skip;
  Variant v_imagename;
  Variant v_file;
  Variant v_handler;
  Variant v_paramMap;
  Variant v_mwArray;
  Variant v_caption;
  Variant v_params;
  Variant v_part;
  Variant v_magicName;
  Variant v_value;
  Variant v_validated;
  Variant v_type;
  Variant v_paramName;
  Variant v_m;
  Variant v_width;
  Variant v_height;
  String v_chars;
  Variant v_prots;
  Variant v_linkTitle;
  bool v_imageIsFramed = false;
  Variant v_ret;

  (v_parts = LINE(4511,throw_fatal("unknown class stringutils", ((void*)NULL))));
  (v_sk = LINE(4512,m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_skip = (v_time = (v_descQuery = false)));
  LINE(4516,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "BeforeParserMakeImageLinkObj").set(1, Array(ArrayInit(5).setRef(0, ref(this)).setRef(1, ref(v_title)).setRef(2, ref(v_skip)).setRef(3, ref(v_time)).setRef(4, ref(v_descQuery)).create())).create()), 0x00000000787A96B2LL));
  if (toBoolean(v_skip)) {
    return LINE(4519,v_sk.o_invoke_few_args("link", 0x230FE1D6EC599525LL, 1, v_title));
  }
  (v_imagename = LINE(4523,v_title.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)));
  (v_file = LINE(4524,invoke_failed("wffindfile", Array(ArrayInit(2).set(0, ref(v_title)).set(1, Array(ArrayInit(1).set(0, "time", v_time, 0x46C13C417DE7E0A0LL).create())).create()), 0x000000008D7BE104LL)));
  (v_handler = toBoolean(v_file) ? ((Variant)(LINE(4526,v_file.o_invoke_few_args("getHandler", 0x179039CC09D3FA6FLL, 0)))) : ((Variant)(false)));
  df_lambda_15(LINE(4528,t_getimageparams(v_handler)), v_paramMap, v_mwArray);
  (v_caption = "");
  (v_params = ScalarArrays::sa_[29]);
  {
    LOOP_COUNTER(115);
    for (ArrayIterPtr iter117 = v_parts.begin("parser"); !iter117->end(); iter117->next()) {
      LOOP_COUNTER_CHECK(115);
      v_part = iter117->second();
      {
        (v_part = LINE(4535,x_trim(toString(v_part))));
        df_lambda_16(LINE(4536,v_mwArray.o_invoke_few_args("matchVariableStartToEnd", 0x26349809C1925B1FLL, 1, v_part)), v_magicName, v_value);
        (v_validated = false);
        if (isset(v_paramMap, v_magicName)) {
          df_lambda_17(v_paramMap.rvalAt(v_magicName), v_type, v_paramName);
          if (same(v_type, "handler") && same(v_paramName, "width")) {
            (v_m = ScalarArrays::sa_[0]);
            if (toBoolean(LINE(4546,x_preg_match("/^([0-9]*)x([0-9]*)\\s*(\?:px)\?\\s*$/", toString(v_value), ref(v_m))))) {
              (v_width = LINE(4547,x_intval(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL))));
              (v_height = LINE(4548,x_intval(v_m.rvalAt(2LL, 0x486AFCC090D5F98CLL))));
              if (toBoolean(LINE(4549,v_handler.o_invoke_few_args("validateParam", 0x42F66BC0EDD952A0LL, 2, "width", v_width)))) {
                lval(v_params.lvalAt(v_type)).set("width", (v_width), 0x79CE5ED9259796ADLL);
                (v_validated = true);
              }
              if (toBoolean(LINE(4553,v_handler.o_invoke_few_args("validateParam", 0x42F66BC0EDD952A0LL, 2, "height", v_height)))) {
                lval(v_params.lvalAt(v_type)).set("height", (v_height), 0x674F0FBB080B0779LL);
                (v_validated = true);
              }
            }
            else if (toBoolean(LINE(4557,x_preg_match("/^[0-9]*\\s*(\?:px)\?\\s*$/", toString(v_value))))) {
              (v_width = LINE(4558,x_intval(v_value)));
              if (toBoolean(LINE(4559,v_handler.o_invoke_few_args("validateParam", 0x42F66BC0EDD952A0LL, 2, "width", v_width)))) {
                lval(v_params.lvalAt(v_type)).set("width", (v_width), 0x79CE5ED9259796ADLL);
                (v_validated = true);
              }
            }
          }
          else {
            if (same(v_type, "handler")) {
              (v_validated = LINE(4567,v_handler.o_invoke_few_args("validateParam", 0x42F66BC0EDD952A0LL, 2, v_paramName, v_value)));
            }
            else {
              {
                Variant tmp119 = (v_paramName);
                int tmp120 = -1;
                if (equal(tmp119, ("manualthumb"))) {
                  tmp120 = 0;
                } else if (equal(tmp119, ("alt"))) {
                  tmp120 = 1;
                } else if (equal(tmp119, ("link"))) {
                  tmp120 = 2;
                } else if (true) {
                  tmp120 = 3;
                }
                switch (tmp120) {
                case 0:
                  {
                  }
                case 1:
                  {
                    (v_validated = true);
                    (v_value = LINE(4577,t_stripalttext(v_value, v_holders)));
                    goto break118;
                  }
                case 2:
                  {
                    (v_chars = "[^][<>\"\\x00-\\x20\\x7F]" /* parser::EXT_LINK_URL_CLASS */);
                    (v_prots = m_mUrlProtocols);
                    if (same(v_value, "")) {
                      (v_paramName = "no-link");
                      (v_value = true);
                      (v_validated = true);
                    }
                    else if (toBoolean(LINE(4586,(assignCallTemp(eo_0, concat3("/^", toString(v_prots), "/")),assignCallTemp(eo_1, toString(v_value)),x_preg_match(eo_0, eo_1))))) {
                      if (toBoolean(LINE(4587,(assignCallTemp(eo_0, concat5("/^(", toString(v_prots), ")", v_chars, "+$/")),assignCallTemp(eo_1, toString(v_value)),assignCallTemp(eo_2, ref(v_m)),x_preg_match(eo_0, eo_1, eo_2))))) {
                        (v_paramName = "link-url");
                        LINE(4589,m_mOutput.o_invoke_few_args("addExternalLink", 0x3FE22FCE3F0B40E8LL, 1, v_value));
                        (v_validated = true);
                      }
                    }
                    else {
                      (v_linkTitle = LINE(4593,throw_fatal("unknown class title", ((void*)NULL))));
                      if (toBoolean(v_linkTitle)) {
                        (v_paramName = "link-title");
                        (v_value = v_linkTitle);
                        LINE(4597,m_mOutput.o_invoke_few_args("addLink", 0x5D10AC6ADCA895ACLL, 1, v_linkTitle));
                        (v_validated = true);
                      }
                    }
                    goto break118;
                  }
                case 3:
                  {
                    (v_validated = (same(v_value, false) || LINE(4604,x_is_numeric(x_trim(toString(v_value))))));
                  }
                }
                break118:;
              }
            }
            if (toBoolean(v_validated)) {
              lval(v_params.lvalAt(v_type)).set(v_paramName, (v_value));
            }
          }
        }
        if (!(toBoolean(v_validated))) {
          (v_caption = v_part);
        }
      }
    }
  }
  if (toBoolean(v_params.rvalAt("horizAlign", 0x0C9554788E4B4CB7LL))) {
    lval(v_params.lvalAt("frame", 0x2D577BEBE8CB9D1FLL)).set("align", (LINE(4620,x_key(ref(lval(v_params.lvalAt("horizAlign", 0x0C9554788E4B4CB7LL)))))), 0x0088E6A851076124LL);
  }
  if (toBoolean(v_params.rvalAt("vertAlign", 0x58EF5364C4E21BF6LL))) {
    lval(v_params.lvalAt("frame", 0x2D577BEBE8CB9D1FLL)).set("valign", (LINE(4623,x_key(ref(lval(v_params.lvalAt("vertAlign", 0x58EF5364C4E21BF6LL)))))), 0x1AC1065EEC01D355LL);
  }
  lval(v_params.lvalAt("frame", 0x2D577BEBE8CB9D1FLL)).set("caption", (v_caption), 0x10826816B41AADFDLL);
  (v_imageIsFramed = isset(v_params.rvalAt("frame", 0x2D577BEBE8CB9D1FLL), "frame", 0x2D577BEBE8CB9D1FLL) || isset(v_params.rvalAt("frame", 0x2D577BEBE8CB9D1FLL), "framed", 0x3AC627E741084D09LL) || isset(v_params.rvalAt("frame", 0x2D577BEBE8CB9D1FLL), "thumbnail", 0x0EA6B2C8F3B06CAFLL) || isset(v_params.rvalAt("frame", 0x2D577BEBE8CB9D1FLL), "manualthumb", 0x7049AB520F6C7D3ELL));
  if (v_imageIsFramed) {
    if (same(v_caption, "") && !(isset(v_params.rvalAt("frame", 0x2D577BEBE8CB9D1FLL), "alt", 0x2AB21AF4F5B8471BLL))) {
      lval(v_params.lvalAt("frame", 0x2D577BEBE8CB9D1FLL)).set("alt", (LINE(4654,v_title.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0))), 0x2AB21AF4F5B8471BLL);
    }
  }
  else {
    if (!(isset(v_params.rvalAt("frame", 0x2D577BEBE8CB9D1FLL), "alt", 0x2AB21AF4F5B8471BLL))) {
      if (!same(v_caption, "")) {
        lval(v_params.lvalAt("frame", 0x2D577BEBE8CB9D1FLL)).set("alt", (LINE(4662,t_stripalttext(v_caption, v_holders))), 0x2AB21AF4F5B8471BLL);
      }
      else {
        lval(v_params.lvalAt("frame", 0x2D577BEBE8CB9D1FLL)).set("alt", (LINE(4666,v_title.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0))), 0x2AB21AF4F5B8471BLL);
      }
    }
    lval(v_params.lvalAt("frame", 0x2D577BEBE8CB9D1FLL)).set("title", (LINE(4670,t_stripalttext(v_caption, v_holders))), 0x66AD900A2301E2FELL);
  }
  LINE(4673,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserMakeImageParams").set(1, Array(ArrayInit(3).set(0, v_title).set(1, v_file).setRef(2, ref(v_params)).create())).create()), 0x00000000787A96B2LL));
  (v_ret = LINE(4676,v_sk.o_invoke_few_args("makeImageLink2", 0x7747CD213EA2EF9ELL, 6, v_title, v_file, v_params.refvalAt("frame", 0x2D577BEBE8CB9D1FLL), v_params.refvalAt("handler", 0x1D2C99E375514408LL), v_time, v_descQuery)));
  if (toBoolean(v_handler)) {
    LINE(4680,v_handler.o_invoke_few_args("parserTransformHook", 0x4F60908BC6CDD8C4LL, 2, this, v_file));
  }
  return v_ret;
} /* function */
/* SRC: Parser.php line 4686 */
Variant c_parser::t_stripalttext(Variant v_caption, CVarRef v_holders) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::stripAltText);
  Variant v_tooltip;

  if (toBoolean(v_holders)) {
    (v_tooltip = LINE(4691,toObject(v_holders)->o_invoke_few_args("replaceText", 0x316E7B92ECA74146LL, 1, v_caption)));
  }
  else {
    (v_tooltip = LINE(4693,t_replacelinkholderstext(v_caption)));
  }
  (v_tooltip = LINE(4699,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_tooltip)));
  (v_tooltip = LINE(4700,throw_fatal("unknown class sanitizer", ((void*)NULL))));
  return v_tooltip;
} /* function */
/* SRC: Parser.php line 4709 */
void c_parser::t_disablecache() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::disableCache);
  LINE(4710,invoke_failed("wfdebug", Array(ArrayInit(1).set(0, "Parser output marked as uncacheable.\n").create()), 0x00000000E441E905LL));
  (m_mOutput.o_lval("mCacheTime", 0x3A96E9F9BCC9D533LL) = -1LL);
} /* function */
/* SRC: Parser.php line 4722 */
Variant c_parser::t_attributestripcallback(Variant v_text, CVarRef v_frame //  = false
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::attributeStripCallback);
  (v_text = LINE(4723,t_replacevariables(v_text, v_frame)));
  (v_text = LINE(4724,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_text)));
  return v_text;
} /* function */
/* SRC: Parser.php line 4733 */
Variant c_parser::t_title(Variant v_x //  = null
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::Title);
  return LINE(4733,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mTitle)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: Parser.php line 4734 */
Variant c_parser::t_options(Variant v_x //  = null
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::Options);
  return LINE(4734,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mOptions)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: Parser.php line 4735 */
Variant c_parser::t_outputtype(Variant v_x //  = null
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::OutputType);
  return LINE(4735,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mOutputType)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: Parser.php line 4741 */
Variant c_parser::t_gettags() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getTags);
  Variant eo_0;
  Variant eo_1;
  return LINE(4741,(assignCallTemp(eo_0, x_array_keys(m_mTransparentTagHooks)),assignCallTemp(eo_1, x_array_keys(m_mTagHooks)),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create()))));
} /* function */
/* SRC: Parser.php line 4770 */
Variant c_parser::t_extractsections(CVarRef v_text, CVarRef v_section, CStrRef v_mode, CVarRef v_newText //  = ""
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::extractSections);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgTitle __attribute__((__unused__)) = g->GV(wgTitle);
  Variant v_outText;
  Variant v_frame;
  int64 v_flags = 0;
  Variant v_sectionParts;
  Variant v_sectionIndex;
  Variant v_part;
  Variant v_root;
  Variant v_node;
  Variant v_targetLevel;
  Variant v_bits;
  Variant v_curLevel;

  LINE(4772,t_clearstate());
  LINE(4773,t_settitle(gv_wgTitle));
  (m_mOptions = ((Object)(LINE(4774,p_parseroptions(p_parseroptions(NEWOBJ(c_parseroptions)())->create())))));
  LINE(4775,t_setoutputtype(2LL /* parser::OT_WIKI */));
  (v_outText = "");
  (v_frame = (assignCallTemp(eo_0, toObject(LINE(4777,t_getpreprocessor()))),eo_0.o_invoke_few_args("newFrame", 0x787B6141D7721675LL, 0)));
  (v_flags = 0LL);
  (v_sectionParts = LINE(4781,x_explode("-", toString(v_section))));
  (v_sectionIndex = LINE(4782,x_array_pop(ref(v_sectionParts))));
  {
    LOOP_COUNTER(121);
    for (ArrayIterPtr iter123 = v_sectionParts.begin("parser"); !iter123->end(); iter123->next()) {
      LOOP_COUNTER_CHECK(121);
      v_part = iter123->second();
      {
        if (same(v_part, "T")) {
          v_flags |= 1LL /* parser::PTD_FOR_INCLUSION */;
        }
      }
    }
  }
  (v_root = LINE(4789,t_preprocesstodom(v_text, v_flags)));
  (v_node = LINE(4793,v_root.o_invoke_few_args("getFirstChild", 0x050681239965E069LL, 0)));
  if (equal(v_sectionIndex, 0LL)) {
    (v_targetLevel = 1000LL);
  }
  else {
    LOOP_COUNTER(124);
    {
      while (toBoolean(v_node)) {
        LOOP_COUNTER_CHECK(124);
        {
          if (same(LINE(4801,v_node.o_invoke_few_args("getName", 0x23F51CDECC198965LL, 0)), "h")) {
            (v_bits = LINE(4802,v_node.o_invoke_few_args("splitHeading", 0x0621AE2D22A1922DLL, 0)));
            if (equal(v_bits.rvalAt("i", 0x0EB22EDA95766E98LL), v_sectionIndex)) {
              (v_targetLevel = v_bits.rvalAt("level", 0x26B910814038EBA3LL));
              break;
            }
          }
          if (same(v_mode, "replace")) {
            concat_assign(v_outText, toString(LINE(4809,v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_node, 27LL /* ppframe::RECOVER_ORIG */))));
          }
          (v_node = LINE(4811,v_node.o_invoke_few_args("getNextSibling", 0x4CB333CF2D880119LL, 0)));
        }
      }
    }
  }
  if (!(toBoolean(v_node))) {
    if (same(v_mode, "get")) {
      return v_newText;
    }
    else {
      return v_text;
    }
  }
  {
    LOOP_COUNTER(125);
    do {
      LOOP_COUNTER_CHECK(125);
      {
        if (same(LINE(4826,v_node.o_invoke_few_args("getName", 0x23F51CDECC198965LL, 0)), "h")) {
          (v_bits = LINE(4827,v_node.o_invoke_few_args("splitHeading", 0x0621AE2D22A1922DLL, 0)));
          (v_curLevel = v_bits.rvalAt("level", 0x26B910814038EBA3LL));
          if (!equal(v_bits.rvalAt("i", 0x0EB22EDA95766E98LL), v_sectionIndex) && not_more(v_curLevel, v_targetLevel)) {
            break;
          }
        }
        if (same(v_mode, "get")) {
          concat_assign(v_outText, toString(LINE(4834,v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_node, 27LL /* ppframe::RECOVER_ORIG */))));
        }
        (v_node = LINE(4836,v_node.o_invoke_few_args("getNextSibling", 0x4CB333CF2D880119LL, 0)));
      }
    } while (toBoolean(v_node));
  }
  if (same(v_mode, "replace")) {
    if (!equal(v_newText, "")) {
      concat_assign(v_outText, concat(toString(v_newText), "\n\n"));
    }
    LOOP_COUNTER(126);
    {
      while (toBoolean(v_node)) {
        LOOP_COUNTER_CHECK(126);
        {
          concat_assign(v_outText, toString(LINE(4850,v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_node, 27LL /* ppframe::RECOVER_ORIG */))));
          (v_node = LINE(4851,v_node.o_invoke_few_args("getNextSibling", 0x4CB333CF2D880119LL, 0)));
        }
      }
    }
  }
  if (LINE(4855,x_is_string(v_outText))) {
    (v_outText = LINE(4857,x_rtrim(toString(m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_outText)))));
  }
  return v_outText;
} /* function */
/* SRC: Parser.php line 4875 */
Variant c_parser::t_getsection(CVarRef v_text, CVarRef v_section, CStrRef v_deftext //  = ""
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getSection);
  return LINE(4876,t_extractsections(v_text, v_section, "get", v_deftext));
} /* function */
/* SRC: Parser.php line 4879 */
Variant c_parser::t_replacesection(CVarRef v_oldtext, CVarRef v_section, CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::replaceSection);
  return LINE(4880,t_extractsections(v_oldtext, v_section, "replace", v_text));
} /* function */
/* SRC: Parser.php line 4887 */
Variant c_parser::t_getrevisiontimestamp() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getRevisionTimestamp);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_wgContLang;
  Variant v_dbr;
  Variant v_timestamp;

  if (LINE(4888,x_is_null(m_mRevisionTimestamp))) {
    LINE(4889,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser::getRevisionTimestamp").create()), 0x0000000075359BAFLL));
    v_wgContLang = ref(g->GV(wgContLang));
    (v_dbr = LINE(4891,invoke_failed("wfgetdb", Array(ArrayInit(1).set(0, k_DB_SLAVE).create()), 0x000000002DA3FF23LL)));
    (v_timestamp = (assignCallTemp(eo_0, toObject(v_dbr)),LINE(4893,eo_0.o_invoke_few_args("selectField", 0x7FEFEB72B2060BADLL, 4, "revision", "rev_timestamp", Array(ArrayInit(1).set(0, "rev_id", m_mRevisionId, 0x60BB6026437DAFD3LL).create()), "Parser::getRevisionTimestamp"))));
    (v_timestamp = LINE(4899,invoke_failed("wftimestamp", Array(ArrayInit(2).set(0, k_TS_MW).set(1, ref(v_timestamp)).create()), 0x00000000AADC1C20LL)));
    (m_mRevisionTimestamp = LINE(4907,v_wgContLang.o_invoke_few_args("userAdjust", 0x5F789F083D8E18B7LL, 2, v_timestamp, "")));
    LINE(4909,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser::getRevisionTimestamp").create()), 0x00000000B599F276LL));
  }
  return m_mRevisionTimestamp;
} /* function */
/* SRC: Parser.php line 4917 */
Variant c_parser::t_getrevisionuser() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getRevisionUser);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_revision;
  Variant v_revuser;
  Variant &gv_wgUser __attribute__((__unused__)) = g->GV(wgUser);
  Variant v_wgUser;

  if (toBoolean(m_mRevisionId)) {
    (v_revision = LINE(4921,throw_fatal("unknown class revision", ((void*)NULL))));
    (v_revuser = LINE(4922,v_revision.o_invoke_few_args("getUserText", 0x66C3616EEC0044C1LL, 0)));
  }
  else {
    v_wgUser = ref(g->GV(wgUser));
    (v_revuser = LINE(4925,v_wgUser.o_invoke_few_args("getName", 0x23F51CDECC198965LL, 0)));
  }
  return v_revuser;
} /* function */
/* SRC: Parser.php line 4935 */
void c_parser::t_setdefaultsort(CVarRef v_sort) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::setDefaultSort);
  (m_mDefaultSort = v_sort);
} /* function */
/* SRC: Parser.php line 4945 */
Variant c_parser::t_getdefaultsort() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getDefaultSort);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgCategoryPrefixedDefaultSortkey __attribute__((__unused__)) = g->GV(wgCategoryPrefixedDefaultSortkey);
  if (!same(m_mDefaultSort, false)) {
    return m_mDefaultSort;
  }
  else if (equal(LINE(4949,m_mTitle.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)), k_NS_CATEGORY) || !(toBoolean(gv_wgCategoryPrefixedDefaultSortkey))) {
    return LINE(4951,m_mTitle.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0));
  }
  else {
    return LINE(4953,m_mTitle.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0));
  }
  return null;
} /* function */
/* SRC: Parser.php line 4963 */
Variant c_parser::t_getcustomdefaultsort() {
  INSTANCE_METHOD_INJECTION(Parser, Parser::getCustomDefaultSort);
  return m_mDefaultSort;
} /* function */
/* SRC: Parser.php line 4972 */
Variant c_parser::t_guesssectionnamefromwikitext(Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::guessSectionNameFromWikiText);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_headline;
  String v_sectionanchor;
  Array v_replacearray;

  (v_text = LINE(4974,t_stripsectionname(v_text)));
  (v_headline = LINE(4975,throw_fatal("unknown class sanitizer", ((void*)NULL))));
  (v_headline = LINE(4977,throw_fatal("unknown class stringutils", ((void*)NULL))));
  (v_headline = LINE(4978,x_trim(toString(v_headline))));
  (v_sectionanchor = concat("#", LINE(4979,x_urlencode(toString(x_str_replace(" ", "_", v_headline))))));
  (v_replacearray = ScalarArrays::sa_[30]);
  return LINE(4987,(assignCallTemp(eo_0, LINE(4985,x_array_keys(v_replacearray))),assignCallTemp(eo_1, LINE(4986,x_array_values(v_replacearray))),assignCallTemp(eo_2, v_sectionanchor),x_str_replace(eo_0, eo_1, eo_2)));
} /* function */
/* SRC: Parser.php line 5004 */
Variant c_parser::t_stripsectionname(Variant v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::stripSectionName);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  (v_text = LINE(5006,x_preg_replace("/\\[\\[:\?([^[|]+)\\|([^[]+)\\]\\]/", "$2", v_text)));
  (v_text = LINE(5007,x_preg_replace("/\\[\\[:\?([^[]+)\\|\?\\]\\]/", "$1", v_text)));
  (v_text = LINE(5012,(assignCallTemp(eo_0, (assignCallTemp(eo_4, toString(invoke_failed("wfurlprotocols", Array(), 0x00000000BD4BD4FELL))),concat3("/\\[(\?:", eo_4, ")([^ ]+\?) ([^[]+)\\]/"))),assignCallTemp(eo_2, v_text),x_preg_replace(eo_0, "$2", eo_2))));
  (v_text = LINE(5015,t_doquotes(v_text)));
  (v_text = LINE(5018,throw_fatal("unknown class stringutils", ((void*)NULL))));
  return v_text;
} /* function */
/* SRC: Parser.php line 5022 */
Variant c_parser::t_srvus(CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::srvus);
  return LINE(5023,o_root_invoke_few_args("testSrvus", 0x2F64E335C4988CBDLL, 2, v_text, m_mOutputType));
} /* function */
/* SRC: Parser.php line 5029 */
Variant c_parser::t_testsrvus(Variant v_text, Variant v_title, CVarRef v_options, int64 v_outputType //  = 1LL /* parser::OT_HTML */
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::testSrvus);
  LINE(5030,t_clearstate());
  if (!((instanceOf(v_title, "Title")))) {
    (v_title = LINE(5032,throw_fatal("unknown class title", ((void*)NULL))));
  }
  (m_mTitle = v_title);
  (m_mOptions = v_options);
  LINE(5036,t_setoutputtype(v_outputType));
  (v_text = LINE(5037,t_replacevariables(v_text)));
  (v_text = LINE(5038,m_mStripState.o_invoke_few_args("unstripBoth", 0x540C7C6FAA3D937BLL, 1, v_text)));
  (v_text = LINE(5039,throw_fatal("unknown class sanitizer", ((void*)NULL))));
  return v_text;
} /* function */
/* SRC: Parser.php line 5043 */
Variant c_parser::t_testpst(CVarRef v_text, Variant v_title, CVarRef v_options) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::testPst);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgUser __attribute__((__unused__)) = g->GV(wgUser);
  if (!((instanceOf(v_title, "Title")))) {
    (v_title = LINE(5046,throw_fatal("unknown class title", ((void*)NULL))));
  }
  return LINE(5048,t_presavetransform(v_text, v_title, gv_wgUser, v_options));
} /* function */
/* SRC: Parser.php line 5051 */
Variant c_parser::t_testpreprocess(CVarRef v_text, Variant v_title, CVarRef v_options) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::testPreprocess);
  if (!((instanceOf(v_title, "Title")))) {
    (v_title = LINE(5053,throw_fatal("unknown class title", ((void*)NULL))));
  }
  return LINE(5055,t_testsrvus(v_text, v_title, v_options, 3LL /* parser::OT_PREPROCESS */));
} /* function */
/* SRC: Parser.php line 5058 */
String c_parser::t_markerskipcallback(CVarRef v_s, CVarRef v_callback) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::markerSkipCallback);
  Variant eo_0;
  Variant eo_1;
  Variant v_i;
  String v_out;
  Variant v_markerStart;
  Variant v_markerEnd;

  (v_i = 0LL);
  (v_out = "");
  LOOP_COUNTER(127);
  {
    while (less(v_i, LINE(5061,x_strlen(toString(v_s))))) {
      LOOP_COUNTER_CHECK(127);
      {
        (v_markerStart = LINE(5062,x_strpos(toString(v_s), o_get("mUniqPrefix", 0x22D00FA247E8311DLL), toInt32(v_i))));
        if (same(v_markerStart, false)) {
          concat_assign(v_out, toString(LINE(5064,(assignCallTemp(eo_0, v_callback),assignCallTemp(eo_1, x_substr(toString(v_s), toInt32(v_i))),x_call_user_func(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create()))))));
          break;
        }
        else {
          concat_assign(v_out, toString(LINE(5067,(assignCallTemp(eo_0, v_callback),assignCallTemp(eo_1, x_substr(toString(v_s), toInt32(v_i), toInt32(v_markerStart - v_i))),x_call_user_func(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create()))))));
          (v_markerEnd = LINE(5068,x_strpos(toString(v_s), "-QINU\177" /* parser::MARKER_SUFFIX */, toInt32(v_markerStart))));
          if (same(v_markerEnd, false)) {
            concat_assign(v_out, toString(LINE(5070,x_substr(toString(v_s), toInt32(v_markerStart)))));
            break;
          }
          else {
            v_markerEnd += LINE(5073,x_strlen("-QINU\177" /* parser::MARKER_SUFFIX */));
            concat_assign(v_out, toString(LINE(5074,x_substr(toString(v_s), toInt32(v_markerStart), toInt32(v_markerEnd - v_markerStart)))));
            (v_i = v_markerEnd);
          }
        }
      }
    }
  }
  return v_out;
} /* function */
/* SRC: Parser.php line 5082 */
Variant c_parser::t_serialisehalfparsedtext(CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::serialiseHalfParsedText);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant v_data;
  p_stripstate v_stripState;
  Variant v_pos;
  Variant v_start_pos;
  Variant v_end_pos;
  Variant v_marker;
  Variant v_replaceArray;
  Variant v_stripText;
  Variant v_links;
  Variant v_ns;
  Variant v_trail;
  String v_key;

  (v_data = ScalarArrays::sa_[0]);
  v_data.set("text", (v_text), 0x2A28A0084DD3A743LL);
  ((Object)((v_stripState = ((Object)(LINE(5088,p_stripstate(p_stripstate(NEWOBJ(c_stripstate)())->create())))))));
  (v_pos = 0LL);
  LOOP_COUNTER(128);
  {
    while ((toBoolean((v_start_pos = LINE(5090,x_strpos(toString(v_text), o_get("mUniqPrefix", 0x22D00FA247E8311DLL), toInt32(v_pos)))))) && (toBoolean((v_end_pos = x_strpos(toString(v_text), "-QINU\177" /* parser::MARKER_SUFFIX */, toInt32(v_pos)))))) {
      LOOP_COUNTER_CHECK(128);
      {
        v_end_pos += LINE(5091,x_strlen("-QINU\177" /* parser::MARKER_SUFFIX */));
        (v_marker = LINE(5092,x_substr(toString(v_text), toInt32(v_start_pos), toInt32(v_end_pos - v_start_pos))));
        if (!(empty(m_mStripState.o_get("general", 0x661B15A9C00F7127LL).o_get("data", 0x30164401A9853128LL), v_marker))) {
          (v_replaceArray = v_stripState->m_general);
          (v_stripText = m_mStripState.o_get("general", 0x661B15A9C00F7127LL).o_get("data", 0x30164401A9853128LL).rvalAt(v_marker));
        }
        else if (!(empty(m_mStripState.o_get("nowiki", 0x78258C7EF69CF55DLL).o_get("data", 0x30164401A9853128LL), v_marker))) {
          (v_replaceArray = v_stripState->m_nowiki);
          (v_stripText = m_mStripState.o_get("nowiki", 0x78258C7EF69CF55DLL).o_get("data", 0x30164401A9853128LL).rvalAt(v_marker));
        }
        else {
          throw_exception(LINE(5101,create_object("mwexception", Array(ArrayInit(1).set(0, concat3("Hanging strip marker: '", toString(v_marker), "'.")).create()))));
        }
        LINE(5104,v_replaceArray.o_invoke_few_args("setPair", 0x1CA93325C3BDD490LL, 2, v_marker, v_stripText));
        (v_pos = v_end_pos);
      }
    }
  }
  v_data.set("stripstate", (((Object)(v_stripState))), 0x30D3F97724431EB9LL);
  (v_links = ScalarArrays::sa_[31]);
  (v_pos = 0LL);
  LOOP_COUNTER(129);
  {
    while ((toBoolean((v_start_pos = LINE(5115,x_strpos(toString(v_text), "<!--LINK ", toInt32(v_pos))))))) {
      LOOP_COUNTER_CHECK(129);
      {
        df_lambda_18(LINE(5116,(assignCallTemp(eo_1, toString((assignCallTemp(eo_3, toString(v_text)),assignCallTemp(eo_4, toInt32(v_start_pos + x_strlen("<!--LINK "))),x_substr(eo_3, eo_4)))),x_explode(":", eo_1, toInt32(2LL)))), v_ns, v_trail);
        (v_ns = LINE(5118,x_trim(toString(v_ns))));
        if (empty(v_links.rvalAt("internal", 0x575D95D69332A8ACLL), v_ns)) {
          lval(v_links.lvalAt("internal", 0x575D95D69332A8ACLL)).set(v_ns, (ScalarArrays::sa_[0]));
        }
        (v_key = LINE(5123,x_trim(toString((assignCallTemp(eo_0, toString(v_trail)),assignCallTemp(eo_2, toInt32(x_strpos(toString(v_trail), "-->"))),x_substr(eo_0, toInt32(0LL), eo_2))))));
        lval(lval(v_links.lvalAt("internal", 0x575D95D69332A8ACLL)).lvalAt(v_ns)).append((m_mLinkHolders.o_get("internals", 0x69A9269F8FE824A3LL).rvalAt(v_ns).rvalAt(v_key)));
        (v_pos = v_start_pos + LINE(5125,x_strlen(concat5("<!--LINK ", toString(v_ns), ":", v_key, "-->"))));
      }
    }
  }
  (v_pos = 0LL);
  LOOP_COUNTER(130);
  {
    while ((toBoolean((v_start_pos = LINE(5131,x_strpos(toString(v_text), "<!--IWLINK ", toInt32(v_pos))))))) {
      LOOP_COUNTER_CHECK(130);
      {
        (v_data = LINE(5132,x_substr(toString(v_text), toInt32(v_start_pos))));
        (v_key = LINE(5133,x_trim(toString((assignCallTemp(eo_0, toString(v_data)),assignCallTemp(eo_2, toInt32(x_strpos(toString(v_data), "-->"))),x_substr(eo_0, toInt32(0LL), eo_2))))));
        lval(v_links.lvalAt("interwiki", 0x148BE9CCAAA589D7LL)).append((m_mLinkHolders.o_get("interwiki", 0x148BE9CCAAA589D7LL).rvalAt(v_key)));
        (v_pos = v_start_pos + LINE(5135,x_strlen(concat3("<!--IWLINK ", v_key, "-->"))));
      }
    }
  }
  v_data.set("linkholder", (v_links), 0x3D463C625DB39AA4LL);
  return v_data;
} /* function */
/* SRC: Parser.php line 5143 */
Variant c_parser::t_unserialisehalfparsedtext(CVarRef v_data, Variant v_intPrefix //  = null
) {
  INSTANCE_METHOD_INJECTION(Parser, Parser::unserialiseHalfParsedText);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_stripState;
  Variant v_text;
  Variant v_links;
  Primitive v_ns = 0;
  Variant v_nsLinks;
  Primitive v_key = 0;
  Variant v_entry;
  String v_newKey;

  if (!(toBoolean(v_intPrefix))) (v_intPrefix = LINE(5145,t_getrandomstring()));
  (v_stripState = v_data.rvalAt("stripstate", 0x30D3F97724431EB9LL));
  LINE(5149,m_mStripState.o_get("general", 0x661B15A9C00F7127LL).o_invoke_few_args("merge", 0x1C2E89C2A927FF7BLL, 1, v_stripState.o_lval("general", 0x661B15A9C00F7127LL)));
  LINE(5150,m_mStripState.o_get("nowiki", 0x78258C7EF69CF55DLL).o_invoke_few_args("merge", 0x1C2E89C2A927FF7BLL, 1, v_stripState.o_lval("nowiki", 0x78258C7EF69CF55DLL)));
  (v_text = v_data.rvalAt("text", 0x2A28A0084DD3A743LL));
  (v_links = v_data.rvalAt("linkholder", 0x3D463C625DB39AA4LL));
  {
    LOOP_COUNTER(131);
    Variant map132 = v_links.rvalAt("internal", 0x575D95D69332A8ACLL);
    for (ArrayIterPtr iter133 = map132.begin("parser"); !iter133->end(); iter133->next()) {
      LOOP_COUNTER_CHECK(131);
      v_nsLinks = iter133->second();
      v_ns = iter133->first();
      {
        {
          LOOP_COUNTER(134);
          for (ArrayIterPtr iter136 = v_nsLinks.begin("parser"); !iter136->end(); iter136->next()) {
            LOOP_COUNTER_CHECK(134);
            v_entry = iter136->second();
            v_key = iter136->first();
            {
              (v_newKey = LINE(5159,concat3(toString(v_intPrefix), "-", toString(v_key))));
              lval(lval(m_mLinkHolders.o_lval("internals", 0x69A9269F8FE824A3LL)).lvalAt(v_ns)).set(v_newKey, (v_entry));
              (v_text = LINE(5162,(assignCallTemp(eo_0, concat5("<!--LINK ", toString(v_ns), ":", toString(v_key), "-->")),assignCallTemp(eo_1, concat5("<!--LINK ", toString(v_ns), ":", v_newKey, "-->")),assignCallTemp(eo_2, v_text),x_str_replace(eo_0, eo_1, eo_2))));
            }
          }
        }
      }
    }
  }
  {
    LOOP_COUNTER(137);
    Variant map138 = v_links.rvalAt("interwiki", 0x148BE9CCAAA589D7LL);
    for (ArrayIterPtr iter139 = map138.begin("parser"); !iter139->end(); iter139->next()) {
      LOOP_COUNTER_CHECK(137);
      v_entry = iter139->second();
      v_key = iter139->first();
      {
        (v_newKey = LINE(5168,concat3(toString(v_intPrefix), "-", toString(v_key))));
        lval(m_mLinkHolders.o_lval("interwikis", 0x7FD8492E01593115LL)).set(v_newKey, (v_entry));
        (v_text = LINE(5171,(assignCallTemp(eo_0, concat3("<!--IWLINK ", toString(v_key), "-->")),assignCallTemp(eo_1, concat3("<!--IWLINK ", v_newKey, "-->")),assignCallTemp(eo_2, v_text),x_str_replace(eo_0, eo_1, eo_2))));
      }
    }
  }
  return v_text;
} /* function */
/* SRC: Parser.php line 5227 */
Variant c_onlyincludereplacer::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_onlyincludereplacer::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_onlyincludereplacer::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("output", m_output.isReferenced() ? ref(m_output) : m_output));
  c_ObjectData::o_get(props);
}
bool c_onlyincludereplacer::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x6F443E367A5DBB24LL, output, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_onlyincludereplacer::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x6F443E367A5DBB24LL, m_output,
                         output, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_onlyincludereplacer::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x6F443E367A5DBB24LL, m_output,
                      output, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_onlyincludereplacer::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x6F443E367A5DBB24LL, m_output,
                         output, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_onlyincludereplacer::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(onlyincludereplacer)
ObjectData *c_onlyincludereplacer::cloneImpl() {
  c_onlyincludereplacer *obj = NEW(c_onlyincludereplacer)();
  cloneSet(obj);
  return obj;
}
void c_onlyincludereplacer::cloneSet(c_onlyincludereplacer *clone) {
  clone->m_output = m_output.isReferenced() ? ref(m_output) : m_output;
  ObjectData::cloneSet(clone);
}
Variant c_onlyincludereplacer::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (t_replace(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_onlyincludereplacer::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 0:
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (t_replace(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_onlyincludereplacer::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_onlyincludereplacer$os_get(const char *s) {
  return c_onlyincludereplacer::os_get(s, -1);
}
Variant &cw_onlyincludereplacer$os_lval(const char *s) {
  return c_onlyincludereplacer::os_lval(s, -1);
}
Variant cw_onlyincludereplacer$os_constant(const char *s) {
  return c_onlyincludereplacer::os_constant(s);
}
Variant cw_onlyincludereplacer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_onlyincludereplacer::os_invoke(c, s, params, -1, fatal);
}
void c_onlyincludereplacer::init() {
  m_output = "";
}
/* SRC: Parser.php line 5230 */
void c_onlyincludereplacer::t_replace(CVarRef v_matches) {
  INSTANCE_METHOD_INJECTION(OnlyIncludeReplacer, OnlyIncludeReplacer::replace);
  if (same(LINE(5231,x_substr(toString(v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toInt32(-1LL))), "\n")) {
    concat_assign(m_output, toString(LINE(5232,x_substr(toString(v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL)), toInt32(0LL), toInt32(-1LL)))));
  }
  else {
    concat_assign(m_output, toString(v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL)));
  }
} /* function */
Object co_stripstate(CArrRef params, bool init /* = true */) {
  return Object(p_stripstate(NEW(c_stripstate)())->dynCreate(params, init));
}
Object co_parser(CArrRef params, bool init /* = true */) {
  return Object(p_parser(NEW(c_parser)())->dynCreate(params, init));
}
Object co_onlyincludereplacer(CArrRef params, bool init /* = true */) {
  return Object(p_onlyincludereplacer(NEW(c_onlyincludereplacer)())->dynCreate(params, init));
}
Variant pm_php$Parser_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::Parser.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$Parser_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
