
#ifndef __GENERATED_cls_parseroutput_h__
#define __GENERATED_cls_parseroutput_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: ParserOutput.php line 6 */
class c_parseroutput : virtual public ObjectData {
  BEGIN_CLASS_MAP(parseroutput)
  END_CLASS_MAP(parseroutput)
  DECLARE_CLASS(parseroutput, ParserOutput, ObjectData)
  void init();
  public: Variant m_mText;
  public: Variant m_mLanguageLinks;
  public: Variant m_mCategories;
  public: Variant m_mContainsOldMagic;
  public: Variant m_mTitleText;
  public: Variant m_mCacheTime;
  public: Variant m_mVersion;
  public: Variant m_mLinks;
  public: Variant m_mTemplates;
  public: Variant m_mTemplateIds;
  public: Variant m_mImages;
  public: Variant m_mExternalLinks;
  public: Variant m_mNewSection;
  public: Variant m_mHideNewSection;
  public: Variant m_mNoGallery;
  public: Variant m_mHeadItems;
  public: Variant m_mOutputHooks;
  public: Variant m_mWarnings;
  public: Variant m_mSections;
  public: Variant m_mProperties;
  public: Variant m_mTOCHTML;
  public: Variant m_mIndexPolicy;
  public: Variant m_displayTitle;
  public: void t_parseroutput(CStrRef v_text = "", CArrRef v_languageLinks = ScalarArrays::sa_[0], CArrRef v_categoryLinks = ScalarArrays::sa_[0], bool v_containsOldMagic = false, CStrRef v_titletext = "");
  public: ObjectData *create(CStrRef v_text = "", CArrRef v_languageLinks = ScalarArrays::sa_[0], CArrRef v_categoryLinks = ScalarArrays::sa_[0], bool v_containsOldMagic = false, CStrRef v_titletext = "");
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_gettext();
  public: Variant t_getlanguagelinks();
  public: Variant t_getcategorylinks();
  public: Variant t_getcategories();
  public: Variant t_getcachetime();
  public: Variant t_gettitletext();
  public: Variant t_getsections();
  public: Variant t_getlinks();
  public: Variant t_gettemplates();
  public: Variant t_getimages();
  public: Variant t_getexternallinks();
  public: Variant t_getnogallery();
  public: Variant t_getsubtitle();
  public: Array t_getoutputhooks();
  public: Variant t_getwarnings();
  public: Variant t_getindexpolicy();
  public: Variant t_gettochtml();
  public: Variant t_containsoldmagic();
  public: Variant t_settext(Variant v_text);
  public: Variant t_setlanguagelinks(Variant v_ll);
  public: Variant t_setcategorylinks(Variant v_cl);
  public: Variant t_setcontainsoldmagic(Variant v_com);
  public: Variant t_setcachetime(Variant v_t);
  public: Variant t_settitletext(Variant v_t);
  public: Variant t_setsections(Variant v_toc);
  public: Variant t_setindexpolicy(Variant v_policy);
  public: Variant t_settochtml(Variant v_tochtml);
  public: void t_addcategory(CVarRef v_c, CVarRef v_sort);
  public: void t_addlanguagelink(CVarRef v_t);
  public: void t_addwarning(CVarRef v_s);
  public: void t_addoutputhook(CVarRef v_hook, bool v_data = false);
  public: void t_setnewsection(CVarRef v_value);
  public: void t_hidenewsection(CVarRef v_value);
  public: bool t_gethidenewsection();
  public: bool t_getnewsection();
  public: void t_addexternallink(CVarRef v_url);
  public: void t_addlink(CVarRef v_title, Variant v_id = null);
  public: void t_addimage(CVarRef v_name);
  public: void t_addtemplate(CVarRef v_title, CVarRef v_page_id, CVarRef v_rev_id);
  public: bool t_expired(CVarRef v_touched);
  public: void t_addheaditem(CVarRef v_section, bool v_tag = false);
  public: void t_setdisplaytitle(CVarRef v_text);
  public: Variant t_getdisplaytitle();
  public: void t_setflag(CVarRef v_flag);
  public: bool t_getflag(CVarRef v_flag);
  public: void t_setproperty(CVarRef v_name, CVarRef v_value);
  public: Variant t_getproperty(CVarRef v_name);
  public: Variant t_getproperties();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parseroutput_h__
