
#ifndef __GENERATED_php_Parser_h__
#define __GENERATED_php_Parser_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/Parser.fw.h>

// Declarations
#include <cls/stripstate.h>
#include <cls/parser.h>
#include <cls/onlyincludereplacer.h>
#include <php/CoreParserFunctions.h>
#include <php/DateFormatter.h>
#include <php/LinkHolderArray.h>
#include <php/Tidy.h>
#include <php/ParserOptions.h>
#include <php/ParserOutput.h>
#include <php/Preprocessor.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$Parser_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_stripstate(CArrRef params, bool init = true);
Object co_parser(CArrRef params, bool init = true);
Object co_onlyincludereplacer(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_Parser_h__
