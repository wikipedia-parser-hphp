
#ifndef __GENERATED_cls_linkholderarray_h__
#define __GENERATED_cls_linkholderarray_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: LinkHolderArray.php line 3 */
class c_linkholderarray : virtual public ObjectData {
  BEGIN_CLASS_MAP(linkholderarray)
  END_CLASS_MAP(linkholderarray)
  DECLARE_CLASS(linkholderarray, LinkHolderArray, ObjectData)
  void init();
  public: Variant m_internals;
  public: Variant m_interwikis;
  public: Variant m_size;
  public: Variant m_parent;
  public: virtual void destruct();
  public: void t___construct(Variant v_parent);
  public: ObjectData *create(Variant v_parent);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t___destruct();
  public: void t_merge(CVarRef v_other);
  public: bool t_isbig();
  public: void t_clear();
  public: String t_makeholder(CVarRef v_nt, CVarRef v_text = "", CVarRef v_query = "", Variant v_trail = "", CVarRef v_prefix = "");
  public: Variant t_getstubthreshold();
  public: Variant t_replace(Variant v_text);
  public: void t_replaceinternal(Variant v_text);
  public: void t_replaceinterwiki(Variant v_text);
  public: void t_dovariants(Variant v_colours);
  public: Variant t_replacetext(Variant v_text);
  public: Variant t_replacetextcallback(CVarRef v_matches);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_linkholderarray_h__
