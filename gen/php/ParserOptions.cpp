
#include <php/ParserOptions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: ParserOptions.php line 8 */
Variant c_parseroptions::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_parseroptions::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_parseroptions::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("mUseTeX", m_mUseTeX.isReferenced() ? ref(m_mUseTeX) : m_mUseTeX));
  props.push_back(NEW(ArrayElement)("mUseDynamicDates", m_mUseDynamicDates.isReferenced() ? ref(m_mUseDynamicDates) : m_mUseDynamicDates));
  props.push_back(NEW(ArrayElement)("mInterwikiMagic", m_mInterwikiMagic.isReferenced() ? ref(m_mInterwikiMagic) : m_mInterwikiMagic));
  props.push_back(NEW(ArrayElement)("mAllowExternalImages", m_mAllowExternalImages.isReferenced() ? ref(m_mAllowExternalImages) : m_mAllowExternalImages));
  props.push_back(NEW(ArrayElement)("mAllowExternalImagesFrom", m_mAllowExternalImagesFrom.isReferenced() ? ref(m_mAllowExternalImagesFrom) : m_mAllowExternalImagesFrom));
  props.push_back(NEW(ArrayElement)("mEnableImageWhitelist", m_mEnableImageWhitelist.isReferenced() ? ref(m_mEnableImageWhitelist) : m_mEnableImageWhitelist));
  props.push_back(NEW(ArrayElement)("mSkin", m_mSkin.isReferenced() ? ref(m_mSkin) : m_mSkin));
  props.push_back(NEW(ArrayElement)("mDateFormat", m_mDateFormat.isReferenced() ? ref(m_mDateFormat) : m_mDateFormat));
  props.push_back(NEW(ArrayElement)("mEditSection", m_mEditSection.isReferenced() ? ref(m_mEditSection) : m_mEditSection));
  props.push_back(NEW(ArrayElement)("mNumberHeadings", m_mNumberHeadings.isReferenced() ? ref(m_mNumberHeadings) : m_mNumberHeadings));
  props.push_back(NEW(ArrayElement)("mAllowSpecialInclusion", m_mAllowSpecialInclusion.isReferenced() ? ref(m_mAllowSpecialInclusion) : m_mAllowSpecialInclusion));
  props.push_back(NEW(ArrayElement)("mTidy", m_mTidy.isReferenced() ? ref(m_mTidy) : m_mTidy));
  props.push_back(NEW(ArrayElement)("mInterfaceMessage", m_mInterfaceMessage.isReferenced() ? ref(m_mInterfaceMessage) : m_mInterfaceMessage));
  props.push_back(NEW(ArrayElement)("mTargetLanguage", m_mTargetLanguage.isReferenced() ? ref(m_mTargetLanguage) : m_mTargetLanguage));
  props.push_back(NEW(ArrayElement)("mMaxIncludeSize", m_mMaxIncludeSize.isReferenced() ? ref(m_mMaxIncludeSize) : m_mMaxIncludeSize));
  props.push_back(NEW(ArrayElement)("mMaxPPNodeCount", m_mMaxPPNodeCount.isReferenced() ? ref(m_mMaxPPNodeCount) : m_mMaxPPNodeCount));
  props.push_back(NEW(ArrayElement)("mMaxPPExpandDepth", m_mMaxPPExpandDepth.isReferenced() ? ref(m_mMaxPPExpandDepth) : m_mMaxPPExpandDepth));
  props.push_back(NEW(ArrayElement)("mMaxTemplateDepth", m_mMaxTemplateDepth.isReferenced() ? ref(m_mMaxTemplateDepth) : m_mMaxTemplateDepth));
  props.push_back(NEW(ArrayElement)("mRemoveComments", m_mRemoveComments.isReferenced() ? ref(m_mRemoveComments) : m_mRemoveComments));
  props.push_back(NEW(ArrayElement)("mTemplateCallback", m_mTemplateCallback.isReferenced() ? ref(m_mTemplateCallback) : m_mTemplateCallback));
  props.push_back(NEW(ArrayElement)("mEnableLimitReport", m_mEnableLimitReport.isReferenced() ? ref(m_mEnableLimitReport) : m_mEnableLimitReport));
  props.push_back(NEW(ArrayElement)("mTimestamp", m_mTimestamp.isReferenced() ? ref(m_mTimestamp) : m_mTimestamp));
  props.push_back(NEW(ArrayElement)("mExternalLinkTarget", m_mExternalLinkTarget.isReferenced() ? ref(m_mExternalLinkTarget) : m_mExternalLinkTarget));
  props.push_back(NEW(ArrayElement)("mUser", m_mUser.isReferenced() ? ref(m_mUser) : m_mUser));
  props.push_back(NEW(ArrayElement)("mIsPreview", m_mIsPreview.isReferenced() ? ref(m_mIsPreview) : m_mIsPreview));
  props.push_back(NEW(ArrayElement)("mIsSectionPreview", m_mIsSectionPreview.isReferenced() ? ref(m_mIsSectionPreview) : m_mIsSectionPreview));
  props.push_back(NEW(ArrayElement)("mIsPrintable", m_mIsPrintable.isReferenced() ? ref(m_mIsPrintable) : m_mIsPrintable));
  c_ObjectData::o_get(props);
}
bool c_parseroptions::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 0:
      HASH_EXISTS_STRING(0x316BC0F2B0E26180LL, mUser, 5);
      break;
    case 4:
      HASH_EXISTS_STRING(0x763C8510123DFFC4LL, mIsPrintable, 12);
      break;
    case 7:
      HASH_EXISTS_STRING(0x46AADA01E8915747LL, mTidy, 5);
      break;
    case 10:
      HASH_EXISTS_STRING(0x73F3AA262BBE77CALL, mDateFormat, 11);
      break;
    case 11:
      HASH_EXISTS_STRING(0x6F5C8D9DF76B8F0BLL, mUseDynamicDates, 16);
      break;
    case 12:
      HASH_EXISTS_STRING(0x0625E872F78E6D8CLL, mExternalLinkTarget, 19);
      break;
    case 14:
      HASH_EXISTS_STRING(0x48B79CB251AA37CELL, mEnableImageWhitelist, 21);
      break;
    case 20:
      HASH_EXISTS_STRING(0x198864A9BC83D7D4LL, mSkin, 5);
      HASH_EXISTS_STRING(0x2B145DAB4A584D54LL, mIsPreview, 10);
      break;
    case 21:
      HASH_EXISTS_STRING(0x3DD526BE5013BD55LL, mMaxPPNodeCount, 15);
      HASH_EXISTS_STRING(0x1D7E526AF4EDBCD5LL, mMaxTemplateDepth, 17);
      break;
    case 26:
      HASH_EXISTS_STRING(0x513A835C94185E5ALL, mEditSection, 12);
      break;
    case 27:
      HASH_EXISTS_STRING(0x18630BFBBD4A10DBLL, mTemplateCallback, 17);
      break;
    case 29:
      HASH_EXISTS_STRING(0x3B40E2FE5D6554DDLL, mNumberHeadings, 15);
      HASH_EXISTS_STRING(0x69C2FB7F954EF5DDLL, mMaxIncludeSize, 15);
      HASH_EXISTS_STRING(0x0AADCB30E7D6EC9DLL, mIsSectionPreview, 17);
      break;
    case 30:
      HASH_EXISTS_STRING(0x2F1977A57AF7D6DELL, mMaxPPExpandDepth, 17);
      break;
    case 36:
      HASH_EXISTS_STRING(0x445D1000EF6096A4LL, mAllowExternalImages, 20);
      break;
    case 38:
      HASH_EXISTS_STRING(0x3CDFE89965667DA6LL, mInterfaceMessage, 17);
      break;
    case 41:
      HASH_EXISTS_STRING(0x7C6DFEF329ED7D69LL, mAllowSpecialInclusion, 22);
      break;
    case 44:
      HASH_EXISTS_STRING(0x7C0C15E96BAB2D2CLL, mTargetLanguage, 15);
      break;
    case 50:
      HASH_EXISTS_STRING(0x0274AAE2A9A37AB2LL, mRemoveComments, 15);
      break;
    case 55:
      HASH_EXISTS_STRING(0x05A881D863419037LL, mAllowExternalImagesFrom, 24);
      HASH_EXISTS_STRING(0x71234371FAE7CC37LL, mEnableLimitReport, 18);
      break;
    case 56:
      HASH_EXISTS_STRING(0x10535E2E0E19F538LL, mUseTeX, 7);
      break;
    case 58:
      HASH_EXISTS_STRING(0x78CCE3041490DE7ALL, mTimestamp, 10);
      break;
    case 59:
      HASH_EXISTS_STRING(0x37FD6D9CA60B237BLL, mInterwikiMagic, 15);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_parseroptions::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 0:
      HASH_RETURN_STRING(0x316BC0F2B0E26180LL, m_mUser,
                         mUser, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x763C8510123DFFC4LL, m_mIsPrintable,
                         mIsPrintable, 12);
      break;
    case 7:
      HASH_RETURN_STRING(0x46AADA01E8915747LL, m_mTidy,
                         mTidy, 5);
      break;
    case 10:
      HASH_RETURN_STRING(0x73F3AA262BBE77CALL, m_mDateFormat,
                         mDateFormat, 11);
      break;
    case 11:
      HASH_RETURN_STRING(0x6F5C8D9DF76B8F0BLL, m_mUseDynamicDates,
                         mUseDynamicDates, 16);
      break;
    case 12:
      HASH_RETURN_STRING(0x0625E872F78E6D8CLL, m_mExternalLinkTarget,
                         mExternalLinkTarget, 19);
      break;
    case 14:
      HASH_RETURN_STRING(0x48B79CB251AA37CELL, m_mEnableImageWhitelist,
                         mEnableImageWhitelist, 21);
      break;
    case 20:
      HASH_RETURN_STRING(0x198864A9BC83D7D4LL, m_mSkin,
                         mSkin, 5);
      HASH_RETURN_STRING(0x2B145DAB4A584D54LL, m_mIsPreview,
                         mIsPreview, 10);
      break;
    case 21:
      HASH_RETURN_STRING(0x3DD526BE5013BD55LL, m_mMaxPPNodeCount,
                         mMaxPPNodeCount, 15);
      HASH_RETURN_STRING(0x1D7E526AF4EDBCD5LL, m_mMaxTemplateDepth,
                         mMaxTemplateDepth, 17);
      break;
    case 26:
      HASH_RETURN_STRING(0x513A835C94185E5ALL, m_mEditSection,
                         mEditSection, 12);
      break;
    case 27:
      HASH_RETURN_STRING(0x18630BFBBD4A10DBLL, m_mTemplateCallback,
                         mTemplateCallback, 17);
      break;
    case 29:
      HASH_RETURN_STRING(0x3B40E2FE5D6554DDLL, m_mNumberHeadings,
                         mNumberHeadings, 15);
      HASH_RETURN_STRING(0x69C2FB7F954EF5DDLL, m_mMaxIncludeSize,
                         mMaxIncludeSize, 15);
      HASH_RETURN_STRING(0x0AADCB30E7D6EC9DLL, m_mIsSectionPreview,
                         mIsSectionPreview, 17);
      break;
    case 30:
      HASH_RETURN_STRING(0x2F1977A57AF7D6DELL, m_mMaxPPExpandDepth,
                         mMaxPPExpandDepth, 17);
      break;
    case 36:
      HASH_RETURN_STRING(0x445D1000EF6096A4LL, m_mAllowExternalImages,
                         mAllowExternalImages, 20);
      break;
    case 38:
      HASH_RETURN_STRING(0x3CDFE89965667DA6LL, m_mInterfaceMessage,
                         mInterfaceMessage, 17);
      break;
    case 41:
      HASH_RETURN_STRING(0x7C6DFEF329ED7D69LL, m_mAllowSpecialInclusion,
                         mAllowSpecialInclusion, 22);
      break;
    case 44:
      HASH_RETURN_STRING(0x7C0C15E96BAB2D2CLL, m_mTargetLanguage,
                         mTargetLanguage, 15);
      break;
    case 50:
      HASH_RETURN_STRING(0x0274AAE2A9A37AB2LL, m_mRemoveComments,
                         mRemoveComments, 15);
      break;
    case 55:
      HASH_RETURN_STRING(0x05A881D863419037LL, m_mAllowExternalImagesFrom,
                         mAllowExternalImagesFrom, 24);
      HASH_RETURN_STRING(0x71234371FAE7CC37LL, m_mEnableLimitReport,
                         mEnableLimitReport, 18);
      break;
    case 56:
      HASH_RETURN_STRING(0x10535E2E0E19F538LL, m_mUseTeX,
                         mUseTeX, 7);
      break;
    case 58:
      HASH_RETURN_STRING(0x78CCE3041490DE7ALL, m_mTimestamp,
                         mTimestamp, 10);
      break;
    case 59:
      HASH_RETURN_STRING(0x37FD6D9CA60B237BLL, m_mInterwikiMagic,
                         mInterwikiMagic, 15);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_parseroptions::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 0:
      HASH_SET_STRING(0x316BC0F2B0E26180LL, m_mUser,
                      mUser, 5);
      break;
    case 4:
      HASH_SET_STRING(0x763C8510123DFFC4LL, m_mIsPrintable,
                      mIsPrintable, 12);
      break;
    case 7:
      HASH_SET_STRING(0x46AADA01E8915747LL, m_mTidy,
                      mTidy, 5);
      break;
    case 10:
      HASH_SET_STRING(0x73F3AA262BBE77CALL, m_mDateFormat,
                      mDateFormat, 11);
      break;
    case 11:
      HASH_SET_STRING(0x6F5C8D9DF76B8F0BLL, m_mUseDynamicDates,
                      mUseDynamicDates, 16);
      break;
    case 12:
      HASH_SET_STRING(0x0625E872F78E6D8CLL, m_mExternalLinkTarget,
                      mExternalLinkTarget, 19);
      break;
    case 14:
      HASH_SET_STRING(0x48B79CB251AA37CELL, m_mEnableImageWhitelist,
                      mEnableImageWhitelist, 21);
      break;
    case 20:
      HASH_SET_STRING(0x198864A9BC83D7D4LL, m_mSkin,
                      mSkin, 5);
      HASH_SET_STRING(0x2B145DAB4A584D54LL, m_mIsPreview,
                      mIsPreview, 10);
      break;
    case 21:
      HASH_SET_STRING(0x3DD526BE5013BD55LL, m_mMaxPPNodeCount,
                      mMaxPPNodeCount, 15);
      HASH_SET_STRING(0x1D7E526AF4EDBCD5LL, m_mMaxTemplateDepth,
                      mMaxTemplateDepth, 17);
      break;
    case 26:
      HASH_SET_STRING(0x513A835C94185E5ALL, m_mEditSection,
                      mEditSection, 12);
      break;
    case 27:
      HASH_SET_STRING(0x18630BFBBD4A10DBLL, m_mTemplateCallback,
                      mTemplateCallback, 17);
      break;
    case 29:
      HASH_SET_STRING(0x3B40E2FE5D6554DDLL, m_mNumberHeadings,
                      mNumberHeadings, 15);
      HASH_SET_STRING(0x69C2FB7F954EF5DDLL, m_mMaxIncludeSize,
                      mMaxIncludeSize, 15);
      HASH_SET_STRING(0x0AADCB30E7D6EC9DLL, m_mIsSectionPreview,
                      mIsSectionPreview, 17);
      break;
    case 30:
      HASH_SET_STRING(0x2F1977A57AF7D6DELL, m_mMaxPPExpandDepth,
                      mMaxPPExpandDepth, 17);
      break;
    case 36:
      HASH_SET_STRING(0x445D1000EF6096A4LL, m_mAllowExternalImages,
                      mAllowExternalImages, 20);
      break;
    case 38:
      HASH_SET_STRING(0x3CDFE89965667DA6LL, m_mInterfaceMessage,
                      mInterfaceMessage, 17);
      break;
    case 41:
      HASH_SET_STRING(0x7C6DFEF329ED7D69LL, m_mAllowSpecialInclusion,
                      mAllowSpecialInclusion, 22);
      break;
    case 44:
      HASH_SET_STRING(0x7C0C15E96BAB2D2CLL, m_mTargetLanguage,
                      mTargetLanguage, 15);
      break;
    case 50:
      HASH_SET_STRING(0x0274AAE2A9A37AB2LL, m_mRemoveComments,
                      mRemoveComments, 15);
      break;
    case 55:
      HASH_SET_STRING(0x05A881D863419037LL, m_mAllowExternalImagesFrom,
                      mAllowExternalImagesFrom, 24);
      HASH_SET_STRING(0x71234371FAE7CC37LL, m_mEnableLimitReport,
                      mEnableLimitReport, 18);
      break;
    case 56:
      HASH_SET_STRING(0x10535E2E0E19F538LL, m_mUseTeX,
                      mUseTeX, 7);
      break;
    case 58:
      HASH_SET_STRING(0x78CCE3041490DE7ALL, m_mTimestamp,
                      mTimestamp, 10);
      break;
    case 59:
      HASH_SET_STRING(0x37FD6D9CA60B237BLL, m_mInterwikiMagic,
                      mInterwikiMagic, 15);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_parseroptions::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 0:
      HASH_RETURN_STRING(0x316BC0F2B0E26180LL, m_mUser,
                         mUser, 5);
      break;
    case 4:
      HASH_RETURN_STRING(0x763C8510123DFFC4LL, m_mIsPrintable,
                         mIsPrintable, 12);
      break;
    case 7:
      HASH_RETURN_STRING(0x46AADA01E8915747LL, m_mTidy,
                         mTidy, 5);
      break;
    case 10:
      HASH_RETURN_STRING(0x73F3AA262BBE77CALL, m_mDateFormat,
                         mDateFormat, 11);
      break;
    case 11:
      HASH_RETURN_STRING(0x6F5C8D9DF76B8F0BLL, m_mUseDynamicDates,
                         mUseDynamicDates, 16);
      break;
    case 12:
      HASH_RETURN_STRING(0x0625E872F78E6D8CLL, m_mExternalLinkTarget,
                         mExternalLinkTarget, 19);
      break;
    case 14:
      HASH_RETURN_STRING(0x48B79CB251AA37CELL, m_mEnableImageWhitelist,
                         mEnableImageWhitelist, 21);
      break;
    case 20:
      HASH_RETURN_STRING(0x198864A9BC83D7D4LL, m_mSkin,
                         mSkin, 5);
      HASH_RETURN_STRING(0x2B145DAB4A584D54LL, m_mIsPreview,
                         mIsPreview, 10);
      break;
    case 21:
      HASH_RETURN_STRING(0x3DD526BE5013BD55LL, m_mMaxPPNodeCount,
                         mMaxPPNodeCount, 15);
      HASH_RETURN_STRING(0x1D7E526AF4EDBCD5LL, m_mMaxTemplateDepth,
                         mMaxTemplateDepth, 17);
      break;
    case 26:
      HASH_RETURN_STRING(0x513A835C94185E5ALL, m_mEditSection,
                         mEditSection, 12);
      break;
    case 27:
      HASH_RETURN_STRING(0x18630BFBBD4A10DBLL, m_mTemplateCallback,
                         mTemplateCallback, 17);
      break;
    case 29:
      HASH_RETURN_STRING(0x3B40E2FE5D6554DDLL, m_mNumberHeadings,
                         mNumberHeadings, 15);
      HASH_RETURN_STRING(0x69C2FB7F954EF5DDLL, m_mMaxIncludeSize,
                         mMaxIncludeSize, 15);
      HASH_RETURN_STRING(0x0AADCB30E7D6EC9DLL, m_mIsSectionPreview,
                         mIsSectionPreview, 17);
      break;
    case 30:
      HASH_RETURN_STRING(0x2F1977A57AF7D6DELL, m_mMaxPPExpandDepth,
                         mMaxPPExpandDepth, 17);
      break;
    case 36:
      HASH_RETURN_STRING(0x445D1000EF6096A4LL, m_mAllowExternalImages,
                         mAllowExternalImages, 20);
      break;
    case 38:
      HASH_RETURN_STRING(0x3CDFE89965667DA6LL, m_mInterfaceMessage,
                         mInterfaceMessage, 17);
      break;
    case 41:
      HASH_RETURN_STRING(0x7C6DFEF329ED7D69LL, m_mAllowSpecialInclusion,
                         mAllowSpecialInclusion, 22);
      break;
    case 44:
      HASH_RETURN_STRING(0x7C0C15E96BAB2D2CLL, m_mTargetLanguage,
                         mTargetLanguage, 15);
      break;
    case 50:
      HASH_RETURN_STRING(0x0274AAE2A9A37AB2LL, m_mRemoveComments,
                         mRemoveComments, 15);
      break;
    case 55:
      HASH_RETURN_STRING(0x05A881D863419037LL, m_mAllowExternalImagesFrom,
                         mAllowExternalImagesFrom, 24);
      HASH_RETURN_STRING(0x71234371FAE7CC37LL, m_mEnableLimitReport,
                         mEnableLimitReport, 18);
      break;
    case 56:
      HASH_RETURN_STRING(0x10535E2E0E19F538LL, m_mUseTeX,
                         mUseTeX, 7);
      break;
    case 58:
      HASH_RETURN_STRING(0x78CCE3041490DE7ALL, m_mTimestamp,
                         mTimestamp, 10);
      break;
    case 59:
      HASH_RETURN_STRING(0x37FD6D9CA60B237BLL, m_mInterwikiMagic,
                         mInterwikiMagic, 15);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_parseroptions::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(parseroptions)
ObjectData *c_parseroptions::create(Variant v_user //  = null
) {
  init();
  t___construct(v_user);
  return this;
}
ObjectData *c_parseroptions::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_parseroptions::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_parseroptions::cloneImpl() {
  c_parseroptions *obj = NEW(c_parseroptions)();
  cloneSet(obj);
  return obj;
}
void c_parseroptions::cloneSet(c_parseroptions *clone) {
  clone->m_mUseTeX = m_mUseTeX.isReferenced() ? ref(m_mUseTeX) : m_mUseTeX;
  clone->m_mUseDynamicDates = m_mUseDynamicDates.isReferenced() ? ref(m_mUseDynamicDates) : m_mUseDynamicDates;
  clone->m_mInterwikiMagic = m_mInterwikiMagic.isReferenced() ? ref(m_mInterwikiMagic) : m_mInterwikiMagic;
  clone->m_mAllowExternalImages = m_mAllowExternalImages.isReferenced() ? ref(m_mAllowExternalImages) : m_mAllowExternalImages;
  clone->m_mAllowExternalImagesFrom = m_mAllowExternalImagesFrom.isReferenced() ? ref(m_mAllowExternalImagesFrom) : m_mAllowExternalImagesFrom;
  clone->m_mEnableImageWhitelist = m_mEnableImageWhitelist.isReferenced() ? ref(m_mEnableImageWhitelist) : m_mEnableImageWhitelist;
  clone->m_mSkin = m_mSkin.isReferenced() ? ref(m_mSkin) : m_mSkin;
  clone->m_mDateFormat = m_mDateFormat.isReferenced() ? ref(m_mDateFormat) : m_mDateFormat;
  clone->m_mEditSection = m_mEditSection.isReferenced() ? ref(m_mEditSection) : m_mEditSection;
  clone->m_mNumberHeadings = m_mNumberHeadings.isReferenced() ? ref(m_mNumberHeadings) : m_mNumberHeadings;
  clone->m_mAllowSpecialInclusion = m_mAllowSpecialInclusion.isReferenced() ? ref(m_mAllowSpecialInclusion) : m_mAllowSpecialInclusion;
  clone->m_mTidy = m_mTidy.isReferenced() ? ref(m_mTidy) : m_mTidy;
  clone->m_mInterfaceMessage = m_mInterfaceMessage.isReferenced() ? ref(m_mInterfaceMessage) : m_mInterfaceMessage;
  clone->m_mTargetLanguage = m_mTargetLanguage.isReferenced() ? ref(m_mTargetLanguage) : m_mTargetLanguage;
  clone->m_mMaxIncludeSize = m_mMaxIncludeSize.isReferenced() ? ref(m_mMaxIncludeSize) : m_mMaxIncludeSize;
  clone->m_mMaxPPNodeCount = m_mMaxPPNodeCount.isReferenced() ? ref(m_mMaxPPNodeCount) : m_mMaxPPNodeCount;
  clone->m_mMaxPPExpandDepth = m_mMaxPPExpandDepth.isReferenced() ? ref(m_mMaxPPExpandDepth) : m_mMaxPPExpandDepth;
  clone->m_mMaxTemplateDepth = m_mMaxTemplateDepth.isReferenced() ? ref(m_mMaxTemplateDepth) : m_mMaxTemplateDepth;
  clone->m_mRemoveComments = m_mRemoveComments.isReferenced() ? ref(m_mRemoveComments) : m_mRemoveComments;
  clone->m_mTemplateCallback = m_mTemplateCallback.isReferenced() ? ref(m_mTemplateCallback) : m_mTemplateCallback;
  clone->m_mEnableLimitReport = m_mEnableLimitReport.isReferenced() ? ref(m_mEnableLimitReport) : m_mEnableLimitReport;
  clone->m_mTimestamp = m_mTimestamp.isReferenced() ? ref(m_mTimestamp) : m_mTimestamp;
  clone->m_mExternalLinkTarget = m_mExternalLinkTarget.isReferenced() ? ref(m_mExternalLinkTarget) : m_mExternalLinkTarget;
  clone->m_mUser = m_mUser.isReferenced() ? ref(m_mUser) : m_mUser;
  clone->m_mIsPreview = m_mIsPreview.isReferenced() ? ref(m_mIsPreview) : m_mIsPreview;
  clone->m_mIsSectionPreview = m_mIsSectionPreview.isReferenced() ? ref(m_mIsSectionPreview) : m_mIsSectionPreview;
  clone->m_mIsPrintable = m_mIsPrintable.isReferenced() ? ref(m_mIsPrintable) : m_mIsPrintable;
  ObjectData::cloneSet(clone);
}
Variant c_parseroptions::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x7533D7D1FB463C81LL, gettimestamp) {
        return (t_gettimestamp());
      }
      HASH_GUARD(0x01659C897F5E99C1LL, getusedynamicdates) {
        return (t_getusedynamicdates());
      }
      break;
    case 6:
      HASH_GUARD(0x269DAD3410FFF8C6LL, getinterwikimagic) {
        return (t_getinterwikimagic());
      }
      break;
    case 9:
      HASH_GUARD(0x6EF12DB3679FD689LL, getdateformat) {
        return (t_getdateformat());
      }
      break;
    case 16:
      HASH_GUARD(0x47CB1D8AB54E1850LL, getinterfacemessage) {
        return (t_getinterfacemessage());
      }
      break;
    case 19:
      HASH_GUARD(0x2C2CE4CD607BE9D3LL, getusetex) {
        return (t_getusetex());
      }
      break;
    case 20:
      HASH_GUARD(0x4E73BF37061917D4LL, getremovecomments) {
        return (t_getremovecomments());
      }
      break;
    case 21:
      HASH_GUARD(0x5F1B74D4BEBB6CD5LL, geteditsection) {
        return (t_geteditsection());
      }
      break;
    case 28:
      HASH_GUARD(0x244B3592B556B85CLL, initialisefromuser) {
        return (t_initialisefromuser(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x6C9C5936F7FFEA1CLL, getallowspecialinclusion) {
        return (t_getallowspecialinclusion());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x6C79601E06BA735FLL, getskin) {
        return (t_getskin());
      }
      break;
    case 35:
      HASH_GUARD(0x4C54B0BC426A6EE3LL, getmaxtemplatedepth) {
        return (t_getmaxtemplatedepth());
      }
      break;
    case 42:
      HASH_GUARD(0x66650D0ADB29FEAALL, gettargetlanguage) {
        return (t_gettargetlanguage());
      }
      break;
    case 46:
      HASH_GUARD(0x24F0B1CAFFCAC26ELL, getnumberheadings) {
        return (t_getnumberheadings());
      }
      break;
    case 50:
      HASH_GUARD(0x3CCD270F1E382832LL, getexternallinktarget) {
        return (t_getexternallinktarget());
      }
      break;
    case 51:
      HASH_GUARD(0x49A4385734AC6E73LL, getcleansignatures) {
        return (t_getcleansignatures());
      }
      break;
    case 53:
      HASH_GUARD(0x4EDD9E12C4CE34F5LL, gettemplatecallback) {
        return (t_gettemplatecallback());
      }
      HASH_GUARD(0x5356D47AAA28FDF5LL, getmaxincludesize) {
        return (t_getmaxincludesize());
      }
      break;
    case 55:
      HASH_GUARD(0x500FFF199D084BF7LL, getenableimagewhitelist) {
        return (t_getenableimagewhitelist());
      }
      HASH_GUARD(0x09A86C9F3335CAB7LL, getallowexternalimages) {
        return (t_getallowexternalimages());
      }
      break;
    case 57:
      HASH_GUARD(0x22B0D101E5B6C279LL, getenablelimitreport) {
        return (t_getenablelimitreport());
      }
      break;
    case 61:
      HASH_GUARD(0x6256ABE618956C3DLL, getisprintable) {
        return (t_getisprintable());
      }
      break;
    case 62:
      HASH_GUARD(0x328637508AD8D9BELL, getallowexternalimagesfrom) {
        return (t_getallowexternalimagesfrom());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_parseroptions::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 1:
      HASH_GUARD(0x7533D7D1FB463C81LL, gettimestamp) {
        return (t_gettimestamp());
      }
      HASH_GUARD(0x01659C897F5E99C1LL, getusedynamicdates) {
        return (t_getusedynamicdates());
      }
      break;
    case 6:
      HASH_GUARD(0x269DAD3410FFF8C6LL, getinterwikimagic) {
        return (t_getinterwikimagic());
      }
      break;
    case 9:
      HASH_GUARD(0x6EF12DB3679FD689LL, getdateformat) {
        return (t_getdateformat());
      }
      break;
    case 16:
      HASH_GUARD(0x47CB1D8AB54E1850LL, getinterfacemessage) {
        return (t_getinterfacemessage());
      }
      break;
    case 19:
      HASH_GUARD(0x2C2CE4CD607BE9D3LL, getusetex) {
        return (t_getusetex());
      }
      break;
    case 20:
      HASH_GUARD(0x4E73BF37061917D4LL, getremovecomments) {
        return (t_getremovecomments());
      }
      break;
    case 21:
      HASH_GUARD(0x5F1B74D4BEBB6CD5LL, geteditsection) {
        return (t_geteditsection());
      }
      break;
    case 28:
      HASH_GUARD(0x244B3592B556B85CLL, initialisefromuser) {
        return (t_initialisefromuser(a0), null);
      }
      HASH_GUARD(0x6C9C5936F7FFEA1CLL, getallowspecialinclusion) {
        return (t_getallowspecialinclusion());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      HASH_GUARD(0x6C79601E06BA735FLL, getskin) {
        return (t_getskin());
      }
      break;
    case 35:
      HASH_GUARD(0x4C54B0BC426A6EE3LL, getmaxtemplatedepth) {
        return (t_getmaxtemplatedepth());
      }
      break;
    case 42:
      HASH_GUARD(0x66650D0ADB29FEAALL, gettargetlanguage) {
        return (t_gettargetlanguage());
      }
      break;
    case 46:
      HASH_GUARD(0x24F0B1CAFFCAC26ELL, getnumberheadings) {
        return (t_getnumberheadings());
      }
      break;
    case 50:
      HASH_GUARD(0x3CCD270F1E382832LL, getexternallinktarget) {
        return (t_getexternallinktarget());
      }
      break;
    case 51:
      HASH_GUARD(0x49A4385734AC6E73LL, getcleansignatures) {
        return (t_getcleansignatures());
      }
      break;
    case 53:
      HASH_GUARD(0x4EDD9E12C4CE34F5LL, gettemplatecallback) {
        return (t_gettemplatecallback());
      }
      HASH_GUARD(0x5356D47AAA28FDF5LL, getmaxincludesize) {
        return (t_getmaxincludesize());
      }
      break;
    case 55:
      HASH_GUARD(0x500FFF199D084BF7LL, getenableimagewhitelist) {
        return (t_getenableimagewhitelist());
      }
      HASH_GUARD(0x09A86C9F3335CAB7LL, getallowexternalimages) {
        return (t_getallowexternalimages());
      }
      break;
    case 57:
      HASH_GUARD(0x22B0D101E5B6C279LL, getenablelimitreport) {
        return (t_getenablelimitreport());
      }
      break;
    case 61:
      HASH_GUARD(0x6256ABE618956C3DLL, getisprintable) {
        return (t_getisprintable());
      }
      break;
    case 62:
      HASH_GUARD(0x328637508AD8D9BELL, getallowexternalimagesfrom) {
        return (t_getallowexternalimagesfrom());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parseroptions::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parseroptions$os_get(const char *s) {
  return c_parseroptions::os_get(s, -1);
}
Variant &cw_parseroptions$os_lval(const char *s) {
  return c_parseroptions::os_lval(s, -1);
}
Variant cw_parseroptions$os_constant(const char *s) {
  return c_parseroptions::os_constant(s);
}
Variant cw_parseroptions$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parseroptions::os_invoke(c, s, params, -1, fatal);
}
void c_parseroptions::init() {
  m_mUseTeX = null;
  m_mUseDynamicDates = null;
  m_mInterwikiMagic = null;
  m_mAllowExternalImages = null;
  m_mAllowExternalImagesFrom = null;
  m_mEnableImageWhitelist = null;
  m_mSkin = null;
  m_mDateFormat = null;
  m_mEditSection = null;
  m_mNumberHeadings = null;
  m_mAllowSpecialInclusion = null;
  m_mTidy = null;
  m_mInterfaceMessage = null;
  m_mTargetLanguage = null;
  m_mMaxIncludeSize = null;
  m_mMaxPPNodeCount = null;
  m_mMaxPPExpandDepth = null;
  m_mMaxTemplateDepth = null;
  m_mRemoveComments = null;
  m_mTemplateCallback = null;
  m_mEnableLimitReport = null;
  m_mTimestamp = null;
  m_mExternalLinkTarget = null;
  m_mUser = null;
  m_mIsPreview = null;
  m_mIsSectionPreview = null;
  m_mIsPrintable = null;
}
/* SRC: ParserOptions.php line 40 */
Variant c_parseroptions::t_getusetex() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getUseTeX);
  return m_mUseTeX;
} /* function */
/* SRC: ParserOptions.php line 41 */
Variant c_parseroptions::t_getusedynamicdates() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getUseDynamicDates);
  return m_mUseDynamicDates;
} /* function */
/* SRC: ParserOptions.php line 42 */
Variant c_parseroptions::t_getinterwikimagic() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getInterwikiMagic);
  return m_mInterwikiMagic;
} /* function */
/* SRC: ParserOptions.php line 43 */
Variant c_parseroptions::t_getallowexternalimages() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getAllowExternalImages);
  return m_mAllowExternalImages;
} /* function */
/* SRC: ParserOptions.php line 44 */
Variant c_parseroptions::t_getallowexternalimagesfrom() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getAllowExternalImagesFrom);
  return m_mAllowExternalImagesFrom;
} /* function */
/* SRC: ParserOptions.php line 45 */
Variant c_parseroptions::t_getenableimagewhitelist() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getEnableImageWhitelist);
  return m_mEnableImageWhitelist;
} /* function */
/* SRC: ParserOptions.php line 46 */
Variant c_parseroptions::t_geteditsection() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getEditSection);
  return m_mEditSection;
} /* function */
/* SRC: ParserOptions.php line 47 */
Variant c_parseroptions::t_getnumberheadings() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getNumberHeadings);
  return m_mNumberHeadings;
} /* function */
/* SRC: ParserOptions.php line 48 */
Variant c_parseroptions::t_getallowspecialinclusion() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getAllowSpecialInclusion);
  return m_mAllowSpecialInclusion;
} /* function */
/* SRC: ParserOptions.php line 49 */
Variant c_parseroptions::t_gettidy() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getTidy);
  return m_mTidy;
} /* function */
/* SRC: ParserOptions.php line 50 */
Variant c_parseroptions::t_getinterfacemessage() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getInterfaceMessage);
  return m_mInterfaceMessage;
} /* function */
/* SRC: ParserOptions.php line 51 */
Variant c_parseroptions::t_gettargetlanguage() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getTargetLanguage);
  return m_mTargetLanguage;
} /* function */
/* SRC: ParserOptions.php line 52 */
Variant c_parseroptions::t_getmaxincludesize() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getMaxIncludeSize);
  return m_mMaxIncludeSize;
} /* function */
/* SRC: ParserOptions.php line 53 */
Variant c_parseroptions::t_getmaxppnodecount() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getMaxPPNodeCount);
  return m_mMaxPPNodeCount;
} /* function */
/* SRC: ParserOptions.php line 54 */
Variant c_parseroptions::t_getmaxtemplatedepth() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getMaxTemplateDepth);
  return m_mMaxTemplateDepth;
} /* function */
/* SRC: ParserOptions.php line 55 */
Variant c_parseroptions::t_getremovecomments() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getRemoveComments);
  return m_mRemoveComments;
} /* function */
/* SRC: ParserOptions.php line 56 */
Variant c_parseroptions::t_gettemplatecallback() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getTemplateCallback);
  return m_mTemplateCallback;
} /* function */
/* SRC: ParserOptions.php line 57 */
Variant c_parseroptions::t_getenablelimitreport() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getEnableLimitReport);
  return m_mEnableLimitReport;
} /* function */
/* SRC: ParserOptions.php line 58 */
Variant c_parseroptions::t_getcleansignatures() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getCleanSignatures);
  return o_get("mCleanSignatures", 0x6FEC17D82AD5A12BLL);
} /* function */
/* SRC: ParserOptions.php line 59 */
Variant c_parseroptions::t_getexternallinktarget() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getExternalLinkTarget);
  return m_mExternalLinkTarget;
} /* function */
/* SRC: ParserOptions.php line 60 */
Variant c_parseroptions::t_getispreview() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getIsPreview);
  return m_mIsPreview;
} /* function */
/* SRC: ParserOptions.php line 61 */
Variant c_parseroptions::t_getissectionpreview() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getIsSectionPreview);
  return m_mIsSectionPreview;
} /* function */
/* SRC: ParserOptions.php line 62 */
Variant c_parseroptions::t_getisprintable() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getIsPrintable);
  return m_mIsPrintable;
} /* function */
/* SRC: ParserOptions.php line 64 */
Variant c_parseroptions::t_getskin() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getSkin);
  if (!(t___isset("mSkin"))) {
    (m_mSkin = LINE(66,m_mUser.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  }
  return m_mSkin;
} /* function */
/* SRC: ParserOptions.php line 71 */
Variant c_parseroptions::t_getdateformat() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getDateFormat);
  if (!(t___isset("mDateFormat"))) {
    (m_mDateFormat = LINE(73,m_mUser.o_invoke_few_args("getDatePreference", 0x1A469A31076AB82CLL, 0)));
  }
  return m_mDateFormat;
} /* function */
/* SRC: ParserOptions.php line 78 */
Variant c_parseroptions::t_gettimestamp() {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::getTimestamp);
  if (!(t___isset("mTimestamp"))) {
    (m_mTimestamp = LINE(80,invoke_failed("wftimestampnow", Array(), 0x00000000151D8950LL)));
  }
  return m_mTimestamp;
} /* function */
/* SRC: ParserOptions.php line 85 */
Variant c_parseroptions::t_setusetex(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setUseTeX);
  return LINE(85,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mUseTeX)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 86 */
Variant c_parseroptions::t_setusedynamicdates(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setUseDynamicDates);
  return LINE(86,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mUseDynamicDates)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 87 */
Variant c_parseroptions::t_setinterwikimagic(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setInterwikiMagic);
  return LINE(87,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mInterwikiMagic)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 88 */
Variant c_parseroptions::t_setallowexternalimages(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setAllowExternalImages);
  return LINE(88,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mAllowExternalImages)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 89 */
Variant c_parseroptions::t_setallowexternalimagesfrom(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setAllowExternalImagesFrom);
  return LINE(89,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mAllowExternalImagesFrom)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 90 */
Variant c_parseroptions::t_setenableimagewhitelist(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setEnableImageWhitelist);
  return LINE(90,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mEnableImageWhitelist)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 91 */
Variant c_parseroptions::t_setdateformat(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setDateFormat);
  return LINE(91,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mDateFormat)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 92 */
Variant c_parseroptions::t_seteditsection(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setEditSection);
  return LINE(92,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mEditSection)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 93 */
Variant c_parseroptions::t_setnumberheadings(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setNumberHeadings);
  return LINE(93,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mNumberHeadings)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 94 */
Variant c_parseroptions::t_setallowspecialinclusion(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setAllowSpecialInclusion);
  return LINE(94,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mAllowSpecialInclusion)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 95 */
Variant c_parseroptions::t_settidy(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setTidy);
  return LINE(95,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mTidy)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 96 */
void c_parseroptions::t_setskin(CVarRef v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setSkin);
  (m_mSkin = v_x);
} /* function */
/* SRC: ParserOptions.php line 97 */
Variant c_parseroptions::t_setinterfacemessage(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setInterfaceMessage);
  return LINE(97,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mInterfaceMessage)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 98 */
Variant c_parseroptions::t_settargetlanguage(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setTargetLanguage);
  return LINE(98,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mTargetLanguage)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 99 */
Variant c_parseroptions::t_setmaxincludesize(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setMaxIncludeSize);
  return LINE(99,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mMaxIncludeSize)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 100 */
Variant c_parseroptions::t_setmaxppnodecount(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setMaxPPNodeCount);
  return LINE(100,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mMaxPPNodeCount)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 101 */
Variant c_parseroptions::t_setmaxtemplatedepth(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setMaxTemplateDepth);
  return LINE(101,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mMaxTemplateDepth)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 102 */
Variant c_parseroptions::t_setremovecomments(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setRemoveComments);
  return LINE(102,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mRemoveComments)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 103 */
Variant c_parseroptions::t_settemplatecallback(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setTemplateCallback);
  return LINE(103,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mTemplateCallback)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 104 */
Variant c_parseroptions::t_enablelimitreport(Variant v_x //  = true
) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::enableLimitReport);
  return LINE(104,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mEnableLimitReport)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 105 */
Variant c_parseroptions::t_settimestamp(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setTimestamp);
  return LINE(105,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mTimestamp)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 106 */
Variant c_parseroptions::t_setcleansignatures(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setCleanSignatures);
  return LINE(106,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(o_lval("mCleanSignatures", 0x6FEC17D82AD5A12BLL))).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 107 */
Variant c_parseroptions::t_setexternallinktarget(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setExternalLinkTarget);
  return LINE(107,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mExternalLinkTarget)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 108 */
Variant c_parseroptions::t_setispreview(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setIsPreview);
  return LINE(108,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mIsPreview)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 109 */
Variant c_parseroptions::t_setissectionpreview(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setIsSectionPreview);
  return LINE(109,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mIsSectionPreview)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 110 */
Variant c_parseroptions::t_setisprintable(Variant v_x) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::setIsPrintable);
  return LINE(110,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mIsPrintable)).set(1, ref(v_x)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOptions.php line 112 */
void c_parseroptions::t___construct(Variant v_user //  = null
) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(113,t_initialisefromuser(v_user));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: ParserOptions.php line 120 */
p_parseroptions c_parseroptions::ti_newfromuser(const char* cls, CVarRef v_user) {
  STATIC_METHOD_INJECTION(ParserOptions, ParserOptions::newFromUser);
  return ((Object)(LINE(121,p_parseroptions(p_parseroptions(NEWOBJ(c_parseroptions)())->create(v_user)))));
} /* function */
/* SRC: ParserOptions.php line 125 */
void c_parseroptions::t_initialisefromuser(Variant v_userInput) {
  INSTANCE_METHOD_INJECTION(ParserOptions, ParserOptions::initialiseFromUser);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgUseTeX __attribute__((__unused__)) = g->GV(wgUseTeX);
  Variant &gv_wgUseDynamicDates __attribute__((__unused__)) = g->GV(wgUseDynamicDates);
  Variant &gv_wgInterwikiMagic __attribute__((__unused__)) = g->GV(wgInterwikiMagic);
  Variant &gv_wgAllowExternalImages __attribute__((__unused__)) = g->GV(wgAllowExternalImages);
  Variant &gv_wgAllowExternalImagesFrom __attribute__((__unused__)) = g->GV(wgAllowExternalImagesFrom);
  Variant &gv_wgEnableImageWhitelist __attribute__((__unused__)) = g->GV(wgEnableImageWhitelist);
  Variant &gv_wgAllowSpecialInclusion __attribute__((__unused__)) = g->GV(wgAllowSpecialInclusion);
  Variant &gv_wgMaxArticleSize __attribute__((__unused__)) = g->GV(wgMaxArticleSize);
  Variant &gv_wgMaxPPNodeCount __attribute__((__unused__)) = g->GV(wgMaxPPNodeCount);
  Variant &gv_wgMaxTemplateDepth __attribute__((__unused__)) = g->GV(wgMaxTemplateDepth);
  Variant &gv_wgMaxPPExpandDepth __attribute__((__unused__)) = g->GV(wgMaxPPExpandDepth);
  Variant &gv_wgCleanSignatures __attribute__((__unused__)) = g->GV(wgCleanSignatures);
  Variant &gv_wgExternalLinkTarget __attribute__((__unused__)) = g->GV(wgExternalLinkTarget);
  Variant v_fname;
  Variant &gv_wgUser __attribute__((__unused__)) = g->GV(wgUser);
  Variant v_wgUser;
  Variant v_user;

  {
  }
  {
  }
  {
  }
  (v_fname = "ParserOptions::initialiseFromUser");
  LINE(131,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, ref(v_fname)).create()), 0x0000000075359BAFLL));
  if (!(toBoolean(v_userInput))) {
    v_wgUser = ref(g->GV(wgUser));
    if (isset(v_wgUser)) {
      (v_user = v_wgUser);
    }
    else {
      (v_user = LINE(137,create_object("user", Array())));
    }
  }
  else {
    (v_user = ref(v_userInput));
  }
  (m_mUser = v_user);
  (m_mUseTeX = gv_wgUseTeX);
  (m_mUseDynamicDates = gv_wgUseDynamicDates);
  (m_mInterwikiMagic = gv_wgInterwikiMagic);
  (m_mAllowExternalImages = gv_wgAllowExternalImages);
  (m_mAllowExternalImagesFrom = gv_wgAllowExternalImagesFrom);
  (m_mEnableImageWhitelist = gv_wgEnableImageWhitelist);
  (m_mSkin = null);
  (m_mDateFormat = null);
  (m_mEditSection = true);
  (m_mNumberHeadings = LINE(154,v_user.o_invoke_few_args("getOption", 0x402DD1A85CAEA6C0LL, 1, "numberheadings")));
  (m_mAllowSpecialInclusion = gv_wgAllowSpecialInclusion);
  (m_mTidy = false);
  (m_mInterfaceMessage = false);
  (m_mTargetLanguage = null);
  (m_mMaxIncludeSize = gv_wgMaxArticleSize * 1024LL);
  (m_mMaxPPNodeCount = gv_wgMaxPPNodeCount);
  (m_mMaxPPExpandDepth = gv_wgMaxPPExpandDepth);
  (m_mMaxTemplateDepth = gv_wgMaxTemplateDepth);
  (m_mRemoveComments = true);
  (m_mTemplateCallback = ScalarArrays::sa_[1]);
  (m_mEnableLimitReport = false);
  (o_lval("mCleanSignatures", 0x6FEC17D82AD5A12BLL) = gv_wgCleanSignatures);
  (m_mExternalLinkTarget = gv_wgExternalLinkTarget);
  (m_mIsPreview = false);
  (m_mIsSectionPreview = false);
  LINE(170,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, ref(v_fname)).create()), 0x00000000B599F276LL));
} /* function */
Object co_parseroptions(CArrRef params, bool init /* = true */) {
  return Object(p_parseroptions(NEW(c_parseroptions)())->dynCreate(params, init));
}
Variant pm_php$ParserOptions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::ParserOptions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$ParserOptions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
