
#ifndef __GENERATED_cls_xmlselect_h__
#define __GENERATED_cls_xmlselect_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 16029 */
class c_xmlselect : virtual public ObjectData {
  BEGIN_CLASS_MAP(xmlselect)
  END_CLASS_MAP(xmlselect)
  DECLARE_CLASS(xmlselect, XmlSelect, ObjectData)
  void init();
  public: Variant m_options;
  public: Variant m_default;
  public: Variant m_attributes;
  public: void t___construct(Variant v_name = false, Variant v_id = false, Variant v_default = false);
  public: ObjectData *create(Variant v_name = false, Variant v_id = false, Variant v_default = false);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_setdefault(CVarRef v_default);
  public: void t_setattribute(CStrRef v_name, CVarRef v_value);
  public: void t_addoption(CVarRef v_name, Variant v_value = false);
  public: String t_gethtml();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_xmlselect_h__
