
#ifndef __GENERATED_cls_linker_h__
#define __GENERATED_cls_linker_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 1413 */
class c_linker : virtual public ObjectData {
  BEGIN_CLASS_MAP(linker)
  END_CLASS_MAP(linker)
  DECLARE_CLASS(linker, Linker, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_postparselinkcolour(CVarRef v_s = null_variant);
  public: String t_getexternallinkattributes(CVarRef v_title, CVarRef v_unused = null_variant, CVarRef v_class = "");
  public: String t_getinterwikilinkattributes(Variant v_title, CVarRef v_unused = null_variant, CStrRef v_class = "");
  public: String t_getinternallinkattributes(Variant v_title, CVarRef v_unused = null_variant, CStrRef v_class = "");
  public: String t_getinternallinkattributesobj(CVarRef v_nt, CVarRef v_unused = null_variant, CVarRef v_class = "", Variant v_title = false);
  public: String t_getlinkattributesinternal(Variant v_title, Variant v_class, CVarRef v_classDefault = false);
  public: String t_getlinkcolour(CVarRef v_t, CVarRef v_threshold);
  public: Variant t_link(Variant v_target, Variant v_text = null, Variant v_customAttribs = ScalarArrays::sa_[0], Variant v_query = ScalarArrays::sa_[0], Variant v_options = ScalarArrays::sa_[0]);
  public: Variant t_linkurl(Variant v_target, Variant v_query, CVarRef v_options);
  public: Variant t_linkattribs(CVarRef v_target, CVarRef v_attribs, CVarRef v_options);
  public: String t_linktext(CVarRef v_target);
  public: Variant t_makelink(CVarRef v_title, CVarRef v_text = "", CVarRef v_query = "", CVarRef v_trail = "");
  public: Variant t_makeknownlink(CVarRef v_title, CVarRef v_text = "", CStrRef v_query = "", CStrRef v_trail = "", CStrRef v_prefix = "", CStrRef v_aprops = "");
  public: Variant t_makebrokenlink(CVarRef v_title, CStrRef v_text = "", CStrRef v_query = "", CStrRef v_trail = "");
  public: Variant t_makestublink(CVarRef v_title, CStrRef v_text = "", CStrRef v_query = "", CStrRef v_trail = "");
  public: String t_makelinkobj(CVarRef v_nt, Variant v_text = "", Variant v_query = "", Variant v_trail = "", CVarRef v_prefix = "");
  public: String t_makeknownlinkobj(CVarRef v_title, Variant v_text = "", Variant v_query = "", Variant v_trail = "", CVarRef v_prefix = "", CVarRef v_aprops = "", CVarRef v_style = "");
  public: String t_makebrokenlinkobj(CVarRef v_title, Variant v_text = "", CVarRef v_query = "", Variant v_trail = "", CVarRef v_prefix = "");
  public: String t_makestublinkobj(CVarRef v_nt, CStrRef v_text = "", CStrRef v_query = "", CStrRef v_trail = "", CStrRef v_prefix = "");
  public: String t_makecolouredlinkobj(CVarRef v_nt, CVarRef v_colour, CVarRef v_text = "", CVarRef v_query = "", CVarRef v_trail = "", CVarRef v_prefix = "");
  public: String t_makesizelinkobj(CVarRef v_size, CVarRef v_nt, CStrRef v_text = "", CStrRef v_query = "", CStrRef v_trail = "", CStrRef v_prefix = "");
  public: String t_makeselflinkobj(CVarRef v_nt, Variant v_text = "", CVarRef v_query = "", Variant v_trail = "", CVarRef v_prefix = "");
  public: Variant t_normalisespecialpage(CVarRef v_title);
  public: Variant t_fnamepart(CVarRef v_url);
  public: Variant t_makeimage(CVarRef v_url, CStrRef v_alt = "");
  public: Variant t_makeexternalimage(Variant v_url, Variant v_alt = "");
  public: Variant t_makeimagelinkobj(CVarRef v_title, CVarRef v_label, CVarRef v_alt, CStrRef v_align = "", CArrRef v_handlerParams = ScalarArrays::sa_[0], bool v_framed = false, bool v_thumb = false, CStrRef v_manualthumb = "", CStrRef v_valign = "", bool v_time = false);
  public: Variant t_makeimagelink2(Variant v_title, Variant v_file, Variant v_frameParams = ScalarArrays::sa_[0], Variant v_handlerParams = ScalarArrays::sa_[0], Variant v_time = false, CVarRef v_query = "");
  public: Variant t_makethumblinkobj(p_title v_title, CVarRef v_file, CStrRef v_label = "", CVarRef v_alt = null_variant, CStrRef v_align = "right", CArrRef v_params = ScalarArrays::sa_[0], bool v_framed = false, CStrRef v_manualthumb = "");
  public: Variant t_makethumblink2(CVarRef v_title, CVarRef v_file, Variant v_frameParams = ScalarArrays::sa_[0], Variant v_handlerParams = ScalarArrays::sa_[0], CVarRef v_time = false, Variant v_query = "");
  public: Variant t_makebrokenimagelinkobj(Variant v_title, String v_text = "", CStrRef v_query = "", Variant v_trail = "", CStrRef v_prefix = "", bool v_time = false);
  public: Variant t_makemedialink(CVarRef v_name, CStrRef v_unused = "", CVarRef v_text = "", bool v_time = false);
  public: Variant t_makemedialinkobj(CVarRef v_title, Variant v_text = "", CVarRef v_time = false);
  public: Variant t_speciallink(Variant v_name, String v_key = "");
  public: Variant t_makeexternallink(Variant v_url, Variant v_text, CVarRef v_escape = true, CVarRef v_linktype = "", CVarRef v_attribs = ScalarArrays::sa_[0]);
  public: Variant t_userlink(CVarRef v_userId, CVarRef v_userText);
  public: Variant t_usertoollinks(CVarRef v_userId, CVarRef v_userText, CVarRef v_redContribsWhenNoEdits = false, CVarRef v_flags = 0LL, CVarRef v_edits = null_variant);
  public: Variant t_usertoollinksredcontribs(CVarRef v_userId, CVarRef v_userText, CVarRef v_edits = null_variant);
  public: Variant t_usertalklink(CVarRef v_userId, CVarRef v_userText);
  public: Variant t_blocklink(CVarRef v_userId, CVarRef v_userText);
  public: Variant t_revuserlink(Object v_rev, bool v_isPublic = false);
  public: Variant t_revusertools(CVarRef v_rev, CVarRef v_isPublic = false);
  public: Variant t_formatcomment(Variant v_comment, CVarRef v_title = null_variant, CVarRef v_local = false);
  public: Variant t_formatautocomments(Variant v_comment, CVarRef v_title = null_variant, CVarRef v_local = false);
  public: String t_formatautocommentscallback(CVarRef v_match);
  public: Variant t_formatlinksincomment(CVarRef v_comment);
  public: Variant t_formatlinksincommentcallback(Variant v_match);
  public: Variant t_commentblock(CVarRef v_comment, CVarRef v_title = null_variant, bool v_local = false);
  public: Variant t_revcomment(p_revision v_rev, bool v_local = false, bool v_isPublic = false);
  public: String t_formatrevisionsize(Variant v_size);
  public: String t_tocindent();
  public: String t_tocunindent(CVarRef v_level);
  public: String t_tocline(CVarRef v_anchor, CVarRef v_tocline, CVarRef v_tocnumber, CVarRef v_level);
  public: String t_toclineend();
  public: String t_toclist(CVarRef v_toc);
  public: Variant t_editsectionlinkforother(Variant v_title, CVarRef v_section);
  public: Variant t_editsectionlink(p_title v_nt, CVarRef v_section, Variant v_hint = "");
  public: Variant t_doeditsectionlink(CVarRef v_nt, CVarRef v_section, CVarRef v_tooltip = null_variant);
  public: String t_makeheadline(CVarRef v_level, CVarRef v_attribs, CVarRef v_anchor, CVarRef v_text, CVarRef v_link, CVarRef v_legacyAnchor = false);
  public: static Array ti_splittrail(const char* cls, Variant v_trail);
  public: String t_generaterollback(CVarRef v_rev);
  public: Variant t_buildrollbacklink(CVarRef v_rev);
  public: String t_formattemplates(Variant v_templates, bool v_preview = false, bool v_section = false);
  public: String t_formathiddencategories(CArrRef v_hiddencats);
  public: String t_formatsize(Variant v_size);
  public: Variant t_tooltipandaccesskey(CVarRef v_name);
  public: Variant t_tooltip(CVarRef v_name, CVarRef v_options = null_variant);
  public: Variant t_titleattrib(CVarRef v_name, CVarRef v_options = null_variant);
  public: Variant t_accesskey(CVarRef v_name);
  public: String t_revdeletelink(CArrRef v_query = ScalarArrays::sa_[0], bool v_restricted = false);
  public: static Array t_splittrail(CVarRef v_trail) { return ti_splittrail("linker", v_trail); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_linker_h__
