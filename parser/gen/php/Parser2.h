
#ifndef __GENERATED_php_Parser2_h__
#define __GENERATED_php_Parser2_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/Parser2.fw.h>

// Declarations
#include <cls/title.h>
#include <cls/request.h>
#include <cls/errorpageerror.h>
#include <cls/stubuser.h>
#include <cls/magicwordarray.h>
#include <cls/linker.h>
#include <cls/utfnormal.h>
#include <cls/stripstate.h>
#include <cls/hashtablereplacer.h>
#include <cls/explodeiterator.h>
#include <cls/linksupdate.h>
#include <cls/getdb.h>
#include <cls/parsertest.h>
#include <cls/htmlcacheupdate.h>
#include <cls/interwiki.h>
#include <cls/messagecache.h>
#include <cls/htmlfilecache.h>
#include <cls/ansitermcolorer.h>
#include <cls/stubobject.h>
#include <cls/doublereplacer.h>
#include <cls/status.h>
#include <cls/mwexception.h>
#include <cls/fakememcachedclient.h>
#include <cls/replacer.h>
#include <cls/parseroptions.h>
#include <cls/ip.h>
#include <cls/outputpage.h>
#include <cls/ppdpart.h>
#include <cls/magicword.h>
#include <cls/revision.h>
#include <cls/ppdstackelement.h>
#include <cls/linkholderarray.h>
#include <cls/ppnode_dom.h>
#include <cls/fakeconverter.h>
#include <cls/skintemplate.h>
#include <cls/mediawiki_i18n.h>
#include <cls/memcached.h>
#include <cls/regexlikereplacer.h>
#include <cls/testrecorder.h>
#include <cls/proveparsertest.h>
#include <cls/parser.h>
#include <cls/stubuserlang.h>
#include <cls/ppdstack.h>
#include <cls/titlearrayfromresult.h>
#include <cls/provetestrecorder.h>
#include <cls/linkbatch.h>
#include <cls/preprocessor.h>
#include <cls/passworderror.h>
#include <cls/preprocessor_dom.h>
#include <cls/monobooktemplate.h>
#include <cls/stubcontlang.h>
#include <cls/article.h>
#include <cls/skin.h>
#include <cls/parseroutput.h>
#include <cls/skinmonobook.h>
#include <cls/ppframe_dom.h>
#include <cls/citerator.h>
#include <cls/ppcustomframe_dom.h>
#include <cls/ctraversable.h>
#include <cls/linkcache.h>
#include <cls/fatalerror.h>
#include <cls/language.h>
#include <cls/dbtestrecorder.h>
#include <cls/pptemplateframe_dom.h>
#include <cls/block.h>
#include <cls/onlyincludereplacer.h>
#include <cls/recentchange.h>
#include <cls/coreparserfunctions.h>
#include <cls/profiler.h>
#include <cls/dummytermcolorer.h>
#include <cls/titlearray.h>
#include <cls/user.h>
#include <cls/ppframe.h>
#include <cls/filecache.h>
#include <cls/xmlselect.h>
#include <cls/sitestatsupdate.h>
#include <cls/replacementarray.h>
#include <cls/ppnode.h>
#include <cls/memcachedclientforwiki.h>
#include <cls/quicktemplate.h>
#include <cls/searchupdate.h>
#include <cls/xml.h>
#include <cls/backlinkcache.h>
#include <cls/mwnamespace.h>
#include <cls/repogroup.h>
#include <cls/dbtestpreviewer.h>
#include <cls/sanitizer.h>
#include <cls/stringutils.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_is_deeply(Variant v_got, Variant v_expected, CStrRef v_desc = "");
void f_myexit();
Variant f__repl(CVarRef v_obj, bool v_deep = true);
Variant f_wfparserteststaticparserhookhook(CVarRef v_in, CVarRef v_argv, Variant v_parser);
String f_wfgeneratetoken(String v_salt = "");
Variant f_wfgetnull();
Variant f_wfmsg(int num_args, CVarRef v_key, Array args = Array());
void f_wfprinterror(CVarRef v_message);
void f_wfprofileout(Variant v_functionname = "missing");
Variant f_wfmsgreplaceargs(Variant v_message, Variant v_args);
void f_wfexceptionhandler(Variant v_e);
Variant f_wfwikiid(CVarRef v_db = null_variant);
Variant f_wfurlprotocols();
String f_wfpercent(Numeric v_nr, int64 v_acc = 2LL, bool v_round = true);
void f_isa_ok(CVarRef v_obj, CVarRef v_expected, CStrRef v_desc = "");
bool f_wfnodeletemainpage(Variant v_title, Variant v_user, CVarRef v_action, Variant v_result);
Variant f_wfgetmimemagic();
void f_wfdebug(Variant v_text, CVarRef v_logonly = false);
Variant f_wffindfile(Variant v_title, Variant v_time = false, Variant v_flags = 0LL, bool v_bypass = false);
void f_wfsuppresswarnings(bool v_end = false);
Variant f_wfistrustedproxy(Variant v_ip);
void f_wfusephp(CVarRef v_req_ver);
Variant f_wfgetprecompileddata(CStrRef v_name);
Variant f_wfbooltostr(CVarRef v_value);
Array f__diff(CStrRef v_gpath, CVarRef v_got, CStrRef v_epath, CVarRef v_expected);
String f_wfmakeurlindex(Primitive v_url);
void f_wfinitshelllocale();
Variant f_wfarraylookup(CVarRef v_a, CVarRef v_b);
void f_wfpurgesquidservers(CVarRef v_urlArr);
Variant f_codepointtoutf8(CVarRef v_codepoint);
Variant f_wfiswindows();
Variant f_wftime();
void f_wfprofilein(Variant v_functionname);
bool f_logprefsemail(CVarRef v_user, CVarRef v_old, CVarRef v_new);
Variant f_wfdebugbacktrace();
Variant f_wfmsgforcontent(int num_args, CVarRef v_key, Array args = Array());
String f_wfshellexec(String v_cmd, Variant v_retval = null);
void f_wfabruptexit(bool v_error = false);
Variant f_wfarraydiff2(CVarRef v_a, CVarRef v_b);
Variant f_wftimestampornull(int64 v_outputtype = 0LL /* TS_UNIX */, CVarRef v_ts = null_variant);
Variant f_cmp_ok(Variant v_got, CVarRef v_op, Variant v_expected, CStrRef v_desc = "");
Variant f_wfmsgforcontentnotrans(int num_args, CVarRef v_key, Array args = Array());
Variant f_wfstripillegalfilenamechars(Variant v_name);
void f_wfappendtoarrayifnotdefault(CVarRef v_key, CVarRef v_value, CVarRef v_default, Variant v_changed);
void f_wfmaxlagerror(CVarRef v_host, CVarRef v_lag, CVarRef v_maxLag);
Variant f_wfgethttp(CVarRef v_url, CStrRef v_timeout = "default");
Variant f_wfformatstackframe(CVarRef v_frame);
void f_wflogdberror(String v_text);
void f_wfsetupsession();
Variant f_wfappendquery(Variant v_url, CVarRef v_query);
Variant f__proclaim(CVarRef v_cond, Variant v_desc = "", bool v_todo = false, CVarRef v_got = null_variant, CVarRef v_expected = null_variant, bool v_negate = false);
Variant f_wfgetcachednotice(CVarRef v_name);
Variant f_is(CVarRef v_got, CVarRef v_expected, CVarRef v_desc = "");
void f_wferrorexit();
void f_wfloadextensionmessages(CVarRef v_extensionName, Variant v_langcode = false);
Variant f_wfgetprofilingoutput(Variant v_start, Variant v_elapsed);
Variant f_wfgetforwardedfor();
void f_wfclearoutputbuffers();
Variant f_wfcgitoarray(Variant v_query);
String f_wfescapeshellarg(int num_args, Array args = Array());
String f__idx(CVarRef v_obj, CStrRef v_path = "");
Variant f_wfmsgnodb(int num_args, CVarRef v_key, Array args = Array());
String f_wfnumlink(CVarRef v_offset, Variant v_limit, CVarRef v_title, CStrRef v_query = "");
Variant f_isnt(CVarRef v_got, CVarRef v_expected, CStrRef v_desc = "");
bool f_wfislocallyblockedproxy(CVarRef v_ip);
Variant f_wfgetcaller(CVarRef v_level = 2LL);
bool f_wfsetbit(Variant v_dest, CVarRef v_bit, bool v_state = true);
Variant f_wfmsgnotrans(int num_args, CVarRef v_key, Array args = Array());
void f_wfproxycheck();
Variant f_include_ok(Variant v_file, Variant v_desc = "");
bool f_wfemptymsg(CVarRef v_msg, CVarRef v_wfMsgOut);
Variant f_wfmsghtml(int num_args, CVarRef v_key, Array args = Array());
Variant f__cmp_deeply(CVarRef v_got, CVarRef v_exp, CStrRef v_path = "");
Variant f_wfaccepttoprefs(CVarRef v_accept, CStrRef v_def = "*/*");
void f_wfreportexception(CVarRef v_e);
Variant f_wfgetparsercachestorage();
Variant f_mimetypematch(CVarRef v_type, CVarRef v_avail);
void f_diag(String v_message);
Variant f_wfhostname();
Variant f_wfmsgwikihtml(int num_args, CVarRef v_key, Array args = Array());
Variant f_ok(CVarRef v_cond, CVarRef v_desc = "");
void f_wfout(CVarRef v_s);
Variant f_wfmsgweirdkey(CVarRef v_key);
Variant f_wfurlencode(Variant v_s);
Variant f_wfbaseconvert(Variant v_input, int64 v_sourceBase, int64 v_destBase, int64 v_pad = 1LL, bool v_lowercase = true);
void f_wfincrstats(Variant v_key);
Variant f_utf8tocodepoint(CVarRef v_char);
Array f_wfsplitwikiid(CVarRef v_wiki);
void f_wfrestorewarnings();
Variant f_wfcreateobject(CVarRef v_name, Variant v_p);
void f_wfseedrandom();
Variant f_wfregexreplacement(CVarRef v_string);
Variant f_wfgetsitenotice();
void f_todo_start(CStrRef v_why = "");
bool f_wmfcentralauthwikilist(Variant v_list);
void f_wfdoupdates();
String f_wfbacktrace();
Variant f_unlike(CVarRef v_got, CVarRef v_expected, CStrRef v_desc = "");
void f_wfdebugmem(bool v_exact = false);
void f_wfwaitforslaves(CVarRef v_maxLag);
Variant f_wfreadonlyreason();
void f_wfusemw(CVarRef v_req_ver);
bool f_wfparserteststaticparserhooksetup(Variant v_parser);
String f_wfforeignmemckey(int num_args, CVarRef v_db, CVarRef v_prefix, Array args = Array());
void f_taint(CVarRef v_var, int64 v_level = 0LL);
Variant f_wfnegotiatetype(CVarRef v_cprefs, CVarRef v_sprefs);
Variant pm_php$Parser2_php(bool incOnce = false, LVariableTable* variables = NULL);
bool f_logprefspassword(CVarRef v_user, CVarRef v_pass, CVarRef v_status);
Variant f_wfshowingresultsnum(CVarRef v_offset, Variant v_limit, Variant v_num);
Variant f_wfgetlangobj(CVarRef v_langcode = false);
int64 f_istainted(CVarRef v_var);
Variant f_wfquotedprintable(CVarRef v_string, Variant v_charset = "");
String f_wfmemckey(int num_args, Array args = Array());
void f_wfhttperror(CVarRef v_code, CVarRef v_label, CVarRef v_desc);
void f_wfdebuglog(CStrRef v_logGroup, String v_text, bool v_public = true);
void f__test_ends();
bool f_wfhttponlysafe();
Variant f_wfmsggetkey(Variant v_key, Variant v_useDB, Variant v_langCode = false, Variant v_transform = true);
void f_wferrorlog(Variant v_text, CVarRef v_file);
void f_plan(CVarRef v_plan, CStrRef v_why = "");
Variant f_wfrelativepath(Variant v_path, Variant v_from);
Variant f_wfmsgreal(CVarRef v_key, CVarRef v_args, bool v_useDB = true, bool v_forContent = false, bool v_transform = true);
Variant f_wfarraydiff2_cmp(Variant v_a, Variant v_b);
Variant f_wfarraymerge(int num_args, CVarRef v_array1, Array args = Array());
void f_wfdie(CStrRef v_msg = "");
Variant f_wfexplodemarkup(CVarRef v_separator, CVarRef v_text);
String f_escapesinglestring(CVarRef v_string);
bool f_wfparsertestparserhooksetup(Variant v_parser);
void f_wfdeprecated(CStrRef v_function);
Variant f_wfbasename(CVarRef v_path, CStrRef v_suffix = "");
void f_wfresetoutputbuffers(bool v_resetGzipEncoding = true);
Variant f_wfdiff(CVarRef v_before, CVarRef v_after, CStrRef v_params = "-u");
Variant f_wfexpandurl(CVarRef v_url);
String f_utf8tohexsequence(CVarRef v_str);
Variant f_pass(CVarRef v_desc = "");
bool f_wfinigetbool(CStrRef v_setting);
Variant f_wfmsgnodbforcontent(int num_args, CVarRef v_key, Array args = Array());
String f_wfscript(CStrRef v_script = "index");
Variant f_wfgetnamespacenotice();
void f_wflogxff();
bool f_logbadpassword(CVarRef v_user, CVarRef v_pass, CVarRef v_retval);
p_getdb f_wfgetdb(int64 v_db, CArrRef v_groups = ScalarArrays::sa_[0], bool v_wiki = false);
String f_wfrandom();
void f_wfdebugdiebacktrace(CStrRef v_msg = "");
Array f_wfparsecidr(CVarRef v_range);
Variant f_wfgetmaincache();
void f_wfvardump(CVarRef v_var);
Variant f_wfsetvar(Variant v_dest, CVarRef v_source);
bool f_wfclientacceptsgzip();
bool f_wfrunhooks(CStrRef v_event, CVarRef v_args = null_variant);
void f_untaint(CVarRef v_var, int64 v_level = 0LL);
void f_wfprofileclose();
Variant f_wfgetagent();
bool f_wfmkdirparents(CStrRef v_dir, Variant v_mode = null);
String f_wfparsertestparserhookhook(CVarRef v_in, CVarRef v_argv);
Variant f_wfescapewikitext(Variant v_text);
Variant f_wfparseurl(CVarRef v_url);
Variant f_wfchecklimits(Variant v_deflimit = 50LL, Variant v_optionname = "rclimit");
bool f_wfparsertimesetup(Variant v_parser, Variant v_ts);
Variant f_require_ok(Variant v_file, Variant v_desc = "");
Variant f_like(CVarRef v_got, CVarRef v_expected, CStrRef v_desc = "");
String f_wfarraytocgi(Variant v_array1, CVarRef v_array2 = null_variant);
Variant f_wftimestampnow();
Variant f_wfencryptpassword(CVarRef v_userid, CVarRef v_password);
Variant f_wfgetmessagecachestorage();
String f_wfreporttime();
Variant f_wftempdir();
Variant f_wfgetcache(CVarRef v_inputType);
String f__plural(Numeric v_n, CStrRef v_singular, Variant v_plural = null);
bool f_efgetprivateandfishbowlwikis(Variant v_private, Variant v_fishbowl);
Variant f_wflocalfile(Variant v_title);
String f_wfspeciallist(CVarRef v_page, Variant v_details);
bool f_wfreadonly();
Variant f__repl_array(CVarRef v_obj, bool v_deep);
Variant f_fail(CVarRef v_desc = "");
Variant f_wfmergeerrorarrays(int num_args, Array args = Array());
Variant f_wfgetip();
Variant f_wfmsgext(int num_args, CVarRef v_key, Variant v_options, Array args = Array());
Object f_wfclone(CVarRef v_object);
bool f_wfqueriesmustscale();
Variant f_wfshowingresults(CVarRef v_offset, Variant v_limit);
String f_wfgetallcallers();
void f_wflogprofilingdata();
void f_wfinstallexceptionhandler();
Variant f_wfviewprevnext(CVarRef v_offset, Variant v_limit, Variant v_link, CStrRef v_query = "", bool v_atend = false);
void f_todo_end();
Variant f_wfislocalurl(CVarRef v_url);
bool f_in_string(CVarRef v_needle, CVarRef v_str);
bool f_wfmerge(CVarRef v_old, CVarRef v_mine, CVarRef v_yours, Variant v_result);
String f_hexsequencetoutf8(CVarRef v_sequence);
Variant f_wftimestamp(int64 v_outputtype = 0LL /* TS_UNIX */, CVarRef v_ts = 0LL);
void f_swap(Variant v_x, Variant v_y);
Object co_title(CArrRef params, bool init = true);
Object co_request(CArrRef params, bool init = true);
Object co_errorpageerror(CArrRef params, bool init = true);
Object co_stubuser(CArrRef params, bool init = true);
Object co_magicwordarray(CArrRef params, bool init = true);
Object co_linker(CArrRef params, bool init = true);
Object co_utfnormal(CArrRef params, bool init = true);
Object co_stripstate(CArrRef params, bool init = true);
Object co_hashtablereplacer(CArrRef params, bool init = true);
Object co_explodeiterator(CArrRef params, bool init = true);
Object co_linksupdate(CArrRef params, bool init = true);
Object co_getdb(CArrRef params, bool init = true);
Object co_parsertest(CArrRef params, bool init = true);
Object co_htmlcacheupdate(CArrRef params, bool init = true);
Object co_interwiki(CArrRef params, bool init = true);
Object co_messagecache(CArrRef params, bool init = true);
Object co_htmlfilecache(CArrRef params, bool init = true);
Object co_ansitermcolorer(CArrRef params, bool init = true);
Object co_stubobject(CArrRef params, bool init = true);
Object co_doublereplacer(CArrRef params, bool init = true);
Object co_status(CArrRef params, bool init = true);
Object co_mwexception(CArrRef params, bool init = true);
Object co_fakememcachedclient(CArrRef params, bool init = true);
Object co_replacer(CArrRef params, bool init = true);
Object co_parseroptions(CArrRef params, bool init = true);
Object co_ip(CArrRef params, bool init = true);
Object co_outputpage(CArrRef params, bool init = true);
Object co_ppdpart(CArrRef params, bool init = true);
Object co_magicword(CArrRef params, bool init = true);
Object co_revision(CArrRef params, bool init = true);
Object co_ppdstackelement(CArrRef params, bool init = true);
Object co_linkholderarray(CArrRef params, bool init = true);
Object co_ppnode_dom(CArrRef params, bool init = true);
Object co_fakeconverter(CArrRef params, bool init = true);
Object co_skintemplate(CArrRef params, bool init = true);
Object co_mediawiki_i18n(CArrRef params, bool init = true);
Object co_memcached(CArrRef params, bool init = true);
Object co_regexlikereplacer(CArrRef params, bool init = true);
Object co_testrecorder(CArrRef params, bool init = true);
Object co_proveparsertest(CArrRef params, bool init = true);
Object co_parser(CArrRef params, bool init = true);
Object co_stubuserlang(CArrRef params, bool init = true);
Object co_ppdstack(CArrRef params, bool init = true);
Object co_titlearrayfromresult(CArrRef params, bool init = true);
Object co_provetestrecorder(CArrRef params, bool init = true);
Object co_linkbatch(CArrRef params, bool init = true);
Object co_preprocessor(CArrRef params, bool init = true);
Object co_passworderror(CArrRef params, bool init = true);
Object co_preprocessor_dom(CArrRef params, bool init = true);
Object co_monobooktemplate(CArrRef params, bool init = true);
Object co_stubcontlang(CArrRef params, bool init = true);
Object co_article(CArrRef params, bool init = true);
Object co_skin(CArrRef params, bool init = true);
Object co_parseroutput(CArrRef params, bool init = true);
Object co_skinmonobook(CArrRef params, bool init = true);
Object co_ppframe_dom(CArrRef params, bool init = true);
Object co_citerator(CArrRef params, bool init = true);
Object co_ppcustomframe_dom(CArrRef params, bool init = true);
Object co_ctraversable(CArrRef params, bool init = true);
Object co_linkcache(CArrRef params, bool init = true);
Object co_fatalerror(CArrRef params, bool init = true);
Object co_language(CArrRef params, bool init = true);
Object co_dbtestrecorder(CArrRef params, bool init = true);
Object co_pptemplateframe_dom(CArrRef params, bool init = true);
Object co_block(CArrRef params, bool init = true);
Object co_onlyincludereplacer(CArrRef params, bool init = true);
Object co_recentchange(CArrRef params, bool init = true);
Object co_coreparserfunctions(CArrRef params, bool init = true);
Object co_profiler(CArrRef params, bool init = true);
Object co_dummytermcolorer(CArrRef params, bool init = true);
Object co_titlearray(CArrRef params, bool init = true);
Object co_user(CArrRef params, bool init = true);
Object co_ppframe(CArrRef params, bool init = true);
Object co_filecache(CArrRef params, bool init = true);
Object co_xmlselect(CArrRef params, bool init = true);
Object co_sitestatsupdate(CArrRef params, bool init = true);
Object co_replacementarray(CArrRef params, bool init = true);
Object co_ppnode(CArrRef params, bool init = true);
Object co_memcachedclientforwiki(CArrRef params, bool init = true);
Object co_quicktemplate(CArrRef params, bool init = true);
Object co_searchupdate(CArrRef params, bool init = true);
Object co_xml(CArrRef params, bool init = true);
Object co_backlinkcache(CArrRef params, bool init = true);
Object co_mwnamespace(CArrRef params, bool init = true);
Object co_repogroup(CArrRef params, bool init = true);
Object co_dbtestpreviewer(CArrRef params, bool init = true);
Object co_sanitizer(CArrRef params, bool init = true);
Object co_stringutils(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_Parser2_h__
