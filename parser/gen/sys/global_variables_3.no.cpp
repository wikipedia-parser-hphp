
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 2047) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      HASH_INDEX(0x3489E1DAAEB9D801LL, wgUsePrivateIPs, 23);
      HASH_INDEX(0x4EE2265B1056F801LL, wgNoticeCentralPath, 475);
      break;
    case 2:
      HASH_INDEX(0x4B565810548AF002LL, wgRightsIcon, 103);
      break;
    case 4:
      HASH_INDEX(0x702395D437CE0804LL, wgPutIPinRC, 200);
      break;
    case 5:
      HASH_INDEX(0x6247B8A9EC4A3805LL, wmgUseConfigure, 509);
      break;
    case 8:
      HASH_INDEX(0x5CEFA3DBDDD90808LL, wmgCaptchaPassword, 435);
      break;
    case 9:
      HASH_INDEX(0x2BF360B54C532809LL, wgLanguageCode, 395);
      break;
    case 10:
      HASH_INDEX(0x62566BC9A306C80ALL, wgAllowDisplayTitle, 274);
      break;
    case 21:
      HASH_INDEX(0x5A724F15A2A6A815LL, wgUseETag, 215);
      break;
    case 26:
      HASH_INDEX(0x0C17BACBF76A681ALL, wgCollectionPortletForLoggedInUsersOnly, 496);
      break;
    case 27:
      HASH_INDEX(0x6E024F7A960D681BLL, wgParserTestFiles, 536);
      break;
    case 38:
      HASH_INDEX(0x66C2DDC470F86826LL, wgMessageCache, 12);
      HASH_INDEX(0x541F0DE9DE5A6826LL, wgParserConf, 18);
      break;
    case 50:
      HASH_INDEX(0x084E4941DD10F032LL, wgCaptchaSecret, 438);
      HASH_INDEX(0x463F4876FD995032LL, wgParserCacheType, 533);
      break;
    case 52:
      HASH_INDEX(0x4E578E2372DC9834LL, wgMiserMode, 271);
      break;
    case 67:
      HASH_INDEX(0x0A779CC563328843LL, dbHostsByName, 452);
      break;
    case 69:
      HASH_INDEX(0x7047E2A1BA590845LL, wgMemCachedServers, 528);
      break;
    case 74:
      HASH_INDEX(0x323EAC15D211984ALL, wgContLang, 38);
      break;
    case 75:
      HASH_INDEX(0x7A0C10E3609EA04BLL, m, 318);
      break;
    case 77:
      HASH_INDEX(0x24761D49EE10584DLL, wgUseCommaCount, 210);
      break;
    case 78:
      HASH_INDEX(0x5AD336E1B2E5304ELL, wgMaxCredits, 57);
      break;
    case 83:
      HASH_INDEX(0x749CA466E7076053LL, wgConfigureWikis, 514);
      break;
    case 90:
      HASH_INDEX(0x306910A71CB1B05ALL, oaiAgentRegex, 400);
      break;
    case 101:
      HASH_INDEX(0x4A97AADAA8A30865LL, wgSiteMatrixFile, 364);
      break;
    case 106:
      HASH_INDEX(0x55AA9BD92D7FB86ALL, wmgUseDPL, 405);
      break;
    case 108:
      HASH_INDEX(0x0318A2A42E99B86CLL, wgLegalTitleChars, 226);
      HASH_INDEX(0x00F2FF20EE9C206CLL, wgImageMagickTempDir, 471);
      break;
    case 111:
      HASH_INDEX(0x56448467D226A06FLL, wgTmpDirectory, 346);
      break;
    case 116:
      HASH_INDEX(0x01A194ACDAA8F874LL, wgEnableDublinCoreRdf, 75);
      break;
    case 120:
      HASH_INDEX(0x2BD015092DD2D878LL, wgTorDisableAdminBlocks, 412);
      break;
    case 129:
      HASH_INDEX(0x6532D7EA771A6881LL, wgEnableScaryTranscluding, 288);
      break;
    case 135:
      HASH_INDEX(0x476478C07603C087LL, wgCortadoJarFile, 375);
      break;
    case 139:
      HASH_INDEX(0x4E4D4C1ADC1F608BLL, wgHtmlEntities, 190);
      break;
    case 143:
      HASH_INDEX(0x0E6BCF90EB2C208FLL, oaiAudit, 402);
      break;
    case 147:
      HASH_INDEX(0x47A9C82B74E22893LL, wgInvalidRedirectTargets, 235);
      break;
    case 151:
      HASH_INDEX(0x0FF89CB119FDF097LL, wgMetaNamespaceTalk, 176);
      HASH_INDEX(0x043D47B39C45D097LL, wgDeleteRevisionsLimit, 220);
      break;
    case 156:
      HASH_INDEX(0x0E952EE1350A109CLL, wgPasswordSalt, 144);
      break;
    case 157:
      HASH_INDEX(0x58BD8A13ED27809DLL, wgScanSetSettings, 434);
      break;
    case 159:
      HASH_INDEX(0x786379403A37E89FLL, wgAllowExternalImagesFrom, 150);
      break;
    case 163:
      HASH_INDEX(0x22BB9E58DA1D68A3LL, wgMaxTemplateDepth, 155);
      break;
    case 165:
      HASH_INDEX(0x39CB51FC012448A5LL, wgDebugLogPrefix, 240);
      break;
    case 169:
      HASH_INDEX(0x4E365B3FC4B038A9LL, wgSidebarCacheExpiry, 108);
      break;
    case 170:
      HASH_INDEX(0x0941B2D54082A0AALL, wgLocalDatabases, 308);
      break;
    case 175:
      HASH_INDEX(0x1A332E88BF1E88AFLL, wgNoticeInfrastructure, 486);
      break;
    case 177:
      HASH_INDEX(0x0B12006D1EA388B1LL, wgGlobalBlockingDatabase, 382);
      break;
    case 178:
      HASH_INDEX(0x396833BC2B5C48B2LL, wgCookieSecure, 268);
      break;
    case 181:
      HASH_INDEX(0x35BC227B49E6F0B5LL, wgTorAutoConfirmCount, 411);
      break;
    case 186:
      HASH_INDEX(0x5E2E485985C6A8BALL, wgRestrictionLevels, 229);
      break;
    case 187:
      HASH_INDEX(0x690A6A790E4710BBLL, tester, 535);
      break;
    case 188:
      HASH_INDEX(0x5B097BD3B4EE58BCLL, wgDefaultRobotPolicy, 214);
      break;
    case 190:
      HASH_INDEX(0x359A82D4CB6EF0BELL, wgMaxNameChars, 111);
      break;
    case 198:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 206:
      HASH_INDEX(0x32E38C199DC208CELL, wgCleanSignatures, 157);
      break;
    case 209:
      HASH_INDEX(0x598250509CB268D1LL, wgCollectionArticleNamespaces, 492);
      break;
    case 211:
      HASH_INDEX(0x288CFF53F9D588D3LL, wgStatsMethod, 255);
      break;
    case 222:
      HASH_INDEX(0x30BAA6EB778E50DELL, sectionsByDB, 454);
      break;
    case 225:
      HASH_INDEX(0x056EF475084230E1LL, wgThumbUpright, 40);
      break;
    case 226:
      HASH_INDEX(0x2119480B686F60E2LL, wgRequest, 14);
      break;
    case 227:
      HASH_INDEX(0x53CCC76EF999B0E3LL, wgContentNamespaces, 36);
      HASH_INDEX(0x02FD34D874E9A8E3LL, wgRC2UDPAddress, 201);
      break;
    case 229:
      HASH_INDEX(0x44596B472FF3D8E5LL, wgDBerrorLog, 243);
      break;
    case 233:
      HASH_INDEX(0x67584BCA34BB58E9LL, wgThumbLimits, 39);
      break;
    case 236:
      HASH_INDEX(0x01813D410134A0ECLL, wgProxyList, 32);
      HASH_INDEX(0x4F935E42020C60ECLL, wgHTCPMulticastTTL, 342);
      break;
    case 238:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 243:
      HASH_INDEX(0x37C0CC66C852F0F3LL, wgUseSquid, 217);
      break;
    case 253:
      HASH_INDEX(0x6809574ACEE3C0FDLL, wgNamespacesToBeSearchedDefault, 21);
      break;
    case 255:
      HASH_INDEX(0x0718A6BE876748FFLL, wgHandheldStyle, 67);
      break;
    case 259:
      HASH_INDEX(0x704973FDADC9F903LL, oldtz, 344);
      break;
    case 263:
      HASH_INDEX(0x26A04AAD2ECBF907LL, wmgNoticeProject, 479);
      break;
    case 275:
      HASH_INDEX(0x371D989211310913LL, wgForeignFileRepos, 392);
      break;
    case 282:
      HASH_INDEX(0x6607BB576509A11ALL, wgCentralNoticeLoader, 482);
      break;
    case 288:
      HASH_INDEX(0x317F209AB8417920LL, wgProfileOnly, 238);
      break;
    case 296:
      HASH_INDEX(0x731BDF51B2005928LL, pathBits, 317);
      HASH_INDEX(0x0655274EEB132128LL, wgCaptchaWhitelist, 443);
      break;
    case 298:
      HASH_INDEX(0x087D95440560592ALL, tag, 327);
      break;
    case 300:
      HASH_INDEX(0x2D7B6BCCE049292CLL, wgMaximumMovedPages, 234);
      break;
    case 304:
      HASH_INDEX(0x0160FF6D2883D930LL, wgAlternateMaster, 453);
      break;
    case 306:
      HASH_INDEX(0x798D683865985932LL, wgAntiBotPayloads, 407);
      break;
    case 320:
      HASH_INDEX(0x0CD3659593691940LL, wgDisableOutputCompression, 252);
      break;
    case 322:
      HASH_INDEX(0x3F3206577A7C7142LL, wgApplyGlobalBlocks, 384);
      break;
    case 324:
      HASH_INDEX(0x3175AF6E6A7E2944LL, wgLogQueries, 359);
      break;
    case 326:
      HASH_INDEX(0x222B475F80948946LL, wgShowHostnames, 242);
      break;
    case 328:
      HASH_INDEX(0x02995E0E4820C948LL, wgExternalDiffEngine, 447);
      break;
    case 329:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      HASH_INDEX(0x12FE343C42677949LL, wgRC2UDPPrefix, 203);
      break;
    case 332:
      HASH_INDEX(0x7ABD1C02DBBBE94CLL, IP, 165);
      break;
    case 336:
      HASH_INDEX(0x342253A26CE6A950LL, wmgUseSpamBlacklist, 370);
      break;
    case 337:
      HASH_INDEX(0x4CCA7F022EF68151LL, action, 98);
      break;
    case 346:
      HASH_INDEX(0x0874F6116DC3395ALL, ubUploadBlacklist, 387);
      break;
    case 354:
      HASH_INDEX(0x49148B0ABD57D162LL, wgProfiling, 362);
      break;
    case 355:
      HASH_INDEX(0x71BEDBEC9F160163LL, wgNoDBParam, 309);
      break;
    case 359:
      HASH_INDEX(0x46BE06983A134967LL, wgAllowRealName, 389);
      break;
    case 360:
      HASH_INDEX(0x6A0CB69B6A85A968LL, wgExtraLanguageNames, 166);
      break;
    case 362:
      HASH_INDEX(0x5080CF3E8A31616ALL, wgProxyMemcExpiry, 30);
      break;
    case 368:
      HASH_INDEX(0x252B4126A4832970LL, space, 189);
      break;
    case 378:
      HASH_INDEX(0x0944E9861083597ALL, wgProxyWhitelist, 118);
      break;
    case 383:
      HASH_INDEX(0x7BB3365BD986697FLL, wgDBtype, 168);
      break;
    case 386:
      HASH_INDEX(0x3A8586F4FA6A2182LL, wgExtensionAliasesFiles, 170);
      break;
    case 390:
      HASH_INDEX(0x4A69C8540FC61186LL, wgLogRestrictions, 205);
      break;
    case 391:
      HASH_INDEX(0x5E016D8F68012187LL, wgUseRCPatrol, 116);
      break;
    case 396:
      HASH_INDEX(0x324C621B7AAEB18CLL, wgTrustedXffFile, 385);
      break;
    case 404:
      HASH_INDEX(0x46C31602DD91A194LL, wgRCMaxAge, 222);
      break;
    case 408:
      HASH_INDEX(0x79D568D750EAD998LL, dblist, 328);
      HASH_INDEX(0x05F8481CC363A998LL, groupOverrides, 419);
      break;
    case 414:
      HASH_INDEX(0x4AF7CD17F976719ELL, args, 297);
      break;
    case 423:
      HASH_INDEX(0x7CAEA4CF6345A1A7LL, wgUseESI, 358);
      break;
    case 433:
      HASH_INDEX(0x504530FC8E84C1B1LL, wgExtDistRemoteClient, 380);
      break;
    case 440:
      HASH_INDEX(0x6CF4B270FE1741B8LL, wgRC2UDPPort, 204);
      break;
    case 442:
      HASH_INDEX(0x7A6C9EDA8F8F61BALL, wgUseTrackbacks, 60);
      break;
    case 444:
      HASH_INDEX(0x38B5A28E5BF7C9BCLL, wgReservedUsernames, 112);
      break;
    case 447:
      HASH_INDEX(0x1AAC778B50B241BFLL, wgLocalTZoffset, 167);
      HASH_INDEX(0x512BB6600AF1A1BFLL, wgCacheEpoch, 198);
      break;
    case 452:
      HASH_INDEX(0x300631D93FA6A1C4LL, utfCompatibilityDecomp, 186);
      break;
    case 453:
      HASH_INDEX(0x36E1EFD4997309C5LL, wgNewPasswordExpiry, 136);
      break;
    case 457:
      HASH_INDEX(0x2DE0DD0CA196D1C9LL, fname, 314);
      break;
    case 461:
      HASH_INDEX(0x12D237609C7569CDLL, wgExtDistTarDir, 377);
      break;
    case 463:
      HASH_INDEX(0x43AF62D963A679CFLL, wgFileExtensions, 353);
      break;
    case 465:
      HASH_INDEX(0x0BEB6D57C677B1D1LL, wgEnableCreativeCommonsRdf, 76);
      break;
    case 467:
      HASH_INDEX(0x2D9BE538C72C31D3LL, wgProxyKey, 31);
      break;
    case 483:
      HASH_INDEX(0x37AC4209571FD9E3LL, wgEnableSorbs, 117);
      break;
    case 490:
      HASH_INDEX(0x7C17922060DCA1EALL, options, 294);
      break;
    case 491:
      HASH_INDEX(0x39637C555608F9EBLL, oaiAuditDatabase, 403);
      break;
    case 495:
      HASH_INDEX(0x19E02A72D4E221EFLL, wgDiff, 251);
      break;
    case 498:
      HASH_INDEX(0x176EC09AB1E071F2LL, wgClockSkewFudge, 125);
      break;
    case 499:
      HASH_INDEX(0x6CD674E44D4749F3LL, wgCentralAuthAutoNew, 466);
      break;
    case 504:
      HASH_INDEX(0x2E3A9BE7A3D449F8LL, wgLang, 16);
      break;
    case 506:
      HASH_INDEX(0x25D14844D69061FALL, parserMemc, 106);
      break;
    case 512:
      HASH_INDEX(0x68DE5EDB9899D200LL, wgNewUserSuppressRC, 500);
      break;
    case 516:
      HASH_INDEX(0x7C2603F6EF23FA04LL, wgArticlePath, 62);
      break;
    case 521:
      HASH_INDEX(0x2E5816FC6A54EA09LL, sectionLoads, 451);
      break;
    case 526:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 528:
      HASH_INDEX(0x58166C9285F7E210LL, wgJsMimeType, 45);
      break;
    case 529:
      HASH_INDEX(0x19950A9D826D7211LL, wgMaxSquidPurgeTitles, 398);
      break;
    case 531:
      HASH_INDEX(0x66C3B564F57C3A13LL, wgSquidServersNoPurge, 25);
      HASH_INDEX(0x5C2714389994FA13LL, wgHTCPMulticastAddress, 341);
      break;
    case 533:
      HASH_INDEX(0x7C958444EC769A15LL, wgExceptionHooks, 180);
      break;
    case 536:
      HASH_INDEX(0x1BA686401B287A18LL, wgCentralAuthUDPAddress, 459);
      break;
    case 545:
      HASH_INDEX(0x112599DDD6071221LL, wgUseGzip, 249);
      break;
    case 546:
      HASH_INDEX(0x737101E2E1EEF222LL, wgImplicitGroups, 143);
      break;
    case 548:
      HASH_INDEX(0x2F4C917F2B883224LL, wgCentralAuthAutoLoginWikis, 462);
      break;
    case 551:
      HASH_INDEX(0x3139086411CA2A27LL, wgUseImageResize, 349);
      break;
    case 552:
      HASH_INDEX(0x0D276462B26E8228LL, wmgCollectionPortletForLoggedInUsersOnly, 495);
      break;
    case 553:
      HASH_INDEX(0x623376B75256A229LL, wgMessageCacheType, 532);
      break;
    case 555:
      HASH_INDEX(0x7C821970ED7BBA2BLL, utfCombiningClass, 183);
      break;
    case 560:
      HASH_INDEX(0x200D4EA31F5EFA30LL, wmgCentralNoticeLoader, 481);
      break;
    case 563:
      HASH_INDEX(0x34EF5D609D7E4A33LL, wgTitleBlacklistSources, 371);
      break;
    case 564:
      HASH_INDEX(0x1E06D47E4EEC3234LL, wgDirectoryMode, 254);
      break;
    case 567:
      HASH_INDEX(0x5E4CF831C9C58A37LL, wgConf, 307);
      break;
    case 572:
      HASH_INDEX(0x2F9D7D9C64E4223CLL, wgAjaxWatch, 84);
      break;
    case 584:
      HASH_INDEX(0x203046DEC44DEA48LL, wgAllowImageMoving, 35);
      break;
    case 587:
      HASH_INDEX(0x55731FE54621BA4BLL, wgUseEnotif, 130);
      break;
    case 588:
      HASH_INDEX(0x0F1A108CC0649A4CLL, wgHTTPTimeout, 424);
      break;
    case 589:
      HASH_INDEX(0x360B6AC185D6F24DLL, wgHtmlEntityAliases, 191);
      break;
    case 611:
      HASH_INDEX(0x655D6045889F7A63LL, wgUseAutomaticEditSummaries, 219);
      break;
    case 612:
      HASH_INDEX(0x3974C065342F2A64LL, wmgNewUserMinorEdit, 501);
      break;
    case 614:
      HASH_INDEX(0x6C7F5992B8026A66LL, wgXhtmlDefaultNamespace, 52);
      break;
    case 616:
      HASH_INDEX(0x0E12BE46FD64D268LL, day, 339);
      break;
    case 621:
      HASH_INDEX(0x3BABC4C6EB7CDA6DLL, group, 417);
      break;
    case 622:
      HASH_INDEX(0x0151ECCE23B73A6ELL, wgUseAjax, 83);
      HASH_INDEX(0x48D4C1D2A849126ELL, wgEnableMWSuggest, 92);
      break;
    case 624:
      HASH_INDEX(0x3902BABD6AC8B270LL, wgReadOnly, 247);
      break;
    case 635:
      HASH_INDEX(0x11B7E569AC1CB27BLL, wgEnforceHtmlIds, 193);
      break;
    case 646:
      HASH_INDEX(0x5579733B1E941A86LL, wgRateLimits, 122);
      break;
    case 651:
      HASH_INDEX(0x798D3418DBF5328BLL, wgDiff3, 250);
      break;
    case 652:
      HASH_INDEX(0x337B054C2BC42A8CLL, wgDefaultExternalStore, 196);
      break;
    case 655:
      HASH_INDEX(0x19BE06787780628FLL, wgRateLimitsExcludedGroups, 120);
      HASH_INDEX(0x0343543E7359CA8FLL, wgCentralAuthCookies, 458);
      break;
    case 656:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 657:
      HASH_INDEX(0x12CC6E807F0AEA91LL, wgValidSkinNames, 70);
      break;
    case 660:
      HASH_INDEX(0x3F7D508716303A94LL, wgDisableLangConversion, 65);
      break;
    case 665:
      HASH_INDEX(0x70D115E2BCD17A99LL, wgConfigureFileSystemCache, 512);
      break;
    case 679:
      HASH_INDEX(0x2DABB6595CC2C2A7LL, wgUseRootUser, 518);
      break;
    case 680:
      HASH_INDEX(0x0EB9D0B46A06A2A8LL, wgHideInterlanguageLinks, 56);
      break;
    case 681:
      HASH_INDEX(0x6EB208CBCA7E0AA9LL, wgMaxShellTime, 261);
      break;
    case 683:
      HASH_INDEX(0x3DA59011258F7AABLL, wgGroupPermissions, 141);
      break;
    case 696:
      HASH_INDEX(0x5EA1EE5833E702B8LL, wgNoticeTimeout, 483);
      break;
    case 697:
      HASH_INDEX(0x61CFEEED54DD0AB9LL, matches, 321);
      break;
    case 712:
      HASH_INDEX(0x52B19BC7F3126AC8LL, wmgUseDrafts, 508);
      break;
    case 713:
      HASH_INDEX(0x2FCD4CD058FE6AC9LL, wgMaxTocLevel, 290);
      break;
    case 714:
      HASH_INDEX(0x45F7742DD17392CALL, wgRawHtml, 281);
      break;
    case 716:
      HASH_INDEX(0x07A1087699D022CCLL, wgScriptExtension, 272);
      break;
    case 717:
      HASH_INDEX(0x7026A29A72AC22CDLL, wgVariantArticlePath, 81);
      break;
    case 729:
      HASH_INDEX(0x1BB3FBD59E522AD9LL, self, 296);
      break;
    case 736:
      HASH_INDEX(0x751AD4E978A01AE0LL, wgShellLocale, 262);
      break;
    case 738:
      HASH_INDEX(0x41998A76A6C112E2LL, wgForceUIMsgAsContentMsg, 248);
      break;
    case 739:
      HASH_INDEX(0x70C29B6AB429BAE3LL, wgProxyPorts, 27);
      break;
    case 749:
      HASH_INDEX(0x4A5D0431E1A3CAEDLL, filename, 323);
      break;
    case 755:
      HASH_INDEX(0x69BB1AD4F9569AF3LL, wgUseSiteJs, 61);
      break;
    case 757:
      HASH_INDEX(0x2ED3A839738B52F5LL, wgDisableAnonTalk, 43);
      break;
    case 759:
      HASH_INDEX(0x0B6CFE5CA752B2F7LL, sep, 303);
      break;
    case 766:
      HASH_INDEX(0x66AD900A2301E2FELL, title, 393);
      break;
    case 771:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x34FACC6293F20B03LL, wgShowExceptionDetails, 181);
      break;
    case 774:
      HASH_INDEX(0x6FF6342F2039DB06LL, wgSqlLogFile, 360);
      break;
    case 776:
      HASH_INDEX(0x7E5ABAA1081BFB08LL, wgEmailAuthentication, 140);
      break;
    case 778:
      HASH_INDEX(0x362C89A936481B0ALL, wgUDPProfilerHost, 256);
      break;
    case 781:
      HASH_INDEX(0x4008C24D7AA4830DLL, lang, 312);
      break;
    case 786:
      HASH_INDEX(0x03E350A8A756DB12LL, wgNamespaceAliases, 177);
      break;
    case 787:
      HASH_INDEX(0x4BAAC6DED702C313LL, wgNewUserLog, 145);
      HASH_INDEX(0x2FF9B075F5BFC313LL, wgExpensiveParserFunctionLimit, 280);
      break;
    case 789:
      HASH_INDEX(0x3AD645EB48693B15LL, wgTitle, 46);
      break;
    case 792:
      HASH_INDEX(0x353888D51FF11318LL, wgMimeType, 50);
      break;
    case 795:
      HASH_INDEX(0x68C88FBC2532F31BLL, wmgPrivateWikiUploads, 354);
      break;
    case 806:
      HASH_INDEX(0x5713CE531BD6F326LL, wgNoFollowDomainExceptions, 284);
      HASH_INDEX(0x4B535D6FA1165B26LL, wgCommandLineDarkBg, 534);
      break;
    case 809:
      HASH_INDEX(0x5AFBCF04E31D7329LL, wgCentralAuthNew2UDPPrefix, 460);
      HASH_INDEX(0x2D026B6CDC7E0B29LL, wgConfigureHandler, 510);
      break;
    case 811:
      HASH_INDEX(0x19FD77778A48832BLL, wmgUseCentralAuth, 456);
      break;
    case 815:
      HASH_INDEX(0x2CB24E9940FE032FLL, wgEnableSidebarCache, 107);
      break;
    case 823:
      HASH_INDEX(0x7FA57490F2479B37LL, __Test, 293);
      break;
    case 826:
      HASH_INDEX(0x062EA4121AF9EB3ALL, wmgUseDismissableSiteNotice, 469);
      break;
    case 827:
      HASH_INDEX(0x7C85270BECBF433BLL, wgEnotifUseJobQ, 450);
      break;
    case 831:
      HASH_INDEX(0x262106CA8F6FC33FLL, wgEnotifUserTalk, 449);
      break;
    case 833:
      HASH_INDEX(0x56D735735ADE5341LL, wgMetaNamespace, 175);
      break;
    case 836:
      HASH_INDEX(0x173543E12A6ACB44LL, wmgUseDualLicense, 361);
      break;
    case 838:
      HASH_INDEX(0x471686E0BE369B46LL, wgEnableUserEmail, 139);
      HASH_INDEX(0x669A492817E02B46LL, wgUpdateRowsPerJob, 208);
      break;
    case 842:
      HASH_INDEX(0x78788009C106034ALL, wgPagePropLinkInvalidations, 209);
      break;
    case 847:
      HASH_INDEX(0x61B21442EB5B6B4FLL, wgDBadminuser, 519);
      break;
    case 848:
      HASH_INDEX(0x7727D0FBE47C9350LL, wgProfileLimit, 245);
      HASH_INDEX(0x6F27ACF0EE1E9B50LL, wmgUseQuiz, 372);
      break;
    case 854:
      HASH_INDEX(0x061C264C73C67B56LL, wgProxyScriptPath, 28);
      break;
    case 855:
      HASH_INDEX(0x11A1AD7A5DBDFB57LL, wgDebugProfiling, 161);
      break;
    case 856:
      HASH_INDEX(0x72050C6E97FB7B58LL, wgFileStore, 333);
      break;
    case 859:
      HASH_INDEX(0x612E37678CE7DB5BLL, file, 330);
      break;
    case 861:
      HASH_INDEX(0x30315AC4F8CCDB5DLL, cacheRecord, 325);
      break;
    case 878:
      HASH_INDEX(0x71CF7D337911E36ELL, wgScriptPath, 63);
      break;
    case 879:
      HASH_INDEX(0x5F5BED26C4C4D36FLL, wgSquidServers, 24);
      HASH_INDEX(0x7691457976BD336FLL, wgNamespaceProtection, 230);
      break;
    case 883:
      HASH_INDEX(0x46B52D5AB15B2B73LL, wgDebugComments, 79);
      break;
    case 888:
      HASH_INDEX(0x172E6B599943FB78LL, optionsWithArgs, 295);
      break;
    case 896:
      HASH_INDEX(0x39E25484A83BAB80LL, wgDebugRawPage, 239);
      break;
    case 899:
      HASH_INDEX(0x6A1F50A5B28C6B83LL, wgRC2UDPInterwikiPrefix, 206);
      break;
    case 913:
      HASH_INDEX(0x1452103FD0885B91LL, wgDeferredUpdateList, 221);
      HASH_INDEX(0x16E01CEFBB639B91LL, wgAbuseFilterAvailableActions, 516);
      break;
    case 914:
      HASH_INDEX(0x47CB32AFA64F1392LL, wgUseImageMagick, 350);
      break;
    case 917:
      HASH_INDEX(0x3732DBE98AD9CB95LL, wgBreakFrames, 80);
      HASH_INDEX(0x7852680AAA755B95LL, wgEnableParserCache, 211);
      break;
    case 921:
      HASH_INDEX(0x1831362001AD1B99LL, wgProfileSampleRate, 363);
      break;
    case 925:
      HASH_INDEX(0x7F7826A784C4939DLL, wgArticleRobotPolicies, 213);
      break;
    case 929:
      HASH_INDEX(0x6A4892EEE04F63A1LL, wgRedirectScript, 105);
      break;
    case 931:
      HASH_INDEX(0x0B2441BD1EA163A3LL, wgConfigureDatabase, 511);
      break;
    case 932:
      HASH_INDEX(0x362E2DDD4FD143A4LL, wgMaxIfExistCount, 365);
      HASH_INDEX(0x0CF3172C1E07B3A4LL, wgDBservers, 426);
      break;
    case 935:
      HASH_INDEX(0x4029AA34891533A7LL, wikiTags, 326);
      break;
    case 937:
      HASH_INDEX(0x337E18C29953D3A9LL, wgCopyrightIcon, 104);
      break;
    case 940:
      HASH_INDEX(0x72D98AF412127BACLL, wgAmericanDates, 178);
      HASH_INDEX(0x5CE004C20B7B83ACLL, secure, 316);
      break;
    case 941:
      HASH_INDEX(0x521D9080185BCBADLL, wgRightsPage, 77);
      break;
    case 942:
      HASH_INDEX(0x57A384A24288B3AELL, wgNoticeLocalDirectory, 478);
      break;
    case 945:
      HASH_INDEX(0x4695D28F4D09FBB1LL, wgCategoryTreeDynamicTag, 367);
      break;
    case 946:
      HASH_INDEX(0x4945BFF1FB1D63B2LL, wgExtraNamespaces, 34);
      break;
    case 958:
      HASH_INDEX(0x17005F4F5B1E83BELL, wgNamespacesWithSubpages, 37);
      HASH_INDEX(0x0010AE53E8B303BELL, wgSecretKey, 126);
      break;
    case 964:
      HASH_INDEX(0x58268D95D840A3C4LL, wgDBpassword, 522);
      break;
    case 965:
      HASH_INDEX(0x5C01C46944D773C5LL, wgDBtransactions, 218);
      break;
    case 970:
      HASH_INDEX(0x651C0F19009043CALL, wgCollectionMWServeURL, 490);
      break;
    case 976:
      HASH_INDEX(0x4469449C6F2433D0LL, wgUseCategoryBrowser, 96);
      break;
    case 977:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 980:
      HASH_INDEX(0x462D0C9A6A274BD4LL, wgNoticeCounterSource, 485);
      break;
    case 985:
      HASH_INDEX(0x33BF6451F7B58BD9LL, wgOut, 17);
      break;
    case 988:
      HASH_INDEX(0x06A61F679D5453DCLL, wgRevisionCacheExpiry, 197);
      break;
    case 989:
      HASH_INDEX(0x051BE5EA4CE45BDDLL, wgImageMagickConvertCommand, 351);
      break;
    case 991:
      HASH_INDEX(0x097D33F7EE7B33DFLL, wgCookiePrefix, 115);
      break;
    case 992:
      HASH_INDEX(0x67E7705CB8B5A3E0LL, wgRequestTime, 244);
      break;
    case 995:
      HASH_INDEX(0x7FE53B6CF0A1B3E3LL, wgUploadPath, 335);
      break;
    case 996:
      HASH_INDEX(0x0E4F0A1D74C1FBE4LL, wgRestrictDisplayTitle, 276);
      break;
    case 1004:
      HASH_INDEX(0x6691B5FAD09ECBECLL, wgDisableCounters, 54);
      HASH_INDEX(0x52D0231E81089BECLL, wgTranslateNumerals, 171);
      break;
    case 1013:
      HASH_INDEX(0x43FBB2C4FE020BF5LL, wgNonincludableNamespaces, 287);
      break;
    case 1018:
      HASH_INDEX(0x0289E71EE62E8BFALL, wgLanguageCodeReal, 396);
      break;
    case 1038:
      HASH_INDEX(0x5ACBE7CAD535AC0ELL, wmgUseLST, 369);
      break;
    case 1040:
      HASH_INDEX(0x1910B041E0EED410LL, wgLicenseURL, 494);
      break;
    case 1044:
      HASH_INDEX(0x5EE826941539E414LL, wgSiteNotice, 253);
      break;
    case 1046:
      HASH_INDEX(0x04BFC205E59FA416LL, x, 431);
      break;
    case 1055:
      HASH_INDEX(0x463F1CB15A76041FLL, wgHitcounterUpdateFreq, 223);
      break;
    case 1056:
      HASH_INDEX(0x6191D1EF499CCC20LL, wgReadOnlyFile, 246);
      HASH_INDEX(0x441B3D2180102C20LL, wgNoticeProject, 480);
      break;
    case 1061:
      HASH_INDEX(0x46C18C39B09FE425LL, wgUseHashTable, 394);
      break;
    case 1066:
      HASH_INDEX(0x538441FB3B0A042ALL, wmgUseFlaggedRevs, 366);
      break;
    case 1067:
      HASH_INDEX(0x6827C1A2D124EC2BLL, wgConfigureEditableSettings, 513);
      break;
    case 1071:
      HASH_INDEX(0x232B852377A7B42FLL, wgAllowUserSkin, 129);
      break;
    case 1075:
      HASH_INDEX(0x296D9A8065A85433LL, wgUseTeX, 147);
      break;
    case 1076:
      HASH_INDEX(0x3B036C7BE9281C34LL, wgAutoblockExpiry, 110);
      break;
    case 1077:
      HASH_INDEX(0x66693EBBD88E0435LL, attrib, 188);
      break;
    case 1081:
      HASH_INDEX(0x5F9E59B3AAB7EC39LL, wgInputEncoding, 164);
      HASH_INDEX(0x4A9A2B8384DECC39LL, wgDebugLogGroups, 241);
      break;
    case 1084:
      HASH_INDEX(0x3354B8ACF06BC43CLL, wgMaxArticleSize, 153);
      break;
    case 1088:
      HASH_INDEX(0x454567E402A60C40LL, wgRightsText, 102);
      break;
    case 1095:
      HASH_INDEX(0x79417070F3B48C47LL, wgExtraSubtitle, 100);
      break;
    case 1109:
      HASH_INDEX(0x55CF1D77F9276C55LL, wmgNewUserSuppressRC, 499);
      break;
    case 1110:
      HASH_INDEX(0x1073013255125C56LL, wgAuth, 114);
      break;
    case 1111:
      HASH_INDEX(0x018E501422A0B457LL, wgLocalInterwiki, 199);
      break;
    case 1112:
      HASH_INDEX(0x48E7F8779A11EC58LL, wgScript, 48);
      HASH_INDEX(0x6EF4B0C760B69458LL, wgLivePreview, 89);
      break;
    case 1129:
      HASH_INDEX(0x2E259B8159F52469LL, wgInterwikiMagic, 148);
      HASH_INDEX(0x3574191639261469LL, wgProfileToDatabase, 162);
      break;
    case 1132:
      HASH_INDEX(0x10B90601AD3E2C6CLL, wgLinkHolderBatchSize, 277);
      break;
    case 1133:
      HASH_INDEX(0x4450CC847C8B546DLL, wmgUseNewUserMessage, 498);
      break;
    case 1135:
      HASH_INDEX(0x5C4B00752D88AC6FLL, wgCentralAuthDryRun, 457);
      break;
    case 1138:
      HASH_INDEX(0x6E99E49E33B88472LL, wgAllowExternalImages, 149);
      break;
    case 1148:
      HASH_INDEX(0x6C5558E2BD73EC7CLL, wgUrlProtocols, 258);
      break;
    case 1158:
      HASH_INDEX(0x2AADB7171DADFC86LL, wgCommandLineMode, 146);
      break;
    case 1162:
      HASH_INDEX(0x4B3834E25BB7448ALL, wgStyleDirectory, 13);
      break;
    case 1164:
      HASH_INDEX(0x1936366D2D1FA48CLL, wgMaxPPExpandDepth, 156);
      break;
    case 1169:
      HASH_INDEX(0x049C095295EA6491LL, wgHttpOnlyBlacklist, 264);
      break;
    case 1172:
      HASH_INDEX(0x043364BAC65CAC94LL, tmarray, 337);
      HASH_INDEX(0x4AB8A71495E5FC94LL, wmgUseAbuseFilter, 515);
      break;
    case 1174:
      HASH_INDEX(0x4A369B78852EDC96LL, wgShowUpdatedMarker, 131);
      break;
    case 1175:
      HASH_INDEX(0x021AD26793AE7C97LL, bits, 301);
      break;
    case 1178:
      HASH_INDEX(0x668D947A942A4C9ALL, wmgSecondLevelDomain, 461);
      break;
    case 1180:
      HASH_INDEX(0x5FCB1A0329D55C9CLL, utfCanonicalComp, 184);
      break;
    case 1185:
      HASH_INDEX(0x436837B76D35B4A1LL, wgNoticeScroll, 487);
      break;
    case 1187:
      HASH_INDEX(0x4A33C63C6B8EECA3LL, wgUseLuceneSearch, 397);
      HASH_INDEX(0x60F4DCC88CA4F4A3LL, adminSettings, 523);
      break;
    case 1188:
      HASH_INDEX(0x1A19A3B64C15FCA4LL, wgWhitelistRead, 232);
      break;
    case 1195:
      HASH_INDEX(0x629910BD11344CABLL, wgInternalServer, 228);
      break;
    case 1196:
      HASH_INDEX(0x488CBC4D4C2E3CACLL, DP, 313);
      break;
    case 1197:
      HASH_INDEX(0x4E21DF371292ECADLL, wgPageShowWatchingUsers, 59);
      HASH_INDEX(0x731EA9D43F3FECADLL, wgCodeReviewENotif, 507);
      break;
    case 1201:
      HASH_INDEX(0x457DA687174D54B1LL, wgArticle, 47);
      break;
    case 1218:
      HASH_INDEX(0x1A392434358B24C2LL, wgStyleVersion, 68);
      break;
    case 1219:
      HASH_INDEX(0x4836A8B25F907CC3LL, wgMajorSiteNoticeID, 470);
      break;
    case 1237:
      HASH_INDEX(0x07282D6481A02CD5LL, wgDefaultSkin, 72);
      break;
    case 1238:
      HASH_INDEX(0x4C1AFFA5E04754D6LL, option, 299);
      break;
    case 1242:
      HASH_INDEX(0x5F2DDAE740A644DALL, wgGrammarForms, 172);
      HASH_INDEX(0x163728A5B186DCDALL, wgSubversionProxy, 506);
      break;
    case 1250:
      HASH_INDEX(0x2F5E554E4A9D34E2LL, wgCaptchaTriggers, 445);
      break;
    case 1251:
      HASH_INDEX(0x163DDB9EB768E4E3LL, wgMaxShellMemory, 259);
      break;
    case 1259:
      HASH_INDEX(0x4BA04FC73DA36CEBLL, wgCookieDomain, 267);
      break;
    case 1261:
      HASH_INDEX(0x36DE4A3BD2F754EDLL, wgEnableAPI, 86);
      break;
    case 1275:
      HASH_INDEX(0x3E078EE8498BCCFBLL, wgSharedThumbnailScriptPath, 334);
      break;
    case 1280:
      HASH_INDEX(0x7A58218A1FE59D00LL, wgAppleTouchIcon, 74);
      break;
    case 1283:
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 1284:
      HASH_INDEX(0x5B73E76B1ACDBD04LL, wgUseFileCache, 225);
      break;
    case 1285:
      HASH_INDEX(0x157405D52897CD05LL, wgTorLoadNodes, 408);
      break;
    case 1290:
      HASH_INDEX(0x4DEDD0C4E06BAD0ALL, wgTorAutoConfirmAge, 410);
      break;
    case 1292:
      HASH_INDEX(0x278B0E130BE1150CLL, wgTorIPs, 409);
      break;
    case 1295:
      HASH_INDEX(0x6DCC412201E4350FLL, wgShowIPinHeader, 101);
      break;
    case 1298:
      HASH_INDEX(0x19A9C284C412E512LL, wgServerName, 285);
      break;
    case 1299:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 1300:
      HASH_INDEX(0x423AF940A9459514LL, wgDBuser, 520);
      break;
    case 1314:
      HASH_INDEX(0x3C3558C7B940E522LL, wgTimelineSettings, 388);
      break;
    case 1315:
      HASH_INDEX(0x5B9A991AE78D8523LL, wgTorTagChanges, 413);
      break;
    case 1317:
      HASH_INDEX(0x29FD3ACC3187D525LL, wgLanguageNames, 163);
      break;
    case 1318:
      HASH_INDEX(0x77F632A4E34F1526LL, p, 302);
      break;
    case 1342:
      HASH_INDEX(0x05F247364E8A2D3ELL, utfCheckNFC, 187);
      break;
    case 1343:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      HASH_INDEX(0x3D50C74AF7EBDD3FLL, wgUseDumbLinkUpdate, 207);
      HASH_INDEX(0x42D2AEA0328D9D3FLL, cluster, 306);
      break;
    case 1347:
      HASH_INDEX(0x6888EE8FACEBC543LL, wgBrowserBlackList, 433);
      break;
    case 1357:
      HASH_INDEX(0x3A5C7A341C25754DLL, wgServer, 64);
      break;
    case 1360:
      HASH_INDEX(0x30E66D49BBCCAD50LL, wgMemc, 29);
      break;
    case 1366:
      HASH_INDEX(0x7A2C52FB86914556LL, wgCentralAuthCookieDomain, 463);
      break;
    case 1369:
      HASH_INDEX(0x10F91D80DF2BB559LL, wgLocaltimezone, 286);
      break;
    case 1370:
      HASH_INDEX(0x4E5E50E23D3CA55ALL, wgCollectionFormats, 493);
      break;
    case 1372:
      HASH_INDEX(0x71B9C6B68C4E7D5CLL, wgDBserver, 429);
      break;
    case 1373:
      HASH_INDEX(0x4AD417058F046D5DLL, wgEnableEmail, 138);
      HASH_INDEX(0x56CE9E40C1C9A55DLL, wgCaptchaDirectory, 439);
      break;
    case 1381:
      HASH_INDEX(0x2589884252E47565LL, wgRenderHashAppend, 133);
      break;
    case 1387:
      HASH_INDEX(0x2D8567285FADF56BLL, wgStyleSheetPath, 331);
      HASH_INDEX(0x36E0FE5F9841656BLL, wgDBadminpassword, 521);
      break;
    case 1394:
      HASH_INDEX(0x63A123E046C6ED72LL, wgMimeTypeBlacklist, 355);
      break;
    case 1397:
      HASH_INDEX(0x16A179F3F7DCFD75LL, wgCheckSerialized, 174);
      break;
    case 1400:
      HASH_INDEX(0x602FD94428C31D78LL, section, 455);
      break;
    case 1405:
      HASH_INDEX(0x6E95875F5D07E57DLL, wgOutputEncoding, 51);
      break;
    case 1406:
      HASH_INDEX(0x0188BE45E73AFD7ELL, globals, 324);
      break;
    case 1414:
      HASH_INDEX(0x29348BAAA90F5586LL, wgBlockAllowsUTEdit, 124);
      break;
    case 1415:
      HASH_INDEX(0x612DD31212E90587LL, key, 427);
      break;
    case 1423:
      HASH_INDEX(0x4EE2267A795CFD8FLL, wgUseTwoButtonsSearchForm, 69);
      break;
    case 1427:
      HASH_INDEX(0x176FF84F38BCB593LL, wgPasswordReminderResendTime, 127);
      HASH_INDEX(0x63A063EDF9482593LL, wmgUseTitleKey, 488);
      break;
    case 1429:
      HASH_INDEX(0x0A84091EA3FEDD95LL, wgExtDistBranches, 381);
      break;
    case 1432:
      HASH_INDEX(0x79CB53EB0854D598LL, wgAntiLockFlags, 109);
      break;
    case 1436:
      HASH_INDEX(0x6A19B93CFBB8559CLL, wgActiveUserEditCount, 134);
      break;
    case 1443:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 1452:
      HASH_INDEX(0x6F9E309EB1E7D5ACLL, wgSVGConverters, 356);
      break;
    case 1453:
      HASH_INDEX(0x4299F1F5792875ADLL, wgCaptchaClass, 442);
      break;
    case 1454:
      HASH_INDEX(0x014509CC09376DAELL, wgRC2UDPOmitBots, 202);
      break;
    case 1456:
      HASH_INDEX(0x2962EA6B30582DB0LL, wmgEmergencyCaptcha, 446);
      break;
    case 1457:
      HASH_INDEX(0x0150F074F25345B1LL, wgCaptchaRegexes, 444);
      break;
    case 1458:
      HASH_INDEX(0x45B9FC509805E5B2LL, wgMaxSigChars, 291);
      break;
    case 1459:
      HASH_INDEX(0x2A71D707D84BDDB3LL, wgAlwaysUseTidy, 279);
      HASH_INDEX(0x05B49F689C5E25B3LL, wgEmergencyContact, 340);
      break;
    case 1463:
      HASH_INDEX(0x1AB7E1CEB32E45B7LL, wmgCentralAuthLoginIcon, 464);
      break;
    case 1469:
      HASH_INDEX(0x0FFD0F06B717ADBDLL, docRoot, 320);
      break;
    case 1472:
      HASH_INDEX(0x73EC2C4CD71805C0LL, wgRateLimitLog, 123);
      break;
    case 1476:
      HASH_INDEX(0x43E03BE5B85A1DC4LL, wgAllowSlowParserFunctions, 275);
      break;
    case 1482:
      HASH_INDEX(0x79F8A66E4D7AADCALL, wgFavicon, 73);
      break;
    case 1483:
      HASH_INDEX(0x1DBDC37E73C135CBLL, wgWikiFarm, 305);
      break;
    case 1484:
      HASH_INDEX(0x062657E94E8EADCCLL, wmgUseSpecialNuke, 406);
      break;
    case 1490:
      HASH_INDEX(0x05551C88820BCDD2LL, wgShowSQLErrors, 526);
      break;
    case 1496:
      HASH_INDEX(0x27440C0251CED5D8LL, wgFeedClasses, 99);
      HASH_INDEX(0x00B8EAEC735E0DD8LL, wgNoFollowNsExceptions, 283);
      HASH_INDEX(0x0AAB197B17BF7DD8LL, wgNewUserMessageOnAutoCreate, 504);
      break;
    case 1504:
      HASH_INDEX(0x2F78A533854E5DE0LL, wgDebugLogFile, 237);
      break;
    case 1507:
      HASH_INDEX(0x051DC70D51D4EDE3LL, wgCaches, 527);
      break;
    case 1514:
      HASH_INDEX(0x2CB7B15F2858E5EALL, wgTexvc, 345);
      break;
    case 1518:
      HASH_INDEX(0x62AC603470B39DEELL, wgRestrictionTypes, 88);
      break;
    case 1522:
      HASH_INDEX(0x2954B42DA780CDF2LL, wgMemCachedPersistent, 530);
      break;
    case 1523:
      HASH_INDEX(0x0D1B1DDB65783DF3LL, wgMediaHandlers, 472);
      break;
    case 1524:
      HASH_INDEX(0x41AC59070F5C55F4LL, groupOverrides2, 416);
      break;
    case 1525:
      HASH_INDEX(0x28DCDB01128435F5LL, wgDisableSearchUpdate, 348);
      break;
    case 1526:
      HASH_INDEX(0x299EDB527A96ADF6LL, wgStylePath, 41);
      break;
    case 1527:
      HASH_INDEX(0x65404FCF82D95DF7LL, wgCommunityCollectionNamespace, 491);
      break;
    case 1528:
      HASH_INDEX(0x107325AF94A9A5F8LL, wgTranscludeCacheExpiry, 289);
      break;
    case 1533:
      HASH_INDEX(0x51810FDD0D1C4DFDLL, wgAllowPageInfo, 224);
      break;
    case 1534:
      HASH_INDEX(0x6D0545C199FCADFELL, wgPasswordSender, 137);
      break;
    case 1542:
      HASH_INDEX(0x5CB58E8A664EFE06LL, wgCentralAuthCreateOnView, 467);
      break;
    case 1543:
      HASH_INDEX(0x0ECB3DA2FE35B607LL, wgExternalServers, 430);
      break;
    case 1554:
      HASH_INDEX(0x5D968409CEE59612LL, wmgEnableCaptcha, 436);
      break;
    case 1557:
      HASH_INDEX(0x62E1F4DBA1130E15LL, wgSquidMaxage, 95);
      break;
    case 1558:
      HASH_INDEX(0x7929B0DE86C51616LL, wgNamespaceRobotPolicies, 212);
      break;
    case 1561:
      HASH_INDEX(0x660E6C8E995BF619LL, wgDisableHardRedirects, 415);
      break;
    case 1567:
      HASH_INDEX(0x1BF02047448F9E1FLL, wgAllowUserCss, 93);
      break;
    case 1568:
      HASH_INDEX(0x061F27F2CDBD1620LL, wgEnableSerializedMessages, 173);
      break;
    case 1576:
      HASH_INDEX(0x0978C2EED25CAE28LL, wgShowDebug, 97);
      break;
    case 1577:
      HASH_INDEX(0x722F599AEA279E29LL, db, 310);
      break;
    case 1582:
      HASH_INDEX(0x7FC54E8E5FD1AE2ELL, hatesSafari, 315);
      break;
    case 1590:
      HASH_INDEX(0x0F25E179DFFE0636LL, wgUseTidy, 192);
      HASH_INDEX(0x371842DCA2467636LL, wgExtensionMessagesFiles, 273);
      break;
    case 1591:
      HASH_INDEX(0x3B24465279BB7E37LL, wgMaxRedirects, 227);
      break;
    case 1597:
      HASH_INDEX(0x694D0EA05378AE3DLL, wgShowCreditsIfMax, 58);
      break;
    case 1598:
      HASH_INDEX(0x254CACEFA4FA763ELL, wgFullyInitialised, 179);
      break;
    case 1610:
      HASH_INDEX(0x224FF8C4CD58764ALL, wmgAutopromoteExtraGroups, 420);
      break;
    case 1617:
      HASH_INDEX(0x538D9FEB1DE97E51LL, wgAccountCreationThrottle, 497);
      break;
    case 1621:
      HASH_INDEX(0x73BBFC69F0B6AE55LL, wgIP, 22);
      break;
    case 1623:
      HASH_INDEX(0x3686C76DF5F7CE57LL, wgPostCommitUpdateList, 263);
      HASH_INDEX(0x0FB82D4E4FECA657LL, wgNoticeProjects, 474);
      break;
    case 1631:
      HASH_INDEX(0x06864431CD5F2E5FLL, wgUseNPPatrol, 128);
      break;
    case 1636:
      HASH_INDEX(0x0099BC4A66CCBE64LL, wgLBFactoryConf, 525);
      break;
    case 1645:
      HASH_INDEX(0x5550309A82D57E6DLL, wgEnotifWatchlist, 448);
      break;
    case 1649:
      HASH_INDEX(0x20973782D3DDA671LL, wgDisableTextSearch, 347);
      break;
    case 1657:
      HASH_INDEX(0x0638127DA5B4B679LL, wgDBprefix, 270);
      break;
    case 1660:
      HASH_INDEX(0x4E679853E6353E7CLL, wgCentralAuthLoginIcon, 465);
      break;
    case 1661:
      HASH_INDEX(0x4D6EE112A9D2E67DLL, bin, 517);
      break;
    case 1672:
      HASH_INDEX(0x7C702CAB981D3E88LL, wgSysopRangeBans, 390);
      break;
    case 1673:
      HASH_INDEX(0x1250903211069E89LL, wgActionPaths, 82);
      break;
    case 1674:
      HASH_INDEX(0x16D0A1E7D73BAE8ALL, wmgContactPageConf, 386);
      break;
    case 1678:
      HASH_INDEX(0x4E71ED8D7F33468ELL, wgCaptchaStorageClass, 441);
      break;
    case 1683:
      HASH_INDEX(0x4F8FB6081FBDD693LL, wgRateLimitsExcludedIPs, 121);
      break;
    case 1685:
      HASH_INDEX(0x097A11531AA46695LL, wmgUseCodeReview, 505);
      break;
    case 1686:
      HASH_INDEX(0x4C40A42FA1A56696LL, wgSorbsUrl, 119);
      HASH_INDEX(0x2032537EAFB84696LL, wgNoFollowLinks, 282);
      break;
    case 1688:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 524);
      break;
    case 1692:
      HASH_INDEX(0x3AD9F28E2BBC1E9CLL, wgUser, 15);
      break;
    case 1693:
      HASH_INDEX(0x0F8C317744981E9DLL, wgMWSuggestTemplate, 90);
      break;
    case 1695:
      HASH_INDEX(0x34F23C8A11797E9FLL, wmgNewUserMessageOnAutoCreate, 503);
      break;
    case 1698:
      HASH_INDEX(0x2A8A478185B60EA2LL, wmgUseGadgets, 373);
      break;
    case 1704:
      HASH_INDEX(0x2342299E93F216A8LL, wgUseSiteCss, 94);
      break;
    case 1707:
      HASH_INDEX(0x4243E2C0D99B5EABLL, wgPreprocessorCacheThreshold, 278);
      break;
    case 1713:
      HASH_INDEX(0x4C80005A6C3C66B1LL, wgNoticeLocalPath, 476);
      break;
    case 1721:
      HASH_INDEX(0x180D1EEFDC087EB9LL, wgExtDistWorkingCopy, 379);
      break;
    case 1732:
      HASH_INDEX(0x2FE50309F9D3FEC4LL, wgEnableUploads, 42);
      break;
    case 1733:
      HASH_INDEX(0x10F3E1A52760B6C5LL, hour, 338);
      break;
    case 1734:
      HASH_INDEX(0x2471AFEA96CE16C6LL, wgDisabledActions, 414);
      break;
    case 1738:
      HASH_INDEX(0x08625588FAC906CALL, wgRedirectSources, 216);
      break;
    case 1740:
      HASH_INDEX(0x7B8EA02672E89ECCLL, wgExtensionFunctions, 404);
      break;
    case 1744:
      HASH_INDEX(0x26B2DFBF69FAB6D0LL, wgUploadSizeWarning, 391);
      break;
    case 1748:
      HASH_INDEX(0x658A7E566743C6D4LL, wgActiveUserDays, 135);
      break;
    case 1749:
      HASH_INDEX(0x20A4AA67AC18FED5LL, wgDebugDumpSql, 425);
      break;
    case 1752:
      HASH_INDEX(0x72E1EE0D651E56D8LL, arg, 298);
      HASH_INDEX(0x3031363A4A2486D8LL, wgFFmpegLocation, 374);
      break;
    case 1753:
      HASH_INDEX(0x6C40E451833476D9LL, wgContactUser, 376);
      break;
    case 1769:
      HASH_INDEX(0x5A2661DC503CFEE9LL, wgUploadNavigationUrl, 66);
      break;
    case 1771:
      HASH_INDEX(0x14A5EE0C30D676EBLL, wgCookieHttpOnly, 269);
      break;
    case 1773:
      HASH_INDEX(0x1338989B9D947EEDLL, wmgUseCollection, 489);
      break;
    case 1775:
      HASH_INDEX(0x7CA5B3BC94DF3EEFLL, wgNoticeCentralDirectory, 477);
      break;
    case 1780:
      HASH_INDEX(0x562A8CEEFF9EEEF4LL, wgDefaultUserOptions, 20);
      HASH_INDEX(0x7B82DEB1E0C0AEF4LL, wmgExtraImplicitGroups, 422);
      break;
    case 1781:
      HASH_INDEX(0x3166C3C2024176F5LL, utfCanonicalDecomp, 185);
      break;
    case 1789:
      HASH_INDEX(0x53BE99C04D2686FDLL, wmgUseCentralNotice, 473);
      break;
    case 1791:
      HASH_INDEX(0x3C14FC791F083EFFLL, wgVersion, 85);
      break;
    case 1793:
      HASH_INDEX(0x617A83EE2D3EEF01LL, wmgApplyGlobalBlocks, 383);
      break;
    case 1794:
      HASH_INDEX(0x749F5E65AD3FAF02LL, wgProfiler, 159);
      break;
    case 1807:
      HASH_INDEX(0x77FD9FFDAD75070FLL, wgCentralAuthCookiePrefix, 468);
      break;
    case 1808:
      HASH_INDEX(0x34441773493C6710LL, wgEnableImageWhitelist, 151);
      break;
    case 1815:
      HASH_INDEX(0x79A9441AA56E7F17LL, wgMemCachedDebug, 529);
      break;
    case 1816:
      HASH_INDEX(0x2EF30F08E88CF718LL, wgThumbnailEpoch, 399);
      break;
    case 1819:
      HASH_INDEX(0x3991BC0C3907B71BLL, server, 319);
      break;
    case 1822:
      HASH_INDEX(0x70FBE2FBA2859F1ELL, wgCategoryPrefixedDefaultSortkey, 292);
      break;
    case 1828:
      HASH_INDEX(0x10E1A63135E52724LL, site, 311);
      break;
    case 1831:
      HASH_INDEX(0x5796B4D976439727LL, wgEnableWriteAPI, 87);
      break;
    case 1834:
      HASH_INDEX(0x3396F1088B2F1F2ALL, wgMaxShellFileSize, 260);
      break;
    case 1842:
      HASH_INDEX(0x7D8C1AC5E54A2F32LL, wgSitename, 182);
      break;
    case 1848:
      HASH_INDEX(0x432096BEB2551738LL, wgCapitalLinks, 233);
      HASH_INDEX(0x0E0A4E58AF902738LL, oaiAuth, 401);
      break;
    case 1849:
      HASH_INDEX(0x21FCEAE39D8F1F39LL, wgRightsUrl, 78);
      break;
    case 1857:
      HASH_INDEX(0x04B2FC81D80AA741LL, param, 300);
      break;
    case 1859:
      HASH_INDEX(0x410D7DDB269B0F43LL, wgUploadDirectory, 336);
      break;
    case 1869:
      HASH_INDEX(0x4431BAD847B37F4DLL, wgEmailConfirmToEdit, 231);
      break;
    case 1880:
      HASH_INDEX(0x4D45F85CE7859F58LL, wgLogo, 55);
      break;
    case 1881:
      HASH_INDEX(0x041CD7ABFA3BC759LL, permissions, 418);
      break;
    case 1891:
      HASH_INDEX(0x45798BB44A42AF63LL, wgNoticeServerTimeout, 484);
      break;
    case 1895:
      HASH_INDEX(0x419CFF2A2E5BCF67LL, wgLegacySchemaConversion, 423);
      break;
    case 1899:
      HASH_INDEX(0x5F98F051A5DFAF6BLL, wgAutopromote, 421);
      break;
    case 1900:
      HASH_INDEX(0x00C6DE6F7DE96F6CLL, wgMainCacheType, 531);
      break;
    case 1908:
      HASH_INDEX(0x25F32E03B5218774LL, wgSysopUserBans, 44);
      break;
    case 1910:
      HASH_INDEX(0x29644029C275B776LL, wgCheckDBSchema, 343);
      break;
    case 1914:
      HASH_INDEX(0x301E67F6386FD77ALL, wgSessionsInMemcached, 265);
      break;
    case 1918:
      HASH_INDEX(0x5B38396CF3A99F7ELL, wgParser, 19);
      break;
    case 1922:
      HASH_INDEX(0x4056C2E766E0B782LL, val, 428);
      break;
    case 1928:
      HASH_INDEX(0x43CAB6B0484C7788LL, wgContLanguageCode, 49);
      break;
    case 1930:
      HASH_INDEX(0x15335C76F750778ALL, wgUseNormalUser, 304);
      HASH_INDEX(0x4F56B733A4DFC78ALL, y, 432);
      break;
    case 1935:
      HASH_INDEX(0x52B40E492EB0878FLL, wgDebugFunctionEntry, 160);
      break;
    case 1937:
      HASH_INDEX(0x75BFE08931DDA791LL, wgMinimalPasswordLength, 113);
      break;
    case 1944:
      HASH_INDEX(0x0B580630050B5798LL, wgExtDistTarUrl, 378);
      break;
    case 1951:
      HASH_INDEX(0x7197AC66F7344F9FLL, wgExternalLinkTarget, 158);
      break;
    case 1955:
      HASH_INDEX(0x2A248A54BB4117A3LL, wgCookiePath, 266);
      break;
    case 1963:
      HASH_INDEX(0x4F92DFC03802B7ABLL, wgDBname, 91);
      break;
    case 1964:
      HASH_INDEX(0x71DAA2A63FF427ACLL, wgStockPath, 332);
      break;
    case 1971:
      HASH_INDEX(0x4F11F806F2DB5FB3LL, wgFileBlacklist, 352);
      break;
    case 1976:
      HASH_INDEX(0x2587A3C7BB424FB8LL, wgBlockOpenProxies, 26);
      break;
    case 1983:
      HASH_INDEX(0x6D4CC110B75F37BFLL, wgLegacyEncoding, 194);
      HASH_INDEX(0x273BD45190637FBFLL, wgCaptchaDirectoryLevels, 440);
      break;
    case 1987:
      HASH_INDEX(0x6DAC6FDB8CC047C3LL, wmgCaptchaSecret, 437);
      break;
    case 1992:
      HASH_INDEX(0x107DDA9CBCCA7FC8LL, wgAllowUserJs, 357);
      break;
    case 1998:
      HASH_INDEX(0x4CA1A40E6B6CAFCELL, wmgUseProofreadPage, 368);
      break;
    case 2005:
      HASH_INDEX(0x6BFA60F06006B7D5LL, wgUseDynamicDates, 132);
      break;
    case 2007:
      HASH_INDEX(0x74F3ED447A35AFD7LL, wgAvailableRights, 142);
      break;
    case 2010:
      HASH_INDEX(0x47D95AEF51D237DALL, wgXhtmlNamespaces, 53);
      break;
    case 2012:
      HASH_INDEX(0x54A6699BEF6B4FDCLL, wgNewUserMinorEdit, 502);
      break;
    case 2016:
      HASH_INDEX(0x3E7A655727E02FE0LL, wgUDPProfilerPort, 257);
      break;
    case 2020:
      HASH_INDEX(0x5C64B65285AEBFE4LL, oldUmask, 329);
      break;
    case 2025:
      HASH_INDEX(0x4DAD2268BAA61FE9LL, dbSuffix, 322);
      break;
    case 2028:
      HASH_INDEX(0x4FF90E6A6A0F0FECLL, wgSkipSkins, 71);
      break;
    case 2029:
      HASH_INDEX(0x34DEC5871A3E97EDLL, wgAllowSpecialInclusion, 152);
      HASH_INDEX(0x147FCED3D9C197EDLL, wgEditEncoding, 169);
      break;
    case 2031:
      HASH_INDEX(0x61FA7102D48317EFLL, wgMaxPPNodeCount, 154);
      break;
    case 2032:
      HASH_INDEX(0x27DECA6D5839F7F0LL, wgCanonicalNamespaceNames, 33);
      break;
    case 2034:
      HASH_INDEX(0x2E884B89708A87F2LL, wgHooks, 236);
      break;
    case 2035:
      HASH_INDEX(0x5BE32ACA8C87AFF3LL, wgCompressRevisions, 195);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 537) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
