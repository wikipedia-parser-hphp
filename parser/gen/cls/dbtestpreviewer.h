
#ifndef __GENERATED_cls_dbtestpreviewer_h__
#define __GENERATED_cls_dbtestpreviewer_h__

#include <cls/testrecorder.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 44761 */
class c_dbtestpreviewer : virtual public c_testrecorder {
  BEGIN_CLASS_MAP(dbtestpreviewer)
    PARENT_CLASS(testrecorder)
  END_CLASS_MAP(dbtestpreviewer)
  DECLARE_CLASS(dbtestpreviewer, DbTestPreviewer, testrecorder)
  void init();
  public: Variant m_lb;
  public: Variant m_db;
  public: Variant m_curRun;
  public: Variant m_prevRun;
  public: Variant m_results;
  public: void t___construct(Variant v_parent);
  public: ObjectData *create(Variant v_parent);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_start();
  public: void t_record(CVarRef v_test, CVarRef v_result);
  public: void t_report();
  public: String t_getteststatusinfo(CVarRef v_testname, CStrRef v_after);
  public: void t_end();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_dbtestpreviewer_h__
