
#include <php/LinkHolderArray.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_DB_SLAVE = "DB_SLAVE";
const StaticString k_NS_CATEGORY = "NS_CATEGORY";

/* preface starts */
inline Variant df_lambda_1(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_2(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_3(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: LinkHolderArray.php line 3 */
Variant c_linkholderarray::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_linkholderarray::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_linkholderarray::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("internals", m_internals.isReferenced() ? ref(m_internals) : m_internals));
  props.push_back(NEW(ArrayElement)("interwikis", m_interwikis.isReferenced() ? ref(m_interwikis) : m_interwikis));
  props.push_back(NEW(ArrayElement)("size", m_size.isReferenced() ? ref(m_size) : m_size));
  props.push_back(NEW(ArrayElement)("parent", m_parent.isReferenced() ? ref(m_parent) : m_parent));
  c_ObjectData::o_get(props);
}
bool c_linkholderarray::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_EXISTS_STRING(0x69A9269F8FE824A3LL, internals, 9);
      break;
    case 4:
      HASH_EXISTS_STRING(0x16E2F26FFB10FD8CLL, parent, 6);
      break;
    case 5:
      HASH_EXISTS_STRING(0x7FD8492E01593115LL, interwikis, 10);
      break;
    case 7:
      HASH_EXISTS_STRING(0x3FE8023BDF261C0FLL, size, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_linkholderarray::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_RETURN_STRING(0x69A9269F8FE824A3LL, m_internals,
                         internals, 9);
      break;
    case 4:
      HASH_RETURN_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                         parent, 6);
      break;
    case 5:
      HASH_RETURN_STRING(0x7FD8492E01593115LL, m_interwikis,
                         interwikis, 10);
      break;
    case 7:
      HASH_RETURN_STRING(0x3FE8023BDF261C0FLL, m_size,
                         size, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_linkholderarray::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_SET_STRING(0x69A9269F8FE824A3LL, m_internals,
                      internals, 9);
      break;
    case 4:
      HASH_SET_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                      parent, 6);
      break;
    case 5:
      HASH_SET_STRING(0x7FD8492E01593115LL, m_interwikis,
                      interwikis, 10);
      break;
    case 7:
      HASH_SET_STRING(0x3FE8023BDF261C0FLL, m_size,
                      size, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_linkholderarray::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 3:
      HASH_RETURN_STRING(0x69A9269F8FE824A3LL, m_internals,
                         internals, 9);
      break;
    case 4:
      HASH_RETURN_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                         parent, 6);
      break;
    case 5:
      HASH_RETURN_STRING(0x7FD8492E01593115LL, m_interwikis,
                         interwikis, 10);
      break;
    case 7:
      HASH_RETURN_STRING(0x3FE8023BDF261C0FLL, m_size,
                         size, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_linkholderarray::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(linkholderarray)
ObjectData *c_linkholderarray::create(Variant v_parent) {
  init();
  t___construct(v_parent);
  return this;
}
ObjectData *c_linkholderarray::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_linkholderarray::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
void c_linkholderarray::destruct() {
  if (!inCtorDtor()) {
    incRefCount();
    try {
      t___destruct();
    } catch (...) { handle_destructor_exception();}
  }
}
ObjectData *c_linkholderarray::cloneImpl() {
  c_linkholderarray *obj = NEW(c_linkholderarray)();
  cloneSet(obj);
  return obj;
}
void c_linkholderarray::cloneSet(c_linkholderarray *clone) {
  clone->m_internals = m_internals.isReferenced() ? ref(m_internals) : m_internals;
  clone->m_interwikis = m_interwikis.isReferenced() ? ref(m_interwikis) : m_interwikis;
  clone->m_size = m_size.isReferenced() ? ref(m_size) : m_size;
  clone->m_parent = m_parent.isReferenced() ? ref(m_parent) : m_parent;
  ObjectData::cloneSet(clone);
}
Variant c_linkholderarray::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x7F974836AACC1EF3LL, __destruct) {
        return (t___destruct());
      }
      break;
    case 5:
      HASH_GUARD(0x0B6C33D73053B0C5LL, makeholder) {
        int count = params.size();
        if (count <= 1) return (t_makeholder(params.rvalAt(0)));
        if (count == 2) return (t_makeholder(params.rvalAt(0), params.rvalAt(1)));
        if (count == 3) return (t_makeholder(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        if (count == 4) return (t_makeholder(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
        return (t_makeholder(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
      }
      break;
    case 6:
      HASH_GUARD(0x316E7B92ECA74146LL, replacetext) {
        return (t_replacetext(params.rvalAt(0)));
      }
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (t_replace(ref(const_cast<Array&>(params).lvalAt(0))));
      }
      break;
    case 11:
      HASH_GUARD(0x1C2E89C2A927FF7BLL, merge) {
        return (t_merge(params.rvalAt(0)), null);
      }
      break;
    case 14:
      HASH_GUARD(0x1D72607A7BDD782ELL, replacetextcallback) {
        return (t_replacetextcallback(params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_linkholderarray::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 3:
      HASH_GUARD(0x7F974836AACC1EF3LL, __destruct) {
        return (t___destruct());
      }
      break;
    case 5:
      HASH_GUARD(0x0B6C33D73053B0C5LL, makeholder) {
        if (count <= 1) return (t_makeholder(a0));
        if (count == 2) return (t_makeholder(a0, a1));
        if (count == 3) return (t_makeholder(a0, a1, a2));
        if (count == 4) return (t_makeholder(a0, a1, a2, a3));
        return (t_makeholder(a0, a1, a2, a3, a4));
      }
      break;
    case 6:
      HASH_GUARD(0x316E7B92ECA74146LL, replacetext) {
        return (t_replacetext(a0));
      }
      HASH_GUARD(0x42FAC655280A6146LL, replace) {
        return (t_replace(ref(a0)));
      }
      break;
    case 11:
      HASH_GUARD(0x1C2E89C2A927FF7BLL, merge) {
        return (t_merge(a0), null);
      }
      break;
    case 14:
      HASH_GUARD(0x1D72607A7BDD782ELL, replacetextcallback) {
        return (t_replacetextcallback(a0));
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_linkholderarray::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_linkholderarray$os_get(const char *s) {
  return c_linkholderarray::os_get(s, -1);
}
Variant &cw_linkholderarray$os_lval(const char *s) {
  return c_linkholderarray::os_lval(s, -1);
}
Variant cw_linkholderarray$os_constant(const char *s) {
  return c_linkholderarray::os_constant(s);
}
Variant cw_linkholderarray$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_linkholderarray::os_invoke(c, s, params, -1, fatal);
}
void c_linkholderarray::init() {
  m_internals = ScalarArrays::sa_[0];
  m_interwikis = ScalarArrays::sa_[0];
  m_size = 0LL;
  m_parent = null;
}
/* SRC: LinkHolderArray.php line 8 */
void c_linkholderarray::t___construct(Variant v_parent) {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_parent = v_parent);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: LinkHolderArray.php line 15 */
Variant c_linkholderarray::t___destruct() {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::__destruct);
  setInDtor();
  DECLARE_GLOBAL_VARIABLES(g);
  Primitive v_name = 0;
  Variant v_value;

  {
    LOOP_COUNTER(1);
    Variant map2 = ((Object)(this));
    for (ArrayIterPtr iter3 = map2.begin("linkholderarray"); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_value = iter3->second();
      v_name = iter3->first();
      {
        t___unset(toString(v_name));
      }
    }
  }
  return null;
} /* function */
/* SRC: LinkHolderArray.php line 24 */
void c_linkholderarray::t_merge(CVarRef v_other) {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::merge);
  Primitive v_ns = 0;
  Variant v_entries;

  {
    LOOP_COUNTER(4);
    Variant map5 = toObject(v_other).o_get("internals", 0x69A9269F8FE824A3LL);
    for (ArrayIterPtr iter6 = map5.begin("linkholderarray"); !iter6->end(); iter6->next()) {
      LOOP_COUNTER_CHECK(4);
      v_entries = iter6->second();
      v_ns = iter6->first();
      {
        m_size += LINE(26,x_count(v_entries));
        if (!(isset(m_internals, v_ns))) {
          m_internals.set(v_ns, (v_entries));
        }
        else {
          lval(m_internals.lvalAt(v_ns)) += v_entries;
        }
      }
    }
  }
  m_interwikis += toObject(v_other).o_get("interwikis", 0x7FD8492E01593115LL);
} /* function */
/* SRC: LinkHolderArray.php line 39 */
bool c_linkholderarray::t_isbig() {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::isBig);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgLinkHolderBatchSize __attribute__((__unused__)) = g->GV(wgLinkHolderBatchSize);
  return more(m_size, gv_wgLinkHolderBatchSize);
} /* function */
/* SRC: LinkHolderArray.php line 48 */
void c_linkholderarray::t_clear() {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::clear);
  (m_internals = ScalarArrays::sa_[0]);
  (m_interwikis = ScalarArrays::sa_[0]);
  (m_size = 0LL);
} /* function */
/* SRC: LinkHolderArray.php line 61 */
String c_linkholderarray::t_makeholder(CVarRef v_nt, CVarRef v_text //  = ""
, CVarRef v_query //  = ""
, Variant v_trail //  = ""
, CVarRef v_prefix //  = ""
) {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::makeHolder);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  String v_retVal;
  Variant v_inside;
  Variant v_entry;
  Variant v_key;
  Variant v_ns;

  LINE(62,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "LinkHolderArray::makeHolder").create()), 0x0000000075359BAFLL));
  if (!(LINE(63,x_is_object(v_nt)))) {
    (v_retVal = LINE(65,concat4("<!-- ERROR -->", toString(v_prefix), toString(v_text), toString(v_trail))));
  }
  else {
    df_lambda_1(LINE(68,throw_fatal("unknown class linker", ((void*)NULL))), v_inside, v_trail);
    (v_entry = (assignCallTemp(eo_0, v_nt),assignCallTemp(eo_1, LINE(72,concat3(toString(v_prefix), toString(v_text), toString(v_inside)))),assignCallTemp(eo_2, LINE(73,toObject(v_nt)->o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0))),Array(ArrayInit(3).set(0, "title", eo_0, 0x66AD900A2301E2FELL).set(1, "text", eo_1, 0x2A28A0084DD3A743LL).set(2, "pdbk", eo_2, 0x18B80C52A2E3FCC5LL).create())));
    if (!same(v_query, "")) {
      v_entry.set("query", (v_query), 0x10A98E9AE3F54806LL);
    }
    if (toBoolean(LINE(79,toObject(v_nt)->o_invoke_few_args("isExternal", 0x16F7C0178CF6E138LL, 0)))) {
      (v_key = LINE(81,m_parent.o_invoke_few_args("nextLinkID", 0x6EFD1FF8956DBC27LL, 0)));
      m_interwikis.set(v_key, (v_entry));
      (v_retVal = LINE(83,concat4("<!--IWLINK ", toString(v_key), "-->", toString(v_trail))));
    }
    else {
      (v_key = LINE(85,m_parent.o_invoke_few_args("nextLinkID", 0x6EFD1FF8956DBC27LL, 0)));
      (v_ns = LINE(86,toObject(v_nt)->o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)));
      lval(m_internals.lvalAt(v_ns)).set(v_key, (v_entry));
      (v_retVal = LINE(88,concat6("<!--LINK ", toString(v_ns), ":", toString(v_key), "-->", toString(v_trail))));
    }
    m_size++;
  }
  LINE(92,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "LinkHolderArray::makeHolder").create()), 0x00000000B599F276LL));
  return v_retVal;
} /* function */
/* SRC: LinkHolderArray.php line 99 */
Variant c_linkholderarray::t_getstubthreshold() {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::getStubThreshold);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgUser __attribute__((__unused__)) = g->GV(wgUser);
  if (!(t___isset("stubThreshold"))) {
    (o_lval("stubThreshold", 0x1995F66FB30F7BC5LL) = LINE(102,gv_wgUser.o_invoke_few_args("getOption", 0x402DD1A85CAEA6C0LL, 1, "stubthreshold")));
  }
  return o_get("stubThreshold", 0x1995F66FB30F7BC5LL);
} /* function */
/* SRC: LinkHolderArray.php line 113 */
Variant c_linkholderarray::t_replace(Variant v_text) {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::replace);
  Variant v_colours;

  LINE(114,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "LinkHolderArray::replace").create()), 0x0000000075359BAFLL));
  (v_colours = (LINE(116,t_replaceinternal(ref(v_text))), null));
  LINE(117,t_replaceinterwiki(ref(v_text)));
  LINE(119,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "LinkHolderArray::replace").create()), 0x00000000B599F276LL));
  return v_colours;
} /* function */
/* SRC: LinkHolderArray.php line 126 */
void c_linkholderarray::t_replaceinternal(Variant v_text) {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::replaceInternal);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_wgContLang;
  Variant v_colours;
  Variant v_sk;
  Variant v_linkCache;
  Variant v_output;
  Variant v_dbr;
  Variant v_page;
  Variant v_threshold;
  Variant v_query;
  Variant v_current;
  Primitive v_ns = 0;
  Variant v_entries;
  Primitive v_index = 0;
  Variant v_entry;
  String v_key;
  Variant v_title;
  Variant v_pdbk;
  Variant v_id;
  Variant v_res;
  Variant v_linkcolour_ids;
  Variant v_s;
  Variant v_replacePairs;
  String v_searchkey;
  Object v_replacer;

  if (!(toBoolean(m_internals))) {
    return;
  }
  LINE(131,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceInternal").create()), 0x0000000075359BAFLL));
  v_wgContLang = ref(g->GV(wgContLang));
  (v_colours = ScalarArrays::sa_[0]);
  (v_sk = (assignCallTemp(eo_0, toObject(LINE(135,m_parent.o_invoke_few_args("getOptions", 0x2040B36587CE56F9LL, 0)))),eo_0.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_linkCache = LINE(136,throw_fatal("unknown class linkcache", ((void*)NULL))));
  (v_output = LINE(137,m_parent.o_invoke_few_args("getOutput", 0x7519C1FAF33D7857LL, 0)));
  LINE(139,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceInternal-check").create()), 0x0000000075359BAFLL));
  (v_dbr = LINE(140,invoke_failed("wfgetdb", Array(ArrayInit(1).set(0, k_DB_SLAVE).create()), 0x000000002DA3FF23LL)));
  (v_page = LINE(141,v_dbr.o_invoke_few_args("tableName", 0x15769EF1E0936722LL, 1, "page")));
  (v_threshold = LINE(142,t_getstubthreshold()));
  LINE(145,x_ksort(ref(lval(m_internals))));
  (v_query = false);
  setNull(v_current);
  {
    LOOP_COUNTER(7);
    Variant map8 = m_internals;
    for (ArrayIterPtr iter9 = map8.begin("linkholderarray"); !iter9->end(); iter9->next()) {
      LOOP_COUNTER_CHECK(7);
      v_entries = iter9->second();
      v_ns = iter9->first();
      {
        {
          LOOP_COUNTER(10);
          for (ArrayIterPtr iter12 = v_entries.begin("linkholderarray"); !iter12->end(); iter12->next()) {
            LOOP_COUNTER_CHECK(10);
            v_entry = iter12->second();
            v_index = iter12->first();
            {
              (v_key = LINE(152,concat3(toString(v_ns), ":", toString(v_index))));
              (v_title = v_entry.rvalAt("title", 0x66AD900A2301E2FELL));
              (v_pdbk = v_entry.rvalAt("pdbk", 0x18B80C52A2E3FCC5LL));
              if (LINE(158,x_is_null(v_title))) {
                continue;
              }
              if (toBoolean(LINE(163,v_title.o_invoke_few_args("isAlwaysKnown", 0x32DB35272B7DDB90LL, 0)))) {
                v_colours.set(v_pdbk, (""));
              }
              else if (!equal(((v_id = LINE(165,v_linkCache.o_invoke_few_args("getGoodLinkID", 0x318581EFAFF78639LL, 1, v_pdbk)))), 0LL)) {
                v_colours.set(v_pdbk, (LINE(166,v_sk.o_invoke_few_args("getLinkColour", 0x0EBA9C609550DE93LL, 2, v_title, v_threshold))));
                LINE(167,v_output.o_invoke_few_args("addLink", 0x5D10AC6ADCA895ACLL, 2, v_title, v_id));
              }
              else if (toBoolean(LINE(168,v_linkCache.o_invoke_few_args("isBadLink", 0x45DC57467F969616LL, 1, v_pdbk)))) {
                v_colours.set(v_pdbk, ("new"));
              }
              else {
                if (!(isset(v_current))) {
                  (v_current = v_ns);
                  (v_query = concat("SELECT page_id, page_namespace, page_title, page_is_redirect, page_len", LINE(175,concat5(" FROM ", toString(v_page), " WHERE (page_namespace=", toString(v_ns), " AND page_title IN("))));
                }
                else if (!equal(v_current, v_ns)) {
                  (v_current = v_ns);
                  concat_assign(v_query, LINE(178,concat3(")) OR (page_namespace=", toString(v_ns), " AND page_title IN(")));
                }
                else {
                  concat_assign(v_query, ", ");
                }
                concat_assign(v_query, toString(LINE(183,v_dbr.o_invoke_few_args("addQuotes", 0x3D76A2E50E13EE4CLL, 1, v_title.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)))));
              }
            }
          }
        }
      }
    }
  }
  if (toBoolean(v_query)) {
    concat_assign(v_query, "))");
    (v_res = LINE(190,v_dbr.o_invoke_few_args("query", 0x356758D4414DA377LL, 2, v_query, "LinkHolderArray::replaceInternal")));
    (v_linkcolour_ids = ScalarArrays::sa_[0]);
    LOOP_COUNTER(13);
    {
      while (toBoolean((v_s = LINE(195,v_dbr.o_invoke_few_args("fetchObject", 0x0E1814AA3327229DLL, 1, v_res))))) {
        LOOP_COUNTER_CHECK(13);
        {
          (v_title = LINE(196,throw_fatal("unknown class title", ((void*)NULL))));
          (v_pdbk = LINE(197,v_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)));
          LINE(198,v_linkCache.o_invoke_few_args("addGoodLinkObj", 0x70697DC360D0776ALL, 4, v_s.o_lval("page_id", 0x1B27A324993FB16CLL), v_title, v_s.o_lval("page_len", 0x2295E5F4A5F7AD5DLL), v_s.o_lval("page_is_redirect", 0x0F78ED8A1BDCEA05LL)));
          LINE(199,v_output.o_invoke_few_args("addLink", 0x5D10AC6ADCA895ACLL, 2, v_title, v_s.o_lval("page_id", 0x1B27A324993FB16CLL)));
          v_colours.set(v_pdbk, (LINE(203,v_sk.o_invoke_few_args("getLinkColour", 0x0EBA9C609550DE93LL, 2, v_title, v_threshold))));
          v_linkcolour_ids.set(v_s.o_get("page_id", 0x1B27A324993FB16CLL), (v_pdbk));
        }
      }
    }
    unset(v_res);
    LINE(209,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "GetLinkColours").set(1, Array(ArrayInit(2).set(0, v_linkcolour_ids).setRef(1, ref(v_colours)).create())).create()), 0x00000000787A96B2LL));
  }
  LINE(211,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceInternal-check").create()), 0x00000000B599F276LL));
  if (toBoolean(LINE(214,v_wgContLang.o_invoke_few_args("hasVariants", 0x5A1FD2E20BD8DBBBLL, 0)))) {
    LINE(215,t_dovariants(ref(v_colours)));
  }
  LINE(219,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceInternal-construct").create()), 0x0000000075359BAFLL));
  (v_replacePairs = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(14);
    Variant map15 = m_internals;
    for (ArrayIterPtr iter16 = map15.begin("linkholderarray"); !iter16->end(); iter16->next()) {
      LOOP_COUNTER_CHECK(14);
      v_entries = iter16->second();
      v_ns = iter16->first();
      {
        {
          LOOP_COUNTER(17);
          for (ArrayIterPtr iter19 = v_entries.begin("linkholderarray"); !iter19->end(); iter19->next()) {
            LOOP_COUNTER_CHECK(17);
            v_entry = iter19->second();
            v_index = iter19->first();
            {
              (v_pdbk = v_entry.rvalAt("pdbk", 0x18B80C52A2E3FCC5LL));
              (v_title = v_entry.rvalAt("title", 0x66AD900A2301E2FELL));
              (v_query = isset(v_entry, "query", 0x10A98E9AE3F54806LL) ? ((Variant)(v_entry.rvalAt("query", 0x10A98E9AE3F54806LL))) : ((Variant)("")));
              (v_key = LINE(226,concat3(toString(v_ns), ":", toString(v_index))));
              (v_searchkey = LINE(227,concat3("<!--LINK ", v_key, "-->")));
              if (!(isset(v_colours, v_pdbk)) || equal(v_colours.rvalAt(v_pdbk), "new")) {
                LINE(229,v_linkCache.o_invoke_few_args("addBadLinkObj", 0x6D7C22FD99A36EDBLL, 1, v_title));
                v_colours.set(v_pdbk, ("new"));
                LINE(231,v_output.o_invoke_few_args("addLink", 0x5D10AC6ADCA895ACLL, 2, v_title, 0LL));
                v_replacePairs.set(v_searchkey, ((assignCallTemp(eo_1, toObject(v_sk)),LINE(235,eo_1.o_invoke_few_args("makeBrokenLinkObj", 0x2A9E5694CCAA3096LL, 3, v_title, v_entry.refvalAt("text", 0x2A28A0084DD3A743LL), v_query)))));
              }
              else {
                v_replacePairs.set(v_searchkey, ((assignCallTemp(eo_2, toObject(v_sk)),LINE(240,eo_2.o_invoke_few_args("makeColouredLinkObj", 0x7BAFB7ABE2AF59F0LL, 4, v_title, v_colours.refvalAt(v_pdbk), v_entry.refvalAt("text", 0x2A28A0084DD3A743LL), v_query)))));
              }
            }
          }
        }
      }
    }
  }
  (v_replacer = LINE(244,create_object("hashtablereplacer", Array(ArrayInit(2).set(0, v_replacePairs).set(1, 1LL).create()))));
  LINE(245,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceInternal-construct").create()), 0x00000000B599F276LL));
  LINE(248,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceInternal-replace").create()), 0x0000000075359BAFLL));
  (v_text = LINE(252,(assignCallTemp(eo_4, LINE(251,v_replacer->o_invoke_few_args("cb", 0x02412237A7302F0ALL, 0))),assignCallTemp(eo_5, v_text),x_preg_replace_callback("/(<!--LINK .*\?-->)/", eo_4, eo_5))));
  LINE(254,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceInternal-replace").create()), 0x00000000B599F276LL));
  LINE(255,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceInternal").create()), 0x00000000B599F276LL));
} /* function */
/* SRC: LinkHolderArray.php line 261 */
void c_linkholderarray::t_replaceinterwiki(Variant v_text) {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::replaceInterwiki);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant v_sk;
  Variant v_replacePairs;
  Primitive v_key = 0;
  Variant v_link;
  Object v_replacer;

  if (empty(m_interwikis)) {
    return;
  }
  LINE(266,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceInterwiki").create()), 0x0000000075359BAFLL));
  (v_sk = (assignCallTemp(eo_0, toObject(LINE(268,m_parent.o_invoke_few_args("getOptions", 0x2040B36587CE56F9LL, 0)))),eo_0.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_replacePairs = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(20);
    Variant map21 = m_interwikis;
    for (ArrayIterPtr iter22 = map21.begin("linkholderarray"); !iter22->end(); iter22->next()) {
      LOOP_COUNTER_CHECK(20);
      v_link = iter22->second();
      v_key = iter22->first();
      {
        v_replacePairs.set(v_key, (LINE(271,v_sk.o_invoke_few_args("link", 0x230FE1D6EC599525LL, 2, v_link.refvalAt("title", 0x66AD900A2301E2FELL), v_link.refvalAt("text", 0x2A28A0084DD3A743LL)))));
      }
    }
  }
  (v_replacer = LINE(273,create_object("hashtablereplacer", Array(ArrayInit(2).set(0, v_replacePairs).set(1, 1LL).create()))));
  (v_text = LINE(278,(assignCallTemp(eo_2, LINE(277,v_replacer->o_invoke_few_args("cb", 0x02412237A7302F0ALL, 0))),assignCallTemp(eo_3, v_text),x_preg_replace_callback("/<!--IWLINK (.*\?)-->/", eo_2, eo_3))));
  LINE(279,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceInterwiki").create()), 0x00000000B599F276LL));
} /* function */
/* SRC: LinkHolderArray.php line 285 */
void c_linkholderarray::t_dovariants(Variant v_colours) {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::doVariants);
  Variant eo_0;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Object v_linkBatch;
  Variant v_variantMap;
  Variant v_output;
  Variant v_linkCache;
  Variant v_sk;
  Variant v_threshold;
  Variant v_ns;
  Variant v_entries;
  Variant v_index;
  Variant v_entry;
  Variant v_key;
  Variant v_pdbk;
  Variant v_title;
  Variant v_titleText;
  Variant v_allTextVariants;
  Variant v_textVariant;
  Variant v_variantTitle;
  Variant v_categoryMap;
  Variant v_varCategories;
  Variant v_category;
  Variant v_variants;
  Variant v_variant;
  Variant v_dbr;
  Variant v_page;
  Variant v_titleClause;
  Variant v_variantQuery;
  Variant v_varRes;
  Variant v_linkcolour_ids;
  Variant v_s;
  Variant v_varPdbk;
  Variant v_vardbk;
  Variant v_holderKeys;
  Variant v_oldkey;
  Variant v_newCats;
  Variant v_originalCats;
  Primitive v_cat = 0;
  Variant v_sortkey;

  (v_linkBatch = LINE(287,create_object("linkbatch", Array())));
  (v_variantMap = ScalarArrays::sa_[0]);
  (v_output = LINE(289,m_parent.o_invoke_few_args("getOutput", 0x7519C1FAF33D7857LL, 0)));
  (v_linkCache = LINE(290,throw_fatal("unknown class linkcache", ((void*)NULL))));
  (v_sk = (assignCallTemp(eo_0, toObject(LINE(291,m_parent.o_invoke_few_args("getOptions", 0x2040B36587CE56F9LL, 0)))),eo_0.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  (v_threshold = LINE(292,t_getstubthreshold()));
  {
    LOOP_COUNTER(23);
    Variant map24 = m_internals;
    for (ArrayIterPtr iter25 = map24.begin("linkholderarray"); !iter25->end(); iter25->next()) {
      LOOP_COUNTER_CHECK(23);
      v_entries = iter25->second();
      v_ns = iter25->first();
      {
        {
          LOOP_COUNTER(26);
          for (ArrayIterPtr iter28 = v_entries.begin("linkholderarray"); !iter28->end(); iter28->next()) {
            LOOP_COUNTER_CHECK(26);
            v_entry = iter28->second();
            v_index = iter28->first();
            {
              (v_key = LINE(297,concat3(toString(v_ns), ":", toString(v_index))));
              (v_pdbk = v_entry.rvalAt("pdbk", 0x18B80C52A2E3FCC5LL));
              (v_title = v_entry.rvalAt("title", 0x66AD900A2301E2FELL));
              (v_titleText = LINE(300,v_title.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0)));
              (v_allTextVariants = LINE(303,gv_wgContLang.o_invoke_few_args("convertLinkToAllVariants", 0x276E1379FA1C5660LL, 1, v_titleText)));
              if (!(isset(v_colours, v_pdbk))) {
                {
                  LOOP_COUNTER(29);
                  for (ArrayIterPtr iter31 = v_allTextVariants.begin("linkholderarray"); !iter31->end(); iter31->next()) {
                    LOOP_COUNTER_CHECK(29);
                    v_textVariant = iter31->second();
                    {
                      if (!equal(v_textVariant, v_titleText)) {
                        (v_variantTitle = LINE(309,throw_fatal("unknown class title", ((void*)NULL))));
                        if (LINE(310,x_is_null(v_variantTitle))) continue;
                        LINE(311,v_linkBatch->o_invoke_few_args("addObj", 0x7083015404130661LL, 1, v_variantTitle));
                        lval(v_variantMap.lvalAt(LINE(312,v_variantTitle.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)))).append((v_key));
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  (v_categoryMap = ScalarArrays::sa_[0]);
  (v_varCategories = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(32);
    Variant map33 = LINE(322,v_output.o_invoke_few_args("getCategoryLinks", 0x3374D72DB895979FLL, 0));
    for (ArrayIterPtr iter34 = map33.begin("linkholderarray"); !iter34->end(); iter34->next()) {
      LOOP_COUNTER_CHECK(32);
      v_category = iter34->second();
      {
        (v_variants = LINE(323,gv_wgContLang.o_invoke_few_args("convertLinkToAllVariants", 0x276E1379FA1C5660LL, 1, v_category)));
        {
          LOOP_COUNTER(35);
          for (ArrayIterPtr iter37 = v_variants.begin("linkholderarray"); !iter37->end(); iter37->next()) {
            LOOP_COUNTER_CHECK(35);
            v_variant = iter37->second();
            {
              if (!equal(v_variant, v_category)) {
                (v_variantTitle = LINE(326,throw_fatal("unknown class title", (throw_fatal("unknown class title", ((void*)NULL)), (void*)NULL))));
                if (LINE(327,x_is_null(v_variantTitle))) continue;
                LINE(328,v_linkBatch->o_invoke_few_args("addObj", 0x7083015404130661LL, 1, v_variantTitle));
                v_categoryMap.set(v_variant, (v_category));
              }
            }
          }
        }
      }
    }
  }
  if (!(toBoolean(LINE(335,v_linkBatch->o_invoke_few_args("isEmpty", 0x6359F42D5FC265E8LL, 0))))) {
    (v_dbr = LINE(337,invoke_failed("wfgetdb", Array(ArrayInit(1).set(0, k_DB_SLAVE).create()), 0x000000002DA3FF23LL)));
    (v_page = LINE(338,v_dbr.o_invoke_few_args("tableName", 0x15769EF1E0936722LL, 1, "page")));
    (v_titleClause = LINE(339,v_linkBatch->o_invoke_few_args("constructSet", 0x3E448E4FAA1093BALL, 2, "page", v_dbr)));
    (v_variantQuery = concat("SELECT page_id, page_namespace, page_title, page_is_redirect, page_len", LINE(341,concat4(" FROM ", toString(v_page), " WHERE ", toString(v_titleClause)))));
    (v_varRes = LINE(342,v_dbr.o_invoke_few_args("query", 0x356758D4414DA377LL, 2, v_variantQuery, "LinkHolderArray::doVariants")));
    (v_linkcolour_ids = ScalarArrays::sa_[0]);
    LOOP_COUNTER(38);
    {
      while (toBoolean((v_s = LINE(346,v_dbr.o_invoke_few_args("fetchObject", 0x0E1814AA3327229DLL, 1, v_varRes))))) {
        LOOP_COUNTER_CHECK(38);
        {
          (v_variantTitle = LINE(348,throw_fatal("unknown class title", ((void*)NULL))));
          (v_varPdbk = LINE(349,v_variantTitle.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)));
          (v_vardbk = LINE(350,v_variantTitle.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)));
          (v_holderKeys = ScalarArrays::sa_[0]);
          if (isset(v_variantMap, v_varPdbk)) {
            (v_holderKeys = v_variantMap.rvalAt(v_varPdbk));
            LINE(355,v_linkCache.o_invoke_few_args("addGoodLinkObj", 0x70697DC360D0776ALL, 4, v_s.o_lval("page_id", 0x1B27A324993FB16CLL), v_variantTitle, v_s.o_lval("page_len", 0x2295E5F4A5F7AD5DLL), v_s.o_lval("page_is_redirect", 0x0F78ED8A1BDCEA05LL)));
            LINE(356,v_output.o_invoke_few_args("addLink", 0x5D10AC6ADCA895ACLL, 2, v_variantTitle, v_s.o_lval("page_id", 0x1B27A324993FB16CLL)));
          }
          {
            LOOP_COUNTER(39);
            for (ArrayIterPtr iter41 = v_holderKeys.begin("linkholderarray"); !iter41->end(); iter41->next()) {
              LOOP_COUNTER_CHECK(39);
              v_key = iter41->second();
              {
                df_lambda_2(LINE(361,x_explode(":", toString(v_key), toInt32(2LL))), v_ns, v_index);
                (v_entry = ref(lval(lval(m_internals.lvalAt(v_ns)).lvalAt(v_index))));
                (v_pdbk = v_entry.rvalAt("pdbk", 0x18B80C52A2E3FCC5LL));
                if (!(isset(v_colours, v_pdbk))) {
                  v_entry.set("title", (v_variantTitle), 0x66AD900A2301E2FELL);
                  v_entry.set("pdbk", (v_varPdbk), 0x18B80C52A2E3FCC5LL);
                  v_colours.set(v_varPdbk, (LINE(374,v_sk.o_invoke_few_args("getLinkColour", 0x0EBA9C609550DE93LL, 2, v_variantTitle, v_threshold))));
                  v_linkcolour_ids.set(v_s.o_get("page_id", 0x1B27A324993FB16CLL), (v_pdbk));
                }
              }
            }
          }
          if (isset(v_categoryMap, v_vardbk)) {
            (v_oldkey = v_categoryMap.rvalAt(v_vardbk));
            if (!equal(v_oldkey, v_vardbk)) v_varCategories.set(v_oldkey, (v_vardbk));
          }
        }
      }
    }
    LINE(386,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "GetLinkColours").set(1, Array(ArrayInit(2).set(0, v_linkcolour_ids).setRef(1, ref(v_colours)).create())).create()), 0x00000000787A96B2LL));
    if (more(LINE(389,x_count(v_varCategories)), 0LL)) {
      (v_newCats = ScalarArrays::sa_[0]);
      (v_originalCats = LINE(391,v_output.o_invoke_few_args("getCategories", 0x72C61C208667527ELL, 0)));
      {
        LOOP_COUNTER(42);
        for (ArrayIterPtr iter44 = v_originalCats.begin("linkholderarray"); !iter44->end(); iter44->next()) {
          LOOP_COUNTER_CHECK(42);
          v_sortkey = iter44->second();
          v_cat = iter44->first();
          {
            if (LINE(394,x_array_key_exists(v_cat, v_varCategories))) v_newCats.set(v_varCategories.rvalAt(v_cat), (v_sortkey));
            else v_newCats.set(v_cat, (v_sortkey));
          }
        }
      }
      LINE(398,v_output.o_invoke_few_args("setCategoryLinks", 0x2295340384610322LL, 1, v_newCats));
    }
  }
} /* function */
/* SRC: LinkHolderArray.php line 409 */
Variant c_linkholderarray::t_replacetext(Variant v_text) {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::replaceText);
  LINE(410,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceText").create()), 0x0000000075359BAFLL));
  (v_text = LINE(415,x_preg_replace_callback("/<!--(LINK|IWLINK) (.*\?)-->/", Array(ArrayInit(2).setRef(0, ref(this)).set(1, "replaceTextCallback").create()), v_text)));
  LINE(417,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "LinkHolderArray::replaceText").create()), 0x00000000B599F276LL));
  return v_text;
} /* function */
/* SRC: LinkHolderArray.php line 426 */
Variant c_linkholderarray::t_replacetextcallback(CVarRef v_matches) {
  INSTANCE_METHOD_INJECTION(LinkHolderArray, LinkHolderArray::replaceTextCallback);
  Variant v_type;
  Variant v_key;
  Variant v_ns;
  Variant v_index;

  (v_type = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  (v_key = v_matches.rvalAt(2LL, 0x486AFCC090D5F98CLL));
  if (equal(v_type, "LINK")) {
    df_lambda_3(LINE(430,x_explode(":", toString(v_key), toInt32(2LL))), v_ns, v_index);
    if (isset(m_internals.rvalAt(v_ns).rvalAt(v_index), "text", 0x2A28A0084DD3A743LL)) {
      return m_internals.rvalAt(v_ns).rvalAt(v_index).rvalAt("text", 0x2A28A0084DD3A743LL);
    }
  }
  else if (equal(v_type, "IWLINK")) {
    if (isset(m_interwikis.rvalAt(v_key), "text", 0x2A28A0084DD3A743LL)) {
      return m_interwikis.rvalAt(v_key).rvalAt("text", 0x2A28A0084DD3A743LL);
    }
  }
  return v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
Object co_linkholderarray(CArrRef params, bool init /* = true */) {
  return Object(p_linkholderarray(NEW(c_linkholderarray)())->dynCreate(params, init));
}
Variant pm_php$LinkHolderArray_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::LinkHolderArray.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$LinkHolderArray_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
