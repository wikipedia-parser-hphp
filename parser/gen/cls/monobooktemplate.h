
#ifndef __GENERATED_cls_monobooktemplate_h__
#define __GENERATED_cls_monobooktemplate_h__

#include <cls/quicktemplate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 4362 */
class c_monobooktemplate : virtual public c_quicktemplate {
  BEGIN_CLASS_MAP(monobooktemplate)
    PARENT_CLASS(quicktemplate)
  END_CLASS_MAP(monobooktemplate)
  DECLARE_CLASS(monobooktemplate, MonoBookTemplate, quicktemplate)
  void init();
  public: Variant m_skin;
  public: void t_execute();
  public: void t_searchbox();
  public: void t_toolbox();
  public: void t_languagebox();
  public: void t_custombox(Primitive v_bar, CVarRef v_cont);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_monobooktemplate_h__
