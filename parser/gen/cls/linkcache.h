
#ifndef __GENERATED_cls_linkcache_h__
#define __GENERATED_cls_linkcache_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 15074 */
class c_linkcache : virtual public ObjectData {
  BEGIN_CLASS_MAP(linkcache)
  END_CLASS_MAP(linkcache)
  DECLARE_CLASS(linkcache, LinkCache, ObjectData)
  void init();
  public: Variant m_mClassVer;
  public: Variant m_mGoodLinks;
  public: Variant m_mBadLinks;
  public: Variant m_mForUpdate;
  public: static Variant ti_singleton(const char* cls);
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_forupdate(CVarRef v_update = null_variant);
  public: Variant t_getgoodlinkid(CVarRef v_title);
  public: Variant t_getgoodlinkfieldobj(CVarRef v_title, CVarRef v_field);
  public: bool t_isbadlink(CVarRef v_title);
  public: void t_addgoodlinkobj(CVarRef v_id, CVarRef v_title, CVarRef v_len = -1LL, CVarRef v_redir = null_variant);
  public: void t_addbadlinkobj(CVarRef v_title);
  public: void t_clearbadlink(CVarRef v_title);
  public: void t_clearlink(CVarRef v_title);
  public: Variant t_getgoodlinks();
  public: Variant t_getbadlinks();
  public: Variant t_addlink(CVarRef v_title, CVarRef v_len = -1LL, CVarRef v_redir = null_variant);
  public: Variant t_addlinkobj(Variant v_nt, Variant v_len = -1LL, Variant v_redirect = null);
  public: void t_clear();
  public: static Variant t_singleton() { return ti_singleton("linkcache"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_linkcache_h__
