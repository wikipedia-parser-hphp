
#ifndef __GENERATED_cls_stripstate_h__
#define __GENERATED_cls_stripstate_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 40438 */
class c_stripstate : virtual public ObjectData {
  BEGIN_CLASS_MAP(stripstate)
  END_CLASS_MAP(stripstate)
  DECLARE_CLASS(stripstate, StripState, ObjectData)
  void init();
  public: Variant m_general;
  public: Variant m_nowiki;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_unstripgeneral(Variant v_text);
  public: Variant t_unstripnowiki(Variant v_text);
  public: Variant t_unstripboth(Variant v_text);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_stripstate_h__
