<?xml version='1.0'?>
<xsl:stylesheet
  xmlns:AST="http://www.phpcompiler.org/phc-1.1"
  xmlns:past="http://www.parrotcode.org/PAST-0.1"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0" >

<xsl:output method='xml' indent='yes' />
<xsl:strip-space  elements="*"/>


<!--

TODO: AST:source_rep

-->
<!--

$Id: phc_xml_to_past_xml.xsl 33276 2008-11-27 20:12:54Z bernhard $

This transformation takes a XML abstract syntax tree as generated
by PHC from PHP source code. It generates an XML representation of a PAST data structure.

-->

<!--

structure: 

ELEMENT AST:PHP_script
    ELEMENT AST:Statement_list
      ELEMENT AST:Class_def
        ELEMENT AST:Class_mod
        ELEMENT AST:CLASS_NAME
        ELEMENT AST:CLASS_NAME
        ELEMENT AST:INTERFACE_NAME_list
        ELEMENT AST:Member_list

AST:Actual_parameter
AST:Actual_parameter_list
AST:Array
AST:Array_elem
AST:Array_elem_list
AST:Assignment
AST:Attr_mod
AST:Attribute
     AST:BOOL /bool
AST:Bin_op
AST:Break
AST:CAST
AST:CLASS_NAME
AST:CONSTANT_NAME
AST:Cast
AST:Class_def
AST:Class_mod
AST:Conditional_expr
AST:Constant
AST:Continue
AST:Do
AST:Eval_expr
AST:Expr
AST:Expr_list
AST:For
AST:Foreach
AST:Formal_parameter
AST:Formal_parameter_list
AST:Global
AST:INT
AST:INTERFACE_NAME_list
AST:If
AST:Ignore_errors
AST:Instanceof
AST:List_assignment
AST:List_element_list
AST:METHOD_NAME
AST:Member_list
AST:Method
AST:Method_invocation
AST:Method_mod
AST:NIL
AST:Name_with_default
AST:Name_with_default_list
AST:New
AST:Nop
AST:OP
AST:Op_assignment
AST:PHP_script
AST:Post_op
AST:Pre_op
AST:Reflection
AST:Return
AST:STRING
AST:Signature
AST:Statement_list
AST:Static_declaration
AST:Switch
AST:Switch_case
AST:Switch_case_list
AST:Target
AST:Throw
AST:Type
AST:Unary_op
AST:VARIABLE_NAME
AST:Variable
AST:Variable_name_list
AST:While
attr
attrs

integer
string
string_list
value

-->

<xsl:template match="/">
  <root>
  <xsl:apply-templates select="AST:PHP_script" />
</root>
</xsl:template>

<xsl:template match="text()">
  <MYTEXT><xsl:value-of select="."></xsl:value-of></MYTEXT>
</xsl:template>

<xsl:include href="newtemplates-inc.xsl"/>
<!--
file://./
-->
<xsl:template match="@*">
  <mattr>
    <xsl:for-each select="@*">
      attribute name: <xsl:value-of select="name()"/>
    attribute value: <xsl:value-of select="."/>
  </xsl:for-each>
  </mattr>

</xsl:template>

<xsl:template match="/root/PHP_script/Statement_list*/Class_def/Member_list/Method/">
  MIKE: method of a class
</xsl:template>

<xsl:template match="/*/Class_def/Member_list/Method/">
  MIKE: method of a class
</xsl:template>

<xsl:template match="text()">
  <MYTEXT>
    <xsl:copy-of select="text()"/>
    <xsl:value-of select="."></xsl:value-of>
  </MYTEXT>
</xsl:template>

 <xsl:template match="node()">
   <node>
     <pname><xsl:value-of select="name(..)"/></pname>
     <name><xsl:value-of select="name(.)"/></name>
     <text><xsl:value-of select="text()"/></text>   
     <child>
       <xsl:apply-templates select="./*" />
     </child>   
</node>
 </xsl:template>

 <xsl:template match="@*">
   <sattribute>
     <pname><xsl:value-of select="name(..)"/></pname>
     <name><xsl:value-of select="name(.)"/></name>
     <attributes><xsl:value-of select="."/></attributes>   
   </sattribute>
  
 </xsl:template>



<xsl:template match="AST:Method_invocation/AST:METHOD_NAME">
  <AST:Method_invocation><!--xsl: -->
  <AST:METHOD_NAME><!--xsl: -->
  <attributes>  <xsl:apply-templates select="@*"/>
</attributes>
<xsl:apply-templates />
<method_name> <xsl:value-of select="value" /></method_name>

<past:Op>
  <xsl:attribute name="name" ><xsl:value-of select="value" /></xsl:attribute>
  <xsl:apply-templates select="AST:Actual_parameter_list" />
</past:Op>

  </AST:METHOD_NAME><!--xsl: -->
</AST:Method_invocation><!--xsl: -->
</xsl:template>


<xsl:template mode="runGenericsParent" 
match="*">
<xsl:if test="name(..)"><xsl:apply-templates mode="runGenericsParent" 
select=".."/>/</xsl:if><xsl:value-of select="name(.)"/>
</xsl:template>

<!-- 
select="name(.)"></xsl:value-of>/<xsl:value-of 
  <xsl:value-of select="name(..)"></xsl:value-of>/<xsl:value-of 
  select="name(../..)"></xsl:value-of>/<xsl:value-of 
  select="name(../../..)"></xsl:value-of>/<xsl:value-of 
  select="name(../../../..)"></xsl:value-of>

-->
<xsl:template name="runGenerics">
  <path> 
  <xsl:apply-templates mode="runGenericsParent" select="."/>
</path> 
  <attributes> 
  <xsl:apply-templates select="@*"/> </attributes>
  <xsl:apply-templates />
</xsl:template>



<!--
     converted
 -->

<xsl:template match="AST:Method_invocation" >
  <Method_invocation>
  <past:Op>
    <xsl:attribute name="name" ><xsl:value-of select="AST:METHOD_NAME/value" /></xsl:attribute>
    <xsl:apply-templates select="AST:Actual_parameter_list" />
  </past:Op>
</Method_invocation>
</xsl:template>


</xsl:stylesheet>
