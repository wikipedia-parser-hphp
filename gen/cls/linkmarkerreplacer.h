
#ifndef __GENERATED_cls_linkmarkerreplacer_h__
#define __GENERATED_cls_linkmarkerreplacer_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser_LinkHooks.php line 279 */
class c_linkmarkerreplacer : virtual public ObjectData {
  BEGIN_CLASS_MAP(linkmarkerreplacer)
  END_CLASS_MAP(linkmarkerreplacer)
  DECLARE_CLASS(linkmarkerreplacer, LinkMarkerReplacer, ObjectData)
  void init();
  public: Variant m_markers;
  public: Variant m_nextId;
  public: Variant m_parser;
  public: Variant m_holders;
  public: Variant m_callback;
  public: void t___construct(Variant v_parser, Variant v_holders, Variant v_callback);
  public: ObjectData *create(Variant v_parser, Variant v_holders, Variant v_callback);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_addmarker(CVarRef v_titleText, CVarRef v_paramText);
  public: bool t_findmarker(CVarRef v_string);
  public: Variant t_expand(CVarRef v_string);
  public: Variant t_callback(CVarRef v_m);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_linkmarkerreplacer_h__
