
#ifndef __GENERATED_cls_regexlikereplacer_h__
#define __GENERATED_cls_regexlikereplacer_h__

#include <cls/replacer.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 40200 */
class c_regexlikereplacer : virtual public c_replacer {
  BEGIN_CLASS_MAP(regexlikereplacer)
    PARENT_CLASS(replacer)
  END_CLASS_MAP(regexlikereplacer)
  DECLARE_CLASS(regexlikereplacer, RegexlikeReplacer, replacer)
  void init();
  public: Variant m_r;
  public: void t___construct(Variant v_r);
  public: ObjectData *create(Variant v_r);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_replace(CVarRef v_matches);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_regexlikereplacer_h__
