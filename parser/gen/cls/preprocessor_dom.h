
#ifndef __GENERATED_cls_preprocessor_dom_h__
#define __GENERATED_cls_preprocessor_dom_h__

#include <cls/preprocessor.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 33687 */
class c_preprocessor_dom : virtual public c_preprocessor {
  BEGIN_CLASS_MAP(preprocessor_dom)
    PARENT_CLASS(preprocessor)
  END_CLASS_MAP(preprocessor_dom)
  DECLARE_CLASS(preprocessor_dom, Preprocessor_DOM, preprocessor)
  void init();
  public: Variant m_parser;
  public: Variant m_memoryLimit;
  public: void t___construct(Variant v_parser);
  public: ObjectData *create(Variant v_parser);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: p_ppframe_dom t_newframe();
  public: p_ppcustomframe_dom t_newcustomframe(CVarRef v_args);
  public: bool t_memcheck();
  public: p_ppnode_dom t_preprocesstoobj(CVarRef v_text, CVarRef v_flags = 0LL);
  public: Variant t_preprocesstoxml(Variant v_text, Variant v_flags = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_preprocessor_dom_h__
