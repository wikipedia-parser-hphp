
#include <php/Parser.h>
#include <php/Preprocessor.h>
#include <php/Preprocessor_DOM.h>
#include <php/Preprocessor_Hash.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: Preprocessor_Hash.php line 666 */
Variant c_ppdstack_hash::os_get(const char *s, int64 hash) {
  return c_ppdstack::os_get(s, hash);
}
Variant &c_ppdstack_hash::os_lval(const char *s, int64 hash) {
  return c_ppdstack::os_lval(s, hash);
}
void c_ppdstack_hash::o_get(ArrayElementVec &props) const {
  c_ppdstack::o_get(props);
}
bool c_ppdstack_hash::o_exists(CStrRef s, int64 hash) const {
  return c_ppdstack::o_exists(s, hash);
}
Variant c_ppdstack_hash::o_get(CStrRef s, int64 hash) {
  return c_ppdstack::o_get(s, hash);
}
Variant c_ppdstack_hash::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ppdstack::o_set(s, hash, v, forInit);
}
Variant &c_ppdstack_hash::o_lval(CStrRef s, int64 hash) {
  return c_ppdstack::o_lval(s, hash);
}
Variant c_ppdstack_hash::os_constant(const char *s) {
  return c_ppdstack::os_constant(s);
}
IMPLEMENT_CLASS(ppdstack_hash)
ObjectData *c_ppdstack_hash::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_ppdstack_hash::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_ppdstack_hash::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_ppdstack_hash::cloneImpl() {
  c_ppdstack_hash *obj = NEW(c_ppdstack_hash)();
  cloneSet(obj);
  return obj;
}
void c_ppdstack_hash::cloneSet(c_ppdstack_hash *clone) {
  c_ppdstack::cloneSet(clone);
}
Variant c_ppdstack_hash::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x5CEFA5A265104D10LL, count) {
        return (t_count());
      }
      break;
    case 1:
      HASH_GUARD(0x27E7DBA875AD17E1LL, getflags) {
        return (t_getflags());
      }
      break;
    case 2:
      HASH_GUARD(0x499E72B719A4CAC2LL, addpart) {
        int count = params.size();
        if (count <= 0) return (t_addpart(), null);
        return (t_addpart(params.rvalAt(0)), null);
      }
      break;
    case 3:
      HASH_GUARD(0x773C5A963CD2AC13LL, pop) {
        return (t_pop());
      }
      break;
    case 6:
      HASH_GUARD(0x1A5F5A2CB292E516LL, getaccum) {
        return ref(t_getaccum());
      }
      break;
    case 8:
      HASH_GUARD(0x321E2BF5D878AA38LL, push) {
        return (t_push(params.rvalAt(0)), null);
      }
      break;
    case 12:
      HASH_GUARD(0x1726802D706D7BECLL, getcurrentpart) {
        return (t_getcurrentpart());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ppdstack::o_invoke(s, params, hash, fatal);
}
Variant c_ppdstack_hash::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 0:
      HASH_GUARD(0x5CEFA5A265104D10LL, count) {
        return (t_count());
      }
      break;
    case 1:
      HASH_GUARD(0x27E7DBA875AD17E1LL, getflags) {
        return (t_getflags());
      }
      break;
    case 2:
      HASH_GUARD(0x499E72B719A4CAC2LL, addpart) {
        if (count <= 0) return (t_addpart(), null);
        return (t_addpart(a0), null);
      }
      break;
    case 3:
      HASH_GUARD(0x773C5A963CD2AC13LL, pop) {
        return (t_pop());
      }
      break;
    case 6:
      HASH_GUARD(0x1A5F5A2CB292E516LL, getaccum) {
        return ref(t_getaccum());
      }
      break;
    case 8:
      HASH_GUARD(0x321E2BF5D878AA38LL, push) {
        return (t_push(a0), null);
      }
      break;
    case 12:
      HASH_GUARD(0x1726802D706D7BECLL, getcurrentpart) {
        return (t_getcurrentpart());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ppdstack::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppdstack_hash::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ppdstack::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppdstack_hash$os_get(const char *s) {
  return c_ppdstack_hash::os_get(s, -1);
}
Variant &cw_ppdstack_hash$os_lval(const char *s) {
  return c_ppdstack_hash::os_lval(s, -1);
}
Variant cw_ppdstack_hash$os_constant(const char *s) {
  return c_ppdstack_hash::os_constant(s);
}
Variant cw_ppdstack_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppdstack_hash::os_invoke(c, s, params, -1, fatal);
}
void c_ppdstack_hash::init() {
  c_ppdstack::init();
}
/* SRC: Preprocessor_Hash.php line 667 */
void c_ppdstack_hash::t___construct() {
  INSTANCE_METHOD_INJECTION(PPDStack_Hash, PPDStack_Hash::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_elementClass = "PPDStackElement_Hash");
  LINE(669,c_ppdstack::t___construct());
  (m_rootAccum = ((Object)(LINE(670,p_ppdaccum_hash(p_ppdaccum_hash(NEWOBJ(c_ppdaccum_hash)())->create())))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 1523 */
Variant c_ppnode_hash_text::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppnode_hash_text::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ppnode_hash_text::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("value", m_value.isReferenced() ? ref(m_value) : m_value));
  props.push_back(NEW(ArrayElement)("nextSibling", m_nextSibling.isReferenced() ? ref(m_nextSibling) : m_nextSibling));
  c_ObjectData::o_get(props);
}
bool c_ppnode_hash_text::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x69E7413AE0C88471LL, value, 5);
      break;
    case 2:
      HASH_EXISTS_STRING(0x0B3EC7643EE81822LL, nextSibling, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppnode_hash_text::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                         nextSibling, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppnode_hash_text::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x69E7413AE0C88471LL, m_value,
                      value, 5);
      break;
    case 2:
      HASH_SET_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                      nextSibling, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppnode_hash_text::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                         nextSibling, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppnode_hash_text::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppnode_hash_text)
ObjectData *c_ppnode_hash_text::create(Variant v_value) {
  init();
  t___construct(v_value);
  return this;
}
ObjectData *c_ppnode_hash_text::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_ppnode_hash_text::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_ppnode_hash_text::cloneImpl() {
  c_ppnode_hash_text *obj = NEW(c_ppnode_hash_text)();
  cloneSet(obj);
  return obj;
}
void c_ppnode_hash_text::cloneSet(c_ppnode_hash_text *clone) {
  clone->m_value = m_value.isReferenced() ? ref(m_value) : m_value;
  clone->m_nextSibling = m_nextSibling.isReferenced() ? ref(m_nextSibling) : m_nextSibling;
  ObjectData::cloneSet(clone);
}
Variant c_ppnode_hash_text::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 0:
      HASH_GUARD(0x73E7C3304C0C5360LL, splitarg) {
        return (t_splitarg(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 6:
      HASH_GUARD(0x0A41DBE0830902C6LL, item) {
        return (t_item(params.rvalAt(0)));
      }
      break;
    case 9:
      HASH_GUARD(0x050681239965E069LL, getfirstchild) {
        return (t_getfirstchild());
      }
      break;
    case 12:
      HASH_GUARD(0x47D4EE3CB3EB696CLL, splitext) {
        return (t_splitext(), null);
      }
      break;
    case 13:
      HASH_GUARD(0x0621AE2D22A1922DLL, splitheading) {
        return (t_splitheading(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x5ABDC66291F1CA2ELL, getchildrenoftype) {
        return (t_getchildrenoftype(params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x732EC1BDA8EC520FLL, getchildren) {
        return (t_getchildren());
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 20:
      HASH_GUARD(0x41EB078EF92C44D4LL, getlength) {
        return (t_getlength());
      }
      break;
    case 25:
      HASH_GUARD(0x4CB333CF2D880119LL, getnextsibling) {
        return (t_getnextsibling());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppnode_hash_text::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 0:
      HASH_GUARD(0x73E7C3304C0C5360LL, splitarg) {
        return (t_splitarg(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 6:
      HASH_GUARD(0x0A41DBE0830902C6LL, item) {
        return (t_item(a0));
      }
      break;
    case 9:
      HASH_GUARD(0x050681239965E069LL, getfirstchild) {
        return (t_getfirstchild());
      }
      break;
    case 12:
      HASH_GUARD(0x47D4EE3CB3EB696CLL, splitext) {
        return (t_splitext(), null);
      }
      break;
    case 13:
      HASH_GUARD(0x0621AE2D22A1922DLL, splitheading) {
        return (t_splitheading(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x5ABDC66291F1CA2ELL, getchildrenoftype) {
        return (t_getchildrenoftype(a0));
      }
      break;
    case 15:
      HASH_GUARD(0x732EC1BDA8EC520FLL, getchildren) {
        return (t_getchildren());
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 20:
      HASH_GUARD(0x41EB078EF92C44D4LL, getlength) {
        return (t_getlength());
      }
      break;
    case 25:
      HASH_GUARD(0x4CB333CF2D880119LL, getnextsibling) {
        return (t_getnextsibling());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppnode_hash_text::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppnode_hash_text$os_get(const char *s) {
  return c_ppnode_hash_text::os_get(s, -1);
}
Variant &cw_ppnode_hash_text$os_lval(const char *s) {
  return c_ppnode_hash_text::os_lval(s, -1);
}
Variant cw_ppnode_hash_text$os_constant(const char *s) {
  return c_ppnode_hash_text::os_constant(s);
}
Variant cw_ppnode_hash_text$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppnode_hash_text::os_invoke(c, s, params, -1, fatal);
}
void c_ppnode_hash_text::init() {
  m_value = null;
  m_nextSibling = null;
}
/* SRC: Preprocessor_Hash.php line 1526 */
void c_ppnode_hash_text::t___construct(Variant v_value) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::__construct);
  bool oldInCtor = gasInCtor(true);
  if (LINE(1527,x_is_object(v_value))) {
    throw_exception(LINE(1528,create_object("mwexception", Array(ArrayInit(1).set(0, "PPNode_Hash_Text given object instead of string").create()))));
  }
  (m_value = v_value);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 1533 */
String c_ppnode_hash_text::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::__toString);
  return LINE(1534,x_htmlspecialchars(toString(m_value)));
} /* function */
/* SRC: Preprocessor_Hash.php line 1537 */
Variant c_ppnode_hash_text::t_getnextsibling() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::getNextSibling);
  return m_nextSibling;
} /* function */
/* SRC: Preprocessor_Hash.php line 1541 */
bool c_ppnode_hash_text::t_getchildren() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::getChildren);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1542 */
bool c_ppnode_hash_text::t_getfirstchild() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::getFirstChild);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1543 */
bool c_ppnode_hash_text::t_getchildrenoftype(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::getChildrenOfType);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1544 */
bool c_ppnode_hash_text::t_getlength() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::getLength);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1545 */
bool c_ppnode_hash_text::t_item(CVarRef v_i) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::item);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1546 */
String c_ppnode_hash_text::t_getname() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::getName);
  return "#text";
} /* function */
/* SRC: Preprocessor_Hash.php line 1547 */
void c_ppnode_hash_text::t_splitarg() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::splitArg);
  throw_exception(LINE(1547,create_object("mwexception", Array(ArrayInit(1).set(0, "PPNode_Hash_Text::splitArg: not supported").create()))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1548 */
void c_ppnode_hash_text::t_splitext() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::splitExt);
  throw_exception(LINE(1548,create_object("mwexception", Array(ArrayInit(1).set(0, "PPNode_Hash_Text::splitExt: not supported").create()))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1549 */
void c_ppnode_hash_text::t_splitheading() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Text, PPNode_Hash_Text::splitHeading);
  throw_exception(LINE(1549,create_object("mwexception", Array(ArrayInit(1).set(0, "PPNode_Hash_Text::splitHeading: not supported").create()))));
} /* function */
/* SRC: Preprocessor_Hash.php line 725 */
Variant c_ppdaccum_hash::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppdaccum_hash::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ppdaccum_hash::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("firstNode", m_firstNode.isReferenced() ? ref(m_firstNode) : m_firstNode));
  props.push_back(NEW(ArrayElement)("lastNode", m_lastNode.isReferenced() ? ref(m_lastNode) : m_lastNode));
  c_ObjectData::o_get(props);
}
bool c_ppdaccum_hash::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x6B446EEF03FAE201LL, lastNode, 8);
      break;
    case 2:
      HASH_EXISTS_STRING(0x4296B55256FA0986LL, firstNode, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppdaccum_hash::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x6B446EEF03FAE201LL, m_lastNode,
                         lastNode, 8);
      break;
    case 2:
      HASH_RETURN_STRING(0x4296B55256FA0986LL, m_firstNode,
                         firstNode, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppdaccum_hash::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x6B446EEF03FAE201LL, m_lastNode,
                      lastNode, 8);
      break;
    case 2:
      HASH_SET_STRING(0x4296B55256FA0986LL, m_firstNode,
                      firstNode, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppdaccum_hash::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x6B446EEF03FAE201LL, m_lastNode,
                         lastNode, 8);
      break;
    case 2:
      HASH_RETURN_STRING(0x4296B55256FA0986LL, m_firstNode,
                         firstNode, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppdaccum_hash::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppdaccum_hash)
ObjectData *c_ppdaccum_hash::create() {
  init();
  t___construct();
  return this;
}
ObjectData *c_ppdaccum_hash::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create());
  } else return this;
}
void c_ppdaccum_hash::dynConstruct(CArrRef params) {
  (t___construct());
}
ObjectData *c_ppdaccum_hash::cloneImpl() {
  c_ppdaccum_hash *obj = NEW(c_ppdaccum_hash)();
  cloneSet(obj);
  return obj;
}
void c_ppdaccum_hash::cloneSet(c_ppdaccum_hash *clone) {
  clone->m_firstNode = m_firstNode.isReferenced() ? ref(m_firstNode) : m_firstNode;
  clone->m_lastNode = m_lastNode.isReferenced() ? ref(m_lastNode) : m_lastNode;
  ObjectData::cloneSet(clone);
}
Variant c_ppdaccum_hash::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x0D8DDF05964C6A31LL, addnode) {
        return (t_addnode(params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x6992F0F690B37356LL, addnodewithtext) {
        return (t_addnodewithtext(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 9:
      HASH_GUARD(0x6A8DCEFA6345DAC9LL, addaccum) {
        return (t_addaccum(params.rvalAt(0)), null);
      }
      break;
    case 14:
      HASH_GUARD(0x05AAC40C7DAD4ACELL, addliteral) {
        return (t_addliteral(params.rvalAt(0)), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppdaccum_hash::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x0D8DDF05964C6A31LL, addnode) {
        return (t_addnode(a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x6992F0F690B37356LL, addnodewithtext) {
        return (t_addnodewithtext(a0, a1), null);
      }
      break;
    case 9:
      HASH_GUARD(0x6A8DCEFA6345DAC9LL, addaccum) {
        return (t_addaccum(a0), null);
      }
      break;
    case 14:
      HASH_GUARD(0x05AAC40C7DAD4ACELL, addliteral) {
        return (t_addliteral(a0), null);
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppdaccum_hash::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppdaccum_hash$os_get(const char *s) {
  return c_ppdaccum_hash::os_get(s, -1);
}
Variant &cw_ppdaccum_hash$os_lval(const char *s) {
  return c_ppdaccum_hash::os_lval(s, -1);
}
Variant cw_ppdaccum_hash$os_constant(const char *s) {
  return c_ppdaccum_hash::os_constant(s);
}
Variant cw_ppdaccum_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppdaccum_hash::os_invoke(c, s, params, -1, fatal);
}
void c_ppdaccum_hash::init() {
  m_firstNode = null;
  m_lastNode = null;
}
/* SRC: Preprocessor_Hash.php line 728 */
void c_ppdaccum_hash::t___construct() {
  INSTANCE_METHOD_INJECTION(PPDAccum_Hash, PPDAccum_Hash::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_firstNode = (m_lastNode = false));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 735 */
void c_ppdaccum_hash::t_addliteral(CVarRef v_s) {
  INSTANCE_METHOD_INJECTION(PPDAccum_Hash, PPDAccum_Hash::addLiteral);
  if (same(m_lastNode, false)) {
    (m_firstNode = ((Object)((m_lastNode = ((Object)(LINE(737,p_ppnode_hash_text(p_ppnode_hash_text(NEWOBJ(c_ppnode_hash_text)())->create(v_s)))))))));
  }
  else if (instanceOf(m_lastNode, "PPNode_Hash_Text")) {
    concat_assign(m_lastNode.o_lval("value", 0x69E7413AE0C88471LL), toString(v_s));
  }
  else {
    (m_lastNode.o_lval("nextSibling", 0x0B3EC7643EE81822LL) = ((Object)(LINE(741,p_ppnode_hash_text(p_ppnode_hash_text(NEWOBJ(c_ppnode_hash_text)())->create(v_s))))));
    (m_lastNode = m_lastNode.o_get("nextSibling", 0x0B3EC7643EE81822LL));
  }
} /* function */
/* SRC: Preprocessor_Hash.php line 749 */
void c_ppdaccum_hash::t_addnode(p_ppnode v_node) {
  INSTANCE_METHOD_INJECTION(PPDAccum_Hash, PPDAccum_Hash::addNode);
  if (same(m_lastNode, false)) {
    (m_firstNode = ((Object)((m_lastNode = ((Object)(v_node))))));
  }
  else {
    (m_lastNode.o_lval("nextSibling", 0x0B3EC7643EE81822LL) = ((Object)(v_node)));
    (m_lastNode = ((Object)(v_node)));
  }
} /* function */
/* SRC: Preprocessor_Hash.php line 761 */
void c_ppdaccum_hash::t_addnodewithtext(CVarRef v_name, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(PPDAccum_Hash, PPDAccum_Hash::addNodeWithText);
  Object v_node;

  (v_node = LINE(762,c_ppnode_hash_tree::t_newwithtext(v_name, v_value)));
  LINE(763,t_addnode(p_ppnode(v_node)));
} /* function */
/* SRC: Preprocessor_Hash.php line 771 */
void c_ppdaccum_hash::t_addaccum(CVarRef v_accum) {
  INSTANCE_METHOD_INJECTION(PPDAccum_Hash, PPDAccum_Hash::addAccum);
  if (same(toObject(v_accum).o_get("lastNode", 0x6B446EEF03FAE201LL), false)) {
  }
  else if (same(m_lastNode, false)) {
    (m_firstNode = toObject(v_accum).o_get("firstNode", 0x4296B55256FA0986LL));
    (m_lastNode = toObject(v_accum).o_get("lastNode", 0x6B446EEF03FAE201LL));
  }
  else {
    (m_lastNode.o_lval("nextSibling", 0x0B3EC7643EE81822LL) = toObject(v_accum).o_get("firstNode", 0x4296B55256FA0986LL));
    (m_lastNode = toObject(v_accum).o_get("lastNode", 0x6B446EEF03FAE201LL));
  }
} /* function */
/* SRC: Preprocessor_Hash.php line 1336 */
Variant c_ppnode_hash_tree::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppnode_hash_tree::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ppnode_hash_tree::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("name", m_name.isReferenced() ? ref(m_name) : m_name));
  props.push_back(NEW(ArrayElement)("firstChild", m_firstChild.isReferenced() ? ref(m_firstChild) : m_firstChild));
  props.push_back(NEW(ArrayElement)("lastChild", m_lastChild.isReferenced() ? ref(m_lastChild) : m_lastChild));
  props.push_back(NEW(ArrayElement)("nextSibling", m_nextSibling.isReferenced() ? ref(m_nextSibling) : m_nextSibling));
  c_ObjectData::o_get(props);
}
bool c_ppnode_hash_tree::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_EXISTS_STRING(0x22862CD26C0AAB50LL, firstChild, 10);
      break;
    case 2:
      HASH_EXISTS_STRING(0x29DD4C280399C7FALL, lastChild, 9);
      HASH_EXISTS_STRING(0x0B3EC7643EE81822LL, nextSibling, 11);
      break;
    case 4:
      HASH_EXISTS_STRING(0x0BCDB293DC3CBDDCLL, name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppnode_hash_tree::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x22862CD26C0AAB50LL, m_firstChild,
                         firstChild, 10);
      break;
    case 2:
      HASH_RETURN_STRING(0x29DD4C280399C7FALL, m_lastChild,
                         lastChild, 9);
      HASH_RETURN_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                         nextSibling, 11);
      break;
    case 4:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppnode_hash_tree::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_SET_STRING(0x22862CD26C0AAB50LL, m_firstChild,
                      firstChild, 10);
      break;
    case 2:
      HASH_SET_STRING(0x29DD4C280399C7FALL, m_lastChild,
                      lastChild, 9);
      HASH_SET_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                      nextSibling, 11);
      break;
    case 4:
      HASH_SET_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                      name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppnode_hash_tree::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 0:
      HASH_RETURN_STRING(0x22862CD26C0AAB50LL, m_firstChild,
                         firstChild, 10);
      break;
    case 2:
      HASH_RETURN_STRING(0x29DD4C280399C7FALL, m_lastChild,
                         lastChild, 9);
      HASH_RETURN_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                         nextSibling, 11);
      break;
    case 4:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppnode_hash_tree::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppnode_hash_tree)
ObjectData *c_ppnode_hash_tree::create(Variant v_name) {
  init();
  t___construct(v_name);
  return this;
}
ObjectData *c_ppnode_hash_tree::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_ppnode_hash_tree::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_ppnode_hash_tree::cloneImpl() {
  c_ppnode_hash_tree *obj = NEW(c_ppnode_hash_tree)();
  cloneSet(obj);
  return obj;
}
void c_ppnode_hash_tree::cloneSet(c_ppnode_hash_tree *clone) {
  clone->m_name = m_name.isReferenced() ? ref(m_name) : m_name;
  clone->m_firstChild = m_firstChild.isReferenced() ? ref(m_firstChild) : m_firstChild;
  clone->m_lastChild = m_lastChild.isReferenced() ? ref(m_lastChild) : m_lastChild;
  clone->m_nextSibling = m_nextSibling.isReferenced() ? ref(m_nextSibling) : m_nextSibling;
  ObjectData::cloneSet(clone);
}
Variant c_ppnode_hash_tree::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 0:
      HASH_GUARD(0x73E7C3304C0C5360LL, splitarg) {
        return (t_splitarg());
      }
      break;
    case 5:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 6:
      HASH_GUARD(0x0A41DBE0830902C6LL, item) {
        return (t_item(params.rvalAt(0)));
      }
      break;
    case 9:
      HASH_GUARD(0x050681239965E069LL, getfirstchild) {
        return (t_getfirstchild());
      }
      break;
    case 12:
      HASH_GUARD(0x47D4EE3CB3EB696CLL, splitext) {
        return (t_splitext());
      }
      break;
    case 13:
      HASH_GUARD(0x0621AE2D22A1922DLL, splitheading) {
        return (t_splitheading());
      }
      HASH_GUARD(0x5D6BF11378AA7D8DLL, splittemplate) {
        return (t_splittemplate());
      }
      break;
    case 14:
      HASH_GUARD(0x5ABDC66291F1CA2ELL, getchildrenoftype) {
        return (t_getchildrenoftype(params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x732EC1BDA8EC520FLL, getchildren) {
        return (t_getchildren());
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 20:
      HASH_GUARD(0x41EB078EF92C44D4LL, getlength) {
        return (t_getlength());
      }
      HASH_GUARD(0x2DDE12A9866FC794LL, addchild) {
        return (t_addchild(params.rvalAt(0)), null);
      }
      break;
    case 25:
      HASH_GUARD(0x4CB333CF2D880119LL, getnextsibling) {
        return (t_getnextsibling());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppnode_hash_tree::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 0:
      HASH_GUARD(0x73E7C3304C0C5360LL, splitarg) {
        return (t_splitarg());
      }
      break;
    case 5:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 6:
      HASH_GUARD(0x0A41DBE0830902C6LL, item) {
        return (t_item(a0));
      }
      break;
    case 9:
      HASH_GUARD(0x050681239965E069LL, getfirstchild) {
        return (t_getfirstchild());
      }
      break;
    case 12:
      HASH_GUARD(0x47D4EE3CB3EB696CLL, splitext) {
        return (t_splitext());
      }
      break;
    case 13:
      HASH_GUARD(0x0621AE2D22A1922DLL, splitheading) {
        return (t_splitheading());
      }
      HASH_GUARD(0x5D6BF11378AA7D8DLL, splittemplate) {
        return (t_splittemplate());
      }
      break;
    case 14:
      HASH_GUARD(0x5ABDC66291F1CA2ELL, getchildrenoftype) {
        return (t_getchildrenoftype(a0));
      }
      break;
    case 15:
      HASH_GUARD(0x732EC1BDA8EC520FLL, getchildren) {
        return (t_getchildren());
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 20:
      HASH_GUARD(0x41EB078EF92C44D4LL, getlength) {
        return (t_getlength());
      }
      HASH_GUARD(0x2DDE12A9866FC794LL, addchild) {
        return (t_addchild(a0), null);
      }
      break;
    case 25:
      HASH_GUARD(0x4CB333CF2D880119LL, getnextsibling) {
        return (t_getnextsibling());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppnode_hash_tree::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppnode_hash_tree$os_get(const char *s) {
  return c_ppnode_hash_tree::os_get(s, -1);
}
Variant &cw_ppnode_hash_tree$os_lval(const char *s) {
  return c_ppnode_hash_tree::os_lval(s, -1);
}
Variant cw_ppnode_hash_tree$os_constant(const char *s) {
  return c_ppnode_hash_tree::os_constant(s);
}
Variant cw_ppnode_hash_tree$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppnode_hash_tree::os_invoke(c, s, params, -1, fatal);
}
void c_ppnode_hash_tree::init() {
  m_name = null;
  m_firstChild = null;
  m_lastChild = null;
  m_nextSibling = null;
}
/* SRC: Preprocessor_Hash.php line 1339 */
void c_ppnode_hash_tree::t___construct(Variant v_name) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_name = v_name);
  (m_firstChild = (m_lastChild = (m_nextSibling = false)));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 1344 */
String c_ppnode_hash_tree::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::__toString);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  String v_inner;
  String v_attribs;
  Variant v_node;

  (v_inner = "");
  (v_attribs = "");
  {
    LOOP_COUNTER(1);
    for ((v_node = m_firstChild); toBoolean(v_node); (v_node = v_node.o_get("nextSibling", 0x0B3EC7643EE81822LL))) {
      LOOP_COUNTER_CHECK(1);
      {
        if (instanceOf(v_node, "PPNode_Hash_Attr")) {
          concat_assign(v_attribs, LINE(1349,(assignCallTemp(eo_1, toString(v_node.o_get("name", 0x0BCDB293DC3CBDDCLL))),assignCallTemp(eo_3, x_htmlspecialchars(toString(v_node.o_get("value", 0x69E7413AE0C88471LL)))),concat5(" ", eo_1, "=\"", eo_3, "\""))));
        }
        else {
          concat_assign(v_inner, toString(LINE(1351,v_node.o_invoke_few_args("__toString", 0x642C2D2994B34A13LL, 0))));
        }
      }
    }
  }
  if (same(v_inner, "")) {
    return LINE(1355,concat4("<", toString(m_name), v_attribs, "/>"));
  }
  else {
    return LINE(1357,(assignCallTemp(eo_1, toString(m_name)),assignCallTemp(eo_2, concat6(v_attribs, ">", v_inner, "</", toString(m_name), ">")),concat3("<", eo_1, eo_2)));
  }
  return toString(null);
} /* function */
/* SRC: Preprocessor_Hash.php line 1361 */
Object c_ppnode_hash_tree::ti_newwithtext(const char* cls, CVarRef v_name, CVarRef v_text) {
  STATIC_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::newWithText);
  Object v_obj;

  (v_obj = LINE(1362,create_object("self", Array(ArrayInit(1).set(0, v_name).create()))));
  LINE(1363,v_obj->o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, p_ppnode_hash_text(p_ppnode_hash_text(NEWOBJ(c_ppnode_hash_text)())->create(v_text))));
  return v_obj;
} /* function */
/* SRC: Preprocessor_Hash.php line 1367 */
void c_ppnode_hash_tree::t_addchild(CVarRef v_node) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::addChild);
  if (same(m_lastChild, false)) {
    (m_firstChild = (m_lastChild = v_node));
  }
  else {
    (m_lastChild.o_lval("nextSibling", 0x0B3EC7643EE81822LL) = v_node);
    (m_lastChild = v_node);
  }
} /* function */
/* SRC: Preprocessor_Hash.php line 1376 */
p_ppnode_hash_array c_ppnode_hash_tree::t_getchildren() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::getChildren);
  Array v_children;
  Variant v_child;

  (v_children = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(2);
    for ((v_child = m_firstChild); toBoolean(v_child); (v_child = v_child.o_get("nextSibling", 0x0B3EC7643EE81822LL))) {
      LOOP_COUNTER_CHECK(2);
      {
        v_children.append((v_child));
      }
    }
  }
  return ((Object)(LINE(1381,p_ppnode_hash_array(p_ppnode_hash_array(NEWOBJ(c_ppnode_hash_array)())->create(v_children)))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1384 */
Variant c_ppnode_hash_tree::t_getfirstchild() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::getFirstChild);
  return m_firstChild;
} /* function */
/* SRC: Preprocessor_Hash.php line 1388 */
Variant c_ppnode_hash_tree::t_getnextsibling() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::getNextSibling);
  return m_nextSibling;
} /* function */
/* SRC: Preprocessor_Hash.php line 1392 */
Array c_ppnode_hash_tree::t_getchildrenoftype(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::getChildrenOfType);
  Array v_children;
  Variant v_child;

  (v_children = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(3);
    for ((v_child = m_firstChild); toBoolean(v_child); (v_child = v_child.o_get("nextSibling", 0x0B3EC7643EE81822LL))) {
      LOOP_COUNTER_CHECK(3);
      {
        if (toObject(v_child)->t___isset("name") && same(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), v_name)) {
          v_children.append((v_name));
        }
      }
    }
  }
  return v_children;
} /* function */
/* SRC: Preprocessor_Hash.php line 1402 */
bool c_ppnode_hash_tree::t_getlength() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::getLength);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1403 */
bool c_ppnode_hash_tree::t_item(CVarRef v_i) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::item);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1405 */
Variant c_ppnode_hash_tree::t_getname() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::getName);
  return m_name;
} /* function */
/* SRC: Preprocessor_Hash.php line 1415 */
Variant c_ppnode_hash_tree::t_splitarg() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::splitArg);
  Variant v_bits;
  Variant v_child;

  (v_bits = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(4);
    for ((v_child = m_firstChild); toBoolean(v_child); (v_child = v_child.o_get("nextSibling", 0x0B3EC7643EE81822LL))) {
      LOOP_COUNTER_CHECK(4);
      {
        if (!(toObject(v_child)->t___isset("name"))) {
          continue;
        }
        if (same(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "name")) {
          v_bits.set("name", (v_child), 0x0BCDB293DC3CBDDCLL);
          if (instanceOf(v_child.o_get("firstChild", 0x22862CD26C0AAB50LL), "PPNode_Hash_Attr") && same(v_child.o_get("firstChild", 0x22862CD26C0AAB50LL).o_get("name", 0x0BCDB293DC3CBDDCLL), "index")) {
            v_bits.set("index", (v_child.o_get("firstChild", 0x22862CD26C0AAB50LL).o_get("value", 0x69E7413AE0C88471LL)), 0x440D5888C0FF3081LL);
          }
        }
        else if (same(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "value")) {
          v_bits.set("value", (v_child), 0x69E7413AE0C88471LL);
        }
      }
    }
  }
  if (!(isset(v_bits, "name", 0x0BCDB293DC3CBDDCLL))) {
    throw_exception(LINE(1434,create_object("mwexception", Array(ArrayInit(1).set(0, "Invalid brace node passed to PPNode_Hash_Tree::splitArg").create()))));
  }
  if (!(isset(v_bits, "index", 0x440D5888C0FF3081LL))) {
    v_bits.set("index", (""), 0x440D5888C0FF3081LL);
  }
  return v_bits;
} /* function */
/* SRC: Preprocessor_Hash.php line 1446 */
Variant c_ppnode_hash_tree::t_splitext() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::splitExt);
  Variant v_bits;
  Variant v_child;

  (v_bits = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(5);
    for ((v_child = m_firstChild); toBoolean(v_child); (v_child = v_child.o_get("nextSibling", 0x0B3EC7643EE81822LL))) {
      LOOP_COUNTER_CHECK(5);
      {
        if (!(toObject(v_child)->t___isset("name"))) {
          continue;
        }
        if (equal(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "name")) {
          v_bits.set("name", (v_child), 0x0BCDB293DC3CBDDCLL);
        }
        else if (equal(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "attr")) {
          v_bits.set("attr", (v_child), 0x64311A2C8443755DLL);
        }
        else if (equal(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "inner")) {
          v_bits.set("inner", (v_child), 0x4F20D07BB803C1FELL);
        }
        else if (equal(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "close")) {
          v_bits.set("close", (v_child), 0x36B43082F052EC6BLL);
        }
      }
    }
  }
  if (!(isset(v_bits, "name", 0x0BCDB293DC3CBDDCLL))) {
    throw_exception(LINE(1463,create_object("mwexception", Array(ArrayInit(1).set(0, "Invalid ext node passed to PPNode_Hash_Tree::splitExt").create()))));
  }
  return v_bits;
} /* function */
/* SRC: Preprocessor_Hash.php line 1471 */
Variant c_ppnode_hash_tree::t_splitheading() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::splitHeading);
  Variant v_bits;
  Variant v_child;

  if (!same(m_name, "h")) {
    throw_exception(LINE(1473,create_object("mwexception", Array(ArrayInit(1).set(0, "Invalid h node passed to PPNode_Hash_Tree::splitHeading").create()))));
  }
  (v_bits = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(6);
    for ((v_child = m_firstChild); toBoolean(v_child); (v_child = v_child.o_get("nextSibling", 0x0B3EC7643EE81822LL))) {
      LOOP_COUNTER_CHECK(6);
      {
        if (!(toObject(v_child)->t___isset("name"))) {
          continue;
        }
        if (equal(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "i")) {
          v_bits.set("i", (v_child.o_get("value", 0x69E7413AE0C88471LL)), 0x0EB22EDA95766E98LL);
        }
        else if (equal(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "level")) {
          v_bits.set("level", (v_child.o_get("value", 0x69E7413AE0C88471LL)), 0x26B910814038EBA3LL);
        }
      }
    }
  }
  if (!(isset(v_bits, "i", 0x0EB22EDA95766E98LL))) {
    throw_exception(LINE(1487,create_object("mwexception", Array(ArrayInit(1).set(0, "Invalid h node passed to PPNode_Hash_Tree::splitHeading").create()))));
  }
  return v_bits;
} /* function */
/* SRC: Preprocessor_Hash.php line 1495 */
Variant c_ppnode_hash_tree::t_splittemplate() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Tree, PPNode_Hash_Tree::splitTemplate);
  Array v_parts;
  Variant v_bits;
  Variant v_child;

  (v_parts = ScalarArrays::sa_[0]);
  (v_bits = ScalarArrays::sa_[13]);
  {
    LOOP_COUNTER(7);
    for ((v_child = m_firstChild); toBoolean(v_child); (v_child = v_child.o_get("nextSibling", 0x0B3EC7643EE81822LL))) {
      LOOP_COUNTER_CHECK(7);
      {
        if (!(toObject(v_child)->t___isset("name"))) {
          continue;
        }
        if (equal(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "title")) {
          v_bits.set("title", (v_child), 0x66AD900A2301E2FELL);
        }
        if (equal(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "part")) {
          v_parts.append((v_child));
        }
        if (equal(v_child.o_get("name", 0x0BCDB293DC3CBDDCLL), "lineStart")) {
          v_bits.set("lineStart", ("1"), 0x0F84CB175598D0A5LL);
        }
      }
    }
  }
  if (!(isset(v_bits, "title", 0x66AD900A2301E2FELL))) {
    throw_exception(LINE(1513,create_object("mwexception", Array(ArrayInit(1).set(0, "Invalid node passed to PPNode_Hash_Tree::splitTemplate").create()))));
  }
  v_bits.set("parts", (((Object)(LINE(1515,p_ppnode_hash_array(p_ppnode_hash_array(NEWOBJ(c_ppnode_hash_array)())->create(v_parts)))))), 0x2708FDA74562AD8DLL);
  return v_bits;
} /* function */
/* SRC: Preprocessor_Hash.php line 788 */
Variant c_ppframe_hash::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppframe_hash::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ppframe_hash::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("preprocessor", m_preprocessor.isReferenced() ? ref(m_preprocessor) : m_preprocessor));
  props.push_back(NEW(ArrayElement)("parser", m_parser.isReferenced() ? ref(m_parser) : m_parser));
  props.push_back(NEW(ArrayElement)("title", m_title.isReferenced() ? ref(m_title) : m_title));
  props.push_back(NEW(ArrayElement)("titleCache", m_titleCache.isReferenced() ? ref(m_titleCache) : m_titleCache));
  props.push_back(NEW(ArrayElement)("loopCheckHash", m_loopCheckHash.isReferenced() ? ref(m_loopCheckHash) : m_loopCheckHash));
  props.push_back(NEW(ArrayElement)("depth", m_depth.isReferenced() ? ref(m_depth) : m_depth));
  c_ObjectData::o_get(props);
}
bool c_ppframe_hash::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_EXISTS_STRING(0x18A8B9D71D4F2D02LL, parser, 6);
      break;
    case 3:
      HASH_EXISTS_STRING(0x0CD4D2916BE2CE13LL, titleCache, 10);
      break;
    case 5:
      HASH_EXISTS_STRING(0x757439BA20184735LL, loopCheckHash, 13);
      break;
    case 7:
      HASH_EXISTS_STRING(0x1AE700EECE6274C7LL, depth, 5);
      break;
    case 14:
      HASH_EXISTS_STRING(0x2940E17D1F5401AELL, preprocessor, 12);
      HASH_EXISTS_STRING(0x66AD900A2301E2FELL, title, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppframe_hash::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                         parser, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x0CD4D2916BE2CE13LL, m_titleCache,
                         titleCache, 10);
      break;
    case 5:
      HASH_RETURN_STRING(0x757439BA20184735LL, m_loopCheckHash,
                         loopCheckHash, 13);
      break;
    case 7:
      HASH_RETURN_STRING(0x1AE700EECE6274C7LL, m_depth,
                         depth, 5);
      break;
    case 14:
      HASH_RETURN_STRING(0x2940E17D1F5401AELL, m_preprocessor,
                         preprocessor, 12);
      HASH_RETURN_STRING(0x66AD900A2301E2FELL, m_title,
                         title, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppframe_hash::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_SET_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                      parser, 6);
      break;
    case 3:
      HASH_SET_STRING(0x0CD4D2916BE2CE13LL, m_titleCache,
                      titleCache, 10);
      break;
    case 5:
      HASH_SET_STRING(0x757439BA20184735LL, m_loopCheckHash,
                      loopCheckHash, 13);
      break;
    case 7:
      HASH_SET_STRING(0x1AE700EECE6274C7LL, m_depth,
                      depth, 5);
      break;
    case 14:
      HASH_SET_STRING(0x2940E17D1F5401AELL, m_preprocessor,
                      preprocessor, 12);
      HASH_SET_STRING(0x66AD900A2301E2FELL, m_title,
                      title, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppframe_hash::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                         parser, 6);
      break;
    case 3:
      HASH_RETURN_STRING(0x0CD4D2916BE2CE13LL, m_titleCache,
                         titleCache, 10);
      break;
    case 5:
      HASH_RETURN_STRING(0x757439BA20184735LL, m_loopCheckHash,
                         loopCheckHash, 13);
      break;
    case 7:
      HASH_RETURN_STRING(0x1AE700EECE6274C7LL, m_depth,
                         depth, 5);
      break;
    case 14:
      HASH_RETURN_STRING(0x2940E17D1F5401AELL, m_preprocessor,
                         preprocessor, 12);
      HASH_RETURN_STRING(0x66AD900A2301E2FELL, m_title,
                         title, 5);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppframe_hash::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppframe_hash)
ObjectData *c_ppframe_hash::create(Variant v_preprocessor) {
  init();
  t___construct(v_preprocessor);
  return this;
}
ObjectData *c_ppframe_hash::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_ppframe_hash::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_ppframe_hash::cloneImpl() {
  c_ppframe_hash *obj = NEW(c_ppframe_hash)();
  cloneSet(obj);
  return obj;
}
void c_ppframe_hash::cloneSet(c_ppframe_hash *clone) {
  clone->m_preprocessor = m_preprocessor.isReferenced() ? ref(m_preprocessor) : m_preprocessor;
  clone->m_parser = m_parser.isReferenced() ? ref(m_parser) : m_parser;
  clone->m_title = m_title.isReferenced() ? ref(m_title) : m_title;
  clone->m_titleCache = m_titleCache.isReferenced() ? ref(m_titleCache) : m_titleCache;
  clone->m_loopCheckHash = m_loopCheckHash.isReferenced() ? ref(m_loopCheckHash) : m_loopCheckHash;
  clone->m_depth = m_depth.isReferenced() ? ref(m_depth) : m_depth;
  ObjectData::cloneSet(clone);
}
Variant c_ppframe_hash::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        int count = params.size();
        if (count <= 3) return (t_virtualbracketedimplode(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        return (t_virtualbracketedimplode(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        int count = params.size();
        if (count <= 1) return (t_expand(params.rvalAt(0)));
        return (t_expand(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        int count = params.size();
        if (count <= 2) return (t_implodewithflags(count, params.rvalAt(0), params.rvalAt(1)));
        return (t_implodewithflags(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        int count = params.size();
        if (count <= 1) return (t_virtualimplode(count, params.rvalAt(0)));
        return (t_virtualimplode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        int count = params.size();
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(params.rvalAt(0)));
        return (t_newchild(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        int count = params.size();
        if (count <= 1) return (t_implode(count, params.rvalAt(0)));
        return (t_implode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(params.rvalAt(0)));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppframe_hash::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        if (count <= 3) return (t_virtualbracketedimplode(count, a0, a1, a2));
        Array params;
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualbracketedimplode(count,a0, a1, a2, params));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        if (count <= 1) return (t_expand(a0));
        return (t_expand(a0, a1));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        if (count <= 2) return (t_implodewithflags(count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implodewithflags(count,a0, a1, params));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        if (count <= 1) return (t_virtualimplode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualimplode(count,a0, params));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(a0));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(a0));
        return (t_newchild(a0, a1));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        if (count <= 1) return (t_implode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implode(count,a0, params));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(a0));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppframe_hash::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppframe_hash$os_get(const char *s) {
  return c_ppframe_hash::os_get(s, -1);
}
Variant &cw_ppframe_hash$os_lval(const char *s) {
  return c_ppframe_hash::os_lval(s, -1);
}
Variant cw_ppframe_hash$os_constant(const char *s) {
  return c_ppframe_hash::os_constant(s);
}
Variant cw_ppframe_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppframe_hash::os_invoke(c, s, params, -1, fatal);
}
void c_ppframe_hash::init() {
  m_preprocessor = null;
  m_parser = null;
  m_title = null;
  m_titleCache = null;
  m_loopCheckHash = null;
  m_depth = null;
}
/* SRC: Preprocessor_Hash.php line 809 */
void c_ppframe_hash::t___construct(Variant v_preprocessor) {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_preprocessor = v_preprocessor);
  (m_parser = v_preprocessor.o_get("parser", 0x18A8B9D71D4F2D02LL));
  (m_title = m_parser.o_get("mTitle", 0x4C804C46307BF0EALL));
  (m_titleCache = Array(ArrayInit(1).set(0, toBoolean(m_title) ? ((Variant)(LINE(813,m_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)))) : ((Variant)(false))).create()));
  (m_loopCheckHash = ScalarArrays::sa_[0]);
  (m_depth = 0LL);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 822 */
p_pptemplateframe_hash c_ppframe_hash::t_newchild(Variant v_args //  = false
, Variant v_title //  = false
) {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::newChild);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_namedArgs;
  Variant v_numberedArgs;
  Variant v_arg;
  Variant v_bits;
  String v_name;

  (v_namedArgs = ScalarArrays::sa_[0]);
  (v_numberedArgs = ScalarArrays::sa_[0]);
  if (same(v_title, false)) {
    (v_title = m_title);
  }
  if (!same(v_args, false)) {
    ;
    if (instanceOf(v_args, "PPNode_Hash_Array")) {
      (v_args = v_args.o_get("value", 0x69E7413AE0C88471LL));
    }
    else if (!(LINE(832,x_is_array(v_args)))) {
      throw_exception(LINE(833,create_object("mwexception", Array(ArrayInit(1).set(0, "PPFrame_Hash::newChild: $args must be array or PPNode_Hash_Array").create()))));
    }
    {
      LOOP_COUNTER(8);
      for (ArrayIterPtr iter10 = v_args.begin("ppframe_hash"); !iter10->end(); iter10->next()) {
        LOOP_COUNTER_CHECK(8);
        v_arg = iter10->second();
        {
          (v_bits = LINE(836,v_arg.o_invoke_few_args("splitArg", 0x73E7C3304C0C5360LL, 0)));
          if (!same(v_bits.rvalAt("index", 0x440D5888C0FF3081LL), "")) {
            v_numberedArgs.set(v_bits.rvalAt("index", 0x440D5888C0FF3081LL), (v_bits.rvalAt("value", 0x69E7413AE0C88471LL)));
            v_namedArgs.weakRemove(v_bits.rvalAt("index", 0x440D5888C0FF3081LL));
          }
          else {
            (v_name = LINE(843,x_trim(toString(o_root_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_bits.refvalAt("name", 0x0BCDB293DC3CBDDCLL), 4LL /* ppframe::STRIP_COMMENTS */)))));
            v_namedArgs.set(v_name, (v_bits.rvalAt("value", 0x69E7413AE0C88471LL)));
            v_numberedArgs.weakRemove(v_name);
          }
        }
      }
    }
  }
  return ((Object)(LINE(849,p_pptemplateframe_hash(p_pptemplateframe_hash(NEWOBJ(c_pptemplateframe_hash)())->create(m_preprocessor, ((Object)(this)), v_numberedArgs, v_namedArgs, v_title)))));
} /* function */
/* SRC: Preprocessor_Hash.php line 852 */
Variant c_ppframe_hash::t_expand(CVarRef v_root, Variant v_flags //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::expand);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_expansionDepth __attribute__((__unused__)) = g->sv_ppframe_hash$$expand$$expansionDepth.lvalAt(this->o_getClassName());
  Variant &inited_sv_expansionDepth __attribute__((__unused__)) = g->inited_sv_ppframe_hash$$expand$$expansionDepth.lvalAt(this->o_getClassName());
  Variant v_outStack;
  Variant v_iteratorStack;
  Variant v_indexStack;
  int64 v_level = 0;
  Variant v_iteratorNode;
  Variant v_out;
  Variant v_index;
  Variant v_contextNode;
  Variant v_newIterator;
  Variant v_bits;
  Variant v_ret;
  Variant v_s;
  Variant v_node;
  Variant v_titleText;
  int64 v_serial = 0;
  Variant v_marker;

  if (!inited_sv_expansionDepth) {
    (sv_expansionDepth = 0LL);
    inited_sv_expansionDepth = true;
  }
  if (LINE(854,x_is_string(v_root))) {
    return v_root;
  }
  if (more(++lval(m_parser.o_lval("mPPNodeCount", 0x17B591196693E481LL)), m_parser.o_get("mOptions", 0x79AC63B0E96DC6E9LL).o_get("mMaxPPNodeCount", 0x3DD526BE5013BD55LL))) {
    return "<span class=\"error\">Node-count limit exceeded</span>";
  }
  if (more(sv_expansionDepth, m_parser.o_get("mOptions", 0x79AC63B0E96DC6E9LL).o_get("mMaxPPExpandDepth", 0x2F1977A57AF7D6DELL))) {
    return "<span class=\"error\">Expansion depth limit exceeded</span>";
  }
  ++sv_expansionDepth;
  (v_outStack = ScalarArrays::sa_[9]);
  (v_iteratorStack = Array(ArrayInit(2).set(0, false).set(1, v_root).create()));
  (v_indexStack = ScalarArrays::sa_[10]);
  LOOP_COUNTER(11);
  {
    while (more(LINE(871,x_count(v_iteratorStack)), 1LL)) {
      LOOP_COUNTER_CHECK(11);
      {
        (v_level = LINE(872,x_count(v_outStack)) - 1LL);
        (v_iteratorNode = ref(lval(v_iteratorStack.lvalAt(v_level))));
        (v_out = ref(lval(v_outStack.lvalAt(v_level))));
        (v_index = ref(lval(v_indexStack.lvalAt(v_level))));
        if (LINE(877,x_is_array(v_iteratorNode))) {
          if (not_less(v_index, LINE(878,x_count(v_iteratorNode)))) {
            v_iteratorStack.set(v_level, (false));
            (v_contextNode = false);
          }
          else {
            (v_contextNode = v_iteratorNode.rvalAt(v_index));
            v_index++;
          }
        }
        else if (instanceOf(v_iteratorNode, "PPNode_Hash_Array")) {
          if (not_less(v_index, LINE(887,v_iteratorNode.o_invoke_few_args("getLength", 0x41EB078EF92C44D4LL, 0)))) {
            v_iteratorStack.set(v_level, (false));
            (v_contextNode = false);
          }
          else {
            (v_contextNode = LINE(892,v_iteratorNode.o_invoke_few_args("item", 0x0A41DBE0830902C6LL, 1, v_index)));
            v_index++;
          }
        }
        else {
          (v_contextNode = v_iteratorStack.rvalAt(v_level));
          v_iteratorStack.set(v_level, (false));
        }
        (v_newIterator = false);
        if (same(v_contextNode, false)) {
        }
        else if (LINE(906,x_is_string(v_contextNode))) {
          concat_assign(v_out, toString(v_contextNode));
        }
        else if (LINE(908,x_is_array(v_contextNode)) || instanceOf(v_contextNode, "PPNode_Hash_Array")) {
          (v_newIterator = v_contextNode);
        }
        else if (instanceOf(v_contextNode, "PPNode_Hash_Attr")) {
        }
        else if (instanceOf(v_contextNode, "PPNode_Hash_Text")) {
          concat_assign(v_out, toString(v_contextNode.o_get("value", 0x69E7413AE0C88471LL)));
        }
        else if (instanceOf(v_contextNode, "PPNode_Hash_Tree")) {
          if (equal(v_contextNode.o_get("name", 0x0BCDB293DC3CBDDCLL), "template")) {
            (v_bits = LINE(917,v_contextNode.o_invoke_few_args("splitTemplate", 0x5D6BF11378AA7D8DLL, 0)));
            if (toBoolean(bitwise_and(v_flags, throw_fatal("unknown class constant ppframe_hash::NO_TEMPLATES")))) {
              (v_newIterator = LINE(919,o_root_invoke_few_args("virtualBracketedImplode", 0x409618A98590618BLL, 5, "{{", "|", "}}", v_bits.refvalAt("title", 0x66AD900A2301E2FELL), v_bits.refvalAt("parts", 0x2708FDA74562AD8DLL))));
            }
            else {
              (v_ret = LINE(921,m_parser.o_invoke_few_args("braceSubstitution", 0x392E383BDD5B10F7LL, 2, v_bits, this)));
              if (isset(v_ret, "object", 0x7F30BC4E222B1861LL)) {
                (v_newIterator = v_ret.rvalAt("object", 0x7F30BC4E222B1861LL));
              }
              else {
                concat_assign(v_out, toString(v_ret.rvalAt("text", 0x2A28A0084DD3A743LL)));
              }
            }
          }
          else if (equal(v_contextNode.o_get("name", 0x0BCDB293DC3CBDDCLL), "tplarg")) {
            (v_bits = LINE(930,v_contextNode.o_invoke_few_args("splitTemplate", 0x5D6BF11378AA7D8DLL, 0)));
            if (toBoolean(bitwise_and(v_flags, throw_fatal("unknown class constant ppframe_hash::NO_ARGS")))) {
              (v_newIterator = LINE(932,o_root_invoke_few_args("virtualBracketedImplode", 0x409618A98590618BLL, 5, "{{{", "|", "}}}", v_bits.refvalAt("title", 0x66AD900A2301E2FELL), v_bits.refvalAt("parts", 0x2708FDA74562AD8DLL))));
            }
            else {
              (v_ret = LINE(934,m_parser.o_invoke_few_args("argSubstitution", 0x333DE406BD9436D1LL, 2, v_bits, this)));
              if (isset(v_ret, "object", 0x7F30BC4E222B1861LL)) {
                (v_newIterator = v_ret.rvalAt("object", 0x7F30BC4E222B1861LL));
              }
              else {
                concat_assign(v_out, toString(v_ret.rvalAt("text", 0x2A28A0084DD3A743LL)));
              }
            }
          }
          else if (equal(v_contextNode.o_get("name", 0x0BCDB293DC3CBDDCLL), "comment")) {
            if (toBoolean(m_parser.o_get("ot", 0x48663D83DD673D4DLL).rvalAt("html", 0x5CA91C812230855DLL)) || (toBoolean(m_parser.o_get("ot", 0x48663D83DD673D4DLL).rvalAt("pre", 0x633242EDD179FBACLL)) && toBoolean(LINE(945,m_parser.o_get("mOptions", 0x79AC63B0E96DC6E9LL).o_invoke_few_args("getRemoveComments", 0x4E73BF37061917D4LL, 0)))) || (toBoolean(bitwise_and(v_flags, throw_fatal("unknown class constant ppframe_hash::STRIP_COMMENTS"))))) {
              concat_assign(v_out, "");
            }
            else if (toBoolean(m_parser.o_get("ot", 0x48663D83DD673D4DLL).rvalAt("wiki", 0x24648D2BCD794D21LL)) && !((toBoolean(bitwise_and(v_flags, throw_fatal("unknown class constant ppframe_hash::RECOVER_COMMENTS")))))) {
              concat_assign(v_out, toString(LINE(953,m_parser.o_invoke_few_args("insertStripItem", 0x0A4CA37F020D70E2LL, 1, v_contextNode.o_get("firstChild", 0x22862CD26C0AAB50LL).o_lval("value", 0x69E7413AE0C88471LL)))));
            }
            else {
              concat_assign(v_out, toString(v_contextNode.o_get("firstChild", 0x22862CD26C0AAB50LL).o_get("value", 0x69E7413AE0C88471LL)));
            }
          }
          else if (equal(v_contextNode.o_get("name", 0x0BCDB293DC3CBDDCLL), "ignore")) {
            if ((!(t___isset("parent")) && toBoolean(m_parser.o_get("ot", 0x48663D83DD673D4DLL).rvalAt("wiki", 0x24648D2BCD794D21LL))) || (toBoolean(bitwise_and(v_flags, throw_fatal("unknown class constant ppframe_hash::NO_IGNORE"))))) {
              concat_assign(v_out, toString(v_contextNode.o_get("firstChild", 0x22862CD26C0AAB50LL).o_get("value", 0x69E7413AE0C88471LL)));
            }
          }
          else if (equal(v_contextNode.o_get("name", 0x0BCDB293DC3CBDDCLL), "ext")) {
            (v_bits = LINE(971,v_contextNode.o_invoke_few_args("splitExt", 0x47D4EE3CB3EB696CLL, 0)) + (Variant)(ScalarArrays::sa_[12]));
            concat_assign(v_out, toString(LINE(972,m_parser.o_invoke_few_args("extensionSubstitution", 0x7D371E851A4798E9LL, 2, v_bits, this))));
          }
          else if (equal(v_contextNode.o_get("name", 0x0BCDB293DC3CBDDCLL), "h")) {
            if (toBoolean(m_parser.o_get("ot", 0x48663D83DD673D4DLL).rvalAt("html", 0x5CA91C812230855DLL))) {
              (v_s = "");
              {
                LOOP_COUNTER(12);
                for ((v_node = v_contextNode.o_get("firstChild", 0x22862CD26C0AAB50LL)); toBoolean(v_node); (v_node = v_node.o_get("nextSibling", 0x0B3EC7643EE81822LL))) {
                  LOOP_COUNTER_CHECK(12);
                  {
                    concat_assign(v_s, toString(LINE(979,o_root_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_node, v_flags))));
                  }
                }
              }
              (v_bits = LINE(982,v_contextNode.o_invoke_few_args("splitHeading", 0x0621AE2D22A1922DLL, 0)));
              (v_titleText = LINE(983,m_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)));
              lval(m_parser.o_lval("mHeadings", 0x5EB895444FBC1E6BLL)).append((Array(ArrayInit(2).set(0, v_titleText).set(1, v_bits.rvalAt("i", 0x0EB22EDA95766E98LL)).create())));
              (v_serial = LINE(985,x_count(m_parser.o_get("mHeadings", 0x5EB895444FBC1E6BLL))) - 1LL);
              (v_marker = LINE(986,concat4(toString(m_parser.o_get("mUniqPrefix", 0x22D00FA247E8311DLL)), "-h-", toString(v_serial), "--QINU\177")));
              (v_s = concat_rev(toString(LINE(987,x_substr(toString(v_s), toInt32(v_bits.rvalAt("level", 0x26B910814038EBA3LL))))), concat(toString(x_substr(toString(v_s), toInt32(0LL), toInt32(v_bits.rvalAt("level", 0x26B910814038EBA3LL)))), toString(v_marker))));
              LINE(988,m_parser.o_get("mStripState", 0x7207EA9A5BBFBF69LL).o_get("general", 0x661B15A9C00F7127LL).o_invoke_few_args("setPair", 0x1CA93325C3BDD490LL, 2, v_marker, ""));
              concat_assign(v_out, toString(v_s));
            }
            else {
              (v_newIterator = LINE(992,v_contextNode.o_invoke_few_args("getChildren", 0x732EC1BDA8EC520FLL, 0)));
            }
          }
          else {
            (v_newIterator = LINE(996,v_contextNode.o_invoke_few_args("getChildren", 0x732EC1BDA8EC520FLL, 0)));
          }
        }
        else {
          throw_exception(LINE(999,create_object("mwexception", Array(ArrayInit(1).set(0, "PPFrame_Hash::expand: Invalid parameter type").create()))));
        }
        if (!same(v_newIterator, false)) {
          v_outStack.append((""));
          v_iteratorStack.append((v_newIterator));
          v_indexStack.append((0LL));
        }
        else if (same(v_iteratorStack.rvalAt(v_level), false)) {
          LOOP_COUNTER(13);
          {
            while (same(v_iteratorStack.rvalAt(v_level), false) && more(v_level, 0LL)) {
              LOOP_COUNTER_CHECK(13);
              {
                concat_assign(lval(v_outStack.lvalAt(v_level - 1LL)), toString(v_out));
                LINE(1011,x_array_pop(ref(v_outStack)));
                LINE(1012,x_array_pop(ref(v_iteratorStack)));
                LINE(1013,x_array_pop(ref(v_indexStack)));
                v_level--;
              }
            }
          }
        }
      }
    }
  }
  --sv_expansionDepth;
  return v_outStack.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
} /* function */
/* SRC: Preprocessor_Hash.php line 1022 */
Variant c_ppframe_hash::t_implodewithflags(int num_args, CVarRef v_sep, Variant v_flags, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::implodeWithFlags);
  Variant eo_0;
  Variant eo_1;
  Variant v_args;
  bool v_first = false;
  Variant v_s;
  Variant v_root;
  Variant v_node;

  (v_args = LINE(1023,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(2).set(0, v_sep).set(1, v_flags).create()),args)),x_array_slice(eo_0, toInt32(2LL)))));
  (v_first = true);
  (v_s = "");
  {
    LOOP_COUNTER(14);
    for (ArrayIterPtr iter16 = v_args.begin("ppframe_hash"); !iter16->end(); iter16->next()) {
      LOOP_COUNTER_CHECK(14);
      v_root = iter16->second();
      {
        if (instanceOf(v_root, "PPNode_Hash_Array")) {
          (v_root = v_root.o_get("value", 0x69E7413AE0C88471LL));
        }
        if (!(LINE(1031,x_is_array(v_root)))) {
          (v_root = Array(ArrayInit(1).set(0, v_root).create()));
        }
        {
          LOOP_COUNTER(17);
          for (ArrayIterPtr iter19 = v_root.begin("ppframe_hash"); !iter19->end(); iter19->next()) {
            LOOP_COUNTER_CHECK(17);
            v_node = iter19->second();
            {
              if (v_first) {
                (v_first = false);
              }
              else {
                concat_assign(v_s, toString(v_sep));
              }
              concat_assign(v_s, toString(LINE(1040,o_root_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_node, v_flags))));
            }
          }
        }
      }
    }
  }
  return v_s;
} /* function */
/* SRC: Preprocessor_Hash.php line 1050 */
Variant c_ppframe_hash::t_implode(int num_args, CVarRef v_sep, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::implode);
  Variant eo_0;
  Variant eo_1;
  Variant v_args;
  bool v_first = false;
  Variant v_s;
  Variant v_root;
  Variant v_node;

  (v_args = LINE(1051,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(1).set(0, v_sep).create()),args)),x_array_slice(eo_0, toInt32(1LL)))));
  (v_first = true);
  (v_s = "");
  {
    LOOP_COUNTER(20);
    for (ArrayIterPtr iter22 = v_args.begin("ppframe_hash"); !iter22->end(); iter22->next()) {
      LOOP_COUNTER_CHECK(20);
      v_root = iter22->second();
      {
        if (instanceOf(v_root, "PPNode_Hash_Array")) {
          (v_root = v_root.o_get("value", 0x69E7413AE0C88471LL));
        }
        if (!(LINE(1059,x_is_array(v_root)))) {
          (v_root = Array(ArrayInit(1).set(0, v_root).create()));
        }
        {
          LOOP_COUNTER(23);
          for (ArrayIterPtr iter25 = v_root.begin("ppframe_hash"); !iter25->end(); iter25->next()) {
            LOOP_COUNTER_CHECK(23);
            v_node = iter25->second();
            {
              if (v_first) {
                (v_first = false);
              }
              else {
                concat_assign(v_s, toString(v_sep));
              }
              concat_assign(v_s, toString(LINE(1068,o_root_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_node))));
            }
          }
        }
      }
    }
  }
  return v_s;
} /* function */
/* SRC: Preprocessor_Hash.php line 1078 */
p_ppnode_hash_array c_ppframe_hash::t_virtualimplode(int num_args, CVarRef v_sep, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::virtualImplode);
  Variant eo_0;
  Variant eo_1;
  Variant v_args;
  Array v_out;
  bool v_first = false;
  Variant v_root;
  Variant v_node;

  (v_args = LINE(1079,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(1).set(0, v_sep).create()),args)),x_array_slice(eo_0, toInt32(1LL)))));
  (v_out = ScalarArrays::sa_[0]);
  (v_first = true);
  {
    LOOP_COUNTER(26);
    for (ArrayIterPtr iter28 = v_args.begin("ppframe_hash"); !iter28->end(); iter28->next()) {
      LOOP_COUNTER_CHECK(26);
      v_root = iter28->second();
      {
        if (instanceOf(v_root, "PPNode_Hash_Array")) {
          (v_root = v_root.o_get("value", 0x69E7413AE0C88471LL));
        }
        if (!(LINE(1087,x_is_array(v_root)))) {
          (v_root = Array(ArrayInit(1).set(0, v_root).create()));
        }
        {
          LOOP_COUNTER(29);
          for (ArrayIterPtr iter31 = v_root.begin("ppframe_hash"); !iter31->end(); iter31->next()) {
            LOOP_COUNTER_CHECK(29);
            v_node = iter31->second();
            {
              if (v_first) {
                (v_first = false);
              }
              else {
                v_out.append((v_sep));
              }
              v_out.append((v_node));
            }
          }
        }
      }
    }
  }
  return ((Object)(LINE(1099,p_ppnode_hash_array(p_ppnode_hash_array(NEWOBJ(c_ppnode_hash_array)())->create(v_out)))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1105 */
p_ppnode_hash_array c_ppframe_hash::t_virtualbracketedimplode(int num_args, CVarRef v_start, CVarRef v_sep, CVarRef v_end, Array args /* = Array() */) {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::virtualBracketedImplode);
  Variant eo_0;
  Variant eo_1;
  Variant v_args;
  Array v_out;
  bool v_first = false;
  Variant v_root;
  Variant v_node;

  (v_args = LINE(1106,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(3).set(0, v_start).set(1, v_sep).set(2, v_end).create()),args)),x_array_slice(eo_0, toInt32(3LL)))));
  (v_out = Array(ArrayInit(1).set(0, v_start).create()));
  (v_first = true);
  {
    LOOP_COUNTER(32);
    for (ArrayIterPtr iter34 = v_args.begin("ppframe_hash"); !iter34->end(); iter34->next()) {
      LOOP_COUNTER_CHECK(32);
      v_root = iter34->second();
      {
        if (instanceOf(v_root, "PPNode_Hash_Array")) {
          (v_root = v_root.o_get("value", 0x69E7413AE0C88471LL));
        }
        if (!(LINE(1114,x_is_array(v_root)))) {
          (v_root = Array(ArrayInit(1).set(0, v_root).create()));
        }
        {
          LOOP_COUNTER(35);
          for (ArrayIterPtr iter37 = v_root.begin("ppframe_hash"); !iter37->end(); iter37->next()) {
            LOOP_COUNTER_CHECK(35);
            v_node = iter37->second();
            {
              if (v_first) {
                (v_first = false);
              }
              else {
                v_out.append((v_sep));
              }
              v_out.append((v_node));
            }
          }
        }
      }
    }
  }
  v_out.append((v_end));
  return ((Object)(LINE(1127,p_ppnode_hash_array(p_ppnode_hash_array(NEWOBJ(c_ppnode_hash_array)())->create(v_out)))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1130 */
String c_ppframe_hash::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::__toString);
  return "frame{}";
} /* function */
/* SRC: Preprocessor_Hash.php line 1134 */
Variant c_ppframe_hash::t_getpdbk(bool v_level //  = false
) {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::getPDBK);
  if (same(v_level, false)) {
    return LINE(1136,m_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0));
  }
  else {
    return isset(m_titleCache, v_level) ? ((Variant)(m_titleCache.rvalAt(v_level))) : ((Variant)(false));
  }
  return null;
} /* function */
/* SRC: Preprocessor_Hash.php line 1142 */
Array c_ppframe_hash::t_getarguments() {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::getArguments);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: Preprocessor_Hash.php line 1146 */
Array c_ppframe_hash::t_getnumberedarguments() {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::getNumberedArguments);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: Preprocessor_Hash.php line 1150 */
Array c_ppframe_hash::t_getnamedarguments() {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::getNamedArguments);
  return ScalarArrays::sa_[0];
} /* function */
/* SRC: Preprocessor_Hash.php line 1157 */
bool c_ppframe_hash::t_isempty() {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::isEmpty);
  return true;
} /* function */
/* SRC: Preprocessor_Hash.php line 1161 */
bool c_ppframe_hash::t_getargument(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::getArgument);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1168 */
bool c_ppframe_hash::t_loopcheck(CVarRef v_title) {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::loopCheck);
  return !(isset(m_loopCheckHash, LINE(1169,toObject(v_title)->o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1175 */
bool c_ppframe_hash::t_istemplate() {
  INSTANCE_METHOD_INJECTION(PPFrame_Hash, PPFrame_Hash::isTemplate);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 9 */
const int64 q_preprocessor_hash_CACHE_VERSION = 1LL;
Variant c_preprocessor_hash::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_preprocessor_hash::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_preprocessor_hash::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("parser", m_parser.isReferenced() ? ref(m_parser) : m_parser));
  c_ObjectData::o_get(props);
}
bool c_preprocessor_hash::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x18A8B9D71D4F2D02LL, parser, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_preprocessor_hash::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                         parser, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_preprocessor_hash::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                      parser, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_preprocessor_hash::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                         parser, 6);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_preprocessor_hash::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 1) {
    case 1:
      HASH_RETURN(0x6AD33BF0C16EEC43LL, q_preprocessor_hash_CACHE_VERSION, CACHE_VERSION);
      break;
    default:
      break;
  }
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(preprocessor_hash)
ObjectData *c_preprocessor_hash::create(Variant v_parser) {
  init();
  t___construct(v_parser);
  return this;
}
ObjectData *c_preprocessor_hash::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_preprocessor_hash::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_preprocessor_hash::cloneImpl() {
  c_preprocessor_hash *obj = NEW(c_preprocessor_hash)();
  cloneSet(obj);
  return obj;
}
void c_preprocessor_hash::cloneSet(c_preprocessor_hash *clone) {
  clone->m_parser = m_parser.isReferenced() ? ref(m_parser) : m_parser;
  ObjectData::cloneSet(clone);
}
Variant c_preprocessor_hash::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x5A11FE5B629AF7A0LL, newcustomframe) {
        return (t_newcustomframe(params.rvalAt(0)));
      }
      break;
    case 3:
      HASH_GUARD(0x0BA55A9913645813LL, preprocesstoobj) {
        int count = params.size();
        if (count <= 1) return (t_preprocesstoobj(params.rvalAt(0)));
        return (t_preprocesstoobj(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 5:
      HASH_GUARD(0x787B6141D7721675LL, newframe) {
        return (t_newframe());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_preprocessor_hash::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x5A11FE5B629AF7A0LL, newcustomframe) {
        return (t_newcustomframe(a0));
      }
      break;
    case 3:
      HASH_GUARD(0x0BA55A9913645813LL, preprocesstoobj) {
        if (count <= 1) return (t_preprocesstoobj(a0));
        return (t_preprocesstoobj(a0, a1));
      }
      break;
    case 5:
      HASH_GUARD(0x787B6141D7721675LL, newframe) {
        return (t_newframe());
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_preprocessor_hash::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_preprocessor_hash$os_get(const char *s) {
  return c_preprocessor_hash::os_get(s, -1);
}
Variant &cw_preprocessor_hash$os_lval(const char *s) {
  return c_preprocessor_hash::os_lval(s, -1);
}
Variant cw_preprocessor_hash$os_constant(const char *s) {
  return c_preprocessor_hash::os_constant(s);
}
Variant cw_preprocessor_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_preprocessor_hash::os_invoke(c, s, params, -1, fatal);
}
void c_preprocessor_hash::init() {
  m_parser = null;
}
/* SRC: Preprocessor_Hash.php line 14 */
void c_preprocessor_hash::t___construct(Variant v_parser) {
  INSTANCE_METHOD_INJECTION(Preprocessor_Hash, Preprocessor_Hash::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_parser = v_parser);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 18 */
p_ppframe_hash c_preprocessor_hash::t_newframe() {
  INSTANCE_METHOD_INJECTION(Preprocessor_Hash, Preprocessor_Hash::newFrame);
  return ((Object)(LINE(19,p_ppframe_hash(p_ppframe_hash(NEWOBJ(c_ppframe_hash)())->create(((Object)(this)))))));
} /* function */
/* SRC: Preprocessor_Hash.php line 22 */
p_ppcustomframe_hash c_preprocessor_hash::t_newcustomframe(CVarRef v_args) {
  INSTANCE_METHOD_INJECTION(Preprocessor_Hash, Preprocessor_Hash::newCustomFrame);
  return ((Object)(LINE(23,p_ppcustomframe_hash(p_ppcustomframe_hash(NEWOBJ(c_ppcustomframe_hash)())->create(((Object)(this)), v_args)))));
} /* function */
/* SRC: Preprocessor_Hash.php line 48 */
Variant c_preprocessor_hash::t_preprocesstoobj(Variant v_text, Variant v_flags //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(Preprocessor_Hash, Preprocessor_Hash::preprocessToObj);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgMemc __attribute__((__unused__)) = g->GV(wgMemc);
  Variant v_wgMemc;
  Variant &gv_wgPreprocessorCacheThreshold __attribute__((__unused__)) = g->GV(wgPreprocessorCacheThreshold);
  Variant v_wgPreprocessorCacheThreshold;
  Variant v_cacheable;
  Variant v_cacheKey;
  Variant v_cacheValue;
  Variant v_version;
  Variant v_hash;
  Variant v_rules;
  Variant v_forInclusion;
  Variant v_xmlishElements;
  Variant v_enableOnlyinclude;
  Variant v_ignoredTags;
  Variant v_ignoredElements;
  Variant v_xmlishRegex;
  Variant v_elementsRegex;
  Variant v_stack;
  Variant v_searchBase;
  Variant v_revText;
  Variant v_i;
  Variant v_accum;
  Variant v_findEquals;
  Variant v_findPipe;
  Variant v_headingIndex;
  Variant v_inHeading;
  Variant v_noMoreGT;
  Variant v_findOnlyinclude;
  Variant v_fakeLineStart;
  Variant v_startPos;
  Variant v_tagEndPos;
  Variant v_found;
  Variant v_curChar;
  Variant v_search;
  Variant v_currentClosing;
  Variant v_rule;
  Variant v_literalLength;
  Variant v_matches;
  Variant v_endPos;
  Variant v_inner;
  Variant v_wsStart;
  Variant v_wsEnd;
  Variant v_wsLength;
  Variant v_part;
  Variant v_name;
  Variant v_lowerName;
  Variant v_attrStart;
  Variant v_tagStartPos;
  Variant v_attrEnd;
  Variant v_close;
  Variant v_attr;
  Variant v_extNode;
  Variant v_count;
  Variant v_piece;
  Variant v_searchStart;
  Variant v_equalsLength;
  Variant v_element;
  Variant v_maxCount;
  Variant v_matchingCount;
  Variant v_parts;
  Variant v_titleAccum;
  Variant v_titleNode;
  Variant v_argIndex;
  Variant v_partIndex;
  Variant v_lastNode;
  Variant v_node;
  Variant v_equalsNode;
  Variant v_nameNode;
  Variant v_valueNode;
  Variant v_partNode;
  Variant v_names;
  Variant v_skippedBraces;
  Variant v_enclosingAccum;
  Variant v_rootNode;


  class VariableTable : public LVariableTable {
  public:
    Variant &v_text; Variant &v_flags; Variant &v_wgMemc; Variant &v_wgPreprocessorCacheThreshold; Variant &v_cacheable; Variant &v_cacheKey; Variant &v_cacheValue; Variant &v_version; Variant &v_hash; Variant &v_rules; Variant &v_forInclusion; Variant &v_xmlishElements; Variant &v_enableOnlyinclude; Variant &v_ignoredTags; Variant &v_ignoredElements; Variant &v_xmlishRegex; Variant &v_elementsRegex; Variant &v_stack; Variant &v_searchBase; Variant &v_revText; Variant &v_i; Variant &v_accum; Variant &v_findEquals; Variant &v_findPipe; Variant &v_headingIndex; Variant &v_inHeading; Variant &v_noMoreGT; Variant &v_findOnlyinclude; Variant &v_fakeLineStart; Variant &v_startPos; Variant &v_tagEndPos; Variant &v_found; Variant &v_curChar; Variant &v_search; Variant &v_currentClosing; Variant &v_rule; Variant &v_literalLength; Variant &v_matches; Variant &v_endPos; Variant &v_inner; Variant &v_wsStart; Variant &v_wsEnd; Variant &v_wsLength; Variant &v_part; Variant &v_name; Variant &v_lowerName; Variant &v_attrStart; Variant &v_tagStartPos; Variant &v_attrEnd; Variant &v_close; Variant &v_attr; Variant &v_extNode; Variant &v_count; Variant &v_piece; Variant &v_searchStart; Variant &v_equalsLength; Variant &v_element; Variant &v_maxCount; Variant &v_matchingCount; Variant &v_parts; Variant &v_titleAccum; Variant &v_titleNode; Variant &v_argIndex; Variant &v_partIndex; Variant &v_lastNode; Variant &v_node; Variant &v_equalsNode; Variant &v_nameNode; Variant &v_valueNode; Variant &v_partNode; Variant &v_names; Variant &v_skippedBraces; Variant &v_enclosingAccum; Variant &v_rootNode;
    VariableTable(Variant &r_text, Variant &r_flags, Variant &r_wgMemc, Variant &r_wgPreprocessorCacheThreshold, Variant &r_cacheable, Variant &r_cacheKey, Variant &r_cacheValue, Variant &r_version, Variant &r_hash, Variant &r_rules, Variant &r_forInclusion, Variant &r_xmlishElements, Variant &r_enableOnlyinclude, Variant &r_ignoredTags, Variant &r_ignoredElements, Variant &r_xmlishRegex, Variant &r_elementsRegex, Variant &r_stack, Variant &r_searchBase, Variant &r_revText, Variant &r_i, Variant &r_accum, Variant &r_findEquals, Variant &r_findPipe, Variant &r_headingIndex, Variant &r_inHeading, Variant &r_noMoreGT, Variant &r_findOnlyinclude, Variant &r_fakeLineStart, Variant &r_startPos, Variant &r_tagEndPos, Variant &r_found, Variant &r_curChar, Variant &r_search, Variant &r_currentClosing, Variant &r_rule, Variant &r_literalLength, Variant &r_matches, Variant &r_endPos, Variant &r_inner, Variant &r_wsStart, Variant &r_wsEnd, Variant &r_wsLength, Variant &r_part, Variant &r_name, Variant &r_lowerName, Variant &r_attrStart, Variant &r_tagStartPos, Variant &r_attrEnd, Variant &r_close, Variant &r_attr, Variant &r_extNode, Variant &r_count, Variant &r_piece, Variant &r_searchStart, Variant &r_equalsLength, Variant &r_element, Variant &r_maxCount, Variant &r_matchingCount, Variant &r_parts, Variant &r_titleAccum, Variant &r_titleNode, Variant &r_argIndex, Variant &r_partIndex, Variant &r_lastNode, Variant &r_node, Variant &r_equalsNode, Variant &r_nameNode, Variant &r_valueNode, Variant &r_partNode, Variant &r_names, Variant &r_skippedBraces, Variant &r_enclosingAccum, Variant &r_rootNode) : v_text(r_text), v_flags(r_flags), v_wgMemc(r_wgMemc), v_wgPreprocessorCacheThreshold(r_wgPreprocessorCacheThreshold), v_cacheable(r_cacheable), v_cacheKey(r_cacheKey), v_cacheValue(r_cacheValue), v_version(r_version), v_hash(r_hash), v_rules(r_rules), v_forInclusion(r_forInclusion), v_xmlishElements(r_xmlishElements), v_enableOnlyinclude(r_enableOnlyinclude), v_ignoredTags(r_ignoredTags), v_ignoredElements(r_ignoredElements), v_xmlishRegex(r_xmlishRegex), v_elementsRegex(r_elementsRegex), v_stack(r_stack), v_searchBase(r_searchBase), v_revText(r_revText), v_i(r_i), v_accum(r_accum), v_findEquals(r_findEquals), v_findPipe(r_findPipe), v_headingIndex(r_headingIndex), v_inHeading(r_inHeading), v_noMoreGT(r_noMoreGT), v_findOnlyinclude(r_findOnlyinclude), v_fakeLineStart(r_fakeLineStart), v_startPos(r_startPos), v_tagEndPos(r_tagEndPos), v_found(r_found), v_curChar(r_curChar), v_search(r_search), v_currentClosing(r_currentClosing), v_rule(r_rule), v_literalLength(r_literalLength), v_matches(r_matches), v_endPos(r_endPos), v_inner(r_inner), v_wsStart(r_wsStart), v_wsEnd(r_wsEnd), v_wsLength(r_wsLength), v_part(r_part), v_name(r_name), v_lowerName(r_lowerName), v_attrStart(r_attrStart), v_tagStartPos(r_tagStartPos), v_attrEnd(r_attrEnd), v_close(r_close), v_attr(r_attr), v_extNode(r_extNode), v_count(r_count), v_piece(r_piece), v_searchStart(r_searchStart), v_equalsLength(r_equalsLength), v_element(r_element), v_maxCount(r_maxCount), v_matchingCount(r_matchingCount), v_parts(r_parts), v_titleAccum(r_titleAccum), v_titleNode(r_titleNode), v_argIndex(r_argIndex), v_partIndex(r_partIndex), v_lastNode(r_lastNode), v_node(r_node), v_equalsNode(r_equalsNode), v_nameNode(r_nameNode), v_valueNode(r_valueNode), v_partNode(r_partNode), v_names(r_names), v_skippedBraces(r_skippedBraces), v_enclosingAccum(r_enclosingAccum), v_rootNode(r_rootNode) {}
    virtual Variant &getImpl(CStrRef str, int64 hash) {
      const char *s __attribute__((__unused__)) = str.data();
      if (hash < 0) hash = hash_string(s);
      switch (hash & 255) {
        case 1:
          HASH_RETURN(0x6B446EEF03FAE201LL, v_lastNode,
                      lastNode);
          break;
        case 12:
          HASH_RETURN(0x24DE1F43AE321C0CLL, v_cacheKey,
                      cacheKey);
          break;
        case 13:
          HASH_RETURN(0x6AFDA85728FAE70DLL, v_flags,
                      flags);
          break;
        case 17:
          HASH_RETURN(0x3C17C0A51918EB11LL, v_cacheValue,
                      cacheValue);
          break;
        case 29:
          HASH_RETURN(0x749C3FAB1E74371DLL, v_startPos,
                      startPos);
          break;
        case 36:
          HASH_RETURN(0x07D9889BE123B524LL, v_searchBase,
                      searchBase);
          break;
        case 37:
          HASH_RETURN(0x78D6949B972CFD25LL, v_rules,
                      rules);
          break;
        case 40:
          HASH_RETURN(0x4761A194B0333B28LL, v_accum,
                      accum);
          break;
        case 43:
          HASH_RETURN(0x5084E637B870262BLL, v_stack,
                      stack);
          HASH_RETURN(0x0577FC17B4E8F92BLL, v_titleNode,
                      titleNode);
          break;
        case 45:
          HASH_RETURN(0x73F30E0F2D277D2DLL, v_inHeading,
                      inHeading);
          break;
        case 46:
          HASH_RETURN(0x7F0BF9704771F92ELL, v_literalLength,
                      literalLength);
          break;
        case 48:
          HASH_RETURN(0x6FD137A54D1BE930LL, v_wsStart,
                      wsStart);
          break;
        case 49:
          HASH_RETURN(0x3CFB4B7DE74B4931LL, v_nameNode,
                      nameNode);
          break;
        case 50:
          HASH_RETURN(0x485CF5F18ACB2632LL, v_endPos,
                      endPos);
          break;
        case 58:
          HASH_RETURN(0x706B04F63B7B2C3ALL, v_titleAccum,
                      titleAccum);
          break;
        case 67:
          HASH_RETURN(0x2A28A0084DD3A743LL, v_text,
                      text);
          HASH_RETURN(0x79E99297D9CF6243LL, v_search,
                      search);
          break;
        case 74:
          HASH_RETURN(0x120827A34A63694ALL, v_valueNode,
                      valueNode);
          break;
        case 75:
          HASH_RETURN(0x4D0B41A665B57E4BLL, v_searchStart,
                      searchStart);
          break;
        case 80:
          HASH_RETURN(0x30E66D49BBCCAD50LL, v_wgMemc,
                      wgMemc);
          break;
        case 81:
          HASH_RETURN(0x4436B8A58BF97C51LL, v_hash,
                      hash);
          break;
        case 86:
          HASH_RETURN(0x2C356137E4207156LL, v_tagStartPos,
                      tagStartPos);
          break;
        case 93:
          HASH_RETURN(0x64311A2C8443755DLL, v_attr,
                      attr);
          break;
        case 101:
          HASH_RETURN(0x0AE2F3887B84C665LL, v_tagEndPos,
                      tagEndPos);
          break;
        case 104:
          HASH_RETURN(0x5D2342E7226E4E68LL, v_skippedBraces,
                      skippedBraces);
          HASH_RETURN(0x6E17E55C72C97768LL, v_rootNode,
                      rootNode);
          break;
        case 105:
          HASH_RETURN(0x754D53FDE12A4569LL, v_equalsNode,
                      equalsNode);
          break;
        case 107:
          HASH_RETURN(0x36B43082F052EC6BLL, v_close,
                      close);
          break;
        case 108:
          HASH_RETURN(0x0FCC00A0960D4D6CLL, v_xmlishRegex,
                      xmlishRegex);
          break;
        case 117:
          HASH_RETURN(0x217F5FC11124CC75LL, v_wsLength,
                      wsLength);
          break;
        case 119:
          HASH_RETURN(0x5C804792579E2477LL, v_forInclusion,
                      forInclusion);
          break;
        case 121:
          HASH_RETURN(0x75E78845AE202B79LL, v_enclosingAccum,
                      enclosingAccum);
          break;
        case 125:
          HASH_RETURN(0x3179991995176C7DLL, v_partIndex,
                      partIndex);
          break;
        case 127:
          HASH_RETURN(0x41E37259D0CE717FLL, v_equalsLength,
                      equalsLength);
          break;
        case 128:
          HASH_RETURN(0x2FA2EA3BE4385180LL, v_ignoredElements,
                      ignoredElements);
          break;
        case 134:
          HASH_RETURN(0x1B3A1B4FC8E2BF86LL, v_findEquals,
                      findEquals);
          break;
        case 137:
          HASH_RETURN(0x1AFD3E7207DEC289LL, v_element,
                      element);
          break;
        case 141:
          HASH_RETURN(0x2708FDA74562AD8DLL, v_parts,
                      parts);
          break;
        case 144:
          HASH_RETURN(0x7A62DFE604197490LL, v_rule,
                      rule);
          break;
        case 152:
          HASH_RETURN(0x0EB22EDA95766E98LL, v_i,
                      i);
          break;
        case 155:
          HASH_RETURN(0x37961753510B769BLL, v_partNode,
                      partNode);
          break;
        case 157:
          HASH_RETURN(0x58F21C5E482FEE9DLL, v_extNode,
                      extNode);
          break;
        case 158:
          HASH_RETURN(0x1D7680FEEF32A39ELL, v_attrStart,
                      attrStart);
          HASH_RETURN(0x3ABE2A7FF197E79ELL, v_maxCount,
                      maxCount);
          break;
        case 162:
          HASH_RETURN(0x7985846879851FA2LL, v_revText,
                      revText);
          HASH_RETURN(0x6F571EAFB109D7A2LL, v_headingIndex,
                      headingIndex);
          break;
        case 169:
          HASH_RETURN(0x2DD17595E81D9BA9LL, v_xmlishElements,
                      xmlishElements);
          break;
        case 171:
          HASH_RETURN(0x4243E2C0D99B5EABLL, v_wgPreprocessorCacheThreshold,
                      wgPreprocessorCacheThreshold);
          break;
        case 174:
          HASH_RETURN(0x75716BF7E20AFAAELL, v_matchingCount,
                      matchingCount);
          break;
        case 175:
          HASH_RETURN(0x7451350C607F1BAFLL, v_noMoreGT,
                      noMoreGT);
          break;
        case 180:
          HASH_RETURN(0x2AF5F0847CD91DB4LL, v_version,
                      version);
          break;
        case 185:
          HASH_RETURN(0x61CFEEED54DD0AB9LL, v_matches,
                      matches);
          break;
        case 187:
          HASH_RETURN(0x3D66B5980D54BABBLL, v_count,
                      count);
          break;
        case 190:
          HASH_RETURN(0x4B89E9FF5DB696BELL, v_currentClosing,
                      currentClosing);
          break;
        case 193:
          HASH_RETURN(0x75AF6DCF22783AC1LL, v_node,
                      node);
          break;
        case 206:
          HASH_RETURN(0x41CC31743A0270CELL, v_found,
                      found);
          HASH_RETURN(0x5AE6844A0CCFE9CELL, v_attrEnd,
                      attrEnd);
          HASH_RETURN(0x0B812E4C8D98FBCELL, v_argIndex,
                      argIndex);
          break;
        case 209:
          HASH_RETURN(0x64DB35AFE8D7B1D1LL, v_cacheable,
                      cacheable);
          break;
        case 217:
          HASH_RETURN(0x3FB915AA8F8F8ED9LL, v_fakeLineStart,
                      fakeLineStart);
          break;
        case 219:
          HASH_RETURN(0x66866B45F7FF96DBLL, v_wsEnd,
                      wsEnd);
          HASH_RETURN(0x7132B63767EAFEDBLL, v_piece,
                      piece);
          break;
        case 220:
          HASH_RETURN(0x0BCDB293DC3CBDDCLL, v_name,
                      name);
          break;
        case 227:
          HASH_RETURN(0x40B8ADAF2E1FB7E3LL, v_elementsRegex,
                      elementsRegex);
          break;
        case 229:
          HASH_RETURN(0x7FE16A3727F18DE5LL, v_findOnlyinclude,
                      findOnlyinclude);
          break;
        case 236:
          HASH_RETURN(0x5DBFA1F0A183E9ECLL, v_curChar,
                      curChar);
          HASH_RETURN(0x7F144219D39BF1ECLL, v_part,
                      part);
          break;
        case 237:
          HASH_RETURN(0x669278D2AC3068EDLL, v_ignoredTags,
                      ignoredTags);
          break;
        case 242:
          HASH_RETURN(0x51A687446C1579F2LL, v_names,
                      names);
          break;
        case 248:
          HASH_RETURN(0x77F20E4209A12DF8LL, v_enableOnlyinclude,
                      enableOnlyinclude);
          break;
        case 249:
          HASH_RETURN(0x5AE3699687C882F9LL, v_findPipe,
                      findPipe);
          break;
        case 252:
          HASH_RETURN(0x35537723FE7928FCLL, v_lowerName,
                      lowerName);
          break;
        case 254:
          HASH_RETURN(0x4F20D07BB803C1FELL, v_inner,
                      inner);
          break;
        default:
          break;
      }
      return lvalAt(str, hash);
    }
    virtual bool exists(const char *s, int64 hash /* = -1 */) const {
      if (hash < 0) hash = hash_string(s);
      switch (hash & 255) {
        case 1:
          HASH_INITIALIZED(0x6B446EEF03FAE201LL, v_lastNode,
                           lastNode);
          break;
        case 12:
          HASH_INITIALIZED(0x24DE1F43AE321C0CLL, v_cacheKey,
                           cacheKey);
          break;
        case 13:
          HASH_INITIALIZED(0x6AFDA85728FAE70DLL, v_flags,
                           flags);
          break;
        case 17:
          HASH_INITIALIZED(0x3C17C0A51918EB11LL, v_cacheValue,
                           cacheValue);
          break;
        case 29:
          HASH_INITIALIZED(0x749C3FAB1E74371DLL, v_startPos,
                           startPos);
          break;
        case 36:
          HASH_INITIALIZED(0x07D9889BE123B524LL, v_searchBase,
                           searchBase);
          break;
        case 37:
          HASH_INITIALIZED(0x78D6949B972CFD25LL, v_rules,
                           rules);
          break;
        case 40:
          HASH_INITIALIZED(0x4761A194B0333B28LL, v_accum,
                           accum);
          break;
        case 43:
          HASH_INITIALIZED(0x5084E637B870262BLL, v_stack,
                           stack);
          HASH_INITIALIZED(0x0577FC17B4E8F92BLL, v_titleNode,
                           titleNode);
          break;
        case 45:
          HASH_INITIALIZED(0x73F30E0F2D277D2DLL, v_inHeading,
                           inHeading);
          break;
        case 46:
          HASH_INITIALIZED(0x7F0BF9704771F92ELL, v_literalLength,
                           literalLength);
          break;
        case 48:
          HASH_INITIALIZED(0x6FD137A54D1BE930LL, v_wsStart,
                           wsStart);
          break;
        case 49:
          HASH_INITIALIZED(0x3CFB4B7DE74B4931LL, v_nameNode,
                           nameNode);
          break;
        case 50:
          HASH_INITIALIZED(0x485CF5F18ACB2632LL, v_endPos,
                           endPos);
          break;
        case 58:
          HASH_INITIALIZED(0x706B04F63B7B2C3ALL, v_titleAccum,
                           titleAccum);
          break;
        case 67:
          HASH_INITIALIZED(0x2A28A0084DD3A743LL, v_text,
                           text);
          HASH_INITIALIZED(0x79E99297D9CF6243LL, v_search,
                           search);
          break;
        case 74:
          HASH_INITIALIZED(0x120827A34A63694ALL, v_valueNode,
                           valueNode);
          break;
        case 75:
          HASH_INITIALIZED(0x4D0B41A665B57E4BLL, v_searchStart,
                           searchStart);
          break;
        case 80:
          HASH_INITIALIZED(0x30E66D49BBCCAD50LL, v_wgMemc,
                           wgMemc);
          break;
        case 81:
          HASH_INITIALIZED(0x4436B8A58BF97C51LL, v_hash,
                           hash);
          break;
        case 86:
          HASH_INITIALIZED(0x2C356137E4207156LL, v_tagStartPos,
                           tagStartPos);
          break;
        case 93:
          HASH_INITIALIZED(0x64311A2C8443755DLL, v_attr,
                           attr);
          break;
        case 101:
          HASH_INITIALIZED(0x0AE2F3887B84C665LL, v_tagEndPos,
                           tagEndPos);
          break;
        case 104:
          HASH_INITIALIZED(0x5D2342E7226E4E68LL, v_skippedBraces,
                           skippedBraces);
          HASH_INITIALIZED(0x6E17E55C72C97768LL, v_rootNode,
                           rootNode);
          break;
        case 105:
          HASH_INITIALIZED(0x754D53FDE12A4569LL, v_equalsNode,
                           equalsNode);
          break;
        case 107:
          HASH_INITIALIZED(0x36B43082F052EC6BLL, v_close,
                           close);
          break;
        case 108:
          HASH_INITIALIZED(0x0FCC00A0960D4D6CLL, v_xmlishRegex,
                           xmlishRegex);
          break;
        case 117:
          HASH_INITIALIZED(0x217F5FC11124CC75LL, v_wsLength,
                           wsLength);
          break;
        case 119:
          HASH_INITIALIZED(0x5C804792579E2477LL, v_forInclusion,
                           forInclusion);
          break;
        case 121:
          HASH_INITIALIZED(0x75E78845AE202B79LL, v_enclosingAccum,
                           enclosingAccum);
          break;
        case 125:
          HASH_INITIALIZED(0x3179991995176C7DLL, v_partIndex,
                           partIndex);
          break;
        case 127:
          HASH_INITIALIZED(0x41E37259D0CE717FLL, v_equalsLength,
                           equalsLength);
          break;
        case 128:
          HASH_INITIALIZED(0x2FA2EA3BE4385180LL, v_ignoredElements,
                           ignoredElements);
          break;
        case 134:
          HASH_INITIALIZED(0x1B3A1B4FC8E2BF86LL, v_findEquals,
                           findEquals);
          break;
        case 137:
          HASH_INITIALIZED(0x1AFD3E7207DEC289LL, v_element,
                           element);
          break;
        case 141:
          HASH_INITIALIZED(0x2708FDA74562AD8DLL, v_parts,
                           parts);
          break;
        case 144:
          HASH_INITIALIZED(0x7A62DFE604197490LL, v_rule,
                           rule);
          break;
        case 152:
          HASH_INITIALIZED(0x0EB22EDA95766E98LL, v_i,
                           i);
          break;
        case 155:
          HASH_INITIALIZED(0x37961753510B769BLL, v_partNode,
                           partNode);
          break;
        case 157:
          HASH_INITIALIZED(0x58F21C5E482FEE9DLL, v_extNode,
                           extNode);
          break;
        case 158:
          HASH_INITIALIZED(0x1D7680FEEF32A39ELL, v_attrStart,
                           attrStart);
          HASH_INITIALIZED(0x3ABE2A7FF197E79ELL, v_maxCount,
                           maxCount);
          break;
        case 162:
          HASH_INITIALIZED(0x7985846879851FA2LL, v_revText,
                           revText);
          HASH_INITIALIZED(0x6F571EAFB109D7A2LL, v_headingIndex,
                           headingIndex);
          break;
        case 169:
          HASH_INITIALIZED(0x2DD17595E81D9BA9LL, v_xmlishElements,
                           xmlishElements);
          break;
        case 171:
          HASH_INITIALIZED(0x4243E2C0D99B5EABLL, v_wgPreprocessorCacheThreshold,
                           wgPreprocessorCacheThreshold);
          break;
        case 174:
          HASH_INITIALIZED(0x75716BF7E20AFAAELL, v_matchingCount,
                           matchingCount);
          break;
        case 175:
          HASH_INITIALIZED(0x7451350C607F1BAFLL, v_noMoreGT,
                           noMoreGT);
          break;
        case 180:
          HASH_INITIALIZED(0x2AF5F0847CD91DB4LL, v_version,
                           version);
          break;
        case 185:
          HASH_INITIALIZED(0x61CFEEED54DD0AB9LL, v_matches,
                           matches);
          break;
        case 187:
          HASH_INITIALIZED(0x3D66B5980D54BABBLL, v_count,
                           count);
          break;
        case 190:
          HASH_INITIALIZED(0x4B89E9FF5DB696BELL, v_currentClosing,
                           currentClosing);
          break;
        case 193:
          HASH_INITIALIZED(0x75AF6DCF22783AC1LL, v_node,
                           node);
          break;
        case 206:
          HASH_INITIALIZED(0x41CC31743A0270CELL, v_found,
                           found);
          HASH_INITIALIZED(0x5AE6844A0CCFE9CELL, v_attrEnd,
                           attrEnd);
          HASH_INITIALIZED(0x0B812E4C8D98FBCELL, v_argIndex,
                           argIndex);
          break;
        case 209:
          HASH_INITIALIZED(0x64DB35AFE8D7B1D1LL, v_cacheable,
                           cacheable);
          break;
        case 217:
          HASH_INITIALIZED(0x3FB915AA8F8F8ED9LL, v_fakeLineStart,
                           fakeLineStart);
          break;
        case 219:
          HASH_INITIALIZED(0x66866B45F7FF96DBLL, v_wsEnd,
                           wsEnd);
          HASH_INITIALIZED(0x7132B63767EAFEDBLL, v_piece,
                           piece);
          break;
        case 220:
          HASH_INITIALIZED(0x0BCDB293DC3CBDDCLL, v_name,
                           name);
          break;
        case 227:
          HASH_INITIALIZED(0x40B8ADAF2E1FB7E3LL, v_elementsRegex,
                           elementsRegex);
          break;
        case 229:
          HASH_INITIALIZED(0x7FE16A3727F18DE5LL, v_findOnlyinclude,
                           findOnlyinclude);
          break;
        case 236:
          HASH_INITIALIZED(0x5DBFA1F0A183E9ECLL, v_curChar,
                           curChar);
          HASH_INITIALIZED(0x7F144219D39BF1ECLL, v_part,
                           part);
          break;
        case 237:
          HASH_INITIALIZED(0x669278D2AC3068EDLL, v_ignoredTags,
                           ignoredTags);
          break;
        case 242:
          HASH_INITIALIZED(0x51A687446C1579F2LL, v_names,
                           names);
          break;
        case 248:
          HASH_INITIALIZED(0x77F20E4209A12DF8LL, v_enableOnlyinclude,
                           enableOnlyinclude);
          break;
        case 249:
          HASH_INITIALIZED(0x5AE3699687C882F9LL, v_findPipe,
                           findPipe);
          break;
        case 252:
          HASH_INITIALIZED(0x35537723FE7928FCLL, v_lowerName,
                           lowerName);
          break;
        case 254:
          HASH_INITIALIZED(0x4F20D07BB803C1FELL, v_inner,
                           inner);
          break;
        default:
          break;
      }
      return LVariableTable::exists(s, hash);
    }
  } variableTable(v_text, v_flags, v_wgMemc, v_wgPreprocessorCacheThreshold, v_cacheable, v_cacheKey, v_cacheValue, v_version, v_hash, v_rules, v_forInclusion, v_xmlishElements, v_enableOnlyinclude, v_ignoredTags, v_ignoredElements, v_xmlishRegex, v_elementsRegex, v_stack, v_searchBase, v_revText, v_i, v_accum, v_findEquals, v_findPipe, v_headingIndex, v_inHeading, v_noMoreGT, v_findOnlyinclude, v_fakeLineStart, v_startPos, v_tagEndPos, v_found, v_curChar, v_search, v_currentClosing, v_rule, v_literalLength, v_matches, v_endPos, v_inner, v_wsStart, v_wsEnd, v_wsLength, v_part, v_name, v_lowerName, v_attrStart, v_tagStartPos, v_attrEnd, v_close, v_attr, v_extNode, v_count, v_piece, v_searchStart, v_equalsLength, v_element, v_maxCount, v_matchingCount, v_parts, v_titleAccum, v_titleNode, v_argIndex, v_partIndex, v_lastNode, v_node, v_equalsNode, v_nameNode, v_valueNode, v_partNode, v_names, v_skippedBraces, v_enclosingAccum, v_rootNode);
  LVariableTable* __attribute__((__unused__)) variables = &variableTable;
  LINE(49,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Preprocessor_Hash::preprocessToObj").create()), 0x0000000075359BAFLL));
  {
    v_wgMemc = ref(g->GV(wgMemc));
    v_wgPreprocessorCacheThreshold = ref(g->GV(wgPreprocessorCacheThreshold));
  }
  (v_cacheable = more(LINE(55,x_strlen(toString(v_text))), v_wgPreprocessorCacheThreshold));
  if (toBoolean(v_cacheable)) {
    LINE(57,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Preprocessor_Hash::preprocessToObj-cacheable").create()), 0x0000000075359BAFLL));
    (v_cacheKey = LINE(59,(assignCallTemp(eo_1, ref(x_md5(toString(v_text)))),assignCallTemp(eo_2, ref(v_flags)),invoke_failed("wfmemckey", Array(ArrayInit(3).set(0, "preprocess-hash").set(1, eo_1).set(2, eo_2).create()), 0x0000000072AF623FLL))));
    (v_cacheValue = LINE(60,v_wgMemc.o_invoke_few_args("get", 0x25DCCC35D69AD828LL, 1, v_cacheKey)));
    if (toBoolean(v_cacheValue)) {
      (v_version = LINE(62,x_substr(toString(v_cacheValue), toInt32(0LL), toInt32(8LL))));
      if (equal(LINE(63,x_intval(v_version)), 1LL /* preprocessor_hash::CACHE_VERSION */)) {
        (v_hash = LINE(64,x_unserialize(toString(x_substr(toString(v_cacheValue), toInt32(8LL))))));
        LINE(67,(assignCallTemp(eo_1, concat3("Loaded preprocessor hash from memcached (key ", toString(v_cacheKey), ")")),invoke_failed("wfdebuglog", Array(ArrayInit(2).set(0, "Preprocessor").set(1, eo_1).create()), 0x00000000E6942489LL)));
        LINE(68,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Preprocessor_Hash::preprocessToObj-cacheable").create()), 0x00000000B599F276LL));
        LINE(69,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Preprocessor_Hash::preprocessToObj").create()), 0x00000000B599F276LL));
        return v_hash;
      }
    }
    LINE(73,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Preprocessor_Hash::preprocessToObj-cache-miss").create()), 0x0000000075359BAFLL));
  }
  (v_rules = ScalarArrays::sa_[3]);
  (v_forInclusion = bitwise_and(v_flags, 1LL /* parser::PTD_FOR_INCLUSION */));
  (v_xmlishElements = LINE(96,m_parser.o_invoke_few_args("getStripList", 0x151B6C9D84BBC1F5LL, 0)));
  (v_enableOnlyinclude = false);
  if (toBoolean(v_forInclusion)) {
    (v_ignoredTags = ScalarArrays::sa_[4]);
    (v_ignoredElements = ScalarArrays::sa_[5]);
    v_xmlishElements.append(("noinclude"));
    if (!same(LINE(102,x_strpos(toString(v_text), "<onlyinclude>")), false) && !same(x_strpos(toString(v_text), "</onlyinclude>"), false)) {
      (v_enableOnlyinclude = true);
    }
  }
  else {
    (v_ignoredTags = ScalarArrays::sa_[6]);
    (v_ignoredElements = ScalarArrays::sa_[7]);
    v_xmlishElements.append(("includeonly"));
  }
  (v_xmlishRegex = LINE(110,(assignCallTemp(eo_1, x_array_merge(2, v_xmlishElements, Array(ArrayInit(1).set(0, v_ignoredTags).create()))),x_implode("|", eo_1))));
  (v_elementsRegex = LINE(113,concat3("~(", toString(v_xmlishRegex), ")(\?:\\s|\\/>|>)|(!--)~iA")));
  (v_stack = ((Object)(LINE(115,p_ppdstack_hash(p_ppdstack_hash(NEWOBJ(c_ppdstack_hash)())->create())))));
  (v_searchBase = "[{<\n");
  (v_revText = LINE(118,x_strrev(toString(v_text))));
  (v_i = 0LL);
  (v_accum = ref(LINE(121,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
  (v_findEquals = false);
  (v_findPipe = false);
  (v_headingIndex = 1LL);
  (v_inHeading = false);
  (v_noMoreGT = false);
  (v_findOnlyinclude = v_enableOnlyinclude);
  (v_fakeLineStart = true);
  LOOP_COUNTER(38);
  {
    while (true) {
      LOOP_COUNTER_CHECK(38);
      {
        if (toBoolean(v_findOnlyinclude)) {
          (v_startPos = LINE(135,x_strpos(toString(v_text), "<onlyinclude>", toInt32(v_i))));
          if (same(v_startPos, false)) {
            (assignCallTemp(eo_1, ref(LINE(138,x_substr(toString(v_text), toInt32(v_i))))),v_accum.o_invoke("addNodeWithText", Array(ArrayInit(2).set(0, "ignore").set(1, eo_1).create()), 0x6992F0F690B37356LL));
            break;
          }
          (v_tagEndPos = v_startPos + LINE(141,x_strlen("<onlyinclude>")));
          (assignCallTemp(eo_1, ref(LINE(142,x_substr(toString(v_text), toInt32(v_i), toInt32(v_tagEndPos - v_i))))),v_accum.o_invoke("addNodeWithText", Array(ArrayInit(2).set(0, "ignore").set(1, eo_1).create()), 0x6992F0F690B37356LL));
          (v_i = v_tagEndPos);
          (v_findOnlyinclude = false);
        }
        if (toBoolean(v_fakeLineStart)) {
          (v_found = "line-start");
          (v_curChar = "");
        }
        else {
          (v_search = v_searchBase);
          if (same(v_stack.o_get("top", 0x1854FD73A00D89E8LL), false)) {
            (v_currentClosing = "");
          }
          else {
            (v_currentClosing = v_stack.o_get("top", 0x1854FD73A00D89E8LL).o_get("close", 0x36B43082F052EC6BLL));
            concat_assign(v_search, toString(v_currentClosing));
          }
          if (toBoolean(v_findPipe)) {
            concat_assign(v_search, "|");
          }
          if (toBoolean(v_findEquals)) {
            concat_assign(v_search, "=");
          }
          setNull(v_rule);
          (v_literalLength = LINE(168,x_strcspn(toString(v_text), toString(v_search), toInt32(v_i))));
          if (more(v_literalLength, 0LL)) {
            LINE(170,v_accum.o_invoke_few_args("addLiteral", 0x05AAC40C7DAD4ACELL, 1, x_substr(toString(v_text), toInt32(v_i), toInt32(v_literalLength))));
            v_i += v_literalLength;
          }
          if (not_less(v_i, LINE(173,x_strlen(toString(v_text))))) {
            if (equal(v_currentClosing, "\n")) {
              (v_curChar = "");
              (v_found = "line-end");
            }
            else {
              break;
            }
          }
          else {
            (v_curChar = v_text.rvalAt(v_i));
            if (equal(v_curChar, "|")) {
              (v_found = "pipe");
            }
            else if (equal(v_curChar, "=")) {
              (v_found = "equals");
            }
            else if (equal(v_curChar, "<")) {
              (v_found = "angle");
            }
            else if (equal(v_curChar, "\n")) {
              if (toBoolean(v_inHeading)) {
                (v_found = "line-end");
              }
              else {
                (v_found = "line-start");
              }
            }
            else if (equal(v_curChar, v_currentClosing)) {
              (v_found = "close");
            }
            else if (isset(v_rules, v_curChar)) {
              (v_found = "open");
              (v_rule = v_rules.rvalAt(v_curChar));
            }
            else {
              ++v_i;
              continue;
            }
          }
        }
        if (equal(v_found, "angle")) {
          (v_matches = false);
          if (toBoolean(v_enableOnlyinclude) && equal(LINE(213,(assignCallTemp(eo_0, toString(v_text)),assignCallTemp(eo_1, toInt32(v_i)),assignCallTemp(eo_2, x_strlen("</onlyinclude>")),x_substr(eo_0, eo_1, eo_2))), "</onlyinclude>")) {
            (v_findOnlyinclude = true);
            continue;
          }
          if (!(toBoolean(LINE(219,x_preg_match(toString(v_elementsRegex), toString(v_text), ref(v_matches), toInt32(0LL), toInt32(v_i + 1LL)))))) {
            LINE(221,v_accum.o_invoke_few_args("addLiteral", 0x05AAC40C7DAD4ACELL, 1, "<"));
            ++v_i;
            continue;
          }
          if (isset(v_matches, 2LL, 0x486AFCC090D5F98CLL) && equal(v_matches.rvalAt(2LL, 0x486AFCC090D5F98CLL), "!--")) {
            (v_endPos = LINE(232,x_strpos(toString(v_text), "-->", toInt32(v_i + 4LL))));
            if (same(v_endPos, false)) {
              (v_inner = LINE(235,x_substr(toString(v_text), toInt32(v_i))));
              LINE(236,v_accum.o_invoke_few_args("addNodeWithText", 0x6992F0F690B37356LL, 2, "comment", v_inner));
              (v_i = LINE(237,x_strlen(toString(v_text))));
            }
            else {
              (v_wsStart = toBoolean(v_i) ? ((Variant)((v_i - LINE(240,(assignCallTemp(eo_0, toString(v_revText)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_text)) - v_i)),x_strspn(eo_0, " ", eo_2)))))) : ((Variant)(0LL)));
              (v_wsEnd = v_endPos + 2LL + LINE(243,x_strspn(toString(v_text), " ", toInt32(v_endPos + 3LL))));
              if (more(v_wsStart, 0LL) && equal(LINE(248,x_substr(toString(v_text), toInt32(v_wsStart - 1LL), toInt32(1LL))), "\n") && equal(LINE(249,x_substr(toString(v_text), toInt32(v_wsEnd + 1LL), toInt32(1LL))), "\n")) {
                (v_startPos = v_wsStart);
                (v_endPos = v_wsEnd + 1LL);
                (v_wsLength = v_i - v_wsStart);
                if (more(v_wsLength, 0LL) && instanceOf(v_accum.o_get("lastNode", 0x6B446EEF03FAE201LL), "PPNode_Hash_Text") && same_rev(LINE(258,x_str_repeat(" ", toInt32(v_wsLength))), x_substr(toString(v_accum.o_get("lastNode", 0x6B446EEF03FAE201LL).o_get("value", 0x69E7413AE0C88471LL)), toInt32(negate(v_wsLength))))) {
                  (lval(v_accum.o_lval("lastNode", 0x6B446EEF03FAE201LL)).o_lval("value", 0x69E7413AE0C88471LL) = LINE(260,x_substr(toString(v_accum.o_get("lastNode", 0x6B446EEF03FAE201LL).o_get("value", 0x69E7413AE0C88471LL)), toInt32(0LL), toInt32(negate(v_wsLength)))));
                }
                (v_fakeLineStart = true);
              }
              else {
                (v_startPos = v_i);
                v_endPos += 2LL;
              }
              if (toBoolean(v_stack.o_get("top", 0x1854FD73A00D89E8LL))) {
                (v_part = LINE(271,v_stack.o_get("top", 0x1854FD73A00D89E8LL).o_invoke_few_args("getCurrentPart", 0x1726802D706D7BECLL, 0)));
                if (toObject(v_part)->t___isset("commentEnd") && equal(v_part.o_get("commentEnd", 0x5537E70C21E6C099LL), v_wsStart - 1LL)) {
                  (v_part.o_lval("commentEnd", 0x5537E70C21E6C099LL) = v_wsEnd);
                }
                else {
                  (v_part.o_lval("visualEnd", 0x101972FF17696EF2LL) = v_wsStart);
                  (v_part.o_lval("commentEnd", 0x5537E70C21E6C099LL) = v_endPos);
                }
              }
              (v_i = v_endPos + 1LL);
              (v_inner = LINE(281,x_substr(toString(v_text), toInt32(v_startPos), toInt32(v_endPos - v_startPos + 1LL))));
              LINE(282,v_accum.o_invoke_few_args("addNodeWithText", 0x6992F0F690B37356LL, 2, "comment", v_inner));
            }
            continue;
          }
          (v_name = v_matches.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
          (v_lowerName = LINE(287,x_strtolower(toString(v_name))));
          (v_attrStart = v_i + LINE(288,x_strlen(toString(v_name))) + 1LL);
          (v_tagEndPos = toBoolean(v_noMoreGT) ? ((Variant)(false)) : ((Variant)(LINE(291,x_strpos(toString(v_text), ">", toInt32(v_attrStart))))));
          if (same(v_tagEndPos, false)) {
            (v_noMoreGT = true);
            LINE(296,v_accum.o_invoke_few_args("addLiteral", 0x05AAC40C7DAD4ACELL, 1, "<"));
            ++v_i;
            continue;
          }
          if (LINE(302,x_in_array(v_lowerName, v_ignoredTags))) {
            (assignCallTemp(eo_1, ref(LINE(303,x_substr(toString(v_text), toInt32(v_i), toInt32(v_tagEndPos - v_i + 1LL))))),v_accum.o_invoke("addNodeWithText", Array(ArrayInit(2).set(0, "ignore").set(1, eo_1).create()), 0x6992F0F690B37356LL));
            (v_i = v_tagEndPos + 1LL);
            continue;
          }
          (v_tagStartPos = v_i);
          if (equal(v_text.rvalAt(v_tagEndPos - 1LL), "/")) {
            (v_attrEnd = v_tagEndPos - 1LL);
            setNull(v_inner);
            (v_i = v_tagEndPos + 1LL);
            setNull(v_close);
          }
          else {
            (v_attrEnd = v_tagEndPos);
            if (toBoolean(LINE(319,(assignCallTemp(eo_0, LINE(318,(assignCallTemp(eo_6, x_preg_quote(toString(v_name), "/")),concat3("/<\\/", eo_6, "\\s*>/i")))),assignCallTemp(eo_1, toString(v_text)),assignCallTemp(eo_2, ref(v_matches)),assignCallTemp(eo_4, toInt32(v_tagEndPos + 1LL)),x_preg_match(eo_0, eo_1, eo_2, toInt32(256LL) /* PREG_OFFSET_CAPTURE */, eo_4))))) {
              (v_inner = LINE(321,x_substr(toString(v_text), toInt32(v_tagEndPos + 1LL), toInt32(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL) - v_tagEndPos - 1LL))));
              (v_i = v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(1LL, 0x5BCA7C69B794F8CELL) + LINE(322,x_strlen(toString(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL)))));
              (v_close = v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
            }
            else {
              (v_inner = LINE(326,x_substr(toString(v_text), toInt32(v_tagEndPos + 1LL))));
              (v_i = LINE(327,x_strlen(toString(v_text))));
              setNull(v_close);
            }
          }
          if (LINE(332,x_in_array(v_lowerName, v_ignoredElements))) {
            (assignCallTemp(eo_1, ref(LINE(333,x_substr(toString(v_text), toInt32(v_tagStartPos), toInt32(v_i - v_tagStartPos))))),v_accum.o_invoke("addNodeWithText", Array(ArrayInit(2).set(0, "ignore").set(1, eo_1).create()), 0x6992F0F690B37356LL));
            continue;
          }
          if (not_more(v_attrEnd, v_attrStart)) {
            (v_attr = "");
          }
          else {
            (v_attr = LINE(342,x_substr(toString(v_text), toInt32(v_attrStart), toInt32(v_attrEnd - v_attrStart))));
          }
          (v_extNode = ((Object)(LINE(345,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create("ext"))))));
          LINE(346,v_extNode.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, c_ppnode_hash_tree::t_newwithtext("name", v_name)));
          LINE(347,v_extNode.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, c_ppnode_hash_tree::t_newwithtext("attr", v_attr)));
          if (!same(v_inner, null)) {
            LINE(349,v_extNode.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, c_ppnode_hash_tree::t_newwithtext("inner", v_inner)));
          }
          if (!same(v_close, null)) {
            LINE(352,v_extNode.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, c_ppnode_hash_tree::t_newwithtext("close", v_close)));
          }
          LINE(354,v_accum.o_invoke_few_args("addNode", 0x0D8DDF05964C6A31LL, 1, v_extNode));
        }
        else if (equal(v_found, "line-start")) {
          if (toBoolean(v_fakeLineStart)) {
            (v_fakeLineStart = false);
          }
          else {
            LINE(363,v_accum.o_invoke_few_args("addLiteral", 0x05AAC40C7DAD4ACELL, 1, v_curChar));
            v_i++;
          }
          (v_count = LINE(367,x_strspn(toString(v_text), "=", toInt32(v_i), toInt32(6LL))));
          if (equal(v_count, 1LL) && toBoolean(v_findEquals)) {
          }
          else if (more(v_count, 0LL)) {
            (v_piece = Array(ArrayInit(5).set(0, "open", "\n", 0x6A760D2EC60228C6LL).set(1, "close", "\n", 0x36B43082F052EC6BLL).set(2, "parts", Array(ArrayInit(1).set(0, ((Object)(LINE(376,p_ppdpart_hash(p_ppdpart_hash(NEWOBJ(c_ppdpart_hash)())->create(x_str_repeat("=", toInt32(v_count)))))))).create()), 0x2708FDA74562AD8DLL).set(3, "startPos", v_i, 0x749C3FAB1E74371DLL).set(4, "count", v_count, 0x3D66B5980D54BABBLL).create()));
            LINE(379,v_stack.o_invoke_few_args("push", 0x321E2BF5D878AA38LL, 1, v_piece));
            (v_accum = ref(LINE(380,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
            LINE(381,extract(variables, toArray(v_stack.o_invoke_few_args("getFlags", 0x27E7DBA875AD17E1LL, 0))));
            v_i += v_count;
          }
        }
        else if (equal(v_found, "line-end")) {
          (v_piece = v_stack.o_get("top", 0x1854FD73A00D89E8LL));
          LINE(389,x_assert(equal(v_piece.o_get("open", 0x6A760D2EC60228C6LL), "\n")));
          (v_part = LINE(390,v_piece.o_invoke_few_args("getCurrentPart", 0x1726802D706D7BECLL, 0)));
          (v_wsLength = LINE(393,(assignCallTemp(eo_0, toString(v_revText)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_text)) - v_i)),x_strspn(eo_0, " \t", eo_2))));
          (v_searchStart = v_i - v_wsLength);
          if (toObject(v_part)->t___isset("commentEnd") && equal(v_searchStart - 1LL, v_part.o_get("commentEnd", 0x5537E70C21E6C099LL))) {
            (v_searchStart = v_part.o_get("visualEnd", 0x101972FF17696EF2LL));
            v_searchStart -= LINE(399,(assignCallTemp(eo_0, toString(v_revText)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_text)) - v_searchStart)),x_strspn(eo_0, " \t", eo_2)));
          }
          (v_count = v_piece.o_get("count", 0x3D66B5980D54BABBLL));
          (v_equalsLength = LINE(402,(assignCallTemp(eo_0, toString(v_revText)),assignCallTemp(eo_2, toInt32(x_strlen(toString(v_text)) - v_searchStart)),x_strspn(eo_0, "=", eo_2))));
          if (more(v_equalsLength, 0LL)) {
            if (equal(v_i - v_equalsLength, v_piece.o_get("startPos", 0x749C3FAB1E74371DLL))) {
              (v_count = v_equalsLength);
              if (less(v_count, 3LL)) {
                (v_count = 0LL);
              }
              else {
                (v_count = LINE(412,(assignCallTemp(eo_1, x_intval(divide((v_count - 1LL), 2LL))),x_min(2, 6LL, Array(ArrayInit(1).set(0, eo_1).create())))));
              }
            }
            else {
              (v_count = LINE(415,x_min(2, v_equalsLength, Array(ArrayInit(1).set(0, v_count).create()))));
            }
            if (more(v_count, 0LL)) {
              (v_element = ((Object)(LINE(419,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create("possible-h"))))));
              LINE(420,v_element.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, p_ppnode_hash_attr(p_ppnode_hash_attr(NEWOBJ(c_ppnode_hash_attr)())->create("level", v_count))));
              LINE(421,v_element.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, (assignCallTemp(eo_1, v_headingIndex++),p_ppnode_hash_attr(p_ppnode_hash_attr(NEWOBJ(c_ppnode_hash_attr)())->create("i", eo_1)))));
              (lval(v_element.o_lval("lastChild", 0x29DD4C280399C7FALL)).o_lval("nextSibling", 0x0B3EC7643EE81822LL) = v_accum.o_get("firstNode", 0x4296B55256FA0986LL));
              (v_element.o_lval("lastChild", 0x29DD4C280399C7FALL) = v_accum.o_get("lastNode", 0x6B446EEF03FAE201LL));
            }
            else {
              (v_element = v_accum);
            }
          }
          else {
            (v_element = v_accum);
          }
          LINE(433,v_stack.o_invoke_few_args("pop", 0x773C5A963CD2AC13LL, 0));
          (v_accum = ref(LINE(434,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
          LINE(435,extract(variables, toArray(v_stack.o_invoke_few_args("getFlags", 0x27E7DBA875AD17E1LL, 0))));
          if (instanceOf(v_element, "PPNode")) {
            LINE(439,v_accum.o_invoke_few_args("addNode", 0x0D8DDF05964C6A31LL, 1, v_element));
          }
          else {
            LINE(441,v_accum.o_invoke_few_args("addAccum", 0x6A8DCEFA6345DAC9LL, 1, v_element));
          }
        }
        else if (equal(v_found, "open")) {
          (v_count = LINE(452,x_strspn(toString(v_text), toString(v_curChar), toInt32(v_i))));
          if (not_less(v_count, v_rule.rvalAt("min", 0x66FFC7365E2D00CALL))) {
            (v_piece = Array(ArrayInit(4).set(0, "open", v_curChar, 0x6A760D2EC60228C6LL).set(1, "close", v_rule.rvalAt("end", 0x6DE935C204DC3D01LL), 0x36B43082F052EC6BLL).set(2, "count", v_count, 0x3D66B5980D54BABBLL).set(3, "lineStart", (more(v_i, 0LL) && equal(v_text.rvalAt(v_i - 1LL), "\n")), 0x0F84CB175598D0A5LL).create()));
            LINE(464,v_stack.o_invoke_few_args("push", 0x321E2BF5D878AA38LL, 1, v_piece));
            (v_accum = ref(LINE(465,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
            LINE(466,extract(variables, toArray(v_stack.o_invoke_few_args("getFlags", 0x27E7DBA875AD17E1LL, 0))));
          }
          else {
            LINE(469,v_accum.o_invoke_few_args("addLiteral", 0x05AAC40C7DAD4ACELL, 1, x_str_repeat(toString(v_curChar), toInt32(v_count))));
          }
          v_i += v_count;
        }
        else if (equal(v_found, "close")) {
          (v_piece = v_stack.o_get("top", 0x1854FD73A00D89E8LL));
          (v_maxCount = v_piece.o_get("count", 0x3D66B5980D54BABBLL));
          (v_count = LINE(478,x_strspn(toString(v_text), toString(v_curChar), toInt32(v_i), toInt32(v_maxCount))));
          (v_matchingCount = 0LL);
          (v_rule = v_rules.rvalAt(v_piece.o_get("open", 0x6A760D2EC60228C6LL)));
          if (more(v_count, v_rule.rvalAt("max", 0x022BD5C706BD9850LL))) {
            (v_matchingCount = v_rule.rvalAt("max", 0x022BD5C706BD9850LL));
          }
          else {
            (v_matchingCount = v_count);
            LOOP_COUNTER(39);
            {
              while (more(v_matchingCount, 0LL) && !(LINE(493,x_array_key_exists(v_matchingCount, v_rule.rvalAt("names", 0x51A687446C1579F2LL))))) {
                LOOP_COUNTER_CHECK(39);
                {
                  --v_matchingCount;
                }
              }
            }
          }
          if (not_more(v_matchingCount, 0LL)) {
            LINE(501,v_accum.o_invoke_few_args("addLiteral", 0x05AAC40C7DAD4ACELL, 1, x_str_repeat(toString(v_curChar), toInt32(v_count))));
            v_i += v_count;
            continue;
          }
          (v_name = v_rule.rvalAt("names", 0x51A687446C1579F2LL).rvalAt(v_matchingCount));
          if (same(v_name, null)) {
            (v_element = LINE(508,v_piece.o_invoke_few_args("breakSyntax", 0x1CF88FA5EFC88C39LL, 1, v_matchingCount)));
            LINE(509,v_element.o_invoke_few_args("addLiteral", 0x05AAC40C7DAD4ACELL, 1, x_str_repeat(toString(v_rule.rvalAt("end", 0x6DE935C204DC3D01LL)), toInt32(v_matchingCount))));
          }
          else {
            (v_parts = v_piece.o_get("parts", 0x2708FDA74562AD8DLL));
            (v_titleAccum = v_parts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("out", 0x7C801AC012E9F722LL));
            v_parts.weakRemove(0LL, 0x77CFA1EEF01BCA90LL);
            (v_element = ((Object)(LINE(517,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create(v_name))))));
            if (equal(v_maxCount, v_matchingCount) && !(empty(toObject(v_piece).o_get("lineStart", 0x0F84CB175598D0A5LL)))) {
              LINE(522,v_element.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, p_ppnode_hash_attr(p_ppnode_hash_attr(NEWOBJ(c_ppnode_hash_attr)())->create("lineStart", 1LL))));
            }
            (v_titleNode = ((Object)(LINE(524,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create("title"))))));
            (v_titleNode.o_lval("firstChild", 0x22862CD26C0AAB50LL) = v_titleAccum.o_get("firstNode", 0x4296B55256FA0986LL));
            (v_titleNode.o_lval("lastChild", 0x29DD4C280399C7FALL) = v_titleAccum.o_get("lastNode", 0x6B446EEF03FAE201LL));
            LINE(527,v_element.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, v_titleNode));
            (v_argIndex = 1LL);
            {
              LOOP_COUNTER(40);
              for (ArrayIterPtr iter42 = v_parts.begin("preprocessor_hash"); !iter42->end(); iter42->next()) {
                LOOP_COUNTER_CHECK(40);
                v_part = iter42->second();
                v_partIndex = iter42->first();
                {
                  if (toObject(v_part)->t___isset("eqpos")) {
                    (v_lastNode = false);
                    {
                      LOOP_COUNTER(43);
                      for ((v_node = v_part.o_get("out", 0x7C801AC012E9F722LL).o_get("firstNode", 0x4296B55256FA0986LL)); toBoolean(v_node); (v_node = v_node.o_get("nextSibling", 0x0B3EC7643EE81822LL))) {
                        LOOP_COUNTER_CHECK(43);
                        {
                          if (same(v_node, v_part.o_get("eqpos", 0x33437B517F4D41D7LL))) {
                            break;
                          }
                          (v_lastNode = v_node);
                        }
                      }
                    }
                    if (!(toBoolean(v_node))) {
                      throw_exception(LINE(540,create_object("mwexception", Array(ArrayInit(1).set(0, "Preprocessor_Hash::preprocessToObj: eqpos not found").create()))));
                    }
                    if (!same(v_node.o_get("name", 0x0BCDB293DC3CBDDCLL), "equals")) {
                      throw_exception(LINE(543,create_object("mwexception", Array(ArrayInit(1).set(0, "Preprocessor_Hash::preprocessToObj: eqpos is not equals").create()))));
                    }
                    (v_equalsNode = v_node);
                    (v_nameNode = ((Object)(LINE(548,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create("name"))))));
                    if (!same(v_lastNode, false)) {
                      (v_lastNode.o_lval("nextSibling", 0x0B3EC7643EE81822LL) = false);
                      (v_nameNode.o_lval("firstChild", 0x22862CD26C0AAB50LL) = v_part.o_get("out", 0x7C801AC012E9F722LL).o_get("firstNode", 0x4296B55256FA0986LL));
                      (v_nameNode.o_lval("lastChild", 0x29DD4C280399C7FALL) = v_lastNode);
                    }
                    (v_valueNode = ((Object)(LINE(556,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create("value"))))));
                    if (!same(v_equalsNode.o_get("nextSibling", 0x0B3EC7643EE81822LL), false)) {
                      (v_valueNode.o_lval("firstChild", 0x22862CD26C0AAB50LL) = v_equalsNode.o_get("nextSibling", 0x0B3EC7643EE81822LL));
                      (v_valueNode.o_lval("lastChild", 0x29DD4C280399C7FALL) = v_part.o_get("out", 0x7C801AC012E9F722LL).o_get("lastNode", 0x6B446EEF03FAE201LL));
                    }
                    (v_partNode = ((Object)(LINE(561,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create("part"))))));
                    LINE(562,v_partNode.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, v_nameNode));
                    LINE(563,v_partNode.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, v_equalsNode.o_lval("firstChild", 0x22862CD26C0AAB50LL)));
                    LINE(564,v_partNode.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, v_valueNode));
                    LINE(565,v_element.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, v_partNode));
                  }
                  else {
                    (v_partNode = ((Object)(LINE(567,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create("part"))))));
                    (v_nameNode = ((Object)(LINE(568,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create("name"))))));
                    LINE(569,v_nameNode.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, (assignCallTemp(eo_1, v_argIndex++),p_ppnode_hash_attr(p_ppnode_hash_attr(NEWOBJ(c_ppnode_hash_attr)())->create("index", eo_1)))));
                    (v_valueNode = ((Object)(LINE(570,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create("value"))))));
                    (v_valueNode.o_lval("firstChild", 0x22862CD26C0AAB50LL) = v_part.o_get("out", 0x7C801AC012E9F722LL).o_get("firstNode", 0x4296B55256FA0986LL));
                    (v_valueNode.o_lval("lastChild", 0x29DD4C280399C7FALL) = v_part.o_get("out", 0x7C801AC012E9F722LL).o_get("lastNode", 0x6B446EEF03FAE201LL));
                    LINE(573,v_partNode.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, v_nameNode));
                    LINE(574,v_partNode.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, v_valueNode));
                    LINE(575,v_element.o_invoke_few_args("addChild", 0x2DDE12A9866FC794LL, 1, v_partNode));
                  }
                }
              }
            }
          }
          v_i += v_matchingCount;
          LINE(584,v_stack.o_invoke_few_args("pop", 0x773C5A963CD2AC13LL, 0));
          (v_accum = ref(LINE(585,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
          if (less(v_matchingCount, v_piece.o_get("count", 0x3D66B5980D54BABBLL))) {
            (v_piece.o_lval("parts", 0x2708FDA74562AD8DLL) = Array(ArrayInit(1).set(0, ((Object)(LINE(589,p_ppdpart_hash(p_ppdpart_hash(NEWOBJ(c_ppdpart_hash)())->create()))))).create()));
            v_piece.o_lval("count", 0x3D66B5980D54BABBLL) -= v_matchingCount;
            (v_names = v_rules.rvalAt(v_piece.o_get("open", 0x6A760D2EC60228C6LL)).rvalAt("names", 0x51A687446C1579F2LL));
            (v_skippedBraces = 0LL);
            (v_enclosingAccum = ref(v_accum));
            LOOP_COUNTER(44);
            {
              while (toBoolean(v_piece.o_get("count", 0x3D66B5980D54BABBLL))) {
                LOOP_COUNTER_CHECK(44);
                {
                  if (LINE(596,x_array_key_exists(v_piece.o_get("count", 0x3D66B5980D54BABBLL), v_names))) {
                    LINE(597,v_stack.o_invoke_few_args("push", 0x321E2BF5D878AA38LL, 1, v_piece));
                    (v_accum = ref(LINE(598,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
                    break;
                  }
                  --lval(v_piece.o_lval("count", 0x3D66B5980D54BABBLL));
                  v_skippedBraces++;
                }
              }
            }
            LINE(604,v_enclosingAccum.o_invoke_few_args("addLiteral", 0x05AAC40C7DAD4ACELL, 1, x_str_repeat(toString(v_piece.o_get("open", 0x6A760D2EC60228C6LL)), toInt32(v_skippedBraces))));
          }
          LINE(607,extract(variables, toArray(v_stack.o_invoke_few_args("getFlags", 0x27E7DBA875AD17E1LL, 0))));
          if (instanceOf(v_element, "PPNode")) {
            LINE(611,v_accum.o_invoke_few_args("addNode", 0x0D8DDF05964C6A31LL, 1, v_element));
          }
          else {
            LINE(613,v_accum.o_invoke_few_args("addAccum", 0x6A8DCEFA6345DAC9LL, 1, v_element));
          }
        }
        else if (equal(v_found, "pipe")) {
          (v_findEquals = true);
          LINE(619,v_stack.o_invoke_few_args("addPart", 0x499E72B719A4CAC2LL, 0));
          (v_accum = ref(LINE(620,v_stack.o_invoke_few_args("getAccum", 0x1A5F5A2CB292E516LL, 0))));
          ++v_i;
        }
        else if (equal(v_found, "equals")) {
          (v_findEquals = false);
          LINE(626,v_accum.o_invoke_few_args("addNodeWithText", 0x6992F0F690B37356LL, 2, "equals", "="));
          (LINE(627,v_stack.o_invoke_few_args("getCurrentPart", 0x1726802D706D7BECLL, 0)).o_lval("eqpos", 0x33437B517F4D41D7LL) = v_accum.o_get("lastNode", 0x6B446EEF03FAE201LL));
          ++v_i;
        }
      }
    }
  }
  {
    LOOP_COUNTER(45);
    Variant map46 = v_stack.o_get("stack", 0x5084E637B870262BLL);
    for (ArrayIterPtr iter47 = map46.begin("preprocessor_hash"); !iter47->end(); iter47->next()) {
      LOOP_COUNTER_CHECK(45);
      v_piece = iter47->second();
      {
        LINE(634,v_stack.o_get("rootAccum", 0x5F4D08F214717E0FLL).o_invoke_few_args("addAccum", 0x6A8DCEFA6345DAC9LL, 1, v_piece.o_invoke_few_args("breakSyntax", 0x1CF88FA5EFC88C39LL, 0)));
      }
    }
  }
  {
    LOOP_COUNTER(48);
    for ((v_node = v_stack.o_get("rootAccum", 0x5F4D08F214717E0FLL).o_get("firstNode", 0x4296B55256FA0986LL)); toBoolean(v_node); (v_node = v_node.o_get("nextSibling", 0x0B3EC7643EE81822LL))) {
      LOOP_COUNTER_CHECK(48);
      {
        if (toObject(v_node)->t___isset("name") && same(v_node.o_get("name", 0x0BCDB293DC3CBDDCLL), "possible-h")) {
          (v_node.o_lval("name", 0x0BCDB293DC3CBDDCLL) = "h");
        }
      }
    }
  }
  (v_rootNode = ((Object)(LINE(644,p_ppnode_hash_tree(p_ppnode_hash_tree(NEWOBJ(c_ppnode_hash_tree)())->create("root"))))));
  (v_rootNode.o_lval("firstChild", 0x22862CD26C0AAB50LL) = v_stack.o_get("rootAccum", 0x5F4D08F214717E0FLL).o_get("firstNode", 0x4296B55256FA0986LL));
  (v_rootNode.o_lval("lastChild", 0x29DD4C280399C7FALL) = v_stack.o_get("rootAccum", 0x5F4D08F214717E0FLL).o_get("lastNode", 0x6B446EEF03FAE201LL));
  if (toBoolean(v_cacheable)) {
    (v_cacheValue = concat_rev(LINE(650,x_serialize(v_rootNode)), x_sprintf(2, "%08d", ScalarArrays::sa_[2])));
    LINE(651,v_wgMemc.o_invoke_few_args("set", 0x399A6427C2185621LL, 3, v_cacheKey, v_cacheValue, 86400LL));
    LINE(652,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Preprocessor_Hash::preprocessToObj-cache-miss").create()), 0x00000000B599F276LL));
    LINE(653,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Preprocessor_Hash::preprocessToObj-cacheable").create()), 0x00000000B599F276LL));
    LINE(654,(assignCallTemp(eo_1, concat3("Saved preprocessor Hash to memcached (key ", toString(v_cacheKey), ")")),invoke_failed("wfdebuglog", Array(ArrayInit(2).set(0, "Preprocessor").set(1, eo_1).create()), 0x00000000E6942489LL)));
  }
  LINE(657,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Preprocessor_Hash::preprocessToObj").create()), 0x00000000B599F276LL));
  return v_rootNode;
} /* function */
/* SRC: Preprocessor_Hash.php line 712 */
Variant c_ppdpart_hash::os_get(const char *s, int64 hash) {
  return c_ppdpart::os_get(s, hash);
}
Variant &c_ppdpart_hash::os_lval(const char *s, int64 hash) {
  return c_ppdpart::os_lval(s, hash);
}
void c_ppdpart_hash::o_get(ArrayElementVec &props) const {
  c_ppdpart::o_get(props);
}
bool c_ppdpart_hash::o_exists(CStrRef s, int64 hash) const {
  return c_ppdpart::o_exists(s, hash);
}
Variant c_ppdpart_hash::o_get(CStrRef s, int64 hash) {
  return c_ppdpart::o_get(s, hash);
}
Variant c_ppdpart_hash::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ppdpart::o_set(s, hash, v, forInit);
}
Variant &c_ppdpart_hash::o_lval(CStrRef s, int64 hash) {
  return c_ppdpart::o_lval(s, hash);
}
Variant c_ppdpart_hash::os_constant(const char *s) {
  return c_ppdpart::os_constant(s);
}
IMPLEMENT_CLASS(ppdpart_hash)
ObjectData *c_ppdpart_hash::create(Variant v_out //  = ""
) {
  init();
  t___construct(v_out);
  return this;
}
ObjectData *c_ppdpart_hash::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_ppdpart_hash::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_ppdpart_hash::cloneImpl() {
  c_ppdpart_hash *obj = NEW(c_ppdpart_hash)();
  cloneSet(obj);
  return obj;
}
void c_ppdpart_hash::cloneSet(c_ppdpart_hash *clone) {
  c_ppdpart::cloneSet(clone);
}
Variant c_ppdpart_hash::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ppdpart::o_invoke(s, params, hash, fatal);
}
Variant c_ppdpart_hash::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ppdpart::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppdpart_hash::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ppdpart::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppdpart_hash$os_get(const char *s) {
  return c_ppdpart_hash::os_get(s, -1);
}
Variant &cw_ppdpart_hash$os_lval(const char *s) {
  return c_ppdpart_hash::os_lval(s, -1);
}
Variant cw_ppdpart_hash$os_constant(const char *s) {
  return c_ppdpart_hash::os_constant(s);
}
Variant cw_ppdpart_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppdpart_hash::os_invoke(c, s, params, -1, fatal);
}
void c_ppdpart_hash::init() {
  c_ppdpart::init();
}
/* SRC: Preprocessor_Hash.php line 713 */
void c_ppdpart_hash::t___construct(Variant v_out //  = ""
) {
  INSTANCE_METHOD_INJECTION(PPDPart_Hash, PPDPart_Hash::__construct);
  bool oldInCtor = gasInCtor(true);
  p_ppdaccum_hash v_accum;

  ((Object)((v_accum = ((Object)(LINE(714,p_ppdaccum_hash(p_ppdaccum_hash(NEWOBJ(c_ppdaccum_hash)())->create())))))));
  if (!same(v_out, "")) {
    LINE(716,v_accum->t_addliteral(v_out));
  }
  LINE(718,c_ppdpart::t___construct(((Object)(v_accum))));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 1297 */
Variant c_ppcustomframe_hash::os_get(const char *s, int64 hash) {
  return c_ppframe_hash::os_get(s, hash);
}
Variant &c_ppcustomframe_hash::os_lval(const char *s, int64 hash) {
  return c_ppframe_hash::os_lval(s, hash);
}
void c_ppcustomframe_hash::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("args", m_args.isReferenced() ? ref(m_args) : m_args));
  c_ppframe_hash::o_get(props);
}
bool c_ppcustomframe_hash::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_EXISTS_STRING(0x4AF7CD17F976719ELL, args, 4);
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_exists(s, hash);
}
Variant c_ppcustomframe_hash::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4AF7CD17F976719ELL, m_args,
                         args, 4);
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_get(s, hash);
}
Variant c_ppcustomframe_hash::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_SET_STRING(0x4AF7CD17F976719ELL, m_args,
                      args, 4);
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_set(s, hash, v, forInit);
}
Variant &c_ppcustomframe_hash::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 0:
      HASH_RETURN_STRING(0x4AF7CD17F976719ELL, m_args,
                         args, 4);
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_lval(s, hash);
}
Variant c_ppcustomframe_hash::os_constant(const char *s) {
  return c_ppframe_hash::os_constant(s);
}
IMPLEMENT_CLASS(ppcustomframe_hash)
ObjectData *c_ppcustomframe_hash::create(Variant v_preprocessor, Variant v_args) {
  init();
  t___construct(v_preprocessor, v_args);
  return this;
}
ObjectData *c_ppcustomframe_hash::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_ppcustomframe_hash::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_ppcustomframe_hash::cloneImpl() {
  c_ppcustomframe_hash *obj = NEW(c_ppcustomframe_hash)();
  cloneSet(obj);
  return obj;
}
void c_ppcustomframe_hash::cloneSet(c_ppcustomframe_hash *clone) {
  clone->m_args = m_args.isReferenced() ? ref(m_args) : m_args;
  c_ppframe_hash::cloneSet(clone);
}
Variant c_ppcustomframe_hash::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        int count = params.size();
        if (count <= 3) return (t_virtualbracketedimplode(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        return (t_virtualbracketedimplode(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        int count = params.size();
        if (count <= 1) return (t_expand(params.rvalAt(0)));
        return (t_expand(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        int count = params.size();
        if (count <= 2) return (t_implodewithflags(count, params.rvalAt(0), params.rvalAt(1)));
        return (t_implodewithflags(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        int count = params.size();
        if (count <= 1) return (t_virtualimplode(count, params.rvalAt(0)));
        return (t_virtualimplode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        int count = params.size();
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(params.rvalAt(0)));
        return (t_newchild(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        int count = params.size();
        if (count <= 1) return (t_implode(count, params.rvalAt(0)));
        return (t_implode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(params.rvalAt(0)));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_invoke(s, params, hash, fatal);
}
Variant c_ppcustomframe_hash::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        if (count <= 3) return (t_virtualbracketedimplode(count, a0, a1, a2));
        Array params;
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualbracketedimplode(count,a0, a1, a2, params));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        if (count <= 1) return (t_expand(a0));
        return (t_expand(a0, a1));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        if (count <= 2) return (t_implodewithflags(count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implodewithflags(count,a0, a1, params));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        if (count <= 1) return (t_virtualimplode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualimplode(count,a0, params));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(a0));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(a0));
        return (t_newchild(a0, a1));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        if (count <= 1) return (t_implode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implode(count,a0, params));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(a0));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppcustomframe_hash::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ppframe_hash::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppcustomframe_hash$os_get(const char *s) {
  return c_ppcustomframe_hash::os_get(s, -1);
}
Variant &cw_ppcustomframe_hash$os_lval(const char *s) {
  return c_ppcustomframe_hash::os_lval(s, -1);
}
Variant cw_ppcustomframe_hash$os_constant(const char *s) {
  return c_ppcustomframe_hash::os_constant(s);
}
Variant cw_ppcustomframe_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppcustomframe_hash::os_invoke(c, s, params, -1, fatal);
}
void c_ppcustomframe_hash::init() {
  c_ppframe_hash::init();
  m_args = null;
}
/* SRC: Preprocessor_Hash.php line 1300 */
void c_ppcustomframe_hash::t___construct(Variant v_preprocessor, Variant v_args) {
  INSTANCE_METHOD_INJECTION(PPCustomFrame_Hash, PPCustomFrame_Hash::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(1301,c_ppframe_hash::t___construct(v_preprocessor));
  (m_args = v_args);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 1305 */
String c_ppcustomframe_hash::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPCustomFrame_Hash, PPCustomFrame_Hash::__toString);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  String v_s;
  bool v_first = false;
  Primitive v_name = 0;
  Variant v_value;

  (v_s = "cstmframe{");
  (v_first = true);
  {
    LOOP_COUNTER(49);
    Variant map50 = m_args;
    for (ArrayIterPtr iter51 = map50.begin("ppcustomframe_hash"); !iter51->end(); iter51->next()) {
      LOOP_COUNTER_CHECK(49);
      v_value = iter51->second();
      v_name = iter51->first();
      {
        if (v_first) {
          (v_first = false);
        }
        else {
          concat_assign(v_s, ", ");
        }
        concat_assign(v_s, LINE(1315,(assignCallTemp(eo_1, toString(v_name)),assignCallTemp(eo_3, toString((assignCallTemp(eo_7, v_value.o_invoke_few_args("__toString", 0x642C2D2994B34A13LL, 0)),x_str_replace("\"", "\\\"", eo_7)))),concat5("\"", eo_1, "\":\"", eo_3, "\""))));
      }
    }
  }
  concat_assign(v_s, "}");
  return v_s;
} /* function */
/* SRC: Preprocessor_Hash.php line 1321 */
bool c_ppcustomframe_hash::t_isempty() {
  INSTANCE_METHOD_INJECTION(PPCustomFrame_Hash, PPCustomFrame_Hash::isEmpty);
  return !(toBoolean(LINE(1322,x_count(m_args))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1325 */
Variant c_ppcustomframe_hash::t_getargument(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(PPCustomFrame_Hash, PPCustomFrame_Hash::getArgument);
  if (!(isset(m_args, v_index))) {
    return false;
  }
  return m_args.rvalAt(v_index);
} /* function */
/* SRC: Preprocessor_Hash.php line 1591 */
Variant c_ppnode_hash_attr::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppnode_hash_attr::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ppnode_hash_attr::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("name", m_name.isReferenced() ? ref(m_name) : m_name));
  props.push_back(NEW(ArrayElement)("value", m_value.isReferenced() ? ref(m_value) : m_value));
  props.push_back(NEW(ArrayElement)("nextSibling", m_nextSibling.isReferenced() ? ref(m_nextSibling) : m_nextSibling));
  c_ObjectData::o_get(props);
}
bool c_ppnode_hash_attr::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_EXISTS_STRING(0x69E7413AE0C88471LL, value, 5);
      break;
    case 2:
      HASH_EXISTS_STRING(0x0B3EC7643EE81822LL, nextSibling, 11);
      break;
    case 4:
      HASH_EXISTS_STRING(0x0BCDB293DC3CBDDCLL, name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppnode_hash_attr::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                         nextSibling, 11);
      break;
    case 4:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppnode_hash_attr::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_SET_STRING(0x69E7413AE0C88471LL, m_value,
                      value, 5);
      break;
    case 2:
      HASH_SET_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                      nextSibling, 11);
      break;
    case 4:
      HASH_SET_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                      name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppnode_hash_attr::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 7) {
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                         nextSibling, 11);
      break;
    case 4:
      HASH_RETURN_STRING(0x0BCDB293DC3CBDDCLL, m_name,
                         name, 4);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppnode_hash_attr::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppnode_hash_attr)
ObjectData *c_ppnode_hash_attr::create(Variant v_name, Variant v_value) {
  init();
  t___construct(v_name, v_value);
  return this;
}
ObjectData *c_ppnode_hash_attr::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1)));
  } else return this;
}
void c_ppnode_hash_attr::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1)));
}
ObjectData *c_ppnode_hash_attr::cloneImpl() {
  c_ppnode_hash_attr *obj = NEW(c_ppnode_hash_attr)();
  cloneSet(obj);
  return obj;
}
void c_ppnode_hash_attr::cloneSet(c_ppnode_hash_attr *clone) {
  clone->m_name = m_name.isReferenced() ? ref(m_name) : m_name;
  clone->m_value = m_value.isReferenced() ? ref(m_value) : m_value;
  clone->m_nextSibling = m_nextSibling.isReferenced() ? ref(m_nextSibling) : m_nextSibling;
  ObjectData::cloneSet(clone);
}
Variant c_ppnode_hash_attr::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 0:
      HASH_GUARD(0x73E7C3304C0C5360LL, splitarg) {
        return (t_splitarg(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 6:
      HASH_GUARD(0x0A41DBE0830902C6LL, item) {
        return (t_item(params.rvalAt(0)));
      }
      break;
    case 9:
      HASH_GUARD(0x050681239965E069LL, getfirstchild) {
        return (t_getfirstchild());
      }
      break;
    case 12:
      HASH_GUARD(0x47D4EE3CB3EB696CLL, splitext) {
        return (t_splitext(), null);
      }
      break;
    case 13:
      HASH_GUARD(0x0621AE2D22A1922DLL, splitheading) {
        return (t_splitheading(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x5ABDC66291F1CA2ELL, getchildrenoftype) {
        return (t_getchildrenoftype(params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x732EC1BDA8EC520FLL, getchildren) {
        return (t_getchildren());
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 20:
      HASH_GUARD(0x41EB078EF92C44D4LL, getlength) {
        return (t_getlength());
      }
      break;
    case 25:
      HASH_GUARD(0x4CB333CF2D880119LL, getnextsibling) {
        return (t_getnextsibling());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppnode_hash_attr::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 0:
      HASH_GUARD(0x73E7C3304C0C5360LL, splitarg) {
        return (t_splitarg(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 6:
      HASH_GUARD(0x0A41DBE0830902C6LL, item) {
        return (t_item(a0));
      }
      break;
    case 9:
      HASH_GUARD(0x050681239965E069LL, getfirstchild) {
        return (t_getfirstchild());
      }
      break;
    case 12:
      HASH_GUARD(0x47D4EE3CB3EB696CLL, splitext) {
        return (t_splitext(), null);
      }
      break;
    case 13:
      HASH_GUARD(0x0621AE2D22A1922DLL, splitheading) {
        return (t_splitheading(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x5ABDC66291F1CA2ELL, getchildrenoftype) {
        return (t_getchildrenoftype(a0));
      }
      break;
    case 15:
      HASH_GUARD(0x732EC1BDA8EC520FLL, getchildren) {
        return (t_getchildren());
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 20:
      HASH_GUARD(0x41EB078EF92C44D4LL, getlength) {
        return (t_getlength());
      }
      break;
    case 25:
      HASH_GUARD(0x4CB333CF2D880119LL, getnextsibling) {
        return (t_getnextsibling());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppnode_hash_attr::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppnode_hash_attr$os_get(const char *s) {
  return c_ppnode_hash_attr::os_get(s, -1);
}
Variant &cw_ppnode_hash_attr$os_lval(const char *s) {
  return c_ppnode_hash_attr::os_lval(s, -1);
}
Variant cw_ppnode_hash_attr$os_constant(const char *s) {
  return c_ppnode_hash_attr::os_constant(s);
}
Variant cw_ppnode_hash_attr$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppnode_hash_attr::os_invoke(c, s, params, -1, fatal);
}
void c_ppnode_hash_attr::init() {
  m_name = null;
  m_value = null;
  m_nextSibling = null;
}
/* SRC: Preprocessor_Hash.php line 1594 */
void c_ppnode_hash_attr::t___construct(Variant v_name, Variant v_value) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_name = v_name);
  (m_value = v_value);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 1599 */
String c_ppnode_hash_attr::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::__toString);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  return LINE(1600,(assignCallTemp(eo_1, toString(m_name)),assignCallTemp(eo_3, x_htmlspecialchars(toString(m_value))),assignCallTemp(eo_4, concat3("</@", toString(m_name), ">")),concat5("<@", eo_1, ">", eo_3, eo_4)));
} /* function */
/* SRC: Preprocessor_Hash.php line 1603 */
Variant c_ppnode_hash_attr::t_getname() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::getName);
  return m_name;
} /* function */
/* SRC: Preprocessor_Hash.php line 1607 */
Variant c_ppnode_hash_attr::t_getnextsibling() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::getNextSibling);
  return m_nextSibling;
} /* function */
/* SRC: Preprocessor_Hash.php line 1611 */
bool c_ppnode_hash_attr::t_getchildren() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::getChildren);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1612 */
bool c_ppnode_hash_attr::t_getfirstchild() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::getFirstChild);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1613 */
bool c_ppnode_hash_attr::t_getchildrenoftype(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::getChildrenOfType);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1614 */
bool c_ppnode_hash_attr::t_getlength() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::getLength);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1615 */
bool c_ppnode_hash_attr::t_item(CVarRef v_i) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::item);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1616 */
void c_ppnode_hash_attr::t_splitarg() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::splitArg);
  throw_exception(LINE(1616,create_object("mwexception", Array(ArrayInit(1).set(0, "PPNode_Hash_Attr::splitArg: not supported").create()))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1617 */
void c_ppnode_hash_attr::t_splitext() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::splitExt);
  throw_exception(LINE(1617,create_object("mwexception", Array(ArrayInit(1).set(0, "PPNode_Hash_Attr::splitExt: not supported").create()))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1618 */
void c_ppnode_hash_attr::t_splitheading() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Attr, PPNode_Hash_Attr::splitHeading);
  throw_exception(LINE(1618,create_object("mwexception", Array(ArrayInit(1).set(0, "PPNode_Hash_Attr::splitHeading: not supported").create()))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1184 */
Variant c_pptemplateframe_hash::os_get(const char *s, int64 hash) {
  return c_ppframe_hash::os_get(s, hash);
}
Variant &c_pptemplateframe_hash::os_lval(const char *s, int64 hash) {
  return c_ppframe_hash::os_lval(s, hash);
}
void c_pptemplateframe_hash::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("numberedArgs", m_numberedArgs.isReferenced() ? ref(m_numberedArgs) : m_numberedArgs));
  props.push_back(NEW(ArrayElement)("namedArgs", m_namedArgs.isReferenced() ? ref(m_namedArgs) : m_namedArgs));
  props.push_back(NEW(ArrayElement)("parent", m_parent.isReferenced() ? ref(m_parent) : m_parent));
  props.push_back(NEW(ArrayElement)("numberedExpansionCache", m_numberedExpansionCache.isReferenced() ? ref(m_numberedExpansionCache) : m_numberedExpansionCache));
  props.push_back(NEW(ArrayElement)("namedExpansionCache", m_namedExpansionCache.isReferenced() ? ref(m_namedExpansionCache) : m_namedExpansionCache));
  c_ppframe_hash::o_get(props);
}
bool c_pptemplateframe_hash::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 5:
      HASH_EXISTS_STRING(0x21A0BD85B8757635LL, numberedExpansionCache, 22);
      break;
    case 8:
      HASH_EXISTS_STRING(0x4C84B2F1BC8BA768LL, namedExpansionCache, 19);
      break;
    case 9:
      HASH_EXISTS_STRING(0x00263ABC07EFFD29LL, numberedArgs, 12);
      break;
    case 10:
      HASH_EXISTS_STRING(0x3F849A2980280C0ALL, namedArgs, 9);
      break;
    case 12:
      HASH_EXISTS_STRING(0x16E2F26FFB10FD8CLL, parent, 6);
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_exists(s, hash);
}
Variant c_pptemplateframe_hash::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 5:
      HASH_RETURN_STRING(0x21A0BD85B8757635LL, m_numberedExpansionCache,
                         numberedExpansionCache, 22);
      break;
    case 8:
      HASH_RETURN_STRING(0x4C84B2F1BC8BA768LL, m_namedExpansionCache,
                         namedExpansionCache, 19);
      break;
    case 9:
      HASH_RETURN_STRING(0x00263ABC07EFFD29LL, m_numberedArgs,
                         numberedArgs, 12);
      break;
    case 10:
      HASH_RETURN_STRING(0x3F849A2980280C0ALL, m_namedArgs,
                         namedArgs, 9);
      break;
    case 12:
      HASH_RETURN_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                         parent, 6);
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_get(s, hash);
}
Variant c_pptemplateframe_hash::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 5:
      HASH_SET_STRING(0x21A0BD85B8757635LL, m_numberedExpansionCache,
                      numberedExpansionCache, 22);
      break;
    case 8:
      HASH_SET_STRING(0x4C84B2F1BC8BA768LL, m_namedExpansionCache,
                      namedExpansionCache, 19);
      break;
    case 9:
      HASH_SET_STRING(0x00263ABC07EFFD29LL, m_numberedArgs,
                      numberedArgs, 12);
      break;
    case 10:
      HASH_SET_STRING(0x3F849A2980280C0ALL, m_namedArgs,
                      namedArgs, 9);
      break;
    case 12:
      HASH_SET_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                      parent, 6);
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_set(s, hash, v, forInit);
}
Variant &c_pptemplateframe_hash::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 5:
      HASH_RETURN_STRING(0x21A0BD85B8757635LL, m_numberedExpansionCache,
                         numberedExpansionCache, 22);
      break;
    case 8:
      HASH_RETURN_STRING(0x4C84B2F1BC8BA768LL, m_namedExpansionCache,
                         namedExpansionCache, 19);
      break;
    case 9:
      HASH_RETURN_STRING(0x00263ABC07EFFD29LL, m_numberedArgs,
                         numberedArgs, 12);
      break;
    case 10:
      HASH_RETURN_STRING(0x3F849A2980280C0ALL, m_namedArgs,
                         namedArgs, 9);
      break;
    case 12:
      HASH_RETURN_STRING(0x16E2F26FFB10FD8CLL, m_parent,
                         parent, 6);
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_lval(s, hash);
}
Variant c_pptemplateframe_hash::os_constant(const char *s) {
  return c_ppframe_hash::os_constant(s);
}
IMPLEMENT_CLASS(pptemplateframe_hash)
ObjectData *c_pptemplateframe_hash::create(Variant v_preprocessor, Variant v_parent //  = false
, Variant v_numberedArgs //  = ScalarArrays::sa_[0]
, Variant v_namedArgs //  = ScalarArrays::sa_[0]
, Variant v_title //  = false
) {
  init();
  t___construct(v_preprocessor, v_parent, v_numberedArgs, v_namedArgs, v_title);
  return this;
}
ObjectData *c_pptemplateframe_hash::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    if (count == 3) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    if (count == 4) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
  } else return this;
}
void c_pptemplateframe_hash::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 1) (t___construct(params.rvalAt(0)));
  if (count == 2) (t___construct(params.rvalAt(0), params.rvalAt(1)));
  if (count == 3) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  if (count == 4) (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
}
ObjectData *c_pptemplateframe_hash::cloneImpl() {
  c_pptemplateframe_hash *obj = NEW(c_pptemplateframe_hash)();
  cloneSet(obj);
  return obj;
}
void c_pptemplateframe_hash::cloneSet(c_pptemplateframe_hash *clone) {
  clone->m_numberedArgs = m_numberedArgs.isReferenced() ? ref(m_numberedArgs) : m_numberedArgs;
  clone->m_namedArgs = m_namedArgs.isReferenced() ? ref(m_namedArgs) : m_namedArgs;
  clone->m_parent = m_parent.isReferenced() ? ref(m_parent) : m_parent;
  clone->m_numberedExpansionCache = m_numberedExpansionCache.isReferenced() ? ref(m_numberedExpansionCache) : m_numberedExpansionCache;
  clone->m_namedExpansionCache = m_namedExpansionCache.isReferenced() ? ref(m_namedExpansionCache) : m_namedExpansionCache;
  c_ppframe_hash::cloneSet(clone);
}
Variant c_pptemplateframe_hash::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        int count = params.size();
        if (count <= 3) return (t_virtualbracketedimplode(count, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        return (t_virtualbracketedimplode(count,params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.slice(3, count - 3, false)));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        int count = params.size();
        if (count <= 1) return (t_expand(params.rvalAt(0)));
        return (t_expand(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        int count = params.size();
        if (count <= 2) return (t_implodewithflags(count, params.rvalAt(0), params.rvalAt(1)));
        return (t_implodewithflags(count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        int count = params.size();
        if (count <= 1) return (t_virtualimplode(count, params.rvalAt(0)));
        return (t_virtualimplode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        int count = params.size();
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(params.rvalAt(0)));
        return (t_newchild(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        int count = params.size();
        if (count <= 1) return (t_implode(count, params.rvalAt(0)));
        return (t_implode(count,params.rvalAt(0), params.slice(1, count - 1, false)));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(params.rvalAt(0)));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 1) return (t___construct(params.rvalAt(0)), null);
        if (count == 2) return (t___construct(params.rvalAt(0), params.rvalAt(1)), null);
        if (count == 3) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
        if (count == 4) return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)), null);
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
      }
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_invoke(s, params, hash, fatal);
}
Variant c_pptemplateframe_hash::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 8:
      HASH_GUARD(0x6359F42D5FC265E8LL, isempty) {
        return (t_isempty());
      }
      break;
    case 11:
      HASH_GUARD(0x409618A98590618BLL, virtualbracketedimplode) {
        if (count <= 3) return (t_virtualbracketedimplode(count, a0, a1, a2));
        Array params;
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualbracketedimplode(count,a0, a1, a2, params));
      }
      break;
    case 13:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        if (count <= 1) return (t_expand(a0));
        return (t_expand(a0, a1));
      }
      break;
    case 14:
      HASH_GUARD(0x5047BA794DE2746ELL, implodewithflags) {
        if (count <= 2) return (t_implodewithflags(count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implodewithflags(count,a0, a1, params));
      }
      break;
    case 16:
      HASH_GUARD(0x7EF27F050E4E0390LL, getnamedarguments) {
        return (t_getnamedarguments());
      }
      break;
    case 17:
      HASH_GUARD(0x1E2D3AE762AF0AF1LL, virtualimplode) {
        if (count <= 1) return (t_virtualimplode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_virtualimplode(count,a0, params));
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 23:
      HASH_GUARD(0x3685C90A1EB568B7LL, getarguments) {
        return (t_getarguments());
      }
      break;
    case 24:
      HASH_GUARD(0x0A95A4780D41F7B8LL, getargument) {
        return (t_getargument(a0));
      }
      break;
    case 25:
      HASH_GUARD(0x7CBE9654ADB6CFB9LL, istemplate) {
        return (t_istemplate());
      }
      break;
    case 28:
      HASH_GUARD(0x7F4AB942B9CDE1BCLL, newchild) {
        if (count <= 0) return (t_newchild());
        if (count == 1) return (t_newchild(a0));
        return (t_newchild(a0, a1));
      }
      break;
    case 29:
      HASH_GUARD(0x7F29FBB1CA49733DLL, getnumberedarguments) {
        return (t_getnumberedarguments());
      }
      break;
    case 30:
      HASH_GUARD(0x36A80B48E08B753ELL, implode) {
        if (count <= 1) return (t_implode(count, a0));
        Array params;
        if (count >= 2) params.append(a1);
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (t_implode(count,a0, params));
      }
      break;
    case 31:
      HASH_GUARD(0x7BA9911BE4A4175FLL, loopcheck) {
        return (t_loopcheck(a0));
      }
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 1) return (t___construct(a0), null);
        if (count == 2) return (t___construct(a0, a1), null);
        if (count == 3) return (t___construct(a0, a1, a2), null);
        if (count == 4) return (t___construct(a0, a1, a2, a3), null);
        return (t___construct(a0, a1, a2, a3, a4), null);
      }
      break;
    default:
      break;
  }
  return c_ppframe_hash::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_pptemplateframe_hash::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ppframe_hash::os_invoke(c, s, params, hash, fatal);
}
Variant cw_pptemplateframe_hash$os_get(const char *s) {
  return c_pptemplateframe_hash::os_get(s, -1);
}
Variant &cw_pptemplateframe_hash$os_lval(const char *s) {
  return c_pptemplateframe_hash::os_lval(s, -1);
}
Variant cw_pptemplateframe_hash$os_constant(const char *s) {
  return c_pptemplateframe_hash::os_constant(s);
}
Variant cw_pptemplateframe_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_pptemplateframe_hash::os_invoke(c, s, params, -1, fatal);
}
void c_pptemplateframe_hash::init() {
  c_ppframe_hash::init();
  m_numberedArgs = null;
  m_namedArgs = null;
  m_parent = null;
  m_numberedExpansionCache = null;
  m_namedExpansionCache = null;
}
/* SRC: Preprocessor_Hash.php line 1188 */
void c_pptemplateframe_hash::t___construct(Variant v_preprocessor, Variant v_parent //  = false
, Variant v_numberedArgs //  = ScalarArrays::sa_[0]
, Variant v_namedArgs //  = ScalarArrays::sa_[0]
, Variant v_title //  = false
) {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_Hash, PPTemplateFrame_Hash::__construct);
  bool oldInCtor = gasInCtor(true);
  Variant v_pdbk;

  LINE(1189,c_ppframe_hash::t___construct(v_preprocessor));
  (m_parent = v_parent);
  (m_numberedArgs = v_numberedArgs);
  (m_namedArgs = v_namedArgs);
  (m_title = v_title);
  (v_pdbk = toBoolean(v_title) ? ((Variant)(LINE(1194,v_title.o_invoke_few_args("getPrefixedDBkey", 0x3E327BDC66DECDABLL, 0)))) : ((Variant)(false)));
  (m_titleCache = v_parent.o_get("titleCache", 0x0CD4D2916BE2CE13LL));
  m_titleCache.append((v_pdbk));
  (m_loopCheckHash = v_parent.o_get("loopCheckHash", 0x757439BA20184735LL));
  if (!same(v_pdbk, false)) {
    m_loopCheckHash.set(v_pdbk, (true));
  }
  (m_depth = v_parent.o_get("depth", 0x1AE700EECE6274C7LL) + 1LL);
  (m_numberedExpansionCache = (m_namedExpansionCache = ScalarArrays::sa_[0]));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 1205 */
String c_pptemplateframe_hash::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_Hash, PPTemplateFrame_Hash::__toString);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  String v_s;
  bool v_first = false;
  Variant v_args;
  Primitive v_name = 0;
  Variant v_value;

  (v_s = "tplframe{");
  (v_first = true);
  (v_args = m_numberedArgs + m_namedArgs);
  {
    LOOP_COUNTER(52);
    for (ArrayIterPtr iter54 = v_args.begin("pptemplateframe_hash"); !iter54->end(); iter54->next()) {
      LOOP_COUNTER_CHECK(52);
      v_value = iter54->second();
      v_name = iter54->first();
      {
        if (v_first) {
          (v_first = false);
        }
        else {
          concat_assign(v_s, ", ");
        }
        concat_assign(v_s, LINE(1216,(assignCallTemp(eo_1, toString(v_name)),assignCallTemp(eo_3, toString((assignCallTemp(eo_7, v_value.o_invoke_few_args("__toString", 0x642C2D2994B34A13LL, 0)),x_str_replace("\"", "\\\"", eo_7)))),concat5("\"", eo_1, "\":\"", eo_3, "\""))));
      }
    }
  }
  concat_assign(v_s, "}");
  return v_s;
} /* function */
/* SRC: Preprocessor_Hash.php line 1224 */
bool c_pptemplateframe_hash::t_isempty() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_Hash, PPTemplateFrame_Hash::isEmpty);
  return !(toBoolean(LINE(1225,x_count(m_numberedArgs)))) && !(toBoolean(x_count(m_namedArgs)));
} /* function */
/* SRC: Preprocessor_Hash.php line 1228 */
Variant c_pptemplateframe_hash::t_getarguments() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_Hash, PPTemplateFrame_Hash::getArguments);
  Variant eo_0;
  Variant eo_1;
  Variant v_arguments;
  Variant v_key;

  (v_arguments = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(55);
    Variant map56 = LINE(1232,(assignCallTemp(eo_0, LINE(1231,x_array_keys(m_numberedArgs))),assignCallTemp(eo_1, LINE(1232,x_array_keys(m_namedArgs))),x_array_merge(2, eo_0, Array(ArrayInit(1).set(0, eo_1).create()))));
    for (ArrayIterPtr iter57 = map56.begin("pptemplateframe_hash"); !iter57->end(); iter57->next()) {
      LOOP_COUNTER_CHECK(55);
      v_key = iter57->second();
      {
        v_arguments.set(v_key, (LINE(1233,o_root_invoke_few_args("getArgument", 0x0A95A4780D41F7B8LL, 1, v_key))));
      }
    }
  }
  return v_arguments;
} /* function */
/* SRC: Preprocessor_Hash.php line 1238 */
Variant c_pptemplateframe_hash::t_getnumberedarguments() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_Hash, PPTemplateFrame_Hash::getNumberedArguments);
  Variant v_arguments;
  Variant v_key;

  (v_arguments = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(58);
    Variant map59 = LINE(1240,x_array_keys(m_numberedArgs));
    for (ArrayIterPtr iter60 = map59.begin("pptemplateframe_hash"); !iter60->end(); iter60->next()) {
      LOOP_COUNTER_CHECK(58);
      v_key = iter60->second();
      {
        v_arguments.set(v_key, (LINE(1241,o_root_invoke_few_args("getArgument", 0x0A95A4780D41F7B8LL, 1, v_key))));
      }
    }
  }
  return v_arguments;
} /* function */
/* SRC: Preprocessor_Hash.php line 1246 */
Variant c_pptemplateframe_hash::t_getnamedarguments() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_Hash, PPTemplateFrame_Hash::getNamedArguments);
  Variant v_arguments;
  Variant v_key;

  (v_arguments = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(61);
    Variant map62 = LINE(1248,x_array_keys(m_namedArgs));
    for (ArrayIterPtr iter63 = map62.begin("pptemplateframe_hash"); !iter63->end(); iter63->next()) {
      LOOP_COUNTER_CHECK(61);
      v_key = iter63->second();
      {
        v_arguments.set(v_key, (LINE(1249,o_root_invoke_few_args("getArgument", 0x0A95A4780D41F7B8LL, 1, v_key))));
      }
    }
  }
  return v_arguments;
} /* function */
/* SRC: Preprocessor_Hash.php line 1254 */
Variant c_pptemplateframe_hash::t_getnumberedargument(CVarRef v_index) {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_Hash, PPTemplateFrame_Hash::getNumberedArgument);
  if (!(isset(m_numberedArgs, v_index))) {
    return false;
  }
  if (!(isset(m_numberedExpansionCache, v_index))) {
    m_numberedExpansionCache.set(v_index, (LINE(1260,m_parent.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, m_numberedArgs.refvalAt(v_index), throw_fatal("unknown class constant pptemplateframe_hash::STRIP_COMMENTS")))));
  }
  return m_numberedExpansionCache.rvalAt(v_index);
} /* function */
/* SRC: Preprocessor_Hash.php line 1265 */
Variant c_pptemplateframe_hash::t_getnamedargument(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_Hash, PPTemplateFrame_Hash::getNamedArgument);
  if (!(isset(m_namedArgs, v_name))) {
    return false;
  }
  if (!(isset(m_namedExpansionCache, v_name))) {
    m_namedExpansionCache.set(v_name, (LINE(1272,x_trim(toString(m_parent.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, m_namedArgs.refvalAt(v_name), throw_fatal("unknown class constant pptemplateframe_hash::STRIP_COMMENTS")))))));
  }
  return m_namedExpansionCache.rvalAt(v_name);
} /* function */
/* SRC: Preprocessor_Hash.php line 1277 */
Variant c_pptemplateframe_hash::t_getargument(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_Hash, PPTemplateFrame_Hash::getArgument);
  Variant v_text;

  (v_text = LINE(1278,t_getnumberedargument(v_name)));
  if (same(v_text, false)) {
    (v_text = LINE(1280,t_getnamedargument(v_name)));
  }
  return v_text;
} /* function */
/* SRC: Preprocessor_Hash.php line 1288 */
bool c_pptemplateframe_hash::t_istemplate() {
  INSTANCE_METHOD_INJECTION(PPTemplateFrame_Hash, PPTemplateFrame_Hash::isTemplate);
  return true;
} /* function */
/* SRC: Preprocessor_Hash.php line 1555 */
Variant c_ppnode_hash_array::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_ppnode_hash_array::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_ppnode_hash_array::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("value", m_value.isReferenced() ? ref(m_value) : m_value));
  props.push_back(NEW(ArrayElement)("nextSibling", m_nextSibling.isReferenced() ? ref(m_nextSibling) : m_nextSibling));
  c_ObjectData::o_get(props);
}
bool c_ppnode_hash_array::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_EXISTS_STRING(0x69E7413AE0C88471LL, value, 5);
      break;
    case 2:
      HASH_EXISTS_STRING(0x0B3EC7643EE81822LL, nextSibling, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_ppnode_hash_array::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                         nextSibling, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_ppnode_hash_array::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_SET_STRING(0x69E7413AE0C88471LL, m_value,
                      value, 5);
      break;
    case 2:
      HASH_SET_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                      nextSibling, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_ppnode_hash_array::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 1:
      HASH_RETURN_STRING(0x69E7413AE0C88471LL, m_value,
                         value, 5);
      break;
    case 2:
      HASH_RETURN_STRING(0x0B3EC7643EE81822LL, m_nextSibling,
                         nextSibling, 11);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_ppnode_hash_array::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(ppnode_hash_array)
ObjectData *c_ppnode_hash_array::create(Variant v_value) {
  init();
  t___construct(v_value);
  return this;
}
ObjectData *c_ppnode_hash_array::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_ppnode_hash_array::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_ppnode_hash_array::cloneImpl() {
  c_ppnode_hash_array *obj = NEW(c_ppnode_hash_array)();
  cloneSet(obj);
  return obj;
}
void c_ppnode_hash_array::cloneSet(c_ppnode_hash_array *clone) {
  clone->m_value = m_value.isReferenced() ? ref(m_value) : m_value;
  clone->m_nextSibling = m_nextSibling.isReferenced() ? ref(m_nextSibling) : m_nextSibling;
  ObjectData::cloneSet(clone);
}
Variant c_ppnode_hash_array::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 0:
      HASH_GUARD(0x73E7C3304C0C5360LL, splitarg) {
        return (t_splitarg(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 6:
      HASH_GUARD(0x0A41DBE0830902C6LL, item) {
        return (t_item(params.rvalAt(0)));
      }
      break;
    case 9:
      HASH_GUARD(0x050681239965E069LL, getfirstchild) {
        return (t_getfirstchild());
      }
      break;
    case 12:
      HASH_GUARD(0x47D4EE3CB3EB696CLL, splitext) {
        return (t_splitext(), null);
      }
      break;
    case 13:
      HASH_GUARD(0x0621AE2D22A1922DLL, splitheading) {
        return (t_splitheading(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x5ABDC66291F1CA2ELL, getchildrenoftype) {
        return (t_getchildrenoftype(params.rvalAt(0)));
      }
      break;
    case 15:
      HASH_GUARD(0x732EC1BDA8EC520FLL, getchildren) {
        return (t_getchildren());
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 20:
      HASH_GUARD(0x41EB078EF92C44D4LL, getlength) {
        return (t_getlength());
      }
      break;
    case 25:
      HASH_GUARD(0x4CB333CF2D880119LL, getnextsibling) {
        return (t_getnextsibling());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_ppnode_hash_array::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 31) {
    case 0:
      HASH_GUARD(0x73E7C3304C0C5360LL, splitarg) {
        return (t_splitarg(), null);
      }
      break;
    case 5:
      HASH_GUARD(0x23F51CDECC198965LL, getname) {
        return (t_getname());
      }
      break;
    case 6:
      HASH_GUARD(0x0A41DBE0830902C6LL, item) {
        return (t_item(a0));
      }
      break;
    case 9:
      HASH_GUARD(0x050681239965E069LL, getfirstchild) {
        return (t_getfirstchild());
      }
      break;
    case 12:
      HASH_GUARD(0x47D4EE3CB3EB696CLL, splitext) {
        return (t_splitext(), null);
      }
      break;
    case 13:
      HASH_GUARD(0x0621AE2D22A1922DLL, splitheading) {
        return (t_splitheading(), null);
      }
      break;
    case 14:
      HASH_GUARD(0x5ABDC66291F1CA2ELL, getchildrenoftype) {
        return (t_getchildrenoftype(a0));
      }
      break;
    case 15:
      HASH_GUARD(0x732EC1BDA8EC520FLL, getchildren) {
        return (t_getchildren());
      }
      break;
    case 19:
      HASH_GUARD(0x642C2D2994B34A13LL, __tostring) {
        return (t___tostring());
      }
      break;
    case 20:
      HASH_GUARD(0x41EB078EF92C44D4LL, getlength) {
        return (t_getlength());
      }
      break;
    case 25:
      HASH_GUARD(0x4CB333CF2D880119LL, getnextsibling) {
        return (t_getnextsibling());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppnode_hash_array::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppnode_hash_array$os_get(const char *s) {
  return c_ppnode_hash_array::os_get(s, -1);
}
Variant &cw_ppnode_hash_array$os_lval(const char *s) {
  return c_ppnode_hash_array::os_lval(s, -1);
}
Variant cw_ppnode_hash_array$os_constant(const char *s) {
  return c_ppnode_hash_array::os_constant(s);
}
Variant cw_ppnode_hash_array$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppnode_hash_array::os_invoke(c, s, params, -1, fatal);
}
void c_ppnode_hash_array::init() {
  m_value = null;
  m_nextSibling = null;
}
/* SRC: Preprocessor_Hash.php line 1558 */
void c_ppnode_hash_array::t___construct(Variant v_value) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_value = v_value);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 1562 */
String c_ppnode_hash_array::t___tostring() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::__toString);
  return toString(LINE(1563,x_var_export(((Object)(this)), true)));
} /* function */
/* SRC: Preprocessor_Hash.php line 1566 */
int c_ppnode_hash_array::t_getlength() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::getLength);
  return LINE(1567,x_count(m_value));
} /* function */
/* SRC: Preprocessor_Hash.php line 1570 */
Variant c_ppnode_hash_array::t_item(CVarRef v_i) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::item);
  return m_value.rvalAt(v_i);
} /* function */
/* SRC: Preprocessor_Hash.php line 1574 */
String c_ppnode_hash_array::t_getname() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::getName);
  return "#nodelist";
} /* function */
/* SRC: Preprocessor_Hash.php line 1576 */
Variant c_ppnode_hash_array::t_getnextsibling() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::getNextSibling);
  return m_nextSibling;
} /* function */
/* SRC: Preprocessor_Hash.php line 1580 */
bool c_ppnode_hash_array::t_getchildren() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::getChildren);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1581 */
bool c_ppnode_hash_array::t_getfirstchild() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::getFirstChild);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1582 */
bool c_ppnode_hash_array::t_getchildrenoftype(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::getChildrenOfType);
  return false;
} /* function */
/* SRC: Preprocessor_Hash.php line 1583 */
void c_ppnode_hash_array::t_splitarg() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::splitArg);
  throw_exception(LINE(1583,create_object("mwexception", Array(ArrayInit(1).set(0, "PPNode_Hash_Array::splitArg: not supported").create()))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1584 */
void c_ppnode_hash_array::t_splitext() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::splitExt);
  throw_exception(LINE(1584,create_object("mwexception", Array(ArrayInit(1).set(0, "PPNode_Hash_Array::splitExt: not supported").create()))));
} /* function */
/* SRC: Preprocessor_Hash.php line 1585 */
void c_ppnode_hash_array::t_splitheading() {
  INSTANCE_METHOD_INJECTION(PPNode_Hash_Array, PPNode_Hash_Array::splitHeading);
  throw_exception(LINE(1585,create_object("mwexception", Array(ArrayInit(1).set(0, "PPNode_Hash_Array::splitHeading: not supported").create()))));
} /* function */
/* SRC: Preprocessor_Hash.php line 677 */
Variant c_ppdstackelement_hash::os_get(const char *s, int64 hash) {
  return c_ppdstackelement::os_get(s, hash);
}
Variant &c_ppdstackelement_hash::os_lval(const char *s, int64 hash) {
  return c_ppdstackelement::os_lval(s, hash);
}
void c_ppdstackelement_hash::o_get(ArrayElementVec &props) const {
  c_ppdstackelement::o_get(props);
}
bool c_ppdstackelement_hash::o_exists(CStrRef s, int64 hash) const {
  return c_ppdstackelement::o_exists(s, hash);
}
Variant c_ppdstackelement_hash::o_get(CStrRef s, int64 hash) {
  return c_ppdstackelement::o_get(s, hash);
}
Variant c_ppdstackelement_hash::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ppdstackelement::o_set(s, hash, v, forInit);
}
Variant &c_ppdstackelement_hash::o_lval(CStrRef s, int64 hash) {
  return c_ppdstackelement::o_lval(s, hash);
}
Variant c_ppdstackelement_hash::os_constant(const char *s) {
  return c_ppdstackelement::os_constant(s);
}
IMPLEMENT_CLASS(ppdstackelement_hash)
ObjectData *c_ppdstackelement_hash::create(Variant v_data //  = ScalarArrays::sa_[0]
) {
  init();
  t___construct(v_data);
  return this;
}
ObjectData *c_ppdstackelement_hash::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_ppdstackelement_hash::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_ppdstackelement_hash::cloneImpl() {
  c_ppdstackelement_hash *obj = NEW(c_ppdstackelement_hash)();
  cloneSet(obj);
  return obj;
}
void c_ppdstackelement_hash::cloneSet(c_ppdstackelement_hash *clone) {
  c_ppdstackelement::cloneSet(clone);
}
Variant c_ppdstackelement_hash::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x27E7DBA875AD17E1LL, getflags) {
        return (t_getflags());
      }
      break;
    case 2:
      HASH_GUARD(0x499E72B719A4CAC2LL, addpart) {
        int count = params.size();
        if (count <= 0) return (t_addpart(), null);
        return (t_addpart(params.rvalAt(0)), null);
      }
      break;
    case 6:
      HASH_GUARD(0x1A5F5A2CB292E516LL, getaccum) {
        return ref(t_getaccum());
      }
      break;
    case 9:
      HASH_GUARD(0x1CF88FA5EFC88C39LL, breaksyntax) {
        int count = params.size();
        if (count <= 0) return (t_breaksyntax());
        return (t_breaksyntax(params.rvalAt(0)));
      }
      break;
    case 12:
      HASH_GUARD(0x1726802D706D7BECLL, getcurrentpart) {
        return (t_getcurrentpart());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_ppdstackelement::o_invoke(s, params, hash, fatal);
}
Variant c_ppdstackelement_hash::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 1:
      HASH_GUARD(0x27E7DBA875AD17E1LL, getflags) {
        return (t_getflags());
      }
      break;
    case 2:
      HASH_GUARD(0x499E72B719A4CAC2LL, addpart) {
        if (count <= 0) return (t_addpart(), null);
        return (t_addpart(a0), null);
      }
      break;
    case 6:
      HASH_GUARD(0x1A5F5A2CB292E516LL, getaccum) {
        return ref(t_getaccum());
      }
      break;
    case 9:
      HASH_GUARD(0x1CF88FA5EFC88C39LL, breaksyntax) {
        if (count <= 0) return (t_breaksyntax());
        return (t_breaksyntax(a0));
      }
      break;
    case 12:
      HASH_GUARD(0x1726802D706D7BECLL, getcurrentpart) {
        return (t_getcurrentpart());
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_ppdstackelement::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_ppdstackelement_hash::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ppdstackelement::os_invoke(c, s, params, hash, fatal);
}
Variant cw_ppdstackelement_hash$os_get(const char *s) {
  return c_ppdstackelement_hash::os_get(s, -1);
}
Variant &cw_ppdstackelement_hash$os_lval(const char *s) {
  return c_ppdstackelement_hash::os_lval(s, -1);
}
Variant cw_ppdstackelement_hash$os_constant(const char *s) {
  return c_ppdstackelement_hash::os_constant(s);
}
Variant cw_ppdstackelement_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_ppdstackelement_hash::os_invoke(c, s, params, -1, fatal);
}
void c_ppdstackelement_hash::init() {
  c_ppdstackelement::init();
}
/* SRC: Preprocessor_Hash.php line 678 */
void c_ppdstackelement_hash::t___construct(Variant v_data //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(PPDStackElement_Hash, PPDStackElement_Hash::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_partClass = "PPDPart_Hash");
  LINE(680,c_ppdstackelement::t___construct(v_data));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Preprocessor_Hash.php line 686 */
Variant c_ppdstackelement_hash::t_breaksyntax(Variant v_openingCount //  = false
) {
  INSTANCE_METHOD_INJECTION(PPDStackElement_Hash, PPDStackElement_Hash::breakSyntax);
  Variant v_accum;
  bool v_first = false;
  Variant v_part;

  if (equal(m_open, "\n")) {
    (v_accum = m_parts.rvalAt(0LL, 0x77CFA1EEF01BCA90LL).o_get("out", 0x7C801AC012E9F722LL));
  }
  else {
    if (same(v_openingCount, false)) {
      (v_openingCount = m_count);
    }
    (v_accum = ((Object)(LINE(693,p_ppdaccum_hash(p_ppdaccum_hash(NEWOBJ(c_ppdaccum_hash)())->create())))));
    LINE(694,v_accum.o_invoke_few_args("addLiteral", 0x05AAC40C7DAD4ACELL, 1, x_str_repeat(toString(m_open), toInt32(v_openingCount))));
    (v_first = true);
    {
      LOOP_COUNTER(64);
      Variant map65 = m_parts;
      for (ArrayIterPtr iter66 = map65.begin("ppdstackelement_hash"); !iter66->end(); iter66->next()) {
        LOOP_COUNTER_CHECK(64);
        v_part = iter66->second();
        {
          if (v_first) {
            (v_first = false);
          }
          else {
            LINE(700,v_accum.o_invoke_few_args("addLiteral", 0x05AAC40C7DAD4ACELL, 1, "|"));
          }
          LINE(702,v_accum.o_invoke_few_args("addAccum", 0x6A8DCEFA6345DAC9LL, 1, v_part.o_lval("out", 0x7C801AC012E9F722LL)));
        }
      }
    }
  }
  return v_accum;
} /* function */
Object co_ppdstack_hash(CArrRef params, bool init /* = true */) {
  return Object(p_ppdstack_hash(NEW(c_ppdstack_hash)())->dynCreate(params, init));
}
Object co_ppnode_hash_text(CArrRef params, bool init /* = true */) {
  return Object(p_ppnode_hash_text(NEW(c_ppnode_hash_text)())->dynCreate(params, init));
}
Object co_ppdaccum_hash(CArrRef params, bool init /* = true */) {
  return Object(p_ppdaccum_hash(NEW(c_ppdaccum_hash)())->dynCreate(params, init));
}
Object co_ppnode_hash_tree(CArrRef params, bool init /* = true */) {
  return Object(p_ppnode_hash_tree(NEW(c_ppnode_hash_tree)())->dynCreate(params, init));
}
Object co_ppframe_hash(CArrRef params, bool init /* = true */) {
  return Object(p_ppframe_hash(NEW(c_ppframe_hash)())->dynCreate(params, init));
}
Object co_preprocessor_hash(CArrRef params, bool init /* = true */) {
  return Object(p_preprocessor_hash(NEW(c_preprocessor_hash)())->dynCreate(params, init));
}
Object co_ppdpart_hash(CArrRef params, bool init /* = true */) {
  return Object(p_ppdpart_hash(NEW(c_ppdpart_hash)())->dynCreate(params, init));
}
Object co_ppcustomframe_hash(CArrRef params, bool init /* = true */) {
  return Object(p_ppcustomframe_hash(NEW(c_ppcustomframe_hash)())->dynCreate(params, init));
}
Object co_ppnode_hash_attr(CArrRef params, bool init /* = true */) {
  return Object(p_ppnode_hash_attr(NEW(c_ppnode_hash_attr)())->dynCreate(params, init));
}
Object co_pptemplateframe_hash(CArrRef params, bool init /* = true */) {
  return Object(p_pptemplateframe_hash(NEW(c_pptemplateframe_hash)())->dynCreate(params, init));
}
Object co_ppnode_hash_array(CArrRef params, bool init /* = true */) {
  return Object(p_ppnode_hash_array(NEW(c_ppnode_hash_array)())->dynCreate(params, init));
}
Object co_ppdstackelement_hash(CArrRef params, bool init /* = true */) {
  return Object(p_ppdstackelement_hash(NEW(c_ppdstackelement_hash)())->dynCreate(params, init));
}
Variant pm_php$Preprocessor_Hash_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::Preprocessor_Hash.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$Preprocessor_Hash_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
