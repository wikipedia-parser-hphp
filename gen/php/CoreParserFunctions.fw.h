
#ifndef __GENERATED_php_CoreParserFunctions_fw_h__
#define __GENERATED_php_CoreParserFunctions_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_NS_USER;


// 2. Classes
FORWARD_DECLARE_CLASS(coreparserfunctions)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/DateFormatter.fw.h>
#include <php/Parser.fw.h>
#include <php/ParserOutput.fw.h>
#include <php/Preprocessor.fw.h>

#endif // __GENERATED_php_CoreParserFunctions_fw_h__
