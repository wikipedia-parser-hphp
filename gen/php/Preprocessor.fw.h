
#ifndef __GENERATED_php_Preprocessor_fw_h__
#define __GENERATED_php_Preprocessor_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(preprocessor);
FORWARD_DECLARE_CLASS(ppframe);
FORWARD_DECLARE_CLASS(ppnode);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_Preprocessor_fw_h__
