
#ifndef __GENERATED_cls_article_h__
#define __GENERATED_cls_article_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 20838 */
class c_article : virtual public ObjectData {
  BEGIN_CLASS_MAP(article)
  END_CLASS_MAP(article)
  DECLARE_CLASS(article, Article, ObjectData)
  void init();
  public: Variant m_mComment;
  public: Variant m_mContent;
  public: Variant m_mContentLoaded;
  public: Variant m_mCounter;
  public: Variant m_mCurID;
  public: Variant m_mDataLoaded;
  public: Variant m_mForUpdate;
  public: Variant m_mGoodAdjustment;
  public: Variant m_mIsRedirect;
  public: Variant m_mLatest;
  public: Variant m_mMinorEdit;
  public: Variant m_mOldId;
  public: Variant m_mPreparedEdit;
  public: Variant m_mRedirectedFrom;
  public: Variant m_mRedirectTarget;
  public: Variant m_mRedirectUrl;
  public: Variant m_mRevIdFetched;
  public: Variant m_mRevision;
  public: Variant m_mTimestamp;
  public: Variant m_mTitle;
  public: Variant m_mTotalAdjustment;
  public: Variant m_mTouched;
  public: Variant m_mUser;
  public: Variant m_mUserText;
  public: void t___construct(Variant v_title, Variant v_oldId = null);
  public: ObjectData *create(Variant v_title, Variant v_oldId = null);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static Variant ti_newfromid(const char* cls, CVarRef v_id);
  public: void t_setredirectedfrom(CVarRef v_from);
  public: Variant t_getredirecttarget();
  public: Variant t_insertredirect();
  public: Variant t_followredirect();
  public: Variant t_followredirecttext(CVarRef v_text);
  public: Variant t_gettitle();
  public: void t_clear();
  public: Variant t_getcontent();
  public: Variant t_getrawtext();
  public: Variant t_getsection(Variant v_text, Variant v_section);
  public: Variant t_getundotext(p_revision v_undo, CVarRef v_undoafter = null_variant);
  public: Variant t_getoldid();
  public: Variant t_getoldidfromrequest();
  public: void t_loadcontent();
  public: Variant t_pagedata(CVarRef v_dbr, CArrRef v_conditions);
  public: Variant t_pagedatafromtitle(p_getdb v_dbr, CVarRef v_title);
  public: Variant t_pagedatafromid(p_getdb v_dbr, CVarRef v_id);
  public: void t_loadpagedata(Variant v_data = "fromdb");
  public: Variant t_fetchcontent(CVarRef v_oldid = 0LL);
  public: Variant t_forupdate(CVarRef v_x = null_variant);
  public: p_getdb t_getdb();
  public: Variant t_getselectoptions(Variant v_options = "");
  public: Variant t_getid();
  public: bool t_exists();
  public: bool t_hasviewablecontent();
  public: Variant t_getcount();
  public: bool t_iscountable(CVarRef v_text);
  public: Variant t_isredirect(CVarRef v_text = false);
  public: bool t_iscurrent();
  public: void t_loadlastedit();
  public: Variant t_gettimestamp();
  public: Variant t_getuser();
  public: Variant t_getusertext();
  public: Variant t_getcomment();
  public: Variant t_getminoredit();
  public: Variant t_getrevidfetched();
  public: Object t_getcontributors(int64 v_limit = 0LL, int64 v_offset = 0LL);
  public: void t_view();
  public: void t_showdeletionlog();
  public: bool t_useparsercache(CVarRef v_oldid);
  public: String t_viewredirect(Variant v_target, bool v_appendSubtitle = true, bool v_forceKnown = false);
  public: void t_addtrackbacks();
  public: void t_deletetrackback();
  public: void t_render();
  public: void t_purge();
  public: void t_dopurge();
  public: Variant t_inserton(CVarRef v_dbw);
  public: Variant t_updaterevisionon(Variant v_dbw, CVarRef v_revision, CVarRef v_lastRevision = null_variant, CVarRef v_lastRevIsRedirect = null_variant);
  public: bool t_updateredirecton(Variant v_dbw, Object v_redirectTitle, CVarRef v_lastRevIsRedirect = null_variant);
  public: Variant t_updateifneweron(Variant v_dbw, Object v_revision);
  public: Variant t_replacesection(Variant v_section, Variant v_text, CVarRef v_summary = "", CVarRef v_edittime = null_variant);
  public: void t_insertnewarticle(Variant v_text, CStrRef v_summary, bool v_isminor, bool v_watchthis, bool v_suppressRC = false, bool v_comment = false, bool v_bot = false);
  public: bool t_updatearticle(CVarRef v_text, CVarRef v_summary, CVarRef v_minor, CVarRef v_watchthis, bool v_forceBot = false, Variant v_sectionanchor = "");
  public: Variant t_doedit(Variant v_text, Variant v_summary, Variant v_flags = 0LL, CVarRef v_baseRevId = false, Variant v_user = null);
  public: void t_showarticle(CVarRef v_text, CVarRef v_subtitle, CStrRef v_sectionanchor = "", CVarRef v_me2 = null_variant, CVarRef v_now = null_variant, CVarRef v_summary = null_variant, CVarRef v_oldid = null_variant);
  public: void t_doredirect(CVarRef v_noRedir = false, CVarRef v_sectionAnchor = "", CVarRef v_extraQuery = "");
  public: void t_markpatrolled();
  public: void t_watch();
  public: bool t_dowatch();
  public: void t_unwatch();
  public: bool t_dounwatch();
  public: void t_protect();
  public: void t_unprotect();
  public: bool t_updaterestrictions(CArrRef v_limit = ScalarArrays::sa_[0], Variant v_reason = "", Variant v_cascade = 0LL, Variant v_expiry = ScalarArrays::sa_[0]);
  public: static String ti_flattenrestrictions(const char* cls, Variant v_limit);
  public: Variant t_generatereason(Variant v_hasHistory);
  public: Variant t_delete();
  public: bool t_isbigdeletion();
  public: Variant t_estimaterevisioncount();
  public: Array t_getlastnauthors(CVarRef v_num, int64 v_revLatest = 0LL);
  public: void t_confirmdelete(CVarRef v_reason);
  public: void t_dodelete(Variant v_reason, bool v_suppress = false);
  public: bool t_dodeletearticle(Variant v_reason, bool v_suppress = false, Variant v_id = 0LL);
  public: Variant t_dorollback(CVarRef v_fromP, CVarRef v_summary, Variant v_token, CVarRef v_bot, Variant v_resultDetails);
  public: Array t_commitrollback(CVarRef v_fromP, Variant v_summary, CVarRef v_bot, Variant v_resultDetails);
  public: void t_rollback();
  public: void t_viewupdates();
  public: Variant t_preparetextforedit(CVarRef v_text, Variant v_revid = null);
  public: void t_editupdates(Variant v_text, CVarRef v_summary, CVarRef v_minoredit, CVarRef v_timestamp_of_pagechange, CVarRef v_newid, bool v_changed = true);
  public: void t_createupdates(Object v_rev);
  public: void t_setoldsubtitle(Variant v_oldid = 0LL);
  public: Variant t_presavetransform(Variant v_text);
  public: bool t_tryfilecache();
  public: bool t_isfilecacheable();
  public: bool t_checktouched();
  public: Variant t_gettouched();
  public: Variant t_getlatest();
  public: void t_quickedit(CVarRef v_text, CStrRef v_comment = "", int64 v_minor = 0LL);
  public: static void ti_incviewcount(const char* cls, Variant v_id);
  public: static void ti_onarticlecreate(const char* cls, CVarRef v_title);
  public: static void ti_onarticledelete(const char* cls, Variant v_title);
  public: static void ti_onarticleedit(const char* cls, CVarRef v_title, CStrRef v_flags = "");
  public: void t_revert();
  public: void t_info();
  public: Variant t_pagecountinfo(CVarRef v_title);
  public: Array t_getusedtemplates();
  public: Array t_gethiddencategories();
  public: static Variant ti_getautosummary(const char* cls, CVarRef v_oldtext, Variant v_newtext, CVarRef v_flags);
  public: void t_outputwikitext(Variant v_text, bool v_cache = true);
  public: void t_updatecategorycounts(CVarRef v_added, CVarRef v_deleted);
  public: static Variant t_newfromid(CVarRef v_id) { return ti_newfromid("article", v_id); }
  public: static Variant t_getautosummary(CVarRef v_oldtext, CVarRef v_newtext, CVarRef v_flags) { return ti_getautosummary("article", v_oldtext, v_newtext, v_flags); }
  public: static String t_flattenrestrictions(CVarRef v_limit) { return ti_flattenrestrictions("article", v_limit); }
  public: static void t_incviewcount(CVarRef v_id) { ti_incviewcount("article", v_id); }
  public: static void t_onarticlecreate(CVarRef v_title) { ti_onarticlecreate("article", v_title); }
  public: static void t_onarticledelete(CVarRef v_title) { ti_onarticledelete("article", v_title); }
  public: static void t_onarticleedit(CVarRef v_title, CStrRef v_flags = "") { ti_onarticleedit("article", v_title, v_flags); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_article_h__
