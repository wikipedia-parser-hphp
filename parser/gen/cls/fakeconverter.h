
#ifndef __GENERATED_cls_fakeconverter_h__
#define __GENERATED_cls_fakeconverter_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 11853 */
class c_fakeconverter : virtual public ObjectData {
  BEGIN_CLASS_MAP(fakeconverter)
  END_CLASS_MAP(fakeconverter)
  DECLARE_CLASS(fakeconverter, FakeConverter, ObjectData)
  void init();
  public: Variant m_mLang;
  public: void t_fakeconverter(p_language v_langobj);
  public: ObjectData *create(p_language v_langobj);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: Variant t_convert(CVarRef v_t, CVarRef v_i);
  public: Variant t_parserconvert(CVarRef v_t, CVarRef v_p);
  public: Array t_getvariants();
  public: Variant t_getpreferredvariant();
  public: void t_findvariantlink(Variant v_l, Variant v_n, CVarRef v_ignoreOtherCond = false);
  public: String t_getextrahashoptions();
  public: String t_getparsedtitle();
  public: Variant t_marknoconversion(CVarRef v_text, CVarRef v_noParse = false);
  public: Variant t_convertcategorykey(CVarRef v_key);
  public: Array t_convertlinktoallvariants(CVarRef v_text);
  public: Variant t_armourmath(CVarRef v_text);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_fakeconverter_h__
