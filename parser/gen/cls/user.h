
#ifndef __GENERATED_cls_user_h__
#define __GENERATED_cls_user_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 7479 */
class c_user : virtual public ObjectData {
  BEGIN_CLASS_MAP(user)
  END_CLASS_MAP(user)
  DECLARE_CLASS(user, User, ObjectData)
  void init();
  public: Variant m_mId;
  public: Variant m_mName;
  public: Variant m_mRealName;
  public: Variant m_mPassword;
  public: Variant m_mNewpassword;
  public: Variant m_mNewpassTime;
  public: Variant m_mEmail;
  public: Variant m_mOptions;
  public: Variant m_mTouched;
  public: Variant m_mToken;
  public: Variant m_mEmailAuthenticated;
  public: Variant m_mEmailToken;
  public: Variant m_mEmailTokenExpires;
  public: Variant m_mRegistration;
  public: Variant m_mGroups;
  public: Variant m_mDataLoaded;
  public: Variant m_mAuthLoaded;
  public: Variant m_mFrom;
  public: Variant m_mNewtalk;
  public: Variant m_mDatePreference;
  public: Variant m_mBlockedby;
  public: Variant m_mHash;
  public: Variant m_mSkin;
  public: Variant m_mRights;
  public: Variant m_mBlockreason;
  public: Variant m_mBlock;
  public: Variant m_mEffectiveGroups;
  public: Variant m_mBlockedGlobally;
  public: Variant m_mLocked;
  public: Variant m_mHideName;
  public: void t_user();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_load();
  public: bool t_loadfromid();
  public: void t_savetocache();
  public: static Variant ti_newfromname(const char* cls, Variant v_name, Variant v_validate = "valid");
  public: static p_user ti_newfromid(const char* cls, CVarRef v_id);
  public: static Variant ti_newfromconfirmationcode(const char* cls, CVarRef v_code);
  public: static p_user ti_newfromsession(const char* cls);
  public: static p_user ti_newfromrow(const char* cls, CVarRef v_row);
  public: static Variant ti_whois(const char* cls, CVarRef v_id);
  public: static Variant ti_whoisreal(const char* cls, CVarRef v_id);
  public: static Variant ti_idfromname(const char* cls, CVarRef v_name);
  public: static bool ti_isip(const char* cls, CVarRef v_name);
  public: static bool ti_isvalidusername(const char* cls, Variant v_name);
  public: static bool ti_isusablename(const char* cls, CVarRef v_name);
  public: static bool ti_iscreatablename(const char* cls, CVarRef v_name);
  public: Variant t_isvalidpassword(Variant v_password);
  public: static Variant ti_isvalidemailaddr(const char* cls, CVarRef v_addr);
  public: static Variant ti_getcanonicalname(const char* cls, Variant v_name, CVarRef v_validate = "valid");
  public: static Variant ti_edits(const char* cls, CVarRef v_uid);
  public: static String ti_randompassword(const char* cls);
  public: void t_loaddefaults(CVarRef v_name = false);
  public: void t_setupsession();
  public: Variant t_loadfromsession();
  public: Variant t_loadfromdatabase();
  public: void t_loadfromrow(CVarRef v_row);
  public: void t_loadgroups();
  public: void t_clearinstancecache(CVarRef v_reloadFrom = false);
  public: static Variant ti_getdefaultoptions(const char* cls);
  public: static Variant ti_getdefaultoption(const char* cls, CStrRef v_opt);
  public: static Variant ti_gettoggles(const char* cls);
  public: void t_getblockedstatus(CVarRef v_bFromSlave = true);
  public: bool t_insorbsblacklist(CVarRef v_ip);
  public: bool t_indnsblacklist(CVarRef v_ip, CVarRef v_base);
  public: bool t_ispinglimitable();
  public: bool t_pinglimiter(CVarRef v_action = "edit");
  public: bool t_isblocked(CVarRef v_bFromSlave = true);
  public: bool t_isblockedfrom(CVarRef v_title, CVarRef v_bFromSlave = false);
  public: Variant t_blockedby();
  public: Variant t_blockedfor();
  public: Variant t_getblockid();
  public: Variant t_isblockedglobally(Variant v_ip = "");
  public: Variant t_islocked();
  public: Variant t_ishidden();
  public: Variant t_getid();
  public: void t_setid(CVarRef v_v);
  public: Variant t_getname();
  public: void t_setname(CVarRef v_str);
  public: Variant t_gettitlekey();
  public: bool t_getnewtalk();
  public: Variant t_getnewmessagelinks();
  public: bool t_checknewtalk(Variant v_field, CVarRef v_id, bool v_fromMaster = false);
  public: Variant t_updatenewtalk(CStrRef v_field, CVarRef v_id);
  public: Variant t_deletenewtalk(CStrRef v_field, CVarRef v_id);
  public: void t_setnewtalk(CVarRef v_val);
  public: static Variant ti_newtouchedtimestamp(const char* cls);
  public: void t_clearsharedcache();
  public: void t_invalidatecache();
  public: bool t_validatecache(CVarRef v_timestamp);
  public: Variant t_gettouched();
  public: bool t_setpassword(Variant v_str);
  public: void t_setinternalpassword(CVarRef v_str);
  public: Variant t_gettoken();
  public: void t_settoken(bool v_token = false);
  public: void t_setcookiepassword(CVarRef v_str);
  public: void t_setnewpassword(CVarRef v_str, bool v_throttle = true);
  public: bool t_ispasswordreminderthrottled();
  public: Variant t_getemail();
  public: Variant t_getemailauthenticationtimestamp();
  public: void t_setemail(CVarRef v_str);
  public: Variant t_getrealname();
  public: void t_setrealname(CVarRef v_str);
  public: Variant t_getoption(CVarRef v_oname, CVarRef v_defaultOverride = "");
  public: bool t_getbooloption(CVarRef v_oname);
  public: int64 t_getintoption(CVarRef v_oname, int64 v_defaultOverride = 0LL);
  public: void t_setoption(CVarRef v_oname, Variant v_val);
  public: void t_restoreoptions();
  public: Variant t_getdatepreference();
  public: Variant t_getrights();
  public: Variant t_getgroups();
  public: Variant t_geteffectivegroups(CVarRef v_recache = false);
  public: Variant t_geteditcount();
  public: void t_addgroup(CVarRef v_group);
  public: void t_removegroup(CVarRef v_group);
  public: bool t_isloggedin();
  public: bool t_isanon();
  public: bool t_isbot();
  public: bool t_isallowed(CVarRef v_action = "");
  public: bool t_usercpatrol();
  public: bool t_usenppatrol();
  public: Variant t_getskin();
  public: Variant t_iswatched(CVarRef v_title);
  public: void t_addwatch(CVarRef v_title);
  public: void t_removewatch(CVarRef v_title);
  public: void t_clearnotification(Variant v_title);
  public: void t_clearallnotifications(CVarRef v_currentUser);
  public: String t_encodeoptions();
  public: void t_decodeoptions(CVarRef v_str);
  public: void t_setcookie(Variant v_name, Variant v_value, Variant v_exp = 0LL);
  public: void t_clearcookie(CVarRef v_name);
  public: void t_setcookies();
  public: void t_logout();
  public: void t_dologout();
  public: void t_savesettings();
  public: Variant t_idforname();
  public: static Variant ti_createnew(const char* cls, Primitive v_name, Variant v_params = ScalarArrays::sa_[0]);
  public: void t_addtodatabase();
  public: void t_spreadblock();
  public: Variant t_getpagerenderinghash();
  public: bool t_isblockedfromcreateaccount();
  public: bool t_isblockedfromemailuser();
  public: bool t_isallowedtocreateaccount();
  public: void t_setloaded(CVarRef v_loaded);
  public: Variant t_getuserpage();
  public: Variant t_gettalkpage();
  public: Variant t_getmaxid();
  public: bool t_isnewbie();
  public: bool t_isactiveeditor();
  public: bool t_checkpassword(Variant v_password);
  public: Variant t_checktemporarypassword(CVarRef v_plaintext);
  public: Variant t_edittoken(Variant v_salt = "");
  public: String t_generatetoken(CStrRef v_salt = "");
  public: bool t_matchedittoken(CVarRef v_val, CVarRef v_salt = "");
  public: bool t_matchedittokennosuffix(CVarRef v_val, CStrRef v_salt = "");
  public: Variant t_sendconfirmationmail();
  public: Variant t_sendmail(CVarRef v_subject, CVarRef v_body, Variant v_from = null, CVarRef v_replyto = null_variant);
  public: String t_confirmationtoken(Variant v_expiration);
  public: Variant t_confirmationtokenurl(CStrRef v_token);
  public: Variant t_invalidationtokenurl(CStrRef v_token);
  public: Variant t_gettokenurl(CStrRef v_page, CStrRef v_token);
  public: bool t_confirmemail();
  public: bool t_invalidateemail();
  public: void t_setemailauthenticationtimestamp(CVarRef v_timestamp);
  public: Variant t_cansendemail();
  public: bool t_canreceiveemail();
  public: Variant t_isemailconfirmed();
  public: bool t_isemailconfirmationpending();
  public: Variant t_getregistration();
  public: Variant t_getfirstedittimestamp();
  public: static Variant ti_getgrouppermissions(const char* cls, CVarRef v_groups);
  public: static Array ti_getgroupswithpermission(const char* cls, CVarRef v_role);
  public: static Variant ti_getgroupname(const char* cls, CVarRef v_group);
  public: static Variant ti_getgroupmember(const char* cls, CVarRef v_group);
  public: static Variant ti_getallgroups(const char* cls);
  public: static Variant ti_getallrights(const char* cls);
  public: static Variant ti_getimplicitgroups(const char* cls);
  public: static Variant ti_getgrouppage(const char* cls, CVarRef v_group);
  public: static Variant ti_makegrouplinkhtml(const char* cls, CVarRef v_group, Variant v_text = "");
  public: static Variant ti_makegrouplinkwiki(const char* cls, CVarRef v_group, Variant v_text = "");
  public: void t_inceditcount();
  public: static Variant ti_getrightdescription(const char* cls, CVarRef v_right);
  public: static Variant ti_oldcrypt(const char* cls, CVarRef v_password, CVarRef v_userId);
  public: static Variant ti_crypt(const char* cls, Variant v_password, Variant v_salt = false);
  public: static Variant ti_comparepasswords(const char* cls, Variant v_hash, Variant v_password, Variant v_userId = false);
  public: bool t_addnewuserlogentry(bool v_byEmail = false);
  public: bool t_addnewuserlogentryautocreate();
  public: static String t_randompassword() { return ti_randompassword("user"); }
  public: static Variant t_edits(CVarRef v_uid) { return ti_edits("user", v_uid); }
  public: static Variant t_whoisreal(CVarRef v_id) { return ti_whoisreal("user", v_id); }
  public: static Variant t_getgrouppermissions(CVarRef v_groups) { return ti_getgrouppermissions("user", v_groups); }
  public: static p_user t_newfromid(CVarRef v_id) { return ti_newfromid("user", v_id); }
  public: static Variant t_getallrights() { return ti_getallrights("user"); }
  public: static Variant t_getgroupname(CVarRef v_group) { return ti_getgroupname("user", v_group); }
  public: static Variant t_newfromname(CVarRef v_name, CVarRef v_validate = "valid") { return ti_newfromname("user", v_name, v_validate); }
  public: static Variant t_getdefaultoptions() { return ti_getdefaultoptions("user"); }
  public: static bool t_isip(CVarRef v_name) { return ti_isip("user", v_name); }
  public: static bool t_isvalidusername(CVarRef v_name) { return ti_isvalidusername("user", v_name); }
  public: static Variant t_oldcrypt(CVarRef v_password, CVarRef v_userId) { return ti_oldcrypt("user", v_password, v_userId); }
  public: static Variant t_getrightdescription(CVarRef v_right) { return ti_getrightdescription("user", v_right); }
  public: static Variant t_getgrouppage(CVarRef v_group) { return ti_getgrouppage("user", v_group); }
  public: static Variant t_whois(CVarRef v_id) { return ti_whois("user", v_id); }
  public: static Variant t_makegrouplinkhtml(CVarRef v_group, CVarRef v_text = "") { return ti_makegrouplinkhtml("user", v_group, v_text); }
  public: static Variant t_isvalidemailaddr(CVarRef v_addr) { return ti_isvalidemailaddr("user", v_addr); }
  public: static Variant t_comparepasswords(CVarRef v_hash, CVarRef v_password, CVarRef v_userId = false) { return ti_comparepasswords("user", v_hash, v_password, v_userId); }
  public: static bool t_isusablename(CVarRef v_name) { return ti_isusablename("user", v_name); }
  public: static Variant t_gettoggles() { return ti_gettoggles("user"); }
  public: static Variant t_newfromconfirmationcode(CVarRef v_code) { return ti_newfromconfirmationcode("user", v_code); }
  public: static Variant t_crypt(CVarRef v_password, CVarRef v_salt = false) { return ti_crypt("user", v_password, v_salt); }
  public: static p_user t_newfromrow(CVarRef v_row) { return ti_newfromrow("user", v_row); }
  public: static Variant t_makegrouplinkwiki(CVarRef v_group, CVarRef v_text = "") { return ti_makegrouplinkwiki("user", v_group, v_text); }
  public: static Variant t_getgroupmember(CVarRef v_group) { return ti_getgroupmember("user", v_group); }
  public: static Variant t_createnew(Primitive v_name, CVarRef v_params = ScalarArrays::sa_[0]) { return ti_createnew("user", v_name, v_params); }
  public: static bool t_iscreatablename(CVarRef v_name) { return ti_iscreatablename("user", v_name); }
  public: static Variant t_getimplicitgroups() { return ti_getimplicitgroups("user"); }
  public: static Variant t_newtouchedtimestamp() { return ti_newtouchedtimestamp("user"); }
  public: static p_user t_newfromsession() { return ti_newfromsession("user"); }
  public: static Variant t_getcanonicalname(CVarRef v_name, CVarRef v_validate = "valid") { return ti_getcanonicalname("user", v_name, v_validate); }
  public: static Variant t_idfromname(CVarRef v_name) { return ti_idfromname("user", v_name); }
  public: static Variant t_getallgroups() { return ti_getallgroups("user"); }
  public: static Variant t_getdefaultoption(CStrRef v_opt) { return ti_getdefaultoption("user", v_opt); }
  public: static Array t_getgroupswithpermission(CVarRef v_role) { return ti_getgroupswithpermission("user", v_role); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_user_h__
