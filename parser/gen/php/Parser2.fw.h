
#ifndef __GENERATED_php_Parser2_fw_h__
#define __GENERATED_php_Parser2_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const int64 k_MW_MATH_MATHML;
extern const StaticString k_MEDIATYPE_MULTIMEDIA;
extern const int64 k_DB_MASTER;
extern const int64 k_NS_MEDIAWIKI;
extern const int64 k_DBO_PERSISTENT;
extern const int64 k_TC_SELF;
extern const StaticString k_MEDIATYPE_VIDEO;
extern const int64 k_MW_SPECIALPAGE_VERSION;
extern const int64 k_CACHE_MEMCACHED;
extern const bool k_UTF8_TAIL;
extern const int64 k_UNICODE_MAX;
extern const int64 k_ALF_PRELOAD_EXISTENCE;
extern const int64 k_UNICODE_HANGUL_NCOUNT;
extern const int64 k_DBO_IGNORE;
extern const StaticString k_MW_DATE_YMD;
extern const StaticString k_MW_CHAR_REFS_REGEX;
extern const StaticString k_UTF8_FDD0;
extern const int64 k_MW_MATH_SOURCE;
extern const StaticString k_RE_IPV6_WORD;
extern const int64 k_UNICODE_HANGUL_LEND;
extern const int64 k_APCOND_IPINRANGE;
extern const int64 k_DBO_NOBUFFER;
extern const int64 k_OT_MSG;
extern const int64 k_TS_DB;
extern const int64 k_TS_DB2;
extern const int64 k_APCOND_AGE_FROM_EDIT;
extern const int64 k_NS_PROJECT_TALK;
extern const int64 k_CACHE_ACCEL;
extern const int64 k_DB_WRITE;
extern const StaticString k_UTF8_HANGUL_LEND;
extern const int64 k_MW_SUPPORTS_PARSERFIRSTCALLINIT;
extern const int64 k_UNORM_FCD;
extern const int64 k_NS_IMAGE_TALK;
extern const int64 k_TS_POSTGRES;
extern const StaticString k_IP_ADDRESS_STRING;
extern const int64 k_TS_RFC2822;
extern const int64 k_UNICODE_HANGUL_TCOUNT;
extern const StaticString k_MW_DATE_ISO;
extern const int64 k_TC_MYSQL;
extern const int64 k_UNORM_NFKC;
extern const int64 k_UNORM_NFKD;
extern const int64 k_NS_HELP;
extern const int64 k_APCOND_EMAILCONFIRMED;
extern const StaticString k_UTF8_FDEF;
extern const int64 k_MW_REV_DELETED_USER;
extern const int64 k_UNICODE_HANGUL_VBASE;
extern const int64 k_UNICODE_HANGUL_FIRST;
extern const int64 k_EDIT_AUTOSUMMARY;
extern const StaticString k_UTF8_MAX;
extern const int64 k_TS_ISO_8601;
extern const bool k_NORMALIZE_ICU;
extern const int64 k_NS_USER;
extern const int64 k_MW_REV_DELETED_TEXT;
extern const int64 k_UNORM_NFC;
extern const int64 k_UNICODE_HANGUL_TEND;
extern const int64 k_UNORM_NFD;
extern const int64 k_AV_VIRUS_FOUND;
extern const int64 k_OT_HTML;
extern const int64 k_APCOND_INGROUPS;
extern const int64 k_MW_SUPPORTS_EDITFILTERMERGED;
extern const int64 k_UNORM_DEFAULT;
extern const int64 k_DB_SLAVE;
extern const int64 k_ALF_NO_LINK_LOCK;
extern const int64 k_MW_REV_DELETED_COMMENT;
extern const int64 k_CASCADE;
extern const StaticString k_UTF8_HANGUL_TEND;
extern const int64 k_TS_EXIF;
extern const int64 k_NS_HELP_TALK;
extern const int64 k_NS_PROJECT;
extern const int64 k_NS_SPECIAL;
extern const int64 k_DBO_DEFAULT;
extern const StaticString k_RE_IPV6_GAP;
extern const StaticString k_UTF8_HANGUL_TBASE;
extern const int64 k_NS_FILE;
extern const StaticString k_MW_DATE_MDY;
extern const int64 k_APCOND_AGE;
extern const int64 k_EDIT_UPDATE;
extern const bool k_AV_SCAN_FAILED;
extern const int64 k_LIST_AND;
extern const int64 k_RC_LOG;
extern const int64 k_NS_IMAGE;
extern const int64 k_NS_TALK;
extern const int64 k_MW_MATH_SIMPLE;
extern const StaticString k_RE_IPV6_PREFIX;
extern const StaticString k_MW_DATE_DMY;
extern const int64 k_NS_TEMPLATE;
extern const StaticString k_UTF8_OVERLONG_A;
extern const int64 k_UNORM_NONE;
extern const StaticString k_UTF8_OVERLONG_B;
extern const int64 k_CACHE_DB;
extern const StaticString k_UTF8_OVERLONG_C;
extern const int64 k_TS_MW;
extern const StaticString k_UTF8_HANGUL_LBASE;
extern const int64 k_APCOND_ISIP;
extern const int64 k_LIST_COMMA;
extern const int64 k_NS_CATEGORY;
extern const int64 k_UNICODE_SURROGATE_FIRST;
extern const int64 k_RC_NEW;
extern const int64 k_NS_FILE_TALK;
extern const int64 k_DB_READ;
extern const StaticString k_UTF8_FFFE;
extern const StaticString k_UTF8_FFFF;
extern const int64 k_TS_UNIX;
extern const StaticString k_EDIT_TOKEN_SUFFIX;
extern const bool k_UTF8_HEAD;
extern const int64 k_OT_WIKI;
extern const int64 k_TS_ORACLE;
extern const int64 k_APCOND_EDITCOUNT;
extern const int64 k_CACHE_NONE;
extern const int64 k_CACHE_ANYTHING;
extern const int64 k_NS_MEDIAWIKI_TALK;
extern const int64 k_GAID_FOR_UPDATE;
extern const int64 k_UNICODE_HANGUL_VEND;
extern const int64 k_UNICODE_HANGUL_VCOUNT;
extern const int64 k_DBO_TRX;
extern const int64 k_LIST_SET;
extern const StaticString k_RE_IPV6_ADD;
extern const int64 k_SFH_OBJECT_ARGS;
extern const int64 k_NS_MEDIA;
extern const int64 k_AV_SCAN_ABORTED;
extern const int64 k_SLH_PATTERN;
extern const StaticString k_UTF8_HANGUL_VEND;
extern const StaticString k_RE_IP_BYTE;
extern const StaticString k_MW_DATE_DEFAULT;
extern const int64 k_DB_LAST;
extern const StaticString k_RE_IP_PREFIX;
extern const int64 k_EDIT_FORCE_BOT;
extern const StaticString k_MEDIATYPE_BITMAP;
extern const int64 k_NS_USER_TALK;
extern const int64 k_UNICODE_HANGUL_LCOUNT;
extern const StaticString k_MEDIATYPE_EXECUTABLE;
extern const int64 k_MW_MATH_MODERN;
extern const int64 k_TC_SHELL;
extern const int64 k_ALF_NO_BLOCK_LOCK;
extern const StaticString k_RE_IPV6_BLOCK;
extern const StaticString k_RE_IPV6_V4_PREFIX;
extern const int64 k_NS_CATEGORY_TALK;
extern const int64 k_EDIT_NEW;
extern const StaticString k_UTF8_SURROGATE_FIRST;
extern const StaticString k_RE_IP_BLOCK;
extern const int64 k_RC_MOVE;
extern const int64 k_EDIT_DEFER_UPDATES;
extern const int64 k_RLH_FOR_UPDATE;
extern const int64 k_OT_PREPROCESS;
extern const int64 k_LIST_OR;
extern const int64 k_ALF_PRELOAD_LINKS;
extern const int64 k_DBO_DEBUG;
extern const int64 k_AV_NO_VIRUS;
extern const int64 k_MW_USER_VERSION;
extern const int64 k_EDIT_MINOR;
extern const int64 k_UNICODE_HANGUL_TBASE;
extern const int64 k_MW_MATH_HTML;
extern const StaticString k_MEDIATYPE_DRAWING;
extern const int64 k_UNICODE_SURROGATE_LAST;
extern const StaticString k_MEDIATYPE_TEXT;
extern const int64 k_MW_MATH_PNG;
extern const int64 k_NS_TEMPLATE_TALK;
extern const int64 k_UNICODE_HANGUL_LAST;
extern const int64 k_SFH_NO_HASH;
extern const int64 k_UNICODE_REPLACEMENT;
extern const StaticString k_MEDIATYPE_OFFICE;
extern const int64 k_NS_MAIN;
extern const int64 k_TC_HTML;
extern const int64 k_RC_EDIT;
extern const StaticString k_UTF8_HANGUL_VBASE;
extern const StaticString k_UTF8_HANGUL_LAST;
extern const StaticString k_UTF8_HANGUL_FIRST;
extern const StaticString k_RE_IP_ADD;
extern const int64 k_MW_REV_DELETED_RESTRICTED;
extern const int64 k_USER_TOKEN_LENGTH;
extern const StaticString k_MEDIATYPE_ARCHIVE;
extern const int64 k_UNICODE_HANGUL_LBASE;
extern const StaticString k_TBLSRC_URL;
extern const StaticString k_MEDIATYPE_AUDIO;
extern const int64 k_RC_MOVE_OVER_REDIRECT;
extern const int64 k_CACHE_DBA;
extern const StaticString k_UTF8_REPLACEMENT;
extern const StaticString k_MEDIATYPE_UNKNOWN;
extern const int64 k_LIST_NAMES;
extern const int64 k_TC_PCRE;
extern const int64 k_EDIT_SUPPRESS_RC;
extern const StaticString k_UTF8_SURROGATE_LAST;


// 2. Classes
FORWARD_DECLARE_CLASS(title)
extern const int64 q_title_CACHE_MAX;
FORWARD_DECLARE_CLASS(request)
FORWARD_DECLARE_CLASS(errorpageerror)
FORWARD_DECLARE_CLASS(stubuser)
FORWARD_DECLARE_CLASS(magicwordarray)
FORWARD_DECLARE_CLASS(linker)
extern const int64 q_linker_TOOL_LINKS_NOBLOCK;
FORWARD_DECLARE_CLASS(utfnormal)
FORWARD_DECLARE_CLASS(stripstate)
FORWARD_DECLARE_CLASS(hashtablereplacer)
FORWARD_DECLARE_CLASS(explodeiterator)
FORWARD_DECLARE_CLASS(linksupdate)
FORWARD_DECLARE_CLASS(getdb)
FORWARD_DECLARE_CLASS(parsertest)
FORWARD_DECLARE_CLASS(htmlcacheupdate)
FORWARD_DECLARE_CLASS(interwiki)
FORWARD_DECLARE_CLASS(messagecache)
FORWARD_DECLARE_CLASS(htmlfilecache)
FORWARD_DECLARE_CLASS(ansitermcolorer)
FORWARD_DECLARE_CLASS(stubobject)
FORWARD_DECLARE_CLASS(doublereplacer)
FORWARD_DECLARE_CLASS(status)
FORWARD_DECLARE_CLASS(mwexception)
FORWARD_DECLARE_CLASS(fakememcachedclient)
FORWARD_DECLARE_CLASS(replacer)
FORWARD_DECLARE_CLASS(parseroptions)
FORWARD_DECLARE_CLASS(ip)
FORWARD_DECLARE_CLASS(outputpage)
FORWARD_DECLARE_CLASS(ppdpart)
FORWARD_DECLARE_CLASS(magicword)
FORWARD_DECLARE_CLASS(revision)
extern const int64 q_revision_DELETED_TEXT;
extern const int64 q_revision_DELETED_COMMENT;
extern const int64 q_revision_DELETED_USER;
extern const int64 q_revision_DELETED_RESTRICTED;
extern const int64 q_revision_FOR_PUBLIC;
extern const int64 q_revision_FOR_THIS_USER;
extern const int64 q_revision_RAW;
FORWARD_DECLARE_CLASS(ppdstackelement)
FORWARD_DECLARE_CLASS(linkholderarray)
FORWARD_DECLARE_CLASS(ppnode_dom)
FORWARD_DECLARE_CLASS(fakeconverter)
FORWARD_DECLARE_CLASS(skintemplate)
FORWARD_DECLARE_CLASS(mediawiki_i18n)
FORWARD_DECLARE_CLASS(memcached)
FORWARD_DECLARE_CLASS(regexlikereplacer)
FORWARD_DECLARE_CLASS(testrecorder)
FORWARD_DECLARE_CLASS(proveparsertest)
FORWARD_DECLARE_CLASS(parser)
extern const StaticString q_parser_VERSION;
extern const int64 q_parser_SFH_NO_HASH;
extern const int64 q_parser_SFH_OBJECT_ARGS;
extern const StaticString q_parser_EXT_LINK_URL_CLASS;
extern const StaticString q_parser_EXT_IMAGE_REGEX;
extern const int64 q_parser_COLON_STATE_TEXT;
extern const int64 q_parser_COLON_STATE_TAG;
extern const int64 q_parser_COLON_STATE_TAGSTART;
extern const int64 q_parser_COLON_STATE_CLOSETAG;
extern const int64 q_parser_COLON_STATE_TAGSLASH;
extern const int64 q_parser_COLON_STATE_COMMENT;
extern const int64 q_parser_COLON_STATE_COMMENTDASH;
extern const int64 q_parser_COLON_STATE_COMMENTDASHDASH;
extern const int64 q_parser_PTD_FOR_INCLUSION;
extern const int64 q_parser_OT_HTML;
extern const int64 q_parser_OT_WIKI;
extern const int64 q_parser_OT_PREPROCESS;
extern const int64 q_parser_OT_MSG;
extern const StaticString q_parser_MARKER_SUFFIX;
FORWARD_DECLARE_CLASS(stubuserlang)
FORWARD_DECLARE_CLASS(ppdstack)
FORWARD_DECLARE_CLASS(titlearrayfromresult)
FORWARD_DECLARE_CLASS(provetestrecorder)
FORWARD_DECLARE_CLASS(linkbatch)
FORWARD_DECLARE_CLASS(preprocessor)
FORWARD_DECLARE_CLASS(passworderror)
FORWARD_DECLARE_CLASS(preprocessor_dom)
extern const int64 q_preprocessor_dom_CACHE_VERSION;
FORWARD_DECLARE_CLASS(monobooktemplate)
FORWARD_DECLARE_CLASS(stubcontlang)
FORWARD_DECLARE_CLASS(article)
FORWARD_DECLARE_CLASS(skin)
FORWARD_DECLARE_CLASS(parseroutput)
FORWARD_DECLARE_CLASS(skinmonobook)
FORWARD_DECLARE_CLASS(ppframe_dom)
FORWARD_DECLARE_CLASS(citerator)
FORWARD_DECLARE_CLASS(ppcustomframe_dom)
FORWARD_DECLARE_CLASS(ctraversable)
FORWARD_DECLARE_CLASS(linkcache)
FORWARD_DECLARE_CLASS(fatalerror)
FORWARD_DECLARE_CLASS(language)
FORWARD_DECLARE_CLASS(dbtestrecorder)
FORWARD_DECLARE_CLASS(pptemplateframe_dom)
FORWARD_DECLARE_CLASS(block)
extern const int64 q_block_EB_KEEP_EXPIRED;
extern const int64 q_block_EB_FOR_UPDATE;
extern const int64 q_block_EB_RANGE_ONLY;
FORWARD_DECLARE_CLASS(onlyincludereplacer)
FORWARD_DECLARE_CLASS(recentchange)
FORWARD_DECLARE_CLASS(coreparserfunctions)
FORWARD_DECLARE_CLASS(profiler)
FORWARD_DECLARE_CLASS(dummytermcolorer)
FORWARD_DECLARE_CLASS(titlearray)
FORWARD_DECLARE_CLASS(user)
FORWARD_DECLARE_CLASS(ppframe)
extern const int64 q_ppframe_NO_ARGS;
extern const int64 q_ppframe_NO_TEMPLATES;
extern const int64 q_ppframe_STRIP_COMMENTS;
extern const int64 q_ppframe_NO_IGNORE;
extern const int64 q_ppframe_RECOVER_COMMENTS;
extern const int64 q_ppframe_RECOVER_ORIG;
FORWARD_DECLARE_CLASS(filecache)
FORWARD_DECLARE_CLASS(xmlselect)
FORWARD_DECLARE_CLASS(sitestatsupdate)
FORWARD_DECLARE_CLASS(replacementarray)
FORWARD_DECLARE_CLASS(ppnode)
FORWARD_DECLARE_CLASS(memcachedclientforwiki)
FORWARD_DECLARE_CLASS(quicktemplate)
FORWARD_DECLARE_CLASS(searchupdate)
FORWARD_DECLARE_CLASS(xml)
FORWARD_DECLARE_CLASS(backlinkcache)
extern const int64 q_backlinkcache_CACHE_EXPIRY;
FORWARD_DECLARE_CLASS(mwnamespace)
FORWARD_DECLARE_CLASS(repogroup)
FORWARD_DECLARE_CLASS(dbtestpreviewer)
FORWARD_DECLARE_CLASS(sanitizer)
FORWARD_DECLARE_CLASS(stringutils)

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_Parser2_fw_h__
