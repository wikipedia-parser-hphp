
#ifndef __GENERATED_cls_ppframe_dom_h__
#define __GENERATED_cls_ppframe_dom_h__

#include <cls/ppframe.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_DOM.php line 806 */
class c_ppframe_dom : virtual public c_ppframe {
  BEGIN_CLASS_MAP(ppframe_dom)
    PARENT_CLASS(ppframe)
  END_CLASS_MAP(ppframe_dom)
  DECLARE_CLASS(ppframe_dom, PPFrame_DOM, ObjectData)
  void init();
  public: Variant m_preprocessor;
  public: Variant m_parser;
  public: Variant m_title;
  public: Variant m_titleCache;
  public: Variant m_loopCheckHash;
  public: Variant m_depth;
  public: void t___construct(Variant v_preprocessor);
  public: ObjectData *create(Variant v_preprocessor);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: p_pptemplateframe_dom t_newchild(Variant v_args = false, Variant v_title = false);
  public: Variant t_expand(Variant v_root, Variant v_flags = 0LL);
  public: Variant t_implodewithflags(int num_args, CVarRef v_sep, Variant v_flags, Array args = Array());
  public: Variant t_implode(int num_args, CVarRef v_sep, Array args = Array());
  public: Array t_virtualimplode(int num_args, CVarRef v_sep, Array args = Array());
  public: Array t_virtualbracketedimplode(int num_args, CVarRef v_start, CVarRef v_sep, CVarRef v_end, Array args = Array());
  public: String t___tostring();
  public: Variant t_getpdbk(bool v_level = false);
  public: Array t_getarguments();
  public: Array t_getnumberedarguments();
  public: Array t_getnamedarguments();
  public: bool t_isempty();
  public: bool t_getargument(CVarRef v_name);
  public: bool t_loopcheck(CVarRef v_title);
  public: bool t_istemplate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppframe_dom_h__
