
#ifndef __GENERATED_cls_errorpageerror_h__
#define __GENERATED_cls_errorpageerror_h__

#include <cls/mwexception.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 14907 */
class c_errorpageerror : virtual public c_mwexception {
  BEGIN_CLASS_MAP(errorpageerror)
    PARENT_CLASS(exception)
    PARENT_CLASS(mwexception)
  END_CLASS_MAP(errorpageerror)
  DECLARE_CLASS(errorpageerror, ErrorPageError, mwexception)
  void init();
  public: Variant m_title;
  public: Variant m_msg;
  public: void t___construct(Variant v_title, Variant v_msg);
  public: ObjectData *create(Variant v_title, Variant v_msg);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_report();
  public: static bool t_iscommandline() { return ti_iscommandline("errorpageerror"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_errorpageerror_h__
