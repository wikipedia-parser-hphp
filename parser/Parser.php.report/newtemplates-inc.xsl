<?xml version='1.0'?>
<xsl:stylesheet
  xmlns:AST="http://www.phpcompiler.org/phc-1.1"
  xmlns:past="http://www.parrotcode.org/PAST-0.1"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0" >

<xsl:output method='xml' indent='yes' />
<xsl:strip-space  elements="*"/>
<xsl:template match="AST:PHP_script">
	<AST:PHP_script>
         <xsl:call-template name="runGenerics"/>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="TOP">
	<TOP>
         <xsl:call-template name="runGenerics"/>
	</TOP>
</xsl:template>
<xsl:template match="TOP/AST:PHP_script">
	<TOP>
		<AST:PHP_script>
                <xsl:call-template name="runGenerics"/>
		</AST:PHP_script>
	</TOP>
</xsl:template>
<xsl:template match="attrs">
	<attrs>
         <xsl:call-template name="runGenerics"/>
	</attrs>
</xsl:template>
<xsl:template match="AST:PHP_script/attrs">
	<AST:PHP_script>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="attr">
	<attr>
         <xsl:call-template name="runGenerics"/>
	</attr>
</xsl:template>
<xsl:template match="attrs/attr">
	<attrs>
		<attr>
                <xsl:call-template name="runGenerics"/>
		</attr>
	</attrs>
</xsl:template>
<xsl:template match="string">
	<string>
         <xsl:call-template name="runGenerics"/>
	</string>
</xsl:template>
<xsl:template match="attr/string">
	<attr>
		<string>
                <xsl:call-template name="runGenerics"/>
		</string>
	</attr>
</xsl:template>
<xsl:template match="AST:Statement_list">
	<AST:Statement_list>
         <xsl:call-template name="runGenerics"/>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Statement_list">
	<AST:PHP_script>
		<AST:Statement_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Statement_list>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:Class_def">
	<AST:Class_def>
         <xsl:call-template name="runGenerics"/>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Class_def">
	<AST:Statement_list>
		<AST:Class_def>
                <xsl:call-template name="runGenerics"/>
		</AST:Class_def>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Class_def/attrs">
	<AST:Class_def>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Class_def>
</xsl:template>
<xsl:template match="string_list">
	<string_list>
         <xsl:call-template name="runGenerics"/>
	</string_list>
</xsl:template>
<xsl:template match="attr/string_list">
	<attr>
		<string_list>
                <xsl:call-template name="runGenerics"/>
		</string_list>
	</attr>
</xsl:template>
<xsl:template match="string_list/string">
	<string_list>
		<string>
                <xsl:call-template name="runGenerics"/>
		</string>
	</string_list>
</xsl:template>
<xsl:template match="integer">
	<integer>
         <xsl:call-template name="runGenerics"/>
	</integer>
</xsl:template>
<xsl:template match="attr/integer">
	<attr>
		<integer>
                <xsl:call-template name="runGenerics"/>
		</integer>
	</attr>
</xsl:template>
<xsl:template match="AST:Class_mod">
	<AST:Class_mod>
         <xsl:call-template name="runGenerics"/>
	</AST:Class_mod>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Class_mod">
	<AST:Class_def>
		<AST:Class_mod>
                <xsl:call-template name="runGenerics"/>
		</AST:Class_mod>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_mod/attrs">
	<AST:Class_mod>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Class_mod>
</xsl:template>
<xsl:template match="bool">
	<bool>
         <xsl:call-template name="runGenerics"/>
	</bool>
</xsl:template>
<xsl:template match="AST:Class_def/bool">
	<AST:Class_def>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:CLASS_NAME">
	<AST:CLASS_NAME>
         <xsl:call-template name="runGenerics"/>
	</AST:CLASS_NAME>
</xsl:template>
<xsl:template match="AST:Class_def/AST:CLASS_NAME">
	<AST:Class_def>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:CLASS_NAME/attrs">
	<AST:CLASS_NAME>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:CLASS_NAME>
</xsl:template>
<xsl:template match="AST:Attribute">
	<AST:Attribute>
         <xsl:call-template name="runGenerics"/>
	</AST:Attribute>
</xsl:template>
<xsl:template match="AST:Member_list">
	<AST:Member_list>
         <xsl:call-template name="runGenerics"/>
	</AST:Member_list>
</xsl:template>
<xsl:template match="AST:Member_list/AST:Attribute">
	<AST:Member_list>
		<AST:Attribute>
                <xsl:call-template name="runGenerics"/>
		</AST:Attribute>
	</AST:Member_list>
</xsl:template>
<xsl:template match="AST:Attribute/attrs">
	<AST:Attribute>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Attribute>
</xsl:template>
<xsl:template match="AST:Attr_mod">
	<AST:Attr_mod>
         <xsl:call-template name="runGenerics"/>
	</AST:Attr_mod>
</xsl:template>
<xsl:template match="AST:Attribute/AST:Attr_mod">
	<AST:Attribute>
		<AST:Attr_mod>
                <xsl:call-template name="runGenerics"/>
		</AST:Attr_mod>
	</AST:Attribute>
</xsl:template>
<xsl:template match="AST:Attr_mod/attrs">
	<AST:Attr_mod>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Attr_mod>
</xsl:template>
<xsl:template match="AST:Name_with_default_list">
	<AST:Name_with_default_list>
         <xsl:call-template name="runGenerics"/>
	</AST:Name_with_default_list>
</xsl:template>
<xsl:template match="AST:Attribute/AST:Name_with_default_list">
	<AST:Attribute>
		<AST:Name_with_default_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Name_with_default_list>
	</AST:Attribute>
</xsl:template>
<xsl:template match="AST:Name_with_default">
	<AST:Name_with_default>
         <xsl:call-template name="runGenerics"/>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:Name_with_default_list/AST:Name_with_default">
	<AST:Name_with_default_list>
		<AST:Name_with_default>
                <xsl:call-template name="runGenerics"/>
		</AST:Name_with_default>
	</AST:Name_with_default_list>
</xsl:template>
<xsl:template match="AST:Name_with_default/attrs">
	<AST:Name_with_default>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:VARIABLE_NAME">
	<AST:VARIABLE_NAME>
         <xsl:call-template name="runGenerics"/>
	</AST:VARIABLE_NAME>
</xsl:template>
<xsl:template match="AST:Name_with_default/AST:VARIABLE_NAME">
	<AST:Name_with_default>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:VARIABLE_NAME/attrs">
	<AST:VARIABLE_NAME>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:VARIABLE_NAME>
</xsl:template>
<xsl:template match="AST:STRING">
	<AST:STRING>
         <xsl:call-template name="runGenerics"/>
	</AST:STRING>
</xsl:template>
<xsl:template match="AST:Name_with_default/AST:STRING">
	<AST:Name_with_default>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:STRING/attrs">
	<AST:STRING>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:STRING>
</xsl:template>
<xsl:template match="value">
	<value>
         <xsl:call-template name="runGenerics"/>
	</value>
</xsl:template>
<xsl:template match="AST:Name_with_default/value">
	<AST:Name_with_default>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Attribute">
	<AST:Class_def>
		<AST:Attribute>
                <xsl:call-template name="runGenerics"/>
		</AST:Attribute>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Attr_mod">
	<AST:Class_def>
		<AST:Attr_mod>
                <xsl:call-template name="runGenerics"/>
		</AST:Attr_mod>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Name_with_default_list">
	<AST:Class_def>
		<AST:Name_with_default_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Name_with_default_list>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:INT">
	<AST:INT>
         <xsl:call-template name="runGenerics"/>
	</AST:INT>
</xsl:template>
<xsl:template match="AST:Name_with_default/AST:INT">
	<AST:Name_with_default>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:INT/attrs">
	<AST:INT>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:INT>
</xsl:template>
<xsl:template match="attr/bool">
	<attr>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</attr>
</xsl:template>
<xsl:template match="AST:Expr">
	<AST:Expr>
         <xsl:call-template name="runGenerics"/>
	</AST:Expr>
</xsl:template>
<xsl:template match="AST:Name_with_default/AST:Expr">
	<AST:Name_with_default>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Name_with_default">
	<AST:Class_def>
		<AST:Name_with_default>
                <xsl:call-template name="runGenerics"/>
		</AST:Name_with_default>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:VARIABLE_NAME">
	<AST:Class_def>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Expr">
	<AST:Class_def>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Method">
	<AST:Method>
         <xsl:call-template name="runGenerics"/>
	</AST:Method>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Method">
	<AST:Class_def>
		<AST:Method>
                <xsl:call-template name="runGenerics"/>
		</AST:Method>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Method/attrs">
	<AST:Method>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Method>
</xsl:template>
<xsl:template match="AST:Signature">
	<AST:Signature>
         <xsl:call-template name="runGenerics"/>
	</AST:Signature>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Signature">
	<AST:Class_def>
		<AST:Signature>
                <xsl:call-template name="runGenerics"/>
		</AST:Signature>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Signature/attrs">
	<AST:Signature>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Signature>
</xsl:template>
<xsl:template match="AST:Method_mod">
	<AST:Method_mod>
         <xsl:call-template name="runGenerics"/>
	</AST:Method_mod>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Method_mod">
	<AST:Class_def>
		<AST:Method_mod>
                <xsl:call-template name="runGenerics"/>
		</AST:Method_mod>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Method_mod/attrs">
	<AST:Method_mod>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Method_mod>
</xsl:template>
<xsl:template match="AST:METHOD_NAME">
	<AST:METHOD_NAME>
         <xsl:call-template name="runGenerics"/>
	</AST:METHOD_NAME>
</xsl:template>
<xsl:template match="AST:METHOD_NAME/attrs">
	<AST:METHOD_NAME>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:METHOD_NAME>
</xsl:template>
<xsl:template match="AST:Formal_parameter_list">
	<AST:Formal_parameter_list>
         <xsl:call-template name="runGenerics"/>
	</AST:Formal_parameter_list>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Formal_parameter_list">
	<AST:Class_def>
		<AST:Formal_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Formal_parameter_list>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Formal_parameter">
	<AST:Formal_parameter>
         <xsl:call-template name="runGenerics"/>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Formal_parameter_list/AST:Formal_parameter">
	<AST:Formal_parameter_list>
		<AST:Formal_parameter>
                <xsl:call-template name="runGenerics"/>
		</AST:Formal_parameter>
	</AST:Formal_parameter_list>
</xsl:template>
<xsl:template match="AST:Formal_parameter/attrs">
	<AST:Formal_parameter>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Type">
	<AST:Type>
         <xsl:call-template name="runGenerics"/>
	</AST:Type>
</xsl:template>
<xsl:template match="AST:Formal_parameter/AST:Type">
	<AST:Formal_parameter>
		<AST:Type>
                <xsl:call-template name="runGenerics"/>
		</AST:Type>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Type/attrs">
	<AST:Type>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Type>
</xsl:template>
<xsl:template match="AST:Formal_parameter/AST:CLASS_NAME">
	<AST:Formal_parameter>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Formal_parameter/bool">
	<AST:Formal_parameter>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Formal_parameter/AST:VARIABLE_NAME">
	<AST:Formal_parameter>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Array">
	<AST:Array>
         <xsl:call-template name="runGenerics"/>
	</AST:Array>
</xsl:template>
<xsl:template match="AST:Formal_parameter/AST:Array">
	<AST:Formal_parameter>
		<AST:Array>
                <xsl:call-template name="runGenerics"/>
		</AST:Array>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Array/attrs">
	<AST:Array>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Array>
</xsl:template>
<xsl:template match="AST:Array_elem_list">
	<AST:Array_elem_list>
         <xsl:call-template name="runGenerics"/>
	</AST:Array_elem_list>
</xsl:template>
<xsl:template match="AST:Formal_parameter/AST:Array_elem_list">
	<AST:Formal_parameter>
		<AST:Array_elem_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem_list>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Statement_list">
	<AST:Class_def>
		<AST:Statement_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Statement_list>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Eval_expr">
	<AST:Eval_expr>
         <xsl:call-template name="runGenerics"/>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Eval_expr">
	<AST:Statement_list>
		<AST:Eval_expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Eval_expr>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Eval_expr/attrs">
	<AST:Eval_expr>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Assignment">
	<AST:Assignment>
         <xsl:call-template name="runGenerics"/>
	</AST:Assignment>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Assignment">
	<AST:Eval_expr>
		<AST:Assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Assignment>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Assignment/attrs">
	<AST:Assignment>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Assignment>
</xsl:template>
<xsl:template match="AST:Variable">
	<AST:Variable>
         <xsl:call-template name="runGenerics"/>
	</AST:Variable>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Variable">
	<AST:Eval_expr>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Variable/attrs">
	<AST:Variable>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Variable>
</xsl:template>
<xsl:template match="AST:Target">
	<AST:Target>
         <xsl:call-template name="runGenerics"/>
	</AST:Target>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Target">
	<AST:Eval_expr>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Expr_list">
	<AST:Expr_list>
         <xsl:call-template name="runGenerics"/>
	</AST:Expr_list>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Expr_list">
	<AST:Eval_expr>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:VARIABLE_NAME">
	<AST:Eval_expr>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Eval_expr/bool">
	<AST:Eval_expr>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Eval_expr">
	<AST:Class_def>
		<AST:Eval_expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Eval_expr>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Assignment">
	<AST:Class_def>
		<AST:Assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Assignment>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Variable">
	<AST:Class_def>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Target">
	<AST:Class_def>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Expr_list">
	<AST:Class_def>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Array_elem_list">
	<AST:Class_def>
		<AST:Array_elem_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem_list>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Array_elem">
	<AST:Array_elem>
         <xsl:call-template name="runGenerics"/>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Array_elem_list/AST:Array_elem">
	<AST:Array_elem_list>
		<AST:Array_elem>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem>
	</AST:Array_elem_list>
</xsl:template>
<xsl:template match="AST:Array_elem/attrs">
	<AST:Array_elem>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:INT">
	<AST:Array_elem>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Array_elem/value">
	<AST:Array_elem>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Array_elem/bool">
	<AST:Array_elem>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:Array_elem_list">
	<AST:Array_elem>
		<AST:Array_elem_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem_list>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Array_elem">
	<AST:Class_def>
		<AST:Array_elem>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:INT">
	<AST:Class_def>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/value">
	<AST:Class_def>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:Expr">
	<AST:Array_elem>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Method_invocation">
	<AST:Method_invocation>
         <xsl:call-template name="runGenerics"/>
	</AST:Method_invocation>
</xsl:template>
<xsl:template match="AST:Method_invocation/attrs">
	<AST:Method_invocation>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Method_invocation>
</xsl:template>
<xsl:template match="AST:Actual_parameter_list">
	<AST:Actual_parameter_list>
         <xsl:call-template name="runGenerics"/>
	</AST:Actual_parameter_list>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Actual_parameter_list">
	<AST:Class_def>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Bin_op">
	<AST:Bin_op>
         <xsl:call-template name="runGenerics"/>
	</AST:Bin_op>
</xsl:template>
<xsl:template match="AST:Bin_op/attrs">
	<AST:Bin_op>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Bin_op>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Bin_op">
	<AST:Class_def>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:STRING">
	<AST:Class_def>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:OP">
	<AST:OP>
         <xsl:call-template name="runGenerics"/>
	</AST:OP>
</xsl:template>
<xsl:template match="AST:Class_def/AST:OP">
	<AST:Class_def>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:OP/attrs">
	<AST:OP>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:OP>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Method_invocation">
	<AST:Class_def>
		<AST:Method_invocation>
                <xsl:call-template name="runGenerics"/>
		</AST:Method_invocation>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:If">
	<AST:If>
         <xsl:call-template name="runGenerics"/>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Class_def/AST:If">
	<AST:Class_def>
		<AST:If>
                <xsl:call-template name="runGenerics"/>
		</AST:If>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:If/attrs">
	<AST:If>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Actual_parameter">
	<AST:Actual_parameter>
         <xsl:call-template name="runGenerics"/>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter_list/AST:Actual_parameter">
	<AST:Actual_parameter_list>
		<AST:Actual_parameter>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter>
	</AST:Actual_parameter_list>
</xsl:template>
<xsl:template match="AST:Actual_parameter/attrs">
	<AST:Actual_parameter>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter/bool">
	<AST:Actual_parameter>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:Target">
	<AST:Actual_parameter>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:Expr_list">
	<AST:Actual_parameter>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Expr_list/AST:STRING">
	<AST:Expr_list>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:Expr_list>
</xsl:template>
<xsl:template match="AST:STRING/value">
	<AST:STRING>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:STRING>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:If">
	<AST:Statement_list>
		<AST:If>
                <xsl:call-template name="runGenerics"/>
		</AST:If>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:If/AST:Method_invocation">
	<AST:If>
		<AST:Method_invocation>
                <xsl:call-template name="runGenerics"/>
		</AST:Method_invocation>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Target">
	<AST:If>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Actual_parameter_list">
	<AST:If>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Actual_parameter/value">
	<AST:Actual_parameter>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:If/AST:Statement_list">
	<AST:If>
		<AST:Statement_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Statement_list>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Method_invocation">
	<AST:Eval_expr>
		<AST:Method_invocation>
                <xsl:call-template name="runGenerics"/>
		</AST:Method_invocation>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Actual_parameter_list">
	<AST:Eval_expr>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:If/AST:Eval_expr">
	<AST:If>
		<AST:Eval_expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Eval_expr>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Assignment">
	<AST:If>
		<AST:Assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Assignment>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Variable">
	<AST:If>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Expr_list">
	<AST:If>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:VARIABLE_NAME">
	<AST:If>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/bool">
	<AST:If>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/value">
	<AST:If>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Eval_expr/value">
	<AST:Eval_expr>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:BOOL">
	<AST:BOOL>
         <xsl:call-template name="runGenerics"/>
	</AST:BOOL>
</xsl:template>
<xsl:template match="AST:BOOL/attrs">
	<AST:BOOL>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:BOOL>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:Variable">
	<AST:Actual_parameter>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:VARIABLE_NAME">
	<AST:Actual_parameter>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:METHOD_NAME">
	<AST:Eval_expr>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Foreach">
	<AST:Foreach>
         <xsl:call-template name="runGenerics"/>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Foreach">
	<AST:Class_def>
		<AST:Foreach>
                <xsl:call-template name="runGenerics"/>
		</AST:Foreach>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Foreach/attrs">
	<AST:Foreach>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Foreach>
</xsl:template>
<xsl:template match="attrs/AST:Variable">
	<attrs>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</attrs>
</xsl:template>
<xsl:template match="attrs/AST:Target">
	<attrs>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</attrs>
</xsl:template>
<xsl:template match="attrs/AST:Expr_list">
	<attrs>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</attrs>
</xsl:template>
<xsl:template match="AST:Reflection">
	<AST:Reflection>
         <xsl:call-template name="runGenerics"/>
	</AST:Reflection>
</xsl:template>
<xsl:template match="attrs/AST:Reflection">
	<attrs>
		<AST:Reflection>
                <xsl:call-template name="runGenerics"/>
		</AST:Reflection>
	</attrs>
</xsl:template>
<xsl:template match="AST:Reflection/attrs">
	<AST:Reflection>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Reflection>
</xsl:template>
<xsl:template match="AST:Unary_op">
	<AST:Unary_op>
         <xsl:call-template name="runGenerics"/>
	</AST:Unary_op>
</xsl:template>
<xsl:template match="AST:If/AST:Unary_op">
	<AST:If>
		<AST:Unary_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Unary_op>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Unary_op/attrs">
	<AST:Unary_op>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Unary_op>
</xsl:template>
<xsl:template match="AST:If/AST:OP">
	<AST:If>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Return">
	<AST:Return>
         <xsl:call-template name="runGenerics"/>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Return">
	<AST:Statement_list>
		<AST:Return>
                <xsl:call-template name="runGenerics"/>
		</AST:Return>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Return/attrs">
	<AST:Return>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/AST:Expr">
	<AST:Return>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Class_def/AST:METHOD_NAME">
	<AST:Class_def>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Actual_parameter">
	<AST:Class_def>
		<AST:Actual_parameter>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:Target">
	<AST:Array_elem>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:Expr_list">
	<AST:Array_elem>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:New">
	<AST:New>
         <xsl:call-template name="runGenerics"/>
	</AST:New>
</xsl:template>
<xsl:template match="AST:New/attrs">
	<AST:New>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:New>
</xsl:template>
<xsl:template match="AST:NIL">
	<AST:NIL>
         <xsl:call-template name="runGenerics"/>
	</AST:NIL>
</xsl:template>
<xsl:template match="AST:NIL/attrs">
	<AST:NIL>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:NIL>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:STRING">
	<AST:Array_elem>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Formal_parameter/AST:Expr">
	<AST:Formal_parameter>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:Variable">
	<AST:Array_elem>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:OP">
	<AST:Array_elem>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Constant">
	<AST:Constant>
         <xsl:call-template name="runGenerics"/>
	</AST:Constant>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:Constant">
	<AST:Array_elem>
		<AST:Constant>
                <xsl:call-template name="runGenerics"/>
		</AST:Constant>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Constant/attrs">
	<AST:Constant>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Constant>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:CLASS_NAME">
	<AST:Array_elem>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:CONSTANT_NAME">
	<AST:CONSTANT_NAME>
         <xsl:call-template name="runGenerics"/>
	</AST:CONSTANT_NAME>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:CONSTANT_NAME">
	<AST:Array_elem>
		<AST:CONSTANT_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CONSTANT_NAME>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:CONSTANT_NAME/attrs">
	<AST:CONSTANT_NAME>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:CONSTANT_NAME>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Constant">
	<AST:Class_def>
		<AST:Constant>
                <xsl:call-template name="runGenerics"/>
		</AST:Constant>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:CONSTANT_NAME">
	<AST:Class_def>
		<AST:CONSTANT_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CONSTANT_NAME>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:If/AST:Bin_op">
	<AST:If>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Instanceof">
	<AST:Instanceof>
         <xsl:call-template name="runGenerics"/>
	</AST:Instanceof>
</xsl:template>
<xsl:template match="AST:If/AST:Instanceof">
	<AST:If>
		<AST:Instanceof>
                <xsl:call-template name="runGenerics"/>
		</AST:Instanceof>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Instanceof/attrs">
	<AST:Instanceof>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Instanceof>
</xsl:template>
<xsl:template match="AST:If/AST:CLASS_NAME">
	<AST:If>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:CLASS_NAME">
	<AST:Eval_expr>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:METHOD_NAME">
	<AST:Actual_parameter>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:Actual_parameter_list">
	<AST:Actual_parameter>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Return/AST:STRING">
	<AST:Return>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/value">
	<AST:Return>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Nop">
	<AST:Nop>
         <xsl:call-template name="runGenerics"/>
	</AST:Nop>
</xsl:template>
<xsl:template match="AST:If/AST:Nop">
	<AST:If>
		<AST:Nop>
                <xsl:call-template name="runGenerics"/>
		</AST:Nop>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Nop/attrs">
	<AST:Nop>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Nop>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Return">
	<AST:Class_def>
		<AST:Return>
                <xsl:call-template name="runGenerics"/>
		</AST:Return>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Formal_parameter">
	<AST:Class_def>
		<AST:Formal_parameter>
                <xsl:call-template name="runGenerics"/>
		</AST:Formal_parameter>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Type">
	<AST:Class_def>
		<AST:Type>
                <xsl:call-template name="runGenerics"/>
		</AST:Type>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:BOOL">
	<AST:Class_def>
		<AST:BOOL>
                <xsl:call-template name="runGenerics"/>
		</AST:BOOL>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:NIL">
	<AST:Class_def>
		<AST:NIL>
                <xsl:call-template name="runGenerics"/>
		</AST:NIL>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Global">
	<AST:Global>
         <xsl:call-template name="runGenerics"/>
	</AST:Global>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Global">
	<AST:Statement_list>
		<AST:Global>
                <xsl:call-template name="runGenerics"/>
		</AST:Global>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Global/attrs">
	<AST:Global>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Global>
</xsl:template>
<xsl:template match="AST:Variable_name_list">
	<AST:Variable_name_list>
         <xsl:call-template name="runGenerics"/>
	</AST:Variable_name_list>
</xsl:template>
<xsl:template match="AST:Global/AST:Variable_name_list">
	<AST:Global>
		<AST:Variable_name_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable_name_list>
	</AST:Global>
</xsl:template>
<xsl:template match="AST:Variable_name_list/AST:VARIABLE_NAME">
	<AST:Variable_name_list>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Variable_name_list>
</xsl:template>
<xsl:template match="AST:VARIABLE_NAME/value">
	<AST:VARIABLE_NAME>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:VARIABLE_NAME>
</xsl:template>
<xsl:template match="AST:Global/AST:VARIABLE_NAME">
	<AST:Global>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Global>
</xsl:template>
<xsl:template match="AST:Global/value">
	<AST:Global>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Global>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:CLASS_NAME">
	<AST:Actual_parameter>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:CONSTANT_NAME">
	<AST:Actual_parameter>
		<AST:CONSTANT_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CONSTANT_NAME>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:List_assignment">
	<AST:List_assignment>
         <xsl:call-template name="runGenerics"/>
	</AST:List_assignment>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:List_assignment">
	<AST:Eval_expr>
		<AST:List_assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:List_assignment>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:List_assignment/attrs">
	<AST:List_assignment>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:List_assignment>
</xsl:template>
<xsl:template match="AST:List_element_list">
	<AST:List_element_list>
         <xsl:call-template name="runGenerics"/>
	</AST:List_element_list>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:List_element_list">
	<AST:Eval_expr>
		<AST:List_element_list>
                <xsl:call-template name="runGenerics"/>
		</AST:List_element_list>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:List_element_list/AST:Variable">
	<AST:List_element_list>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:List_element_list>
</xsl:template>
<xsl:template match="AST:Variable/AST:Target">
	<AST:Variable>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Variable>
</xsl:template>
<xsl:template match="AST:Variable/AST:Expr_list">
	<AST:Variable>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Variable>
</xsl:template>
<xsl:template match="AST:Expr_list/AST:Variable">
	<AST:Expr_list>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Expr_list>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Actual_parameter">
	<AST:Eval_expr>
		<AST:Actual_parameter>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Array_elem_list">
	<AST:Eval_expr>
		<AST:Array_elem_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem_list>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Array_elem">
	<AST:Eval_expr>
		<AST:Array_elem>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Expr">
	<AST:Eval_expr>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:STRING">
	<AST:Eval_expr>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Global">
	<AST:Class_def>
		<AST:Global>
                <xsl:call-template name="runGenerics"/>
		</AST:Global>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Variable_name_list">
	<AST:Class_def>
		<AST:Variable_name_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable_name_list>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Op_assignment">
	<AST:Op_assignment>
         <xsl:call-template name="runGenerics"/>
	</AST:Op_assignment>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Op_assignment">
	<AST:Class_def>
		<AST:Op_assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Op_assignment>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Op_assignment/attrs">
	<AST:Op_assignment>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Op_assignment>
</xsl:template>
<xsl:template match="AST:Return/AST:Bin_op">
	<AST:Return>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/AST:Method_invocation">
	<AST:Return>
		<AST:Method_invocation>
                <xsl:call-template name="runGenerics"/>
		</AST:Method_invocation>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/AST:Target">
	<AST:Return>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/AST:Actual_parameter_list">
	<AST:Return>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/AST:OP">
	<AST:Return>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/AST:Variable">
	<AST:Return>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/AST:Expr_list">
	<AST:Return>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/AST:VARIABLE_NAME">
	<AST:Return>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Post_op">
	<AST:Post_op>
         <xsl:call-template name="runGenerics"/>
	</AST:Post_op>
</xsl:template>
<xsl:template match="AST:Return/AST:Post_op">
	<AST:Return>
		<AST:Post_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Post_op>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Post_op/attrs">
	<AST:Post_op>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Post_op>
</xsl:template>
<xsl:template match="AST:Conditional_expr">
	<AST:Conditional_expr>
         <xsl:call-template name="runGenerics"/>
	</AST:Conditional_expr>
</xsl:template>
<xsl:template match="AST:Return/AST:Conditional_expr">
	<AST:Return>
		<AST:Conditional_expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Conditional_expr>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Conditional_expr/attrs">
	<AST:Conditional_expr>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Conditional_expr>
</xsl:template>
<xsl:template match="AST:Return/AST:METHOD_NAME">
	<AST:Return>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:If/AST:Reflection">
	<AST:If>
		<AST:Reflection>
                <xsl:call-template name="runGenerics"/>
		</AST:Reflection>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Static_declaration">
	<AST:Static_declaration>
         <xsl:call-template name="runGenerics"/>
	</AST:Static_declaration>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Static_declaration">
	<AST:Statement_list>
		<AST:Static_declaration>
                <xsl:call-template name="runGenerics"/>
		</AST:Static_declaration>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Static_declaration/attrs">
	<AST:Static_declaration>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Static_declaration>
</xsl:template>
<xsl:template match="AST:Static_declaration/AST:Name_with_default_list">
	<AST:Static_declaration>
		<AST:Name_with_default_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Name_with_default_list>
	</AST:Static_declaration>
</xsl:template>
<xsl:template match="AST:While">
	<AST:While>
         <xsl:call-template name="runGenerics"/>
	</AST:While>
</xsl:template>
<xsl:template match="AST:Class_def/AST:While">
	<AST:Class_def>
		<AST:While>
                <xsl:call-template name="runGenerics"/>
		</AST:While>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:While/attrs">
	<AST:While>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:While>
</xsl:template>
<xsl:template match="AST:Expr_list/AST:INT">
	<AST:Expr_list>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:Expr_list>
</xsl:template>
<xsl:template match="AST:INT/value">
	<AST:INT>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:INT>
</xsl:template>
<xsl:template match="AST:Break">
	<AST:Break>
         <xsl:call-template name="runGenerics"/>
	</AST:Break>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Break">
	<AST:Statement_list>
		<AST:Break>
                <xsl:call-template name="runGenerics"/>
		</AST:Break>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Break/attrs">
	<AST:Break>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Break>
</xsl:template>
<xsl:template match="AST:Break/AST:Expr">
	<AST:Break>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Break>
</xsl:template>
<xsl:template match="AST:If/AST:STRING">
	<AST:If>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Bin_op">
	<AST:Eval_expr>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:OP">
	<AST:Eval_expr>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Expr_list/AST:Expr">
	<AST:Expr_list>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Expr_list>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Array">
	<AST:Class_def>
		<AST:Array>
                <xsl:call-template name="runGenerics"/>
		</AST:Array>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Constant">
	<AST:Eval_expr>
		<AST:Constant>
                <xsl:call-template name="runGenerics"/>
		</AST:Constant>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:CONSTANT_NAME">
	<AST:Eval_expr>
		<AST:CONSTANT_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CONSTANT_NAME>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Post_op">
	<AST:Class_def>
		<AST:Post_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Post_op>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Op_assignment">
	<AST:Eval_expr>
		<AST:Op_assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Op_assignment>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Continue">
	<AST:Continue>
         <xsl:call-template name="runGenerics"/>
	</AST:Continue>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Continue">
	<AST:Class_def>
		<AST:Continue>
                <xsl:call-template name="runGenerics"/>
		</AST:Continue>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Continue/attrs">
	<AST:Continue>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Continue>
</xsl:template>
<xsl:template match="AST:If/AST:INT">
	<AST:If>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Continue">
	<AST:If>
		<AST:Continue>
                <xsl:call-template name="runGenerics"/>
		</AST:Continue>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Expr">
	<AST:If>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Actual_parameter">
	<AST:If>
		<AST:Actual_parameter>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:METHOD_NAME">
	<AST:If>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Foreach">
	<AST:If>
		<AST:Foreach>
                <xsl:call-template name="runGenerics"/>
		</AST:Foreach>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:BOOL">
	<AST:If>
		<AST:BOOL>
                <xsl:call-template name="runGenerics"/>
		</AST:BOOL>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Op_assignment">
	<AST:If>
		<AST:Op_assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Op_assignment>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Unary_op">
	<AST:Class_def>
		<AST:Unary_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Unary_op>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:OP">
	<AST:Actual_parameter>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:STRING">
	<AST:Actual_parameter>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:Bin_op">
	<AST:Actual_parameter>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Throw">
	<AST:Throw>
         <xsl:call-template name="runGenerics"/>
	</AST:Throw>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Throw">
	<AST:Statement_list>
		<AST:Throw>
                <xsl:call-template name="runGenerics"/>
		</AST:Throw>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Throw/attrs">
	<AST:Throw>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Throw>
</xsl:template>
<xsl:template match="AST:Throw/AST:New">
	<AST:Throw>
		<AST:New>
                <xsl:call-template name="runGenerics"/>
		</AST:New>
	</AST:Throw>
</xsl:template>
<xsl:template match="AST:Throw/AST:CLASS_NAME">
	<AST:Throw>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:Throw>
</xsl:template>
<xsl:template match="AST:Throw/AST:Actual_parameter_list">
	<AST:Throw>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:Throw>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:Method_invocation">
	<AST:Actual_parameter>
		<AST:Method_invocation>
                <xsl:call-template name="runGenerics"/>
		</AST:Method_invocation>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:If/AST:Return">
	<AST:If>
		<AST:Return>
                <xsl:call-template name="runGenerics"/>
		</AST:Return>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Array_elem_list">
	<AST:If>
		<AST:Array_elem_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem_list>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Array_elem">
	<AST:If>
		<AST:Array_elem>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:INT">
	<AST:Eval_expr>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:For">
	<AST:For>
         <xsl:call-template name="runGenerics"/>
	</AST:For>
</xsl:template>
<xsl:template match="AST:Class_def/AST:For">
	<AST:Class_def>
		<AST:For>
                <xsl:call-template name="runGenerics"/>
		</AST:For>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:For/attrs">
	<AST:For>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:For>
</xsl:template>
<xsl:template match="AST:Pre_op">
	<AST:Pre_op>
         <xsl:call-template name="runGenerics"/>
	</AST:Pre_op>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Pre_op">
	<AST:Class_def>
		<AST:Pre_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Pre_op>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Pre_op/attrs">
	<AST:Pre_op>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Pre_op>
</xsl:template>
<xsl:template match="AST:Expr_list/AST:Bin_op">
	<AST:Expr_list>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:Expr_list>
</xsl:template>
<xsl:template match="AST:Bin_op/AST:Variable">
	<AST:Bin_op>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Bin_op>
</xsl:template>
<xsl:template match="AST:Bin_op/AST:Target">
	<AST:Bin_op>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Bin_op>
</xsl:template>
<xsl:template match="AST:Bin_op/AST:Expr_list">
	<AST:Bin_op>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Bin_op>
</xsl:template>
<xsl:template match="AST:Bin_op/AST:OP">
	<AST:Bin_op>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:Bin_op>
</xsl:template>
<xsl:template match="AST:Bin_op/value">
	<AST:Bin_op>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Bin_op>
</xsl:template>
<xsl:template match="AST:Bin_op/AST:INT">
	<AST:Bin_op>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:Bin_op>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Post_op">
	<AST:Eval_expr>
		<AST:Post_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Post_op>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:If/AST:Post_op">
	<AST:If>
		<AST:Post_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Post_op>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Expr_list/AST:Post_op">
	<AST:Expr_list>
		<AST:Post_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Post_op>
	</AST:Expr_list>
</xsl:template>
<xsl:template match="AST:Post_op/AST:Variable">
	<AST:Post_op>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Post_op>
</xsl:template>
<xsl:template match="AST:Post_op/AST:Target">
	<AST:Post_op>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Post_op>
</xsl:template>
<xsl:template match="AST:Post_op/AST:Expr_list">
	<AST:Post_op>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Post_op>
</xsl:template>
<xsl:template match="AST:Post_op/AST:OP">
	<AST:Post_op>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:Post_op>
</xsl:template>
<xsl:template match="AST:Formal_parameter/AST:BOOL">
	<AST:Formal_parameter>
		<AST:BOOL>
                <xsl:call-template name="runGenerics"/>
		</AST:BOOL>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Formal_parameter/value">
	<AST:Formal_parameter>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Foreach">
	<AST:Statement_list>
		<AST:Foreach>
                <xsl:call-template name="runGenerics"/>
		</AST:Foreach>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Variable">
	<AST:Foreach>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Target">
	<AST:Foreach>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Expr_list">
	<AST:Foreach>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Statement_list">
	<AST:Foreach>
		<AST:Statement_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Statement_list>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:If/AST:Break">
	<AST:If>
		<AST:Break>
                <xsl:call-template name="runGenerics"/>
		</AST:Break>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Return/AST:Actual_parameter">
	<AST:Return>
		<AST:Actual_parameter>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/bool">
	<AST:Return>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/AST:Array_elem_list">
	<AST:Return>
		<AST:Array_elem_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem_list>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Return/AST:Array_elem">
	<AST:Return>
		<AST:Array_elem>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Continue">
	<AST:Statement_list>
		<AST:Continue>
                <xsl:call-template name="runGenerics"/>
		</AST:Continue>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Continue/AST:Expr">
	<AST:Continue>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Continue>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Break">
	<AST:Class_def>
		<AST:Break>
                <xsl:call-template name="runGenerics"/>
		</AST:Break>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Static_declaration">
	<AST:Class_def>
		<AST:Static_declaration>
                <xsl:call-template name="runGenerics"/>
		</AST:Static_declaration>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Name_with_default/AST:BOOL">
	<AST:Name_with_default>
		<AST:BOOL>
                <xsl:call-template name="runGenerics"/>
		</AST:BOOL>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Throw">
	<AST:Class_def>
		<AST:Throw>
                <xsl:call-template name="runGenerics"/>
		</AST:Throw>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:New">
	<AST:Class_def>
		<AST:New>
                <xsl:call-template name="runGenerics"/>
		</AST:New>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:VARIABLE_NAME">
	<AST:Array_elem>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:METHOD_NAME">
	<AST:Array_elem>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Array_elem/AST:Actual_parameter_list">
	<AST:Array_elem>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:Array_elem>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:Array_elem_list">
	<AST:Actual_parameter>
		<AST:Array_elem_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem_list>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:Array_elem">
	<AST:Actual_parameter>
		<AST:Array_elem>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:Expr">
	<AST:Actual_parameter>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Conditional_expr">
	<AST:Class_def>
		<AST:Conditional_expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Conditional_expr>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Nop">
	<AST:Class_def>
		<AST:Nop>
                <xsl:call-template name="runGenerics"/>
		</AST:Nop>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Return/AST:CLASS_NAME">
	<AST:Return>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:INT">
	<AST:Actual_parameter>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:Pre_op">
	<AST:Eval_expr>
		<AST:Pre_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Pre_op>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Cast">
	<AST:Cast>
         <xsl:call-template name="runGenerics"/>
	</AST:Cast>
</xsl:template>
<xsl:template match="AST:Cast/attrs">
	<AST:Cast>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Cast>
</xsl:template>
<xsl:template match="AST:CAST">
	<AST:CAST>
         <xsl:call-template name="runGenerics"/>
	</AST:CAST>
</xsl:template>
<xsl:template match="AST:Class_def/AST:CAST">
	<AST:Class_def>
		<AST:CAST>
                <xsl:call-template name="runGenerics"/>
		</AST:CAST>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:CAST/attrs">
	<AST:CAST>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:CAST>
</xsl:template>
<xsl:template match="AST:If/AST:While">
	<AST:If>
		<AST:While>
                <xsl:call-template name="runGenerics"/>
		</AST:While>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Pre_op">
	<AST:If>
		<AST:Pre_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Pre_op>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Switch">
	<AST:Switch>
         <xsl:call-template name="runGenerics"/>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Switch">
	<AST:Class_def>
		<AST:Switch>
                <xsl:call-template name="runGenerics"/>
		</AST:Switch>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Switch/attrs">
	<AST:Switch>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch_case_list">
	<AST:Switch_case_list>
         <xsl:call-template name="runGenerics"/>
	</AST:Switch_case_list>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Switch_case_list">
	<AST:Class_def>
		<AST:Switch_case_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Switch_case_list>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Switch_case">
	<AST:Switch_case>
         <xsl:call-template name="runGenerics"/>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case_list/AST:Switch_case">
	<AST:Switch_case_list>
		<AST:Switch_case>
                <xsl:call-template name="runGenerics"/>
		</AST:Switch_case>
	</AST:Switch_case_list>
</xsl:template>
<xsl:template match="AST:Switch_case/attrs">
	<AST:Switch_case>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:INT">
	<AST:Switch_case>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/value">
	<AST:Switch_case>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:Statement_list">
	<AST:Switch_case>
		<AST:Statement_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Statement_list>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Switch">
	<AST:Statement_list>
		<AST:Switch>
                <xsl:call-template name="runGenerics"/>
		</AST:Switch>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Switch/AST:Variable">
	<AST:Switch>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Target">
	<AST:Switch>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Expr_list">
	<AST:Switch>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Switch_case_list">
	<AST:Switch>
		<AST:Switch_case_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Switch_case_list>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:STRING">
	<AST:Switch_case>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:Break">
	<AST:Switch_case>
		<AST:Break>
                <xsl:call-template name="runGenerics"/>
		</AST:Break>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:Expr">
	<AST:Switch_case>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch/AST:Switch_case">
	<AST:Switch>
		<AST:Switch_case>
                <xsl:call-template name="runGenerics"/>
		</AST:Switch_case>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:STRING">
	<AST:Switch>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/value">
	<AST:Switch>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Statement_list">
	<AST:Switch>
		<AST:Statement_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Statement_list>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Break">
	<AST:Switch>
		<AST:Break>
                <xsl:call-template name="runGenerics"/>
		</AST:Break>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Expr">
	<AST:Switch>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:If">
	<AST:Switch>
		<AST:If>
                <xsl:call-template name="runGenerics"/>
		</AST:If>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Bin_op">
	<AST:Switch>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:OP">
	<AST:Switch>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:BOOL">
	<AST:Switch>
		<AST:BOOL>
                <xsl:call-template name="runGenerics"/>
		</AST:BOOL>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Return">
	<AST:Switch>
		<AST:Return>
                <xsl:call-template name="runGenerics"/>
		</AST:Return>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Eval_expr">
	<AST:Switch>
		<AST:Eval_expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Eval_expr>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Assignment">
	<AST:Switch>
		<AST:Assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Assignment>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/bool">
	<AST:Switch>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Actual_parameter_list">
	<AST:Switch>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:Actual_parameter">
	<AST:Switch>
		<AST:Actual_parameter>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:INT">
	<AST:Switch>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Break/AST:INT">
	<AST:Break>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:Break>
</xsl:template>
<xsl:template match="AST:Break/value">
	<AST:Break>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Break>
</xsl:template>
<xsl:template match="AST:Switch/AST:CLASS_NAME">
	<AST:Switch>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:CONSTANT_NAME">
	<AST:Switch>
		<AST:CONSTANT_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CONSTANT_NAME>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Switch_case">
	<AST:Class_def>
		<AST:Switch_case>
                <xsl:call-template name="runGenerics"/>
		</AST:Switch_case>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:Eval_expr">
	<AST:Switch_case>
		<AST:Eval_expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Eval_expr>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:Assignment">
	<AST:Switch_case>
		<AST:Assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Assignment>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:Variable">
	<AST:Switch_case>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:Target">
	<AST:Switch_case>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:Expr_list">
	<AST:Switch_case>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/bool">
	<AST:Switch_case>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:CLASS_NAME">
	<AST:Switch_case>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:Switch_case/AST:CONSTANT_NAME">
	<AST:Switch_case>
		<AST:CONSTANT_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CONSTANT_NAME>
	</AST:Switch_case>
</xsl:template>
<xsl:template match="AST:If/AST:CONSTANT_NAME">
	<AST:If>
		<AST:CONSTANT_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CONSTANT_NAME>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Return/AST:Assignment">
	<AST:Return>
		<AST:Assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Assignment>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Actual_parameter/AST:CAST">
	<AST:Actual_parameter>
		<AST:CAST>
                <xsl:call-template name="runGenerics"/>
		</AST:CAST>
	</AST:Actual_parameter>
</xsl:template>
<xsl:template match="AST:Return/AST:NIL">
	<AST:Return>
		<AST:NIL>
                <xsl:call-template name="runGenerics"/>
		</AST:NIL>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:For">
	<AST:Statement_list>
		<AST:For>
                <xsl:call-template name="runGenerics"/>
		</AST:For>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:For/AST:Assignment">
	<AST:For>
		<AST:Assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Assignment>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/AST:Variable">
	<AST:For>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/AST:Target">
	<AST:For>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/AST:Expr_list">
	<AST:For>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/bool">
	<AST:For>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/value">
	<AST:For>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/AST:Bin_op">
	<AST:For>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/AST:OP">
	<AST:For>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/AST:Method_invocation">
	<AST:For>
		<AST:Method_invocation>
                <xsl:call-template name="runGenerics"/>
		</AST:Method_invocation>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/AST:METHOD_NAME">
	<AST:For>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/AST:Actual_parameter_list">
	<AST:For>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/AST:Post_op">
	<AST:For>
		<AST:Post_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Post_op>
	</AST:For>
</xsl:template>
<xsl:template match="AST:For/AST:Statement_list">
	<AST:For>
		<AST:Statement_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Statement_list>
	</AST:For>
</xsl:template>
<xsl:template match="AST:If/AST:Constant">
	<AST:If>
		<AST:Constant>
                <xsl:call-template name="runGenerics"/>
		</AST:Constant>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Return/AST:Array">
	<AST:Return>
		<AST:Array>
                <xsl:call-template name="runGenerics"/>
		</AST:Array>
	</AST:Return>
</xsl:template>
<xsl:template match="AST:Class_def/AST:List_assignment">
	<AST:Class_def>
		<AST:List_assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:List_assignment>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Class_def/AST:List_element_list">
	<AST:Class_def>
		<AST:List_element_list>
                <xsl:call-template name="runGenerics"/>
		</AST:List_element_list>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Expr_list/AST:Method_invocation">
	<AST:Expr_list>
		<AST:Method_invocation>
                <xsl:call-template name="runGenerics"/>
		</AST:Method_invocation>
	</AST:Expr_list>
</xsl:template>
<xsl:template match="AST:Method_invocation/AST:Variable">
	<AST:Method_invocation>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:Method_invocation>
</xsl:template>
<xsl:template match="AST:Method_invocation/AST:Target">
	<AST:Method_invocation>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:Method_invocation>
</xsl:template>
<xsl:template match="AST:Method_invocation/AST:Expr_list">
	<AST:Method_invocation>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:Method_invocation>
</xsl:template>
<xsl:template match="AST:Method_invocation/AST:METHOD_NAME">
	<AST:Method_invocation>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:Method_invocation>
</xsl:template>
<xsl:template match="AST:Method_invocation/value">
	<AST:Method_invocation>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Method_invocation>
</xsl:template>
<xsl:template match="AST:Method_invocation/AST:Actual_parameter_list">
	<AST:Method_invocation>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:Method_invocation>
</xsl:template>
<xsl:template match="AST:Foreach/bool">
	<AST:Foreach>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Return/AST:BOOL">
	<AST:Return>
		<AST:BOOL>
                <xsl:call-template name="runGenerics"/>
		</AST:BOOL>
	</AST:Return>
</xsl:template>
<xsl:template match="attrs/AST:VARIABLE_NAME">
	<attrs>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</attrs>
</xsl:template>
<xsl:template match="AST:For/AST:INT">
	<AST:For>
		<AST:INT>
                <xsl:call-template name="runGenerics"/>
		</AST:INT>
	</AST:For>
</xsl:template>
<xsl:template match="AST:Ignore_errors">
	<AST:Ignore_errors>
         <xsl:call-template name="runGenerics"/>
	</AST:Ignore_errors>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Ignore_errors">
	<AST:Class_def>
		<AST:Ignore_errors>
                <xsl:call-template name="runGenerics"/>
		</AST:Ignore_errors>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Ignore_errors/attrs">
	<AST:Ignore_errors>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Ignore_errors>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Nop">
	<AST:Statement_list>
		<AST:Nop>
                <xsl:call-template name="runGenerics"/>
		</AST:Nop>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:NIL">
	<AST:Eval_expr>
		<AST:NIL>
                <xsl:call-template name="runGenerics"/>
		</AST:NIL>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Name_with_default/AST:Array">
	<AST:Name_with_default>
		<AST:Array>
                <xsl:call-template name="runGenerics"/>
		</AST:Array>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:Name_with_default/AST:Array_elem_list">
	<AST:Name_with_default>
		<AST:Array_elem_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem_list>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:Name_with_default/AST:Array_elem">
	<AST:Name_with_default>
		<AST:Array_elem>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:Name_with_default/bool">
	<AST:Name_with_default>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Name_with_default>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Eval_expr">
	<AST:Foreach>
		<AST:Eval_expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Eval_expr>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Assignment">
	<AST:Foreach>
		<AST:Assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Assignment>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Array_elem_list">
	<AST:Foreach>
		<AST:Array_elem_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem_list>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Array_elem">
	<AST:Foreach>
		<AST:Array_elem>
                <xsl:call-template name="runGenerics"/>
		</AST:Array_elem>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Expr">
	<AST:Foreach>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Switch/AST:METHOD_NAME">
	<AST:Switch>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Switch/AST:VARIABLE_NAME">
	<AST:Switch>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Switch>
</xsl:template>
<xsl:template match="AST:Eval_expr/AST:BOOL">
	<AST:Eval_expr>
		<AST:BOOL>
                <xsl:call-template name="runGenerics"/>
		</AST:BOOL>
	</AST:Eval_expr>
</xsl:template>
<xsl:template match="AST:Formal_parameter/AST:NIL">
	<AST:Formal_parameter>
		<AST:NIL>
                <xsl:call-template name="runGenerics"/>
		</AST:NIL>
	</AST:Formal_parameter>
</xsl:template>
<xsl:template match="AST:Statement_list/AST:While">
	<AST:Statement_list>
		<AST:While>
                <xsl:call-template name="runGenerics"/>
		</AST:While>
	</AST:Statement_list>
</xsl:template>
<xsl:template match="AST:While/AST:Variable">
	<AST:While>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:Target">
	<AST:While>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:Expr_list">
	<AST:While>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:Statement_list">
	<AST:While>
		<AST:Statement_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Statement_list>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:If">
	<AST:While>
		<AST:If>
                <xsl:call-template name="runGenerics"/>
		</AST:If>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:Bin_op">
	<AST:While>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:OP">
	<AST:While>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/value">
	<AST:While>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:STRING">
	<AST:While>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:Eval_expr">
	<AST:While>
		<AST:Eval_expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Eval_expr>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:Assignment">
	<AST:While>
		<AST:Assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Assignment>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/bool">
	<AST:While>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:METHOD_NAME">
	<AST:While>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:While>
</xsl:template>
<xsl:template match="AST:While/AST:Actual_parameter_list">
	<AST:While>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:While>
</xsl:template>
<xsl:template match="AST:Do">
	<AST:Do>
         <xsl:call-template name="runGenerics"/>
	</AST:Do>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Do">
	<AST:Class_def>
		<AST:Do>
                <xsl:call-template name="runGenerics"/>
		</AST:Do>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Do/attrs">
	<AST:Do>
		<attrs>
                <xsl:call-template name="runGenerics"/>
		</attrs>
	</AST:Do>
</xsl:template>
<xsl:template match="AST:If/AST:Global">
	<AST:If>
		<AST:Global>
                <xsl:call-template name="runGenerics"/>
		</AST:Global>
	</AST:If>
</xsl:template>
<xsl:template match="AST:If/AST:Variable_name_list">
	<AST:If>
		<AST:Variable_name_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable_name_list>
	</AST:If>
</xsl:template>
<xsl:template match="AST:Class_def/AST:Instanceof">
	<AST:Class_def>
		<AST:Instanceof>
                <xsl:call-template name="runGenerics"/>
		</AST:Instanceof>
	</AST:Class_def>
</xsl:template>
<xsl:template match="AST:Foreach/AST:VARIABLE_NAME">
	<AST:Foreach>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Actual_parameter_list">
	<AST:Foreach>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Actual_parameter">
	<AST:Foreach>
		<AST:Actual_parameter>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:Bin_op">
	<AST:Foreach>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:STRING">
	<AST:Foreach>
		<AST:STRING>
                <xsl:call-template name="runGenerics"/>
		</AST:STRING>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/value">
	<AST:Foreach>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:Foreach/AST:OP">
	<AST:Foreach>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:Foreach>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Class_def">
	<AST:PHP_script>
		<AST:Class_def>
                <xsl:call-template name="runGenerics"/>
		</AST:Class_def>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Class_mod">
	<AST:PHP_script>
		<AST:Class_mod>
                <xsl:call-template name="runGenerics"/>
		</AST:Class_mod>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/bool">
	<AST:PHP_script>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:CLASS_NAME">
	<AST:PHP_script>
		<AST:CLASS_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:CLASS_NAME>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:Attribute/bool">
	<AST:Attribute>
		<bool>
                <xsl:call-template name="runGenerics"/>
		</bool>
	</AST:Attribute>
</xsl:template>
<xsl:template match="AST:Attribute/AST:Name_with_default">
	<AST:Attribute>
		<AST:Name_with_default>
                <xsl:call-template name="runGenerics"/>
		</AST:Name_with_default>
	</AST:Attribute>
</xsl:template>
<xsl:template match="AST:Attribute/AST:VARIABLE_NAME">
	<AST:Attribute>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:Attribute>
</xsl:template>
<xsl:template match="AST:Attribute/AST:Expr">
	<AST:Attribute>
		<AST:Expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr>
	</AST:Attribute>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Method">
	<AST:PHP_script>
		<AST:Method>
                <xsl:call-template name="runGenerics"/>
		</AST:Method>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Signature">
	<AST:PHP_script>
		<AST:Signature>
                <xsl:call-template name="runGenerics"/>
		</AST:Signature>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Method_mod">
	<AST:PHP_script>
		<AST:Method_mod>
                <xsl:call-template name="runGenerics"/>
		</AST:Method_mod>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Formal_parameter_list">
	<AST:PHP_script>
		<AST:Formal_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Formal_parameter_list>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Eval_expr">
	<AST:PHP_script>
		<AST:Eval_expr>
                <xsl:call-template name="runGenerics"/>
		</AST:Eval_expr>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Assignment">
	<AST:PHP_script>
		<AST:Assignment>
                <xsl:call-template name="runGenerics"/>
		</AST:Assignment>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Variable">
	<AST:PHP_script>
		<AST:Variable>
                <xsl:call-template name="runGenerics"/>
		</AST:Variable>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Target">
	<AST:PHP_script>
		<AST:Target>
                <xsl:call-template name="runGenerics"/>
		</AST:Target>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Expr_list">
	<AST:PHP_script>
		<AST:Expr_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Expr_list>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:VARIABLE_NAME">
	<AST:PHP_script>
		<AST:VARIABLE_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:VARIABLE_NAME>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Actual_parameter_list">
	<AST:PHP_script>
		<AST:Actual_parameter_list>
                <xsl:call-template name="runGenerics"/>
		</AST:Actual_parameter_list>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Do">
	<AST:PHP_script>
		<AST:Do>
                <xsl:call-template name="runGenerics"/>
		</AST:Do>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:METHOD_NAME">
	<AST:PHP_script>
		<AST:METHOD_NAME>
                <xsl:call-template name="runGenerics"/>
		</AST:METHOD_NAME>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/value">
	<AST:PHP_script>
		<value>
                <xsl:call-template name="runGenerics"/>
		</value>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Bin_op">
	<AST:PHP_script>
		<AST:Bin_op>
                <xsl:call-template name="runGenerics"/>
		</AST:Bin_op>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:OP">
	<AST:PHP_script>
		<AST:OP>
                <xsl:call-template name="runGenerics"/>
		</AST:OP>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Method_invocation">
	<AST:PHP_script>
		<AST:Method_invocation>
                <xsl:call-template name="runGenerics"/>
		</AST:Method_invocation>
	</AST:PHP_script>
</xsl:template>
<xsl:template match="AST:PHP_script/AST:Return">
	<AST:PHP_script>
		<AST:Return>
                <xsl:call-template name="runGenerics"/>
		</AST:Return>
	</AST:PHP_script>
</xsl:template>
</xsl:stylesheet>