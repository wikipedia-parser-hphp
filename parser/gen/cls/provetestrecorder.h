
#ifndef __GENERATED_cls_provetestrecorder_h__
#define __GENERATED_cls_provetestrecorder_h__

#include <cls/testrecorder.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 45007 */
class c_provetestrecorder : virtual public c_testrecorder {
  BEGIN_CLASS_MAP(provetestrecorder)
    PARENT_CLASS(testrecorder)
  END_CLASS_MAP(provetestrecorder)
  DECLARE_CLASS(provetestrecorder, ProveTestRecorder, testrecorder)
  void init();
  public: void t_record(CVarRef v_name, CVarRef v_res);
  public: void t_report();
  public: void t_reportpercentage(CVarRef v_success, CVarRef v_total);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_provetestrecorder_h__
