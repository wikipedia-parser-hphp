
#ifndef __GENERATED_cls_profiler_h__
#define __GENERATED_cls_profiler_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 11300 */
class c_profiler : virtual public ObjectData {
  BEGIN_CLASS_MAP(profiler)
  END_CLASS_MAP(profiler)
  DECLARE_CLASS(profiler, Profiler, ObjectData)
  void init();
  public: Variant m_mStack;
  public: Variant m_mWorkStack;
  public: Variant m_mCollated;
  public: Variant m_mCalls;
  public: Variant m_mTotals;
  public: Variant m_mDone;
  public: void t_profilein(CVarRef v_functionname);
  public: void t_profileout(CVarRef v_functionname);
  public: void t_close();
  public: String t_getoutput(CVarRef v_scriptStart, CVarRef v_scriptElapsed);
  public: void t_logtodb(Variant v_name, double v_timeSum, CVarRef v_eventCount);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_profiler_h__
