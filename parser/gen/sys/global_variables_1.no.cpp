
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 2047) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      HASH_RETURN(0x3489E1DAAEB9D801LL, g->GV(wgUsePrivateIPs),
                  wgUsePrivateIPs);
      HASH_RETURN(0x4EE2265B1056F801LL, g->GV(wgNoticeCentralPath),
                  wgNoticeCentralPath);
      break;
    case 2:
      HASH_RETURN(0x4B565810548AF002LL, g->GV(wgRightsIcon),
                  wgRightsIcon);
      break;
    case 4:
      HASH_RETURN(0x702395D437CE0804LL, g->GV(wgPutIPinRC),
                  wgPutIPinRC);
      break;
    case 5:
      HASH_RETURN(0x6247B8A9EC4A3805LL, g->GV(wmgUseConfigure),
                  wmgUseConfigure);
      break;
    case 8:
      HASH_RETURN(0x5CEFA3DBDDD90808LL, g->GV(wmgCaptchaPassword),
                  wmgCaptchaPassword);
      break;
    case 9:
      HASH_RETURN(0x2BF360B54C532809LL, g->GV(wgLanguageCode),
                  wgLanguageCode);
      break;
    case 10:
      HASH_RETURN(0x62566BC9A306C80ALL, g->GV(wgAllowDisplayTitle),
                  wgAllowDisplayTitle);
      break;
    case 21:
      HASH_RETURN(0x5A724F15A2A6A815LL, g->GV(wgUseETag),
                  wgUseETag);
      break;
    case 26:
      HASH_RETURN(0x0C17BACBF76A681ALL, g->GV(wgCollectionPortletForLoggedInUsersOnly),
                  wgCollectionPortletForLoggedInUsersOnly);
      break;
    case 27:
      HASH_RETURN(0x6E024F7A960D681BLL, g->GV(wgParserTestFiles),
                  wgParserTestFiles);
      break;
    case 38:
      HASH_RETURN(0x66C2DDC470F86826LL, g->GV(wgMessageCache),
                  wgMessageCache);
      HASH_RETURN(0x541F0DE9DE5A6826LL, g->GV(wgParserConf),
                  wgParserConf);
      break;
    case 50:
      HASH_RETURN(0x084E4941DD10F032LL, g->GV(wgCaptchaSecret),
                  wgCaptchaSecret);
      HASH_RETURN(0x463F4876FD995032LL, g->GV(wgParserCacheType),
                  wgParserCacheType);
      break;
    case 52:
      HASH_RETURN(0x4E578E2372DC9834LL, g->GV(wgMiserMode),
                  wgMiserMode);
      break;
    case 67:
      HASH_RETURN(0x0A779CC563328843LL, g->GV(dbHostsByName),
                  dbHostsByName);
      break;
    case 69:
      HASH_RETURN(0x7047E2A1BA590845LL, g->GV(wgMemCachedServers),
                  wgMemCachedServers);
      break;
    case 74:
      HASH_RETURN(0x323EAC15D211984ALL, g->GV(wgContLang),
                  wgContLang);
      break;
    case 75:
      HASH_RETURN(0x7A0C10E3609EA04BLL, g->GV(m),
                  m);
      break;
    case 77:
      HASH_RETURN(0x24761D49EE10584DLL, g->GV(wgUseCommaCount),
                  wgUseCommaCount);
      break;
    case 78:
      HASH_RETURN(0x5AD336E1B2E5304ELL, g->GV(wgMaxCredits),
                  wgMaxCredits);
      break;
    case 83:
      HASH_RETURN(0x749CA466E7076053LL, g->GV(wgConfigureWikis),
                  wgConfigureWikis);
      break;
    case 90:
      HASH_RETURN(0x306910A71CB1B05ALL, g->GV(oaiAgentRegex),
                  oaiAgentRegex);
      break;
    case 101:
      HASH_RETURN(0x4A97AADAA8A30865LL, g->GV(wgSiteMatrixFile),
                  wgSiteMatrixFile);
      break;
    case 106:
      HASH_RETURN(0x55AA9BD92D7FB86ALL, g->GV(wmgUseDPL),
                  wmgUseDPL);
      break;
    case 108:
      HASH_RETURN(0x0318A2A42E99B86CLL, g->GV(wgLegalTitleChars),
                  wgLegalTitleChars);
      HASH_RETURN(0x00F2FF20EE9C206CLL, g->GV(wgImageMagickTempDir),
                  wgImageMagickTempDir);
      break;
    case 111:
      HASH_RETURN(0x56448467D226A06FLL, g->GV(wgTmpDirectory),
                  wgTmpDirectory);
      break;
    case 116:
      HASH_RETURN(0x01A194ACDAA8F874LL, g->GV(wgEnableDublinCoreRdf),
                  wgEnableDublinCoreRdf);
      break;
    case 120:
      HASH_RETURN(0x2BD015092DD2D878LL, g->GV(wgTorDisableAdminBlocks),
                  wgTorDisableAdminBlocks);
      break;
    case 129:
      HASH_RETURN(0x6532D7EA771A6881LL, g->GV(wgEnableScaryTranscluding),
                  wgEnableScaryTranscluding);
      break;
    case 135:
      HASH_RETURN(0x476478C07603C087LL, g->GV(wgCortadoJarFile),
                  wgCortadoJarFile);
      break;
    case 139:
      HASH_RETURN(0x4E4D4C1ADC1F608BLL, g->GV(wgHtmlEntities),
                  wgHtmlEntities);
      break;
    case 143:
      HASH_RETURN(0x0E6BCF90EB2C208FLL, g->GV(oaiAudit),
                  oaiAudit);
      break;
    case 147:
      HASH_RETURN(0x47A9C82B74E22893LL, g->GV(wgInvalidRedirectTargets),
                  wgInvalidRedirectTargets);
      break;
    case 151:
      HASH_RETURN(0x0FF89CB119FDF097LL, g->GV(wgMetaNamespaceTalk),
                  wgMetaNamespaceTalk);
      HASH_RETURN(0x043D47B39C45D097LL, g->GV(wgDeleteRevisionsLimit),
                  wgDeleteRevisionsLimit);
      break;
    case 156:
      HASH_RETURN(0x0E952EE1350A109CLL, g->GV(wgPasswordSalt),
                  wgPasswordSalt);
      break;
    case 157:
      HASH_RETURN(0x58BD8A13ED27809DLL, g->GV(wgScanSetSettings),
                  wgScanSetSettings);
      break;
    case 159:
      HASH_RETURN(0x786379403A37E89FLL, g->GV(wgAllowExternalImagesFrom),
                  wgAllowExternalImagesFrom);
      break;
    case 163:
      HASH_RETURN(0x22BB9E58DA1D68A3LL, g->GV(wgMaxTemplateDepth),
                  wgMaxTemplateDepth);
      break;
    case 165:
      HASH_RETURN(0x39CB51FC012448A5LL, g->GV(wgDebugLogPrefix),
                  wgDebugLogPrefix);
      break;
    case 169:
      HASH_RETURN(0x4E365B3FC4B038A9LL, g->GV(wgSidebarCacheExpiry),
                  wgSidebarCacheExpiry);
      break;
    case 170:
      HASH_RETURN(0x0941B2D54082A0AALL, g->GV(wgLocalDatabases),
                  wgLocalDatabases);
      break;
    case 175:
      HASH_RETURN(0x1A332E88BF1E88AFLL, g->GV(wgNoticeInfrastructure),
                  wgNoticeInfrastructure);
      break;
    case 177:
      HASH_RETURN(0x0B12006D1EA388B1LL, g->GV(wgGlobalBlockingDatabase),
                  wgGlobalBlockingDatabase);
      break;
    case 178:
      HASH_RETURN(0x396833BC2B5C48B2LL, g->GV(wgCookieSecure),
                  wgCookieSecure);
      break;
    case 181:
      HASH_RETURN(0x35BC227B49E6F0B5LL, g->GV(wgTorAutoConfirmCount),
                  wgTorAutoConfirmCount);
      break;
    case 186:
      HASH_RETURN(0x5E2E485985C6A8BALL, g->GV(wgRestrictionLevels),
                  wgRestrictionLevels);
      break;
    case 187:
      HASH_RETURN(0x690A6A790E4710BBLL, g->GV(tester),
                  tester);
      break;
    case 188:
      HASH_RETURN(0x5B097BD3B4EE58BCLL, g->GV(wgDefaultRobotPolicy),
                  wgDefaultRobotPolicy);
      break;
    case 190:
      HASH_RETURN(0x359A82D4CB6EF0BELL, g->GV(wgMaxNameChars),
                  wgMaxNameChars);
      break;
    case 198:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 206:
      HASH_RETURN(0x32E38C199DC208CELL, g->GV(wgCleanSignatures),
                  wgCleanSignatures);
      break;
    case 209:
      HASH_RETURN(0x598250509CB268D1LL, g->GV(wgCollectionArticleNamespaces),
                  wgCollectionArticleNamespaces);
      break;
    case 211:
      HASH_RETURN(0x288CFF53F9D588D3LL, g->GV(wgStatsMethod),
                  wgStatsMethod);
      break;
    case 222:
      HASH_RETURN(0x30BAA6EB778E50DELL, g->GV(sectionsByDB),
                  sectionsByDB);
      break;
    case 225:
      HASH_RETURN(0x056EF475084230E1LL, g->GV(wgThumbUpright),
                  wgThumbUpright);
      break;
    case 226:
      HASH_RETURN(0x2119480B686F60E2LL, g->GV(wgRequest),
                  wgRequest);
      break;
    case 227:
      HASH_RETURN(0x53CCC76EF999B0E3LL, g->GV(wgContentNamespaces),
                  wgContentNamespaces);
      HASH_RETURN(0x02FD34D874E9A8E3LL, g->GV(wgRC2UDPAddress),
                  wgRC2UDPAddress);
      break;
    case 229:
      HASH_RETURN(0x44596B472FF3D8E5LL, g->GV(wgDBerrorLog),
                  wgDBerrorLog);
      break;
    case 233:
      HASH_RETURN(0x67584BCA34BB58E9LL, g->GV(wgThumbLimits),
                  wgThumbLimits);
      break;
    case 236:
      HASH_RETURN(0x01813D410134A0ECLL, g->GV(wgProxyList),
                  wgProxyList);
      HASH_RETURN(0x4F935E42020C60ECLL, g->GV(wgHTCPMulticastTTL),
                  wgHTCPMulticastTTL);
      break;
    case 238:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 243:
      HASH_RETURN(0x37C0CC66C852F0F3LL, g->GV(wgUseSquid),
                  wgUseSquid);
      break;
    case 253:
      HASH_RETURN(0x6809574ACEE3C0FDLL, g->GV(wgNamespacesToBeSearchedDefault),
                  wgNamespacesToBeSearchedDefault);
      break;
    case 255:
      HASH_RETURN(0x0718A6BE876748FFLL, g->GV(wgHandheldStyle),
                  wgHandheldStyle);
      break;
    case 259:
      HASH_RETURN(0x704973FDADC9F903LL, g->GV(oldtz),
                  oldtz);
      break;
    case 263:
      HASH_RETURN(0x26A04AAD2ECBF907LL, g->GV(wmgNoticeProject),
                  wmgNoticeProject);
      break;
    case 275:
      HASH_RETURN(0x371D989211310913LL, g->GV(wgForeignFileRepos),
                  wgForeignFileRepos);
      break;
    case 282:
      HASH_RETURN(0x6607BB576509A11ALL, g->GV(wgCentralNoticeLoader),
                  wgCentralNoticeLoader);
      break;
    case 288:
      HASH_RETURN(0x317F209AB8417920LL, g->GV(wgProfileOnly),
                  wgProfileOnly);
      break;
    case 296:
      HASH_RETURN(0x731BDF51B2005928LL, g->GV(pathBits),
                  pathBits);
      HASH_RETURN(0x0655274EEB132128LL, g->GV(wgCaptchaWhitelist),
                  wgCaptchaWhitelist);
      break;
    case 298:
      HASH_RETURN(0x087D95440560592ALL, g->GV(tag),
                  tag);
      break;
    case 300:
      HASH_RETURN(0x2D7B6BCCE049292CLL, g->GV(wgMaximumMovedPages),
                  wgMaximumMovedPages);
      break;
    case 304:
      HASH_RETURN(0x0160FF6D2883D930LL, g->GV(wgAlternateMaster),
                  wgAlternateMaster);
      break;
    case 306:
      HASH_RETURN(0x798D683865985932LL, g->GV(wgAntiBotPayloads),
                  wgAntiBotPayloads);
      break;
    case 320:
      HASH_RETURN(0x0CD3659593691940LL, g->GV(wgDisableOutputCompression),
                  wgDisableOutputCompression);
      break;
    case 322:
      HASH_RETURN(0x3F3206577A7C7142LL, g->GV(wgApplyGlobalBlocks),
                  wgApplyGlobalBlocks);
      break;
    case 324:
      HASH_RETURN(0x3175AF6E6A7E2944LL, g->GV(wgLogQueries),
                  wgLogQueries);
      break;
    case 326:
      HASH_RETURN(0x222B475F80948946LL, g->GV(wgShowHostnames),
                  wgShowHostnames);
      break;
    case 328:
      HASH_RETURN(0x02995E0E4820C948LL, g->GV(wgExternalDiffEngine),
                  wgExternalDiffEngine);
      break;
    case 329:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      HASH_RETURN(0x12FE343C42677949LL, g->GV(wgRC2UDPPrefix),
                  wgRC2UDPPrefix);
      break;
    case 332:
      HASH_RETURN(0x7ABD1C02DBBBE94CLL, g->GV(IP),
                  IP);
      break;
    case 336:
      HASH_RETURN(0x342253A26CE6A950LL, g->GV(wmgUseSpamBlacklist),
                  wmgUseSpamBlacklist);
      break;
    case 337:
      HASH_RETURN(0x4CCA7F022EF68151LL, g->GV(action),
                  action);
      break;
    case 346:
      HASH_RETURN(0x0874F6116DC3395ALL, g->GV(ubUploadBlacklist),
                  ubUploadBlacklist);
      break;
    case 354:
      HASH_RETURN(0x49148B0ABD57D162LL, g->GV(wgProfiling),
                  wgProfiling);
      break;
    case 355:
      HASH_RETURN(0x71BEDBEC9F160163LL, g->GV(wgNoDBParam),
                  wgNoDBParam);
      break;
    case 359:
      HASH_RETURN(0x46BE06983A134967LL, g->GV(wgAllowRealName),
                  wgAllowRealName);
      break;
    case 360:
      HASH_RETURN(0x6A0CB69B6A85A968LL, g->GV(wgExtraLanguageNames),
                  wgExtraLanguageNames);
      break;
    case 362:
      HASH_RETURN(0x5080CF3E8A31616ALL, g->GV(wgProxyMemcExpiry),
                  wgProxyMemcExpiry);
      break;
    case 368:
      HASH_RETURN(0x252B4126A4832970LL, g->GV(space),
                  space);
      break;
    case 378:
      HASH_RETURN(0x0944E9861083597ALL, g->GV(wgProxyWhitelist),
                  wgProxyWhitelist);
      break;
    case 383:
      HASH_RETURN(0x7BB3365BD986697FLL, g->GV(wgDBtype),
                  wgDBtype);
      break;
    case 386:
      HASH_RETURN(0x3A8586F4FA6A2182LL, g->GV(wgExtensionAliasesFiles),
                  wgExtensionAliasesFiles);
      break;
    case 390:
      HASH_RETURN(0x4A69C8540FC61186LL, g->GV(wgLogRestrictions),
                  wgLogRestrictions);
      break;
    case 391:
      HASH_RETURN(0x5E016D8F68012187LL, g->GV(wgUseRCPatrol),
                  wgUseRCPatrol);
      break;
    case 396:
      HASH_RETURN(0x324C621B7AAEB18CLL, g->GV(wgTrustedXffFile),
                  wgTrustedXffFile);
      break;
    case 404:
      HASH_RETURN(0x46C31602DD91A194LL, g->GV(wgRCMaxAge),
                  wgRCMaxAge);
      break;
    case 408:
      HASH_RETURN(0x79D568D750EAD998LL, g->GV(dblist),
                  dblist);
      HASH_RETURN(0x05F8481CC363A998LL, g->GV(groupOverrides),
                  groupOverrides);
      break;
    case 414:
      HASH_RETURN(0x4AF7CD17F976719ELL, g->GV(args),
                  args);
      break;
    case 423:
      HASH_RETURN(0x7CAEA4CF6345A1A7LL, g->GV(wgUseESI),
                  wgUseESI);
      break;
    case 433:
      HASH_RETURN(0x504530FC8E84C1B1LL, g->GV(wgExtDistRemoteClient),
                  wgExtDistRemoteClient);
      break;
    case 440:
      HASH_RETURN(0x6CF4B270FE1741B8LL, g->GV(wgRC2UDPPort),
                  wgRC2UDPPort);
      break;
    case 442:
      HASH_RETURN(0x7A6C9EDA8F8F61BALL, g->GV(wgUseTrackbacks),
                  wgUseTrackbacks);
      break;
    case 444:
      HASH_RETURN(0x38B5A28E5BF7C9BCLL, g->GV(wgReservedUsernames),
                  wgReservedUsernames);
      break;
    case 447:
      HASH_RETURN(0x1AAC778B50B241BFLL, g->GV(wgLocalTZoffset),
                  wgLocalTZoffset);
      HASH_RETURN(0x512BB6600AF1A1BFLL, g->GV(wgCacheEpoch),
                  wgCacheEpoch);
      break;
    case 452:
      HASH_RETURN(0x300631D93FA6A1C4LL, g->GV(utfCompatibilityDecomp),
                  utfCompatibilityDecomp);
      break;
    case 453:
      HASH_RETURN(0x36E1EFD4997309C5LL, g->GV(wgNewPasswordExpiry),
                  wgNewPasswordExpiry);
      break;
    case 457:
      HASH_RETURN(0x2DE0DD0CA196D1C9LL, g->GV(fname),
                  fname);
      break;
    case 461:
      HASH_RETURN(0x12D237609C7569CDLL, g->GV(wgExtDistTarDir),
                  wgExtDistTarDir);
      break;
    case 463:
      HASH_RETURN(0x43AF62D963A679CFLL, g->GV(wgFileExtensions),
                  wgFileExtensions);
      break;
    case 465:
      HASH_RETURN(0x0BEB6D57C677B1D1LL, g->GV(wgEnableCreativeCommonsRdf),
                  wgEnableCreativeCommonsRdf);
      break;
    case 467:
      HASH_RETURN(0x2D9BE538C72C31D3LL, g->GV(wgProxyKey),
                  wgProxyKey);
      break;
    case 483:
      HASH_RETURN(0x37AC4209571FD9E3LL, g->GV(wgEnableSorbs),
                  wgEnableSorbs);
      break;
    case 490:
      HASH_RETURN(0x7C17922060DCA1EALL, g->GV(options),
                  options);
      break;
    case 491:
      HASH_RETURN(0x39637C555608F9EBLL, g->GV(oaiAuditDatabase),
                  oaiAuditDatabase);
      break;
    case 495:
      HASH_RETURN(0x19E02A72D4E221EFLL, g->GV(wgDiff),
                  wgDiff);
      break;
    case 498:
      HASH_RETURN(0x176EC09AB1E071F2LL, g->GV(wgClockSkewFudge),
                  wgClockSkewFudge);
      break;
    case 499:
      HASH_RETURN(0x6CD674E44D4749F3LL, g->GV(wgCentralAuthAutoNew),
                  wgCentralAuthAutoNew);
      break;
    case 504:
      HASH_RETURN(0x2E3A9BE7A3D449F8LL, g->GV(wgLang),
                  wgLang);
      break;
    case 506:
      HASH_RETURN(0x25D14844D69061FALL, g->GV(parserMemc),
                  parserMemc);
      break;
    case 512:
      HASH_RETURN(0x68DE5EDB9899D200LL, g->GV(wgNewUserSuppressRC),
                  wgNewUserSuppressRC);
      break;
    case 516:
      HASH_RETURN(0x7C2603F6EF23FA04LL, g->GV(wgArticlePath),
                  wgArticlePath);
      break;
    case 521:
      HASH_RETURN(0x2E5816FC6A54EA09LL, g->GV(sectionLoads),
                  sectionLoads);
      break;
    case 526:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 528:
      HASH_RETURN(0x58166C9285F7E210LL, g->GV(wgJsMimeType),
                  wgJsMimeType);
      break;
    case 529:
      HASH_RETURN(0x19950A9D826D7211LL, g->GV(wgMaxSquidPurgeTitles),
                  wgMaxSquidPurgeTitles);
      break;
    case 531:
      HASH_RETURN(0x66C3B564F57C3A13LL, g->GV(wgSquidServersNoPurge),
                  wgSquidServersNoPurge);
      HASH_RETURN(0x5C2714389994FA13LL, g->GV(wgHTCPMulticastAddress),
                  wgHTCPMulticastAddress);
      break;
    case 533:
      HASH_RETURN(0x7C958444EC769A15LL, g->GV(wgExceptionHooks),
                  wgExceptionHooks);
      break;
    case 536:
      HASH_RETURN(0x1BA686401B287A18LL, g->GV(wgCentralAuthUDPAddress),
                  wgCentralAuthUDPAddress);
      break;
    case 545:
      HASH_RETURN(0x112599DDD6071221LL, g->GV(wgUseGzip),
                  wgUseGzip);
      break;
    case 546:
      HASH_RETURN(0x737101E2E1EEF222LL, g->GV(wgImplicitGroups),
                  wgImplicitGroups);
      break;
    case 548:
      HASH_RETURN(0x2F4C917F2B883224LL, g->GV(wgCentralAuthAutoLoginWikis),
                  wgCentralAuthAutoLoginWikis);
      break;
    case 551:
      HASH_RETURN(0x3139086411CA2A27LL, g->GV(wgUseImageResize),
                  wgUseImageResize);
      break;
    case 552:
      HASH_RETURN(0x0D276462B26E8228LL, g->GV(wmgCollectionPortletForLoggedInUsersOnly),
                  wmgCollectionPortletForLoggedInUsersOnly);
      break;
    case 553:
      HASH_RETURN(0x623376B75256A229LL, g->GV(wgMessageCacheType),
                  wgMessageCacheType);
      break;
    case 555:
      HASH_RETURN(0x7C821970ED7BBA2BLL, g->GV(utfCombiningClass),
                  utfCombiningClass);
      break;
    case 560:
      HASH_RETURN(0x200D4EA31F5EFA30LL, g->GV(wmgCentralNoticeLoader),
                  wmgCentralNoticeLoader);
      break;
    case 563:
      HASH_RETURN(0x34EF5D609D7E4A33LL, g->GV(wgTitleBlacklistSources),
                  wgTitleBlacklistSources);
      break;
    case 564:
      HASH_RETURN(0x1E06D47E4EEC3234LL, g->GV(wgDirectoryMode),
                  wgDirectoryMode);
      break;
    case 567:
      HASH_RETURN(0x5E4CF831C9C58A37LL, g->GV(wgConf),
                  wgConf);
      break;
    case 572:
      HASH_RETURN(0x2F9D7D9C64E4223CLL, g->GV(wgAjaxWatch),
                  wgAjaxWatch);
      break;
    case 584:
      HASH_RETURN(0x203046DEC44DEA48LL, g->GV(wgAllowImageMoving),
                  wgAllowImageMoving);
      break;
    case 587:
      HASH_RETURN(0x55731FE54621BA4BLL, g->GV(wgUseEnotif),
                  wgUseEnotif);
      break;
    case 588:
      HASH_RETURN(0x0F1A108CC0649A4CLL, g->GV(wgHTTPTimeout),
                  wgHTTPTimeout);
      break;
    case 589:
      HASH_RETURN(0x360B6AC185D6F24DLL, g->GV(wgHtmlEntityAliases),
                  wgHtmlEntityAliases);
      break;
    case 611:
      HASH_RETURN(0x655D6045889F7A63LL, g->GV(wgUseAutomaticEditSummaries),
                  wgUseAutomaticEditSummaries);
      break;
    case 612:
      HASH_RETURN(0x3974C065342F2A64LL, g->GV(wmgNewUserMinorEdit),
                  wmgNewUserMinorEdit);
      break;
    case 614:
      HASH_RETURN(0x6C7F5992B8026A66LL, g->GV(wgXhtmlDefaultNamespace),
                  wgXhtmlDefaultNamespace);
      break;
    case 616:
      HASH_RETURN(0x0E12BE46FD64D268LL, g->GV(day),
                  day);
      break;
    case 621:
      HASH_RETURN(0x3BABC4C6EB7CDA6DLL, g->GV(group),
                  group);
      break;
    case 622:
      HASH_RETURN(0x0151ECCE23B73A6ELL, g->GV(wgUseAjax),
                  wgUseAjax);
      HASH_RETURN(0x48D4C1D2A849126ELL, g->GV(wgEnableMWSuggest),
                  wgEnableMWSuggest);
      break;
    case 624:
      HASH_RETURN(0x3902BABD6AC8B270LL, g->GV(wgReadOnly),
                  wgReadOnly);
      break;
    case 635:
      HASH_RETURN(0x11B7E569AC1CB27BLL, g->GV(wgEnforceHtmlIds),
                  wgEnforceHtmlIds);
      break;
    case 646:
      HASH_RETURN(0x5579733B1E941A86LL, g->GV(wgRateLimits),
                  wgRateLimits);
      break;
    case 651:
      HASH_RETURN(0x798D3418DBF5328BLL, g->GV(wgDiff3),
                  wgDiff3);
      break;
    case 652:
      HASH_RETURN(0x337B054C2BC42A8CLL, g->GV(wgDefaultExternalStore),
                  wgDefaultExternalStore);
      break;
    case 655:
      HASH_RETURN(0x19BE06787780628FLL, g->GV(wgRateLimitsExcludedGroups),
                  wgRateLimitsExcludedGroups);
      HASH_RETURN(0x0343543E7359CA8FLL, g->GV(wgCentralAuthCookies),
                  wgCentralAuthCookies);
      break;
    case 656:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 657:
      HASH_RETURN(0x12CC6E807F0AEA91LL, g->GV(wgValidSkinNames),
                  wgValidSkinNames);
      break;
    case 660:
      HASH_RETURN(0x3F7D508716303A94LL, g->GV(wgDisableLangConversion),
                  wgDisableLangConversion);
      break;
    case 665:
      HASH_RETURN(0x70D115E2BCD17A99LL, g->GV(wgConfigureFileSystemCache),
                  wgConfigureFileSystemCache);
      break;
    case 679:
      HASH_RETURN(0x2DABB6595CC2C2A7LL, g->GV(wgUseRootUser),
                  wgUseRootUser);
      break;
    case 680:
      HASH_RETURN(0x0EB9D0B46A06A2A8LL, g->GV(wgHideInterlanguageLinks),
                  wgHideInterlanguageLinks);
      break;
    case 681:
      HASH_RETURN(0x6EB208CBCA7E0AA9LL, g->GV(wgMaxShellTime),
                  wgMaxShellTime);
      break;
    case 683:
      HASH_RETURN(0x3DA59011258F7AABLL, g->GV(wgGroupPermissions),
                  wgGroupPermissions);
      break;
    case 696:
      HASH_RETURN(0x5EA1EE5833E702B8LL, g->GV(wgNoticeTimeout),
                  wgNoticeTimeout);
      break;
    case 697:
      HASH_RETURN(0x61CFEEED54DD0AB9LL, g->GV(matches),
                  matches);
      break;
    case 712:
      HASH_RETURN(0x52B19BC7F3126AC8LL, g->GV(wmgUseDrafts),
                  wmgUseDrafts);
      break;
    case 713:
      HASH_RETURN(0x2FCD4CD058FE6AC9LL, g->GV(wgMaxTocLevel),
                  wgMaxTocLevel);
      break;
    case 714:
      HASH_RETURN(0x45F7742DD17392CALL, g->GV(wgRawHtml),
                  wgRawHtml);
      break;
    case 716:
      HASH_RETURN(0x07A1087699D022CCLL, g->GV(wgScriptExtension),
                  wgScriptExtension);
      break;
    case 717:
      HASH_RETURN(0x7026A29A72AC22CDLL, g->GV(wgVariantArticlePath),
                  wgVariantArticlePath);
      break;
    case 729:
      HASH_RETURN(0x1BB3FBD59E522AD9LL, g->GV(self),
                  self);
      break;
    case 736:
      HASH_RETURN(0x751AD4E978A01AE0LL, g->GV(wgShellLocale),
                  wgShellLocale);
      break;
    case 738:
      HASH_RETURN(0x41998A76A6C112E2LL, g->GV(wgForceUIMsgAsContentMsg),
                  wgForceUIMsgAsContentMsg);
      break;
    case 739:
      HASH_RETURN(0x70C29B6AB429BAE3LL, g->GV(wgProxyPorts),
                  wgProxyPorts);
      break;
    case 749:
      HASH_RETURN(0x4A5D0431E1A3CAEDLL, g->GV(filename),
                  filename);
      break;
    case 755:
      HASH_RETURN(0x69BB1AD4F9569AF3LL, g->GV(wgUseSiteJs),
                  wgUseSiteJs);
      break;
    case 757:
      HASH_RETURN(0x2ED3A839738B52F5LL, g->GV(wgDisableAnonTalk),
                  wgDisableAnonTalk);
      break;
    case 759:
      HASH_RETURN(0x0B6CFE5CA752B2F7LL, g->GV(sep),
                  sep);
      break;
    case 766:
      HASH_RETURN(0x66AD900A2301E2FELL, g->GV(title),
                  title);
      break;
    case 771:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x34FACC6293F20B03LL, g->GV(wgShowExceptionDetails),
                  wgShowExceptionDetails);
      break;
    case 774:
      HASH_RETURN(0x6FF6342F2039DB06LL, g->GV(wgSqlLogFile),
                  wgSqlLogFile);
      break;
    case 776:
      HASH_RETURN(0x7E5ABAA1081BFB08LL, g->GV(wgEmailAuthentication),
                  wgEmailAuthentication);
      break;
    case 778:
      HASH_RETURN(0x362C89A936481B0ALL, g->GV(wgUDPProfilerHost),
                  wgUDPProfilerHost);
      break;
    case 781:
      HASH_RETURN(0x4008C24D7AA4830DLL, g->GV(lang),
                  lang);
      break;
    case 786:
      HASH_RETURN(0x03E350A8A756DB12LL, g->GV(wgNamespaceAliases),
                  wgNamespaceAliases);
      break;
    case 787:
      HASH_RETURN(0x4BAAC6DED702C313LL, g->GV(wgNewUserLog),
                  wgNewUserLog);
      HASH_RETURN(0x2FF9B075F5BFC313LL, g->GV(wgExpensiveParserFunctionLimit),
                  wgExpensiveParserFunctionLimit);
      break;
    case 789:
      HASH_RETURN(0x3AD645EB48693B15LL, g->GV(wgTitle),
                  wgTitle);
      break;
    case 792:
      HASH_RETURN(0x353888D51FF11318LL, g->GV(wgMimeType),
                  wgMimeType);
      break;
    case 795:
      HASH_RETURN(0x68C88FBC2532F31BLL, g->GV(wmgPrivateWikiUploads),
                  wmgPrivateWikiUploads);
      break;
    case 806:
      HASH_RETURN(0x5713CE531BD6F326LL, g->GV(wgNoFollowDomainExceptions),
                  wgNoFollowDomainExceptions);
      HASH_RETURN(0x4B535D6FA1165B26LL, g->GV(wgCommandLineDarkBg),
                  wgCommandLineDarkBg);
      break;
    case 809:
      HASH_RETURN(0x5AFBCF04E31D7329LL, g->GV(wgCentralAuthNew2UDPPrefix),
                  wgCentralAuthNew2UDPPrefix);
      HASH_RETURN(0x2D026B6CDC7E0B29LL, g->GV(wgConfigureHandler),
                  wgConfigureHandler);
      break;
    case 811:
      HASH_RETURN(0x19FD77778A48832BLL, g->GV(wmgUseCentralAuth),
                  wmgUseCentralAuth);
      break;
    case 815:
      HASH_RETURN(0x2CB24E9940FE032FLL, g->GV(wgEnableSidebarCache),
                  wgEnableSidebarCache);
      break;
    case 823:
      HASH_RETURN(0x7FA57490F2479B37LL, g->GV(__Test),
                  __Test);
      break;
    case 826:
      HASH_RETURN(0x062EA4121AF9EB3ALL, g->GV(wmgUseDismissableSiteNotice),
                  wmgUseDismissableSiteNotice);
      break;
    case 827:
      HASH_RETURN(0x7C85270BECBF433BLL, g->GV(wgEnotifUseJobQ),
                  wgEnotifUseJobQ);
      break;
    case 831:
      HASH_RETURN(0x262106CA8F6FC33FLL, g->GV(wgEnotifUserTalk),
                  wgEnotifUserTalk);
      break;
    case 833:
      HASH_RETURN(0x56D735735ADE5341LL, g->GV(wgMetaNamespace),
                  wgMetaNamespace);
      break;
    case 836:
      HASH_RETURN(0x173543E12A6ACB44LL, g->GV(wmgUseDualLicense),
                  wmgUseDualLicense);
      break;
    case 838:
      HASH_RETURN(0x471686E0BE369B46LL, g->GV(wgEnableUserEmail),
                  wgEnableUserEmail);
      HASH_RETURN(0x669A492817E02B46LL, g->GV(wgUpdateRowsPerJob),
                  wgUpdateRowsPerJob);
      break;
    case 842:
      HASH_RETURN(0x78788009C106034ALL, g->GV(wgPagePropLinkInvalidations),
                  wgPagePropLinkInvalidations);
      break;
    case 847:
      HASH_RETURN(0x61B21442EB5B6B4FLL, g->GV(wgDBadminuser),
                  wgDBadminuser);
      break;
    case 848:
      HASH_RETURN(0x7727D0FBE47C9350LL, g->GV(wgProfileLimit),
                  wgProfileLimit);
      HASH_RETURN(0x6F27ACF0EE1E9B50LL, g->GV(wmgUseQuiz),
                  wmgUseQuiz);
      break;
    case 854:
      HASH_RETURN(0x061C264C73C67B56LL, g->GV(wgProxyScriptPath),
                  wgProxyScriptPath);
      break;
    case 855:
      HASH_RETURN(0x11A1AD7A5DBDFB57LL, g->GV(wgDebugProfiling),
                  wgDebugProfiling);
      break;
    case 856:
      HASH_RETURN(0x72050C6E97FB7B58LL, g->GV(wgFileStore),
                  wgFileStore);
      break;
    case 859:
      HASH_RETURN(0x612E37678CE7DB5BLL, g->GV(file),
                  file);
      break;
    case 861:
      HASH_RETURN(0x30315AC4F8CCDB5DLL, g->GV(cacheRecord),
                  cacheRecord);
      break;
    case 878:
      HASH_RETURN(0x71CF7D337911E36ELL, g->GV(wgScriptPath),
                  wgScriptPath);
      break;
    case 879:
      HASH_RETURN(0x5F5BED26C4C4D36FLL, g->GV(wgSquidServers),
                  wgSquidServers);
      HASH_RETURN(0x7691457976BD336FLL, g->GV(wgNamespaceProtection),
                  wgNamespaceProtection);
      break;
    case 883:
      HASH_RETURN(0x46B52D5AB15B2B73LL, g->GV(wgDebugComments),
                  wgDebugComments);
      break;
    case 888:
      HASH_RETURN(0x172E6B599943FB78LL, g->GV(optionsWithArgs),
                  optionsWithArgs);
      break;
    case 896:
      HASH_RETURN(0x39E25484A83BAB80LL, g->GV(wgDebugRawPage),
                  wgDebugRawPage);
      break;
    case 899:
      HASH_RETURN(0x6A1F50A5B28C6B83LL, g->GV(wgRC2UDPInterwikiPrefix),
                  wgRC2UDPInterwikiPrefix);
      break;
    case 913:
      HASH_RETURN(0x1452103FD0885B91LL, g->GV(wgDeferredUpdateList),
                  wgDeferredUpdateList);
      HASH_RETURN(0x16E01CEFBB639B91LL, g->GV(wgAbuseFilterAvailableActions),
                  wgAbuseFilterAvailableActions);
      break;
    case 914:
      HASH_RETURN(0x47CB32AFA64F1392LL, g->GV(wgUseImageMagick),
                  wgUseImageMagick);
      break;
    case 917:
      HASH_RETURN(0x3732DBE98AD9CB95LL, g->GV(wgBreakFrames),
                  wgBreakFrames);
      HASH_RETURN(0x7852680AAA755B95LL, g->GV(wgEnableParserCache),
                  wgEnableParserCache);
      break;
    case 921:
      HASH_RETURN(0x1831362001AD1B99LL, g->GV(wgProfileSampleRate),
                  wgProfileSampleRate);
      break;
    case 925:
      HASH_RETURN(0x7F7826A784C4939DLL, g->GV(wgArticleRobotPolicies),
                  wgArticleRobotPolicies);
      break;
    case 929:
      HASH_RETURN(0x6A4892EEE04F63A1LL, g->GV(wgRedirectScript),
                  wgRedirectScript);
      break;
    case 931:
      HASH_RETURN(0x0B2441BD1EA163A3LL, g->GV(wgConfigureDatabase),
                  wgConfigureDatabase);
      break;
    case 932:
      HASH_RETURN(0x362E2DDD4FD143A4LL, g->GV(wgMaxIfExistCount),
                  wgMaxIfExistCount);
      HASH_RETURN(0x0CF3172C1E07B3A4LL, g->GV(wgDBservers),
                  wgDBservers);
      break;
    case 935:
      HASH_RETURN(0x4029AA34891533A7LL, g->GV(wikiTags),
                  wikiTags);
      break;
    case 937:
      HASH_RETURN(0x337E18C29953D3A9LL, g->GV(wgCopyrightIcon),
                  wgCopyrightIcon);
      break;
    case 940:
      HASH_RETURN(0x72D98AF412127BACLL, g->GV(wgAmericanDates),
                  wgAmericanDates);
      HASH_RETURN(0x5CE004C20B7B83ACLL, g->GV(secure),
                  secure);
      break;
    case 941:
      HASH_RETURN(0x521D9080185BCBADLL, g->GV(wgRightsPage),
                  wgRightsPage);
      break;
    case 942:
      HASH_RETURN(0x57A384A24288B3AELL, g->GV(wgNoticeLocalDirectory),
                  wgNoticeLocalDirectory);
      break;
    case 945:
      HASH_RETURN(0x4695D28F4D09FBB1LL, g->GV(wgCategoryTreeDynamicTag),
                  wgCategoryTreeDynamicTag);
      break;
    case 946:
      HASH_RETURN(0x4945BFF1FB1D63B2LL, g->GV(wgExtraNamespaces),
                  wgExtraNamespaces);
      break;
    case 958:
      HASH_RETURN(0x17005F4F5B1E83BELL, g->GV(wgNamespacesWithSubpages),
                  wgNamespacesWithSubpages);
      HASH_RETURN(0x0010AE53E8B303BELL, g->GV(wgSecretKey),
                  wgSecretKey);
      break;
    case 964:
      HASH_RETURN(0x58268D95D840A3C4LL, g->GV(wgDBpassword),
                  wgDBpassword);
      break;
    case 965:
      HASH_RETURN(0x5C01C46944D773C5LL, g->GV(wgDBtransactions),
                  wgDBtransactions);
      break;
    case 970:
      HASH_RETURN(0x651C0F19009043CALL, g->GV(wgCollectionMWServeURL),
                  wgCollectionMWServeURL);
      break;
    case 976:
      HASH_RETURN(0x4469449C6F2433D0LL, g->GV(wgUseCategoryBrowser),
                  wgUseCategoryBrowser);
      break;
    case 977:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 980:
      HASH_RETURN(0x462D0C9A6A274BD4LL, g->GV(wgNoticeCounterSource),
                  wgNoticeCounterSource);
      break;
    case 985:
      HASH_RETURN(0x33BF6451F7B58BD9LL, g->GV(wgOut),
                  wgOut);
      break;
    case 988:
      HASH_RETURN(0x06A61F679D5453DCLL, g->GV(wgRevisionCacheExpiry),
                  wgRevisionCacheExpiry);
      break;
    case 989:
      HASH_RETURN(0x051BE5EA4CE45BDDLL, g->GV(wgImageMagickConvertCommand),
                  wgImageMagickConvertCommand);
      break;
    case 991:
      HASH_RETURN(0x097D33F7EE7B33DFLL, g->GV(wgCookiePrefix),
                  wgCookiePrefix);
      break;
    case 992:
      HASH_RETURN(0x67E7705CB8B5A3E0LL, g->GV(wgRequestTime),
                  wgRequestTime);
      break;
    case 995:
      HASH_RETURN(0x7FE53B6CF0A1B3E3LL, g->GV(wgUploadPath),
                  wgUploadPath);
      break;
    case 996:
      HASH_RETURN(0x0E4F0A1D74C1FBE4LL, g->GV(wgRestrictDisplayTitle),
                  wgRestrictDisplayTitle);
      break;
    case 1004:
      HASH_RETURN(0x6691B5FAD09ECBECLL, g->GV(wgDisableCounters),
                  wgDisableCounters);
      HASH_RETURN(0x52D0231E81089BECLL, g->GV(wgTranslateNumerals),
                  wgTranslateNumerals);
      break;
    case 1013:
      HASH_RETURN(0x43FBB2C4FE020BF5LL, g->GV(wgNonincludableNamespaces),
                  wgNonincludableNamespaces);
      break;
    case 1018:
      HASH_RETURN(0x0289E71EE62E8BFALL, g->GV(wgLanguageCodeReal),
                  wgLanguageCodeReal);
      break;
    case 1038:
      HASH_RETURN(0x5ACBE7CAD535AC0ELL, g->GV(wmgUseLST),
                  wmgUseLST);
      break;
    case 1040:
      HASH_RETURN(0x1910B041E0EED410LL, g->GV(wgLicenseURL),
                  wgLicenseURL);
      break;
    case 1044:
      HASH_RETURN(0x5EE826941539E414LL, g->GV(wgSiteNotice),
                  wgSiteNotice);
      break;
    case 1046:
      HASH_RETURN(0x04BFC205E59FA416LL, g->GV(x),
                  x);
      break;
    case 1055:
      HASH_RETURN(0x463F1CB15A76041FLL, g->GV(wgHitcounterUpdateFreq),
                  wgHitcounterUpdateFreq);
      break;
    case 1056:
      HASH_RETURN(0x6191D1EF499CCC20LL, g->GV(wgReadOnlyFile),
                  wgReadOnlyFile);
      HASH_RETURN(0x441B3D2180102C20LL, g->GV(wgNoticeProject),
                  wgNoticeProject);
      break;
    case 1061:
      HASH_RETURN(0x46C18C39B09FE425LL, g->GV(wgUseHashTable),
                  wgUseHashTable);
      break;
    case 1066:
      HASH_RETURN(0x538441FB3B0A042ALL, g->GV(wmgUseFlaggedRevs),
                  wmgUseFlaggedRevs);
      break;
    case 1067:
      HASH_RETURN(0x6827C1A2D124EC2BLL, g->GV(wgConfigureEditableSettings),
                  wgConfigureEditableSettings);
      break;
    case 1071:
      HASH_RETURN(0x232B852377A7B42FLL, g->GV(wgAllowUserSkin),
                  wgAllowUserSkin);
      break;
    case 1075:
      HASH_RETURN(0x296D9A8065A85433LL, g->GV(wgUseTeX),
                  wgUseTeX);
      break;
    case 1076:
      HASH_RETURN(0x3B036C7BE9281C34LL, g->GV(wgAutoblockExpiry),
                  wgAutoblockExpiry);
      break;
    case 1077:
      HASH_RETURN(0x66693EBBD88E0435LL, g->GV(attrib),
                  attrib);
      break;
    case 1081:
      HASH_RETURN(0x5F9E59B3AAB7EC39LL, g->GV(wgInputEncoding),
                  wgInputEncoding);
      HASH_RETURN(0x4A9A2B8384DECC39LL, g->GV(wgDebugLogGroups),
                  wgDebugLogGroups);
      break;
    case 1084:
      HASH_RETURN(0x3354B8ACF06BC43CLL, g->GV(wgMaxArticleSize),
                  wgMaxArticleSize);
      break;
    case 1088:
      HASH_RETURN(0x454567E402A60C40LL, g->GV(wgRightsText),
                  wgRightsText);
      break;
    case 1095:
      HASH_RETURN(0x79417070F3B48C47LL, g->GV(wgExtraSubtitle),
                  wgExtraSubtitle);
      break;
    case 1109:
      HASH_RETURN(0x55CF1D77F9276C55LL, g->GV(wmgNewUserSuppressRC),
                  wmgNewUserSuppressRC);
      break;
    case 1110:
      HASH_RETURN(0x1073013255125C56LL, g->GV(wgAuth),
                  wgAuth);
      break;
    case 1111:
      HASH_RETURN(0x018E501422A0B457LL, g->GV(wgLocalInterwiki),
                  wgLocalInterwiki);
      break;
    case 1112:
      HASH_RETURN(0x48E7F8779A11EC58LL, g->GV(wgScript),
                  wgScript);
      HASH_RETURN(0x6EF4B0C760B69458LL, g->GV(wgLivePreview),
                  wgLivePreview);
      break;
    case 1129:
      HASH_RETURN(0x2E259B8159F52469LL, g->GV(wgInterwikiMagic),
                  wgInterwikiMagic);
      HASH_RETURN(0x3574191639261469LL, g->GV(wgProfileToDatabase),
                  wgProfileToDatabase);
      break;
    case 1132:
      HASH_RETURN(0x10B90601AD3E2C6CLL, g->GV(wgLinkHolderBatchSize),
                  wgLinkHolderBatchSize);
      break;
    case 1133:
      HASH_RETURN(0x4450CC847C8B546DLL, g->GV(wmgUseNewUserMessage),
                  wmgUseNewUserMessage);
      break;
    case 1135:
      HASH_RETURN(0x5C4B00752D88AC6FLL, g->GV(wgCentralAuthDryRun),
                  wgCentralAuthDryRun);
      break;
    case 1138:
      HASH_RETURN(0x6E99E49E33B88472LL, g->GV(wgAllowExternalImages),
                  wgAllowExternalImages);
      break;
    case 1148:
      HASH_RETURN(0x6C5558E2BD73EC7CLL, g->GV(wgUrlProtocols),
                  wgUrlProtocols);
      break;
    case 1158:
      HASH_RETURN(0x2AADB7171DADFC86LL, g->GV(wgCommandLineMode),
                  wgCommandLineMode);
      break;
    case 1162:
      HASH_RETURN(0x4B3834E25BB7448ALL, g->GV(wgStyleDirectory),
                  wgStyleDirectory);
      break;
    case 1164:
      HASH_RETURN(0x1936366D2D1FA48CLL, g->GV(wgMaxPPExpandDepth),
                  wgMaxPPExpandDepth);
      break;
    case 1169:
      HASH_RETURN(0x049C095295EA6491LL, g->GV(wgHttpOnlyBlacklist),
                  wgHttpOnlyBlacklist);
      break;
    case 1172:
      HASH_RETURN(0x043364BAC65CAC94LL, g->GV(tmarray),
                  tmarray);
      HASH_RETURN(0x4AB8A71495E5FC94LL, g->GV(wmgUseAbuseFilter),
                  wmgUseAbuseFilter);
      break;
    case 1174:
      HASH_RETURN(0x4A369B78852EDC96LL, g->GV(wgShowUpdatedMarker),
                  wgShowUpdatedMarker);
      break;
    case 1175:
      HASH_RETURN(0x021AD26793AE7C97LL, g->GV(bits),
                  bits);
      break;
    case 1178:
      HASH_RETURN(0x668D947A942A4C9ALL, g->GV(wmgSecondLevelDomain),
                  wmgSecondLevelDomain);
      break;
    case 1180:
      HASH_RETURN(0x5FCB1A0329D55C9CLL, g->GV(utfCanonicalComp),
                  utfCanonicalComp);
      break;
    case 1185:
      HASH_RETURN(0x436837B76D35B4A1LL, g->GV(wgNoticeScroll),
                  wgNoticeScroll);
      break;
    case 1187:
      HASH_RETURN(0x4A33C63C6B8EECA3LL, g->GV(wgUseLuceneSearch),
                  wgUseLuceneSearch);
      HASH_RETURN(0x60F4DCC88CA4F4A3LL, g->GV(adminSettings),
                  adminSettings);
      break;
    case 1188:
      HASH_RETURN(0x1A19A3B64C15FCA4LL, g->GV(wgWhitelistRead),
                  wgWhitelistRead);
      break;
    case 1195:
      HASH_RETURN(0x629910BD11344CABLL, g->GV(wgInternalServer),
                  wgInternalServer);
      break;
    case 1196:
      HASH_RETURN(0x488CBC4D4C2E3CACLL, g->GV(DP),
                  DP);
      break;
    case 1197:
      HASH_RETURN(0x4E21DF371292ECADLL, g->GV(wgPageShowWatchingUsers),
                  wgPageShowWatchingUsers);
      HASH_RETURN(0x731EA9D43F3FECADLL, g->GV(wgCodeReviewENotif),
                  wgCodeReviewENotif);
      break;
    case 1201:
      HASH_RETURN(0x457DA687174D54B1LL, g->GV(wgArticle),
                  wgArticle);
      break;
    case 1218:
      HASH_RETURN(0x1A392434358B24C2LL, g->GV(wgStyleVersion),
                  wgStyleVersion);
      break;
    case 1219:
      HASH_RETURN(0x4836A8B25F907CC3LL, g->GV(wgMajorSiteNoticeID),
                  wgMajorSiteNoticeID);
      break;
    case 1237:
      HASH_RETURN(0x07282D6481A02CD5LL, g->GV(wgDefaultSkin),
                  wgDefaultSkin);
      break;
    case 1238:
      HASH_RETURN(0x4C1AFFA5E04754D6LL, g->GV(option),
                  option);
      break;
    case 1242:
      HASH_RETURN(0x5F2DDAE740A644DALL, g->GV(wgGrammarForms),
                  wgGrammarForms);
      HASH_RETURN(0x163728A5B186DCDALL, g->GV(wgSubversionProxy),
                  wgSubversionProxy);
      break;
    case 1250:
      HASH_RETURN(0x2F5E554E4A9D34E2LL, g->GV(wgCaptchaTriggers),
                  wgCaptchaTriggers);
      break;
    case 1251:
      HASH_RETURN(0x163DDB9EB768E4E3LL, g->GV(wgMaxShellMemory),
                  wgMaxShellMemory);
      break;
    case 1259:
      HASH_RETURN(0x4BA04FC73DA36CEBLL, g->GV(wgCookieDomain),
                  wgCookieDomain);
      break;
    case 1261:
      HASH_RETURN(0x36DE4A3BD2F754EDLL, g->GV(wgEnableAPI),
                  wgEnableAPI);
      break;
    case 1275:
      HASH_RETURN(0x3E078EE8498BCCFBLL, g->GV(wgSharedThumbnailScriptPath),
                  wgSharedThumbnailScriptPath);
      break;
    case 1280:
      HASH_RETURN(0x7A58218A1FE59D00LL, g->GV(wgAppleTouchIcon),
                  wgAppleTouchIcon);
      break;
    case 1283:
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 1284:
      HASH_RETURN(0x5B73E76B1ACDBD04LL, g->GV(wgUseFileCache),
                  wgUseFileCache);
      break;
    case 1285:
      HASH_RETURN(0x157405D52897CD05LL, g->GV(wgTorLoadNodes),
                  wgTorLoadNodes);
      break;
    case 1290:
      HASH_RETURN(0x4DEDD0C4E06BAD0ALL, g->GV(wgTorAutoConfirmAge),
                  wgTorAutoConfirmAge);
      break;
    case 1292:
      HASH_RETURN(0x278B0E130BE1150CLL, g->GV(wgTorIPs),
                  wgTorIPs);
      break;
    case 1295:
      HASH_RETURN(0x6DCC412201E4350FLL, g->GV(wgShowIPinHeader),
                  wgShowIPinHeader);
      break;
    case 1298:
      HASH_RETURN(0x19A9C284C412E512LL, g->GV(wgServerName),
                  wgServerName);
      break;
    case 1299:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 1300:
      HASH_RETURN(0x423AF940A9459514LL, g->GV(wgDBuser),
                  wgDBuser);
      break;
    case 1314:
      HASH_RETURN(0x3C3558C7B940E522LL, g->GV(wgTimelineSettings),
                  wgTimelineSettings);
      break;
    case 1315:
      HASH_RETURN(0x5B9A991AE78D8523LL, g->GV(wgTorTagChanges),
                  wgTorTagChanges);
      break;
    case 1317:
      HASH_RETURN(0x29FD3ACC3187D525LL, g->GV(wgLanguageNames),
                  wgLanguageNames);
      break;
    case 1318:
      HASH_RETURN(0x77F632A4E34F1526LL, g->GV(p),
                  p);
      break;
    case 1342:
      HASH_RETURN(0x05F247364E8A2D3ELL, g->GV(utfCheckNFC),
                  utfCheckNFC);
      break;
    case 1343:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      HASH_RETURN(0x3D50C74AF7EBDD3FLL, g->GV(wgUseDumbLinkUpdate),
                  wgUseDumbLinkUpdate);
      HASH_RETURN(0x42D2AEA0328D9D3FLL, g->GV(cluster),
                  cluster);
      break;
    case 1347:
      HASH_RETURN(0x6888EE8FACEBC543LL, g->GV(wgBrowserBlackList),
                  wgBrowserBlackList);
      break;
    case 1357:
      HASH_RETURN(0x3A5C7A341C25754DLL, g->GV(wgServer),
                  wgServer);
      break;
    case 1360:
      HASH_RETURN(0x30E66D49BBCCAD50LL, g->GV(wgMemc),
                  wgMemc);
      break;
    case 1366:
      HASH_RETURN(0x7A2C52FB86914556LL, g->GV(wgCentralAuthCookieDomain),
                  wgCentralAuthCookieDomain);
      break;
    case 1369:
      HASH_RETURN(0x10F91D80DF2BB559LL, g->GV(wgLocaltimezone),
                  wgLocaltimezone);
      break;
    case 1370:
      HASH_RETURN(0x4E5E50E23D3CA55ALL, g->GV(wgCollectionFormats),
                  wgCollectionFormats);
      break;
    case 1372:
      HASH_RETURN(0x71B9C6B68C4E7D5CLL, g->GV(wgDBserver),
                  wgDBserver);
      break;
    case 1373:
      HASH_RETURN(0x4AD417058F046D5DLL, g->GV(wgEnableEmail),
                  wgEnableEmail);
      HASH_RETURN(0x56CE9E40C1C9A55DLL, g->GV(wgCaptchaDirectory),
                  wgCaptchaDirectory);
      break;
    case 1381:
      HASH_RETURN(0x2589884252E47565LL, g->GV(wgRenderHashAppend),
                  wgRenderHashAppend);
      break;
    case 1387:
      HASH_RETURN(0x2D8567285FADF56BLL, g->GV(wgStyleSheetPath),
                  wgStyleSheetPath);
      HASH_RETURN(0x36E0FE5F9841656BLL, g->GV(wgDBadminpassword),
                  wgDBadminpassword);
      break;
    case 1394:
      HASH_RETURN(0x63A123E046C6ED72LL, g->GV(wgMimeTypeBlacklist),
                  wgMimeTypeBlacklist);
      break;
    case 1397:
      HASH_RETURN(0x16A179F3F7DCFD75LL, g->GV(wgCheckSerialized),
                  wgCheckSerialized);
      break;
    case 1400:
      HASH_RETURN(0x602FD94428C31D78LL, g->GV(section),
                  section);
      break;
    case 1405:
      HASH_RETURN(0x6E95875F5D07E57DLL, g->GV(wgOutputEncoding),
                  wgOutputEncoding);
      break;
    case 1406:
      HASH_RETURN(0x0188BE45E73AFD7ELL, g->GV(globals),
                  globals);
      break;
    case 1414:
      HASH_RETURN(0x29348BAAA90F5586LL, g->GV(wgBlockAllowsUTEdit),
                  wgBlockAllowsUTEdit);
      break;
    case 1415:
      HASH_RETURN(0x612DD31212E90587LL, g->GV(key),
                  key);
      break;
    case 1423:
      HASH_RETURN(0x4EE2267A795CFD8FLL, g->GV(wgUseTwoButtonsSearchForm),
                  wgUseTwoButtonsSearchForm);
      break;
    case 1427:
      HASH_RETURN(0x176FF84F38BCB593LL, g->GV(wgPasswordReminderResendTime),
                  wgPasswordReminderResendTime);
      HASH_RETURN(0x63A063EDF9482593LL, g->GV(wmgUseTitleKey),
                  wmgUseTitleKey);
      break;
    case 1429:
      HASH_RETURN(0x0A84091EA3FEDD95LL, g->GV(wgExtDistBranches),
                  wgExtDistBranches);
      break;
    case 1432:
      HASH_RETURN(0x79CB53EB0854D598LL, g->GV(wgAntiLockFlags),
                  wgAntiLockFlags);
      break;
    case 1436:
      HASH_RETURN(0x6A19B93CFBB8559CLL, g->GV(wgActiveUserEditCount),
                  wgActiveUserEditCount);
      break;
    case 1443:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 1452:
      HASH_RETURN(0x6F9E309EB1E7D5ACLL, g->GV(wgSVGConverters),
                  wgSVGConverters);
      break;
    case 1453:
      HASH_RETURN(0x4299F1F5792875ADLL, g->GV(wgCaptchaClass),
                  wgCaptchaClass);
      break;
    case 1454:
      HASH_RETURN(0x014509CC09376DAELL, g->GV(wgRC2UDPOmitBots),
                  wgRC2UDPOmitBots);
      break;
    case 1456:
      HASH_RETURN(0x2962EA6B30582DB0LL, g->GV(wmgEmergencyCaptcha),
                  wmgEmergencyCaptcha);
      break;
    case 1457:
      HASH_RETURN(0x0150F074F25345B1LL, g->GV(wgCaptchaRegexes),
                  wgCaptchaRegexes);
      break;
    case 1458:
      HASH_RETURN(0x45B9FC509805E5B2LL, g->GV(wgMaxSigChars),
                  wgMaxSigChars);
      break;
    case 1459:
      HASH_RETURN(0x2A71D707D84BDDB3LL, g->GV(wgAlwaysUseTidy),
                  wgAlwaysUseTidy);
      HASH_RETURN(0x05B49F689C5E25B3LL, g->GV(wgEmergencyContact),
                  wgEmergencyContact);
      break;
    case 1463:
      HASH_RETURN(0x1AB7E1CEB32E45B7LL, g->GV(wmgCentralAuthLoginIcon),
                  wmgCentralAuthLoginIcon);
      break;
    case 1469:
      HASH_RETURN(0x0FFD0F06B717ADBDLL, g->GV(docRoot),
                  docRoot);
      break;
    case 1472:
      HASH_RETURN(0x73EC2C4CD71805C0LL, g->GV(wgRateLimitLog),
                  wgRateLimitLog);
      break;
    case 1476:
      HASH_RETURN(0x43E03BE5B85A1DC4LL, g->GV(wgAllowSlowParserFunctions),
                  wgAllowSlowParserFunctions);
      break;
    case 1482:
      HASH_RETURN(0x79F8A66E4D7AADCALL, g->GV(wgFavicon),
                  wgFavicon);
      break;
    case 1483:
      HASH_RETURN(0x1DBDC37E73C135CBLL, g->GV(wgWikiFarm),
                  wgWikiFarm);
      break;
    case 1484:
      HASH_RETURN(0x062657E94E8EADCCLL, g->GV(wmgUseSpecialNuke),
                  wmgUseSpecialNuke);
      break;
    case 1490:
      HASH_RETURN(0x05551C88820BCDD2LL, g->GV(wgShowSQLErrors),
                  wgShowSQLErrors);
      break;
    case 1496:
      HASH_RETURN(0x27440C0251CED5D8LL, g->GV(wgFeedClasses),
                  wgFeedClasses);
      HASH_RETURN(0x00B8EAEC735E0DD8LL, g->GV(wgNoFollowNsExceptions),
                  wgNoFollowNsExceptions);
      HASH_RETURN(0x0AAB197B17BF7DD8LL, g->GV(wgNewUserMessageOnAutoCreate),
                  wgNewUserMessageOnAutoCreate);
      break;
    case 1504:
      HASH_RETURN(0x2F78A533854E5DE0LL, g->GV(wgDebugLogFile),
                  wgDebugLogFile);
      break;
    case 1507:
      HASH_RETURN(0x051DC70D51D4EDE3LL, g->GV(wgCaches),
                  wgCaches);
      break;
    case 1514:
      HASH_RETURN(0x2CB7B15F2858E5EALL, g->GV(wgTexvc),
                  wgTexvc);
      break;
    case 1518:
      HASH_RETURN(0x62AC603470B39DEELL, g->GV(wgRestrictionTypes),
                  wgRestrictionTypes);
      break;
    case 1522:
      HASH_RETURN(0x2954B42DA780CDF2LL, g->GV(wgMemCachedPersistent),
                  wgMemCachedPersistent);
      break;
    case 1523:
      HASH_RETURN(0x0D1B1DDB65783DF3LL, g->GV(wgMediaHandlers),
                  wgMediaHandlers);
      break;
    case 1524:
      HASH_RETURN(0x41AC59070F5C55F4LL, g->GV(groupOverrides2),
                  groupOverrides2);
      break;
    case 1525:
      HASH_RETURN(0x28DCDB01128435F5LL, g->GV(wgDisableSearchUpdate),
                  wgDisableSearchUpdate);
      break;
    case 1526:
      HASH_RETURN(0x299EDB527A96ADF6LL, g->GV(wgStylePath),
                  wgStylePath);
      break;
    case 1527:
      HASH_RETURN(0x65404FCF82D95DF7LL, g->GV(wgCommunityCollectionNamespace),
                  wgCommunityCollectionNamespace);
      break;
    case 1528:
      HASH_RETURN(0x107325AF94A9A5F8LL, g->GV(wgTranscludeCacheExpiry),
                  wgTranscludeCacheExpiry);
      break;
    case 1533:
      HASH_RETURN(0x51810FDD0D1C4DFDLL, g->GV(wgAllowPageInfo),
                  wgAllowPageInfo);
      break;
    case 1534:
      HASH_RETURN(0x6D0545C199FCADFELL, g->GV(wgPasswordSender),
                  wgPasswordSender);
      break;
    case 1542:
      HASH_RETURN(0x5CB58E8A664EFE06LL, g->GV(wgCentralAuthCreateOnView),
                  wgCentralAuthCreateOnView);
      break;
    case 1543:
      HASH_RETURN(0x0ECB3DA2FE35B607LL, g->GV(wgExternalServers),
                  wgExternalServers);
      break;
    case 1554:
      HASH_RETURN(0x5D968409CEE59612LL, g->GV(wmgEnableCaptcha),
                  wmgEnableCaptcha);
      break;
    case 1557:
      HASH_RETURN(0x62E1F4DBA1130E15LL, g->GV(wgSquidMaxage),
                  wgSquidMaxage);
      break;
    case 1558:
      HASH_RETURN(0x7929B0DE86C51616LL, g->GV(wgNamespaceRobotPolicies),
                  wgNamespaceRobotPolicies);
      break;
    case 1561:
      HASH_RETURN(0x660E6C8E995BF619LL, g->GV(wgDisableHardRedirects),
                  wgDisableHardRedirects);
      break;
    case 1567:
      HASH_RETURN(0x1BF02047448F9E1FLL, g->GV(wgAllowUserCss),
                  wgAllowUserCss);
      break;
    case 1568:
      HASH_RETURN(0x061F27F2CDBD1620LL, g->GV(wgEnableSerializedMessages),
                  wgEnableSerializedMessages);
      break;
    case 1576:
      HASH_RETURN(0x0978C2EED25CAE28LL, g->GV(wgShowDebug),
                  wgShowDebug);
      break;
    case 1577:
      HASH_RETURN(0x722F599AEA279E29LL, g->GV(db),
                  db);
      break;
    case 1582:
      HASH_RETURN(0x7FC54E8E5FD1AE2ELL, g->GV(hatesSafari),
                  hatesSafari);
      break;
    case 1590:
      HASH_RETURN(0x0F25E179DFFE0636LL, g->GV(wgUseTidy),
                  wgUseTidy);
      HASH_RETURN(0x371842DCA2467636LL, g->GV(wgExtensionMessagesFiles),
                  wgExtensionMessagesFiles);
      break;
    case 1591:
      HASH_RETURN(0x3B24465279BB7E37LL, g->GV(wgMaxRedirects),
                  wgMaxRedirects);
      break;
    case 1597:
      HASH_RETURN(0x694D0EA05378AE3DLL, g->GV(wgShowCreditsIfMax),
                  wgShowCreditsIfMax);
      break;
    case 1598:
      HASH_RETURN(0x254CACEFA4FA763ELL, g->GV(wgFullyInitialised),
                  wgFullyInitialised);
      break;
    case 1610:
      HASH_RETURN(0x224FF8C4CD58764ALL, g->GV(wmgAutopromoteExtraGroups),
                  wmgAutopromoteExtraGroups);
      break;
    case 1617:
      HASH_RETURN(0x538D9FEB1DE97E51LL, g->GV(wgAccountCreationThrottle),
                  wgAccountCreationThrottle);
      break;
    case 1621:
      HASH_RETURN(0x73BBFC69F0B6AE55LL, g->GV(wgIP),
                  wgIP);
      break;
    case 1623:
      HASH_RETURN(0x3686C76DF5F7CE57LL, g->GV(wgPostCommitUpdateList),
                  wgPostCommitUpdateList);
      HASH_RETURN(0x0FB82D4E4FECA657LL, g->GV(wgNoticeProjects),
                  wgNoticeProjects);
      break;
    case 1631:
      HASH_RETURN(0x06864431CD5F2E5FLL, g->GV(wgUseNPPatrol),
                  wgUseNPPatrol);
      break;
    case 1636:
      HASH_RETURN(0x0099BC4A66CCBE64LL, g->GV(wgLBFactoryConf),
                  wgLBFactoryConf);
      break;
    case 1645:
      HASH_RETURN(0x5550309A82D57E6DLL, g->GV(wgEnotifWatchlist),
                  wgEnotifWatchlist);
      break;
    case 1649:
      HASH_RETURN(0x20973782D3DDA671LL, g->GV(wgDisableTextSearch),
                  wgDisableTextSearch);
      break;
    case 1657:
      HASH_RETURN(0x0638127DA5B4B679LL, g->GV(wgDBprefix),
                  wgDBprefix);
      break;
    case 1660:
      HASH_RETURN(0x4E679853E6353E7CLL, g->GV(wgCentralAuthLoginIcon),
                  wgCentralAuthLoginIcon);
      break;
    case 1661:
      HASH_RETURN(0x4D6EE112A9D2E67DLL, g->GV(bin),
                  bin);
      break;
    case 1672:
      HASH_RETURN(0x7C702CAB981D3E88LL, g->GV(wgSysopRangeBans),
                  wgSysopRangeBans);
      break;
    case 1673:
      HASH_RETURN(0x1250903211069E89LL, g->GV(wgActionPaths),
                  wgActionPaths);
      break;
    case 1674:
      HASH_RETURN(0x16D0A1E7D73BAE8ALL, g->GV(wmgContactPageConf),
                  wmgContactPageConf);
      break;
    case 1678:
      HASH_RETURN(0x4E71ED8D7F33468ELL, g->GV(wgCaptchaStorageClass),
                  wgCaptchaStorageClass);
      break;
    case 1683:
      HASH_RETURN(0x4F8FB6081FBDD693LL, g->GV(wgRateLimitsExcludedIPs),
                  wgRateLimitsExcludedIPs);
      break;
    case 1685:
      HASH_RETURN(0x097A11531AA46695LL, g->GV(wmgUseCodeReview),
                  wmgUseCodeReview);
      break;
    case 1686:
      HASH_RETURN(0x4C40A42FA1A56696LL, g->GV(wgSorbsUrl),
                  wgSorbsUrl);
      HASH_RETURN(0x2032537EAFB84696LL, g->GV(wgNoFollowLinks),
                  wgNoFollowLinks);
      break;
    case 1688:
      HASH_RETURN(0x0EB22EDA95766E98LL, g->GV(i),
                  i);
      break;
    case 1692:
      HASH_RETURN(0x3AD9F28E2BBC1E9CLL, g->GV(wgUser),
                  wgUser);
      break;
    case 1693:
      HASH_RETURN(0x0F8C317744981E9DLL, g->GV(wgMWSuggestTemplate),
                  wgMWSuggestTemplate);
      break;
    case 1695:
      HASH_RETURN(0x34F23C8A11797E9FLL, g->GV(wmgNewUserMessageOnAutoCreate),
                  wmgNewUserMessageOnAutoCreate);
      break;
    case 1698:
      HASH_RETURN(0x2A8A478185B60EA2LL, g->GV(wmgUseGadgets),
                  wmgUseGadgets);
      break;
    case 1704:
      HASH_RETURN(0x2342299E93F216A8LL, g->GV(wgUseSiteCss),
                  wgUseSiteCss);
      break;
    case 1707:
      HASH_RETURN(0x4243E2C0D99B5EABLL, g->GV(wgPreprocessorCacheThreshold),
                  wgPreprocessorCacheThreshold);
      break;
    case 1713:
      HASH_RETURN(0x4C80005A6C3C66B1LL, g->GV(wgNoticeLocalPath),
                  wgNoticeLocalPath);
      break;
    case 1721:
      HASH_RETURN(0x180D1EEFDC087EB9LL, g->GV(wgExtDistWorkingCopy),
                  wgExtDistWorkingCopy);
      break;
    case 1732:
      HASH_RETURN(0x2FE50309F9D3FEC4LL, g->GV(wgEnableUploads),
                  wgEnableUploads);
      break;
    case 1733:
      HASH_RETURN(0x10F3E1A52760B6C5LL, g->GV(hour),
                  hour);
      break;
    case 1734:
      HASH_RETURN(0x2471AFEA96CE16C6LL, g->GV(wgDisabledActions),
                  wgDisabledActions);
      break;
    case 1738:
      HASH_RETURN(0x08625588FAC906CALL, g->GV(wgRedirectSources),
                  wgRedirectSources);
      break;
    case 1740:
      HASH_RETURN(0x7B8EA02672E89ECCLL, g->GV(wgExtensionFunctions),
                  wgExtensionFunctions);
      break;
    case 1744:
      HASH_RETURN(0x26B2DFBF69FAB6D0LL, g->GV(wgUploadSizeWarning),
                  wgUploadSizeWarning);
      break;
    case 1748:
      HASH_RETURN(0x658A7E566743C6D4LL, g->GV(wgActiveUserDays),
                  wgActiveUserDays);
      break;
    case 1749:
      HASH_RETURN(0x20A4AA67AC18FED5LL, g->GV(wgDebugDumpSql),
                  wgDebugDumpSql);
      break;
    case 1752:
      HASH_RETURN(0x72E1EE0D651E56D8LL, g->GV(arg),
                  arg);
      HASH_RETURN(0x3031363A4A2486D8LL, g->GV(wgFFmpegLocation),
                  wgFFmpegLocation);
      break;
    case 1753:
      HASH_RETURN(0x6C40E451833476D9LL, g->GV(wgContactUser),
                  wgContactUser);
      break;
    case 1769:
      HASH_RETURN(0x5A2661DC503CFEE9LL, g->GV(wgUploadNavigationUrl),
                  wgUploadNavigationUrl);
      break;
    case 1771:
      HASH_RETURN(0x14A5EE0C30D676EBLL, g->GV(wgCookieHttpOnly),
                  wgCookieHttpOnly);
      break;
    case 1773:
      HASH_RETURN(0x1338989B9D947EEDLL, g->GV(wmgUseCollection),
                  wmgUseCollection);
      break;
    case 1775:
      HASH_RETURN(0x7CA5B3BC94DF3EEFLL, g->GV(wgNoticeCentralDirectory),
                  wgNoticeCentralDirectory);
      break;
    case 1780:
      HASH_RETURN(0x562A8CEEFF9EEEF4LL, g->GV(wgDefaultUserOptions),
                  wgDefaultUserOptions);
      HASH_RETURN(0x7B82DEB1E0C0AEF4LL, g->GV(wmgExtraImplicitGroups),
                  wmgExtraImplicitGroups);
      break;
    case 1781:
      HASH_RETURN(0x3166C3C2024176F5LL, g->GV(utfCanonicalDecomp),
                  utfCanonicalDecomp);
      break;
    case 1789:
      HASH_RETURN(0x53BE99C04D2686FDLL, g->GV(wmgUseCentralNotice),
                  wmgUseCentralNotice);
      break;
    case 1791:
      HASH_RETURN(0x3C14FC791F083EFFLL, g->GV(wgVersion),
                  wgVersion);
      break;
    case 1793:
      HASH_RETURN(0x617A83EE2D3EEF01LL, g->GV(wmgApplyGlobalBlocks),
                  wmgApplyGlobalBlocks);
      break;
    case 1794:
      HASH_RETURN(0x749F5E65AD3FAF02LL, g->GV(wgProfiler),
                  wgProfiler);
      break;
    case 1807:
      HASH_RETURN(0x77FD9FFDAD75070FLL, g->GV(wgCentralAuthCookiePrefix),
                  wgCentralAuthCookiePrefix);
      break;
    case 1808:
      HASH_RETURN(0x34441773493C6710LL, g->GV(wgEnableImageWhitelist),
                  wgEnableImageWhitelist);
      break;
    case 1815:
      HASH_RETURN(0x79A9441AA56E7F17LL, g->GV(wgMemCachedDebug),
                  wgMemCachedDebug);
      break;
    case 1816:
      HASH_RETURN(0x2EF30F08E88CF718LL, g->GV(wgThumbnailEpoch),
                  wgThumbnailEpoch);
      break;
    case 1819:
      HASH_RETURN(0x3991BC0C3907B71BLL, g->GV(server),
                  server);
      break;
    case 1822:
      HASH_RETURN(0x70FBE2FBA2859F1ELL, g->GV(wgCategoryPrefixedDefaultSortkey),
                  wgCategoryPrefixedDefaultSortkey);
      break;
    case 1828:
      HASH_RETURN(0x10E1A63135E52724LL, g->GV(site),
                  site);
      break;
    case 1831:
      HASH_RETURN(0x5796B4D976439727LL, g->GV(wgEnableWriteAPI),
                  wgEnableWriteAPI);
      break;
    case 1834:
      HASH_RETURN(0x3396F1088B2F1F2ALL, g->GV(wgMaxShellFileSize),
                  wgMaxShellFileSize);
      break;
    case 1842:
      HASH_RETURN(0x7D8C1AC5E54A2F32LL, g->GV(wgSitename),
                  wgSitename);
      break;
    case 1848:
      HASH_RETURN(0x432096BEB2551738LL, g->GV(wgCapitalLinks),
                  wgCapitalLinks);
      HASH_RETURN(0x0E0A4E58AF902738LL, g->GV(oaiAuth),
                  oaiAuth);
      break;
    case 1849:
      HASH_RETURN(0x21FCEAE39D8F1F39LL, g->GV(wgRightsUrl),
                  wgRightsUrl);
      break;
    case 1857:
      HASH_RETURN(0x04B2FC81D80AA741LL, g->GV(param),
                  param);
      break;
    case 1859:
      HASH_RETURN(0x410D7DDB269B0F43LL, g->GV(wgUploadDirectory),
                  wgUploadDirectory);
      break;
    case 1869:
      HASH_RETURN(0x4431BAD847B37F4DLL, g->GV(wgEmailConfirmToEdit),
                  wgEmailConfirmToEdit);
      break;
    case 1880:
      HASH_RETURN(0x4D45F85CE7859F58LL, g->GV(wgLogo),
                  wgLogo);
      break;
    case 1881:
      HASH_RETURN(0x041CD7ABFA3BC759LL, g->GV(permissions),
                  permissions);
      break;
    case 1891:
      HASH_RETURN(0x45798BB44A42AF63LL, g->GV(wgNoticeServerTimeout),
                  wgNoticeServerTimeout);
      break;
    case 1895:
      HASH_RETURN(0x419CFF2A2E5BCF67LL, g->GV(wgLegacySchemaConversion),
                  wgLegacySchemaConversion);
      break;
    case 1899:
      HASH_RETURN(0x5F98F051A5DFAF6BLL, g->GV(wgAutopromote),
                  wgAutopromote);
      break;
    case 1900:
      HASH_RETURN(0x00C6DE6F7DE96F6CLL, g->GV(wgMainCacheType),
                  wgMainCacheType);
      break;
    case 1908:
      HASH_RETURN(0x25F32E03B5218774LL, g->GV(wgSysopUserBans),
                  wgSysopUserBans);
      break;
    case 1910:
      HASH_RETURN(0x29644029C275B776LL, g->GV(wgCheckDBSchema),
                  wgCheckDBSchema);
      break;
    case 1914:
      HASH_RETURN(0x301E67F6386FD77ALL, g->GV(wgSessionsInMemcached),
                  wgSessionsInMemcached);
      break;
    case 1918:
      HASH_RETURN(0x5B38396CF3A99F7ELL, g->GV(wgParser),
                  wgParser);
      break;
    case 1922:
      HASH_RETURN(0x4056C2E766E0B782LL, g->GV(val),
                  val);
      break;
    case 1928:
      HASH_RETURN(0x43CAB6B0484C7788LL, g->GV(wgContLanguageCode),
                  wgContLanguageCode);
      break;
    case 1930:
      HASH_RETURN(0x15335C76F750778ALL, g->GV(wgUseNormalUser),
                  wgUseNormalUser);
      HASH_RETURN(0x4F56B733A4DFC78ALL, g->GV(y),
                  y);
      break;
    case 1935:
      HASH_RETURN(0x52B40E492EB0878FLL, g->GV(wgDebugFunctionEntry),
                  wgDebugFunctionEntry);
      break;
    case 1937:
      HASH_RETURN(0x75BFE08931DDA791LL, g->GV(wgMinimalPasswordLength),
                  wgMinimalPasswordLength);
      break;
    case 1944:
      HASH_RETURN(0x0B580630050B5798LL, g->GV(wgExtDistTarUrl),
                  wgExtDistTarUrl);
      break;
    case 1951:
      HASH_RETURN(0x7197AC66F7344F9FLL, g->GV(wgExternalLinkTarget),
                  wgExternalLinkTarget);
      break;
    case 1955:
      HASH_RETURN(0x2A248A54BB4117A3LL, g->GV(wgCookiePath),
                  wgCookiePath);
      break;
    case 1963:
      HASH_RETURN(0x4F92DFC03802B7ABLL, g->GV(wgDBname),
                  wgDBname);
      break;
    case 1964:
      HASH_RETURN(0x71DAA2A63FF427ACLL, g->GV(wgStockPath),
                  wgStockPath);
      break;
    case 1971:
      HASH_RETURN(0x4F11F806F2DB5FB3LL, g->GV(wgFileBlacklist),
                  wgFileBlacklist);
      break;
    case 1976:
      HASH_RETURN(0x2587A3C7BB424FB8LL, g->GV(wgBlockOpenProxies),
                  wgBlockOpenProxies);
      break;
    case 1983:
      HASH_RETURN(0x6D4CC110B75F37BFLL, g->GV(wgLegacyEncoding),
                  wgLegacyEncoding);
      HASH_RETURN(0x273BD45190637FBFLL, g->GV(wgCaptchaDirectoryLevels),
                  wgCaptchaDirectoryLevels);
      break;
    case 1987:
      HASH_RETURN(0x6DAC6FDB8CC047C3LL, g->GV(wmgCaptchaSecret),
                  wmgCaptchaSecret);
      break;
    case 1992:
      HASH_RETURN(0x107DDA9CBCCA7FC8LL, g->GV(wgAllowUserJs),
                  wgAllowUserJs);
      break;
    case 1998:
      HASH_RETURN(0x4CA1A40E6B6CAFCELL, g->GV(wmgUseProofreadPage),
                  wmgUseProofreadPage);
      break;
    case 2005:
      HASH_RETURN(0x6BFA60F06006B7D5LL, g->GV(wgUseDynamicDates),
                  wgUseDynamicDates);
      break;
    case 2007:
      HASH_RETURN(0x74F3ED447A35AFD7LL, g->GV(wgAvailableRights),
                  wgAvailableRights);
      break;
    case 2010:
      HASH_RETURN(0x47D95AEF51D237DALL, g->GV(wgXhtmlNamespaces),
                  wgXhtmlNamespaces);
      break;
    case 2012:
      HASH_RETURN(0x54A6699BEF6B4FDCLL, g->GV(wgNewUserMinorEdit),
                  wgNewUserMinorEdit);
      break;
    case 2016:
      HASH_RETURN(0x3E7A655727E02FE0LL, g->GV(wgUDPProfilerPort),
                  wgUDPProfilerPort);
      break;
    case 2020:
      HASH_RETURN(0x5C64B65285AEBFE4LL, g->GV(oldUmask),
                  oldUmask);
      break;
    case 2025:
      HASH_RETURN(0x4DAD2268BAA61FE9LL, g->GV(dbSuffix),
                  dbSuffix);
      break;
    case 2028:
      HASH_RETURN(0x4FF90E6A6A0F0FECLL, g->GV(wgSkipSkins),
                  wgSkipSkins);
      break;
    case 2029:
      HASH_RETURN(0x34DEC5871A3E97EDLL, g->GV(wgAllowSpecialInclusion),
                  wgAllowSpecialInclusion);
      HASH_RETURN(0x147FCED3D9C197EDLL, g->GV(wgEditEncoding),
                  wgEditEncoding);
      break;
    case 2031:
      HASH_RETURN(0x61FA7102D48317EFLL, g->GV(wgMaxPPNodeCount),
                  wgMaxPPNodeCount);
      break;
    case 2032:
      HASH_RETURN(0x27DECA6D5839F7F0LL, g->GV(wgCanonicalNamespaceNames),
                  wgCanonicalNamespaceNames);
      break;
    case 2034:
      HASH_RETURN(0x2E884B89708A87F2LL, g->GV(wgHooks),
                  wgHooks);
      break;
    case 2035:
      HASH_RETURN(0x5BE32ACA8C87AFF3LL, g->GV(wgCompressRevisions),
                  wgCompressRevisions);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
