
#ifndef __GENERATED_php_Parser_DiffTest_h__
#define __GENERATED_php_Parser_DiffTest_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/Parser_DiffTest.fw.h>

// Declarations
#include <cls/parser_difftest.h>
#include <php/Parser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$Parser_DiffTest_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_parser_difftest(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_Parser_DiffTest_h__
