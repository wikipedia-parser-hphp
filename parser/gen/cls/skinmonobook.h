
#ifndef __GENERATED_cls_skinmonobook_h__
#define __GENERATED_cls_skinmonobook_h__

#include <cls/skintemplate.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 4327 */
class c_skinmonobook : virtual public c_skintemplate {
  BEGIN_CLASS_MAP(skinmonobook)
    PARENT_CLASS(linker)
    PARENT_CLASS(skin)
    PARENT_CLASS(skintemplate)
  END_CLASS_MAP(skinmonobook)
  DECLARE_CLASS(skinmonobook, SkinMonoBook, skintemplate)
  void init();
  public: void t_initpage(p_outputpage v_out);
  public: void t_setupskinusercss(p_outputpage v_out);
  public: static Variant t_makei18nurl(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makei18nurl("skinmonobook", v_name, v_urlaction); }
  public: static Variant t_getusableskins() { return ti_getusableskins("skinmonobook"); }
  public: static String t_makeglobalvariablesscript(CVarRef v_data) { return ti_makeglobalvariablesscript("skinmonobook", v_data); }
  public: static String t_makevariablesscript(CVarRef v_data) { return ti_makevariablesscript("skinmonobook", v_data); }
  public: static Variant t_makensurl(CStrRef v_name, CVarRef v_urlaction = "", int64 v_namespace = 0LL /* NS_MAIN */) { return ti_makensurl("skinmonobook", v_name, v_urlaction, v_namespace); }
  public: static Variant t_makemainpageurl(CVarRef v_urlaction = "") { return ti_makemainpageurl("skinmonobook", v_urlaction); }
  public: static Variant t_makespecialurlsubpage(CStrRef v_name, CVarRef v_subpage, CVarRef v_urlaction = "") { return ti_makespecialurlsubpage("skinmonobook", v_name, v_subpage, v_urlaction); }
  public: static Variant t_makeinternalorexternalurl(CVarRef v_name) { return ti_makeinternalorexternalurl("skinmonobook", v_name); }
  public: static Variant t_newfromkey(CVarRef v_key) { return ti_newfromkey("skinmonobook", v_key); }
  public: static Variant t_normalizekey(CVarRef v_key) { return ti_normalizekey("skinmonobook", v_key); }
  public: static Variant t_getskinnames() { return ti_getskinnames("skinmonobook"); }
  public: static Variant t_makeurl(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makeurl("skinmonobook", v_name, v_urlaction); }
  public: static Array t_makeknownurldetails(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makeknownurldetails("skinmonobook", v_name, v_urlaction); }
  public: static void t_checktitle(CVarRef v_title, CVarRef v_name) { ti_checktitle("skinmonobook", v_title, v_name); }
  public: static Variant t_makespecialurl(CStrRef v_name, CVarRef v_urlaction = "") { return ti_makespecialurl("skinmonobook", v_name, v_urlaction); }
  public: static Array t_makeurldetails(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makeurldetails("skinmonobook", v_name, v_urlaction); }
  public: static Array t_splittrail(CVarRef v_trail) { return ti_splittrail("skinmonobook", v_trail); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_skinmonobook_h__
