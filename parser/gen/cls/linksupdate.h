
#ifndef __GENERATED_cls_linksupdate_h__
#define __GENERATED_cls_linksupdate_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 20120 */
class c_linksupdate : virtual public ObjectData {
  BEGIN_CLASS_MAP(linksupdate)
  END_CLASS_MAP(linksupdate)
  DECLARE_CLASS(linksupdate, LinksUpdate, ObjectData)
  void init();
  public: Variant m_mId;
  public: Variant m_mTitle;
  public: Variant m_mLinks;
  public: Variant m_mImages;
  public: Variant m_mTemplates;
  public: Variant m_mExternals;
  public: Variant m_mCategories;
  public: Variant m_mInterlangs;
  public: Variant m_mProperties;
  public: Variant m_mDb;
  public: Variant m_mOptions;
  public: Variant m_mRecursive;
  public: void t_linksupdate(Variant v_title, CVarRef v_parserOutput, CVarRef v_recursive = true);
  public: ObjectData *create(Variant v_title, CVarRef v_parserOutput, CVarRef v_recursive = true);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_doupdate();
  public: void t_doincrementalupdate();
  public: void t_dodumbupdate();
  public: void t_queuerecursivejobs();
  public: void t_invalidatepages(int64 v_namespace, Variant v_dbkeys);
  public: void t_invalidatecategories(PlusOperand v_cats);
  public: void t_updatecategorycounts(CVarRef v_added, CVarRef v_deleted);
  public: void t_invalidateimagedescriptions(PlusOperand v_images);
  public: void t_dumbtableupdate(Variant v_table, Variant v_insertions, CStrRef v_fromField);
  public: Variant t_makewherefrom2d(Variant v_arr, CStrRef v_prefix);
  public: void t_incrtableupdate(Variant v_table, CStrRef v_prefix, Variant v_deletions, CArrRef v_insertions);
  public: Array t_getlinkinsertions(CVarRef v_existing = ScalarArrays::sa_[0]);
  public: Array t_gettemplateinsertions(CVarRef v_existing = ScalarArrays::sa_[0]);
  public: Array t_getimageinsertions(CVarRef v_existing = ScalarArrays::sa_[0]);
  public: Array t_getexternalinsertions(CVarRef v_existing = ScalarArrays::sa_[0]);
  public: Array t_getcategoryinsertions(CVarRef v_existing = ScalarArrays::sa_[0]);
  public: Array t_getinterlanginsertions(CVarRef v_existing = ScalarArrays::sa_[0]);
  public: Array t_getpropertyinsertions(CVarRef v_existing = ScalarArrays::sa_[0]);
  public: Variant t_getlinkdeletions(CVarRef v_existing);
  public: Variant t_gettemplatedeletions(CVarRef v_existing);
  public: Variant t_getimagedeletions(CVarRef v_existing);
  public: Variant t_getexternaldeletions(CVarRef v_existing);
  public: Variant t_getcategorydeletions(CVarRef v_existing);
  public: Variant t_getinterlangdeletions(CVarRef v_existing);
  public: Variant t_getpropertydeletions(CVarRef v_existing);
  public: Array t_getexistinglinks();
  public: Variant t_getexistingtemplates();
  public: Array t_getexistingimages();
  public: Array t_getexistingexternals();
  public: Variant t_getexistingcategories();
  public: Variant t_getexistinginterlangs();
  public: Variant t_getexistingproperties();
  public: Variant t_gettitle();
  public: void t_invalidateproperties(CVarRef v_changed);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_linksupdate_h__
