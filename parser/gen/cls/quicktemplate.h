
#ifndef __GENERATED_cls_quicktemplate_h__
#define __GENERATED_cls_quicktemplate_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 4220 */
class c_quicktemplate : virtual public ObjectData {
  BEGIN_CLASS_MAP(quicktemplate)
  END_CLASS_MAP(quicktemplate)
  DECLARE_CLASS(quicktemplate, QuickTemplate, ObjectData)
  void init();
  public: void t_quicktemplate();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_set(CVarRef v_name, CVarRef v_value);
  public: void t_setref(CVarRef v_name, Variant v_value);
  public: void t_settranslator(Variant v_t);
  public: void t_execute();
  public: void t_text(CVarRef v_str);
  public: void t_jstext(CVarRef v_str);
  public: void t_html(CVarRef v_str);
  public: void t_msg(Variant v_str);
  public: void t_msghtml(Variant v_str);
  public: void t_msgwiki(Variant v_str);
  public: bool t_havedata(CVarRef v_str);
  public: bool t_havemsg(Variant v_str);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_quicktemplate_h__
