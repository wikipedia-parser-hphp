<?xml version='1.0'?>
<xsl:stylesheet
  xmlns:AST="http://www.phpcompiler.org/phc-1.1"
  xmlns:past="http://www.parrotcode.org/PAST-0.1"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:HIR="http://www.phpcompiler.org/phc-hir-1.1"
  xmlns:MIR="http://www.phpcompiler.org/phc-mir-1.1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  version="1.0" >

<xsl:output method='xml' indent='yes' />
<xsl:strip-space  elements="*"/>

<xsl:template match="/">
  <root>
  <xsl:apply-templates select="AST:PHP_script" />
</root>
</xsl:template>


<xsl:template  match="AST:Method_invocation">
<xsl:text >

  METHOD NAME:
</xsl:text>

  <AST:Method_invocation>
<past:Op>
  <xsl:attribute name="name" ><xsl:value-of select="AST:METHOD_NAME/value" /></xsl:attribute>
  <xsl:apply-templates mode="Method_invocation" select="AST:Actual_parameter_list" />
</past:Op>


</AST:Method_invocation>


<xsl:text >

  END OF METHOD INVOKE:
</xsl:text>

</xsl:template>


<xsl:template mode="Method_invocation" match="AST:Actual_parameter_list">
  <AST:Actual_parameter_list>
    <xsl:apply-templates mode="Method_invocation" select="AST:Actual_parameter"/>
  </AST:Actual_parameter_list>
</xsl:template>

<xsl:template  mode="Method_invocation" match="AST:Actual_parameter" >
  <AST:Actual_parameter>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Array"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:BOOL"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Bin_op"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Cast"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Constant"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:INT"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Method_invocation"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Post_op"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Pre_op"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:STRING"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Unary_op"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Variable"/>
  </AST:Actual_parameter>
</xsl:template>  

<xsl:template mode="Method_invocation" match="AST:Variable">
  <AST:Variable>
    <xsl:apply-templates mode="Method_invocation" select="AST:Expr_list"/>
    <xsl:apply-templates mode="Method_invocation" select="AST:Reflection"/>
    <xsl:apply-templates mode="Method_invocation" select="AST:Target"/>
    <xsl:apply-templates mode="Method_invocation" select="AST:VARIABLE_NAME"/>    
  </AST:Variable>
</xsl:template>

<xsl:template  mode="Method_invocation" match="AST:Expr_list">
  <AST:Expr_list>
<xsl:apply-templates  mode="Method_invocation" select="AST:Bin_op"/>
<xsl:apply-templates  mode="Method_invocation" select="AST:Expr"/>
<xsl:apply-templates  mode="Method_invocation" select="AST:INT"/>
<xsl:apply-templates  mode="Method_invocation" select="AST:Method_invocation"/>
<xsl:apply-templates  mode="Method_invocation" select="AST:Post_op"/>
<xsl:apply-templates  mode="Method_invocation" select="AST:STRING"/>
<xsl:apply-templates  mode="Method_invocation" select="AST:Variable"/>
</AST:Expr_list>
</xsl:template>


<xsl:template  mode="Method_invocation" match="AST:Array">
  <xsl:apply-templates  mode="Method_invocation" select="AST:Array_elem_list"/>
</xsl:template>

<xsl:template mode="Method_invocation" match="AST:Array_elem_list">
  <xsl:apply-templates  mode="Method_invocation" select="AST:Array_elem"/>
</xsl:template>

<xsl:template mode="Method_invocation" match="AST:Array_elem" >
  <ArrayElem>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Array"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Assignment"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:BOOL"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Bin_op"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:INT"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Method_invocation"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:NIL"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:STRING"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Variable"/>
  </ArrayElem>
</xsl:template>


<xsl:template mode="Method_invocation" match="AST:Assignment" >
      <xsl:apply-templates  mode="Method_invocation" select="AST:Bin_op"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:Array"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:BOOL"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:Cast"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:Constant"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:INT"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:Method_invocation"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:Post_op"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:Pre_op"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:STRING"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:Unary_op"/>
      <xsl:apply-templates  mode="Method_invocation" select="AST:Variable"/>
</xsl:template>

<!--
-->
<xsl:template mode="runGenericsParent" 
match="*">
<xsl:if test="name(..)"><xsl:apply-templates mode="runGenericsParent" 
select=".."/>/</xsl:if><xsl:value-of select="name(.)"/>
</xsl:template>

<xsl:template name="runGenerics">
<xsl:text >
</xsl:text>

  <path> 
  <xsl:apply-templates mode="runGenericsParent" select="."/>
</path> 
  <attributes> 
  <xsl:apply-templates mode="Method_invocation" select="@*"/> </attributes>
  <xsl:apply-templates />
</xsl:template>

<xsl:template mode="Method_invocation" match="node()">
  <xsl:text >
  </xsl:text> 
  <node>
    <name><xsl:value-of select="name(.)"/></name>
    <text><xsl:value-of select="text()"/></text>     
    <child>
      <xsl:call-template name="runGenerics"/>
    </child>   
    <xsl:text >
    </xsl:text>
  </node>
</xsl:template>


<xsl:template mode="Method_invocation" match="AST:STRING" >
  <past:Val returns="PhpString" >
    <xsl:attribute name="encoding" ><xsl:value-of select="value/@encoding" /></xsl:attribute>
    <xsl:attribute name="value" ><xsl:value-of select="value" /></xsl:attribute>
  </past:Val>
</xsl:template>

<xsl:template mode="Method_invocation"  match="AST:INT" >
  <past:Val returns="PhpInteger" >
    <xsl:attribute name="value" ><xsl:value-of select="value" /></xsl:attribute>
  </past:Val>
</xsl:template>

<xsl:template mode="Method_invocation"  match="AST:BOOL" >
  <past:Val returns="PhpBoolean" >
    <xsl:attribute name="value" ><xsl:choose>
      <xsl:when test="value = 'True'"  >1</xsl:when>
      <xsl:when test="value = 'False'" >0</xsl:when>
    </xsl:choose></xsl:attribute>
  </past:Val>
</xsl:template>

<xsl:template mode="Method_invocation"  match="AST:VARIABLE_NAME">
  <VARIABLE_NAME>
    <xsl:attribute name="value"
      ><xsl:value-of select="./value"/></xsl:attribute>
  </VARIABLE_NAME>
</xsl:template>

<xsl:template mode="Method_invocation"  match="AST:Target">
<AST:Target></AST:Target>
  <!--  <AST:Target xsi:nil="true" />-->
</xsl:template>


<xsl:template mode="Method_invocation"  match="AST:Bin_op">
  <AST:Bin_op>
    <xsl:apply-templates mode="Method_invocation"  select="AST:Assignment"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Assignment"/>
    <xsl:apply-templates mode="Method_invocation"  select="AST:BOOL"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:BOOL"/>
    <xsl:apply-templates mode="Method_invocation"  select="AST:Constant"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:INT"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Instanceof"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Method_invocation"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:NIL"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:OP"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:STRING"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Unary_op"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Variable"/>
  </AST:Bin_op>  
</xsl:template>

<xsl:template  mode="Method_invocation" match="AST:Method_invocation">
<xsl:text >

  METHOD NAME:
</xsl:text>

  <AST:Method_invocation>
<past:Op>
  <xsl:attribute name="name" ><xsl:value-of select="AST:METHOD_NAME/value" /></xsl:attribute>
  <xsl:apply-templates mode="Method_invocation" select="AST:Actual_parameter_list" />
</past:Op>
</AST:Method_invocation>
<xsl:text >
  END OF METHOD INVOKE:
</xsl:text>
</xsl:template>

<xsl:template mode="Method_invocation" match="AST:OP">
  <past:Op>
    <xsl:attribute name="pirop" >
      <xsl:choose>
        <xsl:when test="value = '+'" >n_add</xsl:when>
        <xsl:when test="value = '-'" >n_sub</xsl:when>
        <xsl:when test="value = '*'" >n_mul</xsl:when>
        <xsl:when test="value = '/'" >n_div</xsl:when>
        <xsl:when test="value = '%'" >n_mod</xsl:when>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="name" >
      <xsl:choose>
        <xsl:when test="value/@encoding = 'base64'" >
          <xsl:choose>
            <xsl:when test="value = 'PA=='" >infix:&lt;</xsl:when>
            <xsl:when test="value = 'PD0='" >infix:&lt;=</xsl:when>
            <xsl:when test="value = 'Pj0='" >infix:&gt;=</xsl:when>
            <xsl:when test="value = 'Pg=='" >infix:&gt;</xsl:when>
            <xsl:when test="value = 'Jg=='" >infix:&amp;</xsl:when>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="value = '&amp;&amp;'" >infix:AND</xsl:when>
        <xsl:when test="value = '||'" >infix:OR</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat('infix:', AST:OP/value)" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </past:Op>
</xsl:template>

<!--
AST:Cast
  <node><name>AST:Constant</name><text/><child>
  <node><name>AST:Post_op</name><text/><child>
  <node><name>AST:Pre_op</name><text/><child>
  <node><name>AST:Reflection</name><text/><child>
  <node><name>AST:Unary_op</name><text/><child>
-->
<xsl:template  mode="Method_invocation" match="AST:Cast">
  <AST:Cast>
    <type><xsl:value-of  mode="Method_invocation" select="value" /></type>
  </AST:Cast>
</xsl:template>

<xsl:template  mode="Method_invocation" match="AST:Constant">
  <AST:Constant>
    <xsl:apply-templates  mode="Method_invocation" select="AST:CLASS_NAME"/>     
    <xsl:apply-templates  mode="Method_invocation" select="AST:CONSTANT_NAME"/>
  </AST:Constant>  
</xsl:template>

<xsl:template  mode="Method_invocation" match="AST:Post_op" >
  <AST:Post_op>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Target"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Expr_list"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:OP"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Variable"/>
  </AST:Post_op>
</xsl:template>  

<xsl:template  mode="Method_invocation" match="AST:Pre_op">
  <AST:Pre_op>
    <xsl:apply-templates  mode="Method_invocation" select="AST:OP"/>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Variable"/>
  </AST:Pre_op>
</xsl:template>

<xsl:template  mode="Method_invocation" match="AST:Reflection">
  <AST:Reflection>
    <xsl:apply-templates  mode="Method_invocation" select="AST:Variable"/>
  </AST:Reflection>
</xsl:template>

<xsl:template  mode="Method_invocation" match="AST:Unary_op">
  <AST:Unary_op>    
  <xsl:apply-templates  mode="Method_invocation" select="AST:Bin_op"/>
  <xsl:apply-templates  mode="Method_invocation" select="AST:Instanceof"/>
  <xsl:apply-templates  mode="Method_invocation" select="AST:Method_invocation"/>
  <xsl:apply-templates  mode="Method_invocation" select="AST:OP"/>
  <xsl:apply-templates  mode="Method_invocation" select="AST:Variable"/>
</AST:Unary_op>
</xsl:template>

<xsl:template mode="Method_invocation" match="AST:CLASS_NAME">
  <AST:CLASS_NAME>
    <xsl:value-of select="value"/>
  </AST:CLASS_NAME>
</xsl:template>

<xsl:template mode="Method_invocation" match="AST:CONSTANT_NAME">
  <AST:CONSTANT_NAME>
    <xsl:value-of  select="value" />
  </AST:CONSTANT_NAME>  
</xsl:template>

<xsl:template match="attrs/attr[@key='phc.line_number']"></xsl:template>
<xsl:template match="attrs/attr[@key='phc.filename']"></xsl:template>
<xsl:template match="attrs/attr[@key='phc.comments']"></xsl:template>
<xsl:template match="text()"></xsl:template>

</xsl:stylesheet>
