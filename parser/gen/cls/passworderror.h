
#ifndef __GENERATED_cls_passworderror_h__
#define __GENERATED_cls_passworderror_h__

#include <cls/mwexception.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 159 */
class c_passworderror : virtual public c_mwexception {
  BEGIN_CLASS_MAP(passworderror)
    PARENT_CLASS(exception)
    PARENT_CLASS(mwexception)
  END_CLASS_MAP(passworderror)
  DECLARE_CLASS(passworderror, PasswordError, mwexception)
  void init();
  public: static bool t_iscommandline() { return ti_iscommandline("passworderror"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_passworderror_h__
