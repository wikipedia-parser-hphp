
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_NS_USER;

extern const StaticString k_DB_SLAVE;
extern const StaticString k_NS_CATEGORY;

extern const Variant k_SLH_PATTERN;

extern const StaticString k_NS_SPECIAL;
extern const StaticString k_NS_FILE;
extern const StaticString k_NS_MEDIA;

extern const StaticString k_DB_MASTER;
extern const StaticString k_NS_MEDIAWIKI;
extern const StaticString k_NS_TEMPLATE;
extern const StaticString k_TS_MW;
extern const StaticString k_TS_UNIX;
extern const StaticString k_SFH_OBJECT_ARGS;
extern const StaticString k_SFH_NO_HASH;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 31) {
    case 0:
      HASH_RETURN(0x54B3D538F88BF040LL, k_NS_TEMPLATE, NS_TEMPLATE);
      break;
    case 2:
      HASH_RETURN(0x080741BDD303B402LL, k_NS_MEDIA, NS_MEDIA);
      HASH_RETURN(0x51EBFCB49FDB87C2LL, k_DB_MASTER, DB_MASTER);
      break;
    case 3:
      HASH_RETURN(0x39454943C9F77183LL, k_SFH_NO_HASH, SFH_NO_HASH);
      break;
    case 6:
      HASH_RETURN(0x269292323C90CD06LL, k_NS_CATEGORY, NS_CATEGORY);
      break;
    case 7:
      HASH_RETURN(0x1551A4A841687467LL, k_NS_USER, NS_USER);
      HASH_RETURN(0x1A97B0076C879367LL, k_NS_FILE, NS_FILE);
      break;
    case 12:
      HASH_RETURN(0x7C7C96A77F65158CLL, k_SFH_OBJECT_ARGS, SFH_OBJECT_ARGS);
      break;
    case 13:
      HASH_RETURN(0x3597B220C626640DLL, k_TS_MW, TS_MW);
      break;
    case 14:
      HASH_RETURN(0x2D1B05555882C72ELL, k_NS_MEDIAWIKI, NS_MEDIAWIKI);
      break;
    case 15:
      HASH_RETURN(0x6EBDE8C33B7C7BEFLL, k_TS_UNIX, TS_UNIX);
      break;
    case 21:
      HASH_RETURN(0x2E2C33121835E575LL, k_SLH_PATTERN, SLH_PATTERN);
      break;
    case 25:
      HASH_RETURN(0x7A5B9C85D0D14179LL, k_DB_SLAVE, DB_SLAVE);
      break;
    case 26:
      HASH_RETURN(0x5519329301794FBALL, k_NS_SPECIAL, NS_SPECIAL);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
