
#ifndef __GENERATED_cls_parser_difftest_h__
#define __GENERATED_cls_parser_difftest_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser_DiffTest.php line 6 */
class c_parser_difftest : virtual public ObjectData {
  BEGIN_CLASS_MAP(parser_difftest)
  END_CLASS_MAP(parser_difftest)
  DECLARE_CLASS(parser_difftest, Parser_DiffTest, ObjectData)
  void init();
  public: Variant m_parsers;
  public: Variant m_conf;
  public: Variant m_shortOutput;
  public: Variant m_dfUniqPrefix;
  Variant doCall(Variant v_name, Variant v_arguments, bool fatal);
  public: void t___construct(Variant v_conf);
  public: ObjectData *create(Variant v_conf);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_init();
  public: Variant t___call(Variant v_name, Variant v_args);
  public: Variant t_formatarray(Variant v_array);
  public: void t_setfunctionhook(Variant v_id, Variant v_callback, Variant v_flags = 0LL);
  public: bool t_onclearstate(Variant v_parser);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parser_difftest_h__
