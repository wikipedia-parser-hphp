
#ifndef __GENERATED_cls_stubuserlang_h__
#define __GENERATED_cls_stubuserlang_h__

#include <cls/stubobject.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 10976 */
class c_stubuserlang : virtual public c_stubobject {
  BEGIN_CLASS_MAP(stubuserlang)
    PARENT_CLASS(stubobject)
  END_CLASS_MAP(stubuserlang)
  DECLARE_CLASS(stubuserlang, StubUserLang, stubobject)
  void init();
  Variant doCall(Variant v_name, Variant v_arguments, bool fatal);
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t___call(Variant v_name, Variant v_args);
  public: Variant t__newobject();
  public: static bool t_isrealobject(CVarRef v_obj) { return ti_isrealobject("stubuserlang", v_obj); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_stubuserlang_h__
