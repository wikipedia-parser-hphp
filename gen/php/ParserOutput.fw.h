
#ifndef __GENERATED_php_ParserOutput_fw_h__
#define __GENERATED_php_ParserOutput_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_NS_SPECIAL;
extern const StaticString k_NS_FILE;
extern const StaticString k_NS_MEDIA;


// 2. Classes
FORWARD_DECLARE_CLASS(parseroutput)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/Parser.fw.h>

#endif // __GENERATED_php_ParserOutput_fw_h__
