
#ifndef __GENERATED_cls_titlearrayfromresult_h__
#define __GENERATED_cls_titlearrayfromresult_h__

#include <cls/titlearray.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 25609 */
class c_titlearrayfromresult : virtual public c_titlearray {
  BEGIN_CLASS_MAP(titlearrayfromresult)
    PARENT_CLASS(ctraversable)
    PARENT_CLASS(citerator)
    PARENT_CLASS(titlearray)
  END_CLASS_MAP(titlearrayfromresult)
  DECLARE_CLASS(titlearrayfromresult, TitleArrayFromResult, titlearray)
  void init();
  public: Variant m_res;
  public: Variant m_key;
  public: Variant m_current;
  public: void t___construct(Variant v_res);
  public: ObjectData *create(Variant v_res);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_setcurrent(CVarRef v_row);
  public: Variant t_count();
  public: Variant t_current();
  public: Variant t_key();
  public: void t_next();
  public: void t_rewind();
  public: bool t_valid();
  public: static p_titlearrayfromresult t_newfromresult_internal(CVarRef v_res) { return ti_newfromresult_internal("titlearrayfromresult", v_res); }
  public: static Variant t_newfromresult(CVarRef v_res) { return ti_newfromresult("titlearrayfromresult", v_res); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_titlearrayfromresult_h__
