
#ifndef __GENERATED_cls_messagecache_h__
#define __GENERATED_cls_messagecache_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 56 */
class c_messagecache : virtual public ObjectData {
  BEGIN_CLASS_MAP(messagecache)
  END_CLASS_MAP(messagecache)
  DECLARE_CLASS(messagecache, MessageCache, ObjectData)
  void init();
  public: void t_get();
  public: void t_transform();
  public: void t_figuremessage();
  public: void t_loadallmessages();
  public: void t_replace();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_messagecache_h__
