
#ifndef __GENERATED_cls_ppdaccum_hash_h__
#define __GENERATED_cls_ppdaccum_hash_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_Hash.php line 725 */
class c_ppdaccum_hash : virtual public ObjectData {
  BEGIN_CLASS_MAP(ppdaccum_hash)
  END_CLASS_MAP(ppdaccum_hash)
  DECLARE_CLASS(ppdaccum_hash, PPDAccum_Hash, ObjectData)
  void init();
  public: Variant m_firstNode;
  public: Variant m_lastNode;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_addliteral(CVarRef v_s);
  public: void t_addnode(p_ppnode v_node);
  public: void t_addnodewithtext(CVarRef v_name, CVarRef v_value);
  public: void t_addaccum(CVarRef v_accum);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppdaccum_hash_h__
