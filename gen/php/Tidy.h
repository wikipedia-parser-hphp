
#ifndef __GENERATED_php_Tidy_h__
#define __GENERATED_php_Tidy_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/Tidy.fw.h>

// Declarations
#include <cls/mwtidy.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$Tidy_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_mwtidy(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_Tidy_h__
