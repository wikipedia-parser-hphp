
#include <php/Parser.h>
#include <php/ParserOutput.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_NS_SPECIAL = "NS_SPECIAL";
const StaticString k_NS_FILE = "NS_FILE";
const StaticString k_NS_MEDIA = "NS_MEDIA";

/* preface starts */
/* preface finishes */
/* SRC: ParserOutput.php line 6 */
Variant c_parseroutput::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_parseroutput::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_parseroutput::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("mText", m_mText.isReferenced() ? ref(m_mText) : m_mText));
  props.push_back(NEW(ArrayElement)("mLanguageLinks", m_mLanguageLinks.isReferenced() ? ref(m_mLanguageLinks) : m_mLanguageLinks));
  props.push_back(NEW(ArrayElement)("mCategories", m_mCategories.isReferenced() ? ref(m_mCategories) : m_mCategories));
  props.push_back(NEW(ArrayElement)("mContainsOldMagic", m_mContainsOldMagic.isReferenced() ? ref(m_mContainsOldMagic) : m_mContainsOldMagic));
  props.push_back(NEW(ArrayElement)("mTitleText", m_mTitleText.isReferenced() ? ref(m_mTitleText) : m_mTitleText));
  props.push_back(NEW(ArrayElement)("mCacheTime", m_mCacheTime.isReferenced() ? ref(m_mCacheTime) : m_mCacheTime));
  props.push_back(NEW(ArrayElement)("mVersion", m_mVersion.isReferenced() ? ref(m_mVersion) : m_mVersion));
  props.push_back(NEW(ArrayElement)("mLinks", m_mLinks.isReferenced() ? ref(m_mLinks) : m_mLinks));
  props.push_back(NEW(ArrayElement)("mTemplates", m_mTemplates.isReferenced() ? ref(m_mTemplates) : m_mTemplates));
  props.push_back(NEW(ArrayElement)("mTemplateIds", m_mTemplateIds.isReferenced() ? ref(m_mTemplateIds) : m_mTemplateIds));
  props.push_back(NEW(ArrayElement)("mImages", m_mImages.isReferenced() ? ref(m_mImages) : m_mImages));
  props.push_back(NEW(ArrayElement)("mExternalLinks", m_mExternalLinks.isReferenced() ? ref(m_mExternalLinks) : m_mExternalLinks));
  props.push_back(NEW(ArrayElement)("mNewSection", m_mNewSection.isReferenced() ? ref(m_mNewSection) : m_mNewSection));
  props.push_back(NEW(ArrayElement)("mHideNewSection", m_mHideNewSection.isReferenced() ? ref(m_mHideNewSection) : m_mHideNewSection));
  props.push_back(NEW(ArrayElement)("mNoGallery", m_mNoGallery.isReferenced() ? ref(m_mNoGallery) : m_mNoGallery));
  props.push_back(NEW(ArrayElement)("mHeadItems", m_mHeadItems.isReferenced() ? ref(m_mHeadItems) : m_mHeadItems));
  props.push_back(NEW(ArrayElement)("mOutputHooks", m_mOutputHooks.isReferenced() ? ref(m_mOutputHooks) : m_mOutputHooks));
  props.push_back(NEW(ArrayElement)("mWarnings", m_mWarnings.isReferenced() ? ref(m_mWarnings) : m_mWarnings));
  props.push_back(NEW(ArrayElement)("mSections", m_mSections.isReferenced() ? ref(m_mSections) : m_mSections));
  props.push_back(NEW(ArrayElement)("mProperties", m_mProperties.isReferenced() ? ref(m_mProperties) : m_mProperties));
  props.push_back(NEW(ArrayElement)("mTOCHTML", m_mTOCHTML.isReferenced() ? ref(m_mTOCHTML) : m_mTOCHTML));
  props.push_back(NEW(ArrayElement)("mIndexPolicy", m_mIndexPolicy.isReferenced() ? ref(m_mIndexPolicy) : m_mIndexPolicy));
  props.push_back(NEW(ArrayElement)("displayTitle", m_displayTitle.isReferenced() ? ref(m_displayTitle) : m_displayTitle));
  c_ObjectData::o_get(props);
}
bool c_parseroutput::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 1:
      HASH_EXISTS_STRING(0x4D0FABAA7AF12241LL, mNewSection, 11);
      HASH_EXISTS_STRING(0x51991D5F3290D781LL, mProperties, 11);
      break;
    case 6:
      HASH_EXISTS_STRING(0x601BA3BD6D279B46LL, mHeadItems, 10);
      break;
    case 8:
      HASH_EXISTS_STRING(0x0007BEC0539A5248LL, mTOCHTML, 8);
      HASH_EXISTS_STRING(0x5523B0DBE1475D48LL, displayTitle, 12);
      break;
    case 15:
      HASH_EXISTS_STRING(0x46C79A73EB626D0FLL, mImages, 7);
      break;
    case 21:
      HASH_EXISTS_STRING(0x54B0F4A9D39B8015LL, mLinks, 6);
      HASH_EXISTS_STRING(0x7660548AFAAC8FD5LL, mTemplates, 10);
      HASH_EXISTS_STRING(0x306EDD7587944F95LL, mWarnings, 9);
      break;
    case 23:
      HASH_EXISTS_STRING(0x517EDA759E438B97LL, mContainsOldMagic, 17);
      break;
    case 24:
      HASH_EXISTS_STRING(0x6C677FFFF7B1BA18LL, mLanguageLinks, 14);
      break;
    case 25:
      HASH_EXISTS_STRING(0x1F1223E454FD0099LL, mCategories, 11);
      break;
    case 35:
      HASH_EXISTS_STRING(0x69302AA34BFECFE3LL, mText, 5);
      HASH_EXISTS_STRING(0x105ED53F02463263LL, mTemplateIds, 12);
      break;
    case 37:
      HASH_EXISTS_STRING(0x4B82A404AEB169A5LL, mHideNewSection, 15);
      break;
    case 42:
      HASH_EXISTS_STRING(0x2E65BD084E4ED46ALL, mExternalLinks, 14);
      break;
    case 46:
      HASH_EXISTS_STRING(0x4A1938B4088847EELL, mVersion, 8);
      break;
    case 51:
      HASH_EXISTS_STRING(0x3A96E9F9BCC9D533LL, mCacheTime, 10);
      break;
    case 52:
      HASH_EXISTS_STRING(0x6E0F8F67A1BF3D74LL, mIndexPolicy, 12);
      break;
    case 55:
      HASH_EXISTS_STRING(0x1A3DE12EE5312AF7LL, mNoGallery, 10);
      break;
    case 62:
      HASH_EXISTS_STRING(0x69C42A41FA284BFELL, mTitleText, 10);
      HASH_EXISTS_STRING(0x767B3F04BC53C6FELL, mOutputHooks, 12);
      HASH_EXISTS_STRING(0x3557A3D58DFF483ELL, mSections, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_parseroutput::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 1:
      HASH_RETURN_STRING(0x4D0FABAA7AF12241LL, m_mNewSection,
                         mNewSection, 11);
      HASH_RETURN_STRING(0x51991D5F3290D781LL, m_mProperties,
                         mProperties, 11);
      break;
    case 6:
      HASH_RETURN_STRING(0x601BA3BD6D279B46LL, m_mHeadItems,
                         mHeadItems, 10);
      break;
    case 8:
      HASH_RETURN_STRING(0x0007BEC0539A5248LL, m_mTOCHTML,
                         mTOCHTML, 8);
      HASH_RETURN_STRING(0x5523B0DBE1475D48LL, m_displayTitle,
                         displayTitle, 12);
      break;
    case 15:
      HASH_RETURN_STRING(0x46C79A73EB626D0FLL, m_mImages,
                         mImages, 7);
      break;
    case 21:
      HASH_RETURN_STRING(0x54B0F4A9D39B8015LL, m_mLinks,
                         mLinks, 6);
      HASH_RETURN_STRING(0x7660548AFAAC8FD5LL, m_mTemplates,
                         mTemplates, 10);
      HASH_RETURN_STRING(0x306EDD7587944F95LL, m_mWarnings,
                         mWarnings, 9);
      break;
    case 23:
      HASH_RETURN_STRING(0x517EDA759E438B97LL, m_mContainsOldMagic,
                         mContainsOldMagic, 17);
      break;
    case 24:
      HASH_RETURN_STRING(0x6C677FFFF7B1BA18LL, m_mLanguageLinks,
                         mLanguageLinks, 14);
      break;
    case 25:
      HASH_RETURN_STRING(0x1F1223E454FD0099LL, m_mCategories,
                         mCategories, 11);
      break;
    case 35:
      HASH_RETURN_STRING(0x69302AA34BFECFE3LL, m_mText,
                         mText, 5);
      HASH_RETURN_STRING(0x105ED53F02463263LL, m_mTemplateIds,
                         mTemplateIds, 12);
      break;
    case 37:
      HASH_RETURN_STRING(0x4B82A404AEB169A5LL, m_mHideNewSection,
                         mHideNewSection, 15);
      break;
    case 42:
      HASH_RETURN_STRING(0x2E65BD084E4ED46ALL, m_mExternalLinks,
                         mExternalLinks, 14);
      break;
    case 46:
      HASH_RETURN_STRING(0x4A1938B4088847EELL, m_mVersion,
                         mVersion, 8);
      break;
    case 51:
      HASH_RETURN_STRING(0x3A96E9F9BCC9D533LL, m_mCacheTime,
                         mCacheTime, 10);
      break;
    case 52:
      HASH_RETURN_STRING(0x6E0F8F67A1BF3D74LL, m_mIndexPolicy,
                         mIndexPolicy, 12);
      break;
    case 55:
      HASH_RETURN_STRING(0x1A3DE12EE5312AF7LL, m_mNoGallery,
                         mNoGallery, 10);
      break;
    case 62:
      HASH_RETURN_STRING(0x69C42A41FA284BFELL, m_mTitleText,
                         mTitleText, 10);
      HASH_RETURN_STRING(0x767B3F04BC53C6FELL, m_mOutputHooks,
                         mOutputHooks, 12);
      HASH_RETURN_STRING(0x3557A3D58DFF483ELL, m_mSections,
                         mSections, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_parseroutput::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 1:
      HASH_SET_STRING(0x4D0FABAA7AF12241LL, m_mNewSection,
                      mNewSection, 11);
      HASH_SET_STRING(0x51991D5F3290D781LL, m_mProperties,
                      mProperties, 11);
      break;
    case 6:
      HASH_SET_STRING(0x601BA3BD6D279B46LL, m_mHeadItems,
                      mHeadItems, 10);
      break;
    case 8:
      HASH_SET_STRING(0x0007BEC0539A5248LL, m_mTOCHTML,
                      mTOCHTML, 8);
      HASH_SET_STRING(0x5523B0DBE1475D48LL, m_displayTitle,
                      displayTitle, 12);
      break;
    case 15:
      HASH_SET_STRING(0x46C79A73EB626D0FLL, m_mImages,
                      mImages, 7);
      break;
    case 21:
      HASH_SET_STRING(0x54B0F4A9D39B8015LL, m_mLinks,
                      mLinks, 6);
      HASH_SET_STRING(0x7660548AFAAC8FD5LL, m_mTemplates,
                      mTemplates, 10);
      HASH_SET_STRING(0x306EDD7587944F95LL, m_mWarnings,
                      mWarnings, 9);
      break;
    case 23:
      HASH_SET_STRING(0x517EDA759E438B97LL, m_mContainsOldMagic,
                      mContainsOldMagic, 17);
      break;
    case 24:
      HASH_SET_STRING(0x6C677FFFF7B1BA18LL, m_mLanguageLinks,
                      mLanguageLinks, 14);
      break;
    case 25:
      HASH_SET_STRING(0x1F1223E454FD0099LL, m_mCategories,
                      mCategories, 11);
      break;
    case 35:
      HASH_SET_STRING(0x69302AA34BFECFE3LL, m_mText,
                      mText, 5);
      HASH_SET_STRING(0x105ED53F02463263LL, m_mTemplateIds,
                      mTemplateIds, 12);
      break;
    case 37:
      HASH_SET_STRING(0x4B82A404AEB169A5LL, m_mHideNewSection,
                      mHideNewSection, 15);
      break;
    case 42:
      HASH_SET_STRING(0x2E65BD084E4ED46ALL, m_mExternalLinks,
                      mExternalLinks, 14);
      break;
    case 46:
      HASH_SET_STRING(0x4A1938B4088847EELL, m_mVersion,
                      mVersion, 8);
      break;
    case 51:
      HASH_SET_STRING(0x3A96E9F9BCC9D533LL, m_mCacheTime,
                      mCacheTime, 10);
      break;
    case 52:
      HASH_SET_STRING(0x6E0F8F67A1BF3D74LL, m_mIndexPolicy,
                      mIndexPolicy, 12);
      break;
    case 55:
      HASH_SET_STRING(0x1A3DE12EE5312AF7LL, m_mNoGallery,
                      mNoGallery, 10);
      break;
    case 62:
      HASH_SET_STRING(0x69C42A41FA284BFELL, m_mTitleText,
                      mTitleText, 10);
      HASH_SET_STRING(0x767B3F04BC53C6FELL, m_mOutputHooks,
                      mOutputHooks, 12);
      HASH_SET_STRING(0x3557A3D58DFF483ELL, m_mSections,
                      mSections, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_parseroutput::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 63) {
    case 1:
      HASH_RETURN_STRING(0x4D0FABAA7AF12241LL, m_mNewSection,
                         mNewSection, 11);
      HASH_RETURN_STRING(0x51991D5F3290D781LL, m_mProperties,
                         mProperties, 11);
      break;
    case 6:
      HASH_RETURN_STRING(0x601BA3BD6D279B46LL, m_mHeadItems,
                         mHeadItems, 10);
      break;
    case 8:
      HASH_RETURN_STRING(0x0007BEC0539A5248LL, m_mTOCHTML,
                         mTOCHTML, 8);
      HASH_RETURN_STRING(0x5523B0DBE1475D48LL, m_displayTitle,
                         displayTitle, 12);
      break;
    case 15:
      HASH_RETURN_STRING(0x46C79A73EB626D0FLL, m_mImages,
                         mImages, 7);
      break;
    case 21:
      HASH_RETURN_STRING(0x54B0F4A9D39B8015LL, m_mLinks,
                         mLinks, 6);
      HASH_RETURN_STRING(0x7660548AFAAC8FD5LL, m_mTemplates,
                         mTemplates, 10);
      HASH_RETURN_STRING(0x306EDD7587944F95LL, m_mWarnings,
                         mWarnings, 9);
      break;
    case 23:
      HASH_RETURN_STRING(0x517EDA759E438B97LL, m_mContainsOldMagic,
                         mContainsOldMagic, 17);
      break;
    case 24:
      HASH_RETURN_STRING(0x6C677FFFF7B1BA18LL, m_mLanguageLinks,
                         mLanguageLinks, 14);
      break;
    case 25:
      HASH_RETURN_STRING(0x1F1223E454FD0099LL, m_mCategories,
                         mCategories, 11);
      break;
    case 35:
      HASH_RETURN_STRING(0x69302AA34BFECFE3LL, m_mText,
                         mText, 5);
      HASH_RETURN_STRING(0x105ED53F02463263LL, m_mTemplateIds,
                         mTemplateIds, 12);
      break;
    case 37:
      HASH_RETURN_STRING(0x4B82A404AEB169A5LL, m_mHideNewSection,
                         mHideNewSection, 15);
      break;
    case 42:
      HASH_RETURN_STRING(0x2E65BD084E4ED46ALL, m_mExternalLinks,
                         mExternalLinks, 14);
      break;
    case 46:
      HASH_RETURN_STRING(0x4A1938B4088847EELL, m_mVersion,
                         mVersion, 8);
      break;
    case 51:
      HASH_RETURN_STRING(0x3A96E9F9BCC9D533LL, m_mCacheTime,
                         mCacheTime, 10);
      break;
    case 52:
      HASH_RETURN_STRING(0x6E0F8F67A1BF3D74LL, m_mIndexPolicy,
                         mIndexPolicy, 12);
      break;
    case 55:
      HASH_RETURN_STRING(0x1A3DE12EE5312AF7LL, m_mNoGallery,
                         mNoGallery, 10);
      break;
    case 62:
      HASH_RETURN_STRING(0x69C42A41FA284BFELL, m_mTitleText,
                         mTitleText, 10);
      HASH_RETURN_STRING(0x767B3F04BC53C6FELL, m_mOutputHooks,
                         mOutputHooks, 12);
      HASH_RETURN_STRING(0x3557A3D58DFF483ELL, m_mSections,
                         mSections, 9);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_parseroutput::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(parseroutput)
ObjectData *c_parseroutput::create(CStrRef v_text //  = ""
, CArrRef v_languageLinks //  = ScalarArrays::sa_[0]
, CArrRef v_categoryLinks //  = ScalarArrays::sa_[0]
, bool v_containsOldMagic //  = false
, CStrRef v_titletext //  = ""
) {
  init();
  t_parseroutput(v_text, v_languageLinks, v_categoryLinks, v_containsOldMagic, v_titletext);
  return this;
}
ObjectData *c_parseroutput::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    if (count == 1) return (create(params.rvalAt(0)));
    if (count == 2) return (create(params.rvalAt(0), params.rvalAt(1)));
    if (count == 3) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
    if (count == 4) return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
  } else return this;
}
ObjectData *c_parseroutput::cloneImpl() {
  c_parseroutput *obj = NEW(c_parseroutput)();
  cloneSet(obj);
  return obj;
}
void c_parseroutput::cloneSet(c_parseroutput *clone) {
  clone->m_mText = m_mText.isReferenced() ? ref(m_mText) : m_mText;
  clone->m_mLanguageLinks = m_mLanguageLinks.isReferenced() ? ref(m_mLanguageLinks) : m_mLanguageLinks;
  clone->m_mCategories = m_mCategories.isReferenced() ? ref(m_mCategories) : m_mCategories;
  clone->m_mContainsOldMagic = m_mContainsOldMagic.isReferenced() ? ref(m_mContainsOldMagic) : m_mContainsOldMagic;
  clone->m_mTitleText = m_mTitleText.isReferenced() ? ref(m_mTitleText) : m_mTitleText;
  clone->m_mCacheTime = m_mCacheTime.isReferenced() ? ref(m_mCacheTime) : m_mCacheTime;
  clone->m_mVersion = m_mVersion.isReferenced() ? ref(m_mVersion) : m_mVersion;
  clone->m_mLinks = m_mLinks.isReferenced() ? ref(m_mLinks) : m_mLinks;
  clone->m_mTemplates = m_mTemplates.isReferenced() ? ref(m_mTemplates) : m_mTemplates;
  clone->m_mTemplateIds = m_mTemplateIds.isReferenced() ? ref(m_mTemplateIds) : m_mTemplateIds;
  clone->m_mImages = m_mImages.isReferenced() ? ref(m_mImages) : m_mImages;
  clone->m_mExternalLinks = m_mExternalLinks.isReferenced() ? ref(m_mExternalLinks) : m_mExternalLinks;
  clone->m_mNewSection = m_mNewSection.isReferenced() ? ref(m_mNewSection) : m_mNewSection;
  clone->m_mHideNewSection = m_mHideNewSection.isReferenced() ? ref(m_mHideNewSection) : m_mHideNewSection;
  clone->m_mNoGallery = m_mNoGallery.isReferenced() ? ref(m_mNoGallery) : m_mNoGallery;
  clone->m_mHeadItems = m_mHeadItems.isReferenced() ? ref(m_mHeadItems) : m_mHeadItems;
  clone->m_mOutputHooks = m_mOutputHooks.isReferenced() ? ref(m_mOutputHooks) : m_mOutputHooks;
  clone->m_mWarnings = m_mWarnings.isReferenced() ? ref(m_mWarnings) : m_mWarnings;
  clone->m_mSections = m_mSections.isReferenced() ? ref(m_mSections) : m_mSections;
  clone->m_mProperties = m_mProperties.isReferenced() ? ref(m_mProperties) : m_mProperties;
  clone->m_mTOCHTML = m_mTOCHTML.isReferenced() ? ref(m_mTOCHTML) : m_mTOCHTML;
  clone->m_mIndexPolicy = m_mIndexPolicy.isReferenced() ? ref(m_mIndexPolicy) : m_mIndexPolicy;
  clone->m_displayTitle = m_displayTitle.isReferenced() ? ref(m_displayTitle) : m_displayTitle;
  ObjectData::cloneSet(clone);
}
Variant c_parseroutput::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 0:
      HASH_GUARD(0x434CEBEC8FDBB880LL, gettext) {
        return (t_gettext());
      }
      break;
    case 1:
      HASH_GUARD(0x325093066EFE0201LL, setdisplaytitle) {
        return (t_setdisplaytitle(params.rvalAt(0)), null);
      }
      break;
    case 5:
      HASH_GUARD(0x1E1ACEDC035034C5LL, addwarning) {
        return (t_addwarning(params.rvalAt(0)), null);
      }
      break;
    case 19:
      HASH_GUARD(0x71FC9AA7F05A6BD3LL, setproperty) {
        return (t_setproperty(params.rvalAt(0), params.rvalAt(1)), null);
      }
      HASH_GUARD(0x6DBAA9816B039793LL, addcategory) {
        return (t_addcategory(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 21:
      HASH_GUARD(0x2983A8B8675FE955LL, setcachetime) {
        return (t_setcachetime(params.rvalAt(0)));
      }
      break;
    case 25:
      HASH_GUARD(0x1DEB2F58AAA8A419LL, expired) {
        return (t_expired(params.rvalAt(0)));
      }
      break;
    case 27:
      HASH_GUARD(0x78B79F97AFD45D1BLL, setindexpolicy) {
        return (t_setindexpolicy(params.rvalAt(0)));
      }
      break;
    case 28:
      HASH_GUARD(0x3F7198F790CA3D5CLL, hidenewsection) {
        return (t_hidenewsection(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x7207F5C225B9E59CLL, settochtml) {
        return (t_settochtml(params.rvalAt(0)));
      }
      break;
    case 31:
      HASH_GUARD(0x3374D72DB895979FLL, getcategorylinks) {
        return (t_getcategorylinks());
      }
      break;
    case 34:
      HASH_GUARD(0x2295340384610322LL, setcategorylinks) {
        return (t_setcategorylinks(params.rvalAt(0)));
      }
      break;
    case 35:
      HASH_GUARD(0x17EB983A0D55AFA3LL, setflag) {
        return (t_setflag(params.rvalAt(0)), null);
      }
      break;
    case 37:
      HASH_GUARD(0x0B5494D93581FFA5LL, containsoldmagic) {
        return (t_containsoldmagic());
      }
      break;
    case 38:
      HASH_GUARD(0x5C75DB446C2096A6LL, settext) {
        return (t_settext(params.rvalAt(0)));
      }
      HASH_GUARD(0x668299C8203F7926LL, addtemplate) {
        return (t_addtemplate(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    case 39:
      HASH_GUARD(0x1026F251B81BD927LL, addimage) {
        return (t_addimage(params.rvalAt(0)), null);
      }
      break;
    case 40:
      HASH_GUARD(0x5C8BDF93ED656C68LL, getcachetime) {
        return (t_getcachetime());
      }
      HASH_GUARD(0x3FE22FCE3F0B40E8LL, addexternallink) {
        return (t_addexternallink(params.rvalAt(0)), null);
      }
      break;
    case 44:
      HASH_GUARD(0x657AED0066D1BEECLL, addlanguagelink) {
        return (t_addlanguagelink(params.rvalAt(0)), null);
      }
      HASH_GUARD(0x5D10AC6ADCA895ACLL, addlink) {
        int count = params.size();
        if (count <= 1) return (t_addlink(params.rvalAt(0)), null);
        return (t_addlink(params.rvalAt(0), params.rvalAt(1)), null);
      }
      break;
    case 51:
      HASH_GUARD(0x7AAA4B3474D6F233LL, setnewsection) {
        return (t_setnewsection(params.rvalAt(0)), null);
      }
      break;
    case 53:
      HASH_GUARD(0x6A6A6F940F0C36F5LL, setsections) {
        return (t_setsections(params.rvalAt(0)));
      }
      break;
    case 62:
      HASH_GUARD(0x72C61C208667527ELL, getcategories) {
        return ref(t_getcategories());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_parseroutput::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 0:
      HASH_GUARD(0x434CEBEC8FDBB880LL, gettext) {
        return (t_gettext());
      }
      break;
    case 1:
      HASH_GUARD(0x325093066EFE0201LL, setdisplaytitle) {
        return (t_setdisplaytitle(a0), null);
      }
      break;
    case 5:
      HASH_GUARD(0x1E1ACEDC035034C5LL, addwarning) {
        return (t_addwarning(a0), null);
      }
      break;
    case 19:
      HASH_GUARD(0x71FC9AA7F05A6BD3LL, setproperty) {
        return (t_setproperty(a0, a1), null);
      }
      HASH_GUARD(0x6DBAA9816B039793LL, addcategory) {
        return (t_addcategory(a0, a1), null);
      }
      break;
    case 21:
      HASH_GUARD(0x2983A8B8675FE955LL, setcachetime) {
        return (t_setcachetime(a0));
      }
      break;
    case 25:
      HASH_GUARD(0x1DEB2F58AAA8A419LL, expired) {
        return (t_expired(a0));
      }
      break;
    case 27:
      HASH_GUARD(0x78B79F97AFD45D1BLL, setindexpolicy) {
        return (t_setindexpolicy(a0));
      }
      break;
    case 28:
      HASH_GUARD(0x3F7198F790CA3D5CLL, hidenewsection) {
        return (t_hidenewsection(a0), null);
      }
      HASH_GUARD(0x7207F5C225B9E59CLL, settochtml) {
        return (t_settochtml(a0));
      }
      break;
    case 31:
      HASH_GUARD(0x3374D72DB895979FLL, getcategorylinks) {
        return (t_getcategorylinks());
      }
      break;
    case 34:
      HASH_GUARD(0x2295340384610322LL, setcategorylinks) {
        return (t_setcategorylinks(a0));
      }
      break;
    case 35:
      HASH_GUARD(0x17EB983A0D55AFA3LL, setflag) {
        return (t_setflag(a0), null);
      }
      break;
    case 37:
      HASH_GUARD(0x0B5494D93581FFA5LL, containsoldmagic) {
        return (t_containsoldmagic());
      }
      break;
    case 38:
      HASH_GUARD(0x5C75DB446C2096A6LL, settext) {
        return (t_settext(a0));
      }
      HASH_GUARD(0x668299C8203F7926LL, addtemplate) {
        return (t_addtemplate(a0, a1, a2), null);
      }
      break;
    case 39:
      HASH_GUARD(0x1026F251B81BD927LL, addimage) {
        return (t_addimage(a0), null);
      }
      break;
    case 40:
      HASH_GUARD(0x5C8BDF93ED656C68LL, getcachetime) {
        return (t_getcachetime());
      }
      HASH_GUARD(0x3FE22FCE3F0B40E8LL, addexternallink) {
        return (t_addexternallink(a0), null);
      }
      break;
    case 44:
      HASH_GUARD(0x657AED0066D1BEECLL, addlanguagelink) {
        return (t_addlanguagelink(a0), null);
      }
      HASH_GUARD(0x5D10AC6ADCA895ACLL, addlink) {
        if (count <= 1) return (t_addlink(a0), null);
        return (t_addlink(a0, a1), null);
      }
      break;
    case 51:
      HASH_GUARD(0x7AAA4B3474D6F233LL, setnewsection) {
        return (t_setnewsection(a0), null);
      }
      break;
    case 53:
      HASH_GUARD(0x6A6A6F940F0C36F5LL, setsections) {
        return (t_setsections(a0));
      }
      break;
    case 62:
      HASH_GUARD(0x72C61C208667527ELL, getcategories) {
        return ref(t_getcategories());
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parseroutput::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parseroutput$os_get(const char *s) {
  return c_parseroutput::os_get(s, -1);
}
Variant &cw_parseroutput$os_lval(const char *s) {
  return c_parseroutput::os_lval(s, -1);
}
Variant cw_parseroutput$os_constant(const char *s) {
  return c_parseroutput::os_constant(s);
}
Variant cw_parseroutput$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parseroutput::os_invoke(c, s, params, -1, fatal);
}
void c_parseroutput::init() {
  m_mText = null;
  m_mLanguageLinks = null;
  m_mCategories = null;
  m_mContainsOldMagic = null;
  m_mTitleText = null;
  m_mCacheTime = "";
  m_mVersion = "1.6.4" /* parser::VERSION */;
  m_mLinks = ScalarArrays::sa_[0];
  m_mTemplates = ScalarArrays::sa_[0];
  m_mTemplateIds = ScalarArrays::sa_[0];
  m_mImages = ScalarArrays::sa_[0];
  m_mExternalLinks = ScalarArrays::sa_[0];
  m_mNewSection = false;
  m_mHideNewSection = false;
  m_mNoGallery = false;
  m_mHeadItems = ScalarArrays::sa_[0];
  m_mOutputHooks = ScalarArrays::sa_[0];
  m_mWarnings = ScalarArrays::sa_[0];
  m_mSections = ScalarArrays::sa_[0];
  m_mProperties = ScalarArrays::sa_[0];
  m_mTOCHTML = "";
  m_mIndexPolicy = "";
  m_displayTitle = false;
}
/* SRC: ParserOutput.php line 36 */
void c_parseroutput::t_parseroutput(CStrRef v_text //  = ""
, CArrRef v_languageLinks //  = ScalarArrays::sa_[0]
, CArrRef v_categoryLinks //  = ScalarArrays::sa_[0]
, bool v_containsOldMagic //  = false
, CStrRef v_titletext //  = ""
) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::ParserOutput);
  bool oldInCtor = gasInCtor(true);
  (m_mText = v_text);
  (m_mLanguageLinks = v_languageLinks);
  (m_mCategories = v_categoryLinks);
  (m_mContainsOldMagic = v_containsOldMagic);
  (m_mTitleText = v_titletext);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: ParserOutput.php line 46 */
Variant c_parseroutput::t_gettext() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getText);
  return m_mText;
} /* function */
/* SRC: ParserOutput.php line 47 */
Variant c_parseroutput::t_getlanguagelinks() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getLanguageLinks);
  return ref(lval(m_mLanguageLinks));
} /* function */
/* SRC: ParserOutput.php line 48 */
Variant c_parseroutput::t_getcategorylinks() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getCategoryLinks);
  return LINE(48,x_array_keys(m_mCategories));
} /* function */
/* SRC: ParserOutput.php line 49 */
Variant c_parseroutput::t_getcategories() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getCategories);
  return ref(lval(m_mCategories));
} /* function */
/* SRC: ParserOutput.php line 50 */
Variant c_parseroutput::t_getcachetime() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getCacheTime);
  return m_mCacheTime;
} /* function */
/* SRC: ParserOutput.php line 51 */
Variant c_parseroutput::t_gettitletext() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getTitleText);
  return m_mTitleText;
} /* function */
/* SRC: ParserOutput.php line 52 */
Variant c_parseroutput::t_getsections() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getSections);
  return m_mSections;
} /* function */
/* SRC: ParserOutput.php line 53 */
Variant c_parseroutput::t_getlinks() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getLinks);
  return ref(lval(m_mLinks));
} /* function */
/* SRC: ParserOutput.php line 54 */
Variant c_parseroutput::t_gettemplates() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getTemplates);
  return ref(lval(m_mTemplates));
} /* function */
/* SRC: ParserOutput.php line 55 */
Variant c_parseroutput::t_getimages() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getImages);
  return ref(lval(m_mImages));
} /* function */
/* SRC: ParserOutput.php line 56 */
Variant c_parseroutput::t_getexternallinks() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getExternalLinks);
  return ref(lval(m_mExternalLinks));
} /* function */
/* SRC: ParserOutput.php line 57 */
Variant c_parseroutput::t_getnogallery() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getNoGallery);
  return m_mNoGallery;
} /* function */
/* SRC: ParserOutput.php line 58 */
Variant c_parseroutput::t_getsubtitle() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getSubtitle);
  return o_get("mSubtitle", 0x643E52ADEE1E7DA4LL);
} /* function */
/* SRC: ParserOutput.php line 59 */
Array c_parseroutput::t_getoutputhooks() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getOutputHooks);
  return toArray(m_mOutputHooks);
} /* function */
/* SRC: ParserOutput.php line 60 */
Variant c_parseroutput::t_getwarnings() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getWarnings);
  return LINE(60,x_array_keys(m_mWarnings));
} /* function */
/* SRC: ParserOutput.php line 61 */
Variant c_parseroutput::t_getindexpolicy() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getIndexPolicy);
  return m_mIndexPolicy;
} /* function */
/* SRC: ParserOutput.php line 62 */
Variant c_parseroutput::t_gettochtml() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getTOCHTML);
  return m_mTOCHTML;
} /* function */
/* SRC: ParserOutput.php line 64 */
Variant c_parseroutput::t_containsoldmagic() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::containsOldMagic);
  return m_mContainsOldMagic;
} /* function */
/* SRC: ParserOutput.php line 65 */
Variant c_parseroutput::t_settext(Variant v_text) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setText);
  return LINE(65,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mText)).set(1, ref(v_text)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOutput.php line 66 */
Variant c_parseroutput::t_setlanguagelinks(Variant v_ll) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setLanguageLinks);
  return LINE(66,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mLanguageLinks)).set(1, ref(v_ll)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOutput.php line 67 */
Variant c_parseroutput::t_setcategorylinks(Variant v_cl) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setCategoryLinks);
  return LINE(67,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mCategories)).set(1, ref(v_cl)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOutput.php line 68 */
Variant c_parseroutput::t_setcontainsoldmagic(Variant v_com) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setContainsOldMagic);
  return LINE(68,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mContainsOldMagic)).set(1, ref(v_com)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOutput.php line 69 */
Variant c_parseroutput::t_setcachetime(Variant v_t) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setCacheTime);
  return LINE(69,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mCacheTime)).set(1, ref(v_t)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOutput.php line 70 */
Variant c_parseroutput::t_settitletext(Variant v_t) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setTitleText);
  return LINE(70,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mTitleText)).set(1, ref(v_t)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOutput.php line 71 */
Variant c_parseroutput::t_setsections(Variant v_toc) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setSections);
  return LINE(71,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mSections)).set(1, ref(v_toc)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOutput.php line 72 */
Variant c_parseroutput::t_setindexpolicy(Variant v_policy) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setIndexPolicy);
  return LINE(72,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mIndexPolicy)).set(1, ref(v_policy)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOutput.php line 73 */
Variant c_parseroutput::t_settochtml(Variant v_tochtml) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setTOCHTML);
  return LINE(73,invoke_failed("wfsetvar", Array(ArrayInit(2).set(0, ref(m_mTOCHTML)).set(1, ref(v_tochtml)).create()), 0x000000009E927E0BLL));
} /* function */
/* SRC: ParserOutput.php line 75 */
void c_parseroutput::t_addcategory(CVarRef v_c, CVarRef v_sort) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::addCategory);
  m_mCategories.set(v_c, (v_sort));
} /* function */
/* SRC: ParserOutput.php line 76 */
void c_parseroutput::t_addlanguagelink(CVarRef v_t) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::addLanguageLink);
  m_mLanguageLinks.append((v_t));
} /* function */
/* SRC: ParserOutput.php line 77 */
void c_parseroutput::t_addwarning(CVarRef v_s) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::addWarning);
  m_mWarnings.set(v_s, (1LL));
} /* function */
/* SRC: ParserOutput.php line 79 */
void c_parseroutput::t_addoutputhook(CVarRef v_hook, bool v_data //  = false
) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::addOutputHook);
  m_mOutputHooks.append((Array(ArrayInit(2).set(0, v_hook).set(1, v_data).create())));
} /* function */
/* SRC: ParserOutput.php line 83 */
void c_parseroutput::t_setnewsection(CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setNewSection);
  (m_mNewSection = toBoolean(v_value));
} /* function */
/* SRC: ParserOutput.php line 86 */
void c_parseroutput::t_hidenewsection(CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::hideNewSection);
  (m_mHideNewSection = toBoolean(v_value));
} /* function */
/* SRC: ParserOutput.php line 89 */
bool c_parseroutput::t_gethidenewsection() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getHideNewSection);
  return toBoolean(m_mHideNewSection);
} /* function */
/* SRC: ParserOutput.php line 92 */
bool c_parseroutput::t_getnewsection() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getNewSection);
  return toBoolean(m_mNewSection);
} /* function */
/* SRC: ParserOutput.php line 96 */
void c_parseroutput::t_addexternallink(CVarRef v_url) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::addExternalLink);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgServer __attribute__((__unused__)) = g->GV(wgServer);
  Variant &gv_wgRegisterInternalExternals __attribute__((__unused__)) = g->GV(wgRegisterInternalExternals);
  {
  }
  if ((toBoolean(gv_wgRegisterInternalExternals)) || (!same(LINE(99,x_stripos(toString(v_url), concat(toString(gv_wgServer), "/"))), 0LL))) m_mExternalLinks.set(v_url, (1LL));
} /* function */
/* SRC: ParserOutput.php line 103 */
void c_parseroutput::t_addlink(CVarRef v_title, Variant v_id //  = null
) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::addLink);
  Variant v_ns;
  Variant v_dbk;

  if (toBoolean(LINE(104,toObject(v_title)->o_invoke_few_args("isExternal", 0x16F7C0178CF6E138LL, 0)))) {
    return;
  }
  (v_ns = LINE(108,toObject(v_title)->o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)));
  (v_dbk = LINE(109,toObject(v_title)->o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)));
  if (equal(v_ns, k_NS_MEDIA)) {
    (v_ns = k_NS_FILE);
  }
  else if (equal(v_ns, k_NS_SPECIAL)) {
    return;
  }
  else if (same(v_dbk, "")) {
    return;
  }
  if (!(isset(m_mLinks, v_ns))) {
    m_mLinks.set(v_ns, (ScalarArrays::sa_[0]));
  }
  if (LINE(124,x_is_null(v_id))) {
    (v_id = LINE(125,toObject(v_title)->o_invoke_few_args("getArticleID", 0x094F8EE5A02782D6LL, 0)));
  }
  lval(m_mLinks.lvalAt(v_ns)).set(v_dbk, (v_id));
} /* function */
/* SRC: ParserOutput.php line 130 */
void c_parseroutput::t_addimage(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::addImage);
  m_mImages.set(v_name, (1LL));
} /* function */
/* SRC: ParserOutput.php line 134 */
void c_parseroutput::t_addtemplate(CVarRef v_title, CVarRef v_page_id, CVarRef v_rev_id) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::addTemplate);
  Variant v_ns;
  Variant v_dbk;

  (v_ns = LINE(135,toObject(v_title)->o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)));
  (v_dbk = LINE(136,toObject(v_title)->o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)));
  if (!(isset(m_mTemplates, v_ns))) {
    m_mTemplates.set(v_ns, (ScalarArrays::sa_[0]));
  }
  lval(m_mTemplates.lvalAt(v_ns)).set(v_dbk, (v_page_id));
  if (!(isset(m_mTemplateIds, v_ns))) {
    m_mTemplateIds.set(v_ns, (ScalarArrays::sa_[0]));
  }
  lval(m_mTemplateIds.lvalAt(v_ns)).set(v_dbk, (v_rev_id));
} /* function */
/* SRC: ParserOutput.php line 156 */
bool c_parseroutput::t_expired(CVarRef v_touched) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::expired);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgCacheEpoch __attribute__((__unused__)) = g->GV(wgCacheEpoch);
  return equal(LINE(158,t_getcachetime()), -1LL) || less(LINE(159,t_getcachetime()), v_touched) || not_more(LINE(160,t_getcachetime()), gv_wgCacheEpoch) || !(t___isset("mVersion")) || toBoolean(LINE(162,x_version_compare(toString(m_mVersion), "1.6.4" /* parser::VERSION */, "lt")));
} /* function */
/* SRC: ParserOutput.php line 170 */
void c_parseroutput::t_addheaditem(CVarRef v_section, bool v_tag //  = false
) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::addHeadItem);
  if (!same(v_tag, false)) {
    m_mHeadItems.set(v_tag, (v_section));
  }
  else {
    m_mHeadItems.append((v_section));
  }
} /* function */
/* SRC: ParserOutput.php line 185 */
void c_parseroutput::t_setdisplaytitle(CVarRef v_text) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setDisplayTitle);
  (m_displayTitle = v_text);
} /* function */
/* SRC: ParserOutput.php line 194 */
Variant c_parseroutput::t_getdisplaytitle() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getDisplayTitle);
  return m_displayTitle;
} /* function */
/* SRC: ParserOutput.php line 201 */
void c_parseroutput::t_setflag(CVarRef v_flag) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setFlag);
  lval(o_lval("mFlags", 0x3A295C456F72E3D0LL)).set(v_flag, (true));
} /* function */
/* SRC: ParserOutput.php line 205 */
bool c_parseroutput::t_getflag(CVarRef v_flag) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getFlag);
  return isset(o_get("mFlags", 0x3A295C456F72E3D0LL), v_flag);
} /* function */
/* SRC: ParserOutput.php line 212 */
void c_parseroutput::t_setproperty(CVarRef v_name, CVarRef v_value) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::setProperty);
  m_mProperties.set(v_name, (v_value));
} /* function */
/* SRC: ParserOutput.php line 216 */
Variant c_parseroutput::t_getproperty(CVarRef v_name) {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getProperty);
  return isset(m_mProperties, v_name) ? ((Variant)(m_mProperties.rvalAt(v_name))) : ((Variant)(false));
} /* function */
/* SRC: ParserOutput.php line 220 */
Variant c_parseroutput::t_getproperties() {
  INSTANCE_METHOD_INJECTION(ParserOutput, ParserOutput::getProperties);
  if (!(t___isset("mProperties"))) {
    (m_mProperties = ScalarArrays::sa_[0]);
  }
  return m_mProperties;
} /* function */
Object co_parseroutput(CArrRef params, bool init /* = true */) {
  return Object(p_parseroutput(NEW(c_parseroutput)())->dynCreate(params, init));
}
Variant pm_php$ParserOutput_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::ParserOutput.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$ParserOutput_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
