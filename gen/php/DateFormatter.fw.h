
#ifndef __GENERATED_php_DateFormatter_fw_h__
#define __GENERATED_php_DateFormatter_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants

// 2. Classes
FORWARD_DECLARE_CLASS(dateformatter)
extern const int64 q_dateformatter_ALL;
extern const int64 q_dateformatter_NONE;
extern const int64 q_dateformatter_MDY;
extern const int64 q_dateformatter_DMY;
extern const int64 q_dateformatter_YMD;
extern const int64 q_dateformatter_ISO1;
extern const int64 q_dateformatter_LASTPREF;
extern const int64 q_dateformatter_ISO2;
extern const int64 q_dateformatter_YDM;
extern const int64 q_dateformatter_DM;
extern const int64 q_dateformatter_MD;
extern const int64 q_dateformatter_LAST;

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_DateFormatter_fw_h__
