
#include <php/CoreParserFunctions.h>
#include <php/DateFormatter.h>
#include <php/Parser.h>
#include <php/Preprocessor.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_NS_USER = "NS_USER";

/* preface starts */
/* preface finishes */
/* SRC: CoreParserFunctions.php line 7 */
Variant c_coreparserfunctions::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_coreparserfunctions::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_coreparserfunctions::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_coreparserfunctions::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_coreparserfunctions::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_coreparserfunctions::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_coreparserfunctions::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_coreparserfunctions::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(coreparserfunctions)
ObjectData *c_coreparserfunctions::cloneImpl() {
  c_coreparserfunctions *obj = NEW(c_coreparserfunctions)();
  cloneSet(obj);
  return obj;
}
void c_coreparserfunctions::cloneSet(c_coreparserfunctions *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_coreparserfunctions::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 5:
      HASH_GUARD(0x35B7424DC6DDC785LL, subpagename) {
        int count = params.size();
        if (count <= 1) return (ti_subpagename(o_getClassName(), params.rvalAt(0)));
        return (ti_subpagename(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 9:
      HASH_GUARD(0x3551C2202E31F909LL, special) {
        return (ti_special(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x359F7DDBB439CC09LL, filepath) {
        int count = params.size();
        if (count <= 1) return (ti_filepath(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_filepath(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_filepath(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 10:
      HASH_GUARD(0x08D08963095FA98ALL, localurl) {
        int count = params.size();
        if (count <= 1) return (ti_localurl(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_localurl(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_localurl(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 16:
      HASH_GUARD(0x1CB8D83F9130E490LL, formatdate) {
        int count = params.size();
        if (count <= 2) return (ti_formatdate(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_formatdate(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 18:
      HASH_GUARD(0x05FAA2085D94FE12LL, urlencode) {
        int count = params.size();
        if (count <= 1) return (ti_urlencode(o_getClassName(), params.rvalAt(0)));
        return (ti_urlencode(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 20:
      HASH_GUARD(0x051F57783523D714LL, pagesinnamespace) {
        int count = params.size();
        if (count <= 1) return (ti_pagesinnamespace(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_pagesinnamespace(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_pagesinnamespace(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 25:
      HASH_GUARD(0x6EB6D615714DF099LL, anchorencode) {
        return (ti_anchorencode(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 28:
      HASH_GUARD(0x0191205BE4FF8F9CLL, lc) {
        int count = params.size();
        if (count <= 1) return (ti_lc(o_getClassName(), params.rvalAt(0)));
        return (ti_lc(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 32:
      HASH_GUARD(0x34DEB5D98BDFF6A0LL, fullurl) {
        int count = params.size();
        if (count <= 1) return (ti_fullurl(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_fullurl(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_fullurl(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      HASH_GUARD(0x09928523585E54A0LL, displaytitle) {
        int count = params.size();
        if (count <= 1) return (ti_displaytitle(o_getClassName(), params.rvalAt(0)));
        return (ti_displaytitle(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 33:
      HASH_GUARD(0x011E1E4D7CFABEA1LL, basepagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_basepagenamee(o_getClassName(), params.rvalAt(0)));
        return (ti_basepagenamee(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 34:
      HASH_GUARD(0x074544A52D6A5422LL, talkpagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_talkpagenamee(o_getClassName(), params.rvalAt(0)));
        return (ti_talkpagenamee(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 36:
      HASH_GUARD(0x51260D879D3BB124LL, talkspacee) {
        int count = params.size();
        if (count <= 1) return (ti_talkspacee(o_getClassName(), params.rvalAt(0)));
        return (ti_talkspacee(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 37:
      HASH_GUARD(0x582523D03167F625LL, basepagename) {
        int count = params.size();
        if (count <= 1) return (ti_basepagename(o_getClassName(), params.rvalAt(0)));
        return (ti_basepagename(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 38:
      HASH_GUARD(0x503A779628C29926LL, intfunction) {
        int count = params.size();
        if (count <= 1) return (ti_intfunction(o_getClassName(),count, params.rvalAt(0)));
        if (count == 2) return (ti_intfunction(o_getClassName(),count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_intfunction(o_getClassName(),count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 39:
      HASH_GUARD(0x6200C73E146615A7LL, subjectspacee) {
        int count = params.size();
        if (count <= 1) return (ti_subjectspacee(o_getClassName(), params.rvalAt(0)));
        return (ti_subjectspacee(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 40:
      HASH_GUARD(0x1486123D24E4C8A8LL, localurle) {
        int count = params.size();
        if (count <= 1) return (ti_localurle(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_localurle(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_localurle(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 45:
      HASH_GUARD(0x4638E1CDC3D8982DLL, language) {
        int count = params.size();
        if (count <= 1) return (ti_language(o_getClassName(), params.rvalAt(0)));
        return (ti_language(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 47:
      HASH_GUARD(0x6ABED8C07DA91FAFLL, talkspace) {
        int count = params.size();
        if (count <= 1) return (ti_talkspace(o_getClassName(), params.rvalAt(0)));
        return (ti_talkspace(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 49:
      HASH_GUARD(0x364B31DA720A6131LL, grammar) {
        int count = params.size();
        if (count <= 1) return (ti_grammar(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_grammar(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_grammar(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 51:
      HASH_GUARD(0x7448B5BAF0B032B3LL, lcfirst) {
        int count = params.size();
        if (count <= 1) return (ti_lcfirst(o_getClassName(), params.rvalAt(0)));
        return (ti_lcfirst(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 53:
      HASH_GUARD(0x2D44AE5D8AEB6E35LL, subjectspace) {
        int count = params.size();
        if (count <= 1) return (ti_subjectspace(o_getClassName(), params.rvalAt(0)));
        return (ti_subjectspace(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x6FEE7241B788FA35LL, numberofusers) {
        int count = params.size();
        if (count <= 1) return (ti_numberofusers(o_getClassName(), params.rvalAt(0)));
        return (ti_numberofusers(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 63:
      HASH_GUARD(0x24206A195B9C203FLL, ucfirst) {
        int count = params.size();
        if (count <= 1) return (ti_ucfirst(o_getClassName(), params.rvalAt(0)));
        return (ti_ucfirst(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 68:
      HASH_GUARD(0x086A22F539BFE744LL, talkpagename) {
        int count = params.size();
        if (count <= 1) return (ti_talkpagename(o_getClassName(), params.rvalAt(0)));
        return (ti_talkpagename(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 70:
      HASH_GUARD(0x2FD3C556DBEC8046LL, subjectpagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_subjectpagenamee(o_getClassName(), params.rvalAt(0)));
        return (ti_subjectpagenamee(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 72:
      HASH_GUARD(0x244F137FE9602148LL, protectionlevel) {
        int count = params.size();
        if (count <= 1) return (ti_protectionlevel(o_getClassName(), params.rvalAt(0)));
        return (ti_protectionlevel(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 75:
      HASH_GUARD(0x638DFF1F9ED1A7CBLL, mwnamespace) {
        int count = params.size();
        if (count <= 1) return (ti_mwnamespace(o_getClassName(), params.rvalAt(0)));
        return (ti_mwnamespace(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 77:
      HASH_GUARD(0x09958033D977C0CDLL, plural) {
        int count = params.size();
        if (count <= 1) return (ti_plural(o_getClassName(),count, params.rvalAt(0)));
        if (count == 2) return (ti_plural(o_getClassName(),count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_plural(o_getClassName(),count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 82:
      HASH_GUARD(0x18956BD975EB0AD2LL, fullpagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_fullpagenamee(o_getClassName(), params.rvalAt(0)));
        return (ti_fullpagenamee(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x6389384D327B8DD2LL, subpagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_subpagenamee(o_getClassName(), params.rvalAt(0)));
        return (ti_subpagenamee(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x1ED58DF7346FAFD2LL, padright) {
        int count = params.size();
        if (count <= 1) return (ti_padright(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_padright(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        if (count == 3) return (ti_padright(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        return (ti_padright(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
      }
      break;
    case 83:
      HASH_GUARD(0x7D776D0AEA9F38D3LL, nse) {
        int count = params.size();
        if (count <= 1) return (ti_nse(o_getClassName(), params.rvalAt(0)));
        return (ti_nse(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 85:
      HASH_GUARD(0x6CEC0D016D9655D5LL, ns) {
        int count = params.size();
        if (count <= 1) return (ti_ns(o_getClassName(), params.rvalAt(0)));
        return (ti_ns(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 90:
      HASH_GUARD(0x0FEB8F5DDB8EBB5ALL, formatnum) {
        int count = params.size();
        if (count <= 1) return (ti_formatnum(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_formatnum(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_formatnum(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 95:
      HASH_GUARD(0x5681CDFC9A4BD65FLL, padleft) {
        int count = params.size();
        if (count <= 1) return (ti_padleft(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_padleft(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        if (count == 3) return (ti_padleft(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        return (ti_padleft(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
      }
      break;
    case 96:
      HASH_GUARD(0x6403181CDD90B160LL, numberofarticles) {
        int count = params.size();
        if (count <= 1) return (ti_numberofarticles(o_getClassName(), params.rvalAt(0)));
        return (ti_numberofarticles(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 99:
      HASH_GUARD(0x48B122ACB9475BE3LL, numberofadmins) {
        int count = params.size();
        if (count <= 1) return (ti_numberofadmins(o_getClassName(), params.rvalAt(0)));
        return (ti_numberofadmins(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 102:
      HASH_GUARD(0x4A887CFE1BC101E6LL, defaultsort) {
        return (ti_defaultsort(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x5DB1F157D242C7E6LL, tagobj) {
        return (ti_tagobj(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 104:
      HASH_GUARD(0x498C20093EED53E8LL, fullpagename) {
        int count = params.size();
        if (count <= 1) return (ti_fullpagename(o_getClassName(), params.rvalAt(0)));
        return (ti_fullpagename(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 109:
      HASH_GUARD(0x4207BDDD2C59BA6DLL, gender) {
        int count = params.size();
        if (count <= 2) return (ti_gender(o_getClassName(),count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_gender(o_getClassName(),count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      HASH_GUARD(0x57C3D544AB79B6EDLL, pagename) {
        int count = params.size();
        if (count <= 1) return (ti_pagename(o_getClassName(), params.rvalAt(0)));
        return (ti_pagename(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 113:
      HASH_GUARD(0x25197E8E5501F271LL, uc) {
        int count = params.size();
        if (count <= 1) return (ti_uc(o_getClassName(), params.rvalAt(0)));
        return (ti_uc(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 114:
      HASH_GUARD(0x08935F4176449D72LL, numberofactiveusers) {
        int count = params.size();
        if (count <= 1) return (ti_numberofactiveusers(o_getClassName(), params.rvalAt(0)));
        return (ti_numberofactiveusers(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 116:
      HASH_GUARD(0x47C3ED014D9D0774LL, numberofpages) {
        int count = params.size();
        if (count <= 1) return (ti_numberofpages(o_getClassName(), params.rvalAt(0)));
        return (ti_numberofpages(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x46BFC9BC72D7EC74LL, pagesize) {
        int count = params.size();
        if (count <= 1) return (ti_pagesize(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_pagesize(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_pagesize(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 118:
      HASH_GUARD(0x04CE8705E23B4576LL, numberofedits) {
        int count = params.size();
        if (count <= 1) return (ti_numberofedits(o_getClassName(), params.rvalAt(0)));
        return (ti_numberofedits(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 119:
      HASH_GUARD(0x60214AC5E7E359F7LL, namespacee) {
        int count = params.size();
        if (count <= 1) return (ti_namespacee(o_getClassName(), params.rvalAt(0)));
        return (ti_namespacee(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x6BCAE6ECCB5DEEF7LL, numberoffiles) {
        int count = params.size();
        if (count <= 1) return (ti_numberoffiles(o_getClassName(), params.rvalAt(0)));
        return (ti_numberoffiles(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 120:
      HASH_GUARD(0x243DB740F9A237F8LL, fullurle) {
        int count = params.size();
        if (count <= 1) return (ti_fullurle(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_fullurle(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_fullurle(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 121:
      HASH_GUARD(0x1E98450B38F85F79LL, numberofviews) {
        int count = params.size();
        if (count <= 1) return (ti_numberofviews(o_getClassName(), params.rvalAt(0)));
        return (ti_numberofviews(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 122:
      HASH_GUARD(0x16A117F2904BE67ALL, pagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_pagenamee(o_getClassName(), params.rvalAt(0)));
        return (ti_pagenamee(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 124:
      HASH_GUARD(0x56A7922F2498CFFCLL, numberingroup) {
        int count = params.size();
        if (count <= 1) return (ti_numberingroup(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_numberingroup(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_numberingroup(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 125:
      HASH_GUARD(0x577562411EAFEDFDLL, subjectpagename) {
        int count = params.size();
        if (count <= 1) return (ti_subjectpagename(o_getClassName(), params.rvalAt(0)));
        return (ti_subjectpagename(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 127:
      HASH_GUARD(0x560F8E26E840C57FLL, pagesincategory) {
        int count = params.size();
        if (count <= 1) return (ti_pagesincategory(o_getClassName(), params.rvalAt(0)));
        if (count == 2) return (ti_pagesincategory(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
        return (ti_pagesincategory(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_coreparserfunctions::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 5:
      HASH_GUARD(0x35B7424DC6DDC785LL, subpagename) {
        if (count <= 1) return (ti_subpagename(o_getClassName(), a0));
        return (ti_subpagename(o_getClassName(), a0, a1));
      }
      break;
    case 9:
      HASH_GUARD(0x3551C2202E31F909LL, special) {
        return (ti_special(o_getClassName(), a0, a1));
      }
      HASH_GUARD(0x359F7DDBB439CC09LL, filepath) {
        if (count <= 1) return (ti_filepath(o_getClassName(), a0));
        if (count == 2) return (ti_filepath(o_getClassName(), a0, a1));
        return (ti_filepath(o_getClassName(), a0, a1, a2));
      }
      break;
    case 10:
      HASH_GUARD(0x08D08963095FA98ALL, localurl) {
        if (count <= 1) return (ti_localurl(o_getClassName(), a0));
        if (count == 2) return (ti_localurl(o_getClassName(), a0, a1));
        return (ti_localurl(o_getClassName(), a0, a1, a2));
      }
      break;
    case 16:
      HASH_GUARD(0x1CB8D83F9130E490LL, formatdate) {
        if (count <= 2) return (ti_formatdate(o_getClassName(), a0, a1));
        return (ti_formatdate(o_getClassName(), a0, a1, a2));
      }
      break;
    case 18:
      HASH_GUARD(0x05FAA2085D94FE12LL, urlencode) {
        if (count <= 1) return (ti_urlencode(o_getClassName(), a0));
        return (ti_urlencode(o_getClassName(), a0, a1));
      }
      break;
    case 20:
      HASH_GUARD(0x051F57783523D714LL, pagesinnamespace) {
        if (count <= 1) return (ti_pagesinnamespace(o_getClassName(), a0));
        if (count == 2) return (ti_pagesinnamespace(o_getClassName(), a0, a1));
        return (ti_pagesinnamespace(o_getClassName(), a0, a1, a2));
      }
      break;
    case 25:
      HASH_GUARD(0x6EB6D615714DF099LL, anchorencode) {
        return (ti_anchorencode(o_getClassName(), a0, a1));
      }
      break;
    case 28:
      HASH_GUARD(0x0191205BE4FF8F9CLL, lc) {
        if (count <= 1) return (ti_lc(o_getClassName(), a0));
        return (ti_lc(o_getClassName(), a0, a1));
      }
      break;
    case 32:
      HASH_GUARD(0x34DEB5D98BDFF6A0LL, fullurl) {
        if (count <= 1) return (ti_fullurl(o_getClassName(), a0));
        if (count == 2) return (ti_fullurl(o_getClassName(), a0, a1));
        return (ti_fullurl(o_getClassName(), a0, a1, a2));
      }
      HASH_GUARD(0x09928523585E54A0LL, displaytitle) {
        if (count <= 1) return (ti_displaytitle(o_getClassName(), a0));
        return (ti_displaytitle(o_getClassName(), a0, a1));
      }
      break;
    case 33:
      HASH_GUARD(0x011E1E4D7CFABEA1LL, basepagenamee) {
        if (count <= 1) return (ti_basepagenamee(o_getClassName(), a0));
        return (ti_basepagenamee(o_getClassName(), a0, a1));
      }
      break;
    case 34:
      HASH_GUARD(0x074544A52D6A5422LL, talkpagenamee) {
        if (count <= 1) return (ti_talkpagenamee(o_getClassName(), a0));
        return (ti_talkpagenamee(o_getClassName(), a0, a1));
      }
      break;
    case 36:
      HASH_GUARD(0x51260D879D3BB124LL, talkspacee) {
        if (count <= 1) return (ti_talkspacee(o_getClassName(), a0));
        return (ti_talkspacee(o_getClassName(), a0, a1));
      }
      break;
    case 37:
      HASH_GUARD(0x582523D03167F625LL, basepagename) {
        if (count <= 1) return (ti_basepagename(o_getClassName(), a0));
        return (ti_basepagename(o_getClassName(), a0, a1));
      }
      break;
    case 38:
      HASH_GUARD(0x503A779628C29926LL, intfunction) {
        if (count <= 1) return (ti_intfunction(o_getClassName(),count, a0));
        if (count == 2) return (ti_intfunction(o_getClassName(),count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_intfunction(o_getClassName(),count,a0, a1, params));
      }
      break;
    case 39:
      HASH_GUARD(0x6200C73E146615A7LL, subjectspacee) {
        if (count <= 1) return (ti_subjectspacee(o_getClassName(), a0));
        return (ti_subjectspacee(o_getClassName(), a0, a1));
      }
      break;
    case 40:
      HASH_GUARD(0x1486123D24E4C8A8LL, localurle) {
        if (count <= 1) return (ti_localurle(o_getClassName(), a0));
        if (count == 2) return (ti_localurle(o_getClassName(), a0, a1));
        return (ti_localurle(o_getClassName(), a0, a1, a2));
      }
      break;
    case 45:
      HASH_GUARD(0x4638E1CDC3D8982DLL, language) {
        if (count <= 1) return (ti_language(o_getClassName(), a0));
        return (ti_language(o_getClassName(), a0, a1));
      }
      break;
    case 47:
      HASH_GUARD(0x6ABED8C07DA91FAFLL, talkspace) {
        if (count <= 1) return (ti_talkspace(o_getClassName(), a0));
        return (ti_talkspace(o_getClassName(), a0, a1));
      }
      break;
    case 49:
      HASH_GUARD(0x364B31DA720A6131LL, grammar) {
        if (count <= 1) return (ti_grammar(o_getClassName(), a0));
        if (count == 2) return (ti_grammar(o_getClassName(), a0, a1));
        return (ti_grammar(o_getClassName(), a0, a1, a2));
      }
      break;
    case 51:
      HASH_GUARD(0x7448B5BAF0B032B3LL, lcfirst) {
        if (count <= 1) return (ti_lcfirst(o_getClassName(), a0));
        return (ti_lcfirst(o_getClassName(), a0, a1));
      }
      break;
    case 53:
      HASH_GUARD(0x2D44AE5D8AEB6E35LL, subjectspace) {
        if (count <= 1) return (ti_subjectspace(o_getClassName(), a0));
        return (ti_subjectspace(o_getClassName(), a0, a1));
      }
      HASH_GUARD(0x6FEE7241B788FA35LL, numberofusers) {
        if (count <= 1) return (ti_numberofusers(o_getClassName(), a0));
        return (ti_numberofusers(o_getClassName(), a0, a1));
      }
      break;
    case 63:
      HASH_GUARD(0x24206A195B9C203FLL, ucfirst) {
        if (count <= 1) return (ti_ucfirst(o_getClassName(), a0));
        return (ti_ucfirst(o_getClassName(), a0, a1));
      }
      break;
    case 68:
      HASH_GUARD(0x086A22F539BFE744LL, talkpagename) {
        if (count <= 1) return (ti_talkpagename(o_getClassName(), a0));
        return (ti_talkpagename(o_getClassName(), a0, a1));
      }
      break;
    case 70:
      HASH_GUARD(0x2FD3C556DBEC8046LL, subjectpagenamee) {
        if (count <= 1) return (ti_subjectpagenamee(o_getClassName(), a0));
        return (ti_subjectpagenamee(o_getClassName(), a0, a1));
      }
      break;
    case 72:
      HASH_GUARD(0x244F137FE9602148LL, protectionlevel) {
        if (count <= 1) return (ti_protectionlevel(o_getClassName(), a0));
        return (ti_protectionlevel(o_getClassName(), a0, a1));
      }
      break;
    case 75:
      HASH_GUARD(0x638DFF1F9ED1A7CBLL, mwnamespace) {
        if (count <= 1) return (ti_mwnamespace(o_getClassName(), a0));
        return (ti_mwnamespace(o_getClassName(), a0, a1));
      }
      break;
    case 77:
      HASH_GUARD(0x09958033D977C0CDLL, plural) {
        if (count <= 1) return (ti_plural(o_getClassName(),count, a0));
        if (count == 2) return (ti_plural(o_getClassName(),count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_plural(o_getClassName(),count,a0, a1, params));
      }
      break;
    case 82:
      HASH_GUARD(0x18956BD975EB0AD2LL, fullpagenamee) {
        if (count <= 1) return (ti_fullpagenamee(o_getClassName(), a0));
        return (ti_fullpagenamee(o_getClassName(), a0, a1));
      }
      HASH_GUARD(0x6389384D327B8DD2LL, subpagenamee) {
        if (count <= 1) return (ti_subpagenamee(o_getClassName(), a0));
        return (ti_subpagenamee(o_getClassName(), a0, a1));
      }
      HASH_GUARD(0x1ED58DF7346FAFD2LL, padright) {
        if (count <= 1) return (ti_padright(o_getClassName(), a0));
        if (count == 2) return (ti_padright(o_getClassName(), a0, a1));
        if (count == 3) return (ti_padright(o_getClassName(), a0, a1, a2));
        return (ti_padright(o_getClassName(), a0, a1, a2, a3));
      }
      break;
    case 83:
      HASH_GUARD(0x7D776D0AEA9F38D3LL, nse) {
        if (count <= 1) return (ti_nse(o_getClassName(), a0));
        return (ti_nse(o_getClassName(), a0, a1));
      }
      break;
    case 85:
      HASH_GUARD(0x6CEC0D016D9655D5LL, ns) {
        if (count <= 1) return (ti_ns(o_getClassName(), a0));
        return (ti_ns(o_getClassName(), a0, a1));
      }
      break;
    case 90:
      HASH_GUARD(0x0FEB8F5DDB8EBB5ALL, formatnum) {
        if (count <= 1) return (ti_formatnum(o_getClassName(), a0));
        if (count == 2) return (ti_formatnum(o_getClassName(), a0, a1));
        return (ti_formatnum(o_getClassName(), a0, a1, a2));
      }
      break;
    case 95:
      HASH_GUARD(0x5681CDFC9A4BD65FLL, padleft) {
        if (count <= 1) return (ti_padleft(o_getClassName(), a0));
        if (count == 2) return (ti_padleft(o_getClassName(), a0, a1));
        if (count == 3) return (ti_padleft(o_getClassName(), a0, a1, a2));
        return (ti_padleft(o_getClassName(), a0, a1, a2, a3));
      }
      break;
    case 96:
      HASH_GUARD(0x6403181CDD90B160LL, numberofarticles) {
        if (count <= 1) return (ti_numberofarticles(o_getClassName(), a0));
        return (ti_numberofarticles(o_getClassName(), a0, a1));
      }
      break;
    case 99:
      HASH_GUARD(0x48B122ACB9475BE3LL, numberofadmins) {
        if (count <= 1) return (ti_numberofadmins(o_getClassName(), a0));
        return (ti_numberofadmins(o_getClassName(), a0, a1));
      }
      break;
    case 102:
      HASH_GUARD(0x4A887CFE1BC101E6LL, defaultsort) {
        return (ti_defaultsort(o_getClassName(), a0, a1));
      }
      HASH_GUARD(0x5DB1F157D242C7E6LL, tagobj) {
        return (ti_tagobj(o_getClassName(), a0, a1, a2));
      }
      break;
    case 104:
      HASH_GUARD(0x498C20093EED53E8LL, fullpagename) {
        if (count <= 1) return (ti_fullpagename(o_getClassName(), a0));
        return (ti_fullpagename(o_getClassName(), a0, a1));
      }
      break;
    case 109:
      HASH_GUARD(0x4207BDDD2C59BA6DLL, gender) {
        if (count <= 2) return (ti_gender(o_getClassName(),count, a0, a1));
        Array params;
        if (count >= 3) params.append(a2);
        if (count >= 4) params.append(a3);
        if (count >= 5) params.append(a4);
        if (count >= 6) params.append(a5);
        return (ti_gender(o_getClassName(),count,a0, a1, params));
      }
      HASH_GUARD(0x57C3D544AB79B6EDLL, pagename) {
        if (count <= 1) return (ti_pagename(o_getClassName(), a0));
        return (ti_pagename(o_getClassName(), a0, a1));
      }
      break;
    case 113:
      HASH_GUARD(0x25197E8E5501F271LL, uc) {
        if (count <= 1) return (ti_uc(o_getClassName(), a0));
        return (ti_uc(o_getClassName(), a0, a1));
      }
      break;
    case 114:
      HASH_GUARD(0x08935F4176449D72LL, numberofactiveusers) {
        if (count <= 1) return (ti_numberofactiveusers(o_getClassName(), a0));
        return (ti_numberofactiveusers(o_getClassName(), a0, a1));
      }
      break;
    case 116:
      HASH_GUARD(0x47C3ED014D9D0774LL, numberofpages) {
        if (count <= 1) return (ti_numberofpages(o_getClassName(), a0));
        return (ti_numberofpages(o_getClassName(), a0, a1));
      }
      HASH_GUARD(0x46BFC9BC72D7EC74LL, pagesize) {
        if (count <= 1) return (ti_pagesize(o_getClassName(), a0));
        if (count == 2) return (ti_pagesize(o_getClassName(), a0, a1));
        return (ti_pagesize(o_getClassName(), a0, a1, a2));
      }
      break;
    case 118:
      HASH_GUARD(0x04CE8705E23B4576LL, numberofedits) {
        if (count <= 1) return (ti_numberofedits(o_getClassName(), a0));
        return (ti_numberofedits(o_getClassName(), a0, a1));
      }
      break;
    case 119:
      HASH_GUARD(0x60214AC5E7E359F7LL, namespacee) {
        if (count <= 1) return (ti_namespacee(o_getClassName(), a0));
        return (ti_namespacee(o_getClassName(), a0, a1));
      }
      HASH_GUARD(0x6BCAE6ECCB5DEEF7LL, numberoffiles) {
        if (count <= 1) return (ti_numberoffiles(o_getClassName(), a0));
        return (ti_numberoffiles(o_getClassName(), a0, a1));
      }
      break;
    case 120:
      HASH_GUARD(0x243DB740F9A237F8LL, fullurle) {
        if (count <= 1) return (ti_fullurle(o_getClassName(), a0));
        if (count == 2) return (ti_fullurle(o_getClassName(), a0, a1));
        return (ti_fullurle(o_getClassName(), a0, a1, a2));
      }
      break;
    case 121:
      HASH_GUARD(0x1E98450B38F85F79LL, numberofviews) {
        if (count <= 1) return (ti_numberofviews(o_getClassName(), a0));
        return (ti_numberofviews(o_getClassName(), a0, a1));
      }
      break;
    case 122:
      HASH_GUARD(0x16A117F2904BE67ALL, pagenamee) {
        if (count <= 1) return (ti_pagenamee(o_getClassName(), a0));
        return (ti_pagenamee(o_getClassName(), a0, a1));
      }
      break;
    case 124:
      HASH_GUARD(0x56A7922F2498CFFCLL, numberingroup) {
        if (count <= 1) return (ti_numberingroup(o_getClassName(), a0));
        if (count == 2) return (ti_numberingroup(o_getClassName(), a0, a1));
        return (ti_numberingroup(o_getClassName(), a0, a1, a2));
      }
      break;
    case 125:
      HASH_GUARD(0x577562411EAFEDFDLL, subjectpagename) {
        if (count <= 1) return (ti_subjectpagename(o_getClassName(), a0));
        return (ti_subjectpagename(o_getClassName(), a0, a1));
      }
      break;
    case 127:
      HASH_GUARD(0x560F8E26E840C57FLL, pagesincategory) {
        if (count <= 1) return (ti_pagesincategory(o_getClassName(), a0));
        if (count == 2) return (ti_pagesincategory(o_getClassName(), a0, a1));
        return (ti_pagesincategory(o_getClassName(), a0, a1, a2));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_coreparserfunctions::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 127) {
    case 5:
      HASH_GUARD(0x35B7424DC6DDC785LL, subpagename) {
        int count = params.size();
        if (count <= 1) return (ti_subpagename(c, params.rvalAt(0)));
        return (ti_subpagename(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 9:
      HASH_GUARD(0x3551C2202E31F909LL, special) {
        return (ti_special(c, params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x359F7DDBB439CC09LL, filepath) {
        int count = params.size();
        if (count <= 1) return (ti_filepath(c, params.rvalAt(0)));
        if (count == 2) return (ti_filepath(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_filepath(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 10:
      HASH_GUARD(0x08D08963095FA98ALL, localurl) {
        int count = params.size();
        if (count <= 1) return (ti_localurl(c, params.rvalAt(0)));
        if (count == 2) return (ti_localurl(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_localurl(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 16:
      HASH_GUARD(0x1CB8D83F9130E490LL, formatdate) {
        int count = params.size();
        if (count <= 2) return (ti_formatdate(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_formatdate(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 18:
      HASH_GUARD(0x05FAA2085D94FE12LL, urlencode) {
        int count = params.size();
        if (count <= 1) return (ti_urlencode(c, params.rvalAt(0)));
        return (ti_urlencode(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 20:
      HASH_GUARD(0x051F57783523D714LL, pagesinnamespace) {
        int count = params.size();
        if (count <= 1) return (ti_pagesinnamespace(c, params.rvalAt(0)));
        if (count == 2) return (ti_pagesinnamespace(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_pagesinnamespace(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 25:
      HASH_GUARD(0x6EB6D615714DF099LL, anchorencode) {
        return (ti_anchorencode(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 28:
      HASH_GUARD(0x0191205BE4FF8F9CLL, lc) {
        int count = params.size();
        if (count <= 1) return (ti_lc(c, params.rvalAt(0)));
        return (ti_lc(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 32:
      HASH_GUARD(0x34DEB5D98BDFF6A0LL, fullurl) {
        int count = params.size();
        if (count <= 1) return (ti_fullurl(c, params.rvalAt(0)));
        if (count == 2) return (ti_fullurl(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_fullurl(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      HASH_GUARD(0x09928523585E54A0LL, displaytitle) {
        int count = params.size();
        if (count <= 1) return (ti_displaytitle(c, params.rvalAt(0)));
        return (ti_displaytitle(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 33:
      HASH_GUARD(0x011E1E4D7CFABEA1LL, basepagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_basepagenamee(c, params.rvalAt(0)));
        return (ti_basepagenamee(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 34:
      HASH_GUARD(0x074544A52D6A5422LL, talkpagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_talkpagenamee(c, params.rvalAt(0)));
        return (ti_talkpagenamee(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 36:
      HASH_GUARD(0x51260D879D3BB124LL, talkspacee) {
        int count = params.size();
        if (count <= 1) return (ti_talkspacee(c, params.rvalAt(0)));
        return (ti_talkspacee(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 37:
      HASH_GUARD(0x582523D03167F625LL, basepagename) {
        int count = params.size();
        if (count <= 1) return (ti_basepagename(c, params.rvalAt(0)));
        return (ti_basepagename(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 38:
      HASH_GUARD(0x503A779628C29926LL, intfunction) {
        int count = params.size();
        if (count <= 1) return (ti_intfunction(c,count, params.rvalAt(0)));
        if (count == 2) return (ti_intfunction(c,count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_intfunction(c,count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 39:
      HASH_GUARD(0x6200C73E146615A7LL, subjectspacee) {
        int count = params.size();
        if (count <= 1) return (ti_subjectspacee(c, params.rvalAt(0)));
        return (ti_subjectspacee(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 40:
      HASH_GUARD(0x1486123D24E4C8A8LL, localurle) {
        int count = params.size();
        if (count <= 1) return (ti_localurle(c, params.rvalAt(0)));
        if (count == 2) return (ti_localurle(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_localurle(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 45:
      HASH_GUARD(0x4638E1CDC3D8982DLL, language) {
        int count = params.size();
        if (count <= 1) return (ti_language(c, params.rvalAt(0)));
        return (ti_language(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 47:
      HASH_GUARD(0x6ABED8C07DA91FAFLL, talkspace) {
        int count = params.size();
        if (count <= 1) return (ti_talkspace(c, params.rvalAt(0)));
        return (ti_talkspace(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 49:
      HASH_GUARD(0x364B31DA720A6131LL, grammar) {
        int count = params.size();
        if (count <= 1) return (ti_grammar(c, params.rvalAt(0)));
        if (count == 2) return (ti_grammar(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_grammar(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 51:
      HASH_GUARD(0x7448B5BAF0B032B3LL, lcfirst) {
        int count = params.size();
        if (count <= 1) return (ti_lcfirst(c, params.rvalAt(0)));
        return (ti_lcfirst(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 53:
      HASH_GUARD(0x2D44AE5D8AEB6E35LL, subjectspace) {
        int count = params.size();
        if (count <= 1) return (ti_subjectspace(c, params.rvalAt(0)));
        return (ti_subjectspace(c, params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x6FEE7241B788FA35LL, numberofusers) {
        int count = params.size();
        if (count <= 1) return (ti_numberofusers(c, params.rvalAt(0)));
        return (ti_numberofusers(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 63:
      HASH_GUARD(0x24206A195B9C203FLL, ucfirst) {
        int count = params.size();
        if (count <= 1) return (ti_ucfirst(c, params.rvalAt(0)));
        return (ti_ucfirst(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 68:
      HASH_GUARD(0x086A22F539BFE744LL, talkpagename) {
        int count = params.size();
        if (count <= 1) return (ti_talkpagename(c, params.rvalAt(0)));
        return (ti_talkpagename(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 70:
      HASH_GUARD(0x2FD3C556DBEC8046LL, subjectpagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_subjectpagenamee(c, params.rvalAt(0)));
        return (ti_subjectpagenamee(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 72:
      HASH_GUARD(0x244F137FE9602148LL, protectionlevel) {
        int count = params.size();
        if (count <= 1) return (ti_protectionlevel(c, params.rvalAt(0)));
        return (ti_protectionlevel(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 75:
      HASH_GUARD(0x638DFF1F9ED1A7CBLL, mwnamespace) {
        int count = params.size();
        if (count <= 1) return (ti_mwnamespace(c, params.rvalAt(0)));
        return (ti_mwnamespace(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 77:
      HASH_GUARD(0x09958033D977C0CDLL, plural) {
        int count = params.size();
        if (count <= 1) return (ti_plural(c,count, params.rvalAt(0)));
        if (count == 2) return (ti_plural(c,count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_plural(c,count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      break;
    case 82:
      HASH_GUARD(0x18956BD975EB0AD2LL, fullpagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_fullpagenamee(c, params.rvalAt(0)));
        return (ti_fullpagenamee(c, params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x6389384D327B8DD2LL, subpagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_subpagenamee(c, params.rvalAt(0)));
        return (ti_subpagenamee(c, params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x1ED58DF7346FAFD2LL, padright) {
        int count = params.size();
        if (count <= 1) return (ti_padright(c, params.rvalAt(0)));
        if (count == 2) return (ti_padright(c, params.rvalAt(0), params.rvalAt(1)));
        if (count == 3) return (ti_padright(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        return (ti_padright(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
      }
      break;
    case 83:
      HASH_GUARD(0x7D776D0AEA9F38D3LL, nse) {
        int count = params.size();
        if (count <= 1) return (ti_nse(c, params.rvalAt(0)));
        return (ti_nse(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 85:
      HASH_GUARD(0x6CEC0D016D9655D5LL, ns) {
        int count = params.size();
        if (count <= 1) return (ti_ns(c, params.rvalAt(0)));
        return (ti_ns(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 90:
      HASH_GUARD(0x0FEB8F5DDB8EBB5ALL, formatnum) {
        int count = params.size();
        if (count <= 1) return (ti_formatnum(c, params.rvalAt(0)));
        if (count == 2) return (ti_formatnum(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_formatnum(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 95:
      HASH_GUARD(0x5681CDFC9A4BD65FLL, padleft) {
        int count = params.size();
        if (count <= 1) return (ti_padleft(c, params.rvalAt(0)));
        if (count == 2) return (ti_padleft(c, params.rvalAt(0), params.rvalAt(1)));
        if (count == 3) return (ti_padleft(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
        return (ti_padleft(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3)));
      }
      break;
    case 96:
      HASH_GUARD(0x6403181CDD90B160LL, numberofarticles) {
        int count = params.size();
        if (count <= 1) return (ti_numberofarticles(c, params.rvalAt(0)));
        return (ti_numberofarticles(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 99:
      HASH_GUARD(0x48B122ACB9475BE3LL, numberofadmins) {
        int count = params.size();
        if (count <= 1) return (ti_numberofadmins(c, params.rvalAt(0)));
        return (ti_numberofadmins(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 102:
      HASH_GUARD(0x4A887CFE1BC101E6LL, defaultsort) {
        return (ti_defaultsort(c, params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x5DB1F157D242C7E6LL, tagobj) {
        return (ti_tagobj(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 104:
      HASH_GUARD(0x498C20093EED53E8LL, fullpagename) {
        int count = params.size();
        if (count <= 1) return (ti_fullpagename(c, params.rvalAt(0)));
        return (ti_fullpagename(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 109:
      HASH_GUARD(0x4207BDDD2C59BA6DLL, gender) {
        int count = params.size();
        if (count <= 2) return (ti_gender(c,count, params.rvalAt(0), params.rvalAt(1)));
        return (ti_gender(c,count,params.rvalAt(0), params.rvalAt(1), params.slice(2, count - 2, false)));
      }
      HASH_GUARD(0x57C3D544AB79B6EDLL, pagename) {
        int count = params.size();
        if (count <= 1) return (ti_pagename(c, params.rvalAt(0)));
        return (ti_pagename(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 113:
      HASH_GUARD(0x25197E8E5501F271LL, uc) {
        int count = params.size();
        if (count <= 1) return (ti_uc(c, params.rvalAt(0)));
        return (ti_uc(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 114:
      HASH_GUARD(0x08935F4176449D72LL, numberofactiveusers) {
        int count = params.size();
        if (count <= 1) return (ti_numberofactiveusers(c, params.rvalAt(0)));
        return (ti_numberofactiveusers(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 116:
      HASH_GUARD(0x47C3ED014D9D0774LL, numberofpages) {
        int count = params.size();
        if (count <= 1) return (ti_numberofpages(c, params.rvalAt(0)));
        return (ti_numberofpages(c, params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x46BFC9BC72D7EC74LL, pagesize) {
        int count = params.size();
        if (count <= 1) return (ti_pagesize(c, params.rvalAt(0)));
        if (count == 2) return (ti_pagesize(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_pagesize(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 118:
      HASH_GUARD(0x04CE8705E23B4576LL, numberofedits) {
        int count = params.size();
        if (count <= 1) return (ti_numberofedits(c, params.rvalAt(0)));
        return (ti_numberofedits(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 119:
      HASH_GUARD(0x60214AC5E7E359F7LL, namespacee) {
        int count = params.size();
        if (count <= 1) return (ti_namespacee(c, params.rvalAt(0)));
        return (ti_namespacee(c, params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x6BCAE6ECCB5DEEF7LL, numberoffiles) {
        int count = params.size();
        if (count <= 1) return (ti_numberoffiles(c, params.rvalAt(0)));
        return (ti_numberoffiles(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 120:
      HASH_GUARD(0x243DB740F9A237F8LL, fullurle) {
        int count = params.size();
        if (count <= 1) return (ti_fullurle(c, params.rvalAt(0)));
        if (count == 2) return (ti_fullurle(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_fullurle(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 121:
      HASH_GUARD(0x1E98450B38F85F79LL, numberofviews) {
        int count = params.size();
        if (count <= 1) return (ti_numberofviews(c, params.rvalAt(0)));
        return (ti_numberofviews(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 122:
      HASH_GUARD(0x16A117F2904BE67ALL, pagenamee) {
        int count = params.size();
        if (count <= 1) return (ti_pagenamee(c, params.rvalAt(0)));
        return (ti_pagenamee(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 124:
      HASH_GUARD(0x56A7922F2498CFFCLL, numberingroup) {
        int count = params.size();
        if (count <= 1) return (ti_numberingroup(c, params.rvalAt(0)));
        if (count == 2) return (ti_numberingroup(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_numberingroup(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    case 125:
      HASH_GUARD(0x577562411EAFEDFDLL, subjectpagename) {
        int count = params.size();
        if (count <= 1) return (ti_subjectpagename(c, params.rvalAt(0)));
        return (ti_subjectpagename(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 127:
      HASH_GUARD(0x560F8E26E840C57FLL, pagesincategory) {
        int count = params.size();
        if (count <= 1) return (ti_pagesincategory(c, params.rvalAt(0)));
        if (count == 2) return (ti_pagesincategory(c, params.rvalAt(0), params.rvalAt(1)));
        return (ti_pagesincategory(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_coreparserfunctions$os_get(const char *s) {
  return c_coreparserfunctions::os_get(s, -1);
}
Variant &cw_coreparserfunctions$os_lval(const char *s) {
  return c_coreparserfunctions::os_lval(s, -1);
}
Variant cw_coreparserfunctions$os_constant(const char *s) {
  return c_coreparserfunctions::os_constant(s);
}
Variant cw_coreparserfunctions$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_coreparserfunctions::os_invoke(c, s, params, -1, fatal);
}
void c_coreparserfunctions::init() {
}
/* SRC: CoreParserFunctions.php line 8 */
void c_coreparserfunctions::ti_register(const char* cls, p_parser v_parser) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::register);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgAllowDisplayTitle __attribute__((__unused__)) = g->GV(wgAllowDisplayTitle);
  Variant &gv_wgAllowSlowParserFunctions __attribute__((__unused__)) = g->GV(wgAllowSlowParserFunctions);
  {
  }
  LINE(17,v_parser->t_setfunctionhook("int", ScalarArrays::sa_[39], k_SFH_NO_HASH));
  LINE(18,v_parser->t_setfunctionhook("ns", ScalarArrays::sa_[40], k_SFH_NO_HASH));
  LINE(19,v_parser->t_setfunctionhook("nse", ScalarArrays::sa_[41], k_SFH_NO_HASH));
  LINE(20,v_parser->t_setfunctionhook("urlencode", ScalarArrays::sa_[42], k_SFH_NO_HASH));
  LINE(21,v_parser->t_setfunctionhook("lcfirst", ScalarArrays::sa_[43], k_SFH_NO_HASH));
  LINE(22,v_parser->t_setfunctionhook("ucfirst", ScalarArrays::sa_[44], k_SFH_NO_HASH));
  LINE(23,v_parser->t_setfunctionhook("lc", ScalarArrays::sa_[45], k_SFH_NO_HASH));
  LINE(24,v_parser->t_setfunctionhook("uc", ScalarArrays::sa_[46], k_SFH_NO_HASH));
  LINE(25,v_parser->t_setfunctionhook("localurl", ScalarArrays::sa_[47], k_SFH_NO_HASH));
  LINE(26,v_parser->t_setfunctionhook("localurle", ScalarArrays::sa_[48], k_SFH_NO_HASH));
  LINE(27,v_parser->t_setfunctionhook("fullurl", ScalarArrays::sa_[49], k_SFH_NO_HASH));
  LINE(28,v_parser->t_setfunctionhook("fullurle", ScalarArrays::sa_[50], k_SFH_NO_HASH));
  LINE(29,v_parser->t_setfunctionhook("formatnum", ScalarArrays::sa_[51], k_SFH_NO_HASH));
  LINE(30,v_parser->t_setfunctionhook("grammar", ScalarArrays::sa_[52], k_SFH_NO_HASH));
  LINE(31,v_parser->t_setfunctionhook("gender", ScalarArrays::sa_[53], k_SFH_NO_HASH));
  LINE(32,v_parser->t_setfunctionhook("plural", ScalarArrays::sa_[54], k_SFH_NO_HASH));
  LINE(33,v_parser->t_setfunctionhook("numberofpages", ScalarArrays::sa_[55], k_SFH_NO_HASH));
  LINE(34,v_parser->t_setfunctionhook("numberofusers", ScalarArrays::sa_[56], k_SFH_NO_HASH));
  LINE(35,v_parser->t_setfunctionhook("numberofactiveusers", ScalarArrays::sa_[57], k_SFH_NO_HASH));
  LINE(36,v_parser->t_setfunctionhook("numberofarticles", ScalarArrays::sa_[58], k_SFH_NO_HASH));
  LINE(37,v_parser->t_setfunctionhook("numberoffiles", ScalarArrays::sa_[59], k_SFH_NO_HASH));
  LINE(38,v_parser->t_setfunctionhook("numberofadmins", ScalarArrays::sa_[60], k_SFH_NO_HASH));
  LINE(39,v_parser->t_setfunctionhook("numberingroup", ScalarArrays::sa_[61], k_SFH_NO_HASH));
  LINE(40,v_parser->t_setfunctionhook("numberofedits", ScalarArrays::sa_[62], k_SFH_NO_HASH));
  LINE(41,v_parser->t_setfunctionhook("numberofviews", ScalarArrays::sa_[63], k_SFH_NO_HASH));
  LINE(42,v_parser->t_setfunctionhook("language", ScalarArrays::sa_[64], k_SFH_NO_HASH));
  LINE(43,v_parser->t_setfunctionhook("padleft", ScalarArrays::sa_[65], k_SFH_NO_HASH));
  LINE(44,v_parser->t_setfunctionhook("padright", ScalarArrays::sa_[66], k_SFH_NO_HASH));
  LINE(45,v_parser->t_setfunctionhook("anchorencode", ScalarArrays::sa_[67], k_SFH_NO_HASH));
  LINE(46,v_parser->t_setfunctionhook("special", ScalarArrays::sa_[68]));
  LINE(47,v_parser->t_setfunctionhook("defaultsort", ScalarArrays::sa_[69], k_SFH_NO_HASH));
  LINE(48,v_parser->t_setfunctionhook("filepath", ScalarArrays::sa_[70], k_SFH_NO_HASH));
  LINE(49,v_parser->t_setfunctionhook("pagesincategory", ScalarArrays::sa_[71], k_SFH_NO_HASH));
  LINE(50,v_parser->t_setfunctionhook("pagesize", ScalarArrays::sa_[72], k_SFH_NO_HASH));
  LINE(51,v_parser->t_setfunctionhook("protectionlevel", ScalarArrays::sa_[73], k_SFH_NO_HASH));
  LINE(52,v_parser->t_setfunctionhook("namespace", ScalarArrays::sa_[74], k_SFH_NO_HASH));
  LINE(53,v_parser->t_setfunctionhook("namespacee", ScalarArrays::sa_[75], k_SFH_NO_HASH));
  LINE(54,v_parser->t_setfunctionhook("talkspace", ScalarArrays::sa_[76], k_SFH_NO_HASH));
  LINE(55,v_parser->t_setfunctionhook("talkspacee", ScalarArrays::sa_[77], k_SFH_NO_HASH));
  LINE(56,v_parser->t_setfunctionhook("subjectspace", ScalarArrays::sa_[78], k_SFH_NO_HASH));
  LINE(57,v_parser->t_setfunctionhook("subjectspacee", ScalarArrays::sa_[79], k_SFH_NO_HASH));
  LINE(58,v_parser->t_setfunctionhook("pagename", ScalarArrays::sa_[80], k_SFH_NO_HASH));
  LINE(59,v_parser->t_setfunctionhook("pagenamee", ScalarArrays::sa_[81], k_SFH_NO_HASH));
  LINE(60,v_parser->t_setfunctionhook("fullpagename", ScalarArrays::sa_[82], k_SFH_NO_HASH));
  LINE(61,v_parser->t_setfunctionhook("fullpagenamee", ScalarArrays::sa_[83], k_SFH_NO_HASH));
  LINE(62,v_parser->t_setfunctionhook("basepagename", ScalarArrays::sa_[84], k_SFH_NO_HASH));
  LINE(63,v_parser->t_setfunctionhook("basepagenamee", ScalarArrays::sa_[85], k_SFH_NO_HASH));
  LINE(64,v_parser->t_setfunctionhook("subpagename", ScalarArrays::sa_[86], k_SFH_NO_HASH));
  LINE(65,v_parser->t_setfunctionhook("subpagenamee", ScalarArrays::sa_[87], k_SFH_NO_HASH));
  LINE(66,v_parser->t_setfunctionhook("talkpagename", ScalarArrays::sa_[88], k_SFH_NO_HASH));
  LINE(67,v_parser->t_setfunctionhook("talkpagenamee", ScalarArrays::sa_[89], k_SFH_NO_HASH));
  LINE(68,v_parser->t_setfunctionhook("subjectpagename", ScalarArrays::sa_[90], k_SFH_NO_HASH));
  LINE(69,v_parser->t_setfunctionhook("subjectpagenamee", ScalarArrays::sa_[91], k_SFH_NO_HASH));
  LINE(70,v_parser->t_setfunctionhook("tag", ScalarArrays::sa_[92], k_SFH_OBJECT_ARGS));
  LINE(71,v_parser->t_setfunctionhook("formatdate", ScalarArrays::sa_[93]));
  if (toBoolean(gv_wgAllowDisplayTitle)) {
    LINE(74,v_parser->t_setfunctionhook("displaytitle", ScalarArrays::sa_[94], k_SFH_NO_HASH));
  }
  if (toBoolean(gv_wgAllowSlowParserFunctions)) {
    LINE(77,v_parser->t_setfunctionhook("pagesinnamespace", ScalarArrays::sa_[95], k_SFH_NO_HASH));
  }
} /* function */
/* SRC: CoreParserFunctions.php line 81 */
Variant c_coreparserfunctions::ti_intfunction(const char* cls, int num_args, CVarRef v_parser, Variant v_part1 //  = ""
, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::intFunction);
  Variant eo_0;
  Variant eo_1;
  Variant v_args;
  Variant v_message;

  if (!same(LINE(82,x_strval(v_part1)), "")) {
    (v_args = LINE(83,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(2).set(0, v_parser).set(1, v_part1).create()),args)),x_array_slice(eo_0, toInt32(2LL)))));
    (v_message = LINE(84,invoke_failed("wfmsggetkey", Array(ArrayInit(4).set(0, ref(v_part1)).set(1, true).set(2, false).set(3, false).create()), 0x00000000111683E0LL)));
    (v_message = LINE(85,invoke_failed("wfmsgreplaceargs", Array(ArrayInit(2).set(0, ref(v_message)).set(1, ref(v_args)).create()), 0x0000000084B4C707LL)));
    (v_message = LINE(86,toObject(v_parser)->o_invoke_few_args("replaceVariables", 0x655F4A045CA8303ALL, 1, v_message)));
    return v_message;
  }
  else {
    return ScalarArrays::sa_[96];
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 93 */
Variant c_coreparserfunctions::ti_formatdate(const char* cls, CVarRef v_parser, Variant v_date, CVarRef v_defaultPref //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::formatDate);
  Variant v_df;
  Variant v_pref;

  (v_df = LINE(94,c_dateformatter::t_getinstance()));
  (v_date = LINE(96,x_trim(toString(v_date))));
  (v_pref = LINE(98,toObject(v_parser).o_get("mOptions", 0x79AC63B0E96DC6E9LL).o_invoke_few_args("getDateFormat", 0x6EF12DB3679FD689LL, 0)));
  if (equal(v_pref, "default") && toBoolean(v_defaultPref)) (v_pref = v_defaultPref);
  (v_date = LINE(105,v_df.o_invoke_few_args("reformat", 0x59D9A29BB871E42ELL, 3, v_pref, v_date, Array(ArrayInit(1).set(0, "match-whole").create()))));
  return v_date;
} /* function */
/* SRC: CoreParserFunctions.php line 109 */
Variant c_coreparserfunctions::ti_ns(const char* cls, CVarRef v_parser, CVarRef v_part1 //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::ns);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_index;

  if (toBoolean(LINE(111,x_intval(v_part1))) || equal(v_part1, "0")) {
    (v_index = LINE(112,x_intval(v_part1)));
  }
  else {
    (v_index = LINE(114,gv_wgContLang.o_invoke_few_args("getNsIndex", 0x3C0869AE9723B850LL, 1, x_str_replace(" ", "_", v_part1))));
  }
  if (!same(v_index, false)) {
    return LINE(117,gv_wgContLang.o_invoke_few_args("getFormattedNsText", 0x4D89E9995199AB7BLL, 1, v_index));
  }
  else {
    return ScalarArrays::sa_[96];
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 123 */
Variant c_coreparserfunctions::ti_nse(const char* cls, CVarRef v_parser, CVarRef v_part1 //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::nse);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  return LINE(124,invoke_failed("wfurlencode", Array(ArrayInit(1).set(0, ref((assignCallTemp(eo_2, c_coreparserfunctions::t_ns(v_parser, v_part1)),x_str_replace(" ", "_", eo_2)))).create()), 0x00000000BFE709E0LL));
} /* function */
/* SRC: CoreParserFunctions.php line 127 */
String c_coreparserfunctions::ti_urlencode(const char* cls, CVarRef v_parser, CVarRef v_s //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::urlencode);
  return LINE(128,x_urlencode(toString(v_s)));
} /* function */
/* SRC: CoreParserFunctions.php line 131 */
Variant c_coreparserfunctions::ti_lcfirst(const char* cls, CVarRef v_parser, Variant v_s //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::lcfirst);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  return LINE(133,gv_wgContLang.o_invoke_few_args("lcfirst", 0x7448B5BAF0B032B3LL, 1, v_s));
} /* function */
/* SRC: CoreParserFunctions.php line 136 */
Variant c_coreparserfunctions::ti_ucfirst(const char* cls, CVarRef v_parser, Variant v_s //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::ucfirst);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  return LINE(138,gv_wgContLang.o_invoke_few_args("ucfirst", 0x24206A195B9C203FLL, 1, v_s));
} /* function */
/* SRC: CoreParserFunctions.php line 141 */
Variant c_coreparserfunctions::ti_lc(const char* cls, CVarRef v_parser, Variant v_s //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::lc);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  if (LINE(143,x_is_callable(Array(ArrayInit(2).set(0, v_parser).set(1, "markerSkipCallback").create())))) {
    return LINE(144,toObject(v_parser)->o_invoke_few_args("markerSkipCallback", 0x1919E33A3EB87298LL, 2, v_s, Array(ArrayInit(2).set(0, gv_wgContLang).set(1, "lc").create())));
  }
  else {
    return LINE(146,gv_wgContLang.o_invoke_few_args("lc", 0x0191205BE4FF8F9CLL, 1, v_s));
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 150 */
Variant c_coreparserfunctions::ti_uc(const char* cls, CVarRef v_parser, Variant v_s //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::uc);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  if (LINE(152,x_is_callable(Array(ArrayInit(2).set(0, v_parser).set(1, "markerSkipCallback").create())))) {
    return LINE(153,toObject(v_parser)->o_invoke_few_args("markerSkipCallback", 0x1919E33A3EB87298LL, 2, v_s, Array(ArrayInit(2).set(0, gv_wgContLang).set(1, "uc").create())));
  }
  else {
    return LINE(155,gv_wgContLang.o_invoke_few_args("uc", 0x25197E8E5501F271LL, 1, v_s));
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 159 */
Variant c_coreparserfunctions::ti_localurl(const char* cls, CVarRef v_parser, CVarRef v_s //  = ""
, CVarRef v_arg //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::localurl);
  return LINE(159,c_coreparserfunctions::t_urlfunction("getLocalURL", v_s, v_arg));
} /* function */
/* SRC: CoreParserFunctions.php line 160 */
Variant c_coreparserfunctions::ti_localurle(const char* cls, CVarRef v_parser, CVarRef v_s //  = ""
, CVarRef v_arg //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::localurle);
  return LINE(160,c_coreparserfunctions::t_urlfunction("escapeLocalURL", v_s, v_arg));
} /* function */
/* SRC: CoreParserFunctions.php line 161 */
Variant c_coreparserfunctions::ti_fullurl(const char* cls, CVarRef v_parser, CVarRef v_s //  = ""
, CVarRef v_arg //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::fullurl);
  return LINE(161,c_coreparserfunctions::t_urlfunction("getFullURL", v_s, v_arg));
} /* function */
/* SRC: CoreParserFunctions.php line 162 */
Variant c_coreparserfunctions::ti_fullurle(const char* cls, CVarRef v_parser, CVarRef v_s //  = ""
, CVarRef v_arg //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::fullurle);
  return LINE(162,c_coreparserfunctions::t_urlfunction("escapeFullURL", v_s, v_arg));
} /* function */
/* SRC: CoreParserFunctions.php line 164 */
Variant c_coreparserfunctions::ti_urlfunction(const char* cls, CStrRef v_func, CVarRef v_s //  = ""
, Variant v_arg //  = null
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::urlFunction);
  Variant eo_0;
  Variant eo_1;
  Variant v_title;
  Variant v_text;

  (v_title = LINE(165,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(170,x_is_null(v_title))) (v_title = LINE(171,throw_fatal("unknown class title", (x_urldecode(toString(v_s)), (void*)NULL))));
  if (!(LINE(172,x_is_null(v_title)))) {
    if (equal(LINE(174,v_title.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)), k_NS_MEDIA)) {
      (v_title = LINE(175,(assignCallTemp(eo_0, k_NS_FILE),assignCallTemp(eo_1, v_title.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)),throw_fatal("unknown class title", (v_title.o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0), (void*)NULL)))));
    }
    if (!(LINE(177,x_is_null(v_arg)))) {
      (v_text = LINE(178,v_title.o_invoke_few_args(v_func, -1LL, 1, v_arg)));
    }
    else {
      (v_text = LINE(180,v_title.o_invoke_few_args(v_func, -1LL, 0)));
    }
    return v_text;
  }
  else {
    return ScalarArrays::sa_[96];
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 188 */
Variant c_coreparserfunctions::ti_formatnum(const char* cls, CVarRef v_parser, Variant v_num //  = ""
, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::formatNum);
  Variant eo_0;
  Variant eo_1;
  if (toBoolean(LINE(189,c_coreparserfunctions::t_israw(v_raw)))) {
    return (assignCallTemp(eo_0, toObject(LINE(190,toObject(v_parser)->o_invoke_few_args("getFunctionLang", 0x3BBB4BCA49D08AEFLL, 0)))),eo_0.o_invoke_few_args("parseFormattedNumber", 0x656FA992A3120DB0LL, 1, v_num));
  }
  else {
    return (assignCallTemp(eo_1, toObject(LINE(192,toObject(v_parser)->o_invoke_few_args("getFunctionLang", 0x3BBB4BCA49D08AEFLL, 0)))),eo_1.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, v_num));
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 196 */
Variant c_coreparserfunctions::ti_grammar(const char* cls, CVarRef v_parser, Variant v_case //  = ""
, Variant v_word //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::grammar);
  Variant eo_0;
  return (assignCallTemp(eo_0, toObject(LINE(197,toObject(v_parser)->o_invoke_few_args("getFunctionLang", 0x3BBB4BCA49D08AEFLL, 0)))),eo_0.o_invoke_few_args("convertGrammar", 0x2CE3076EE088D4ABLL, 2, v_word, v_case));
} /* function */
/* SRC: CoreParserFunctions.php line 200 */
Variant c_coreparserfunctions::ti_gender(const char* cls, int num_args, CVarRef v_parser, Variant v_user, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::gender);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant v_forms;
  Variant v_gender;
  Variant v_title;
  Variant &gv_wgUser __attribute__((__unused__)) = g->GV(wgUser);
  Variant v_wgUser;
  Variant v_ret;

  LINE(201,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "CoreParserFunctions::gender").create()), 0x0000000075359BAFLL));
  (v_forms = LINE(202,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(2).set(0, v_parser).set(1, v_user).create()),args)),x_array_slice(eo_0, toInt32(2LL)))));
  (v_gender = LINE(205,throw_fatal("unknown class user", ((void*)NULL))));
  (v_title = LINE(208,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(210,x_is_object(v_title)) && equal(v_title.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0), k_NS_USER)) (v_user = LINE(211,v_title.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0)));
  (v_user = LINE(214,throw_fatal("unknown class user", ((void*)NULL))));
  if (toBoolean(v_user)) {
    (v_gender = LINE(216,v_user.o_invoke_few_args("getOption", 0x402DD1A85CAEA6C0LL, 1, "gender")));
  }
  else if (toBoolean(LINE(217,toObject(v_parser).o_get("mOptions", 0x79AC63B0E96DC6E9LL).o_invoke_few_args("getInterfaceMessage", 0x47CB1D8AB54E1850LL, 0)))) {
    v_wgUser = ref(g->GV(wgUser));
    (v_gender = LINE(219,v_wgUser.o_invoke_few_args("getOption", 0x402DD1A85CAEA6C0LL, 1, "gender")));
  }
  (v_ret = (assignCallTemp(eo_0, toObject(LINE(221,toObject(v_parser)->o_invoke_few_args("getFunctionLang", 0x3BBB4BCA49D08AEFLL, 0)))),eo_0.o_invoke_few_args("gender", 0x4207BDDD2C59BA6DLL, 2, v_gender, v_forms)));
  LINE(222,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "CoreParserFunctions::gender").create()), 0x00000000B599F276LL));
  return v_ret;
} /* function */
/* SRC: CoreParserFunctions.php line 225 */
Variant c_coreparserfunctions::ti_plural(const char* cls, int num_args, CVarRef v_parser, Variant v_text //  = ""
, Array args /* = Array() */) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::plural);
  Variant eo_0;
  Variant eo_1;
  Variant v_forms;

  (v_forms = LINE(226,(assignCallTemp(eo_0, func_get_args(num_args, Array(ArrayInit(2).set(0, v_parser).set(1, v_text).create()),args)),x_array_slice(eo_0, toInt32(2LL)))));
  (v_text = (assignCallTemp(eo_0, toObject(LINE(227,toObject(v_parser)->o_invoke_few_args("getFunctionLang", 0x3BBB4BCA49D08AEFLL, 0)))),eo_0.o_invoke_few_args("parseFormattedNumber", 0x656FA992A3120DB0LL, 1, v_text)));
  return (assignCallTemp(eo_1, toObject(LINE(228,toObject(v_parser)->o_invoke_few_args("getFunctionLang", 0x3BBB4BCA49D08AEFLL, 0)))),eo_1.o_invoke_few_args("convertPlural", 0x472611E72C5AF005LL, 2, v_text, v_forms));
} /* function */
/* SRC: CoreParserFunctions.php line 239 */
String c_coreparserfunctions::ti_displaytitle(const char* cls, CVarRef v_parser, Variant v_text //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::displaytitle);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant eo_6;
  Variant eo_7;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgRestrictDisplayTitle __attribute__((__unused__)) = g->GV(wgRestrictDisplayTitle);
  Array v_bad;
  Variant v_title;

  (v_text = LINE(243,toObject(v_parser)->o_invoke_few_args("doQuotes", 0x7E4180EEBEB7A62FLL, 1, v_text)));
  (v_text = LINE(247,(assignCallTemp(eo_0, concat(concat_rev(x_preg_quote("-QINU\177" /* parser::MARKER_SUFFIX */, "/"), (assignCallTemp(eo_4, LINE(246,(assignCallTemp(eo_6, toString(toObject(v_parser)->o_invoke_few_args("uniqPrefix", 0x524D522D7D9607FALL, 0))),x_preg_quote(eo_6, "/")))),concat3("/", eo_4, ".*\?"))), "/")),assignCallTemp(eo_2, v_text),x_preg_replace(eo_0, "", eo_2))));
  (v_bad = ScalarArrays::sa_[97]);
  (v_text = LINE(257,throw_fatal("unknown class sanitizer", (throw_fatal("unknown class sanitizer", ((void*)NULL)), (void*)NULL))));
  (v_title = LINE(258,throw_fatal("unknown class title", (throw_fatal("unknown class sanitizer", ((void*)NULL)), (void*)NULL))));
  if (!(toBoolean(gv_wgRestrictDisplayTitle))) {
    LINE(261,toObject(v_parser).o_get("mOutput", 0x7F7FE020D521DBD0LL).o_invoke_few_args("setDisplayTitle", 0x325093066EFE0201LL, 1, v_text));
  }
  else {
    if (instanceOf(v_title, "Title") && equal(LINE(263,v_title.o_invoke_few_args("getFragment", 0x2354C40909546D77LL, 0)), "") && toBoolean(v_title.o_invoke_few_args("equals", 0x6E9F97FFEE61A498LL, 1, toObject(v_parser).o_lval("mTitle", 0x4C804C46307BF0EALL)))) {
      LINE(264,toObject(v_parser).o_get("mOutput", 0x7F7FE020D521DBD0LL).o_invoke_few_args("setDisplayTitle", 0x325093066EFE0201LL, 1, v_text));
    }
  }
  return "";
} /* function */
/* SRC: CoreParserFunctions.php line 271 */
Variant c_coreparserfunctions::ti_israw(const char* cls, Variant v_param) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::isRaw);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_mwRaw __attribute__((__unused__)) = g->sv_coreparserfunctions$$israw$$mwRaw.lvalAt(cls);
  Variant &inited_sv_mwRaw __attribute__((__unused__)) = g->inited_sv_coreparserfunctions$$israw$$mwRaw.lvalAt(cls);
  if (!inited_sv_mwRaw) {
    (sv_mwRaw = null);
    inited_sv_mwRaw = true;
  }
  if (!(toBoolean(sv_mwRaw))) {
    (sv_mwRaw = ref(LINE(274,throw_fatal("unknown class magicword", ((void*)NULL)))));
  }
  if (LINE(276,x_is_null(v_param))) {
    return false;
  }
  else {
    return LINE(279,sv_mwRaw.o_invoke_few_args("match", 0x2B8782BF7206E093LL, 1, v_param));
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 283 */
Variant c_coreparserfunctions::ti_formatraw(const char* cls, Variant v_num, CVarRef v_raw) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::formatRaw);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_wgContLang;

  if (toBoolean(LINE(284,c_coreparserfunctions::t_israw(v_raw)))) {
    return v_num;
  }
  else {
    v_wgContLang = ref(g->GV(wgContLang));
    return LINE(288,v_wgContLang.o_invoke_few_args("formatNum", 0x0FEB8F5DDB8EBB5ALL, 1, v_num));
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 291 */
Variant c_coreparserfunctions::ti_numberofpages(const char* cls, CVarRef v_parser, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::numberofpages);
  Variant eo_0;
  Variant eo_1;
  return LINE(292,(assignCallTemp(eo_0, throw_fatal("unknown class sitestats", ((void*)NULL))),assignCallTemp(eo_1, v_raw),c_coreparserfunctions::t_formatraw(eo_0, eo_1)));
} /* function */
/* SRC: CoreParserFunctions.php line 294 */
Variant c_coreparserfunctions::ti_numberofusers(const char* cls, CVarRef v_parser, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::numberofusers);
  Variant eo_0;
  Variant eo_1;
  return LINE(295,(assignCallTemp(eo_0, throw_fatal("unknown class sitestats", ((void*)NULL))),assignCallTemp(eo_1, v_raw),c_coreparserfunctions::t_formatraw(eo_0, eo_1)));
} /* function */
/* SRC: CoreParserFunctions.php line 297 */
Variant c_coreparserfunctions::ti_numberofactiveusers(const char* cls, CVarRef v_parser, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::numberofactiveusers);
  Variant eo_0;
  Variant eo_1;
  return LINE(298,(assignCallTemp(eo_0, throw_fatal("unknown class sitestats", ((void*)NULL))),assignCallTemp(eo_1, v_raw),c_coreparserfunctions::t_formatraw(eo_0, eo_1)));
} /* function */
/* SRC: CoreParserFunctions.php line 300 */
Variant c_coreparserfunctions::ti_numberofarticles(const char* cls, CVarRef v_parser, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::numberofarticles);
  Variant eo_0;
  Variant eo_1;
  return LINE(301,(assignCallTemp(eo_0, throw_fatal("unknown class sitestats", ((void*)NULL))),assignCallTemp(eo_1, v_raw),c_coreparserfunctions::t_formatraw(eo_0, eo_1)));
} /* function */
/* SRC: CoreParserFunctions.php line 303 */
Variant c_coreparserfunctions::ti_numberoffiles(const char* cls, CVarRef v_parser, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::numberoffiles);
  Variant eo_0;
  Variant eo_1;
  return LINE(304,(assignCallTemp(eo_0, throw_fatal("unknown class sitestats", ((void*)NULL))),assignCallTemp(eo_1, v_raw),c_coreparserfunctions::t_formatraw(eo_0, eo_1)));
} /* function */
/* SRC: CoreParserFunctions.php line 306 */
Variant c_coreparserfunctions::ti_numberofadmins(const char* cls, CVarRef v_parser, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::numberofadmins);
  Variant eo_0;
  Variant eo_1;
  return LINE(307,(assignCallTemp(eo_0, throw_fatal("unknown class sitestats", ((void*)NULL))),assignCallTemp(eo_1, v_raw),c_coreparserfunctions::t_formatraw(eo_0, eo_1)));
} /* function */
/* SRC: CoreParserFunctions.php line 309 */
Variant c_coreparserfunctions::ti_numberofedits(const char* cls, CVarRef v_parser, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::numberofedits);
  Variant eo_0;
  Variant eo_1;
  return LINE(310,(assignCallTemp(eo_0, throw_fatal("unknown class sitestats", ((void*)NULL))),assignCallTemp(eo_1, v_raw),c_coreparserfunctions::t_formatraw(eo_0, eo_1)));
} /* function */
/* SRC: CoreParserFunctions.php line 312 */
Variant c_coreparserfunctions::ti_numberofviews(const char* cls, CVarRef v_parser, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::numberofviews);
  Variant eo_0;
  Variant eo_1;
  return LINE(313,(assignCallTemp(eo_0, throw_fatal("unknown class sitestats", ((void*)NULL))),assignCallTemp(eo_1, v_raw),c_coreparserfunctions::t_formatraw(eo_0, eo_1)));
} /* function */
/* SRC: CoreParserFunctions.php line 315 */
Variant c_coreparserfunctions::ti_pagesinnamespace(const char* cls, CVarRef v_parser, CVarRef v_namespace //  = 0LL
, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::pagesinnamespace);
  Variant eo_0;
  Variant eo_1;
  return LINE(316,(assignCallTemp(eo_0, throw_fatal("unknown class sitestats", (x_intval(v_namespace), (void*)NULL))),assignCallTemp(eo_1, v_raw),c_coreparserfunctions::t_formatraw(eo_0, eo_1)));
} /* function */
/* SRC: CoreParserFunctions.php line 318 */
Variant c_coreparserfunctions::ti_numberingroup(const char* cls, CVarRef v_parser, CVarRef v_name //  = ""
, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::numberingroup);
  Variant eo_0;
  Variant eo_1;
  return LINE(319,(assignCallTemp(eo_0, throw_fatal("unknown class sitestats", (x_strtolower(toString(v_name)), (void*)NULL))),assignCallTemp(eo_1, v_raw),c_coreparserfunctions::t_formatraw(eo_0, eo_1)));
} /* function */
/* SRC: CoreParserFunctions.php line 329 */
Variant c_coreparserfunctions::ti_mwnamespace(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::mwnamespace);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_t;

  (v_t = LINE(330,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(331,x_is_null(v_t))) return "";
  return LINE(333,(assignCallTemp(eo_2, v_t.o_invoke_few_args("getNsText", 0x3A9D9935C2A87562LL, 0)),x_str_replace("_", " ", eo_2)));
} /* function */
/* SRC: CoreParserFunctions.php line 335 */
Variant c_coreparserfunctions::ti_namespacee(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::namespacee);
  Variant v_t;

  (v_t = LINE(336,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(337,x_is_null(v_t))) return "";
  return LINE(339,invoke_failed("wfurlencode", Array(ArrayInit(1).set(0, ref(v_t.o_invoke_few_args("getNsText", 0x3A9D9935C2A87562LL, 0))).create()), 0x00000000BFE709E0LL));
} /* function */
/* SRC: CoreParserFunctions.php line 341 */
Variant c_coreparserfunctions::ti_talkspace(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::talkspace);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_t;

  (v_t = LINE(342,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(343,x_is_null(v_t)) || !(toBoolean(v_t.o_invoke_few_args("canTalk", 0x6F6C6847DA1689A2LL, 0)))) return "";
  return LINE(345,(assignCallTemp(eo_2, v_t.o_invoke_few_args("getTalkNsText", 0x51ABF4A6AFECC524LL, 0)),x_str_replace("_", " ", eo_2)));
} /* function */
/* SRC: CoreParserFunctions.php line 347 */
Variant c_coreparserfunctions::ti_talkspacee(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::talkspacee);
  Variant v_t;

  (v_t = LINE(348,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(349,x_is_null(v_t)) || !(toBoolean(v_t.o_invoke_few_args("canTalk", 0x6F6C6847DA1689A2LL, 0)))) return "";
  return LINE(351,invoke_failed("wfurlencode", Array(ArrayInit(1).set(0, ref(v_t.o_invoke_few_args("getTalkNsText", 0x51ABF4A6AFECC524LL, 0))).create()), 0x00000000BFE709E0LL));
} /* function */
/* SRC: CoreParserFunctions.php line 353 */
Variant c_coreparserfunctions::ti_subjectspace(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::subjectspace);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_t;

  (v_t = LINE(354,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(355,x_is_null(v_t))) return "";
  return LINE(357,(assignCallTemp(eo_2, v_t.o_invoke_few_args("getSubjectNsText", 0x601B23155AB888BDLL, 0)),x_str_replace("_", " ", eo_2)));
} /* function */
/* SRC: CoreParserFunctions.php line 359 */
Variant c_coreparserfunctions::ti_subjectspacee(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::subjectspacee);
  Variant v_t;

  (v_t = LINE(360,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(361,x_is_null(v_t))) return "";
  return LINE(363,invoke_failed("wfurlencode", Array(ArrayInit(1).set(0, ref(v_t.o_invoke_few_args("getSubjectNsText", 0x601B23155AB888BDLL, 0))).create()), 0x00000000BFE709E0LL));
} /* function */
/* SRC: CoreParserFunctions.php line 369 */
Variant c_coreparserfunctions::ti_pagename(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::pagename);
  Variant v_t;

  (v_t = LINE(370,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(371,x_is_null(v_t))) return "";
  return LINE(373,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(v_t.o_invoke_few_args("getText", 0x434CEBEC8FDBB880LL, 0))).create()), 0x00000000B3382909LL));
} /* function */
/* SRC: CoreParserFunctions.php line 375 */
Variant c_coreparserfunctions::ti_pagenamee(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::pagenamee);
  Variant v_t;

  (v_t = LINE(376,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(377,x_is_null(v_t))) return "";
  return LINE(379,v_t.o_invoke_few_args("getPartialURL", 0x174317C1190941C3LL, 0));
} /* function */
/* SRC: CoreParserFunctions.php line 381 */
Variant c_coreparserfunctions::ti_fullpagename(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::fullpagename);
  Variant v_t;

  (v_t = LINE(382,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(383,x_is_null(v_t)) || !(toBoolean(v_t.o_invoke_few_args("canTalk", 0x6F6C6847DA1689A2LL, 0)))) return "";
  return LINE(385,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref(v_t.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0))).create()), 0x00000000B3382909LL));
} /* function */
/* SRC: CoreParserFunctions.php line 387 */
Variant c_coreparserfunctions::ti_fullpagenamee(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::fullpagenamee);
  Variant v_t;

  (v_t = LINE(388,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(389,x_is_null(v_t)) || !(toBoolean(v_t.o_invoke_few_args("canTalk", 0x6F6C6847DA1689A2LL, 0)))) return "";
  return LINE(391,v_t.o_invoke_few_args("getPrefixedURL", 0x087B6C631A0C440BLL, 0));
} /* function */
/* SRC: CoreParserFunctions.php line 393 */
Variant c_coreparserfunctions::ti_subpagename(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::subpagename);
  Variant v_t;

  (v_t = LINE(394,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(395,x_is_null(v_t))) return "";
  return LINE(397,v_t.o_invoke_few_args("getSubpageText", 0x01F4FD5FE8105897LL, 0));
} /* function */
/* SRC: CoreParserFunctions.php line 399 */
Variant c_coreparserfunctions::ti_subpagenamee(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::subpagenamee);
  Variant v_t;

  (v_t = LINE(400,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(401,x_is_null(v_t))) return "";
  return LINE(403,v_t.o_invoke_few_args("getSubpageUrlForm", 0x491DC72710364585LL, 0));
} /* function */
/* SRC: CoreParserFunctions.php line 405 */
Variant c_coreparserfunctions::ti_basepagename(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::basepagename);
  Variant v_t;

  (v_t = LINE(406,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(407,x_is_null(v_t))) return "";
  return LINE(409,v_t.o_invoke_few_args("getBaseText", 0x23DB8A9B7829E674LL, 0));
} /* function */
/* SRC: CoreParserFunctions.php line 411 */
Variant c_coreparserfunctions::ti_basepagenamee(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::basepagenamee);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant v_t;

  (v_t = LINE(412,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(413,x_is_null(v_t))) return "";
  return LINE(415,invoke_failed("wfurlencode", Array(ArrayInit(1).set(0, ref((assignCallTemp(eo_2, v_t.o_invoke_few_args("getBaseText", 0x23DB8A9B7829E674LL, 0)),x_str_replace(" ", "_", eo_2)))).create()), 0x00000000BFE709E0LL));
} /* function */
/* SRC: CoreParserFunctions.php line 417 */
Variant c_coreparserfunctions::ti_talkpagename(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::talkpagename);
  Variant eo_0;
  Variant v_t;

  (v_t = LINE(418,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(419,x_is_null(v_t)) || !(toBoolean(v_t.o_invoke_few_args("canTalk", 0x6F6C6847DA1689A2LL, 0)))) return "";
  return LINE(421,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref((assignCallTemp(eo_0, toObject(v_t.o_invoke_few_args("getTalkPage", 0x58258DF57A9BDD12LL, 0))),eo_0.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0)))).create()), 0x00000000B3382909LL));
} /* function */
/* SRC: CoreParserFunctions.php line 423 */
Variant c_coreparserfunctions::ti_talkpagenamee(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::talkpagenamee);
  Variant eo_0;
  Variant v_t;

  (v_t = LINE(424,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(425,x_is_null(v_t)) || !(toBoolean(v_t.o_invoke_few_args("canTalk", 0x6F6C6847DA1689A2LL, 0)))) return "";
  return (assignCallTemp(eo_0, toObject(LINE(427,v_t.o_invoke_few_args("getTalkPage", 0x58258DF57A9BDD12LL, 0)))),eo_0.o_invoke_few_args("getPrefixedUrl", 0x087B6C631A0C440BLL, 0));
} /* function */
/* SRC: CoreParserFunctions.php line 429 */
Variant c_coreparserfunctions::ti_subjectpagename(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::subjectpagename);
  Variant eo_0;
  Variant v_t;

  (v_t = LINE(430,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(431,x_is_null(v_t))) return "";
  return LINE(433,invoke_failed("wfescapewikitext", Array(ArrayInit(1).set(0, ref((assignCallTemp(eo_0, toObject(v_t.o_invoke_few_args("getSubjectPage", 0x366024925AE201F8LL, 0))),eo_0.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0)))).create()), 0x00000000B3382909LL));
} /* function */
/* SRC: CoreParserFunctions.php line 435 */
Variant c_coreparserfunctions::ti_subjectpagenamee(const char* cls, CVarRef v_parser, CVarRef v_title //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::subjectpagenamee);
  Variant eo_0;
  Variant v_t;

  (v_t = LINE(436,throw_fatal("unknown class title", ((void*)NULL))));
  if (LINE(437,x_is_null(v_t))) return "";
  return (assignCallTemp(eo_0, toObject(LINE(439,v_t.o_invoke_few_args("getSubjectPage", 0x366024925AE201F8LL, 0)))),eo_0.o_invoke_few_args("getPrefixedUrl", 0x087B6C631A0C440BLL, 0));
} /* function */
/* SRC: CoreParserFunctions.php line 447 */
Variant c_coreparserfunctions::ti_pagesincategory(const char* cls, CVarRef v_parser, Variant v_name //  = ""
, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::pagesincategory);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_cache __attribute__((__unused__)) = g->sv_coreparserfunctions$$pagesincategory$$cache.lvalAt(cls);
  Variant &inited_sv_cache __attribute__((__unused__)) = g->inited_sv_coreparserfunctions$$pagesincategory$$cache.lvalAt(cls);
  Variant v_category;
  Variant v_count;

  if (!inited_sv_cache) {
    (sv_cache = ScalarArrays::sa_[0]);
    inited_sv_cache = true;
  }
  (v_category = LINE(449,throw_fatal("unknown class category", ((void*)NULL))));
  if (!(LINE(451,x_is_object(v_category)))) {
    sv_cache.set(v_name, (0LL));
    return LINE(453,c_coreparserfunctions::t_formatraw(0LL, v_raw));
  }
  (v_name = LINE(457,v_category.o_invoke_few_args("getName", 0x23F51CDECC198965LL, 0)));
  (v_count = 0LL);
  if (isset(sv_cache, v_name)) {
    (v_count = sv_cache.rvalAt(v_name));
  }
  else if (toBoolean(LINE(462,toObject(v_parser)->o_invoke_few_args("incrementExpensiveFunctionCount", 0x6159C1C3A582E4D3LL, 0)))) {
    (v_count = sv_cache.set(v_name, (toInt64(LINE(463,v_category.o_invoke_few_args("getPageCount", 0x07171F09AF72AB88LL, 0))))));
  }
  return LINE(465,c_coreparserfunctions::t_formatraw(v_count, v_raw));
} /* function */
/* SRC: CoreParserFunctions.php line 479 */
Variant c_coreparserfunctions::ti_pagesize(const char* cls, CVarRef v_parser, Variant v_page //  = ""
, CVarRef v_raw //  = null_variant
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::pagesize);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &sv_cache __attribute__((__unused__)) = g->sv_coreparserfunctions$$pagesize$$cache.lvalAt(cls);
  Variant &inited_sv_cache __attribute__((__unused__)) = g->inited_sv_coreparserfunctions$$pagesize$$cache.lvalAt(cls);
  Variant v_title;
  Variant v_length;
  Variant v_rev;
  Variant v_id;

  if (!inited_sv_cache) {
    (sv_cache = ScalarArrays::sa_[0]);
    inited_sv_cache = true;
  }
  (v_title = LINE(481,throw_fatal("unknown class title", ((void*)NULL))));
  if (!(LINE(483,x_is_object(v_title)))) {
    sv_cache.set(v_page, (0LL));
    return LINE(485,c_coreparserfunctions::t_formatraw(0LL, v_raw));
  }
  (v_page = LINE(489,v_title.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0)));
  (v_length = 0LL);
  if (isset(sv_cache, v_page)) {
    (v_length = sv_cache.rvalAt(v_page));
  }
  else if (toBoolean(LINE(494,toObject(v_parser)->o_invoke_few_args("incrementExpensiveFunctionCount", 0x6159C1C3A582E4D3LL, 0)))) {
    (v_rev = LINE(495,throw_fatal("unknown class revision", ((void*)NULL))));
    (v_id = toBoolean(v_rev) ? ((Variant)(LINE(496,v_rev.o_invoke_few_args("getPage", 0x49A919C283C68A54LL, 0)))) : ((Variant)(0LL)));
    (v_length = sv_cache.set(v_page, (toBoolean(v_rev) ? ((Variant)(LINE(497,v_rev.o_invoke_few_args("getSize", 0x6615B5496D03A6EALL, 0)))) : ((Variant)(0LL)))));
    (assignCallTemp(eo_0, ref(v_title)),assignCallTemp(eo_1, ref(v_id)),assignCallTemp(eo_2, toBoolean(v_rev) ? ((Variant)(LINE(500,v_rev.o_invoke_few_args("getId", 0x5ADBB1D3A7BA92B8LL, 0)))) : ((Variant)(0LL))),toObject(v_parser).o_get("mOutput", 0x7F7FE020D521DBD0LL).o_invoke("addTemplate", Array(ArrayInit(3).set(0, eo_0).set(1, eo_1).set(2, eo_2).create()), 0x668299C8203F7926LL));
  }
  return LINE(502,c_coreparserfunctions::t_formatraw(v_length, v_raw));
} /* function */
/* SRC: CoreParserFunctions.php line 508 */
String c_coreparserfunctions::ti_protectionlevel(const char* cls, CVarRef v_parser, CVarRef v_type //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::protectionlevel);
  Variant v_restrictions;

  (v_restrictions = LINE(509,toObject(v_parser).o_get("mTitle", 0x4C804C46307BF0EALL).o_invoke_few_args("getRestrictions", 0x759EDC9A4F3FB6CALL, 1, x_strtolower(toString(v_type)))));
  return LINE(512,x_implode(v_restrictions, ","));
} /* function */
/* SRC: CoreParserFunctions.php line 515 */
Variant c_coreparserfunctions::ti_language(const char* cls, CVarRef v_parser, CVarRef v_arg //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::language);
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant v_lang;

  (v_lang = LINE(517,gv_wgContLang.o_invoke_few_args("getLanguageName", 0x3D24C2035027A7DELL, 1, x_strtolower(toString(v_arg)))));
  return !equal(v_lang, "") ? ((Variant)(v_lang)) : ((Variant)(v_arg));
} /* function */
/* SRC: CoreParserFunctions.php line 524 */
Variant c_coreparserfunctions::ti_pad(const char* cls, CVarRef v_string, Variant v_length, CVarRef v_padding //  = "0"
, int64 v_direction //  = 1LL /* STR_PAD_RIGHT */
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::pad);
  Variant v_lengthOfPadding;
  String v_finalPadding;

  (v_lengthOfPadding = LINE(525,x_mb_strlen(toString(v_padding))));
  if (equal(v_lengthOfPadding, 0LL)) return v_string;
  (v_length = minus_rev(LINE(529,x_mb_strlen(toString(v_string))), x_min(2, v_length, ScalarArrays::sa_[98])));
  (v_finalPadding = "");
  LOOP_COUNTER(1);
  {
    while (more(v_length, 0LL)) {
      LOOP_COUNTER_CHECK(1);
      {
        concat_assign(v_finalPadding, toString(LINE(536,x_mb_substr(toString(v_padding), toInt32(0LL), toInt32(v_length)))));
        v_length -= v_lengthOfPadding;
      }
    }
  }
  if (equal(v_direction, 0LL /* STR_PAD_LEFT */)) {
    return concat(v_finalPadding, toString(v_string));
  }
  else {
    return concat(toString(v_string), v_finalPadding);
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 547 */
Variant c_coreparserfunctions::ti_padleft(const char* cls, CVarRef v_parser, CVarRef v_string //  = ""
, CVarRef v_length //  = 0LL
, CVarRef v_padding //  = "0"
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::padleft);
  return LINE(548,c_coreparserfunctions::t_pad(v_string, v_length, v_padding, 0LL /* STR_PAD_LEFT */));
} /* function */
/* SRC: CoreParserFunctions.php line 551 */
Variant c_coreparserfunctions::ti_padright(const char* cls, CVarRef v_parser, CVarRef v_string //  = ""
, CVarRef v_length //  = 0LL
, CVarRef v_padding //  = "0"
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::padright);
  return LINE(552,c_coreparserfunctions::t_pad(v_string, v_length, v_padding));
} /* function */
/* SRC: CoreParserFunctions.php line 555 */
Variant c_coreparserfunctions::ti_anchorencode(const char* cls, CVarRef v_parser, CVarRef v_text) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::anchorencode);
  Variant v_a;

  (v_a = LINE(556,x_urlencode(toString(v_text))));
  (v_a = LINE(557,x_strtr(toString(v_a), ScalarArrays::sa_[99])));
  (v_a = LINE(559,x_str_replace(".3A", ":", v_a)));
  return v_a;
} /* function */
/* SRC: CoreParserFunctions.php line 563 */
Variant c_coreparserfunctions::ti_special(const char* cls, CVarRef v_parser, CVarRef v_text) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::special);
  Variant v_title;

  (v_title = LINE(564,throw_fatal("unknown class specialpage", ((void*)NULL))));
  if (toBoolean(v_title)) {
    return LINE(566,v_title.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0));
  }
  else {
    return LINE(568,invoke_failed("wfmsgforcontent", Array(ArrayInit(1).set(0, "nosuchspecialpage").create()), 0x0000000033607CCALL));
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 572 */
Variant c_coreparserfunctions::ti_defaultsort(const char* cls, CVarRef v_parser, Variant v_text) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::defaultsort);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  Variant eo_5;
  Variant v_old;

  (v_text = LINE(573,x_trim(toString(v_text))));
  if (equal(LINE(574,x_strlen(toString(v_text))), 0LL)) return "";
  (v_old = LINE(576,toObject(v_parser)->o_invoke_few_args("getCustomDefaultSort", 0x565D9E5A4801A09ELL, 0)));
  LINE(577,toObject(v_parser)->o_invoke_few_args("setDefaultSort", 0x0FE91E46B43AEF32LL, 1, v_text));
  if (same(v_old, false) || equal(v_old, v_text)) return "";
  else return (LINE(585,(assignCallTemp(eo_1, toString(LINE(584,(assignCallTemp(eo_4, ref(LINE(583,x_htmlspecialchars(toString(v_old))))),assignCallTemp(eo_5, ref(LINE(584,x_htmlspecialchars(toString(v_text))))),invoke_failed("wfmsg", Array(ArrayInit(3).set(0, "duplicate-defaultsort").set(1, eo_4).set(2, eo_5).create()), 0x0000000002E29FB3LL))))),concat3("<span class=\"error\">", eo_1, "</span>"))));
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 588 */
Variant c_coreparserfunctions::ti_filepath(const char* cls, CVarRef v_parser, Variant v_name //  = ""
, CVarRef v_option //  = ""
) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::filepath);
  Variant v_file;
  Variant v_url;

  (v_file = LINE(589,invoke_failed("wffindfile", Array(ArrayInit(1).set(0, ref(v_name)).create()), 0x000000008D7BE104LL)));
  if (toBoolean(v_file)) {
    (v_url = LINE(591,v_file.o_invoke_few_args("getFullUrl", 0x4D03E4B69713A33BLL, 0)));
    if (equal(v_option, "nowiki")) {
      return Array(ArrayInit(2).set(0, v_url).set(1, "nowiki", true, 0x78258C7EF69CF55DLL).create());
    }
    return v_url;
  }
  else {
    return "";
  }
  return null;
} /* function */
/* SRC: CoreParserFunctions.php line 604 */
Variant c_coreparserfunctions::ti_tagobj(const char* cls, CVarRef v_parser, Variant v_frame, Variant v_args) {
  STATIC_METHOD_INJECTION(CoreParserFunctions, CoreParserFunctions::tagObj);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant v_tagName;
  Variant v_inner;
  Variant v_stripList;
  Variant v_attributes;
  Variant v_arg;
  Variant v_bits;
  String v_name;
  Variant v_value;
  Variant v_m;
  Variant v_params;

  ;
  if (!(toBoolean(LINE(606,x_count(v_args))))) {
    return "";
  }
  (v_tagName = LINE(609,x_strtolower(x_trim(toString(v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, x_array_shift(ref(v_args))))))));
  if (toBoolean(LINE(611,x_count(v_args)))) {
    (v_inner = LINE(612,v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, x_array_shift(ref(v_args)))));
  }
  else {
    setNull(v_inner);
  }
  (v_stripList = LINE(617,toObject(v_parser)->o_invoke_few_args("getStripList", 0x151B6C9D84BBC1F5LL, 0)));
  if (!(LINE(618,x_in_array(v_tagName, v_stripList)))) {
    return LINE(621,(assignCallTemp(eo_1, toString(LINE(620,invoke_failed("wfmsg", Array(ArrayInit(2).set(0, "unknown_extension_tag").set(1, ref(v_tagName)).create()), 0x0000000002E29FB3LL)))),concat3("<span class=\"error\">", eo_1, "</span>")));
  }
  (v_attributes = ScalarArrays::sa_[0]);
  {
    LOOP_COUNTER(2);
    for (ArrayIterPtr iter4 = v_args.begin("coreparserfunctions"); !iter4->end(); iter4->next()) {
      LOOP_COUNTER_CHECK(2);
      v_arg = iter4->second();
      {
        (v_bits = LINE(626,v_arg.o_invoke_few_args("splitArg", 0x73E7C3304C0C5360LL, 0)));
        if (same(LINE(627,x_strval(v_bits.rvalAt("index", 0x440D5888C0FF3081LL))), "")) {
          (v_name = LINE(628,x_trim(toString(v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 2, v_bits.refvalAt("name", 0x0BCDB293DC3CBDDCLL), 4LL /* ppframe::STRIP_COMMENTS */)))));
          (v_value = LINE(629,x_trim(toString(v_frame.o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_bits.refvalAt("value", 0x69E7413AE0C88471LL))))));
          if (toBoolean(LINE(630,x_preg_match("/^(\?:[\"'](.+)[\"']|\"\"|'')$/s", toString(v_value), ref(v_m))))) {
            (v_value = isset(v_m, 1LL, 0x5BCA7C69B794F8CELL) ? ((Variant)(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL))) : ((Variant)("")));
          }
          v_attributes.set(v_name, (v_value));
        }
      }
    }
  }
  (v_params = (assignCallTemp(eo_0, v_tagName),assignCallTemp(eo_1, v_inner),assignCallTemp(eo_2, v_attributes),assignCallTemp(eo_3, LINE(641,concat3("</", toString(v_tagName), ">"))),Array(ArrayInit(4).set(0, "name", eo_0, 0x0BCDB293DC3CBDDCLL).set(1, "inner", eo_1, 0x4F20D07BB803C1FELL).set(2, "attributes", eo_2, 0x3E5975BA0FC37E03LL).set(3, "close", eo_3, 0x36B43082F052EC6BLL).create())));
  return LINE(643,toObject(v_parser)->o_invoke_few_args("extensionSubstitution", 0x7D371E851A4798E9LL, 2, v_params, v_frame));
} /* function */
Object co_coreparserfunctions(CArrRef params, bool init /* = true */) {
  return Object(p_coreparserfunctions(NEW(c_coreparserfunctions)())->dynCreate(params, init));
}
Variant pm_php$CoreParserFunctions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::CoreParserFunctions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$CoreParserFunctions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
