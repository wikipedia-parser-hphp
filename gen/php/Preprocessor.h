
#ifndef __GENERATED_php_Preprocessor_h__
#define __GENERATED_php_Preprocessor_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/Preprocessor.fw.h>

// Declarations
#include <cls/preprocessor.h>
#include <cls/ppframe.h>
#include <cls/ppnode.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$Preprocessor_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_Preprocessor_h__
