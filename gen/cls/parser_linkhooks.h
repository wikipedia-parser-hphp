
#ifndef __GENERATED_cls_parser_linkhooks_h__
#define __GENERATED_cls_parser_linkhooks_h__

#include <cls/parser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser_LinkHooks.php line 6 */
class c_parser_linkhooks : virtual public c_parser {
  BEGIN_CLASS_MAP(parser_linkhooks)
    PARENT_CLASS(parser)
  END_CLASS_MAP(parser_linkhooks)
  DECLARE_CLASS(parser_linkhooks, Parser_LinkHooks, parser)
  void init();
  public: Variant m_mLinkHooks;
  public: void t___construct(Variant v_conf = ScalarArrays::sa_[0]);
  public: ObjectData *create(Variant v_conf = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_firstcallinit();
  public: Variant t_setlinkhook(CVarRef v_ns, CArrRef v_callback, int64 v_flags = 0LL);
  public: Variant t_getlinkhooks();
  public: p_linkholderarray t_replaceinternallinks2(Variant v_s);
  public: Variant t_replaceinternallinkscallback(CVarRef v_parser, CVarRef v_holders, CVarRef v_markers, Variant v_titleText, Variant v_paramText);
  public: static String t_getrandomstring() { return ti_getrandomstring("parser_linkhooks"); }
  public: static Variant t_replaceunusualescapescallback(CVarRef v_matches) { return ti_replaceunusualescapescallback("parser_linkhooks", v_matches); }
  public: static Array t_statelessfetchtemplate(CVarRef v_title, CVarRef v_parser = false) { return ti_statelessfetchtemplate("parser_linkhooks", v_title, v_parser); }
  public: static void t_incrementnumbering(CVarRef v_number, CVarRef v_level, CVarRef v_lastLevel) { ti_incrementnumbering("parser_linkhooks", v_number, v_level, v_lastLevel); }
  public: static Variant t_replaceunusualescapes(CVarRef v_url) { return ti_replaceunusualescapes("parser_linkhooks", v_url); }
  public: static Array t_mergesectiontrees(CArrRef v_tree1, CArrRef v_tree2, CVarRef v_section, Object v_title, CVarRef v_len2) { return ti_mergesectiontrees("parser_linkhooks", v_tree1, v_tree2, v_section, v_title, v_len2); }
  public: static Array t_splitwhitespace(CVarRef v_s) { return ti_splitwhitespace("parser_linkhooks", v_s); }
  public: static Variant t_createassocargs(CArrRef v_args) { return ti_createassocargs("parser_linkhooks", v_args); }
  public: static Variant t_tidy(CVarRef v_text) { return ti_tidy("parser_linkhooks", v_text); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_parser_linkhooks_h__
