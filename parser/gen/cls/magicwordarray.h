
#ifndef __GENERATED_cls_magicwordarray_h__
#define __GENERATED_cls_magicwordarray_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 25148 */
class c_magicwordarray : virtual public ObjectData {
  BEGIN_CLASS_MAP(magicwordarray)
  END_CLASS_MAP(magicwordarray)
  DECLARE_CLASS(magicwordarray, MagicWordArray, ObjectData)
  void init();
  public: Variant m_names;
  public: Variant m_hash;
  public: Variant m_baseRegex;
  public: Variant m_regex;
  public: Variant m_matches;
  public: void t___construct(Variant v_names = ScalarArrays::sa_[0]);
  public: ObjectData *create(Variant v_names = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_add(CVarRef v_name);
  public: void t_addarray(CVarRef v_names);
  public: Variant t_gethash();
  public: Variant t_getbaseregex();
  public: Variant t_getregex();
  public: Variant t_getvariableregex();
  public: Variant t_getvariablestarttoendregex();
  public: Array t_parsematch(Variant v_m);
  public: Array t_matchvariablestarttoend(CVarRef v_text);
  public: Variant t_matchstarttoend(Variant v_text);
  public: Variant t_matchandremove(Variant v_text);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_magicwordarray_h__
