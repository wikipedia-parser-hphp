
#ifndef __GENERATED_cls_titlearray_h__
#define __GENERATED_cls_titlearray_h__

#include <cls/citerator.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 25585 */
class c_titlearray : virtual public c_citerator {
  BEGIN_CLASS_MAP(titlearray)
    PARENT_CLASS(ctraversable)
    PARENT_CLASS(citerator)
  END_CLASS_MAP(titlearray)
  DECLARE_CLASS(titlearray, TitleArray, citerator)
  void init();
  public: static Variant ti_newfromresult(const char* cls, CVarRef v_res);
  public: static p_titlearrayfromresult ti_newfromresult_internal(const char* cls, CVarRef v_res);
  public: static p_titlearrayfromresult t_newfromresult_internal(CVarRef v_res) { return ti_newfromresult_internal("titlearray", v_res); }
  public: static Variant t_newfromresult(CVarRef v_res) { return ti_newfromresult("titlearray", v_res); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_titlearray_h__
