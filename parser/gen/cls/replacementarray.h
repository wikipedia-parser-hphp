
#ifndef __GENERATED_cls_replacementarray_h__
#define __GENERATED_cls_replacementarray_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 40251 */
class c_replacementarray : virtual public ObjectData {
  BEGIN_CLASS_MAP(replacementarray)
  END_CLASS_MAP(replacementarray)
  DECLARE_CLASS(replacementarray, ReplacementArray, ObjectData)
  void init();
  public: Variant m_data;
  public: Variant m_fss;
  public: void t___construct(Variant v_data = ScalarArrays::sa_[0]);
  public: ObjectData *create(Variant v_data = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t___sleep();
  public: Variant t___wakeup();
  public: void t_setarray(CVarRef v_data);
  public: Variant t_getarray();
  public: void t_setpair(CVarRef v_from, CVarRef v_to);
  public: void t_mergearray(CVarRef v_data);
  public: void t_merge(CVarRef v_other);
  public: void t_removepair(Primitive v_from);
  public: void t_removearray(CArrRef v_data);
  public: String t_replace(CVarRef v_subject);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_replacementarray_h__
