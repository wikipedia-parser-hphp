
#ifndef __GENERATED_cls_recentchange_h__
#define __GENERATED_cls_recentchange_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 19457 */
class c_recentchange : virtual public ObjectData {
  BEGIN_CLASS_MAP(recentchange)
  END_CLASS_MAP(recentchange)
  DECLARE_CLASS(recentchange, RecentChange, ObjectData)
  void init();
  public: Variant m_mAttribs;
  public: Variant m_mExtra;
  public: Variant m_mTitle;
  public: Variant m_mMovedToTitle;
  public: Variant m_numberofWatchingusers;
  public: static p_recentchange ti_newfromrow(const char* cls, CArrRef v_row);
  public: static p_recentchange ti_newfromcurrow(const char* cls, CVarRef v_row);
  public: static Variant ti_newfromid(const char* cls, CVarRef v_rcid);
  public: static Variant ti_newfromconds(const char* cls, CVarRef v_conds, Variant v_fname = false);
  public: void t_setattribs(CVarRef v_attribs);
  public: void t_setextra(CVarRef v_extra);
  public: Variant t_gettitle();
  public: Variant t_getmovedtotitle();
  public: void t_save();
  public: void t_notifyrc2udp();
  public: static bool ti_sendtoudp(const char* cls, Variant v_line, Variant v_address = "", Variant v_prefix = "");
  public: static Variant ti_cleanupforirc(const char* cls, CVarRef v_text);
  public: static Variant ti_markpatrolled(const char* cls, Variant v_change, Variant v_auto = false);
  public: Variant t_domarkpatrolled(CVarRef v_auto = false);
  public: Variant t_reallymarkpatrolled();
  public: static p_recentchange ti_notifyedit(const char* cls, CVarRef v_timestamp, Variant v_title, bool v_minor, Variant v_user, CVarRef v_comment, CVarRef v_oldId, CVarRef v_lastTimestamp, Primitive v_bot, Variant v_ip = "", int64 v_oldSize = 0LL, int64 v_newSize = 0LL, CVarRef v_newId = 0LL, CVarRef v_patrol = 0LL);
  public: static p_recentchange ti_notifynew(const char* cls, CVarRef v_timestamp, Variant v_title, bool v_minor, Variant v_user, CVarRef v_comment, Primitive v_bot, Variant v_ip = "", int64 v_size = 0LL, CVarRef v_newId = 0LL, CVarRef v_patrol = 0LL);
  public: static void ti_notifymove(const char* cls, CVarRef v_timestamp, Variant v_oldTitle, Variant v_newTitle, Variant v_user, CVarRef v_comment, Variant v_ip = "", bool v_overRedir = false);
  public: static void ti_notifymovetonew(const char* cls, CVarRef v_timestamp, Variant v_oldTitle, Variant v_newTitle, Variant v_user, CVarRef v_comment, CStrRef v_ip = "");
  public: static void ti_notifymoveoverredirect(const char* cls, CVarRef v_timestamp, Variant v_oldTitle, Variant v_newTitle, Variant v_user, CVarRef v_comment, CStrRef v_ip = "");
  public: static bool ti_notifylog(const char* cls, CVarRef v_timestamp, Variant v_title, Variant v_user, CVarRef v_actionComment, CStrRef v_ip = "", CVarRef v_type = null_variant, CVarRef v_action = null_variant, CVarRef v_target = null_variant, CVarRef v_logComment = null_variant, CVarRef v_params = null_variant, int64 v_newId = 0LL);
  public: static p_recentchange ti_newlogentry(const char* cls, CVarRef v_timestamp, Variant v_title, Variant v_user, CVarRef v_actionComment, Variant v_ip = "", CVarRef v_type = null_variant, CVarRef v_action = null_variant, CVarRef v_target = null_variant, CVarRef v_logComment = null_variant, CVarRef v_params = null_variant, int64 v_newId = 0LL);
  public: void t_loadfromrow(CVarRef v_row);
  public: void t_loadfromcurrow(CVarRef v_row);
  public: Variant t_getattribute(CVarRef v_name);
  public: String t_difflinktrail(CVarRef v_forceCur);
  public: Variant t_getircline();
  public: Variant t_getcharacterdifference(Variant v_old = 0LL, Variant v_new = 0LL);
  public: static Variant t_markpatrolled(CVarRef v_change, CVarRef v_auto = false) { return ti_markpatrolled("recentchange", v_change, v_auto); }
  public: static Variant t_newfromid(CVarRef v_rcid) { return ti_newfromid("recentchange", v_rcid); }
  public: static Variant t_cleanupforirc(CVarRef v_text) { return ti_cleanupforirc("recentchange", v_text); }
  public: static p_recentchange t_newlogentry(CVarRef v_timestamp, CVarRef v_title, CVarRef v_user, CVarRef v_actionComment, CVarRef v_ip = "", CVarRef v_type = null_variant, CVarRef v_action = null_variant, CVarRef v_target = null_variant, CVarRef v_logComment = null_variant, CVarRef v_params = null_variant, int64 v_newId = 0LL) { return ti_newlogentry("recentchange", v_timestamp, v_title, v_user, v_actionComment, v_ip, v_type, v_action, v_target, v_logComment, v_params, v_newId); }
  public: static void t_notifymovetonew(CVarRef v_timestamp, CVarRef v_oldTitle, CVarRef v_newTitle, CVarRef v_user, CVarRef v_comment, CStrRef v_ip = "") { ti_notifymovetonew("recentchange", v_timestamp, v_oldTitle, v_newTitle, v_user, v_comment, v_ip); }
  public: static bool t_sendtoudp(CVarRef v_line, CVarRef v_address = "", CVarRef v_prefix = "") { return ti_sendtoudp("recentchange", v_line, v_address, v_prefix); }
  public: static bool t_notifylog(CVarRef v_timestamp, CVarRef v_title, CVarRef v_user, CVarRef v_actionComment, CStrRef v_ip = "", CVarRef v_type = null_variant, CVarRef v_action = null_variant, CVarRef v_target = null_variant, CVarRef v_logComment = null_variant, CVarRef v_params = null_variant, int64 v_newId = 0LL) { return ti_notifylog("recentchange", v_timestamp, v_title, v_user, v_actionComment, v_ip, v_type, v_action, v_target, v_logComment, v_params, v_newId); }
  public: static Variant t_newfromconds(CVarRef v_conds, CVarRef v_fname = false) { return ti_newfromconds("recentchange", v_conds, v_fname); }
  public: static p_recentchange t_notifynew(CVarRef v_timestamp, CVarRef v_title, bool v_minor, CVarRef v_user, CVarRef v_comment, Primitive v_bot, CVarRef v_ip = "", int64 v_size = 0LL, CVarRef v_newId = 0LL, CVarRef v_patrol = 0LL) { return ti_notifynew("recentchange", v_timestamp, v_title, v_minor, v_user, v_comment, v_bot, v_ip, v_size, v_newId, v_patrol); }
  public: static p_recentchange t_newfromcurrow(CVarRef v_row) { return ti_newfromcurrow("recentchange", v_row); }
  public: static void t_notifymove(CVarRef v_timestamp, CVarRef v_oldTitle, CVarRef v_newTitle, CVarRef v_user, CVarRef v_comment, CVarRef v_ip = "", bool v_overRedir = false) { ti_notifymove("recentchange", v_timestamp, v_oldTitle, v_newTitle, v_user, v_comment, v_ip, v_overRedir); }
  public: static void t_notifymoveoverredirect(CVarRef v_timestamp, CVarRef v_oldTitle, CVarRef v_newTitle, CVarRef v_user, CVarRef v_comment, CStrRef v_ip = "") { ti_notifymoveoverredirect("recentchange", v_timestamp, v_oldTitle, v_newTitle, v_user, v_comment, v_ip); }
  public: static p_recentchange t_newfromrow(CArrRef v_row) { return ti_newfromrow("recentchange", v_row); }
  public: static p_recentchange t_notifyedit(CVarRef v_timestamp, CVarRef v_title, bool v_minor, CVarRef v_user, CVarRef v_comment, CVarRef v_oldId, CVarRef v_lastTimestamp, Primitive v_bot, CVarRef v_ip = "", int64 v_oldSize = 0LL, int64 v_newSize = 0LL, CVarRef v_newId = 0LL, CVarRef v_patrol = 0LL) { return ti_notifyedit("recentchange", v_timestamp, v_title, v_minor, v_user, v_comment, v_oldId, v_lastTimestamp, v_bot, v_ip, v_oldSize, v_newSize, v_newId, v_patrol); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_recentchange_h__
