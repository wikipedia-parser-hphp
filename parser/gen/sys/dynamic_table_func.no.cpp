
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// Function Invoke Proxies
Variant i_efgetprivateandfishbowlwikis(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wfnodeletemainpage(CArrRef params);
Variant i_wfdebug(CArrRef params);
static Variant invoke_case_5(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x159D6E82B3105805LL, wfnodeletemainpage);
  HASH_INVOKE(0x6C158C49E441E905LL, wfdebug);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_wfformatstackframe(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wfparserteststaticparserhookhook(CArrRef params);
Variant i_wfescapewikitext(CArrRef params);
static Variant invoke_case_9(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x69E6A2B1F84B6C09LL, wfparserteststaticparserhookhook);
  HASH_INVOKE(0x134707E3B3382909LL, wfescapewikitext);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_istainted(CArrRef params);
Variant i__test_ends(CArrRef params);
Variant i_logbadpassword(CArrRef params);
static Variant invoke_case_15(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x016A142A22C8554FLL, istainted);
  HASH_INVOKE(0x43576D51E932B44FLL, _test_ends);
  HASH_INVOKE(0x395BCCD2DE614BCFLL, logbadpassword);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_is(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wfparsertestparserhookhook(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wfparsertestparserhooksetup(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_logprefsemail(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_ok(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wfgetcachednotice(CArrRef params);
Variant i_logprefspassword(CArrRef params);
static Variant invoke_case_34(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x4F1D6F9616C25DA2LL, wfgetcachednotice);
  HASH_INVOKE(0x4EF74B11D15C4822LL, logprefspassword);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_wfarraydiff2_cmp(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wfislocallyblockedproxy(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wfexceptionhandler(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wfparsertimesetup(CArrRef params);
Variant i_fail(CArrRef params);
static Variant invoke_case_42(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x6F6CB5A6AA463B6ALL, wfparsertimesetup);
  HASH_INVOKE(0x731DBC30C2876EAALL, fail);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_wfprofilein(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wmfcentralauthwikilist(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wfgetsitenotice(const char *s, CArrRef params, int64 hash, bool fatal);
Variant i_wfparserteststaticparserhooksetup(CArrRef params);
Variant i_wflogxff(CArrRef params);
static Variant invoke_case_62(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_INVOKE(0x6F7251EC941CF73ELL, wfparserteststaticparserhooksetup);
  HASH_INVOKE(0x7127F595EEC1B47ELL, wflogxff);
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_wfgetnamespacenotice(const char *s, CArrRef params, int64 hash, bool fatal);

// Function Invoke Table
static Variant (*funcTable[64])(const char *, CArrRef, int64, bool);
static class FuncTableInitializer {
  public: FuncTableInitializer() {
    for (int i = 0; i < 64; i++) funcTable[i] = &invoke_builtin;
    funcTable[2] = &i_efgetprivateandfishbowlwikis;
    funcTable[5] = &invoke_case_5;
    funcTable[7] = &i_wfformatstackframe;
    funcTable[9] = &invoke_case_9;
    funcTable[15] = &invoke_case_15;
    funcTable[18] = &i_is;
    funcTable[23] = &i_wfparsertestparserhookhook;
    funcTable[25] = &i_wfparsertestparserhooksetup;
    funcTable[28] = &i_logprefsemail;
    funcTable[30] = &i_ok;
    funcTable[34] = &invoke_case_34;
    funcTable[35] = &i_wfarraydiff2_cmp;
    funcTable[40] = &i_wfislocallyblockedproxy;
    funcTable[41] = &i_wfexceptionhandler;
    funcTable[42] = &invoke_case_42;
    funcTable[47] = &i_wfprofilein;
    funcTable[49] = &i_wmfcentralauthwikilist;
    funcTable[56] = &i_wfgetsitenotice;
    funcTable[62] = &invoke_case_62;
    funcTable[63] = &i_wfgetnamespacenotice;
  }
} func_table_initializer;
Variant invoke(const char *s, CArrRef params, int64 hash, bool tryInterp /* = true */, bool fatal /* = true */) {
  if (hash < 0) hash = hash_string_i(s);
  return funcTable[hash & 63](s, params, hash, fatal);
}
Variant Eval::invoke_from_eval(const char *s, Eval::VariableEnvironment &env, const Eval::FunctionCallExpression *caller, int64 hash, bool fatal) {
  return invoke_from_eval_builtin(s, env, caller, hash, fatal);
}

///////////////////////////////////////////////////////////////////////////////
}
