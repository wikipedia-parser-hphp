
#ifndef __GENERATED_php_ParserCache_h__
#define __GENERATED_php_ParserCache_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/ParserCache.fw.h>

// Declarations
#include <cls/parsercache.h>
#include <php/ParserOptions.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$ParserCache_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_parsercache(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_ParserCache_h__
