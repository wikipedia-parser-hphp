
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_parsercache(CArrRef params, bool init = true);
Variant cw_parsercache$os_get(const char *s);
Variant &cw_parsercache$os_lval(const char *s);
Variant cw_parsercache$os_constant(const char *s);
Variant cw_parsercache$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_stripstate(CArrRef params, bool init = true);
Variant cw_stripstate$os_get(const char *s);
Variant &cw_stripstate$os_lval(const char *s);
Variant cw_stripstate$os_constant(const char *s);
Variant cw_stripstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parser_difftest(CArrRef params, bool init = true);
Variant cw_parser_difftest$os_get(const char *s);
Variant &cw_parser_difftest$os_lval(const char *s);
Variant cw_parser_difftest$os_constant(const char *s);
Variant cw_parser_difftest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppdstack_hash(CArrRef params, bool init = true);
Variant cw_ppdstack_hash$os_get(const char *s);
Variant &cw_ppdstack_hash$os_lval(const char *s);
Variant cw_ppdstack_hash$os_constant(const char *s);
Variant cw_ppdstack_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppnode_hash_text(CArrRef params, bool init = true);
Variant cw_ppnode_hash_text$os_get(const char *s);
Variant &cw_ppnode_hash_text$os_lval(const char *s);
Variant cw_ppnode_hash_text$os_constant(const char *s);
Variant cw_ppnode_hash_text$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_mwtidy(CArrRef params, bool init = true);
Variant cw_mwtidy$os_get(const char *s);
Variant &cw_mwtidy$os_lval(const char *s);
Variant cw_mwtidy$os_constant(const char *s);
Variant cw_mwtidy$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppdaccum_hash(CArrRef params, bool init = true);
Variant cw_ppdaccum_hash$os_get(const char *s);
Variant &cw_ppdaccum_hash$os_lval(const char *s);
Variant cw_ppdaccum_hash$os_constant(const char *s);
Variant cw_ppdaccum_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parseroptions(CArrRef params, bool init = true);
Variant cw_parseroptions$os_get(const char *s);
Variant &cw_parseroptions$os_lval(const char *s);
Variant cw_parseroptions$os_constant(const char *s);
Variant cw_parseroptions$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppnode_hash_tree(CArrRef params, bool init = true);
Variant cw_ppnode_hash_tree$os_get(const char *s);
Variant &cw_ppnode_hash_tree$os_lval(const char *s);
Variant cw_ppnode_hash_tree$os_constant(const char *s);
Variant cw_ppnode_hash_tree$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppframe_hash(CArrRef params, bool init = true);
Variant cw_ppframe_hash$os_get(const char *s);
Variant &cw_ppframe_hash$os_lval(const char *s);
Variant cw_ppframe_hash$os_constant(const char *s);
Variant cw_ppframe_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppdpart(CArrRef params, bool init = true);
Variant cw_ppdpart$os_get(const char *s);
Variant &cw_ppdpart$os_lval(const char *s);
Variant cw_ppdpart$os_constant(const char *s);
Variant cw_ppdpart$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppdstackelement(CArrRef params, bool init = true);
Variant cw_ppdstackelement$os_get(const char *s);
Variant &cw_ppdstackelement$os_lval(const char *s);
Variant cw_ppdstackelement$os_constant(const char *s);
Variant cw_ppdstackelement$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_linkholderarray(CArrRef params, bool init = true);
Variant cw_linkholderarray$os_get(const char *s);
Variant &cw_linkholderarray$os_lval(const char *s);
Variant cw_linkholderarray$os_constant(const char *s);
Variant cw_linkholderarray$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_preprocessor_hash(CArrRef params, bool init = true);
Variant cw_preprocessor_hash$os_get(const char *s);
Variant &cw_preprocessor_hash$os_lval(const char *s);
Variant cw_preprocessor_hash$os_constant(const char *s);
Variant cw_preprocessor_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppnode_dom(CArrRef params, bool init = true);
Variant cw_ppnode_dom$os_get(const char *s);
Variant &cw_ppnode_dom$os_lval(const char *s);
Variant cw_ppnode_dom$os_constant(const char *s);
Variant cw_ppnode_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppdpart_hash(CArrRef params, bool init = true);
Variant cw_ppdpart_hash$os_get(const char *s);
Variant &cw_ppdpart_hash$os_lval(const char *s);
Variant cw_ppdpart_hash$os_constant(const char *s);
Variant cw_ppdpart_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parser(CArrRef params, bool init = true);
Variant cw_parser$os_get(const char *s);
Variant &cw_parser$os_lval(const char *s);
Variant cw_parser$os_constant(const char *s);
Variant cw_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppdstack(CArrRef params, bool init = true);
Variant cw_ppdstack$os_get(const char *s);
Variant &cw_ppdstack$os_lval(const char *s);
Variant cw_ppdstack$os_constant(const char *s);
Variant cw_ppdstack$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_preprocessor_dom(CArrRef params, bool init = true);
Variant cw_preprocessor_dom$os_get(const char *s);
Variant &cw_preprocessor_dom$os_lval(const char *s);
Variant cw_preprocessor_dom$os_constant(const char *s);
Variant cw_preprocessor_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parseroutput(CArrRef params, bool init = true);
Variant cw_parseroutput$os_get(const char *s);
Variant &cw_parseroutput$os_lval(const char *s);
Variant cw_parseroutput$os_constant(const char *s);
Variant cw_parseroutput$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppcustomframe_hash(CArrRef params, bool init = true);
Variant cw_ppcustomframe_hash$os_get(const char *s);
Variant &cw_ppcustomframe_hash$os_lval(const char *s);
Variant cw_ppcustomframe_hash$os_constant(const char *s);
Variant cw_ppcustomframe_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppframe_dom(CArrRef params, bool init = true);
Variant cw_ppframe_dom$os_get(const char *s);
Variant &cw_ppframe_dom$os_lval(const char *s);
Variant cw_ppframe_dom$os_constant(const char *s);
Variant cw_ppframe_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppcustomframe_dom(CArrRef params, bool init = true);
Variant cw_ppcustomframe_dom$os_get(const char *s);
Variant &cw_ppcustomframe_dom$os_lval(const char *s);
Variant cw_ppcustomframe_dom$os_constant(const char *s);
Variant cw_ppcustomframe_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppnode_hash_attr(CArrRef params, bool init = true);
Variant cw_ppnode_hash_attr$os_get(const char *s);
Variant &cw_ppnode_hash_attr$os_lval(const char *s);
Variant cw_ppnode_hash_attr$os_constant(const char *s);
Variant cw_ppnode_hash_attr$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_pptemplateframe_dom(CArrRef params, bool init = true);
Variant cw_pptemplateframe_dom$os_get(const char *s);
Variant &cw_pptemplateframe_dom$os_lval(const char *s);
Variant cw_pptemplateframe_dom$os_constant(const char *s);
Variant cw_pptemplateframe_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_onlyincludereplacer(CArrRef params, bool init = true);
Variant cw_onlyincludereplacer$os_get(const char *s);
Variant &cw_onlyincludereplacer$os_lval(const char *s);
Variant cw_onlyincludereplacer$os_constant(const char *s);
Variant cw_onlyincludereplacer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_pptemplateframe_hash(CArrRef params, bool init = true);
Variant cw_pptemplateframe_hash$os_get(const char *s);
Variant &cw_pptemplateframe_hash$os_lval(const char *s);
Variant cw_pptemplateframe_hash$os_constant(const char *s);
Variant cw_pptemplateframe_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_coreparserfunctions(CArrRef params, bool init = true);
Variant cw_coreparserfunctions$os_get(const char *s);
Variant &cw_coreparserfunctions$os_lval(const char *s);
Variant cw_coreparserfunctions$os_constant(const char *s);
Variant cw_coreparserfunctions$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_corelinkfunctions(CArrRef params, bool init = true);
Variant cw_corelinkfunctions$os_get(const char *s);
Variant &cw_corelinkfunctions$os_lval(const char *s);
Variant cw_corelinkfunctions$os_constant(const char *s);
Variant cw_corelinkfunctions$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_dateformatter(CArrRef params, bool init = true);
Variant cw_dateformatter$os_get(const char *s);
Variant &cw_dateformatter$os_lval(const char *s);
Variant cw_dateformatter$os_constant(const char *s);
Variant cw_dateformatter$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parser_linkhooks(CArrRef params, bool init = true);
Variant cw_parser_linkhooks$os_get(const char *s);
Variant &cw_parser_linkhooks$os_lval(const char *s);
Variant cw_parser_linkhooks$os_constant(const char *s);
Variant cw_parser_linkhooks$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_linkmarkerreplacer(CArrRef params, bool init = true);
Variant cw_linkmarkerreplacer$os_get(const char *s);
Variant &cw_linkmarkerreplacer$os_lval(const char *s);
Variant cw_linkmarkerreplacer$os_constant(const char *s);
Variant cw_linkmarkerreplacer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppnode_hash_array(CArrRef params, bool init = true);
Variant cw_ppnode_hash_array$os_get(const char *s);
Variant &cw_ppnode_hash_array$os_lval(const char *s);
Variant cw_ppnode_hash_array$os_constant(const char *s);
Variant cw_ppnode_hash_array$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppdstackelement_hash(CArrRef params, bool init = true);
Variant cw_ppdstackelement_hash$os_get(const char *s);
Variant &cw_ppdstackelement_hash$os_lval(const char *s);
Variant cw_ppdstackelement_hash$os_constant(const char *s);
Variant cw_ppdstackelement_hash$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 127) {
    case 8:
      HASH_CREATE_OBJECT(0x4B1D337C630F8C88LL, ppdstack_hash);
      break;
    case 10:
      HASH_CREATE_OBJECT(0x5FAE2838195DC60ALL, parser_difftest);
      break;
    case 14:
      HASH_CREATE_OBJECT(0x696D931EC4A9598ELL, dateformatter);
      break;
    case 15:
      HASH_CREATE_OBJECT(0x465F5F4F142EDE8FLL, parser);
      break;
    case 16:
      HASH_CREATE_OBJECT(0x0301D2F46DDC7F90LL, linkholderarray);
      HASH_CREATE_OBJECT(0x20107B32F3A2D110LL, ppnode_hash_array);
      break;
    case 19:
      HASH_CREATE_OBJECT(0x7C178B2BFCA03693LL, parseroptions);
      HASH_CREATE_OBJECT(0x53917CE42D229113LL, ppdstackelement_hash);
      break;
    case 20:
      HASH_CREATE_OBJECT(0x54A5D6532527C714LL, mwtidy);
      break;
    case 26:
      HASH_CREATE_OBJECT(0x1976C8E9CAE4D79ALL, preprocessor_hash);
      break;
    case 28:
      HASH_CREATE_OBJECT(0x0E385D3C4653369CLL, pptemplateframe_hash);
      break;
    case 40:
      HASH_CREATE_OBJECT(0x292F71DA174B8FA8LL, coreparserfunctions);
      break;
    case 42:
      HASH_CREATE_OBJECT(0x10456EC31B4BFB2ALL, ppnode_hash_text);
      break;
    case 45:
      HASH_CREATE_OBJECT(0x22E1A15A5C2B5B2DLL, corelinkfunctions);
      break;
    case 51:
      HASH_CREATE_OBJECT(0x7C241D3F299577B3LL, ppnode_hash_attr);
      break;
    case 54:
      HASH_CREATE_OBJECT(0x3B5D5B251E2AC936LL, stripstate);
      HASH_CREATE_OBJECT(0x108F82A2A8707EB6LL, ppdaccum_hash);
      HASH_CREATE_OBJECT(0x1FB0C5A1E83638B6LL, pptemplateframe_dom);
      break;
    case 65:
      HASH_CREATE_OBJECT(0x758B1E2426B4FCC1LL, onlyincludereplacer);
      break;
    case 67:
      HASH_CREATE_OBJECT(0x5A549ACF8D18C043LL, ppframe_hash);
      break;
    case 68:
      HASH_CREATE_OBJECT(0x6143CF5F8E106F44LL, preprocessor_dom);
      break;
    case 69:
      HASH_CREATE_OBJECT(0x7CBBE2907466E7C5LL, ppcustomframe_dom);
      break;
    case 77:
      HASH_CREATE_OBJECT(0x27CB8FF224A1304DLL, ppcustomframe_hash);
      break;
    case 78:
      HASH_CREATE_OBJECT(0x64CA426643E9574ELL, ppnode_hash_tree);
      break;
    case 82:
      HASH_CREATE_OBJECT(0x6566F74128FE72D2LL, ppdstackelement);
      break;
    case 83:
      HASH_CREATE_OBJECT(0x7405ED841E3C0B53LL, parsercache);
      break;
    case 89:
      HASH_CREATE_OBJECT(0x41F18C209AE6D959LL, ppframe_dom);
      break;
    case 94:
      HASH_CREATE_OBJECT(0x1FFE0211B5F57DDELL, parseroutput);
      break;
    case 99:
      HASH_CREATE_OBJECT(0x025B7C56B69451E3LL, ppdstack);
      break;
    case 101:
      HASH_CREATE_OBJECT(0x4C6DB3EB60853BE5LL, linkmarkerreplacer);
      break;
    case 102:
      HASH_CREATE_OBJECT(0x4547ADA8DCEEF266LL, ppnode_dom);
      break;
    case 105:
      HASH_CREATE_OBJECT(0x7B4A05176D718BE9LL, ppdpart_hash);
      break;
    case 108:
      HASH_CREATE_OBJECT(0x4FBEA60C53B842ECLL, parser_linkhooks);
      break;
    case 109:
      HASH_CREATE_OBJECT(0x7C7CEDA62E334FEDLL, ppdpart);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 127) {
    case 8:
      HASH_INVOKE_STATIC_METHOD(0x4B1D337C630F8C88LL, ppdstack_hash);
      break;
    case 10:
      HASH_INVOKE_STATIC_METHOD(0x5FAE2838195DC60ALL, parser_difftest);
      break;
    case 14:
      HASH_INVOKE_STATIC_METHOD(0x696D931EC4A9598ELL, dateformatter);
      break;
    case 15:
      HASH_INVOKE_STATIC_METHOD(0x465F5F4F142EDE8FLL, parser);
      break;
    case 16:
      HASH_INVOKE_STATIC_METHOD(0x0301D2F46DDC7F90LL, linkholderarray);
      HASH_INVOKE_STATIC_METHOD(0x20107B32F3A2D110LL, ppnode_hash_array);
      break;
    case 19:
      HASH_INVOKE_STATIC_METHOD(0x7C178B2BFCA03693LL, parseroptions);
      HASH_INVOKE_STATIC_METHOD(0x53917CE42D229113LL, ppdstackelement_hash);
      break;
    case 20:
      HASH_INVOKE_STATIC_METHOD(0x54A5D6532527C714LL, mwtidy);
      break;
    case 26:
      HASH_INVOKE_STATIC_METHOD(0x1976C8E9CAE4D79ALL, preprocessor_hash);
      break;
    case 28:
      HASH_INVOKE_STATIC_METHOD(0x0E385D3C4653369CLL, pptemplateframe_hash);
      break;
    case 40:
      HASH_INVOKE_STATIC_METHOD(0x292F71DA174B8FA8LL, coreparserfunctions);
      break;
    case 42:
      HASH_INVOKE_STATIC_METHOD(0x10456EC31B4BFB2ALL, ppnode_hash_text);
      break;
    case 45:
      HASH_INVOKE_STATIC_METHOD(0x22E1A15A5C2B5B2DLL, corelinkfunctions);
      break;
    case 51:
      HASH_INVOKE_STATIC_METHOD(0x7C241D3F299577B3LL, ppnode_hash_attr);
      break;
    case 54:
      HASH_INVOKE_STATIC_METHOD(0x3B5D5B251E2AC936LL, stripstate);
      HASH_INVOKE_STATIC_METHOD(0x108F82A2A8707EB6LL, ppdaccum_hash);
      HASH_INVOKE_STATIC_METHOD(0x1FB0C5A1E83638B6LL, pptemplateframe_dom);
      break;
    case 65:
      HASH_INVOKE_STATIC_METHOD(0x758B1E2426B4FCC1LL, onlyincludereplacer);
      break;
    case 67:
      HASH_INVOKE_STATIC_METHOD(0x5A549ACF8D18C043LL, ppframe_hash);
      break;
    case 68:
      HASH_INVOKE_STATIC_METHOD(0x6143CF5F8E106F44LL, preprocessor_dom);
      break;
    case 69:
      HASH_INVOKE_STATIC_METHOD(0x7CBBE2907466E7C5LL, ppcustomframe_dom);
      break;
    case 77:
      HASH_INVOKE_STATIC_METHOD(0x27CB8FF224A1304DLL, ppcustomframe_hash);
      break;
    case 78:
      HASH_INVOKE_STATIC_METHOD(0x64CA426643E9574ELL, ppnode_hash_tree);
      break;
    case 82:
      HASH_INVOKE_STATIC_METHOD(0x6566F74128FE72D2LL, ppdstackelement);
      break;
    case 83:
      HASH_INVOKE_STATIC_METHOD(0x7405ED841E3C0B53LL, parsercache);
      break;
    case 89:
      HASH_INVOKE_STATIC_METHOD(0x41F18C209AE6D959LL, ppframe_dom);
      break;
    case 94:
      HASH_INVOKE_STATIC_METHOD(0x1FFE0211B5F57DDELL, parseroutput);
      break;
    case 99:
      HASH_INVOKE_STATIC_METHOD(0x025B7C56B69451E3LL, ppdstack);
      break;
    case 101:
      HASH_INVOKE_STATIC_METHOD(0x4C6DB3EB60853BE5LL, linkmarkerreplacer);
      break;
    case 102:
      HASH_INVOKE_STATIC_METHOD(0x4547ADA8DCEEF266LL, ppnode_dom);
      break;
    case 105:
      HASH_INVOKE_STATIC_METHOD(0x7B4A05176D718BE9LL, ppdpart_hash);
      break;
    case 108:
      HASH_INVOKE_STATIC_METHOD(0x4FBEA60C53B842ECLL, parser_linkhooks);
      break;
    case 109:
      HASH_INVOKE_STATIC_METHOD(0x7C7CEDA62E334FEDLL, ppdpart);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 127) {
    case 8:
      HASH_GET_STATIC_PROPERTY(0x4B1D337C630F8C88LL, ppdstack_hash);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY(0x5FAE2838195DC60ALL, parser_difftest);
      break;
    case 14:
      HASH_GET_STATIC_PROPERTY(0x696D931EC4A9598ELL, dateformatter);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY(0x465F5F4F142EDE8FLL, parser);
      break;
    case 16:
      HASH_GET_STATIC_PROPERTY(0x0301D2F46DDC7F90LL, linkholderarray);
      HASH_GET_STATIC_PROPERTY(0x20107B32F3A2D110LL, ppnode_hash_array);
      break;
    case 19:
      HASH_GET_STATIC_PROPERTY(0x7C178B2BFCA03693LL, parseroptions);
      HASH_GET_STATIC_PROPERTY(0x53917CE42D229113LL, ppdstackelement_hash);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY(0x54A5D6532527C714LL, mwtidy);
      break;
    case 26:
      HASH_GET_STATIC_PROPERTY(0x1976C8E9CAE4D79ALL, preprocessor_hash);
      break;
    case 28:
      HASH_GET_STATIC_PROPERTY(0x0E385D3C4653369CLL, pptemplateframe_hash);
      break;
    case 40:
      HASH_GET_STATIC_PROPERTY(0x292F71DA174B8FA8LL, coreparserfunctions);
      break;
    case 42:
      HASH_GET_STATIC_PROPERTY(0x10456EC31B4BFB2ALL, ppnode_hash_text);
      break;
    case 45:
      HASH_GET_STATIC_PROPERTY(0x22E1A15A5C2B5B2DLL, corelinkfunctions);
      break;
    case 51:
      HASH_GET_STATIC_PROPERTY(0x7C241D3F299577B3LL, ppnode_hash_attr);
      break;
    case 54:
      HASH_GET_STATIC_PROPERTY(0x3B5D5B251E2AC936LL, stripstate);
      HASH_GET_STATIC_PROPERTY(0x108F82A2A8707EB6LL, ppdaccum_hash);
      HASH_GET_STATIC_PROPERTY(0x1FB0C5A1E83638B6LL, pptemplateframe_dom);
      break;
    case 65:
      HASH_GET_STATIC_PROPERTY(0x758B1E2426B4FCC1LL, onlyincludereplacer);
      break;
    case 67:
      HASH_GET_STATIC_PROPERTY(0x5A549ACF8D18C043LL, ppframe_hash);
      break;
    case 68:
      HASH_GET_STATIC_PROPERTY(0x6143CF5F8E106F44LL, preprocessor_dom);
      break;
    case 69:
      HASH_GET_STATIC_PROPERTY(0x7CBBE2907466E7C5LL, ppcustomframe_dom);
      break;
    case 77:
      HASH_GET_STATIC_PROPERTY(0x27CB8FF224A1304DLL, ppcustomframe_hash);
      break;
    case 78:
      HASH_GET_STATIC_PROPERTY(0x64CA426643E9574ELL, ppnode_hash_tree);
      break;
    case 82:
      HASH_GET_STATIC_PROPERTY(0x6566F74128FE72D2LL, ppdstackelement);
      break;
    case 83:
      HASH_GET_STATIC_PROPERTY(0x7405ED841E3C0B53LL, parsercache);
      break;
    case 89:
      HASH_GET_STATIC_PROPERTY(0x41F18C209AE6D959LL, ppframe_dom);
      break;
    case 94:
      HASH_GET_STATIC_PROPERTY(0x1FFE0211B5F57DDELL, parseroutput);
      break;
    case 99:
      HASH_GET_STATIC_PROPERTY(0x025B7C56B69451E3LL, ppdstack);
      break;
    case 101:
      HASH_GET_STATIC_PROPERTY(0x4C6DB3EB60853BE5LL, linkmarkerreplacer);
      break;
    case 102:
      HASH_GET_STATIC_PROPERTY(0x4547ADA8DCEEF266LL, ppnode_dom);
      break;
    case 105:
      HASH_GET_STATIC_PROPERTY(0x7B4A05176D718BE9LL, ppdpart_hash);
      break;
    case 108:
      HASH_GET_STATIC_PROPERTY(0x4FBEA60C53B842ECLL, parser_linkhooks);
      break;
    case 109:
      HASH_GET_STATIC_PROPERTY(0x7C7CEDA62E334FEDLL, ppdpart);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 127) {
    case 8:
      HASH_GET_STATIC_PROPERTY_LV(0x4B1D337C630F8C88LL, ppdstack_hash);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY_LV(0x5FAE2838195DC60ALL, parser_difftest);
      break;
    case 14:
      HASH_GET_STATIC_PROPERTY_LV(0x696D931EC4A9598ELL, dateformatter);
      break;
    case 15:
      HASH_GET_STATIC_PROPERTY_LV(0x465F5F4F142EDE8FLL, parser);
      break;
    case 16:
      HASH_GET_STATIC_PROPERTY_LV(0x0301D2F46DDC7F90LL, linkholderarray);
      HASH_GET_STATIC_PROPERTY_LV(0x20107B32F3A2D110LL, ppnode_hash_array);
      break;
    case 19:
      HASH_GET_STATIC_PROPERTY_LV(0x7C178B2BFCA03693LL, parseroptions);
      HASH_GET_STATIC_PROPERTY_LV(0x53917CE42D229113LL, ppdstackelement_hash);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY_LV(0x54A5D6532527C714LL, mwtidy);
      break;
    case 26:
      HASH_GET_STATIC_PROPERTY_LV(0x1976C8E9CAE4D79ALL, preprocessor_hash);
      break;
    case 28:
      HASH_GET_STATIC_PROPERTY_LV(0x0E385D3C4653369CLL, pptemplateframe_hash);
      break;
    case 40:
      HASH_GET_STATIC_PROPERTY_LV(0x292F71DA174B8FA8LL, coreparserfunctions);
      break;
    case 42:
      HASH_GET_STATIC_PROPERTY_LV(0x10456EC31B4BFB2ALL, ppnode_hash_text);
      break;
    case 45:
      HASH_GET_STATIC_PROPERTY_LV(0x22E1A15A5C2B5B2DLL, corelinkfunctions);
      break;
    case 51:
      HASH_GET_STATIC_PROPERTY_LV(0x7C241D3F299577B3LL, ppnode_hash_attr);
      break;
    case 54:
      HASH_GET_STATIC_PROPERTY_LV(0x3B5D5B251E2AC936LL, stripstate);
      HASH_GET_STATIC_PROPERTY_LV(0x108F82A2A8707EB6LL, ppdaccum_hash);
      HASH_GET_STATIC_PROPERTY_LV(0x1FB0C5A1E83638B6LL, pptemplateframe_dom);
      break;
    case 65:
      HASH_GET_STATIC_PROPERTY_LV(0x758B1E2426B4FCC1LL, onlyincludereplacer);
      break;
    case 67:
      HASH_GET_STATIC_PROPERTY_LV(0x5A549ACF8D18C043LL, ppframe_hash);
      break;
    case 68:
      HASH_GET_STATIC_PROPERTY_LV(0x6143CF5F8E106F44LL, preprocessor_dom);
      break;
    case 69:
      HASH_GET_STATIC_PROPERTY_LV(0x7CBBE2907466E7C5LL, ppcustomframe_dom);
      break;
    case 77:
      HASH_GET_STATIC_PROPERTY_LV(0x27CB8FF224A1304DLL, ppcustomframe_hash);
      break;
    case 78:
      HASH_GET_STATIC_PROPERTY_LV(0x64CA426643E9574ELL, ppnode_hash_tree);
      break;
    case 82:
      HASH_GET_STATIC_PROPERTY_LV(0x6566F74128FE72D2LL, ppdstackelement);
      break;
    case 83:
      HASH_GET_STATIC_PROPERTY_LV(0x7405ED841E3C0B53LL, parsercache);
      break;
    case 89:
      HASH_GET_STATIC_PROPERTY_LV(0x41F18C209AE6D959LL, ppframe_dom);
      break;
    case 94:
      HASH_GET_STATIC_PROPERTY_LV(0x1FFE0211B5F57DDELL, parseroutput);
      break;
    case 99:
      HASH_GET_STATIC_PROPERTY_LV(0x025B7C56B69451E3LL, ppdstack);
      break;
    case 101:
      HASH_GET_STATIC_PROPERTY_LV(0x4C6DB3EB60853BE5LL, linkmarkerreplacer);
      break;
    case 102:
      HASH_GET_STATIC_PROPERTY_LV(0x4547ADA8DCEEF266LL, ppnode_dom);
      break;
    case 105:
      HASH_GET_STATIC_PROPERTY_LV(0x7B4A05176D718BE9LL, ppdpart_hash);
      break;
    case 108:
      HASH_GET_STATIC_PROPERTY_LV(0x4FBEA60C53B842ECLL, parser_linkhooks);
      break;
    case 109:
      HASH_GET_STATIC_PROPERTY_LV(0x7C7CEDA62E334FEDLL, ppdpart);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 127) {
    case 8:
      HASH_GET_CLASS_CONSTANT(0x4B1D337C630F8C88LL, ppdstack_hash);
      break;
    case 10:
      HASH_GET_CLASS_CONSTANT(0x5FAE2838195DC60ALL, parser_difftest);
      break;
    case 14:
      HASH_GET_CLASS_CONSTANT(0x696D931EC4A9598ELL, dateformatter);
      break;
    case 15:
      HASH_GET_CLASS_CONSTANT(0x465F5F4F142EDE8FLL, parser);
      break;
    case 16:
      HASH_GET_CLASS_CONSTANT(0x0301D2F46DDC7F90LL, linkholderarray);
      HASH_GET_CLASS_CONSTANT(0x20107B32F3A2D110LL, ppnode_hash_array);
      break;
    case 19:
      HASH_GET_CLASS_CONSTANT(0x7C178B2BFCA03693LL, parseroptions);
      HASH_GET_CLASS_CONSTANT(0x53917CE42D229113LL, ppdstackelement_hash);
      break;
    case 20:
      HASH_GET_CLASS_CONSTANT(0x54A5D6532527C714LL, mwtidy);
      break;
    case 26:
      HASH_GET_CLASS_CONSTANT(0x1976C8E9CAE4D79ALL, preprocessor_hash);
      break;
    case 28:
      HASH_GET_CLASS_CONSTANT(0x0E385D3C4653369CLL, pptemplateframe_hash);
      break;
    case 40:
      HASH_GET_CLASS_CONSTANT(0x292F71DA174B8FA8LL, coreparserfunctions);
      break;
    case 42:
      HASH_GET_CLASS_CONSTANT(0x10456EC31B4BFB2ALL, ppnode_hash_text);
      break;
    case 45:
      HASH_GET_CLASS_CONSTANT(0x22E1A15A5C2B5B2DLL, corelinkfunctions);
      break;
    case 51:
      HASH_GET_CLASS_CONSTANT(0x7C241D3F299577B3LL, ppnode_hash_attr);
      break;
    case 54:
      HASH_GET_CLASS_CONSTANT(0x3B5D5B251E2AC936LL, stripstate);
      HASH_GET_CLASS_CONSTANT(0x108F82A2A8707EB6LL, ppdaccum_hash);
      HASH_GET_CLASS_CONSTANT(0x1FB0C5A1E83638B6LL, pptemplateframe_dom);
      break;
    case 65:
      HASH_GET_CLASS_CONSTANT(0x758B1E2426B4FCC1LL, onlyincludereplacer);
      break;
    case 67:
      HASH_GET_CLASS_CONSTANT(0x5A549ACF8D18C043LL, ppframe_hash);
      break;
    case 68:
      HASH_GET_CLASS_CONSTANT(0x6143CF5F8E106F44LL, preprocessor_dom);
      break;
    case 69:
      HASH_GET_CLASS_CONSTANT(0x7CBBE2907466E7C5LL, ppcustomframe_dom);
      break;
    case 77:
      HASH_GET_CLASS_CONSTANT(0x27CB8FF224A1304DLL, ppcustomframe_hash);
      break;
    case 78:
      HASH_GET_CLASS_CONSTANT(0x64CA426643E9574ELL, ppnode_hash_tree);
      break;
    case 82:
      HASH_GET_CLASS_CONSTANT(0x6566F74128FE72D2LL, ppdstackelement);
      break;
    case 83:
      HASH_GET_CLASS_CONSTANT(0x7405ED841E3C0B53LL, parsercache);
      break;
    case 89:
      HASH_GET_CLASS_CONSTANT(0x41F18C209AE6D959LL, ppframe_dom);
      break;
    case 94:
      HASH_GET_CLASS_CONSTANT(0x1FFE0211B5F57DDELL, parseroutput);
      break;
    case 99:
      HASH_GET_CLASS_CONSTANT(0x025B7C56B69451E3LL, ppdstack);
      break;
    case 101:
      HASH_GET_CLASS_CONSTANT(0x4C6DB3EB60853BE5LL, linkmarkerreplacer);
      break;
    case 102:
      HASH_GET_CLASS_CONSTANT(0x4547ADA8DCEEF266LL, ppnode_dom);
      break;
    case 105:
      HASH_GET_CLASS_CONSTANT(0x7B4A05176D718BE9LL, ppdpart_hash);
      break;
    case 108:
      HASH_GET_CLASS_CONSTANT(0x4FBEA60C53B842ECLL, parser_linkhooks);
      break;
    case 109:
      HASH_GET_CLASS_CONSTANT(0x7C7CEDA62E334FEDLL, ppdpart);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
