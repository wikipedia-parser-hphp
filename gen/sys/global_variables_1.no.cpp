
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 255) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      break;
    case 10:
      HASH_RETURN(0x62566BC9A306C80ALL, g->GV(wgAllowDisplayTitle),
                  wgAllowDisplayTitle);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x34441773493C6710LL, g->GV(wgEnableImageWhitelist),
                  wgEnableImageWhitelist);
      break;
    case 18:
      HASH_RETURN(0x19A9C284C412E512LL, g->GV(wgServerName),
                  wgServerName);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      HASH_RETURN(0x2FF9B075F5BFC313LL, g->GV(wgExpensiveParserFunctionLimit),
                  wgExpensiveParserFunctionLimit);
      break;
    case 21:
      HASH_RETURN(0x3AD645EB48693B15LL, g->GV(wgTitle),
                  wgTitle);
      break;
    case 30:
      HASH_RETURN(0x70FBE2FBA2859F1ELL, g->GV(wgCategoryPrefixedDefaultSortkey),
                  wgCategoryPrefixedDefaultSortkey);
      break;
    case 38:
      HASH_RETURN(0x5713CE531BD6F326LL, g->GV(wgNoFollowDomainExceptions),
                  wgNoFollowDomainExceptions);
      break;
    case 50:
      HASH_RETURN(0x7D8C1AC5E54A2F32LL, g->GV(wgSitename),
                  wgSitename);
      break;
    case 51:
      HASH_RETURN(0x296D9A8065A85433LL, g->GV(wgUseTeX),
                  wgUseTeX);
      break;
    case 54:
      HASH_RETURN(0x0F25E179DFFE0636LL, g->GV(wgUseTidy),
                  wgUseTidy);
      break;
    case 60:
      HASH_RETURN(0x3354B8ACF06BC43CLL, g->GV(wgMaxArticleSize),
                  wgMaxArticleSize);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    case 68:
      HASH_RETURN(0x167513A6A61FC144LL, g->GV(wgDebugTidy),
                  wgDebugTidy);
      break;
    case 73:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 74:
      HASH_RETURN(0x323EAC15D211984ALL, g->GV(wgContLang),
                  wgContLang);
      break;
    case 76:
      HASH_RETURN(0x7ABD1C02DBBBE94CLL, g->GV(IP),
                  IP);
      break;
    case 77:
      HASH_RETURN(0x3A5C7A341C25754DLL, g->GV(wgServer),
                  wgServer);
      break;
    case 78:
      HASH_RETURN(0x5C8355EBD3A39F4ELL, g->GV(wgTidyOpts),
                  wgTidyOpts);
      break;
    case 80:
      HASH_RETURN(0x30E66D49BBCCAD50LL, g->GV(wgMemc),
                  wgMemc);
      break;
    case 89:
      HASH_RETURN(0x6A62472A9F803459LL, g->GV(wgRegisterInternalExternals),
                  wgRegisterInternalExternals);
      HASH_RETURN(0x10F91D80DF2BB559LL, g->GV(wgLocaltimezone),
                  wgLocaltimezone);
      break;
    case 91:
      HASH_RETURN(0x0139765761C67A5BLL, g->GV(wgTidyInternal),
                  wgTidyInternal);
      break;
    case 105:
      HASH_RETURN(0x2E259B8159F52469LL, g->GV(wgInterwikiMagic),
                  wgInterwikiMagic);
      break;
    case 108:
      HASH_RETURN(0x10B90601AD3E2C6CLL, g->GV(wgLinkHolderBatchSize),
                  wgLinkHolderBatchSize);
      HASH_RETURN(0x0318A2A42E99B86CLL, g->GV(wgLegalTitleChars),
                  wgLegalTitleChars);
      break;
    case 110:
      HASH_RETURN(0x71CF7D337911E36ELL, g->GV(wgScriptPath),
                  wgScriptPath);
      break;
    case 114:
      HASH_RETURN(0x6E99E49E33B88472LL, g->GV(wgAllowExternalImages),
                  wgAllowExternalImages);
      break;
    case 123:
      HASH_RETURN(0x11B7E569AC1CB27BLL, g->GV(wgEnforceHtmlIds),
                  wgEnforceHtmlIds);
      break;
    case 129:
      HASH_RETURN(0x6532D7EA771A6881LL, g->GV(wgEnableScaryTranscluding),
                  wgEnableScaryTranscluding);
      break;
    case 136:
      HASH_RETURN(0x43CAB6B0484C7788LL, g->GV(wgContLanguageCode),
                  wgContLanguageCode);
      break;
    case 140:
      HASH_RETURN(0x1936366D2D1FA48CLL, g->GV(wgMaxPPExpandDepth),
                  wgMaxPPExpandDepth);
      break;
    case 142:
      HASH_RETURN(0x143D72CD152B8F8ELL, g->GV(wgTidyConf),
                  wgTidyConf);
      break;
    case 144:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 150:
      HASH_RETURN(0x2032537EAFB84696LL, g->GV(wgNoFollowLinks),
                  wgNoFollowLinks);
      break;
    case 156:
      HASH_RETURN(0x3AD9F28E2BBC1E9CLL, g->GV(wgUser),
                  wgUser);
      break;
    case 159:
      HASH_RETURN(0x786379403A37E89FLL, g->GV(wgAllowExternalImagesFrom),
                  wgAllowExternalImagesFrom);
      HASH_RETURN(0x7197AC66F7344F9FLL, g->GV(wgExternalLinkTarget),
                  wgExternalLinkTarget);
      break;
    case 161:
      HASH_RETURN(0x5739DEAC95D0EDA1LL, g->GV(wgTidyBin),
                  wgTidyBin);
      break;
    case 163:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      HASH_RETURN(0x22BB9E58DA1D68A3LL, g->GV(wgMaxTemplateDepth),
                  wgMaxTemplateDepth);
      break;
    case 171:
      HASH_RETURN(0x4243E2C0D99B5EABLL, g->GV(wgPreprocessorCacheThreshold),
                  wgPreprocessorCacheThreshold);
      break;
    case 178:
      HASH_RETURN(0x45B9FC509805E5B2LL, g->GV(wgMaxSigChars),
                  wgMaxSigChars);
      break;
    case 179:
      HASH_RETURN(0x2A71D707D84BDDB3LL, g->GV(wgAlwaysUseTidy),
                  wgAlwaysUseTidy);
      break;
    case 191:
      HASH_RETURN(0x512BB6600AF1A1BFLL, g->GV(wgCacheEpoch),
                  wgCacheEpoch);
      break;
    case 194:
      HASH_RETURN(0x414DA0222C674AC2LL, g->GV(wgParserCacheExpireTime),
                  wgParserCacheExpireTime);
      break;
    case 196:
      HASH_RETURN(0x43E03BE5B85A1DC4LL, g->GV(wgAllowSlowParserFunctions),
                  wgAllowSlowParserFunctions);
      break;
    case 198:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      break;
    case 201:
      HASH_RETURN(0x2FCD4CD058FE6AC9LL, g->GV(wgMaxTocLevel),
                  wgMaxTocLevel);
      break;
    case 202:
      HASH_RETURN(0x45F7742DD17392CALL, g->GV(wgRawHtml),
                  wgRawHtml);
      break;
    case 206:
      HASH_RETURN(0x32E38C199DC208CELL, g->GV(wgCleanSignatures),
                  wgCleanSignatures);
      break;
    case 209:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      break;
    case 213:
      HASH_RETURN(0x6BFA60F06006B7D5LL, g->GV(wgUseDynamicDates),
                  wgUseDynamicDates);
      break;
    case 216:
      HASH_RETURN(0x00B8EAEC735E0DD8LL, g->GV(wgNoFollowNsExceptions),
                  wgNoFollowNsExceptions);
      break;
    case 226:
      HASH_RETURN(0x2119480B686F60E2LL, g->GV(wgRequest),
                  wgRequest);
      break;
    case 228:
      HASH_RETURN(0x0E4F0A1D74C1FBE4LL, g->GV(wgRestrictDisplayTitle),
                  wgRestrictDisplayTitle);
      break;
    case 237:
      HASH_RETURN(0x34DEC5871A3E97EDLL, g->GV(wgAllowSpecialInclusion),
                  wgAllowSpecialInclusion);
      break;
    case 238:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 239:
      HASH_RETURN(0x61FA7102D48317EFLL, g->GV(wgMaxPPNodeCount),
                  wgMaxPPNodeCount);
      break;
    case 242:
      HASH_RETURN(0x2E884B89708A87F2LL, g->GV(wgHooks),
                  wgHooks);
      break;
    case 245:
      HASH_RETURN(0x43FBB2C4FE020BF5LL, g->GV(wgNonincludableNamespaces),
                  wgNonincludableNamespaces);
      break;
    case 246:
      HASH_RETURN(0x299EDB527A96ADF6LL, g->GV(wgStylePath),
                  wgStylePath);
      break;
    case 248:
      HASH_RETURN(0x2E3A9BE7A3D449F8LL, g->GV(wgLang),
                  wgLang);
      HASH_RETURN(0x107325AF94A9A5F8LL, g->GV(wgTranscludeCacheExpiry),
                  wgTranscludeCacheExpiry);
      break;
    case 250:
      HASH_RETURN(0x25D14844D69061FALL, g->GV(parserMemc),
                  parserMemc);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
