
#ifndef __GENERATED_cls_pptemplateframe_hash_h__
#define __GENERATED_cls_pptemplateframe_hash_h__

#include <cls/ppframe_hash.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_Hash.php line 1184 */
class c_pptemplateframe_hash : virtual public c_ppframe_hash {
  BEGIN_CLASS_MAP(pptemplateframe_hash)
    PARENT_CLASS(ppframe)
    PARENT_CLASS(ppframe_hash)
  END_CLASS_MAP(pptemplateframe_hash)
  DECLARE_CLASS(pptemplateframe_hash, PPTemplateFrame_Hash, ppframe_hash)
  void init();
  public: Variant m_numberedArgs;
  public: Variant m_namedArgs;
  public: Variant m_parent;
  public: Variant m_numberedExpansionCache;
  public: Variant m_namedExpansionCache;
  public: void t___construct(Variant v_preprocessor, Variant v_parent = false, Variant v_numberedArgs = ScalarArrays::sa_[0], Variant v_namedArgs = ScalarArrays::sa_[0], Variant v_title = false);
  public: ObjectData *create(Variant v_preprocessor, Variant v_parent = false, Variant v_numberedArgs = ScalarArrays::sa_[0], Variant v_namedArgs = ScalarArrays::sa_[0], Variant v_title = false);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t___tostring();
  public: bool t_isempty();
  public: Variant t_getarguments();
  public: Variant t_getnumberedarguments();
  public: Variant t_getnamedarguments();
  public: Variant t_getnumberedargument(CVarRef v_index);
  public: Variant t_getnamedargument(CVarRef v_name);
  public: Variant t_getargument(CVarRef v_name);
  public: bool t_istemplate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_pptemplateframe_hash_h__
