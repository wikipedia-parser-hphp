
#ifndef __GENERATED_cls_dbtestrecorder_h__
#define __GENERATED_cls_dbtestrecorder_h__

#include <cls/dbtestpreviewer.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 44950 */
class c_dbtestrecorder : virtual public c_dbtestpreviewer {
  BEGIN_CLASS_MAP(dbtestrecorder)
    PARENT_CLASS(testrecorder)
    PARENT_CLASS(dbtestpreviewer)
  END_CLASS_MAP(dbtestrecorder)
  DECLARE_CLASS(dbtestrecorder, DbTestRecorder, dbtestpreviewer)
  void init();
  public: void t_start();
  public: void t_record(CVarRef v_test, CVarRef v_result);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_dbtestrecorder_h__
