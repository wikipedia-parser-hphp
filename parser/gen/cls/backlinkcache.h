
#ifndef __GENERATED_cls_backlinkcache_h__
#define __GENERATED_cls_backlinkcache_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 25351 */
class c_backlinkcache : virtual public ObjectData {
  BEGIN_CLASS_MAP(backlinkcache)
  END_CLASS_MAP(backlinkcache)
  DECLARE_CLASS(backlinkcache, BacklinkCache, ObjectData)
  void init();
  public: Variant m_partitionCache;
  public: Variant m_fullResultCache;
  public: Variant m_title;
  public: Variant m_db;
  public: void t___construct(Variant v_title);
  public: ObjectData *create(Variant v_title);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: void t_clear();
  public: void t_setdb(CVarRef v_db);
  public: Variant t_getdb();
  public: Variant t_getlinks(CVarRef v_table, CVarRef v_startId = false, CVarRef v_endId = false);
  public: Variant t_getprefix(CVarRef v_table);
  public: Array t_getconditions(CVarRef v_table);
  public: Variant t_getnumlinks(CVarRef v_table);
  public: Variant t_partition(CVarRef v_table, CVarRef v_batchSize);
  public: Array t_partitionresult(CVarRef v_res, CVarRef v_batchSize);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_backlinkcache_h__
