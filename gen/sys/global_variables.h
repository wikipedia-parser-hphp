
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(wgServer)
    GVS(wgRegisterInternalExternals)
    GVS(wgCacheEpoch)
    GVS(wgUseTeX)
    GVS(wgUseDynamicDates)
    GVS(wgInterwikiMagic)
    GVS(wgAllowExternalImages)
    GVS(wgAllowExternalImagesFrom)
    GVS(wgEnableImageWhitelist)
    GVS(wgAllowSpecialInclusion)
    GVS(wgMaxArticleSize)
    GVS(wgMaxPPNodeCount)
    GVS(wgMaxTemplateDepth)
    GVS(wgMaxPPExpandDepth)
    GVS(wgCleanSignatures)
    GVS(wgExternalLinkTarget)
    GVS(wgUser)
    GVS(wgHooks)
    GVS(wgMemc)
    GVS(wgPreprocessorCacheThreshold)
    GVS(wgContLang)
    GVS(wgLinkHolderBatchSize)
    GVS(wgUseTidy)
    GVS(wgAlwaysUseTidy)
    GVS(wgExpensiveParserFunctionLimit)
    GVS(wgLang)
    GVS(wgRawHtml)
    GVS(wgNoFollowLinks)
    GVS(wgNoFollowNsExceptions)
    GVS(wgNoFollowDomainExceptions)
    GVS(wgSitename)
    GVS(wgServerName)
    GVS(wgScriptPath)
    GVS(wgStylePath)
    GVS(wgLocaltimezone)
    GVS(wgContLanguageCode)
    GVS(wgNonincludableNamespaces)
    GVS(wgEnableScaryTranscluding)
    GVS(wgTranscludeCacheExpiry)
    GVS(wgMaxTocLevel)
    GVS(wgEnforceHtmlIds)
    GVS(wgLegalTitleChars)
    GVS(wgMaxSigChars)
    GVS(wgTitle)
    GVS(wgCategoryPrefixedDefaultSortkey)
    GVS(parserMemc)
    GVS(wgRequest)
    GVS(wgParserCacheExpireTime)
    GVS(wgTidyInternal)
    GVS(wgTidyConf)
    GVS(wgTidyBin)
    GVS(wgTidyOpts)
    GVS(IP)
    GVS(wgDebugTidy)
    GVS(wgAllowDisplayTitle)
    GVS(wgAllowSlowParserFunctions)
    GVS(wgRestrictDisplayTitle)
  END_GVS(57)

  // Dynamic Constants

  // Function/Method Static Variables
  Variant sv_parser_linkhooks$$replaceinternallinks2$$tc;
  Variant sv_ppframe_hash$$expand$$expansionDepth;
  Variant sv_parser$$replaceinternallinks2$$tc;
  Variant sv_parser_linkhooks$$replaceinternallinks2$$titleRegex;
  Variant sv_parser_difftest$$init$$doneHook;
  Variant sv_coreparserfunctions$$pagesincategory$$cache;
  Variant sv_ppframe_dom$$expand$$expansionDepth;
  Variant sv_dateformatter$$getinstance$$dateFormatter;
  Variant sv_parser$$replaceinternallinks2$$e1;
  Variant sv_coreparserfunctions$$israw$$mwRaw;
  Variant sv_coreparserfunctions$$pagesize$$cache;
  Variant sv_parser$$getimageparams$$internalParamNames;
  Variant sv_parser$$getimageparams$$internalParamMap;
  Variant sv_parser$$extracttagsandparams$$n;
  Variant sv_parser$$replaceinternallinks2$$e1_img;
  Variant sv_parsercache$$singleton$$instance;
  Variant sv_parser$$transformmsg$$executing;

  // Function/Method Static Variable Initialization Booleans
  Variant inited_sv_parser_linkhooks$$replaceinternallinks2$$tc;
  Variant inited_sv_ppframe_hash$$expand$$expansionDepth;
  Variant inited_sv_parser$$replaceinternallinks2$$tc;
  Variant inited_sv_parser_linkhooks$$replaceinternallinks2$$titleRegex;
  Variant inited_sv_parser_difftest$$init$$doneHook;
  Variant inited_sv_coreparserfunctions$$pagesincategory$$cache;
  Variant inited_sv_ppframe_dom$$expand$$expansionDepth;
  Variant inited_sv_dateformatter$$getinstance$$dateFormatter;
  Variant inited_sv_parser$$replaceinternallinks2$$e1;
  Variant inited_sv_coreparserfunctions$$israw$$mwRaw;
  Variant inited_sv_coreparserfunctions$$pagesize$$cache;
  Variant inited_sv_parser$$getimageparams$$internalParamNames;
  Variant inited_sv_parser$$getimageparams$$internalParamMap;
  Variant inited_sv_parser$$extracttagsandparams$$n;
  Variant inited_sv_parser$$replaceinternallinks2$$e1_img;
  Variant inited_sv_parsercache$$singleton$$instance;
  Variant inited_sv_parser$$transformmsg$$executing;

  // Class Static Variables
  Variant s_ppdstack$$false;

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$CoreLinkFunctions_php;
  bool run_pm_php$CoreParserFunctions_php;
  bool run_pm_php$DateFormatter_php;
  bool run_pm_php$LinkHolderArray_php;
  bool run_pm_php$ParserCache_php;
  bool run_pm_php$Parser_DiffTest_php;
  bool run_pm_php$Parser_LinkHooks_php;
  bool run_pm_php$ParserOptions_php;
  bool run_pm_php$ParserOutput_php;
  bool run_pm_php$Parser_php;
  bool run_pm_php$Preprocessor_DOM_php;
  bool run_pm_php$Preprocessor_Hash_php;
  bool run_pm_php$Preprocessor_php;
  bool run_pm_php$Tidy_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 69;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[100];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
