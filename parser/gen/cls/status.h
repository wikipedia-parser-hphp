
#ifndef __GENERATED_cls_status_h__
#define __GENERATED_cls_status_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 24483 */
class c_status : virtual public ObjectData {
  BEGIN_CLASS_MAP(status)
  END_CLASS_MAP(status)
  DECLARE_CLASS(status, Status, ObjectData)
  void init();
  public: Variant m_ok;
  public: Variant m_value;
  public: Variant m_successCount;
  public: Variant m_failCount;
  public: Variant m_errors;
  public: Variant m_cleanCallback;
  public: static Variant ti_newfatal(const char* cls, int num_args, CVarRef v_message, Array args = Array());
  public: static p_status ti_newgood(const char* cls, CVarRef v_value = null_variant);
  public: void t_setresult(CVarRef v_ok, CVarRef v_value = null_variant);
  public: bool t_isgood();
  public: Variant t_isok();
  public: void t_warning(int num_args, CVarRef v_message, Array args = Array());
  public: void t_error(int num_args, CVarRef v_message, Array args = Array());
  public: void t_fatal(int num_args, CVarRef v_message, Array args = Array());
  public: Variant t_cleanparams(CVarRef v_params);
  public: String t_getitemxml(CVarRef v_item);
  public: String t_getxml();
  public: Variant t_getwikitext(bool v_shortContext = false, bool v_longContext = false);
  public: void t_merge(CVarRef v_other, CVarRef v_overwriteValue = false);
  public: Array t_geterrorsarray();
  public: bool t_hasmessage(CVarRef v_msg);
  public: static Variant t_newfatal(int num_args, CVarRef v_message, Array args = Array()) { return ti_newfatal("status", num_args, v_message, args); }
  public: static p_status t_newgood(CVarRef v_value = null_variant) { return ti_newgood("status", v_value); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_status_h__
