
#include <cpp/base/hphp.h>
#include <cpp/base/array/zend_array.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void csi_ppdstack();

IMPLEMENT_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
GlobalVariables::GlobalVariables() : dummy(false),
  inited_sv_parser_linkhooks$$replaceinternallinks2$$tc(),
  inited_sv_ppframe_hash$$expand$$expansionDepth(),
  inited_sv_parser$$replaceinternallinks2$$tc(),
  inited_sv_parser_linkhooks$$replaceinternallinks2$$titleRegex(),
  inited_sv_parser_difftest$$init$$doneHook(),
  inited_sv_coreparserfunctions$$pagesincategory$$cache(),
  inited_sv_ppframe_dom$$expand$$expansionDepth(),
  inited_sv_dateformatter$$getinstance$$dateFormatter(),
  inited_sv_parser$$replaceinternallinks2$$e1(),
  inited_sv_coreparserfunctions$$israw$$mwRaw(),
  inited_sv_coreparserfunctions$$pagesize$$cache(),
  inited_sv_parser$$getimageparams$$internalParamNames(),
  inited_sv_parser$$getimageparams$$internalParamMap(),
  inited_sv_parser$$extracttagsandparams$$n(),
  inited_sv_parser$$replaceinternallinks2$$e1_img(),
  inited_sv_parsercache$$singleton$$instance(),
  inited_sv_parser$$transformmsg$$executing(),
  run_pm_php$CoreLinkFunctions_php(false),
  run_pm_php$CoreParserFunctions_php(false),
  run_pm_php$DateFormatter_php(false),
  run_pm_php$LinkHolderArray_php(false),
  run_pm_php$ParserCache_php(false),
  run_pm_php$Parser_DiffTest_php(false),
  run_pm_php$Parser_LinkHooks_php(false),
  run_pm_php$ParserOptions_php(false),
  run_pm_php$ParserOutput_php(false),
  run_pm_php$Parser_php(false),
  run_pm_php$Preprocessor_DOM_php(false),
  run_pm_php$Preprocessor_Hash_php(false),
  run_pm_php$Preprocessor_php(false),
  run_pm_php$Tidy_php(false) {

  // Dynamic Constants

  // Primitive Function/Method Static Variables

  // Primitive Class Static Variables

  // Redeclared Functions

  // Redeclared Classes
}

void GlobalVariables::initialize() {
  SystemGlobals::initialize();
  csi_ppdstack();
}

void init_static_variables() { ScalarArrays::initialize();}
static ThreadLocalSingleton<GlobalVariables> g_variables;
static IMPLEMENT_THREAD_LOCAL(GlobalArrayWrapper, g_array_wrapper);
GlobalVariables *get_global_variables() {
  return g_variables.get();
}
void init_global_variables() { GlobalVariables::initialize();}
void free_global_variables() {
  g_variables.reset();
  g_array_wrapper.reset();
}
LVariableTable *get_variable_table() { return (LVariableTable*)get_global_variables();}
Globals *get_globals() { return (Globals*)get_global_variables();}
SystemGlobals *get_system_globals() { return (SystemGlobals*)get_global_variables();}
Array get_global_array_wrapper(){ return g_array_wrapper.get();}

///////////////////////////////////////////////////////////////////////////////
}

#ifndef HPHP_BUILD_LIBRARY
int main(int argc, char** argv) {
  return HPHP::execute_program(argc, argv);
}
#endif
