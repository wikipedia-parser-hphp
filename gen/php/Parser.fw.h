
#ifndef __GENERATED_php_Parser_fw_h__
#define __GENERATED_php_Parser_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_DB_MASTER;
extern const StaticString k_NS_MEDIAWIKI;
extern const StaticString k_NS_TEMPLATE;
extern const StaticString k_TS_MW;
extern const StaticString k_TS_UNIX;
extern const StaticString k_SFH_OBJECT_ARGS;
extern const StaticString k_SFH_NO_HASH;


// 2. Classes
FORWARD_DECLARE_CLASS(stripstate)
FORWARD_DECLARE_CLASS(parser)
extern const StaticString q_parser_VERSION;
extern const int64 q_parser_SFH_NO_HASH;
extern const int64 q_parser_SFH_OBJECT_ARGS;
extern const StaticString q_parser_EXT_LINK_URL_CLASS;
extern const StaticString q_parser_EXT_IMAGE_REGEX;
extern const int64 q_parser_COLON_STATE_TEXT;
extern const int64 q_parser_COLON_STATE_TAG;
extern const int64 q_parser_COLON_STATE_TAGSTART;
extern const int64 q_parser_COLON_STATE_CLOSETAG;
extern const int64 q_parser_COLON_STATE_TAGSLASH;
extern const int64 q_parser_COLON_STATE_COMMENT;
extern const int64 q_parser_COLON_STATE_COMMENTDASH;
extern const int64 q_parser_COLON_STATE_COMMENTDASHDASH;
extern const int64 q_parser_PTD_FOR_INCLUSION;
extern const int64 q_parser_OT_HTML;
extern const int64 q_parser_OT_WIKI;
extern const int64 q_parser_OT_PREPROCESS;
extern const int64 q_parser_OT_MSG;
extern const StaticString q_parser_MARKER_SUFFIX;
FORWARD_DECLARE_CLASS(onlyincludereplacer)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/CoreParserFunctions.fw.h>
#include <php/DateFormatter.fw.h>
#include <php/LinkHolderArray.fw.h>
#include <php/ParserOptions.fw.h>
#include <php/ParserOutput.fw.h>
#include <php/Preprocessor.fw.h>
#include <php/Tidy.fw.h>

#endif // __GENERATED_php_Parser_fw_h__
