
#ifndef __GENERATED_cls_stringutils_h__
#define __GENERATED_cls_stringutils_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 40008 */
class c_stringutils : virtual public ObjectData {
  BEGIN_CLASS_MAP(stringutils)
  END_CLASS_MAP(stringutils)
  DECLARE_CLASS(stringutils, StringUtils, ObjectData)
  void init();
  public: static Variant ti_hungrydelimiterreplace(const char* cls, CVarRef v_startDelim, CVarRef v_endDelim, CVarRef v_replace, CVarRef v_subject);
  public: static String ti_delimiterreplacecallback(const char* cls, CStrRef v_startDelim, Variant v_endDelim, CVarRef v_callback, CVarRef v_subject, CStrRef v_flags = "");
  public: static String ti_delimiterreplace(const char* cls, CStrRef v_startDelim, CStrRef v_endDelim, CStrRef v_replace, CVarRef v_subject, CStrRef v_flags = "");
  public: static Variant ti_explodemarkup(const char* cls, CVarRef v_separator, Variant v_text);
  public: static Variant ti_escaperegexreplacement(const char* cls, Variant v_string);
  public: static Variant ti_explode(const char* cls, CStrRef v_separator, CVarRef v_subject);
  public: static Variant t_escaperegexreplacement(CVarRef v_string) { return ti_escaperegexreplacement("stringutils", v_string); }
  public: static String t_delimiterreplace(CStrRef v_startDelim, CStrRef v_endDelim, CStrRef v_replace, CVarRef v_subject, CStrRef v_flags = "") { return ti_delimiterreplace("stringutils", v_startDelim, v_endDelim, v_replace, v_subject, v_flags); }
  public: static Variant t_hungrydelimiterreplace(CVarRef v_startDelim, CVarRef v_endDelim, CVarRef v_replace, CVarRef v_subject) { return ti_hungrydelimiterreplace("stringutils", v_startDelim, v_endDelim, v_replace, v_subject); }
  public: static Variant t_explodemarkup(CVarRef v_separator, CVarRef v_text) { return ti_explodemarkup("stringutils", v_separator, v_text); }
  public: static String t_delimiterreplacecallback(CStrRef v_startDelim, CVarRef v_endDelim, CVarRef v_callback, CVarRef v_subject, CStrRef v_flags = "") { return ti_delimiterreplacecallback("stringutils", v_startDelim, v_endDelim, v_callback, v_subject, v_flags); }
  public: static Variant t_explode(CStrRef v_separator, CVarRef v_subject) { return ti_explode("stringutils", v_separator, v_subject); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_stringutils_h__
