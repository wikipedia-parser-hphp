/*
** MyHacks.h
** 
** Made by Michael DuPont
** Login   <mdupont@mdupontdesktop2>
** 
** Started on  Sat Mar 14 17:38:01 2009 Michael DuPont
** Last update Sat Mar 14 17:38:01 2009 Michael DuPont
*/

#ifndef   	MYHACKS_H_
# define   	MYHACKS_H_

#include "php.h"


extern void * s;// ups
extern void * TODOVariableFunction;
extern void * TODO_fci;
extern void *  local_htmlelements;
extern void *  local_htmllist;
extern void *  local_htmlnest;
extern void *  local_htmlpairs;
extern void *  local_htmlsingle;
extern void *  local_htmlsingleallowed;
extern void *  local_htmlsingleonly;
extern void *  local_listtags;
extern void *  local_recursionGuard;
extern void *  local_staticInitialised;
extern void *  local_tabletags;
extern void *  local_wikiLowerChars;
extern void *  local_wikiUpperChars;
extern void *  my_local_TLE25724;
extern void *  my_local_TSt25723;
extern void *  my_local_TSt26239;
extern void *  my_local_TSt26261;
extern void *  my_local_TSt26267;
extern void *  my_local_args;
extern void *  my_local_article;
extern void *  my_local_change;
extern void *  my_local_contextNode;
extern void *  my_local_data;
extern void *  my_local_database;
extern void *  my_local_db;
extern void *  my_local_domdocument;
extern void *  my_local_domnode;
extern void *  my_local_domnodelist;
extern void *  my_local_e;
extern void *  my_local_faketitle;
extern void *  my_local_frame;
extern void *  my_local_iteratorNode;
extern void *  my_local_langcode;
extern void *  my_local_language;
extern void *  my_local_mwexception;
extern void *  my_local_newIterator;
extern void *  my_local_newtitle;
extern void *  my_local_obj;
extern void *  my_local_ppframe;
extern void *  my_local_ppnode;
extern void *  my_local_ppnode_dom;
extern void *  my_local_recentchange;
extern void *  my_local_res;
extern void *  my_local_resultwrapper;
extern void *  my_local_root;
extern void *  my_local_stubobject;
extern void *  my_local_subpages;
extern void *  my_local_t;
extern void *  my_local_titlearray;
extern void *  my_local_wgArticle;
extern void *  name;

// hack these values
extern void * my_local_title; 
extern void * my_local_nt; 
extern void * my_local_target; 


//zval* local_TLE2375
void MyThrowImp();





#define MyThrow(X) MyThrowImp( local_ ## X );






#define BEGINMyTry
#define EndMyTry
#define RefCast


#define String(X) MyStringImp( my_local_ ## X )


//MyStatic(privateRanges);

#define MyStatic(X) MyStaticImp( local_ ## X );






zvalP getThis();

#define TSRMLS_D
#define TSRMLS_C

#include "phcstd.h"

#endif 	    /* !MYHACKS_H_ */
