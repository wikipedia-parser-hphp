
#ifndef __GENERATED_cls_ppcustomframe_dom_h__
#define __GENERATED_cls_ppcustomframe_dom_h__

#include <cls/ppframe_dom.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 35017 */
class c_ppcustomframe_dom : virtual public c_ppframe_dom {
  BEGIN_CLASS_MAP(ppcustomframe_dom)
    PARENT_CLASS(ppframe)
    PARENT_CLASS(ppframe_dom)
  END_CLASS_MAP(ppcustomframe_dom)
  DECLARE_CLASS(ppcustomframe_dom, PPCustomFrame_DOM, ppframe_dom)
  void init();
  public: Variant m_args;
  public: void t___construct(Variant v_preprocessor, Variant v_args);
  public: ObjectData *create(Variant v_preprocessor, Variant v_args);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t___tostring();
  public: bool t_isempty();
  public: Variant t_getargument(CVarRef v_index);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppcustomframe_dom_h__
