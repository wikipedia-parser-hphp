
#ifndef __GENERATED_cls_filecache_h__
#define __GENERATED_cls_filecache_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 95 */
class c_filecache : virtual public ObjectData {
  BEGIN_CLASS_MAP(filecache)
  END_CLASS_MAP(filecache)
  DECLARE_CLASS(filecache, FileCache, ObjectData)
  void init();
  public: static void ti_destroysingleton(const char* cls);
  public: static void t_destroysingleton() { ti_destroysingleton("filecache"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_filecache_h__
