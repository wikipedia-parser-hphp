
#ifndef __GENERATED_cls_language_h__
#define __GENERATED_cls_language_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 11873 */
class c_language : virtual public ObjectData {
  BEGIN_CLASS_MAP(language)
  END_CLASS_MAP(language)
  DECLARE_CLASS(language, Language, ObjectData)
  void init();
  public: Variant m_mConverter;
  public: Variant m_mVariants;
  public: Variant m_mCode;
  public: Variant m_mLoaded;
  public: Variant m_mMagicExtensions;
  public: Variant m_mMagicHookDone;
  public: virtual void destruct();
  public: static Variant ti_factory(const char* cls, CVarRef v_code);
  public: static Variant ti_newfromcode(const char* cls, Variant v_code);
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t___destruct();
  public: void t_initcontlang();
  public: Variant t_getdefaultuseroptions();
  public: Variant t_getfallbacklanguagecode();
  public: Variant t_getbookstorelist();
  public: Variant t_getnamespaces();
  public: Variant t_getformattednamespaces();
  public: Variant t_getnstext(CVarRef v_index);
  public: String t_getformattednstext(CVarRef v_index);
  public: Variant t_getlocalnsindex(CVarRef v_text);
  public: Variant t_getnsindex(CVarRef v_text);
  public: Variant t_getvariantname(CVarRef v_code);
  public: String t_specialpage(Variant v_name);
  public: Array t_getquickbarsettings();
  public: Variant t_getmathnames();
  public: Variant t_getdatepreferences();
  public: Variant t_getdateformats();
  public: Variant t_getdefaultdateformat();
  public: Variant t_getdatepreferencemigrationmap();
  public: Variant t_getimagefile(CVarRef v_image);
  public: Variant t_getdefaultuseroptionoverrides();
  public: Variant t_getextrausertoggles();
  public: Variant t_getusertoggle(CVarRef v_tog);
  public: static Variant ti_getlanguagenames(const char* cls, bool v_customisedOnly = false);
  public: Variant t_getmessagefromdb(CVarRef v_msg);
  public: Variant t_getlanguagename(CVarRef v_code);
  public: Variant t_getmonthname(CVarRef v_key);
  public: Variant t_getmonthnamegen(CVarRef v_key);
  public: Variant t_getmonthabbreviation(CVarRef v_key);
  public: Variant t_getweekdayname(CVarRef v_key);
  public: Variant t_getweekdayabbreviation(Numeric v_key);
  public: Variant t_getiraniancalendarmonthname(CVarRef v_key);
  public: Variant t_gethebrewcalendarmonthname(CVarRef v_key);
  public: Variant t_gethebrewcalendarmonthnamegen(CVarRef v_key);
  public: Variant t_gethijricalendarmonthname(CVarRef v_key);
  public: Variant t_useradjust(CVarRef v_ts, Variant v_tz = false);
  public: Variant t_sprintfdate(CVarRef v_format, CVarRef v_ts);
  public: static Array ti_tstoiranian(const char* cls, CVarRef v_ts);
  public: static Array ti_tstohijri(const char* cls, CVarRef v_ts);
  public: static Array ti_tstohebrew(const char* cls, CVarRef v_ts);
  public: static Numeric ti_hebrewyearstart(const char* cls, Numeric v_year);
  public: static Array ti_tstothai(const char* cls, CVarRef v_ts);
  public: static Variant ti_romannumeral(const char* cls, Variant v_num);
  public: static Variant ti_hebrewnumeral(const char* cls, Variant v_num);
  public: Variant t_dateformat(CVarRef v_usePrefs = true);
  public: Variant t_date(Variant v_ts, CVarRef v_adj = false, CVarRef v_format = true, CVarRef v_timecorrection = false);
  public: Variant t_time(Variant v_ts, CVarRef v_adj = false, CVarRef v_format = true, CVarRef v_timecorrection = false);
  public: Variant t_timeanddate(Variant v_ts, CVarRef v_adj = false, CVarRef v_format = true, CVarRef v_timecorrection = false);
  public: Variant t_getmessage(CVarRef v_key);
  public: Variant t_getallmessages();
  public: Variant t_iconv(CVarRef v_in, CVarRef v_out, CVarRef v_string);
  public: Variant t_ucwordbreakscallbackascii(CVarRef v_matches);
  public: Variant t_ucwordbreakscallbackmb(CVarRef v_matches);
  public: String t_uccallback(CVarRef v_matches);
  public: String t_lccallback(CVarRef v_matches);
  public: Variant t_ucwordscallbackmb(CVarRef v_matches);
  public: String t_ucwordscallbackwiki(CVarRef v_matches);
  public: Variant t_ucfirst(CVarRef v_str);
  public: Variant t_uc(CVarRef v_str, CVarRef v_first = false);
  public: Variant t_lcfirst(Variant v_str);
  public: Variant t_lc(CVarRef v_str, CVarRef v_first = false);
  public: bool t_ismultibyte(CVarRef v_str);
  public: Variant t_ucwords(Variant v_str);
  public: Variant t_ucwordbreaks(Variant v_str);
  public: Variant t_casefold(CVarRef v_s);
  public: Variant t_checktitleencoding(CVarRef v_s);
  public: Variant t_fallback8bitencoding();
  public: Variant t_stripforsearch(CVarRef v_string);
  public: String t_stripforsearchcallback(CVarRef v_matches);
  public: Variant t_minsearchlength();
  public: Variant t_convertforsearchresult(CVarRef v_termsArray);
  public: Variant t_firstchar(CVarRef v_s);
  public: void t_initencoding();
  public: Variant t_recodeforedit(CVarRef v_s);
  public: Variant t_recodeinput(CVarRef v_s);
  public: Variant t_isrtl();
  public: Variant t_getdirmark();
  public: Variant t_getarrow();
  public: Variant t_linkprefixextension();
  public: Variant t_getmagicwords();
  public: void t_getmagic(Variant v_mw);
  public: void t_addmagicwordsbylang(CVarRef v_newWords);
  public: Variant t_getspecialpagealiases();
  public: Variant t_fixspecialpagealiases(CVarRef v_mixed);
  public: String t_emphasize(CVarRef v_text);
  public: Variant t_formatnum(Variant v_number, CVarRef v_nocommafy = false);
  public: Variant t_parseformattednumber(Variant v_number);
  public: String t_commafy(CVarRef v__);
  public: Variant t_digittransformtable();
  public: Variant t_separatortransformtable();
  public: Variant t_listtotext(CVarRef v_l);
  public: String t_commalist(CVarRef v_list);
  public: String t_pipelist(CVarRef v_list);
  public: Variant t_truncate(Variant v_string, CVarRef v_length, CVarRef v_ellipsis = "");
  public: Variant t_convertgrammar(CVarRef v_word, CVarRef v_case);
  public: Variant t_gender(CVarRef v_gender, Variant v_forms);
  public: Variant t_convertplural(CVarRef v_count, Variant v_forms);
  public: Variant t_preconvertplural(Variant v_forms, int64 v_count);
  public: Variant t_translateblockexpiry(CVarRef v_str);
  public: Variant t_segmentfordiff(CVarRef v_text);
  public: Variant t_unsegmentfordiff(CVarRef v_text);
  public: Variant t_convert(Variant v_text, Variant v_isTitle = false);
  public: Variant t_parserconvert(Variant v_text, Variant v_parser);
  public: bool t_hasvariants();
  public: Variant t_armourmath(Variant v_text);
  public: String t_converthtml(CVarRef v_text, bool v_isTitle = false);
  public: Variant t_convertcategorykey(Variant v_key);
  public: Variant t_getvariants();
  public: Variant t_getpreferredvariant(Variant v_fromUser = true);
  public: void t_findvariantlink(Variant v_link, Variant v_nt, Variant v_ignoreOtherCond = false);
  public: Variant t_convertlinktoallvariants(Variant v_text);
  public: Variant t_getextrahashoptions();
  public: Variant t_getparsedtitle();
  public: Variant t_marknoconversion(Variant v_text, Variant v_noParse = false);
  public: Variant t_linktrail();
  public: p_language t_getlangobj();
  public: Variant t_getcode();
  public: void t_setcode(CVarRef v_code);
  public: static String ti_getfilename(const char* cls, CStrRef v_prefix = "Language", CVarRef v_code = null_variant, CStrRef v_suffix = ".php");
  public: static String ti_getmessagesfilename(const char* cls, CVarRef v_code);
  public: static String ti_getclassfilename(const char* cls, CVarRef v_code);
  public: static Variant ti_getlocalisationarray(const char* cls, CVarRef v_code, bool v_disableCache = false);
  public: static Variant ti_loadlocalisation(const char* cls, Variant v_code, Variant v_disableCache = false);
  public: static bool ti_islocalisationoutofdate(const char* cls, Variant v_cache);
  public: static Variant ti_getfallbackfor(const char* cls, CVarRef v_code);
  public: static Variant ti_getmessagesfor(const char* cls, CVarRef v_code);
  public: static Variant ti_getmessagefor(const char* cls, CVarRef v_key, CVarRef v_code);
  public: void t_load();
  public: void t_fixupsettings();
  public: Variant t_fixvariableinnamespace(Variant v_talk);
  public: Variant t_replacegrammarinnamespace(CVarRef v_m);
  public: static Array ti_getcasemaps(const char* cls);
  public: Variant t_formattimeperiod(CVarRef v_seconds);
  public: String t_formatbitrate(CVarRef v_bps);
  public: Variant t_formatsize(Variant v_size);
  public: static Variant t_getmessagesfor(CVarRef v_code) { return ti_getmessagesfor("language", v_code); }
  public: static Variant t_factory(CVarRef v_code) { return ti_factory("language", v_code); }
  public: static Variant t_loadlocalisation(CVarRef v_code, CVarRef v_disableCache = false) { return ti_loadlocalisation("language", v_code, v_disableCache); }
  public: static String t_getmessagesfilename(CVarRef v_code) { return ti_getmessagesfilename("language", v_code); }
  public: static Array t_tstoiranian(CVarRef v_ts) { return ti_tstoiranian("language", v_ts); }
  public: static Array t_getcasemaps() { return ti_getcasemaps("language"); }
  public: static Variant t_getmessagefor(CVarRef v_key, CVarRef v_code) { return ti_getmessagefor("language", v_key, v_code); }
  public: static Array t_tstohijri(CVarRef v_ts) { return ti_tstohijri("language", v_ts); }
  public: static String t_getfilename(CStrRef v_prefix = "Language", CVarRef v_code = null_variant, CStrRef v_suffix = ".php") { return ti_getfilename("language", v_prefix, v_code, v_suffix); }
  public: static Array t_tstothai(CVarRef v_ts) { return ti_tstothai("language", v_ts); }
  public: static Array t_tstohebrew(CVarRef v_ts) { return ti_tstohebrew("language", v_ts); }
  public: static Variant t_newfromcode(CVarRef v_code) { return ti_newfromcode("language", v_code); }
  public: static String t_getclassfilename(CVarRef v_code) { return ti_getclassfilename("language", v_code); }
  public: static Variant t_getlocalisationarray(CVarRef v_code, bool v_disableCache = false) { return ti_getlocalisationarray("language", v_code, v_disableCache); }
  public: static Variant t_getlanguagenames(bool v_customisedOnly = false) { return ti_getlanguagenames("language", v_customisedOnly); }
  public: static Numeric t_hebrewyearstart(Numeric v_year) { return ti_hebrewyearstart("language", v_year); }
  public: static Variant t_hebrewnumeral(CVarRef v_num) { return ti_hebrewnumeral("language", v_num); }
  public: static Variant t_getfallbackfor(CVarRef v_code) { return ti_getfallbackfor("language", v_code); }
  public: static bool t_islocalisationoutofdate(CVarRef v_cache) { return ti_islocalisationoutofdate("language", v_cache); }
  public: static Variant t_romannumeral(CVarRef v_num) { return ti_romannumeral("language", v_num); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_language_h__
