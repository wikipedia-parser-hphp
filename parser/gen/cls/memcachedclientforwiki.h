
#ifndef __GENERATED_cls_memcachedclientforwiki_h__
#define __GENERATED_cls_memcachedclientforwiki_h__

#include <cls/memcached.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 43523 */
class c_memcachedclientforwiki : virtual public c_memcached {
  BEGIN_CLASS_MAP(memcachedclientforwiki)
    PARENT_CLASS(memcached)
  END_CLASS_MAP(memcachedclientforwiki)
  DECLARE_CLASS(memcachedclientforwiki, MemCachedClientforWiki, memcached)
  void init();
  public: void t__debugprint(CVarRef v_text);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_memcachedclientforwiki_h__
