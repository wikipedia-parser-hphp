
#ifndef __GENERATED_cls_ppnode_h__
#define __GENERATED_cls_ppnode_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 33615 */
class c_ppnode : virtual public ObjectData {
  BEGIN_CLASS_MAP(ppnode)
  END_CLASS_MAP(ppnode)
  DECLARE_CLASS(ppnode, PPNode, ObjectData)
  void init();
  public: void t_getchildren();
  public: void t_getfirstchild();
  public: void t_getnextsibling();
  public: void t_getchildrenoftype(CVarRef v_type);
  public: void t_getlength();
  public: void t_item(CVarRef v_i);
  public: void t_getname();
  public: void t_splitarg();
  public: void t_splitext();
  public: void t_splitheading();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppnode_h__
