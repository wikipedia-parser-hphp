<?xml version='1.0'?>
<xsl:stylesheet
  xmlns:AST="http://www.phpcompiler.org/phc-1.1"
  xmlns:past="http://www.parrotcode.org/PAST-0.1"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0" >

<xsl:template match="AST:PHP_script">
	<AST:PHP_script><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:PHP_script><!--xsl: -->
</xsl:template>
<xsl:template match="TOP">
	<TOP><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</TOP><!--xsl: -->
</xsl:template>
<xsl:template match="TOP/AST:PHP_script">
	<TOP><!--xsl: -->
		<AST:PHP_script><!--xsl: -->
		    <xsl:apply-templates select="AST:PHP_script"/>
		</AST:PHP_script><!--xsl: -->
	</TOP><!--xsl: -->
</xsl:template>
<xsl:template match="attrs">
	<attrs><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</attrs><!--xsl: -->
</xsl:template>
<xsl:template match="AST:PHP_script/attrs">
	<AST:PHP_script><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:PHP_script><!--xsl: -->
</xsl:template>
<xsl:template match="attr">
	<attr><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</attr><!--xsl: -->
</xsl:template>
<xsl:template match="attrs/attr">
	<attrs><!--xsl: -->
		<attr><!--xsl: -->
		    <xsl:apply-templates select="attr"/>
		</attr><!--xsl: -->
	</attrs><!--xsl: -->
</xsl:template>
<xsl:template match="string">
	<string><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</string><!--xsl: -->
</xsl:template>
<xsl:template match="attr/string">
	<attr><!--xsl: -->
		<string><!--xsl: -->
		    <xsl:apply-templates select="string"/>
		</string><!--xsl: -->
	</attr><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Class_def">
	<AST:Class_def><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Class_def><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list">
	<AST:Statement_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Class_def">
	<AST:Statement_list><!--xsl: -->
		<AST:Class_def><!--xsl: -->
		    <xsl:apply-templates select="AST:Class_def"/>
		</AST:Class_def><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Class_def/attrs">
	<AST:Class_def><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Class_def><!--xsl: -->
</xsl:template>
<xsl:template match="string_list">
	<string_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</string_list><!--xsl: -->
</xsl:template>
<xsl:template match="attr/string_list">
	<attr><!--xsl: -->
		<string_list><!--xsl: -->
		    <xsl:apply-templates select="string_list"/>
		</string_list><!--xsl: -->
	</attr><!--xsl: -->
</xsl:template>
<xsl:template match="string_list/string">
	<string_list><!--xsl: -->
		<string><!--xsl: -->
		    <xsl:apply-templates select="string"/>
		</string><!--xsl: -->
	</string_list><!--xsl: -->
</xsl:template>
<xsl:template match="integer">
	<integer><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</integer><!--xsl: -->
</xsl:template>
<xsl:template match="attr/integer">
	<attr><!--xsl: -->
		<integer><!--xsl: -->
		    <xsl:apply-templates select="integer"/>
		</integer><!--xsl: -->
	</attr><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Class_mod">
	<AST:Class_mod><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Class_mod><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Class_mod/attrs">
	<AST:Class_mod><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Class_mod><!--xsl: -->
</xsl:template>
<xsl:template match="AST:CLASS_NAME">
	<AST:CLASS_NAME><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:CLASS_NAME><!--xsl: -->
</xsl:template>
<xsl:template match="AST:CLASS_NAME/attrs">
	<AST:CLASS_NAME><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:CLASS_NAME><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Attribute">
	<AST:Attribute><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Attribute><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Member_list">
	<AST:Member_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Member_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Member_list/AST:Attribute">
	<AST:Member_list><!--xsl: -->
		<AST:Attribute><!--xsl: -->
		    <xsl:apply-templates select="AST:Attribute"/>
		</AST:Attribute><!--xsl: -->
	</AST:Member_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Attribute/attrs">
	<AST:Attribute><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Attribute><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Attr_mod">
	<AST:Attr_mod><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Attr_mod><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Attr_mod/attrs">
	<AST:Attr_mod><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Attr_mod><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Name_with_default">
	<AST:Name_with_default><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Name_with_default><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Name_with_default_list">
	<AST:Name_with_default_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Name_with_default_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Name_with_default_list/AST:Name_with_default">
	<AST:Name_with_default_list><!--xsl: -->
		<AST:Name_with_default><!--xsl: -->
		    <xsl:apply-templates select="AST:Name_with_default"/>
		</AST:Name_with_default><!--xsl: -->
	</AST:Name_with_default_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Name_with_default/attrs">
	<AST:Name_with_default><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Name_with_default><!--xsl: -->
</xsl:template>
<xsl:template match="AST:VARIABLE_NAME">
	<AST:VARIABLE_NAME><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:VARIABLE_NAME><!--xsl: -->
</xsl:template>
<xsl:template match="AST:VARIABLE_NAME/attrs">
	<AST:VARIABLE_NAME><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:VARIABLE_NAME><!--xsl: -->
</xsl:template>
<xsl:template match="AST:STRING">
	<AST:STRING><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:STRING><!--xsl: -->
</xsl:template>
<xsl:template match="AST:STRING/attrs">
	<AST:STRING><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:STRING><!--xsl: -->
</xsl:template>
<xsl:template match="AST:INT">
	<AST:INT><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:INT><!--xsl: -->
</xsl:template>
<xsl:template match="AST:INT/attrs">
	<AST:INT><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:INT><!--xsl: -->
</xsl:template>
<xsl:template match="bool">
	<bool><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</bool><!--xsl: -->
</xsl:template>
<xsl:template match="attr/bool">
	<attr><!--xsl: -->
		<bool><!--xsl: -->
		    <xsl:apply-templates select="bool"/>
		</bool><!--xsl: -->
	</attr><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Method">
	<AST:Method><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Method><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Method/attrs">
	<AST:Method><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Method><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Signature">
	<AST:Signature><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Signature><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Signature/attrs">
	<AST:Signature><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Signature><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Method_mod">
	<AST:Method_mod><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Method_mod><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Method_mod/attrs">
	<AST:Method_mod><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Method_mod><!--xsl: -->
</xsl:template>
<xsl:template match="AST:METHOD_NAME">
	<AST:METHOD_NAME><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:METHOD_NAME><!--xsl: -->
</xsl:template>
<xsl:template match="AST:METHOD_NAME/attrs">
	<AST:METHOD_NAME><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:METHOD_NAME><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Formal_parameter">
	<AST:Formal_parameter><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Formal_parameter><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Formal_parameter_list">
	<AST:Formal_parameter_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Formal_parameter_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Formal_parameter_list/AST:Formal_parameter">
	<AST:Formal_parameter_list><!--xsl: -->
		<AST:Formal_parameter><!--xsl: -->
		    <xsl:apply-templates select="AST:Formal_parameter"/>
		</AST:Formal_parameter><!--xsl: -->
	</AST:Formal_parameter_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Formal_parameter/attrs">
	<AST:Formal_parameter><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Formal_parameter><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Type">
	<AST:Type><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Type><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Type/attrs">
	<AST:Type><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Type><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Array">
	<AST:Array><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Array><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Array/attrs">
	<AST:Array><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Array><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Eval_expr">
	<AST:Eval_expr><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Eval_expr><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Eval_expr">
	<AST:Statement_list><!--xsl: -->
		<AST:Eval_expr><!--xsl: -->
		    <xsl:apply-templates select="AST:Eval_expr"/>
		</AST:Eval_expr><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Eval_expr/attrs">
	<AST:Eval_expr><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Eval_expr><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Assignment">
	<AST:Assignment><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Assignment><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Assignment/attrs">
	<AST:Assignment><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Assignment><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Variable">
	<AST:Variable><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Variable><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Variable/attrs">
	<AST:Variable><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Variable><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Array_elem">
	<AST:Array_elem><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Array_elem><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Array_elem_list">
	<AST:Array_elem_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Array_elem_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Array_elem_list/AST:Array_elem">
	<AST:Array_elem_list><!--xsl: -->
		<AST:Array_elem><!--xsl: -->
		    <xsl:apply-templates select="AST:Array_elem"/>
		</AST:Array_elem><!--xsl: -->
	</AST:Array_elem_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Array_elem/attrs">
	<AST:Array_elem><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Array_elem><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Method_invocation">
	<AST:Method_invocation><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Method_invocation><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Method_invocation/attrs">
	<AST:Method_invocation><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Method_invocation><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Bin_op">
	<AST:Bin_op><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Bin_op><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Bin_op/attrs">
	<AST:Bin_op><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Bin_op><!--xsl: -->
</xsl:template>
<xsl:template match="AST:OP">
	<AST:OP><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:OP><!--xsl: -->
</xsl:template>
<xsl:template match="AST:OP/attrs">
	<AST:OP><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:OP><!--xsl: -->
</xsl:template>
<xsl:template match="AST:If">
	<AST:If><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:If><!--xsl: -->
</xsl:template>
<xsl:template match="AST:If/attrs">
	<AST:If><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:If><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Actual_parameter">
	<AST:Actual_parameter><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Actual_parameter><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Actual_parameter_list">
	<AST:Actual_parameter_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Actual_parameter_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Actual_parameter_list/AST:Actual_parameter">
	<AST:Actual_parameter_list><!--xsl: -->
		<AST:Actual_parameter><!--xsl: -->
		    <xsl:apply-templates select="AST:Actual_parameter"/>
		</AST:Actual_parameter><!--xsl: -->
	</AST:Actual_parameter_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Actual_parameter/attrs">
	<AST:Actual_parameter><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Actual_parameter><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Expr_list">
	<AST:Expr_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Expr_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Expr_list/AST:STRING">
	<AST:Expr_list><!--xsl: -->
		<AST:STRING><!--xsl: -->
		    <xsl:apply-templates select="AST:STRING"/>
		</AST:STRING><!--xsl: -->
	</AST:Expr_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:If">
	<AST:Statement_list><!--xsl: -->
		<AST:If><!--xsl: -->
		    <xsl:apply-templates select="AST:If"/>
		</AST:If><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:BOOL">
	<AST:BOOL><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:BOOL><!--xsl: -->
</xsl:template>
<xsl:template match="AST:BOOL/attrs">
	<AST:BOOL><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:BOOL><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Foreach">
	<AST:Foreach><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Foreach><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Foreach/attrs">
	<AST:Foreach><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Foreach><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Reflection">
	<AST:Reflection><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Reflection><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Reflection/attrs">
	<AST:Reflection><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Reflection><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Unary_op">
	<AST:Unary_op><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Unary_op><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Unary_op/attrs">
	<AST:Unary_op><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Unary_op><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Return">
	<AST:Return><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Return><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Return">
	<AST:Statement_list><!--xsl: -->
		<AST:Return><!--xsl: -->
		    <xsl:apply-templates select="AST:Return"/>
		</AST:Return><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Return/attrs">
	<AST:Return><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Return><!--xsl: -->
</xsl:template>
<xsl:template match="AST:New">
	<AST:New><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:New><!--xsl: -->
</xsl:template>
<xsl:template match="AST:New/attrs">
	<AST:New><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:New><!--xsl: -->
</xsl:template>
<xsl:template match="AST:NIL">
	<AST:NIL><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:NIL><!--xsl: -->
</xsl:template>
<xsl:template match="AST:NIL/attrs">
	<AST:NIL><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:NIL><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Constant">
	<AST:Constant><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Constant><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Constant/attrs">
	<AST:Constant><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Constant><!--xsl: -->
</xsl:template>
<xsl:template match="AST:CONSTANT_NAME">
	<AST:CONSTANT_NAME><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:CONSTANT_NAME><!--xsl: -->
</xsl:template>
<xsl:template match="AST:CONSTANT_NAME/attrs">
	<AST:CONSTANT_NAME><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:CONSTANT_NAME><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Instanceof">
	<AST:Instanceof><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Instanceof><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Instanceof/attrs">
	<AST:Instanceof><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Instanceof><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Nop">
	<AST:Nop><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Nop><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Nop/attrs">
	<AST:Nop><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Nop><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Global">
	<AST:Global><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Global><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Global">
	<AST:Statement_list><!--xsl: -->
		<AST:Global><!--xsl: -->
		    <xsl:apply-templates select="AST:Global"/>
		</AST:Global><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Global/attrs">
	<AST:Global><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Global><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Variable_name_list">
	<AST:Variable_name_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Variable_name_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Variable_name_list/AST:VARIABLE_NAME">
	<AST:Variable_name_list><!--xsl: -->
		<AST:VARIABLE_NAME><!--xsl: -->
		    <xsl:apply-templates select="AST:VARIABLE_NAME"/>
		</AST:VARIABLE_NAME><!--xsl: -->
	</AST:Variable_name_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:List_assignment">
	<AST:List_assignment><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:List_assignment><!--xsl: -->
</xsl:template>
<xsl:template match="AST:List_assignment/attrs">
	<AST:List_assignment><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:List_assignment><!--xsl: -->
</xsl:template>
<xsl:template match="AST:List_element_list">
	<AST:List_element_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:List_element_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:List_element_list/AST:Variable">
	<AST:List_element_list><!--xsl: -->
		<AST:Variable><!--xsl: -->
		    <xsl:apply-templates select="AST:Variable"/>
		</AST:Variable><!--xsl: -->
	</AST:List_element_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Expr_list/AST:Variable">
	<AST:Expr_list><!--xsl: -->
		<AST:Variable><!--xsl: -->
		    <xsl:apply-templates select="AST:Variable"/>
		</AST:Variable><!--xsl: -->
	</AST:Expr_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Op_assignment">
	<AST:Op_assignment><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Op_assignment><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Op_assignment/attrs">
	<AST:Op_assignment><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Op_assignment><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Post_op">
	<AST:Post_op><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Post_op><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Post_op/attrs">
	<AST:Post_op><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Post_op><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Conditional_expr">
	<AST:Conditional_expr><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Conditional_expr><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Conditional_expr/attrs">
	<AST:Conditional_expr><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Conditional_expr><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Static_declaration">
	<AST:Static_declaration><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Static_declaration><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Static_declaration">
	<AST:Statement_list><!--xsl: -->
		<AST:Static_declaration><!--xsl: -->
		    <xsl:apply-templates select="AST:Static_declaration"/>
		</AST:Static_declaration><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Static_declaration/attrs">
	<AST:Static_declaration><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Static_declaration><!--xsl: -->
</xsl:template>
<xsl:template match="AST:While">
	<AST:While><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:While><!--xsl: -->
</xsl:template>
<xsl:template match="AST:While/attrs">
	<AST:While><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:While><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Expr_list/AST:INT">
	<AST:Expr_list><!--xsl: -->
		<AST:INT><!--xsl: -->
		    <xsl:apply-templates select="AST:INT"/>
		</AST:INT><!--xsl: -->
	</AST:Expr_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Break">
	<AST:Break><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Break><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Break">
	<AST:Statement_list><!--xsl: -->
		<AST:Break><!--xsl: -->
		    <xsl:apply-templates select="AST:Break"/>
		</AST:Break><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Break/attrs">
	<AST:Break><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Break><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Expr">
	<AST:Expr><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Expr><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Expr_list/AST:Expr">
	<AST:Expr_list><!--xsl: -->
		<AST:Expr><!--xsl: -->
		    <xsl:apply-templates select="AST:Expr"/>
		</AST:Expr><!--xsl: -->
	</AST:Expr_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Continue">
	<AST:Continue><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Continue><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Continue/attrs">
	<AST:Continue><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Continue><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Throw">
	<AST:Throw><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Throw><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Throw">
	<AST:Statement_list><!--xsl: -->
		<AST:Throw><!--xsl: -->
		    <xsl:apply-templates select="AST:Throw"/>
		</AST:Throw><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Throw/attrs">
	<AST:Throw><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Throw><!--xsl: -->
</xsl:template>
<xsl:template match="AST:For">
	<AST:For><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:For><!--xsl: -->
</xsl:template>
<xsl:template match="AST:For/attrs">
	<AST:For><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:For><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Pre_op">
	<AST:Pre_op><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Pre_op><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Pre_op/attrs">
	<AST:Pre_op><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Pre_op><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Expr_list/AST:Bin_op">
	<AST:Expr_list><!--xsl: -->
		<AST:Bin_op><!--xsl: -->
		    <xsl:apply-templates select="AST:Bin_op"/>
		</AST:Bin_op><!--xsl: -->
	</AST:Expr_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Expr_list/AST:Post_op">
	<AST:Expr_list><!--xsl: -->
		<AST:Post_op><!--xsl: -->
		    <xsl:apply-templates select="AST:Post_op"/>
		</AST:Post_op><!--xsl: -->
	</AST:Expr_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Foreach">
	<AST:Statement_list><!--xsl: -->
		<AST:Foreach><!--xsl: -->
		    <xsl:apply-templates select="AST:Foreach"/>
		</AST:Foreach><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Continue">
	<AST:Statement_list><!--xsl: -->
		<AST:Continue><!--xsl: -->
		    <xsl:apply-templates select="AST:Continue"/>
		</AST:Continue><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Cast">
	<AST:Cast><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Cast><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Cast/attrs">
	<AST:Cast><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Cast><!--xsl: -->
</xsl:template>
<xsl:template match="AST:CAST">
	<AST:CAST><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:CAST><!--xsl: -->
</xsl:template>
<xsl:template match="AST:CAST/attrs">
	<AST:CAST><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:CAST><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Switch">
	<AST:Switch><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Switch><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Switch/attrs">
	<AST:Switch><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Switch><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Switch_case">
	<AST:Switch_case><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Switch_case><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Switch_case_list">
	<AST:Switch_case_list><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Switch_case_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Switch_case_list/AST:Switch_case">
	<AST:Switch_case_list><!--xsl: -->
		<AST:Switch_case><!--xsl: -->
		    <xsl:apply-templates select="AST:Switch_case"/>
		</AST:Switch_case><!--xsl: -->
	</AST:Switch_case_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Switch_case/attrs">
	<AST:Switch_case><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Switch_case><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Switch">
	<AST:Statement_list><!--xsl: -->
		<AST:Switch><!--xsl: -->
		    <xsl:apply-templates select="AST:Switch"/>
		</AST:Switch><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:For">
	<AST:Statement_list><!--xsl: -->
		<AST:For><!--xsl: -->
		    <xsl:apply-templates select="AST:For"/>
		</AST:For><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Expr_list/AST:Method_invocation">
	<AST:Expr_list><!--xsl: -->
		<AST:Method_invocation><!--xsl: -->
		    <xsl:apply-templates select="AST:Method_invocation"/>
		</AST:Method_invocation><!--xsl: -->
	</AST:Expr_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Ignore_errors">
	<AST:Ignore_errors><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Ignore_errors><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Ignore_errors/attrs">
	<AST:Ignore_errors><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Ignore_errors><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:Nop">
	<AST:Statement_list><!--xsl: -->
		<AST:Nop><!--xsl: -->
		    <xsl:apply-templates select="AST:Nop"/>
		</AST:Nop><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Statement_list/AST:While">
	<AST:Statement_list><!--xsl: -->
		<AST:While><!--xsl: -->
		    <xsl:apply-templates select="AST:While"/>
		</AST:While><!--xsl: -->
	</AST:Statement_list><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Do">
	<AST:Do><!--xsl: -->
		<xsl:apply-templates select="./*"/>
	</AST:Do><!--xsl: -->
</xsl:template>
<xsl:template match="AST:Do/attrs">
	<AST:Do><!--xsl: -->
		<attrs><!--xsl: -->
		    <xsl:apply-templates select="attrs"/>
		</attrs><!--xsl: -->
	</AST:Do><!--xsl: -->
</xsl:template>
</xsl:stylesheet>
