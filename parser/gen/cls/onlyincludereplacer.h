
#ifndef __GENERATED_cls_onlyincludereplacer_h__
#define __GENERATED_cls_onlyincludereplacer_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 40482 */
class c_onlyincludereplacer : virtual public ObjectData {
  BEGIN_CLASS_MAP(onlyincludereplacer)
  END_CLASS_MAP(onlyincludereplacer)
  DECLARE_CLASS(onlyincludereplacer, OnlyIncludeReplacer, ObjectData)
  void init();
  public: Variant m_output;
  public: void t_replace(CVarRef v_matches);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_onlyincludereplacer_h__
