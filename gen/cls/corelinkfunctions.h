
#ifndef __GENERATED_cls_corelinkfunctions_h__
#define __GENERATED_cls_corelinkfunctions_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: CoreLinkFunctions.php line 7 */
class c_corelinkfunctions : virtual public ObjectData {
  BEGIN_CLASS_MAP(corelinkfunctions)
  END_CLASS_MAP(corelinkfunctions)
  DECLARE_CLASS(corelinkfunctions, CoreLinkFunctions, ObjectData)
  void init();
  public: static bool ti_register(const char* cls, p_parser_linkhooks v_parser);
  public: static Variant ti_defaultlinkhook(const char* cls, CVarRef v_parser, CVarRef v_holders, CVarRef v_markers, Variant v_title, CVarRef v_titleText, Variant v_displayText = null, Variant v_leadingColon = false);
  public: static Variant ti_categorylinkhook(const char* cls, CVarRef v_parser, CVarRef v_holders, CVarRef v_markers, Object v_title, CVarRef v_titleText, Variant v_sortText = null, Variant v_leadingColon = false);
  public: static Variant t_defaultlinkhook(CVarRef v_parser, CVarRef v_holders, CVarRef v_markers, CVarRef v_title, CVarRef v_titleText, CVarRef v_displayText = null_variant, CVarRef v_leadingColon = false) { return ti_defaultlinkhook("corelinkfunctions", v_parser, v_holders, v_markers, v_title, v_titleText, v_displayText, v_leadingColon); }
  public: static bool t_register(p_parser_linkhooks v_parser) { return ti_register("corelinkfunctions", v_parser); }
  public: static Variant t_categorylinkhook(CVarRef v_parser, CVarRef v_holders, CVarRef v_markers, Object v_title, CVarRef v_titleText, CVarRef v_sortText = null_variant, CVarRef v_leadingColon = false) { return ti_categorylinkhook("corelinkfunctions", v_parser, v_holders, v_markers, v_title, v_titleText, v_sortText, v_leadingColon); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_corelinkfunctions_h__
