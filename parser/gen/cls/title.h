
#ifndef __GENERATED_cls_title_h__
#define __GENERATED_cls_title_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 25659 */
class c_title : virtual public ObjectData {
  BEGIN_CLASS_MAP(title)
  END_CLASS_MAP(title)
  DECLARE_CLASS(title, Title, ObjectData)
  void init();
  public: Variant m_mTextform;
  public: Variant m_mUrlform;
  public: Variant m_mDbkeyform;
  public: Variant m_mUserCaseDBKey;
  public: Variant m_mNamespace;
  public: Variant m_mInterwiki;
  public: Variant m_mFragment;
  public: Variant m_mArticleID;
  public: Variant m_mLatestID;
  public: Variant m_mRestrictions;
  public: Variant m_mOldRestrictions;
  public: Variant m_mCascadeRestriction;
  public: Variant m_mRestrictionsExpiry;
  public: Variant m_mHasCascadingRestrictions;
  public: Variant m_mCascadeSources;
  public: Variant m_mRestrictionsLoaded;
  public: Variant m_mPrefixedText;
  public: Variant m_mDefaultNamespace;
  public: Variant m_mWatched;
  public: Variant m_mLength;
  public: Variant m_mRedirect;
  public: Variant m_mNotificationTimestamp;
  public: Variant m_mBacklinkCache;
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static Variant ti_newfromdbkey(const char* cls, CVarRef v_key);
  public: static Variant ti_newfromtext(const char* cls, CVarRef v_text, CVarRef v_defaultNamespace = 0LL /* NS_MAIN */);
  public: static Variant ti_newfromurl(const char* cls, Variant v_url);
  public: static Variant ti_newfromid(const char* cls, CVarRef v_id, CVarRef v_flags = 0LL);
  public: static Array ti_newfromids(const char* cls, Variant v_ids);
  public: static Variant ti_newfromrow(const char* cls, CVarRef v_row);
  public: static Variant ti_maketitle(const char* cls, Variant v_ns, CVarRef v_title, CStrRef v_fragment = "");
  public: static Variant ti_maketitlesafe(const char* cls, CVarRef v_ns, CVarRef v_title, CVarRef v_fragment = "");
  public: static Variant ti_newmainpage(const char* cls);
  public: static Variant ti_newfromredirect(const char* cls, CVarRef v_text);
  public: static Variant ti_newfromredirectrecurse(const char* cls, CVarRef v_text);
  public: static Variant ti_newfromredirectarray(const char* cls, CVarRef v_text);
  public: static Variant ti_newfromredirectinternal(const char* cls, Variant v_text);
  public: static Variant ti_nameof(const char* cls, CVarRef v_id);
  public: static Variant ti_legalchars(const char* cls);
  public: static String ti_indextitle(const char* cls, CVarRef v_ns, Variant v_title);
  public: static Variant ti_makename(const char* cls, Variant v_ns, CVarRef v_title, CVarRef v_fragment = "");
  public: Variant t_getinterwikilink(Variant v_key);
  public: Variant t_islocal();
  public: Variant t_istrans();
  public: static Variant ti_escapefragmentforurl(const char* cls, CVarRef v_fragment);
  public: Variant t_gettext();
  public: Variant t_getpartialurl();
  public: Variant t_getdbkey();
  public: Variant t_getnamespace();
  public: Variant t_getnstext();
  public: Variant t_getusercasedbkey();
  public: Variant t_getsubjectnstext();
  public: Variant t_gettalknstext();
  public: bool t_cantalk();
  public: Variant t_getinterwiki();
  public: Variant t_getfragment();
  public: Variant t_getfragmentforurl();
  public: Variant t_getdefaultnamespace();
  public: String t_getindextitle();
  public: Variant t_getprefixeddbkey();
  public: Variant t_getprefixedtext();
  public: Variant t_getfulltext();
  public: Variant t_getbasetext();
  public: Variant t_getsubpagetext();
  public: Variant t_getsubpageurlform();
  public: Variant t_getprefixedurl();
  public: Variant t_getfullurl(Variant v_query = "", CVarRef v_variant = false);
  public: Variant t_getlocalurl(Variant v_query = "", Variant v_variant = false);
  public: Variant t_getlinkurl(CVarRef v_query = ScalarArrays::sa_[0], CVarRef v_variant = false);
  public: String t_escapelocalurl(CVarRef v_query = "");
  public: String t_escapefullurl(CVarRef v_query = "");
  public: Variant t_getinternalurl(CVarRef v_query = "", CVarRef v_variant = false);
  public: Variant t_getediturl();
  public: String t_getescapedtext();
  public: bool t_isexternal();
  public: Variant t_issemiprotected(CStrRef v_action = "edit");
  public: bool t_isprotected(CVarRef v_action = "");
  public: Variant t_useriswatching();
  public: bool t_quickusercan(CVarRef v_action);
  public: bool t_isnamespaceprotected();
  public: bool t_usercan(CVarRef v_action, CVarRef v_doExpensiveQueries = true);
  public: Variant t_getuserpermissionserrors(CVarRef v_action, Variant v_user, CVarRef v_doExpensiveQueries = true, CVarRef v_ignoreErrors = ScalarArrays::sa_[0]);
  public: Variant t_getuserpermissionserrorsinternal(Variant v_action, Variant v_user, Variant v_doExpensiveQueries = true, Variant v_short = false);
  public: bool t_gettitleprotection();
  public: bool t_updatetitleprotection(CVarRef v_create_perm, Variant v_reason, Variant v_expiry);
  public: void t_deletetitleprotection();
  public: bool t_usercanedit(bool v_doExpensiveQueries = true);
  public: bool t_usercancreate(bool v_doExpensiveQueries = true);
  public: bool t_usercanmove(bool v_doExpensiveQueries = true);
  public: bool t_ismovable();
  public: Variant t_usercanread();
  public: bool t_istalkpage();
  public: bool t_issubpage();
  public: Variant t_hassubpages();
  public: Variant t_getsubpages(Numeric v_limit = -1LL);
  public: bool t_iscssorjspage();
  public: bool t_iscssjssubpage();
  public: Variant t_isvalidcssjssubpage();
  public: Variant t_getskinfromcssjssubpage();
  public: bool t_iscsssubpage();
  public: bool t_isjssubpage();
  public: bool t_usercaneditcssjssubpage();
  public: bool t_iscascadeprotected();
  public: Array t_getcascadeprotectionsources(bool v_get_pages = true);
  public: Variant t_arerestrictionscascading();
  public: void t_loadrestrictionsfromrow(CVarRef v_res, Variant v_oldFashionedRestrictions = null);
  public: void t_loadrestrictions(Variant v_oldFashionedRestrictions = null);
  public: static void ti_purgeexpiredrestrictions(const char* cls);
  public: Variant t_getrestrictions(CVarRef v_action);
  public: Variant t_getrestrictionexpiry(CVarRef v_action);
  public: int64 t_isdeleted();
  public: bool t_isdeletedquick();
  public: Variant t_getarticleid(CVarRef v_flags = 0LL);
  public: Variant t_isredirect(CVarRef v_flags = 0LL);
  public: Variant t_getlength(CVarRef v_flags = 0LL);
  public: Variant t_getlatestrevid(int64 v_flags = 0LL);
  public: void t_resetarticleid(CVarRef v_newid);
  public: p_getdb t_invalidatecache();
  public: String t_prefix(CVarRef v_name);
  public: bool t_secureandsplit();
  public: void t_setfragment(CVarRef v_fragment);
  public: Variant t_gettalkpage();
  public: Variant t_getsubjectpage();
  public: Array t_getlinksto(CArrRef v_options = ScalarArrays::sa_[0], CStrRef v_table = "pagelinks", CStrRef v_prefix = "pl");
  public: Array t_gettemplatelinksto(CArrRef v_options = ScalarArrays::sa_[0]);
  public: Array t_getbrokenlinksfrom();
  public: Array t_getsquidurls();
  public: void t_purgesquid();
  public: Variant t_movenoauth(Variant v_nt);
  public: Variant t_isvalidmoveoperation(Variant v_nt, CVarRef v_auth = true, CVarRef v_reason = "");
  public: Variant t_moveto(Variant v_nt, CVarRef v_auth = true, CVarRef v_reason = "", CVarRef v_createRedirect = true);
  public: Variant t_moveoverexistingredirect(Variant v_nt, Variant v_reason = "", CVarRef v_createRedirect = true);
  public: Variant t_movetonewtitle(Variant v_nt, Variant v_reason = "", CVarRef v_createRedirect = true);
  public: Variant t_movesubpages(Object v_nt, Variant v_auth = true, Variant v_reason = "", Variant v_createRedirect = true);
  public: bool t_issinglerevredirect();
  public: bool t_isvalidmovetarget(CVarRef v_nt);
  public: bool t_iswatchable();
  public: Variant t_getparentcategories();
  public: Variant t_getparentcategorytree(CVarRef v_children = ScalarArrays::sa_[0]);
  public: Variant t_pagecond();
  public: Variant t_getpreviousrevisionid(CVarRef v_revId, CVarRef v_flags = 0LL);
  public: Variant t_getnextrevisionid(CVarRef v_revId, CVarRef v_flags = 0LL);
  public: Variant t_getfirstrevision(int64 v_flags = 0LL);
  public: bool t_isnewpage();
  public: Variant t_getearliestrevtime();
  public: Variant t_countrevisionsbetween(CVarRef v_old, CVarRef v_new);
  public: bool t_equals(CVarRef v_title);
  public: static Variant ti_compare(const char* cls, CVarRef v_a, CVarRef v_b);
  public: String t___tostring();
  public: bool t_exists();
  public: Variant t_isalwaysknown();
  public: bool t_isknown();
  public: bool t_canexist();
  public: void t_touchlinks();
  public: Variant t_gettouched(Variant v_db = null);
  public: Variant t_getnotificationtimestamp(Variant v_user = null);
  public: String t_trackbackurl();
  public: String t_trackbackrdf();
  public: Variant t_getnamespacekey();
  public: bool t_isspecial(CVarRef v_name);
  public: Variant t_fixspecialname();
  public: bool t_iscontentpage();
  public: Array t_getredirectshere(CVarRef v_ns = null_variant);
  public: bool t_isvalidredirecttarget();
  public: Variant t_getbacklinkcache();
  public: static Variant t_newfromredirectinternal(CVarRef v_text) { return ti_newfromredirectinternal("title", v_text); }
  public: static String t_indextitle(CVarRef v_ns, CVarRef v_title) { return ti_indextitle("title", v_ns, v_title); }
  public: static Variant t_newfromurl(CVarRef v_url) { return ti_newfromurl("title", v_url); }
  public: static Variant t_newfromid(CVarRef v_id, CVarRef v_flags = 0LL) { return ti_newfromid("title", v_id, v_flags); }
  public: static Variant t_makename(CVarRef v_ns, CVarRef v_title, CVarRef v_fragment = "") { return ti_makename("title", v_ns, v_title, v_fragment); }
  public: static Variant t_legalchars() { return ti_legalchars("title"); }
  public: static Array t_newfromids(CVarRef v_ids) { return ti_newfromids("title", v_ids); }
  public: static Variant t_escapefragmentforurl(CVarRef v_fragment) { return ti_escapefragmentforurl("title", v_fragment); }
  public: static Variant t_nameof(CVarRef v_id) { return ti_nameof("title", v_id); }
  public: static Variant t_maketitlesafe(CVarRef v_ns, CVarRef v_title, CVarRef v_fragment = "") { return ti_maketitlesafe("title", v_ns, v_title, v_fragment); }
  public: static Variant t_newfromredirect(CVarRef v_text) { return ti_newfromredirect("title", v_text); }
  public: static Variant t_newfromredirectrecurse(CVarRef v_text) { return ti_newfromredirectrecurse("title", v_text); }
  public: static Variant t_newmainpage() { return ti_newmainpage("title"); }
  public: static Variant t_newfromrow(CVarRef v_row) { return ti_newfromrow("title", v_row); }
  public: static Variant t_compare(CVarRef v_a, CVarRef v_b) { return ti_compare("title", v_a, v_b); }
  public: static void t_purgeexpiredrestrictions() { ti_purgeexpiredrestrictions("title"); }
  public: static Variant t_newfromredirectarray(CVarRef v_text) { return ti_newfromredirectarray("title", v_text); }
  public: static Variant t_maketitle(CVarRef v_ns, CVarRef v_title, CStrRef v_fragment = "") { return ti_maketitle("title", v_ns, v_title, v_fragment); }
  public: static Variant t_newfromtext(CVarRef v_text, CVarRef v_defaultNamespace = 0LL /* NS_MAIN */) { return ti_newfromtext("title", v_text, v_defaultNamespace); }
  public: static Variant t_newfromdbkey(CVarRef v_key) { return ti_newfromdbkey("title", v_key); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_title_h__
