
#ifndef __GENERATED_cls_dummytermcolorer_h__
#define __GENERATED_cls_dummytermcolorer_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 44706 */
class c_dummytermcolorer : virtual public ObjectData {
  BEGIN_CLASS_MAP(dummytermcolorer)
  END_CLASS_MAP(dummytermcolorer)
  DECLARE_CLASS(dummytermcolorer, DummyTermColorer, ObjectData)
  void init();
  public: String t_color(CVarRef v_color);
  public: String t_reset();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_dummytermcolorer_h__
