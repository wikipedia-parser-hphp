
#include <php/CoreLinkFunctions.h>
#include <php/Parser_LinkHooks.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: CoreLinkFunctions.php line 7 */
Variant c_corelinkfunctions::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_corelinkfunctions::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_corelinkfunctions::o_get(ArrayElementVec &props) const {
  c_ObjectData::o_get(props);
}
bool c_corelinkfunctions::o_exists(CStrRef s, int64 hash) const {
  return c_ObjectData::o_exists(s, hash);
}
Variant c_corelinkfunctions::o_get(CStrRef s, int64 hash) {
  return c_ObjectData::o_get(s, hash);
}
Variant c_corelinkfunctions::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_corelinkfunctions::o_lval(CStrRef s, int64 hash) {
  return c_ObjectData::o_lval(s, hash);
}
Variant c_corelinkfunctions::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(corelinkfunctions)
ObjectData *c_corelinkfunctions::cloneImpl() {
  c_corelinkfunctions *obj = NEW(c_corelinkfunctions)();
  cloneSet(obj);
  return obj;
}
void c_corelinkfunctions::cloneSet(c_corelinkfunctions *clone) {
  ObjectData::cloneSet(clone);
}
Variant c_corelinkfunctions::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x1D738EB8C393D900LL, defaultlinkhook) {
        int count = params.size();
        if (count <= 5) return (ti_defaultlinkhook(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
        if (count == 6) return (ti_defaultlinkhook(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), ref(const_cast<Array&>(params).lvalAt(5))));
        return (ti_defaultlinkhook(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), ref(const_cast<Array&>(params).lvalAt(5)), ref(const_cast<Array&>(params).lvalAt(6))));
      }
      break;
    case 2:
      HASH_GUARD(0x7C88307983EF6A96LL, categorylinkhook) {
        int count = params.size();
        if (count <= 5) return (ti_categorylinkhook(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
        if (count == 6) return (ti_categorylinkhook(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), ref(const_cast<Array&>(params).lvalAt(5))));
        return (ti_categorylinkhook(o_getClassName(), params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), ref(const_cast<Array&>(params).lvalAt(5)), ref(const_cast<Array&>(params).lvalAt(6))));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_corelinkfunctions::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x1D738EB8C393D900LL, defaultlinkhook) {
        if (count <= 5) return (ti_defaultlinkhook(o_getClassName(), a0, a1, a2, a3, a4));
        return (ti_defaultlinkhook(o_getClassName(), a0, a1, a2, a3, a4, ref(a5)));
      }
      break;
    case 2:
      HASH_GUARD(0x7C88307983EF6A96LL, categorylinkhook) {
        if (count <= 5) return (ti_categorylinkhook(o_getClassName(), a0, a1, a2, a3, a4));
        return (ti_categorylinkhook(o_getClassName(), a0, a1, a2, a3, a4, ref(a5)));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_corelinkfunctions::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 3) {
    case 0:
      HASH_GUARD(0x1D738EB8C393D900LL, defaultlinkhook) {
        int count = params.size();
        if (count <= 5) return (ti_defaultlinkhook(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
        if (count == 6) return (ti_defaultlinkhook(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), ref(const_cast<Array&>(params).lvalAt(5))));
        return (ti_defaultlinkhook(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), ref(const_cast<Array&>(params).lvalAt(5)), ref(const_cast<Array&>(params).lvalAt(6))));
      }
      break;
    case 2:
      HASH_GUARD(0x7C88307983EF6A96LL, categorylinkhook) {
        int count = params.size();
        if (count <= 5) return (ti_categorylinkhook(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
        if (count == 6) return (ti_categorylinkhook(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), ref(const_cast<Array&>(params).lvalAt(5))));
        return (ti_categorylinkhook(c, params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), ref(const_cast<Array&>(params).lvalAt(5)), ref(const_cast<Array&>(params).lvalAt(6))));
      }
      break;
    default:
      break;
  }
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_corelinkfunctions$os_get(const char *s) {
  return c_corelinkfunctions::os_get(s, -1);
}
Variant &cw_corelinkfunctions$os_lval(const char *s) {
  return c_corelinkfunctions::os_lval(s, -1);
}
Variant cw_corelinkfunctions$os_constant(const char *s) {
  return c_corelinkfunctions::os_constant(s);
}
Variant cw_corelinkfunctions$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_corelinkfunctions::os_invoke(c, s, params, -1, fatal);
}
void c_corelinkfunctions::init() {
}
/* SRC: CoreLinkFunctions.php line 8 */
bool c_corelinkfunctions::ti_register(const char* cls, p_parser_linkhooks v_parser) {
  STATIC_METHOD_INJECTION(CoreLinkFunctions, CoreLinkFunctions::register);
  LINE(9,v_parser->t_setlinkhook(k_NS_CATEGORY, ScalarArrays::sa_[32]));
  return true;
} /* function */
/* SRC: CoreLinkFunctions.php line 13 */
Variant c_corelinkfunctions::ti_defaultlinkhook(const char* cls, CVarRef v_parser, CVarRef v_holders, CVarRef v_markers, Variant v_title, CVarRef v_titleText, Variant v_displayText //  = null
, Variant v_leadingColon //  = false
) {
  STATIC_METHOD_INJECTION(CoreLinkFunctions, CoreLinkFunctions::defaultLinkHook);
  if (isset(v_displayText) && toBoolean(LINE(15,toObject(v_markers)->o_invoke_few_args("findMarker", 0x21C2BB2267C07FDALL, 1, v_displayText)))) {
    (v_displayText = LINE(19,toObject(v_markers)->o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_displayText)));
    return false;
  }
  return LINE(23,toObject(v_holders)->o_invoke_few_args("makeHolder", 0x0B6C33D73053B0C5LL, 5, v_title, isset(v_displayText) ? ((Variant)(v_displayText)) : ((Variant)(v_titleText)), "", "", ""));
} /* function */
/* SRC: CoreLinkFunctions.php line 26 */
Variant c_corelinkfunctions::ti_categorylinkhook(const char* cls, CVarRef v_parser, CVarRef v_holders, CVarRef v_markers, Object v_title, CVarRef v_titleText, Variant v_sortText //  = null
, Variant v_leadingColon //  = false
) {
  STATIC_METHOD_INJECTION(CoreLinkFunctions, CoreLinkFunctions::categoryLinkHook);
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  if (toBoolean(v_leadingColon)) return true;
  if (isset(v_sortText) && toBoolean(LINE(31,toObject(v_markers)->o_invoke_few_args("findMarker", 0x21C2BB2267C07FDALL, 1, v_sortText)))) {
    (v_sortText = LINE(35,toObject(v_markers)->o_invoke_few_args("expand", 0x39B7BB05F05A37CDLL, 1, v_sortText)));
    return false;
  }
  if (!(isset(v_sortText))) (v_sortText = LINE(39,toObject(v_parser)->o_invoke_few_args("getDefaultSort", 0x39ADF3C1E6A55D3ALL, 0)));
  (v_sortText = LINE(40,throw_fatal("unknown class sanitizer", ((void*)NULL))));
  (v_sortText = LINE(41,x_str_replace("\n", "", v_sortText)));
  (v_sortText = LINE(42,gv_wgContLang.o_invoke_few_args("convertCategoryKey", 0x11AD19C1DBDF452ALL, 1, v_sortText)));
  (assignCallTemp(eo_0, ref(LINE(43,v_title->o_invoke_few_args("getDBkey", 0x7A72F137192AEF00LL, 0)))),assignCallTemp(eo_1, ref(v_sortText)),toObject(v_parser).o_get("mOutput", 0x7F7FE020D521DBD0LL).o_invoke("addCategory", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x6DBAA9816B039793LL));
  return "";
} /* function */
Object co_corelinkfunctions(CArrRef params, bool init /* = true */) {
  return Object(p_corelinkfunctions(NEW(c_corelinkfunctions)())->dynCreate(params, init));
}
Variant pm_php$CoreLinkFunctions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::CoreLinkFunctions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$CoreLinkFunctions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
