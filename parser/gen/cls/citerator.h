
#ifndef __GENERATED_cls_citerator_h__
#define __GENERATED_cls_citerator_h__

#include <cls/ctraversable.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 40333 */
class c_citerator : virtual public c_ctraversable {
  BEGIN_CLASS_MAP(citerator)
    PARENT_CLASS(ctraversable)
  END_CLASS_MAP(citerator)
  DECLARE_CLASS(citerator, CIterator, ctraversable)
  void init();
  public: void t_rewind();
  public: void t_current();
  public: void t_key();
  public: void t_next();
  public: void t_valid();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_citerator_h__
