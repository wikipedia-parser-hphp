
#ifndef __GENERATED_php_CoreParserFunctions_h__
#define __GENERATED_php_CoreParserFunctions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/CoreParserFunctions.fw.h>

// Declarations
#include <cls/coreparserfunctions.h>
#include <php/DateFormatter.h>
#include <php/Parser.h>
#include <php/Preprocessor.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$CoreParserFunctions_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_coreparserfunctions(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_CoreParserFunctions_h__
