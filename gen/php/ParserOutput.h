
#ifndef __GENERATED_php_ParserOutput_h__
#define __GENERATED_php_ParserOutput_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/ParserOutput.fw.h>

// Declarations
#include <cls/parseroutput.h>
#include <php/Parser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$ParserOutput_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_parseroutput(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_ParserOutput_h__
