#include "StandardHeader.h"

/**
	protected function formatLinksInCommentCallback( $match ) {
		global $wgContLang;

		$medians = '(?:' . preg_quote( MWNamespace::getCanonicalName( NS_MEDIA ), '/' ) . '|';
		$medians .= preg_quote( $wgContLang->getNsText( NS_MEDIA ), '/' ) . '):';

		$comment = $match[0];

		# fix up urlencoded title texts (copied from Parser::replaceInternalLinks)
		if( strpos( $match[1], '%' ) !== false ) {
			$match[1] = str_replace( array('<', '>'), array('&lt;', '&gt;'), urldecode($match[1]) );
		}

		# Handle link renaming [[foo|text]] will show link as "text"
		if( "" != $match[3] ) {
			$text = $match[3];
		} else {
			$text = $match[1];
		}
		$submatch = array();
		if( preg_match( '/^' . $medians . '(.*)$/i', $match[1], $submatch ) ) {
			# Media link; trail not supported.
			$linkRegexp = '/\[\[(.*?)\]\]/';
			$thelink = $this->makeMediaLink( $submatch[1], "", $text );
		} else {
			# Other kind of link
			if( preg_match( $wgContLang->linkTrail(), $match[4], $submatch ) ) {
				$trail = $submatch[1];
			} else {
				$trail = "";
			}
			$linkRegexp = '/\[\[(.*?)\]\]' . preg_quote( $trail, '/' ) . '/';
			if (isset($match[1][0]) && $match[1][0] == ':')
				$match[1] = substr($match[1], 1);
			$thelink = $this->makeLink( $match[1], $text, "", $trail );
		}
		$comment = preg_replace( $linkRegexp, StringUtils::escapeRegexReplacement( $thelink ), $comment, 1 );

		return $comment;
	}

*/
// protected function formatlinksincommentcallback($match)
// {
// 	global $wgContLang;
// 	$TLE4128 = '(?:';
// 	$TLE4129 = NS_MEDIA;
// 	$TLE4130 = mwnamespace::getcanonicalname($TLE4129);
// 	$TLE4131 = '/';
// 	$TLE4132 = preg_quote($TLE4130, $TLE4131);
// 	$TLE4133 = ($TLE4128 . $TLE4132);
// 	$TLE4134 = '|';
// 	$medians = ($TLE4133 . $TLE4134);
// 	$TLE4135 = NS_MEDIA;
// 	$TLE4136 = $wgContLang->getnstext($TLE4135);
// 	$TLE4137 = '/';
// 	$TLE4138 = preg_quote($TLE4136, $TLE4137);
// 	$TLE4139 = '):';
// 	$TLE4140 = ($TLE4138 . $TLE4139);
// 	$medians = ($medians . $TLE4140);
// 	$TLE4141 = 0;
// 	$TSi4142 = $match[$TLE4141];
// 	$comment = $TSi4142;
// 	$TLE4143 = 1;
// 	$TLE4144 = '%';
// 	$TLE33469 = param_is_ref (NULL, "strpos", 0);
// 	;
// 	if (TLE33469) goto L36614 else goto L36615;
// L36614:
// 	$TMIi33468 =& $match[$TLE4143];
// 	goto L36616;
// L36615:
// 	$TMIi33468 = $match[$TLE4143];
// 	goto L36616;
// L36616:
// 	$TLE4145 = strpos($TMIi33468, $TLE4144);
// 	$TLE4146 = False;
// 	$TLE4147 = ($TLE4145 !== $TLE4146);
// 	if (TLE4147) goto L36620 else goto L36621;
// L36620:
// 	$TLE4148 = 1;
// 	$TLE4149 = '<';
// 	$TLE4150 = '>';
// 	unset($TSa4151);
// 	$TSa4151 = (array) $TSa4151;
// 	$TSa4151[] = $TLE4149;
// 	$TSa4151[] = $TLE4150;
// 	$TLE4152 = '&lt;';
// 	$TLE4153 = '&gt;';
// 	unset($TSa4154);
// 	$TSa4154 = (array) $TSa4154;
// 	$TSa4154[] = $TLE4152;
// 	$TSa4154[] = $TLE4153;
// 	$TLE4155 = 1;
// 	$TLE33471 = param_is_ref (NULL, "urldecode", 0);
// 	;
// 	if (TLE33471) goto L36617 else goto L36618;
// L36617:
// 	$TMIi33470 =& $match[$TLE4155];
// 	goto L36619;
// L36618:
// 	$TMIi33470 = $match[$TLE4155];
// 	goto L36619;
// L36619:
// 	$TLE4156 = urldecode($TMIi33470);
// 	$TLE4157 = str_replace($TSa4151, $TSa4154, $TLE4156);
// 	$match[$TLE4148] = $TLE4157;
// 	goto L36622;
// L36621:
// 	goto L36622;
// L36622:
// 	$TLE4158 = '';
// 	$TLE4159 = 3;
// 	$TSi4160 = $match[$TLE4159];
// 	$TLE4161 = ($TLE4158 != $TSi4160);
// 	if (TLE4161) goto L36623 else goto L36624;
// L36623:
// 	$TLE4162 = 3;
// 	$TSi4163 = $match[$TLE4162];
// 	$text = $TSi4163;
// 	goto L36625;
// L36624:
// 	$TLE4164 = 1;
// 	$TSi4165 = $match[$TLE4164];
// 	$text = $TSi4165;
// 	goto L36625;
// L36625:
// 	unset($TSa4166);
// 	$TSa4166 = (array) $TSa4166;
// 	$submatch = $TSa4166;
// 	$TLE4167 = '/^';
// 	$TLE4168 = ($TLE4167 . $medians);
// 	$TLE4169 = '(.*)$/i';
// 	$TLE4170 = ($TLE4168 . $TLE4169);
// 	$TLE4171 = 1;
// 	$TLE33473 = param_is_ref (NULL, "preg_match", 0);
// 	;
// 	if (TLE33473) goto L36626 else goto L36627;
// L36626:
// 	$TMIi33472 =& $match[$TLE4171];
// 	goto L36628;
// L36627:
// 	$TMIi33472 = $match[$TLE4171];
// 	goto L36628;
// L36628:
// 	$TLE4172 = preg_match($TLE4170, $TMIi33472, $submatch);
// 	if (TLE4172) goto L36650 else goto L36651;
// L36650:
// 	$linkRegexp = '/\\[\\[(.*?)\\]\\]/';
// 	$TLE4173 = 1;
// 	$TLE4174 = '';
// 	$TLE33475 = param_is_ref (this, "makemedialink", 0);
// 	;
// 	if (TLE33475) goto L36629 else goto L36630;
// L36629:
// 	$TMIi33474 =& $submatch[$TLE4173];
// 	goto L36631;
// L36630:
// 	$TMIi33474 = $submatch[$TLE4173];
// 	goto L36631;
// L36631:
// 	$thelink = $this->makemedialink($TMIi33474, $TLE4174, $text);
// 	goto L36652;
// L36651:
// 	$TLE4175 = $wgContLang->linktrail();
// 	$TLE4176 = 4;
// 	$TLE33477 = param_is_ref (NULL, "preg_match", 0);
// 	;
// 	if (TLE33477) goto L36632 else goto L36633;
// L36632:
// 	$TMIi33476 =& $match[$TLE4176];
// 	goto L36634;
// L36633:
// 	$TMIi33476 = $match[$TLE4176];
// 	goto L36634;
// L36634:
// 	$TLE4177 = preg_match($TLE4175, $TMIi33476, $submatch);
// 	if (TLE4177) goto L36635 else goto L36636;
// L36635:
// 	$TLE4178 = 1;
// 	$TSi4179 = $submatch[$TLE4178];
// 	$trail = $TSi4179;
// 	goto L36637;
// L36636:
// 	$trail = '';
// 	goto L36637;
// L36637:
// 	$TLE4180 = '/\\[\\[(.*?)\\]\\]';
// 	$TLE4181 = '/';
// 	$TLE4182 = preg_quote($trail, $TLE4181);
// 	$TLE4183 = ($TLE4180 . $TLE4182);
// 	$TLE4184 = '/';
// 	$linkRegexp = ($TLE4183 . $TLE4184);
// 	$TLE4185 = 1;
// 	$TLE4186 = 0;
// 	$TMIi33478 = $match[$TLE4185];
// 	$TLE476 = isset($TMIi33478[$TLE4186]);
// 	if (TLE476) goto L36638 else goto L36639;
// L36638:
// 	$TLE4187 = 1;
// 	$TLE4188 = 0;
// 	$TSi4189 = $match[$TLE4187];
// 	$TSi4190 = $TSi4189[$TLE4188];
// 	$TLE4191 = ':';
// 	$TEF477 = ($TSi4190 == $TLE4191);
// 	goto L36640;
// L36639:
// 	$TEF477 = $TLE476;
// 	goto L36640;
// L36640:
// 	$TLE4192 = (bool) $TEF477;
// 	if (TLE4192) goto L36644 else goto L36645;
// L36644:
// 	$TLE4193 = 1;
// 	$TLE4194 = 1;
// 	$TLE4195 = 1;
// 	$TLE33480 = param_is_ref (NULL, "substr", 0);
// 	;
// 	if (TLE33480) goto L36641 else goto L36642;
// L36641:
// 	$TMIi33479 =& $match[$TLE4194];
// 	goto L36643;
// L36642:
// 	$TMIi33479 = $match[$TLE4194];
// 	goto L36643;
// L36643:
// 	$TLE4196 = substr($TMIi33479, $TLE4195);
// 	$match[$TLE4193] = $TLE4196;
// 	goto L36646;
// L36645:
// 	goto L36646;
// L36646:
// 	$TLE4197 = 1;
// 	$TLE4198 = '';
// 	$TLE33482 = param_is_ref (this, "makelink", 0);
// 	;
// 	if (TLE33482) goto L36647 else goto L36648;
// L36647:
// 	$TMIi33481 =& $match[$TLE4197];
// 	goto L36649;
// L36648:
// 	$TMIi33481 = $match[$TLE4197];
// 	goto L36649;
// L36649:
// 	$thelink = $this->makelink($TMIi33481, $text, $TLE4198, $trail);
// 	goto L36652;
// L36652:
// 	$TLE4199 = stringutils::escaperegexreplacement($thelink);
// 	$TLE4200 = 1;
// 	$comment = preg_replace($linkRegexp, $TLE4199, $comment, $TLE4200);
// 	return $comment;
// }

void CallFunction()
{

}

PHP_METHOD(Linker, formatlinksincommentcallback)
{
zval* local_TEF477 = NULL;
zval* local_TLE33469 = NULL;
zval* local_TLE33471 = NULL;
zval* local_TLE33473 = NULL;
zval* local_TLE33475 = NULL;
zval* local_TLE33477 = NULL;
zval* local_TLE33480 = NULL;
zval* local_TLE33482 = NULL;
zval* local_TLE4128 = NULL;
zval* local_TLE4129 = NULL;
 zval* local_TLE4130 = NULL; // return of the function
zval* local_TLE4131 = NULL;
zval* local_TLE4132 = NULL;
zval* local_TLE4133 = NULL;
zval* local_TLE4134 = NULL;
zval* local_TLE4135 = NULL;
zval* local_TLE4136 = NULL;
zval* local_TLE4137 = NULL;
zval* local_TLE4138 = NULL;
zval* local_TLE4139 = NULL;
zval* local_TLE4140 = NULL;
zval* local_TLE4141 = NULL;
zval* local_TLE4143 = NULL;
zval* local_TLE4144 = NULL;
zval* local_TLE4145 = NULL;
zval* local_TLE4146 = NULL;
zval* local_TLE4147 = NULL;
zval* local_TLE4148 = NULL;
zval* local_TLE4149 = NULL;
zval* local_TLE4150 = NULL;
zval* local_TLE4152 = NULL;
zval* local_TLE4153 = NULL;
zval* local_TLE4155 = NULL;
zval* local_TLE4156 = NULL;
zval* local_TLE4157 = NULL;
zval* local_TLE4158 = NULL;
zval* local_TLE4159 = NULL;
zval* local_TLE4161 = NULL;
zval* local_TLE4162 = NULL;
zval* local_TLE4164 = NULL;
zval* local_TLE4167 = NULL;
zval* local_TLE4168 = NULL;
zval* local_TLE4169 = NULL;
zval* local_TLE4170 = NULL;
zval* local_TLE4171 = NULL;
zval* local_TLE4172 = NULL;
zval* local_TLE4173 = NULL;
zval* local_TLE4174 = NULL;
zval* local_TLE4175 = NULL;
zval* local_TLE4176 = NULL;
zval* local_TLE4177 = NULL;
zval* local_TLE4178 = NULL;
zval* local_TLE4180 = NULL;
zval* local_TLE4181 = NULL;
zval* local_TLE4182 = NULL;
zval* local_TLE4183 = NULL;
zval* local_TLE4184 = NULL;
zval* local_TLE4185 = NULL;
zval* local_TLE4186 = NULL;
zval* local_TLE4187 = NULL;
zval* local_TLE4188 = NULL;
zval* local_TLE4191 = NULL;
zval* local_TLE4192 = NULL;
zval* local_TLE4193 = NULL;
zval* local_TLE4194 = NULL;
zval* local_TLE4195 = NULL;
zval* local_TLE4196 = NULL;
zval* local_TLE4197 = NULL;
zval* local_TLE4198 = NULL;
zval* local_TLE4199 = NULL;
zval* local_TLE4200 = NULL;
zval* local_TLE476 = NULL;
zval* local_TMIi33468 = NULL;
zval* local_TMIi33470 = NULL;
zval* local_TMIi33472 = NULL;
zval* local_TMIi33474 = NULL;
zval* local_TMIi33476 = NULL;
zval* local_TMIi33478 = NULL;
zval* local_TMIi33479 = NULL;
zval* local_TMIi33481 = NULL;
zval* local_TSa4151 = NULL;
zval* local_TSa4154 = NULL;
zval* local_TSa4166 = NULL;
zval* local_TSi4142 = NULL;
zval* local_TSi4160 = NULL;
zval* local_TSi4163 = NULL;
zval* local_TSi4165 = NULL;
zval* local_TSi4179 = NULL;
zval* local_TSi4189 = NULL;
zval* local_TSi4190 = NULL;
zval* local_comment = NULL;
zval* local_linkRegexp = NULL;
zval* local_match = NULL;
zval* local_medians = NULL;
zval* local_submatch = NULL;
zval* local_text = NULL;
zval* local_thelink = NULL;
zval* local_this = getThis();
zval* local_trail = NULL;
zval* local_wgContLang = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_match != NULL)
{
	zval_ptr_dtor (&local_match);
}
local_match = params[0];
}
// Function body
// global $wgContLang;
{
     if (local_wgContLang == NULL)
    {
      local_wgContLang = EG (uninitialized_zval_ptr);
      local_wgContLang->refcount++;
    }
  zval** p_local = &local_wgContLang;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgContLang", 10 + 1, 2660379225u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// $TLE4128 = '(?:';
{
        if (local_TLE4128 == NULL)
    {
      local_TLE4128 = EG (uninitialized_zval_ptr);
      local_TLE4128->refcount++;
    }
  zval** p_lhs = &local_TLE4128;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "(?:", 3, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4129 = NS_MEDIA;
{
   // No null-terminator in length for get_constant.
   // zend_get_constant always returns a copy of the constant.
     if (local_TLE4129 == NULL)
    {
      local_TLE4129 = EG (uninitialized_zval_ptr);
      local_TLE4129->refcount++;
    }
  zval** p_lhs = &local_TLE4129;

   if (!(*p_lhs)->is_ref)
   {
        zval_ptr_dtor (p_lhs);
   get_constant ("NS_MEDIA", 8, p_lhs TSRMLS_CC);

   }
   else
   {
         zval* constant;
    get_constant ("NS_MEDIA", 8, p_lhs TSRMLS_CC);
    overwrite_lhs_no_copy (*p_lhs, constant);
    safe_free_zval_ptr (constant);

   }
phc_check_invariants (TSRMLS_C);
}


// $TLE4130 = mwnamespace::getcanonicalname($TLE4129);
{
   initialize_function_call (&mwnamespace_getcanonicalname_fci, &mwnamespace_getcanonicalname_fcic, "mwnamespace::getcanonicalname", "Parser2.php", 2679 TSRMLS_CC);
      zend_function* signature = mwnamespace_getcanonicalname_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4129 == NULL)
    {
      local_TLE4129 = EG (uninitialized_zval_ptr);
      local_TLE4129->refcount++;
    }
  zval** p_arg = &local_TLE4129;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4129 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4129;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2679, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = mwnamespace_getcanonicalname_fci.param_count;
   zvalParamsT<mwnamespace_getcanonicalname> params_save = mwnamespace_getcanonicalname_fci.params;
   zend_functionRT<mwnamespace_getcanonicalname> retval_save = mwnamespace_getcanonicalname_fci.retval_ptr_ptr;

   zvalPReturn<mwnamespace_getcanonicalname> rhs = NULL; // the return of the function, to be assigned to the variable

   // set up params
   mwnamespace_getcanonicalname_fci.params = args_ind;
   mwnamespace_getcanonicalname_fci.param_count = 1;
   mwnamespace_getcanonicalname_fci.retval_ptr_ptr = &rhs; // where to store the value returned... it starts with zero

   // call the function
   int success = zend_call_function (&mwnamespace_getcanonicalname_fci, &mwnamespace_getcanonicalname_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   mwnamespace_getcanonicalname_fci.params = params_save;
   mwnamespace_getcanonicalname_fci.param_count = param_count_save;
   mwnamespace_getcanonicalname_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
     assert (rhs != EG(uninitialized_zval_ptr)); // we make sure that the function returns something
     rhs->is_ref = 1;// it is a pointer
     if (saved_refcount != 0)
       {
	 rhs->refcount = saved_refcount;
       }
     rhs->refcount++; // add a reference
   }
   saved_refcount = 0; // for 'obscure cases'


   // return value 
   if (local_TLE4130 == NULL) // we can optimize this out with pgo
     {
       local_TLE4130 = EG (uninitialized_zval_ptr);// create an empty pointer
       local_TLE4130->refcount++;
     }
   zval** p_lhs = &local_TLE4130; // save this
   write_var (p_lhs, rhs); // the left hand side transfered.


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}

/*
  end of the call
*/
// $TLE4131 = '/';
{
        if (local_TLE4131 == NULL)
    {
      local_TLE4131 = EG (uninitialized_zval_ptr);
      local_TLE4131->refcount++;
    }
  zval** p_lhs = &local_TLE4131;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "/", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4132 = preg_quote($TLE4130, $TLE4131);
{
   initialize_function_call (&preg_quote_fci, &preg_quote_fcic, "preg_quote", "Parser2.php", 2679 TSRMLS_CC);
      zend_function* signature = preg_quote_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4130 == NULL)
    {
      local_TLE4130 = EG (uninitialized_zval_ptr);
      local_TLE4130->refcount++;
    }
  zval** p_arg = &local_TLE4130;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4130 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4130;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4131 == NULL)
    {
      local_TLE4131 = EG (uninitialized_zval_ptr);
      local_TLE4131->refcount++;
    }
  zval** p_arg = &local_TLE4131;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4131 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4131;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2679, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = preg_quote_fci.param_count;
   zval*** params_save = preg_quote_fci.params;
   zval** retval_save = preg_quote_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   preg_quote_fci.params = args_ind;
   preg_quote_fci.param_count = 2;
   preg_quote_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&preg_quote_fci, &preg_quote_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   preg_quote_fci.params = params_save;
   preg_quote_fci.param_count = param_count_save;
   preg_quote_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4132 == NULL)
    {
      local_TLE4132 = EG (uninitialized_zval_ptr);
      local_TLE4132->refcount++;
    }
  zval** p_lhs = &local_TLE4132;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE4133 = ($TLE4128 . $TLE4132);
{
    if (local_TLE4133 == NULL)
    {
      local_TLE4133 = EG (uninitialized_zval_ptr);
      local_TLE4133->refcount++;
    }
  zval** p_lhs = &local_TLE4133;

    zval* left;
  if (local_TLE4128 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE4128;

    zval* right;
  if (local_TLE4132 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE4132;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE4134 = '|';
{
        if (local_TLE4134 == NULL)
    {
      local_TLE4134 = EG (uninitialized_zval_ptr);
      local_TLE4134->refcount++;
    }
  zval** p_lhs = &local_TLE4134;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "|", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $medians = ($TLE4133 . $TLE4134);
{
    if (local_medians == NULL)
    {
      local_medians = EG (uninitialized_zval_ptr);
      local_medians->refcount++;
    }
  zval** p_lhs = &local_medians;

    zval* left;
  if (local_TLE4133 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE4133;

    zval* right;
  if (local_TLE4134 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE4134;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE4135 = NS_MEDIA;
{
   // No null-terminator in length for get_constant.
   // zend_get_constant always returns a copy of the constant.
     if (local_TLE4135 == NULL)
    {
      local_TLE4135 = EG (uninitialized_zval_ptr);
      local_TLE4135->refcount++;
    }
  zval** p_lhs = &local_TLE4135;

   if (!(*p_lhs)->is_ref)
   {
        zval_ptr_dtor (p_lhs);
   get_constant ("NS_MEDIA", 8, p_lhs TSRMLS_CC);

   }
   else
   {
         zval* constant;
    get_constant ("NS_MEDIA", 8, p_lhs TSRMLS_CC);
    overwrite_lhs_no_copy (*p_lhs, constant);
    safe_free_zval_ptr (constant);

   }
phc_check_invariants (TSRMLS_C);
}
// $TLE4136 = $wgContLang->getnstext($TLE4135);
{
     if (local_wgContLang == NULL)
    {
      local_wgContLang = EG (uninitialized_zval_ptr);
      local_wgContLang->refcount++;
    }
  zval** p_obj = &local_wgContLang;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getnstext", "Parser2.php", 2680 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4135 == NULL)
    {
      local_TLE4135 = EG (uninitialized_zval_ptr);
      local_TLE4135->refcount++;
    }
  zval** p_arg = &local_TLE4135;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4135 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4135;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2680, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 1;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4136 == NULL)
    {
      local_TLE4136 = EG (uninitialized_zval_ptr);
      local_TLE4136->refcount++;
    }
  zval** p_lhs = &local_TLE4136;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE4137 = '/';
{
        if (local_TLE4137 == NULL)
    {
      local_TLE4137 = EG (uninitialized_zval_ptr);
      local_TLE4137->refcount++;
    }
  zval** p_lhs = &local_TLE4137;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "/", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4138 = preg_quote($TLE4136, $TLE4137);
{
   initialize_function_call (&preg_quote_fci, &preg_quote_fcic, "preg_quote", "Parser2.php", 2680 TSRMLS_CC);
      zend_function* signature = preg_quote_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4136 == NULL)
    {
      local_TLE4136 = EG (uninitialized_zval_ptr);
      local_TLE4136->refcount++;
    }
  zval** p_arg = &local_TLE4136;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4136 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4136;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4137 == NULL)
    {
      local_TLE4137 = EG (uninitialized_zval_ptr);
      local_TLE4137->refcount++;
    }
  zval** p_arg = &local_TLE4137;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4137 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4137;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2680, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = preg_quote_fci.param_count;
   zval*** params_save = preg_quote_fci.params;
   zval** retval_save = preg_quote_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   preg_quote_fci.params = args_ind;
   preg_quote_fci.param_count = 2;
   preg_quote_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&preg_quote_fci, &preg_quote_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   preg_quote_fci.params = params_save;
   preg_quote_fci.param_count = param_count_save;
   preg_quote_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4138 == NULL)
    {
      local_TLE4138 = EG (uninitialized_zval_ptr);
      local_TLE4138->refcount++;
    }
  zval** p_lhs = &local_TLE4138;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE4139 = '):';
{
        if (local_TLE4139 == NULL)
    {
      local_TLE4139 = EG (uninitialized_zval_ptr);
      local_TLE4139->refcount++;
    }
  zval** p_lhs = &local_TLE4139;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "):", 2, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4140 = ($TLE4138 . $TLE4139);
{
    if (local_TLE4140 == NULL)
    {
      local_TLE4140 = EG (uninitialized_zval_ptr);
      local_TLE4140->refcount++;
    }
  zval** p_lhs = &local_TLE4140;

    zval* left;
  if (local_TLE4138 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE4138;

    zval* right;
  if (local_TLE4139 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE4139;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $medians = ($medians . $TLE4140);
{
    if (local_medians == NULL)
    {
      local_medians = EG (uninitialized_zval_ptr);
      local_medians->refcount++;
    }
  zval** p_lhs = &local_medians;

    zval* left;
  if (local_medians == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_medians;

    zval* right;
  if (local_TLE4140 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE4140;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE4141 = 0;
{
        if (local_TLE4141 == NULL)
    {
      local_TLE4141 = EG (uninitialized_zval_ptr);
      local_TLE4141->refcount++;
    }
  zval** p_lhs = &local_TLE4141;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TSi4142 = $match[$TLE4141];
{
     if (local_TSi4142 == NULL)
    {
      local_TSi4142 = EG (uninitialized_zval_ptr);
      local_TSi4142->refcount++;
    }
  zval** p_lhs = &local_TSi4142;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4141 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4141;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// $comment = $TSi4142;
{
    if (local_comment == NULL)
    {
      local_comment = EG (uninitialized_zval_ptr);
      local_comment->refcount++;
    }
  zval** p_lhs = &local_comment;

    zval* rhs;
  if (local_TSi4142 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSi4142;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// $TLE4143 = 1;
{
        if (local_TLE4143 == NULL)
    {
      local_TLE4143 = EG (uninitialized_zval_ptr);
      local_TLE4143->refcount++;
    }
  zval** p_lhs = &local_TLE4143;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4144 = '%';
{
        if (local_TLE4144 == NULL)
    {
      local_TLE4144 = EG (uninitialized_zval_ptr);
      local_TLE4144->refcount++;
    }
  zval** p_lhs = &local_TLE4144;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "%", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE33469 = param_is_ref (NULL, "strpos", 0);
// ;
{
   initialize_function_call (&strpos_fci, &strpos_fcic, "strpos", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = strpos_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE33469 == NULL)
    {
      local_TLE33469 = EG (uninitialized_zval_ptr);
      local_TLE33469->refcount++;
    }
  zval** p_lhs = &local_TLE33469;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE33469) goto L36614 else goto L36615;
{
     zval* p_cond;
  if (local_TLE33469 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE33469;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36614;
   else
      goto L36615;
phc_check_invariants (TSRMLS_C);
}
// L36614:
L36614:;
// $TMIi33468 =& $match[$TLE4143];
{
     if (local_TMIi33468 == NULL)
    {
      local_TMIi33468 = EG (uninitialized_zval_ptr);
      local_TMIi33468->refcount++;
    }
  zval** p_lhs = &local_TMIi33468;

     if (local_match == NULL)
    {
      local_match = EG (uninitialized_zval_ptr);
      local_match->refcount++;
    }
  zval** p_r_array = &local_match;

     zval* r_index;
  if (local_TLE4143 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4143;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36616;
{
goto L36616;
phc_check_invariants (TSRMLS_C);
}
// L36615:
L36615:;
// $TMIi33468 = $match[$TLE4143];
{
     if (local_TMIi33468 == NULL)
    {
      local_TMIi33468 = EG (uninitialized_zval_ptr);
      local_TMIi33468->refcount++;
    }
  zval** p_lhs = &local_TMIi33468;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4143 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4143;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36616;
{
goto L36616;
phc_check_invariants (TSRMLS_C);
}
// L36616:
L36616:;
// $TLE4145 = strpos($TMIi33468, $TLE4144);
{
   initialize_function_call (&strpos_fci, &strpos_fcic, "strpos", "Parser2.php", 2685 TSRMLS_CC);
      zend_function* signature = strpos_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi33468 == NULL)
    {
      local_TMIi33468 = EG (uninitialized_zval_ptr);
      local_TMIi33468->refcount++;
    }
  zval** p_arg = &local_TMIi33468;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi33468 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi33468;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4144 == NULL)
    {
      local_TLE4144 = EG (uninitialized_zval_ptr);
      local_TLE4144->refcount++;
    }
  zval** p_arg = &local_TLE4144;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4144 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4144;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2685, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = strpos_fci.param_count;
   zval*** params_save = strpos_fci.params;
   zval** retval_save = strpos_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   strpos_fci.params = args_ind;
   strpos_fci.param_count = 2;
   strpos_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&strpos_fci, &strpos_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   strpos_fci.params = params_save;
   strpos_fci.param_count = param_count_save;
   strpos_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4145 == NULL)
    {
      local_TLE4145 = EG (uninitialized_zval_ptr);
      local_TLE4145->refcount++;
    }
  zval** p_lhs = &local_TLE4145;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE4146 = False;
{
        if (local_TLE4146 == NULL)
    {
      local_TLE4146 = EG (uninitialized_zval_ptr);
      local_TLE4146->refcount++;
    }
  zval** p_lhs = &local_TLE4146;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TLE4147 = ($TLE4145 !== $TLE4146);
{
    if (local_TLE4147 == NULL)
    {
      local_TLE4147 = EG (uninitialized_zval_ptr);
      local_TLE4147->refcount++;
    }
  zval** p_lhs = &local_TLE4147;

    zval* left;
  if (local_TLE4145 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE4145;

    zval* right;
  if (local_TLE4146 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE4146;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_not_identical_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE4147) goto L36620 else goto L36621;
{
     zval* p_cond;
  if (local_TLE4147 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE4147;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36620;
   else
      goto L36621;
phc_check_invariants (TSRMLS_C);
}
// L36620:
L36620:;
// $TLE4148 = 1;
{
        if (local_TLE4148 == NULL)
    {
      local_TLE4148 = EG (uninitialized_zval_ptr);
      local_TLE4148->refcount++;
    }
  zval** p_lhs = &local_TLE4148;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4149 = '<';
{
        if (local_TLE4149 == NULL)
    {
      local_TLE4149 = EG (uninitialized_zval_ptr);
      local_TLE4149->refcount++;
    }
  zval** p_lhs = &local_TLE4149;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "<", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4150 = '>';
{
        if (local_TLE4150 == NULL)
    {
      local_TLE4150 = EG (uninitialized_zval_ptr);
      local_TLE4150->refcount++;
    }
  zval** p_lhs = &local_TLE4150;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, ">", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// unset($TSa4151);
{
if (local_TSa4151 != NULL)
{
zval_ptr_dtor (&local_TSa4151);
local_TSa4151 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa4151 = (array) $TSa4151;
{
      if (local_TSa4151 == NULL)
    {
      local_TSa4151 = EG (uninitialized_zval_ptr);
      local_TSa4151->refcount++;
    }
  zval** p_lhs = &local_TSa4151;

    zval* rhs;
  if (local_TSa4151 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa4151;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa4151[] = $TLE4149;
{
     if (local_TSa4151 == NULL)
    {
      local_TSa4151 = EG (uninitialized_zval_ptr);
      local_TSa4151->refcount++;
    }
  zval** p_array = &local_TSa4151;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE4149 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE4149;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa4151[] = $TLE4150;
{
     if (local_TSa4151 == NULL)
    {
      local_TSa4151 = EG (uninitialized_zval_ptr);
      local_TSa4151->refcount++;
    }
  zval** p_array = &local_TSa4151;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE4150 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE4150;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TLE4152 = '&lt;';
{
        if (local_TLE4152 == NULL)
    {
      local_TLE4152 = EG (uninitialized_zval_ptr);
      local_TLE4152->refcount++;
    }
  zval** p_lhs = &local_TLE4152;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "&lt;", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4153 = '&gt;';
{
        if (local_TLE4153 == NULL)
    {
      local_TLE4153 = EG (uninitialized_zval_ptr);
      local_TLE4153->refcount++;
    }
  zval** p_lhs = &local_TLE4153;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "&gt;", 4, 1);

phc_check_invariants (TSRMLS_C);
}
// unset($TSa4154);
{
if (local_TSa4154 != NULL)
{
zval_ptr_dtor (&local_TSa4154);
local_TSa4154 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa4154 = (array) $TSa4154;
{
      if (local_TSa4154 == NULL)
    {
      local_TSa4154 = EG (uninitialized_zval_ptr);
      local_TSa4154->refcount++;
    }
  zval** p_lhs = &local_TSa4154;

    zval* rhs;
  if (local_TSa4154 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa4154;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa4154[] = $TLE4152;
{
     if (local_TSa4154 == NULL)
    {
      local_TSa4154 = EG (uninitialized_zval_ptr);
      local_TSa4154->refcount++;
    }
  zval** p_array = &local_TSa4154;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE4152 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE4152;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa4154[] = $TLE4153;
{
     if (local_TSa4154 == NULL)
    {
      local_TSa4154 = EG (uninitialized_zval_ptr);
      local_TSa4154->refcount++;
    }
  zval** p_array = &local_TSa4154;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TLE4153 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE4153;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TLE4155 = 1;
{
        if (local_TLE4155 == NULL)
    {
      local_TLE4155 = EG (uninitialized_zval_ptr);
      local_TLE4155->refcount++;
    }
  zval** p_lhs = &local_TLE4155;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE33471 = param_is_ref (NULL, "urldecode", 0);
// ;
{
   initialize_function_call (&urldecode_fci, &urldecode_fcic, "urldecode", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = urldecode_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE33471 == NULL)
    {
      local_TLE33471 = EG (uninitialized_zval_ptr);
      local_TLE33471->refcount++;
    }
  zval** p_lhs = &local_TLE33471;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE33471) goto L36617 else goto L36618;
{
     zval* p_cond;
  if (local_TLE33471 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE33471;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36617;
   else
      goto L36618;
phc_check_invariants (TSRMLS_C);
}
// L36617:
L36617:;
// $TMIi33470 =& $match[$TLE4155];
{
     if (local_TMIi33470 == NULL)
    {
      local_TMIi33470 = EG (uninitialized_zval_ptr);
      local_TMIi33470->refcount++;
    }
  zval** p_lhs = &local_TMIi33470;

     if (local_match == NULL)
    {
      local_match = EG (uninitialized_zval_ptr);
      local_match->refcount++;
    }
  zval** p_r_array = &local_match;

     zval* r_index;
  if (local_TLE4155 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4155;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36619;
{
goto L36619;
phc_check_invariants (TSRMLS_C);
}
// L36618:
L36618:;
// $TMIi33470 = $match[$TLE4155];
{
     if (local_TMIi33470 == NULL)
    {
      local_TMIi33470 = EG (uninitialized_zval_ptr);
      local_TMIi33470->refcount++;
    }
  zval** p_lhs = &local_TMIi33470;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4155 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4155;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36619;
{
goto L36619;
phc_check_invariants (TSRMLS_C);
}
// L36619:
L36619:;
// $TLE4156 = urldecode($TMIi33470);
{
   initialize_function_call (&urldecode_fci, &urldecode_fcic, "urldecode", "Parser2.php", 2686 TSRMLS_CC);
      zend_function* signature = urldecode_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi33470 == NULL)
    {
      local_TMIi33470 = EG (uninitialized_zval_ptr);
      local_TMIi33470->refcount++;
    }
  zval** p_arg = &local_TMIi33470;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi33470 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi33470;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2686, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = urldecode_fci.param_count;
   zval*** params_save = urldecode_fci.params;
   zval** retval_save = urldecode_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   urldecode_fci.params = args_ind;
   urldecode_fci.param_count = 1;
   urldecode_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&urldecode_fci, &urldecode_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   urldecode_fci.params = params_save;
   urldecode_fci.param_count = param_count_save;
   urldecode_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4156 == NULL)
    {
      local_TLE4156 = EG (uninitialized_zval_ptr);
      local_TLE4156->refcount++;
    }
  zval** p_lhs = &local_TLE4156;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE4157 = str_replace($TSa4151, $TSa4154, $TLE4156);
{
   initialize_function_call (&str_replace_fci, &str_replace_fcic, "str_replace", "Parser2.php", 2686 TSRMLS_CC);
      zend_function* signature = str_replace_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TSa4151 == NULL)
    {
      local_TSa4151 = EG (uninitialized_zval_ptr);
      local_TSa4151->refcount++;
    }
  zval** p_arg = &local_TSa4151;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TSa4151 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TSa4151;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TSa4154 == NULL)
    {
      local_TSa4154 = EG (uninitialized_zval_ptr);
      local_TSa4154->refcount++;
    }
  zval** p_arg = &local_TSa4154;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TSa4154 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TSa4154;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4156 == NULL)
    {
      local_TLE4156 = EG (uninitialized_zval_ptr);
      local_TLE4156->refcount++;
    }
  zval** p_arg = &local_TLE4156;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4156 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4156;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2686, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = str_replace_fci.param_count;
   zval*** params_save = str_replace_fci.params;
   zval** retval_save = str_replace_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   str_replace_fci.params = args_ind;
   str_replace_fci.param_count = 3;
   str_replace_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&str_replace_fci, &str_replace_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   str_replace_fci.params = params_save;
   str_replace_fci.param_count = param_count_save;
   str_replace_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4157 == NULL)
    {
      local_TLE4157 = EG (uninitialized_zval_ptr);
      local_TLE4157->refcount++;
    }
  zval** p_lhs = &local_TLE4157;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $match[$TLE4148] = $TLE4157;
{
     if (local_match == NULL)
    {
      local_match = EG (uninitialized_zval_ptr);
      local_match->refcount++;
    }
  zval** p_array = &local_match;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_TLE4148 == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_TLE4148;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TLE4157 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE4157;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TLE4157 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE4157;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// goto L36622;
{
goto L36622;
phc_check_invariants (TSRMLS_C);
}
// L36621:
L36621:;
// goto L36622;
{
goto L36622;
phc_check_invariants (TSRMLS_C);
}
// L36622:
L36622:;
// $TLE4158 = '';
{
        if (local_TLE4158 == NULL)
    {
      local_TLE4158 = EG (uninitialized_zval_ptr);
      local_TLE4158->refcount++;
    }
  zval** p_lhs = &local_TLE4158;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4159 = 3;
{
        if (local_TLE4159 == NULL)
    {
      local_TLE4159 = EG (uninitialized_zval_ptr);
      local_TLE4159->refcount++;
    }
  zval** p_lhs = &local_TLE4159;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 3);

phc_check_invariants (TSRMLS_C);
}
// $TSi4160 = $match[$TLE4159];
{
     if (local_TSi4160 == NULL)
    {
      local_TSi4160 = EG (uninitialized_zval_ptr);
      local_TSi4160->refcount++;
    }
  zval** p_lhs = &local_TSi4160;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4159 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4159;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// $TLE4161 = ($TLE4158 != $TSi4160);
{
    if (local_TLE4161 == NULL)
    {
      local_TLE4161 = EG (uninitialized_zval_ptr);
      local_TLE4161->refcount++;
    }
  zval** p_lhs = &local_TLE4161;

    zval* left;
  if (local_TLE4158 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE4158;

    zval* right;
  if (local_TSi4160 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TSi4160;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_not_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE4161) goto L36623 else goto L36624;
{
     zval* p_cond;
  if (local_TLE4161 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE4161;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36623;
   else
      goto L36624;
phc_check_invariants (TSRMLS_C);
}
// L36623:
L36623:;
// $TLE4162 = 3;
{
        if (local_TLE4162 == NULL)
    {
      local_TLE4162 = EG (uninitialized_zval_ptr);
      local_TLE4162->refcount++;
    }
  zval** p_lhs = &local_TLE4162;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 3);

phc_check_invariants (TSRMLS_C);
}
// $TSi4163 = $match[$TLE4162];
{
     if (local_TSi4163 == NULL)
    {
      local_TSi4163 = EG (uninitialized_zval_ptr);
      local_TSi4163->refcount++;
    }
  zval** p_lhs = &local_TSi4163;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4162 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4162;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// $text = $TSi4163;
{
    if (local_text == NULL)
    {
      local_text = EG (uninitialized_zval_ptr);
      local_text->refcount++;
    }
  zval** p_lhs = &local_text;

    zval* rhs;
  if (local_TSi4163 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSi4163;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L36625;
{
goto L36625;
phc_check_invariants (TSRMLS_C);
}
// L36624:
L36624:;
// $TLE4164 = 1;
{
        if (local_TLE4164 == NULL)
    {
      local_TLE4164 = EG (uninitialized_zval_ptr);
      local_TLE4164->refcount++;
    }
  zval** p_lhs = &local_TLE4164;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TSi4165 = $match[$TLE4164];
{
     if (local_TSi4165 == NULL)
    {
      local_TSi4165 = EG (uninitialized_zval_ptr);
      local_TSi4165->refcount++;
    }
  zval** p_lhs = &local_TSi4165;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4164 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4164;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// $text = $TSi4165;
{
    if (local_text == NULL)
    {
      local_text = EG (uninitialized_zval_ptr);
      local_text->refcount++;
    }
  zval** p_lhs = &local_text;

    zval* rhs;
  if (local_TSi4165 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSi4165;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L36625;
{
goto L36625;
phc_check_invariants (TSRMLS_C);
}
// L36625:
L36625:;
// unset($TSa4166);
{
if (local_TSa4166 != NULL)
{
zval_ptr_dtor (&local_TSa4166);
local_TSa4166 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa4166 = (array) $TSa4166;
{
      if (local_TSa4166 == NULL)
    {
      local_TSa4166 = EG (uninitialized_zval_ptr);
      local_TSa4166->refcount++;
    }
  zval** p_lhs = &local_TSa4166;

    zval* rhs;
  if (local_TSa4166 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa4166;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $submatch = $TSa4166;
{
    if (local_submatch == NULL)
    {
      local_submatch = EG (uninitialized_zval_ptr);
      local_submatch->refcount++;
    }
  zval** p_lhs = &local_submatch;

    zval* rhs;
  if (local_TSa4166 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa4166;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// $TLE4167 = '/^';
{
        if (local_TLE4167 == NULL)
    {
      local_TLE4167 = EG (uninitialized_zval_ptr);
      local_TLE4167->refcount++;
    }
  zval** p_lhs = &local_TLE4167;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "/^", 2, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4168 = ($TLE4167 . $medians);
{
    if (local_TLE4168 == NULL)
    {
      local_TLE4168 = EG (uninitialized_zval_ptr);
      local_TLE4168->refcount++;
    }
  zval** p_lhs = &local_TLE4168;

    zval* left;
  if (local_TLE4167 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE4167;

    zval* right;
  if (local_medians == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_medians;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE4169 = '(.*)$/i';
{
        if (local_TLE4169 == NULL)
    {
      local_TLE4169 = EG (uninitialized_zval_ptr);
      local_TLE4169->refcount++;
    }
  zval** p_lhs = &local_TLE4169;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "(.*)$/i", 7, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4170 = ($TLE4168 . $TLE4169);
{
    if (local_TLE4170 == NULL)
    {
      local_TLE4170 = EG (uninitialized_zval_ptr);
      local_TLE4170->refcount++;
    }
  zval** p_lhs = &local_TLE4170;

    zval* left;
  if (local_TLE4168 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE4168;

    zval* right;
  if (local_TLE4169 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE4169;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE4171 = 1;
{
        if (local_TLE4171 == NULL)
    {
      local_TLE4171 = EG (uninitialized_zval_ptr);
      local_TLE4171->refcount++;
    }
  zval** p_lhs = &local_TLE4171;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE33473 = param_is_ref (NULL, "preg_match", 0);
// ;
{
   initialize_function_call (&preg_match_fci, &preg_match_fcic, "preg_match", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = preg_match_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE33473 == NULL)
    {
      local_TLE33473 = EG (uninitialized_zval_ptr);
      local_TLE33473->refcount++;
    }
  zval** p_lhs = &local_TLE33473;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE33473) goto L36626 else goto L36627;
{
     zval* p_cond;
  if (local_TLE33473 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE33473;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36626;
   else
      goto L36627;
phc_check_invariants (TSRMLS_C);
}
// L36626:
L36626:;
// $TMIi33472 =& $match[$TLE4171];
{
     if (local_TMIi33472 == NULL)
    {
      local_TMIi33472 = EG (uninitialized_zval_ptr);
      local_TMIi33472->refcount++;
    }
  zval** p_lhs = &local_TMIi33472;

     if (local_match == NULL)
    {
      local_match = EG (uninitialized_zval_ptr);
      local_match->refcount++;
    }
  zval** p_r_array = &local_match;

     zval* r_index;
  if (local_TLE4171 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4171;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36628;
{
goto L36628;
phc_check_invariants (TSRMLS_C);
}
// L36627:
L36627:;
// $TMIi33472 = $match[$TLE4171];
{
     if (local_TMIi33472 == NULL)
    {
      local_TMIi33472 = EG (uninitialized_zval_ptr);
      local_TMIi33472->refcount++;
    }
  zval** p_lhs = &local_TMIi33472;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4171 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4171;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36628;
{
goto L36628;
phc_check_invariants (TSRMLS_C);
}
// L36628:
L36628:;
// $TLE4172 = preg_match($TLE4170, $TMIi33472, $submatch);
{
   initialize_function_call (&preg_match_fci, &preg_match_fcic, "preg_match", "Parser2.php", 2696 TSRMLS_CC);
      zend_function* signature = preg_match_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4170 == NULL)
    {
      local_TLE4170 = EG (uninitialized_zval_ptr);
      local_TLE4170->refcount++;
    }
  zval** p_arg = &local_TLE4170;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4170 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4170;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi33472 == NULL)
    {
      local_TMIi33472 = EG (uninitialized_zval_ptr);
      local_TMIi33472->refcount++;
    }
  zval** p_arg = &local_TMIi33472;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi33472 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi33472;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_submatch == NULL)
    {
      local_submatch = EG (uninitialized_zval_ptr);
      local_submatch->refcount++;
    }
  zval** p_arg = &local_submatch;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_submatch == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_submatch;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2696, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = preg_match_fci.param_count;
   zval*** params_save = preg_match_fci.params;
   zval** retval_save = preg_match_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   preg_match_fci.params = args_ind;
   preg_match_fci.param_count = 3;
   preg_match_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&preg_match_fci, &preg_match_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   preg_match_fci.params = params_save;
   preg_match_fci.param_count = param_count_save;
   preg_match_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4172 == NULL)
    {
      local_TLE4172 = EG (uninitialized_zval_ptr);
      local_TLE4172->refcount++;
    }
  zval** p_lhs = &local_TLE4172;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// if (TLE4172) goto L36650 else goto L36651;
{
     zval* p_cond;
  if (local_TLE4172 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE4172;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36650;
   else
      goto L36651;
phc_check_invariants (TSRMLS_C);
}
// L36650:
L36650:;
// $linkRegexp = '/\\[\\[(.*?)\\]\\]/';
{
        if (local_linkRegexp == NULL)
    {
      local_linkRegexp = EG (uninitialized_zval_ptr);
      local_linkRegexp->refcount++;
    }
  zval** p_lhs = &local_linkRegexp;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "/\\[\\[(.*?)\\]\\]/", 15, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4173 = 1;
{
        if (local_TLE4173 == NULL)
    {
      local_TLE4173 = EG (uninitialized_zval_ptr);
      local_TLE4173->refcount++;
    }
  zval** p_lhs = &local_TLE4173;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4174 = '';
{
        if (local_TLE4174 == NULL)
    {
      local_TLE4174 = EG (uninitialized_zval_ptr);
      local_TLE4174->refcount++;
    }
  zval** p_lhs = &local_TLE4174;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE33475 = param_is_ref (this, "makemedialink", 0);
// ;
{
   initialize_function_call (&makemedialink_fci, &makemedialink_fcic, "makemedialink", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = makemedialink_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE33475 == NULL)
    {
      local_TLE33475 = EG (uninitialized_zval_ptr);
      local_TLE33475->refcount++;
    }
  zval** p_lhs = &local_TLE33475;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE33475) goto L36629 else goto L36630;
{
     zval* p_cond;
  if (local_TLE33475 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE33475;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36629;
   else
      goto L36630;
phc_check_invariants (TSRMLS_C);
}
// L36629:
L36629:;
// $TMIi33474 =& $submatch[$TLE4173];
{
     if (local_TMIi33474 == NULL)
    {
      local_TMIi33474 = EG (uninitialized_zval_ptr);
      local_TMIi33474->refcount++;
    }
  zval** p_lhs = &local_TMIi33474;

     if (local_submatch == NULL)
    {
      local_submatch = EG (uninitialized_zval_ptr);
      local_submatch->refcount++;
    }
  zval** p_r_array = &local_submatch;

     zval* r_index;
  if (local_TLE4173 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4173;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36631;
{
goto L36631;
phc_check_invariants (TSRMLS_C);
}
// L36630:
L36630:;
// $TMIi33474 = $submatch[$TLE4173];
{
     if (local_TMIi33474 == NULL)
    {
      local_TMIi33474 = EG (uninitialized_zval_ptr);
      local_TMIi33474->refcount++;
    }
  zval** p_lhs = &local_TMIi33474;

     zval* r_array;
  if (local_submatch == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_submatch;

     zval* r_index;
  if (local_TLE4173 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4173;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36631;
{
goto L36631;
phc_check_invariants (TSRMLS_C);
}
// L36631:
L36631:;
// $thelink = $this->makemedialink($TMIi33474, $TLE4174, $text);
{
     if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "makemedialink", "Parser2.php", 2699 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi33474 == NULL)
    {
      local_TMIi33474 = EG (uninitialized_zval_ptr);
      local_TMIi33474->refcount++;
    }
  zval** p_arg = &local_TMIi33474;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi33474 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi33474;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4174 == NULL)
    {
      local_TLE4174 = EG (uninitialized_zval_ptr);
      local_TLE4174->refcount++;
    }
  zval** p_arg = &local_TLE4174;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4174 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4174;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_text == NULL)
    {
      local_text = EG (uninitialized_zval_ptr);
      local_text->refcount++;
    }
  zval** p_arg = &local_text;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_text == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_text;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2699, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 3;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_thelink == NULL)
    {
      local_thelink = EG (uninitialized_zval_ptr);
      local_thelink->refcount++;
    }
  zval** p_lhs = &local_thelink;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// goto L36652;
{
goto L36652;
phc_check_invariants (TSRMLS_C);
}
// L36651:
L36651:;
// $TLE4175 = $wgContLang->linktrail();
{
     if (local_wgContLang == NULL)
    {
      local_wgContLang = EG (uninitialized_zval_ptr);
      local_wgContLang->refcount++;
    }
  zval** p_obj = &local_wgContLang;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "linktrail", "Parser2.php", 2702 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "Parser2.php", 2702, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4175 == NULL)
    {
      local_TLE4175 = EG (uninitialized_zval_ptr);
      local_TLE4175->refcount++;
    }
  zval** p_lhs = &local_TLE4175;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE4176 = 4;
{
        if (local_TLE4176 == NULL)
    {
      local_TLE4176 = EG (uninitialized_zval_ptr);
      local_TLE4176->refcount++;
    }
  zval** p_lhs = &local_TLE4176;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 4);

phc_check_invariants (TSRMLS_C);
}
// $TLE33477 = param_is_ref (NULL, "preg_match", 0);
// ;
{
   initialize_function_call (&preg_match_fci, &preg_match_fcic, "preg_match", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = preg_match_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE33477 == NULL)
    {
      local_TLE33477 = EG (uninitialized_zval_ptr);
      local_TLE33477->refcount++;
    }
  zval** p_lhs = &local_TLE33477;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE33477) goto L36632 else goto L36633;
{
     zval* p_cond;
  if (local_TLE33477 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE33477;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36632;
   else
      goto L36633;
phc_check_invariants (TSRMLS_C);
}
// L36632:
L36632:;
// $TMIi33476 =& $match[$TLE4176];
{
     if (local_TMIi33476 == NULL)
    {
      local_TMIi33476 = EG (uninitialized_zval_ptr);
      local_TMIi33476->refcount++;
    }
  zval** p_lhs = &local_TMIi33476;

     if (local_match == NULL)
    {
      local_match = EG (uninitialized_zval_ptr);
      local_match->refcount++;
    }
  zval** p_r_array = &local_match;

     zval* r_index;
  if (local_TLE4176 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4176;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36634;
{
goto L36634;
phc_check_invariants (TSRMLS_C);
}
// L36633:
L36633:;
// $TMIi33476 = $match[$TLE4176];
{
     if (local_TMIi33476 == NULL)
    {
      local_TMIi33476 = EG (uninitialized_zval_ptr);
      local_TMIi33476->refcount++;
    }
  zval** p_lhs = &local_TMIi33476;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4176 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4176;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36634;
{
goto L36634;
phc_check_invariants (TSRMLS_C);
}
// L36634:
L36634:;
// $TLE4177 = preg_match($TLE4175, $TMIi33476, $submatch);
{
   initialize_function_call (&preg_match_fci, &preg_match_fcic, "preg_match", "Parser2.php", 2702 TSRMLS_CC);
      zend_function* signature = preg_match_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4175 == NULL)
    {
      local_TLE4175 = EG (uninitialized_zval_ptr);
      local_TLE4175->refcount++;
    }
  zval** p_arg = &local_TLE4175;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4175 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4175;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi33476 == NULL)
    {
      local_TMIi33476 = EG (uninitialized_zval_ptr);
      local_TMIi33476->refcount++;
    }
  zval** p_arg = &local_TMIi33476;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi33476 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi33476;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_submatch == NULL)
    {
      local_submatch = EG (uninitialized_zval_ptr);
      local_submatch->refcount++;
    }
  zval** p_arg = &local_submatch;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_submatch == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_submatch;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2702, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = preg_match_fci.param_count;
   zval*** params_save = preg_match_fci.params;
   zval** retval_save = preg_match_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   preg_match_fci.params = args_ind;
   preg_match_fci.param_count = 3;
   preg_match_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&preg_match_fci, &preg_match_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   preg_match_fci.params = params_save;
   preg_match_fci.param_count = param_count_save;
   preg_match_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4177 == NULL)
    {
      local_TLE4177 = EG (uninitialized_zval_ptr);
      local_TLE4177->refcount++;
    }
  zval** p_lhs = &local_TLE4177;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// if (TLE4177) goto L36635 else goto L36636;
{
     zval* p_cond;
  if (local_TLE4177 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE4177;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36635;
   else
      goto L36636;
phc_check_invariants (TSRMLS_C);
}
// L36635:
L36635:;
// $TLE4178 = 1;
{
        if (local_TLE4178 == NULL)
    {
      local_TLE4178 = EG (uninitialized_zval_ptr);
      local_TLE4178->refcount++;
    }
  zval** p_lhs = &local_TLE4178;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TSi4179 = $submatch[$TLE4178];
{
     if (local_TSi4179 == NULL)
    {
      local_TSi4179 = EG (uninitialized_zval_ptr);
      local_TSi4179->refcount++;
    }
  zval** p_lhs = &local_TSi4179;

     zval* r_array;
  if (local_submatch == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_submatch;

     zval* r_index;
  if (local_TLE4178 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4178;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// $trail = $TSi4179;
{
    if (local_trail == NULL)
    {
      local_trail = EG (uninitialized_zval_ptr);
      local_trail->refcount++;
    }
  zval** p_lhs = &local_trail;

    zval* rhs;
  if (local_TSi4179 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSi4179;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L36637;
{
goto L36637;
phc_check_invariants (TSRMLS_C);
}
// L36636:
L36636:;
// $trail = '';
{
        if (local_trail == NULL)
    {
      local_trail = EG (uninitialized_zval_ptr);
      local_trail->refcount++;
    }
  zval** p_lhs = &local_trail;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
// goto L36637;
{
goto L36637;
phc_check_invariants (TSRMLS_C);
}
// L36637:
L36637:;
// $TLE4180 = '/\\[\\[(.*?)\\]\\]';
{
        if (local_TLE4180 == NULL)
    {
      local_TLE4180 = EG (uninitialized_zval_ptr);
      local_TLE4180->refcount++;
    }
  zval** p_lhs = &local_TLE4180;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "/\\[\\[(.*?)\\]\\]", 14, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4181 = '/';
{
        if (local_TLE4181 == NULL)
    {
      local_TLE4181 = EG (uninitialized_zval_ptr);
      local_TLE4181->refcount++;
    }
  zval** p_lhs = &local_TLE4181;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "/", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4182 = preg_quote($trail, $TLE4181);
{
   initialize_function_call (&preg_quote_fci, &preg_quote_fcic, "preg_quote", "Parser2.php", 2707 TSRMLS_CC);
      zend_function* signature = preg_quote_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_trail == NULL)
    {
      local_trail = EG (uninitialized_zval_ptr);
      local_trail->refcount++;
    }
  zval** p_arg = &local_trail;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_trail == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_trail;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4181 == NULL)
    {
      local_TLE4181 = EG (uninitialized_zval_ptr);
      local_TLE4181->refcount++;
    }
  zval** p_arg = &local_TLE4181;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4181 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4181;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2707, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = preg_quote_fci.param_count;
   zval*** params_save = preg_quote_fci.params;
   zval** retval_save = preg_quote_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   preg_quote_fci.params = args_ind;
   preg_quote_fci.param_count = 2;
   preg_quote_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&preg_quote_fci, &preg_quote_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   preg_quote_fci.params = params_save;
   preg_quote_fci.param_count = param_count_save;
   preg_quote_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4182 == NULL)
    {
      local_TLE4182 = EG (uninitialized_zval_ptr);
      local_TLE4182->refcount++;
    }
  zval** p_lhs = &local_TLE4182;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE4183 = ($TLE4180 . $TLE4182);
{
    if (local_TLE4183 == NULL)
    {
      local_TLE4183 = EG (uninitialized_zval_ptr);
      local_TLE4183->refcount++;
    }
  zval** p_lhs = &local_TLE4183;

    zval* left;
  if (local_TLE4180 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE4180;

    zval* right;
  if (local_TLE4182 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE4182;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE4184 = '/';
{
        if (local_TLE4184 == NULL)
    {
      local_TLE4184 = EG (uninitialized_zval_ptr);
      local_TLE4184->refcount++;
    }
  zval** p_lhs = &local_TLE4184;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "/", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $linkRegexp = ($TLE4183 . $TLE4184);
{
    if (local_linkRegexp == NULL)
    {
      local_linkRegexp = EG (uninitialized_zval_ptr);
      local_linkRegexp->refcount++;
    }
  zval** p_lhs = &local_linkRegexp;

    zval* left;
  if (local_TLE4183 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE4183;

    zval* right;
  if (local_TLE4184 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE4184;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  concat_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// $TLE4185 = 1;
{
        if (local_TLE4185 == NULL)
    {
      local_TLE4185 = EG (uninitialized_zval_ptr);
      local_TLE4185->refcount++;
    }
  zval** p_lhs = &local_TLE4185;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4186 = 0;
{
        if (local_TLE4186 == NULL)
    {
      local_TLE4186 = EG (uninitialized_zval_ptr);
      local_TLE4186->refcount++;
    }
  zval** p_lhs = &local_TLE4186;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TMIi33478 = $match[$TLE4185];
{
     if (local_TMIi33478 == NULL)
    {
      local_TMIi33478 = EG (uninitialized_zval_ptr);
      local_TMIi33478->refcount++;
    }
  zval** p_lhs = &local_TMIi33478;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4185 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4185;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// $TLE476 = isset($TMIi33478[$TLE4186]);
{
if (local_TLE476 == NULL)
{
local_TLE476 = EG (uninitialized_zval_ptr);
local_TLE476->refcount++;
}
zval** p_lhs = &local_TLE476;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
if (local_TMIi33478 == NULL)
{
local_TMIi33478 = EG (uninitialized_zval_ptr);
local_TMIi33478->refcount++;
}
zval** u_array = &local_TMIi33478;
zval* u_index;
if (local_TLE4186 == NULL)
{
u_index = EG (uninitialized_zval_ptr);
}
else
{
u_index = local_TLE4186;
}
ZVAL_BOOL(value, isset_array (u_array, u_index));
phc_check_invariants (TSRMLS_C);
}
// if (TLE476) goto L36638 else goto L36639;
{
     zval* p_cond;
  if (local_TLE476 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE476;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36638;
   else
      goto L36639;
phc_check_invariants (TSRMLS_C);
}
// L36638:
L36638:;
// $TLE4187 = 1;
{
        if (local_TLE4187 == NULL)
    {
      local_TLE4187 = EG (uninitialized_zval_ptr);
      local_TLE4187->refcount++;
    }
  zval** p_lhs = &local_TLE4187;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4188 = 0;
{
        if (local_TLE4188 == NULL)
    {
      local_TLE4188 = EG (uninitialized_zval_ptr);
      local_TLE4188->refcount++;
    }
  zval** p_lhs = &local_TLE4188;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TSi4189 = $match[$TLE4187];
{
     if (local_TSi4189 == NULL)
    {
      local_TSi4189 = EG (uninitialized_zval_ptr);
      local_TSi4189->refcount++;
    }
  zval** p_lhs = &local_TSi4189;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4187 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4187;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// $TSi4190 = $TSi4189[$TLE4188];
{
     if (local_TSi4190 == NULL)
    {
      local_TSi4190 = EG (uninitialized_zval_ptr);
      local_TSi4190->refcount++;
    }
  zval** p_lhs = &local_TSi4190;

     zval* r_array;
  if (local_TSi4189 == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_TSi4189;

     zval* r_index;
  if (local_TLE4188 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4188;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// $TLE4191 = ':';
{
        if (local_TLE4191 == NULL)
    {
      local_TLE4191 = EG (uninitialized_zval_ptr);
      local_TLE4191->refcount++;
    }
  zval** p_lhs = &local_TLE4191;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, ":", 1, 1);

phc_check_invariants (TSRMLS_C);
}
// $TEF477 = ($TSi4190 == $TLE4191);
{
    if (local_TEF477 == NULL)
    {
      local_TEF477 = EG (uninitialized_zval_ptr);
      local_TEF477->refcount++;
    }
  zval** p_lhs = &local_TEF477;

    zval* left;
  if (local_TSi4190 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TSi4190;

    zval* right;
  if (local_TLE4191 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE4191;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L36640;
{
goto L36640;
phc_check_invariants (TSRMLS_C);
}
// L36639:
L36639:;
// $TEF477 = $TLE476;
{
    if (local_TEF477 == NULL)
    {
      local_TEF477 = EG (uninitialized_zval_ptr);
      local_TEF477->refcount++;
    }
  zval** p_lhs = &local_TEF477;

    zval* rhs;
  if (local_TLE476 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE476;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L36640;
{
goto L36640;
phc_check_invariants (TSRMLS_C);
}
// L36640:
L36640:;
// $TLE4192 = (bool) $TEF477;
{
      if (local_TLE4192 == NULL)
    {
      local_TLE4192 = EG (uninitialized_zval_ptr);
      local_TLE4192->refcount++;
    }
  zval** p_lhs = &local_TLE4192;

    zval* rhs;
  if (local_TEF477 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF477;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// if (TLE4192) goto L36644 else goto L36645;
{
     zval* p_cond;
  if (local_TLE4192 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE4192;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36644;
   else
      goto L36645;
phc_check_invariants (TSRMLS_C);
}
// L36644:
L36644:;
// $TLE4193 = 1;
{
        if (local_TLE4193 == NULL)
    {
      local_TLE4193 = EG (uninitialized_zval_ptr);
      local_TLE4193->refcount++;
    }
  zval** p_lhs = &local_TLE4193;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4194 = 1;
{
        if (local_TLE4194 == NULL)
    {
      local_TLE4194 = EG (uninitialized_zval_ptr);
      local_TLE4194->refcount++;
    }
  zval** p_lhs = &local_TLE4194;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4195 = 1;
{
        if (local_TLE4195 == NULL)
    {
      local_TLE4195 = EG (uninitialized_zval_ptr);
      local_TLE4195->refcount++;
    }
  zval** p_lhs = &local_TLE4195;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE33480 = param_is_ref (NULL, "substr", 0);
// ;
{
   initialize_function_call (&substr_fci, &substr_fcic, "substr", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = substr_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE33480 == NULL)
    {
      local_TLE33480 = EG (uninitialized_zval_ptr);
      local_TLE33480->refcount++;
    }
  zval** p_lhs = &local_TLE33480;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE33480) goto L36641 else goto L36642;
{
     zval* p_cond;
  if (local_TLE33480 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE33480;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36641;
   else
      goto L36642;
phc_check_invariants (TSRMLS_C);
}
// L36641:
L36641:;
// $TMIi33479 =& $match[$TLE4194];
{
     if (local_TMIi33479 == NULL)
    {
      local_TMIi33479 = EG (uninitialized_zval_ptr);
      local_TMIi33479->refcount++;
    }
  zval** p_lhs = &local_TMIi33479;

     if (local_match == NULL)
    {
      local_match = EG (uninitialized_zval_ptr);
      local_match->refcount++;
    }
  zval** p_r_array = &local_match;

     zval* r_index;
  if (local_TLE4194 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4194;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36643;
{
goto L36643;
phc_check_invariants (TSRMLS_C);
}
// L36642:
L36642:;
// $TMIi33479 = $match[$TLE4194];
{
     if (local_TMIi33479 == NULL)
    {
      local_TMIi33479 = EG (uninitialized_zval_ptr);
      local_TMIi33479->refcount++;
    }
  zval** p_lhs = &local_TMIi33479;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4194 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4194;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36643;
{
goto L36643;
phc_check_invariants (TSRMLS_C);
}
// L36643:
L36643:;
// $TLE4196 = substr($TMIi33479, $TLE4195);
{
   initialize_function_call (&substr_fci, &substr_fcic, "substr", "Parser2.php", 2709 TSRMLS_CC);
      zend_function* signature = substr_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi33479 == NULL)
    {
      local_TMIi33479 = EG (uninitialized_zval_ptr);
      local_TMIi33479->refcount++;
    }
  zval** p_arg = &local_TMIi33479;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi33479 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi33479;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4195 == NULL)
    {
      local_TLE4195 = EG (uninitialized_zval_ptr);
      local_TLE4195->refcount++;
    }
  zval** p_arg = &local_TLE4195;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4195 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4195;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2709, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = substr_fci.param_count;
   zval*** params_save = substr_fci.params;
   zval** retval_save = substr_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   substr_fci.params = args_ind;
   substr_fci.param_count = 2;
   substr_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&substr_fci, &substr_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   substr_fci.params = params_save;
   substr_fci.param_count = param_count_save;
   substr_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4196 == NULL)
    {
      local_TLE4196 = EG (uninitialized_zval_ptr);
      local_TLE4196->refcount++;
    }
  zval** p_lhs = &local_TLE4196;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $match[$TLE4193] = $TLE4196;
{
     if (local_match == NULL)
    {
      local_match = EG (uninitialized_zval_ptr);
      local_match->refcount++;
    }
  zval** p_array = &local_match;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_TLE4193 == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_TLE4193;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TLE4196 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE4196;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TLE4196 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE4196;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// goto L36646;
{
goto L36646;
phc_check_invariants (TSRMLS_C);
}
// L36645:
L36645:;
// goto L36646;
{
goto L36646;
phc_check_invariants (TSRMLS_C);
}
// L36646:
L36646:;
// $TLE4197 = 1;
{
        if (local_TLE4197 == NULL)
    {
      local_TLE4197 = EG (uninitialized_zval_ptr);
      local_TLE4197->refcount++;
    }
  zval** p_lhs = &local_TLE4197;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE4198 = '';
{
        if (local_TLE4198 == NULL)
    {
      local_TLE4198 = EG (uninitialized_zval_ptr);
      local_TLE4198->refcount++;
    }
  zval** p_lhs = &local_TLE4198;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE33482 = param_is_ref (this, "makelink", 0);
// ;
{
   initialize_function_call (&makelink_fci, &makelink_fcic, "makelink", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = makelink_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE33482 == NULL)
    {
      local_TLE33482 = EG (uninitialized_zval_ptr);
      local_TLE33482->refcount++;
    }
  zval** p_lhs = &local_TLE33482;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE33482) goto L36647 else goto L36648;
{
     zval* p_cond;
  if (local_TLE33482 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE33482;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L36647;
   else
      goto L36648;
phc_check_invariants (TSRMLS_C);
}
// L36647:
L36647:;
// $TMIi33481 =& $match[$TLE4197];
{
     if (local_TMIi33481 == NULL)
    {
      local_TMIi33481 = EG (uninitialized_zval_ptr);
      local_TMIi33481->refcount++;
    }
  zval** p_lhs = &local_TMIi33481;

     if (local_match == NULL)
    {
      local_match = EG (uninitialized_zval_ptr);
      local_match->refcount++;
    }
  zval** p_r_array = &local_match;

     zval* r_index;
  if (local_TLE4197 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4197;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36649;
{
goto L36649;
phc_check_invariants (TSRMLS_C);
}
// L36648:
L36648:;
// $TMIi33481 = $match[$TLE4197];
{
     if (local_TMIi33481 == NULL)
    {
      local_TMIi33481 = EG (uninitialized_zval_ptr);
      local_TMIi33481->refcount++;
    }
  zval** p_lhs = &local_TMIi33481;

     zval* r_array;
  if (local_match == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_match;

     zval* r_index;
  if (local_TLE4197 == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_TLE4197;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// goto L36649;
{
goto L36649;
phc_check_invariants (TSRMLS_C);
}
// L36649:
L36649:;
// $thelink = $this->makelink($TMIi33481, $text, $TLE4198, $trail);
{
     if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "makelink", "Parser2.php", 2710 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[4];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [4];
   zval* args [4];
   zval** args_ind [4];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIi33481 == NULL)
    {
      local_TMIi33481 = EG (uninitialized_zval_ptr);
      local_TMIi33481->refcount++;
    }
  zval** p_arg = &local_TMIi33481;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIi33481 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIi33481;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_text == NULL)
    {
      local_text = EG (uninitialized_zval_ptr);
      local_text->refcount++;
    }
  zval** p_arg = &local_text;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_text == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_text;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4198 == NULL)
    {
      local_TLE4198 = EG (uninitialized_zval_ptr);
      local_TLE4198->refcount++;
    }
  zval** p_arg = &local_TLE4198;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4198 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4198;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_trail == NULL)
    {
      local_trail = EG (uninitialized_zval_ptr);
      local_trail->refcount++;
    }
  zval** p_arg = &local_trail;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_trail == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_trail;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2710, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 4;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 4; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_thelink == NULL)
    {
      local_thelink = EG (uninitialized_zval_ptr);
      local_thelink->refcount++;
    }
  zval** p_lhs = &local_thelink;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// goto L36652;
{
goto L36652;
phc_check_invariants (TSRMLS_C);
}
// L36652:
L36652:;
// $TLE4199 = stringutils::escaperegexreplacement($thelink);
{
   initialize_function_call (&stringutils_escaperegexreplacement_fci, &stringutils_escaperegexreplacement_fcic, "stringutils::escaperegexreplacement", "Parser2.php", 2712 TSRMLS_CC);
      zend_function* signature = stringutils_escaperegexreplacement_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_thelink == NULL)
    {
      local_thelink = EG (uninitialized_zval_ptr);
      local_thelink->refcount++;
    }
  zval** p_arg = &local_thelink;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_thelink == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_thelink;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2712, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = stringutils_escaperegexreplacement_fci.param_count;
   zval*** params_save = stringutils_escaperegexreplacement_fci.params;
   zval** retval_save = stringutils_escaperegexreplacement_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   stringutils_escaperegexreplacement_fci.params = args_ind;
   stringutils_escaperegexreplacement_fci.param_count = 1;
   stringutils_escaperegexreplacement_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&stringutils_escaperegexreplacement_fci, &stringutils_escaperegexreplacement_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   stringutils_escaperegexreplacement_fci.params = params_save;
   stringutils_escaperegexreplacement_fci.param_count = param_count_save;
   stringutils_escaperegexreplacement_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE4199 == NULL)
    {
      local_TLE4199 = EG (uninitialized_zval_ptr);
      local_TLE4199->refcount++;
    }
  zval** p_lhs = &local_TLE4199;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE4200 = 1;
{
        if (local_TLE4200 == NULL)
    {
      local_TLE4200 = EG (uninitialized_zval_ptr);
      local_TLE4200->refcount++;
    }
  zval** p_lhs = &local_TLE4200;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $comment = preg_replace($linkRegexp, $TLE4199, $comment, $TLE4200);
{
   initialize_function_call (&preg_replace_fci, &preg_replace_fcic, "preg_replace", "Parser2.php", 2712 TSRMLS_CC);
      zend_function* signature = preg_replace_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[4];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [4];
   zval* args [4];
   zval** args_ind [4];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_linkRegexp == NULL)
    {
      local_linkRegexp = EG (uninitialized_zval_ptr);
      local_linkRegexp->refcount++;
    }
  zval** p_arg = &local_linkRegexp;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_linkRegexp == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_linkRegexp;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4199 == NULL)
    {
      local_TLE4199 = EG (uninitialized_zval_ptr);
      local_TLE4199->refcount++;
    }
  zval** p_arg = &local_TLE4199;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4199 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4199;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_comment == NULL)
    {
      local_comment = EG (uninitialized_zval_ptr);
      local_comment->refcount++;
    }
  zval** p_arg = &local_comment;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_comment == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_comment;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE4200 == NULL)
    {
      local_TLE4200 = EG (uninitialized_zval_ptr);
      local_TLE4200->refcount++;
    }
  zval** p_arg = &local_TLE4200;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE4200 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE4200;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "Parser2.php", 2712, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = preg_replace_fci.param_count;
   zval*** params_save = preg_replace_fci.params;
   zval** retval_save = preg_replace_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   preg_replace_fci.params = args_ind;
   preg_replace_fci.param_count = 4;
   preg_replace_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&preg_replace_fci, &preg_replace_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   preg_replace_fci.params = params_save;
   preg_replace_fci.param_count = param_count_save;
   preg_replace_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 4; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_comment == NULL)
    {
      local_comment = EG (uninitialized_zval_ptr);
      local_comment->refcount++;
    }
  zval** p_lhs = &local_comment;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $comment;
{
     zval* rhs;
  if (local_comment == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_comment;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:/*__attribute__((unused))*/;
if (local_TEF477 != NULL)
{
zval_ptr_dtor (&local_TEF477);
}
if (local_TLE33469 != NULL)
{
zval_ptr_dtor (&local_TLE33469);
}
if (local_TLE33471 != NULL)
{
zval_ptr_dtor (&local_TLE33471);
}
if (local_TLE33473 != NULL)
{
zval_ptr_dtor (&local_TLE33473);
}
if (local_TLE33475 != NULL)
{
zval_ptr_dtor (&local_TLE33475);
}
if (local_TLE33477 != NULL)
{
zval_ptr_dtor (&local_TLE33477);
}
if (local_TLE33480 != NULL)
{
zval_ptr_dtor (&local_TLE33480);
}
if (local_TLE33482 != NULL)
{
zval_ptr_dtor (&local_TLE33482);
}
if (local_TLE4128 != NULL)
{
zval_ptr_dtor (&local_TLE4128);
}
if (local_TLE4129 != NULL)
{
zval_ptr_dtor (&local_TLE4129);
}
if (local_TLE4130 != NULL)
{
zval_ptr_dtor (&local_TLE4130);
}
if (local_TLE4131 != NULL)
{
zval_ptr_dtor (&local_TLE4131);
}
if (local_TLE4132 != NULL)
{
zval_ptr_dtor (&local_TLE4132);
}
if (local_TLE4133 != NULL)
{
zval_ptr_dtor (&local_TLE4133);
}
if (local_TLE4134 != NULL)
{
zval_ptr_dtor (&local_TLE4134);
}
if (local_TLE4135 != NULL)
{
zval_ptr_dtor (&local_TLE4135);
}
if (local_TLE4136 != NULL)
{
zval_ptr_dtor (&local_TLE4136);
}
if (local_TLE4137 != NULL)
{
zval_ptr_dtor (&local_TLE4137);
}
if (local_TLE4138 != NULL)
{
zval_ptr_dtor (&local_TLE4138);
}
if (local_TLE4139 != NULL)
{
zval_ptr_dtor (&local_TLE4139);
}
if (local_TLE4140 != NULL)
{
zval_ptr_dtor (&local_TLE4140);
}
if (local_TLE4141 != NULL)
{
zval_ptr_dtor (&local_TLE4141);
}
if (local_TLE4143 != NULL)
{
zval_ptr_dtor (&local_TLE4143);
}
if (local_TLE4144 != NULL)
{
zval_ptr_dtor (&local_TLE4144);
}
if (local_TLE4145 != NULL)
{
zval_ptr_dtor (&local_TLE4145);
}
if (local_TLE4146 != NULL)
{
zval_ptr_dtor (&local_TLE4146);
}
if (local_TLE4147 != NULL)
{
zval_ptr_dtor (&local_TLE4147);
}
if (local_TLE4148 != NULL)
{
zval_ptr_dtor (&local_TLE4148);
}
if (local_TLE4149 != NULL)
{
zval_ptr_dtor (&local_TLE4149);
}
if (local_TLE4150 != NULL)
{
zval_ptr_dtor (&local_TLE4150);
}
if (local_TLE4152 != NULL)
{
zval_ptr_dtor (&local_TLE4152);
}
if (local_TLE4153 != NULL)
{
zval_ptr_dtor (&local_TLE4153);
}
if (local_TLE4155 != NULL)
{
zval_ptr_dtor (&local_TLE4155);
}
if (local_TLE4156 != NULL)
{
zval_ptr_dtor (&local_TLE4156);
}
if (local_TLE4157 != NULL)
{
zval_ptr_dtor (&local_TLE4157);
}
if (local_TLE4158 != NULL)
{
zval_ptr_dtor (&local_TLE4158);
}
if (local_TLE4159 != NULL)
{
zval_ptr_dtor (&local_TLE4159);
}
if (local_TLE4161 != NULL)
{
zval_ptr_dtor (&local_TLE4161);
}
if (local_TLE4162 != NULL)
{
zval_ptr_dtor (&local_TLE4162);
}
if (local_TLE4164 != NULL)
{
zval_ptr_dtor (&local_TLE4164);
}
if (local_TLE4167 != NULL)
{
zval_ptr_dtor (&local_TLE4167);
}
if (local_TLE4168 != NULL)
{
zval_ptr_dtor (&local_TLE4168);
}
if (local_TLE4169 != NULL)
{
zval_ptr_dtor (&local_TLE4169);
}
if (local_TLE4170 != NULL)
{
zval_ptr_dtor (&local_TLE4170);
}
if (local_TLE4171 != NULL)
{
zval_ptr_dtor (&local_TLE4171);
}
if (local_TLE4172 != NULL)
{
zval_ptr_dtor (&local_TLE4172);
}
if (local_TLE4173 != NULL)
{
zval_ptr_dtor (&local_TLE4173);
}
if (local_TLE4174 != NULL)
{
zval_ptr_dtor (&local_TLE4174);
}
if (local_TLE4175 != NULL)
{
zval_ptr_dtor (&local_TLE4175);
}
if (local_TLE4176 != NULL)
{
zval_ptr_dtor (&local_TLE4176);
}
if (local_TLE4177 != NULL)
{
zval_ptr_dtor (&local_TLE4177);
}
if (local_TLE4178 != NULL)
{
zval_ptr_dtor (&local_TLE4178);
}
if (local_TLE4180 != NULL)
{
zval_ptr_dtor (&local_TLE4180);
}
if (local_TLE4181 != NULL)
{
zval_ptr_dtor (&local_TLE4181);
}
if (local_TLE4182 != NULL)
{
zval_ptr_dtor (&local_TLE4182);
}
if (local_TLE4183 != NULL)
{
zval_ptr_dtor (&local_TLE4183);
}
if (local_TLE4184 != NULL)
{
zval_ptr_dtor (&local_TLE4184);
}
if (local_TLE4185 != NULL)
{
zval_ptr_dtor (&local_TLE4185);
}
if (local_TLE4186 != NULL)
{
zval_ptr_dtor (&local_TLE4186);
}
if (local_TLE4187 != NULL)
{
zval_ptr_dtor (&local_TLE4187);
}
if (local_TLE4188 != NULL)
{
zval_ptr_dtor (&local_TLE4188);
}
if (local_TLE4191 != NULL)
{
zval_ptr_dtor (&local_TLE4191);
}
if (local_TLE4192 != NULL)
{
zval_ptr_dtor (&local_TLE4192);
}
if (local_TLE4193 != NULL)
{
zval_ptr_dtor (&local_TLE4193);
}
if (local_TLE4194 != NULL)
{
zval_ptr_dtor (&local_TLE4194);
}
if (local_TLE4195 != NULL)
{
zval_ptr_dtor (&local_TLE4195);
}
if (local_TLE4196 != NULL)
{
zval_ptr_dtor (&local_TLE4196);
}
if (local_TLE4197 != NULL)
{
zval_ptr_dtor (&local_TLE4197);
}
if (local_TLE4198 != NULL)
{
zval_ptr_dtor (&local_TLE4198);
}
if (local_TLE4199 != NULL)
{
zval_ptr_dtor (&local_TLE4199);
}
if (local_TLE4200 != NULL)
{
zval_ptr_dtor (&local_TLE4200);
}
if (local_TLE476 != NULL)
{
zval_ptr_dtor (&local_TLE476);
}
if (local_TMIi33468 != NULL)
{
zval_ptr_dtor (&local_TMIi33468);
}
if (local_TMIi33470 != NULL)
{
zval_ptr_dtor (&local_TMIi33470);
}
if (local_TMIi33472 != NULL)
{
zval_ptr_dtor (&local_TMIi33472);
}
if (local_TMIi33474 != NULL)
{
zval_ptr_dtor (&local_TMIi33474);
}
if (local_TMIi33476 != NULL)
{
zval_ptr_dtor (&local_TMIi33476);
}
if (local_TMIi33478 != NULL)
{
zval_ptr_dtor (&local_TMIi33478);
}
if (local_TMIi33479 != NULL)
{
zval_ptr_dtor (&local_TMIi33479);
}
if (local_TMIi33481 != NULL)
{
zval_ptr_dtor (&local_TMIi33481);
}
if (local_TSa4151 != NULL)
{
zval_ptr_dtor (&local_TSa4151);
}
if (local_TSa4154 != NULL)
{
zval_ptr_dtor (&local_TSa4154);
}
if (local_TSa4166 != NULL)
{
zval_ptr_dtor (&local_TSa4166);
}
if (local_TSi4142 != NULL)
{
zval_ptr_dtor (&local_TSi4142);
}
if (local_TSi4160 != NULL)
{
zval_ptr_dtor (&local_TSi4160);
}
if (local_TSi4163 != NULL)
{
zval_ptr_dtor (&local_TSi4163);
}
if (local_TSi4165 != NULL)
{
zval_ptr_dtor (&local_TSi4165);
}
if (local_TSi4179 != NULL)
{
zval_ptr_dtor (&local_TSi4179);
}
if (local_TSi4189 != NULL)
{
zval_ptr_dtor (&local_TSi4189);
}
if (local_TSi4190 != NULL)
{
zval_ptr_dtor (&local_TSi4190);
}
if (local_comment != NULL)
{
zval_ptr_dtor (&local_comment);
}
if (local_linkRegexp != NULL)
{
zval_ptr_dtor (&local_linkRegexp);
}
if (local_match != NULL)
{
zval_ptr_dtor (&local_match);
}
if (local_medians != NULL)
{
zval_ptr_dtor (&local_medians);
}
if (local_submatch != NULL)
{
zval_ptr_dtor (&local_submatch);
}
if (local_text != NULL)
{
zval_ptr_dtor (&local_text);
}
if (local_thelink != NULL)
{
zval_ptr_dtor (&local_thelink);
}
if (local_trail != NULL)
{
zval_ptr_dtor (&local_trail);
}
if (local_wgContLang != NULL)
{
zval_ptr_dtor (&local_wgContLang);
}
}
// function commentblock($comment, $title = NULL, $local = False)
// {
// 	$TLE4201 = '';
// 	$TLE478 = ($comment == $TLE4201);
// 	if (TLE478) goto L36653 else goto L36654;
// L36653:
// 	$TEF479 = $TLE478;
// 	goto L36655;
// L36654:
// 	$TLE4202 = '*';
// 	$TEF479 = ($comment == $TLE4202);
// 	goto L36655;
// L36655:
// 	$TLE4203 = (bool) $TEF479;
// 	if (TLE4203) goto L36656 else goto L36657;
// L36656:
// 	$TLE4204 = '';
// 	return $TLE4204;
// 	goto L36658;
// L36657:
// 	$formatted = $this->formatcomment($comment, $title, $local);
// 	$TLE4205 = ' <span class="comment">(';
// 	$TLE4206 = ($TLE4205 . $formatted);
// 	$TLE4207 = ')</span>';
// 	$TLE4208 = ($TLE4206 . $TLE4207);
// 	return $TLE4208;
// 	goto L36658;
// L36658:
// }
