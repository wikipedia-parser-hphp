
#ifndef __GENERATED_cls_sanitizer_h__
#define __GENERATED_cls_sanitizer_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 17064 */
class c_sanitizer : virtual public ObjectData {
  BEGIN_CLASS_MAP(sanitizer)
  END_CLASS_MAP(sanitizer)
  DECLARE_CLASS(sanitizer, Sanitizer, ObjectData)
  void init();
  public: static Variant ti_removehtmltags(const char* cls, Variant v_text, Variant v_processCallback = null, Variant v_args = ScalarArrays::sa_[0], Variant v_extratags = ScalarArrays::sa_[0]);
  public: static Variant ti_removehtmlcomments(const char* cls, Variant v_text);
  public: static Variant ti_validatetagattributes(const char* cls, CVarRef v_attribs, CVarRef v_element);
  public: static Variant ti_validateattributes(const char* cls, CVarRef v_attribs, Variant v_whitelist);
  public: static Variant ti_mergeattributes(const char* cls, CVarRef v_a, CVarRef v_b);
  public: static Variant ti_checkcss(const char* cls, Variant v_value);
  public: static Variant ti_fixtagattributes(const char* cls, CVarRef v_text, CVarRef v_element);
  public: static String ti_encodeattribute(const char* cls, CVarRef v_text);
  public: static Variant ti_safeencodeattribute(const char* cls, CVarRef v_text);
  public: static Variant ti_escapeid(const char* cls, Variant v_id, Variant v_options = ScalarArrays::sa_[0]);
  public: static String ti_escapeclass(const char* cls, CVarRef v_class);
  public: static Variant ti_escapehtmlallowentities(const char* cls, Variant v_html);
  public: static Variant ti_armorlinkscallback(const char* cls, CVarRef v_matches);
  public: static Variant ti_decodetagattributes(const char* cls, CVarRef v_text);
  public: static Variant ti_gettagattributecallback(const char* cls, CVarRef v_set);
  public: static Variant ti_normalizeattributevalue(const char* cls, CVarRef v_text);
  public: static Variant ti_normalizewhitespace(const char* cls, CVarRef v_text);
  public: static Variant ti_normalizecharreferences(const char* cls, CVarRef v_text);
  public: static Variant ti_normalizecharreferencescallback(const char* cls, CVarRef v_matches);
  public: static Variant ti_normalizeentity(const char* cls, CVarRef v_name);
  public: static Variant ti_deccharreference(const char* cls, CVarRef v_codepoint);
  public: static Variant ti_hexcharreference(const char* cls, CVarRef v_codepoint);
  public: static bool ti_validatecodepoint(const char* cls, CVarRef v_codepoint);
  public: static Variant ti_decodecharreferences(const char* cls, CVarRef v_text);
  public: static Variant ti_decodecharreferencescallback(const char* cls, CVarRef v_matches);
  public: static Variant ti_decodechar(const char* cls, CVarRef v_codepoint);
  public: static Variant ti_decodeentity(const char* cls, Variant v_name);
  public: static Variant ti_attributewhitelist(const char* cls, CVarRef v_element);
  public: static Array ti_setupattributewhitelist(const char* cls);
  public: static Variant ti_stripalltags(const char* cls, Variant v_text);
  public: static String ti_hackdoctype(const char* cls);
  public: static Variant ti_cleanurl(const char* cls, Variant v_url);
  public: static Variant t_decodechar(CVarRef v_codepoint) { return ti_decodechar("sanitizer", v_codepoint); }
  public: static Variant t_removehtmltags(CVarRef v_text, CVarRef v_processCallback = null_variant, CVarRef v_args = ScalarArrays::sa_[0], CVarRef v_extratags = ScalarArrays::sa_[0]) { return ti_removehtmltags("sanitizer", v_text, v_processCallback, v_args, v_extratags); }
  public: static Variant t_fixtagattributes(CVarRef v_text, CVarRef v_element) { return ti_fixtagattributes("sanitizer", v_text, v_element); }
  public: static Variant t_attributewhitelist(CVarRef v_element) { return ti_attributewhitelist("sanitizer", v_element); }
  public: static Variant t_safeencodeattribute(CVarRef v_text) { return ti_safeencodeattribute("sanitizer", v_text); }
  public: static Variant t_escapeid(CVarRef v_id, CVarRef v_options = ScalarArrays::sa_[0]) { return ti_escapeid("sanitizer", v_id, v_options); }
  public: static Array t_setupattributewhitelist() { return ti_setupattributewhitelist("sanitizer"); }
  public: static String t_escapeclass(CVarRef v_class) { return ti_escapeclass("sanitizer", v_class); }
  public: static String t_hackdoctype() { return ti_hackdoctype("sanitizer"); }
  public: static Variant t_hexcharreference(CVarRef v_codepoint) { return ti_hexcharreference("sanitizer", v_codepoint); }
  public: static Variant t_normalizecharreferences(CVarRef v_text) { return ti_normalizecharreferences("sanitizer", v_text); }
  public: static Variant t_cleanurl(CVarRef v_url) { return ti_cleanurl("sanitizer", v_url); }
  public: static Variant t_mergeattributes(CVarRef v_a, CVarRef v_b) { return ti_mergeattributes("sanitizer", v_a, v_b); }
  public: static Variant t_gettagattributecallback(CVarRef v_set) { return ti_gettagattributecallback("sanitizer", v_set); }
  public: static Variant t_validateattributes(CVarRef v_attribs, CVarRef v_whitelist) { return ti_validateattributes("sanitizer", v_attribs, v_whitelist); }
  public: static Variant t_armorlinkscallback(CVarRef v_matches) { return ti_armorlinkscallback("sanitizer", v_matches); }
  public: static Variant t_validatetagattributes(CVarRef v_attribs, CVarRef v_element) { return ti_validatetagattributes("sanitizer", v_attribs, v_element); }
  public: static Variant t_decodeentity(CVarRef v_name) { return ti_decodeentity("sanitizer", v_name); }
  public: static Variant t_stripalltags(CVarRef v_text) { return ti_stripalltags("sanitizer", v_text); }
  public: static Variant t_normalizeentity(CVarRef v_name) { return ti_normalizeentity("sanitizer", v_name); }
  public: static bool t_validatecodepoint(CVarRef v_codepoint) { return ti_validatecodepoint("sanitizer", v_codepoint); }
  public: static Variant t_removehtmlcomments(CVarRef v_text) { return ti_removehtmlcomments("sanitizer", v_text); }
  public: static Variant t_decodecharreferences(CVarRef v_text) { return ti_decodecharreferences("sanitizer", v_text); }
  public: static String t_encodeattribute(CVarRef v_text) { return ti_encodeattribute("sanitizer", v_text); }
  public: static Variant t_deccharreference(CVarRef v_codepoint) { return ti_deccharreference("sanitizer", v_codepoint); }
  public: static Variant t_escapehtmlallowentities(CVarRef v_html) { return ti_escapehtmlallowentities("sanitizer", v_html); }
  public: static Variant t_normalizewhitespace(CVarRef v_text) { return ti_normalizewhitespace("sanitizer", v_text); }
  public: static Variant t_decodecharreferencescallback(CVarRef v_matches) { return ti_decodecharreferencescallback("sanitizer", v_matches); }
  public: static Variant t_checkcss(CVarRef v_value) { return ti_checkcss("sanitizer", v_value); }
  public: static Variant t_decodetagattributes(CVarRef v_text) { return ti_decodetagattributes("sanitizer", v_text); }
  public: static Variant t_normalizecharreferencescallback(CVarRef v_matches) { return ti_normalizecharreferencescallback("sanitizer", v_matches); }
  public: static Variant t_normalizeattributevalue(CVarRef v_text) { return ti_normalizeattributevalue("sanitizer", v_text); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_sanitizer_h__
