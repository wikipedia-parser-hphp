
#ifndef __GENERATED_cls_dateformatter_h__
#define __GENERATED_cls_dateformatter_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: DateFormatter.php line 8 */
class c_dateformatter : virtual public ObjectData {
  BEGIN_CLASS_MAP(dateformatter)
  END_CLASS_MAP(dateformatter)
  DECLARE_CLASS(dateformatter, DateFormatter, ObjectData)
  void init();
  public: Variant m_mSource;
  public: Variant m_mTarget;
  public: Variant m_monthNames;
  public: Variant m_rxDM;
  public: Variant m_rxMD;
  public: Variant m_rxDMY;
  public: Variant m_rxYDM;
  public: Variant m_rxMDY;
  public: Variant m_rxYMD;
  public: Variant m_regexes;
  public: Variant m_pDays;
  public: Variant m_pMonths;
  public: Variant m_pYears;
  public: Variant m_rules;
  public: Variant m_xMonths;
  public: Variant m_preferences;
  public: void t_dateformatter();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static Variant ti_getinstance(const char* cls);
  public: Variant t_reformat(Variant v_preference, Variant v_text, CVarRef v_options = ScalarArrays::sa_[34]);
  public: Variant t_replace(CVarRef v_matches);
  public: Variant t_formatdate(Variant v_bits, CVarRef v_link = true);
  public: String t_getmonthregex();
  public: String t_makeisomonth(Variant v_monthName);
  public: String t_makeisoyear(CVarRef v_year);
  public: Variant t_makenormalyear(CVarRef v_iso);
  public: static Variant t_getinstance() { return ti_getinstance("dateformatter"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_dateformatter_h__
