
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  "corelinkfunctions", "CoreLinkFunctions.php",
  "coreparserfunctions", "CoreParserFunctions.php",
  "dateformatter", "DateFormatter.php",
  "linkholderarray", "LinkHolderArray.php",
  "linkmarkerreplacer", "Parser_LinkHooks.php",
  "mwtidy", "Tidy.php",
  "onlyincludereplacer", "Parser.php",
  "parser", "Parser.php",
  "parser_difftest", "Parser_DiffTest.php",
  "parser_linkhooks", "Parser_LinkHooks.php",
  "parsercache", "ParserCache.php",
  "parseroptions", "ParserOptions.php",
  "parseroutput", "ParserOutput.php",
  "ppcustomframe_dom", "Preprocessor_DOM.php",
  "ppcustomframe_hash", "Preprocessor_Hash.php",
  "ppdaccum_hash", "Preprocessor_Hash.php",
  "ppdpart", "Preprocessor_DOM.php",
  "ppdpart_hash", "Preprocessor_Hash.php",
  "ppdstack", "Preprocessor_DOM.php",
  "ppdstack_hash", "Preprocessor_Hash.php",
  "ppdstackelement", "Preprocessor_DOM.php",
  "ppdstackelement_hash", "Preprocessor_Hash.php",
  "ppframe", "Preprocessor.php",
  "ppframe_dom", "Preprocessor_DOM.php",
  "ppframe_hash", "Preprocessor_Hash.php",
  "ppnode", "Preprocessor.php",
  "ppnode_dom", "Preprocessor_DOM.php",
  "ppnode_hash_array", "Preprocessor_Hash.php",
  "ppnode_hash_attr", "Preprocessor_Hash.php",
  "ppnode_hash_text", "Preprocessor_Hash.php",
  "ppnode_hash_tree", "Preprocessor_Hash.php",
  "pptemplateframe_dom", "Preprocessor_DOM.php",
  "pptemplateframe_hash", "Preprocessor_Hash.php",
  "preprocessor", "Preprocessor.php",
  "preprocessor_dom", "Preprocessor_DOM.php",
  "preprocessor_hash", "Preprocessor_Hash.php",
  "stripstate", "Parser.php",
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
