
#include <php/CoreLinkFunctions.h>
#include <php/CoreParserFunctions.h>
#include <php/LinkHolderArray.h>
#include <php/Parser.h>
#include <php/Parser_LinkHooks.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const Variant k_SLH_PATTERN = "SLH_PATTERN";

/* preface starts */
inline Variant df_lambda_4(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
inline Variant df_lambda_5(CVarRef aa, Variant &p0, Variant &p1) {
  Variant a0; if (aa.is(KindOfArray)) a0 = aa;
p0 = a0.rvalAt(0);
  p1 = a0.rvalAt(1);
  return aa;
  
  }
/* preface finishes */
/* SRC: Parser_LinkHooks.php line 6 */
const StaticString q_parser_linkhooks_VERSION = "1.6.4";
const int64 q_parser_linkhooks_SLH_PATTERN = 1LL;
const StaticString q_parser_linkhooks_EXT_LINK_URL_CLASS = "[^][<>\"\\x00-\\x20\\x7F]";
const StaticString q_parser_linkhooks_EXT_IMAGE_REGEX = "/^(http:\\/\\/|https:\\/\\/)([^][<>\"\\x00-\\x20\\x7F]+)\n\t\t\\/([A-Za-z0-9_.,~%\\-+&;#*\?!=()@\\x80-\\xFF]+)\\.((\?i)gif|png|jpg|jpeg)$/Sx";
Variant c_parser_linkhooks::os_get(const char *s, int64 hash) {
  return c_parser::os_get(s, hash);
}
Variant &c_parser_linkhooks::os_lval(const char *s, int64 hash) {
  return c_parser::os_lval(s, hash);
}
void c_parser_linkhooks::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("mLinkHooks", m_mLinkHooks.isReferenced() ? ref(m_mLinkHooks) : m_mLinkHooks));
  c_parser::o_get(props);
}
bool c_parser_linkhooks::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_EXISTS_STRING(0x6AEEA98517405037LL, mLinkHooks, 10);
      break;
    default:
      break;
  }
  return c_parser::o_exists(s, hash);
}
Variant c_parser_linkhooks::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x6AEEA98517405037LL, m_mLinkHooks,
                         mLinkHooks, 10);
      break;
    default:
      break;
  }
  return c_parser::o_get(s, hash);
}
Variant c_parser_linkhooks::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_SET_STRING(0x6AEEA98517405037LL, m_mLinkHooks,
                      mLinkHooks, 10);
      break;
    default:
      break;
  }
  return c_parser::o_set(s, hash, v, forInit);
}
Variant &c_parser_linkhooks::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 1) {
    case 1:
      HASH_RETURN_STRING(0x6AEEA98517405037LL, m_mLinkHooks,
                         mLinkHooks, 10);
      break;
    default:
      break;
  }
  return c_parser::o_lval(s, hash);
}
Variant c_parser_linkhooks::os_constant(const char *s) {
  int64 hash = hash_string(s);
  switch (hash & 7) {
    case 0:
      HASH_RETURN(0x63F95DCAC9867D78LL, q_parser_linkhooks_EXT_LINK_URL_CLASS, EXT_LINK_URL_CLASS);
      break;
    case 2:
      HASH_RETURN(0x5AE41239FF63D86ALL, q_parser_linkhooks_VERSION, VERSION);
      HASH_RETURN(0x53D1BB8BE4436E6ALL, q_parser_linkhooks_EXT_IMAGE_REGEX, EXT_IMAGE_REGEX);
      break;
    case 5:
      HASH_RETURN(0x2E2C33121835E575LL, q_parser_linkhooks_SLH_PATTERN, SLH_PATTERN);
      break;
    default:
      break;
  }
  return c_parser::os_constant(s);
}
IMPLEMENT_CLASS(parser_linkhooks)
ObjectData *c_parser_linkhooks::create(Variant v_conf //  = ScalarArrays::sa_[0]
) {
  init();
  t___construct(v_conf);
  return this;
}
ObjectData *c_parser_linkhooks::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_parser_linkhooks::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_parser_linkhooks::cloneImpl() {
  c_parser_linkhooks *obj = NEW(c_parser_linkhooks)();
  cloneSet(obj);
  return obj;
}
void c_parser_linkhooks::cloneSet(c_parser_linkhooks *clone) {
  clone->m_mLinkHooks = m_mLinkHooks.isReferenced() ? ref(m_mLinkHooks) : m_mLinkHooks;
  c_parser::cloneSet(clone);
}
Variant c_parser_linkhooks::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 5:
      HASH_GUARD(0x04CA531DD3FB2705LL, attributestripcallback) {
        int count = params.size();
        if (count <= 1) return (t_attributestripcallback(ref(const_cast<Array&>(params).lvalAt(0))));
        return (t_attributestripcallback(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)));
      }
      HASH_GUARD(0x298D74DE5F3B8B45LL, magiclinkcallback) {
        return (t_magiclinkcallback(params.rvalAt(0)));
      }
      break;
    case 17:
      HASH_GUARD(0x333DE406BD9436D1LL, argsubstitution) {
        return (t_argsubstitution(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 19:
      HASH_GUARD(0x6159C1C3A582E4D3LL, incrementexpensivefunctioncount) {
        return (t_incrementexpensivefunctioncount());
      }
      break;
    case 20:
      HASH_GUARD(0x07B2DE5DC3BC16D4LL, setfunctionhook) {
        int count = params.size();
        if (count <= 2) return (t_setfunctionhook(params.rvalAt(0), params.rvalAt(1)));
        return (t_setfunctionhook(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      HASH_GUARD(0x4AC114BB40027BD4LL, renderpretag) {
        return (t_renderpretag(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 23:
      HASH_GUARD(0x7519C1FAF33D7857LL, getoutput) {
        return (t_getoutput());
      }
      break;
    case 24:
      HASH_GUARD(0x1919E33A3EB87298LL, markerskipcallback) {
        return (t_markerskipcallback(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 30:
      HASH_GUARD(0x565D9E5A4801A09ELL, getcustomdefaultsort) {
        return (t_getcustomdefaultsort());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    case 34:
      HASH_GUARD(0x0A4CA37F020D70E2LL, insertstripitem) {
        return (t_insertstripitem(params.rvalAt(0)));
      }
      break;
    case 35:
      HASH_GUARD(0x7159A008E3EA5B23LL, replaceinternallinkscallback) {
        return (t_replaceinternallinkscallback(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)));
      }
      break;
    case 37:
      HASH_GUARD(0x443229B8642123A5LL, statelessfetchtemplate) {
        int count = params.size();
        if (count <= 1) return (ti_statelessfetchtemplate(o_getClassName(), params.rvalAt(0)));
        return (ti_statelessfetchtemplate(o_getClassName(), params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 39:
      HASH_GUARD(0x6EFD1FF8956DBC27LL, nextlinkid) {
        return (t_nextlinkid());
      }
      break;
    case 41:
      HASH_GUARD(0x7D371E851A4798E9LL, extensionsubstitution) {
        return (t_extensionsubstitution(params.rvalAt(0), params.rvalAt(1)));
      }
      HASH_GUARD(0x2374179DF576FCA9LL, unstripnowiki) {
        return (t_unstripnowiki(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 43:
      HASH_GUARD(0x132FD54E0D0A99EBLL, title) {
        int count = params.size();
        if (count <= 0) return (t_title());
        return (t_title(params.rvalAt(0)));
      }
      break;
    case 47:
      HASH_GUARD(0x3BBB4BCA49D08AEFLL, getfunctionlang) {
        return (t_getfunctionlang());
      }
      HASH_GUARD(0x7E4180EEBEB7A62FLL, doquotes) {
        return (t_doquotes(params.rvalAt(0)));
      }
      break;
    case 50:
      HASH_GUARD(0x0FE91E46B43AEF32LL, setdefaultsort) {
        return (t_setdefaultsort(params.rvalAt(0)), null);
      }
      break;
    case 51:
      HASH_GUARD(0x7F974836AACC1EF3LL, __destruct) {
        return (t___destruct());
      }
      break;
    case 53:
      HASH_GUARD(0x151B6C9D84BBC1F5LL, getstriplist) {
        return (t_getstriplist());
      }
      HASH_GUARD(0x241A6011A10C7EF5LL, firstcallinit) {
        return (t_firstcallinit(), null);
      }
      break;
    case 54:
      HASH_GUARD(0x37AA0C66F5C36136LL, replaceinternallinks2) {
        return (t_replaceinternallinks2(ref(const_cast<Array&>(params).lvalAt(0))));
      }
      break;
    case 55:
      HASH_GUARD(0x392E383BDD5B10F7LL, bracesubstitution) {
        return (t_bracesubstitution(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 57:
      HASH_GUARD(0x2040B36587CE56F9LL, getoptions) {
        return (t_getoptions());
      }
      break;
    case 58:
      HASH_GUARD(0x655F4A045CA8303ALL, replacevariables) {
        int count = params.size();
        if (count <= 1) return (t_replacevariables(params.rvalAt(0)));
        if (count == 2) return (t_replacevariables(params.rvalAt(0), params.rvalAt(1)));
        return (t_replacevariables(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
      }
      HASH_GUARD(0x39ADF3C1E6A55D3ALL, getdefaultsort) {
        return (t_getdefaultsort());
      }
      HASH_GUARD(0x524D522D7D9607FALL, uniqprefix) {
        return (t_uniqprefix());
      }
      break;
    default:
      break;
  }
  return c_parser::o_invoke(s, params, hash, fatal);
}
Variant c_parser_linkhooks::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 63) {
    case 5:
      HASH_GUARD(0x04CA531DD3FB2705LL, attributestripcallback) {
        if (count <= 1) return (t_attributestripcallback(ref(a0)));
        return (t_attributestripcallback(ref(a0), a1));
      }
      HASH_GUARD(0x298D74DE5F3B8B45LL, magiclinkcallback) {
        return (t_magiclinkcallback(a0));
      }
      break;
    case 17:
      HASH_GUARD(0x333DE406BD9436D1LL, argsubstitution) {
        return (t_argsubstitution(a0, a1));
      }
      break;
    case 19:
      HASH_GUARD(0x6159C1C3A582E4D3LL, incrementexpensivefunctioncount) {
        return (t_incrementexpensivefunctioncount());
      }
      break;
    case 20:
      HASH_GUARD(0x07B2DE5DC3BC16D4LL, setfunctionhook) {
        if (count <= 2) return (t_setfunctionhook(a0, a1));
        return (t_setfunctionhook(a0, a1, a2));
      }
      HASH_GUARD(0x4AC114BB40027BD4LL, renderpretag) {
        return (t_renderpretag(a0, a1));
      }
      break;
    case 23:
      HASH_GUARD(0x7519C1FAF33D7857LL, getoutput) {
        return (t_getoutput());
      }
      break;
    case 24:
      HASH_GUARD(0x1919E33A3EB87298LL, markerskipcallback) {
        return (t_markerskipcallback(a0, a1));
      }
      break;
    case 30:
      HASH_GUARD(0x565D9E5A4801A09ELL, getcustomdefaultsort) {
        return (t_getcustomdefaultsort());
      }
      break;
    case 31:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    case 34:
      HASH_GUARD(0x0A4CA37F020D70E2LL, insertstripitem) {
        return (t_insertstripitem(a0));
      }
      break;
    case 35:
      HASH_GUARD(0x7159A008E3EA5B23LL, replaceinternallinkscallback) {
        return (t_replaceinternallinkscallback(a0, a1, a2, a3, a4));
      }
      break;
    case 37:
      HASH_GUARD(0x443229B8642123A5LL, statelessfetchtemplate) {
        if (count <= 1) return (ti_statelessfetchtemplate(o_getClassName(), a0));
        return (ti_statelessfetchtemplate(o_getClassName(), a0, a1));
      }
      break;
    case 39:
      HASH_GUARD(0x6EFD1FF8956DBC27LL, nextlinkid) {
        return (t_nextlinkid());
      }
      break;
    case 41:
      HASH_GUARD(0x7D371E851A4798E9LL, extensionsubstitution) {
        return (t_extensionsubstitution(a0, a1));
      }
      HASH_GUARD(0x2374179DF576FCA9LL, unstripnowiki) {
        return (t_unstripnowiki(a0, a1));
      }
      break;
    case 43:
      HASH_GUARD(0x132FD54E0D0A99EBLL, title) {
        if (count <= 0) return (t_title());
        return (t_title(a0));
      }
      break;
    case 47:
      HASH_GUARD(0x3BBB4BCA49D08AEFLL, getfunctionlang) {
        return (t_getfunctionlang());
      }
      HASH_GUARD(0x7E4180EEBEB7A62FLL, doquotes) {
        return (t_doquotes(a0));
      }
      break;
    case 50:
      HASH_GUARD(0x0FE91E46B43AEF32LL, setdefaultsort) {
        return (t_setdefaultsort(a0), null);
      }
      break;
    case 51:
      HASH_GUARD(0x7F974836AACC1EF3LL, __destruct) {
        return (t___destruct());
      }
      break;
    case 53:
      HASH_GUARD(0x151B6C9D84BBC1F5LL, getstriplist) {
        return (t_getstriplist());
      }
      HASH_GUARD(0x241A6011A10C7EF5LL, firstcallinit) {
        return (t_firstcallinit(), null);
      }
      break;
    case 54:
      HASH_GUARD(0x37AA0C66F5C36136LL, replaceinternallinks2) {
        return (t_replaceinternallinks2(ref(a0)));
      }
      break;
    case 55:
      HASH_GUARD(0x392E383BDD5B10F7LL, bracesubstitution) {
        return (t_bracesubstitution(a0, a1));
      }
      break;
    case 57:
      HASH_GUARD(0x2040B36587CE56F9LL, getoptions) {
        return (t_getoptions());
      }
      break;
    case 58:
      HASH_GUARD(0x655F4A045CA8303ALL, replacevariables) {
        if (count <= 1) return (t_replacevariables(a0));
        if (count == 2) return (t_replacevariables(a0, a1));
        return (t_replacevariables(a0, a1, a2));
      }
      HASH_GUARD(0x39ADF3C1E6A55D3ALL, getdefaultsort) {
        return (t_getdefaultsort());
      }
      HASH_GUARD(0x524D522D7D9607FALL, uniqprefix) {
        return (t_uniqprefix());
      }
      break;
    default:
      break;
  }
  return c_parser::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_parser_linkhooks::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 1) {
    case 1:
      HASH_GUARD(0x443229B8642123A5LL, statelessfetchtemplate) {
        int count = params.size();
        if (count <= 1) return (ti_statelessfetchtemplate(c, params.rvalAt(0)));
        return (ti_statelessfetchtemplate(c, params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    default:
      break;
  }
  return c_parser::os_invoke(c, s, params, hash, fatal);
}
Variant cw_parser_linkhooks$os_get(const char *s) {
  return c_parser_linkhooks::os_get(s, -1);
}
Variant &cw_parser_linkhooks$os_lval(const char *s) {
  return c_parser_linkhooks::os_lval(s, -1);
}
Variant cw_parser_linkhooks$os_constant(const char *s) {
  return c_parser_linkhooks::os_constant(s);
}
Variant cw_parser_linkhooks$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_parser_linkhooks::os_invoke(c, s, params, -1, fatal);
}
void c_parser_linkhooks::init() {
  c_parser::init();
  m_mLinkHooks = null;
}
/* SRC: Parser_LinkHooks.php line 38 */
void c_parser_linkhooks::t___construct(Variant v_conf //  = ScalarArrays::sa_[0]
) {
  INSTANCE_METHOD_INJECTION(Parser_LinkHooks, Parser_LinkHooks::__construct);
  bool oldInCtor = gasInCtor(true);
  LINE(39,c_parser::t___construct(v_conf));
  (m_mLinkHooks = ScalarArrays::sa_[0]);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Parser_LinkHooks.php line 46 */
void c_parser_linkhooks::t_firstcallinit() {
  INSTANCE_METHOD_INJECTION(Parser_LinkHooks, Parser_LinkHooks::firstCallInit);
  LINE(47,c_parser::t___construct());
  if (!(toBoolean(o_get("mFirstCall", 0x017DA31DE741B5E9LL)))) {
    return;
  }
  (o_lval("mFirstCall", 0x017DA31DE741B5E9LL) = false);
  LINE(53,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser_LinkHooks::firstCallInit").create()), 0x0000000075359BAFLL));
  LINE(55,t_sethook("pre", Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "renderPreTag").create())));
  LINE(56,c_coreparserfunctions::t_register(p_parser(this)));
  LINE(57,c_corelinkfunctions::t_register(((Object)(this))));
  LINE(58,t_initialisevariables());
  LINE(60,invoke_failed("wfrunhooks", Array(ArrayInit(2).set(0, "ParserFirstCallInit").set(1, Array(ArrayInit(1).setRef(0, ref(this)).create())).create()), 0x00000000787A96B2LL));
  LINE(61,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::firstCallInit").create()), 0x00000000B599F276LL));
} /* function */
/* SRC: Parser_LinkHooks.php line 88 */
Variant c_parser_linkhooks::t_setlinkhook(CVarRef v_ns, CArrRef v_callback, int64 v_flags //  = 0LL
) {
  INSTANCE_METHOD_INJECTION(Parser_LinkHooks, Parser_LinkHooks::setLinkHook);
  Variant v_oldVal;

  if (toBoolean(bitwise_and(v_flags, k_SLH_PATTERN)) && !(LINE(89,x_is_string(v_ns)))) throw_exception(LINE(90,create_object("mwexception", Array(ArrayInit(1).set(0, "Parser_LinkHooks::setLinkHook() expecting a regex string pattern.").create()))));
  else if (toBoolean(bitwise_or(v_flags, ~k_SLH_PATTERN)) && !(LINE(91,x_is_int(v_ns)))) throw_exception(LINE(92,create_object("mwexception", Array(ArrayInit(1).set(0, "Parser_LinkHooks::setLinkHook() expecting a namespace index.").create()))));
  (v_oldVal = isset(m_mLinkHooks, v_ns) ? ((Variant)(m_mLinkHooks.rvalAt(v_ns).rvalAt(0LL, 0x77CFA1EEF01BCA90LL))) : ((Variant)(null)));
  m_mLinkHooks.set(v_ns, (Array(ArrayInit(2).set(0, v_callback).set(1, v_flags).create())));
  return v_oldVal;
} /* function */
/* SRC: Parser_LinkHooks.php line 103 */
Variant c_parser_linkhooks::t_getlinkhooks() {
  INSTANCE_METHOD_INJECTION(Parser_LinkHooks, Parser_LinkHooks::getLinkHooks);
  return LINE(104,x_array_keys(m_mLinkHooks));
} /* function */
/* SRC: Parser_LinkHooks.php line 113 */
p_linkholderarray c_parser_linkhooks::t_replaceinternallinks2(Variant v_s) {
  INSTANCE_METHOD_INJECTION(Parser_LinkHooks, Parser_LinkHooks::replaceInternalLinks2);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  Variant &gv_wgContLang __attribute__((__unused__)) = g->GV(wgContLang);
  Variant &sv_tc __attribute__((__unused__)) = g->sv_parser_linkhooks$$replaceinternallinks2$$tc.lvalAt(this->o_getClassName());
  Variant &inited_sv_tc __attribute__((__unused__)) = g->inited_sv_parser_linkhooks$$replaceinternallinks2$$tc.lvalAt(this->o_getClassName());
  Variant &sv_titleRegex __attribute__((__unused__)) = g->sv_parser_linkhooks$$replaceinternallinks2$$titleRegex.lvalAt(this->o_getClassName());
  Variant &inited_sv_titleRegex __attribute__((__unused__)) = g->inited_sv_parser_linkhooks$$replaceinternallinks2$$titleRegex.lvalAt(this->o_getClassName());
  Variant v_sk;
  p_linkholderarray v_holders;
  Variant v_selflink;
  Numeric v_offset = 0;
  Variant v_offsetStack;
  p_linkmarkerreplacer v_markers;
  Variant v_startBracketOffset;
  Variant v_endBracketOffset;
  bool v_isStart = false;
  Variant v_bracketOffset;
  Variant v_titleText;
  Variant v_paramText;
  String v_marker;

  LINE(116,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinks2").create()), 0x0000000075359BAFLL));
  LINE(118,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinks2-setup").create()), 0x0000000075359BAFLL));
  {
    if (!inited_sv_tc) {
      (sv_tc = false);
      inited_sv_tc = true;
    }
    if (!inited_sv_titleRegex) {
      (sv_titleRegex = null);
      inited_sv_titleRegex = true;
    }
  }
  if (!(toBoolean(sv_tc))) {
    (sv_tc = concat(toString(LINE(122,throw_fatal("unknown class title", ((void*)NULL)))), "#%"));
    (sv_titleRegex = LINE(128,concat3("/^([", toString(sv_tc), "]+)$/sD")));
  }
  (v_sk = LINE(131,m_mOptions.o_invoke_few_args("getSkin", 0x6C79601E06BA735FLL, 0)));
  ((Object)((v_holders = ((Object)(LINE(132,p_linkholderarray(p_linkholderarray(NEWOBJ(c_linkholderarray)())->create(((Object)(this))))))))));
  if (LINE(134,x_is_null(m_mTitle))) {
    LINE(135,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinks2").create()), 0x00000000B599F276LL));
    LINE(136,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinks2-setup").create()), 0x00000000B599F276LL));
    throw_exception(LINE(137,create_object("mwexception", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinks2: $this->mTitle is null\n").create()))));
  }
  ;
  if (toBoolean(LINE(141,gv_wgContLang.o_invoke_few_args("hasVariants", 0x5A1FD2E20BD8DBBBLL, 0)))) {
    (v_selflink = LINE(142,gv_wgContLang.o_invoke_few_args("convertLinkToAllVariants", 0x276E1379FA1C5660LL, 1, m_mTitle.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0))));
  }
  else {
    (v_selflink = Array(ArrayInit(1).set(0, LINE(144,m_mTitle.o_invoke_few_args("getPrefixedText", 0x33044BA725446A43LL, 0))).create()));
  }
  LINE(146,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinks2-setup").create()), 0x00000000B599F276LL));
  (v_offset = 0LL);
  (v_offsetStack = ScalarArrays::sa_[0]);
  ((Object)((v_markers = ((Object)(LINE(150,p_linkmarkerreplacer(p_linkmarkerreplacer(NEWOBJ(c_linkmarkerreplacer)())->create(((Object)(this)), ((Object)(v_holders)), Array(ArrayInit(2).setRef(0, ref(this)).set(1, "replaceInternalLinksCallback").create())))))))));
  LOOP_COUNTER(1);
  {
    while (true) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_startBracketOffset = LINE(152,x_strpos(toString(v_s), "[[", toInt32(v_offset))));
        (v_endBracketOffset = LINE(153,x_strpos(toString(v_s), "]]", toInt32(v_offset))));
        if (same(v_startBracketOffset, false) && same(v_endBracketOffset, false)) break;
        else if (!same(v_startBracketOffset, false) && !same(v_endBracketOffset, false)) (v_isStart = not_more(v_startBracketOffset, v_endBracketOffset));
        else (v_isStart = !same(v_startBracketOffset, false));
        (v_bracketOffset = v_isStart ? ((Variant)(v_startBracketOffset)) : ((Variant)(v_endBracketOffset)));
        if (v_isStart) {
          v_offsetStack.append((v_startBracketOffset));
        }
        else {
          (v_startBracketOffset = LINE(170,x_array_pop(ref(v_offsetStack))));
          v_endBracketOffset += 2LL;
          if (isset(v_startBracketOffset)) {
            (silenceInc(), silenceDec(df_lambda_4(LINE(178,(assignCallTemp(eo_1, toString(x_substr(toString(v_s), toInt32(v_startBracketOffset + 2LL), toInt32(v_endBracketOffset - v_startBracketOffset - 4LL)))),x_explode("|", eo_1, toInt32(2LL)))), v_titleText, v_paramText)));
            if (toBoolean(LINE(180,x_preg_match(toString(sv_titleRegex), toString(v_titleText))))) {
              (v_marker = LINE(182,v_markers->t_addmarker(v_titleText, v_paramText)));
              (v_s = concat_rev(toString(LINE(186,x_substr(toString(v_s), toInt32(v_endBracketOffset)))), concat(toString(LINE(184,x_substr(toString(v_s), toInt32(0LL), toInt32(v_startBracketOffset)))), v_marker)));
              (v_offset = v_startBracketOffset + LINE(189,x_strlen(v_marker)));
              continue;
            }
          }
        }
        (v_offset = v_bracketOffset + 2LL);
      }
    }
  }
  LINE(206,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinks2-expand").create()), 0x0000000075359BAFLL));
  (v_s = LINE(207,v_markers->t_expand(v_s)));
  LINE(208,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinks2-expand").create()), 0x00000000B599F276LL));
  LINE(210,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinks2").create()), 0x00000000B599F276LL));
  return ((Object)(v_holders));
} /* function */
/* SRC: Parser_LinkHooks.php line 214 */
Variant c_parser_linkhooks::t_replaceinternallinkscallback(CVarRef v_parser, CVarRef v_holders, CVarRef v_markers, Variant v_titleText, Variant v_paramText) {
  INSTANCE_METHOD_INJECTION(Parser_LinkHooks, Parser_LinkHooks::replaceInternalLinksCallback);
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  String v_wt;
  Variant v_leadingColon;
  Variant v_title;
  Variant v_ns;
  Variant v_return;
  Variant v_callback;
  Variant v_flags;
  Array v_args;
  Variant v_name;

  LINE(215,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinksCallback").create()), 0x0000000075359BAFLL));
  (v_wt = isset(v_paramText) ? ((LINE(216,concat5("[[", toString(v_titleText), "|", toString(v_paramText), "]]")))) : ((concat3("[[", toString(v_titleText), "]]"))));
  LINE(217,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinksCallback-misc").create()), 0x0000000075359BAFLL));
  if (toBoolean(LINE(221,(assignCallTemp(eo_0, (assignCallTemp(eo_3, toString(invoke_failed("wfurlprotocols", Array(), 0x00000000BD4BD4FELL))),concat3("/^\\b(\?:", eo_3, ")/"))),assignCallTemp(eo_1, toString(v_titleText)),x_preg_match(eo_0, eo_1))))) {
    LINE(222,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinksCallback").create()), 0x00000000B599F276LL));
    return v_wt;
  }
  if (toBoolean(LINE(227,t_aresubpagesallowed()))) {
    (v_titleText = LINE(228,t_maybedosubpagelink(v_titleText, ref(v_paramText))));
  }
  (v_leadingColon = equal(v_titleText.rvalAt(0LL, 0x77CFA1EEF01BCA90LL), ":"));
  if (toBoolean(v_leadingColon)) (v_titleText = LINE(233,x_substr(toString(v_titleText), toInt32(1LL))));
  LINE(235,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinksCallback-misc").create()), 0x00000000B599F276LL));
  LINE(237,invoke_failed("wfprofilein", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinksCallback-title").create()), 0x0000000075359BAFLL));
  (v_title = LINE(238,throw_fatal("unknown class title", (m_mStripState.o_invoke_few_args("unstripNoWiki", 0x2374179DF576FCA9LL, 1, v_titleText), (void*)NULL))));
  if (!(toBoolean(v_title))) {
    LINE(240,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinksCallback-title").create()), 0x00000000B599F276LL));
    LINE(241,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinksCallback").create()), 0x00000000B599F276LL));
    return v_wt;
  }
  (v_ns = LINE(244,v_title.o_invoke_few_args("getNamespace", 0x6DC05480D007D522LL, 0)));
  LINE(245,invoke_failed("wfprofileout", Array(ArrayInit(1).set(0, "Parser_LinkHooks::replaceInternalLinksCallback-title").create()), 0x00000000B599F276LL));
  (v_return = true);
  if (isset(m_mLinkHooks, v_ns)) {
    df_lambda_5(m_mLinkHooks.rvalAt(v_ns), v_callback, v_flags);
    if (toBoolean(bitwise_and(v_flags, k_SLH_PATTERN))) {
      (v_args = Array(ArrayInit(6).set(0, v_parser).set(1, v_holders).set(2, v_markers).set(3, v_titleText).setRef(4, ref(v_paramText)).setRef(5, ref(v_leadingColon)).create()));
    }
    else {
      (v_args = Array(ArrayInit(7).set(0, v_parser).set(1, v_holders).set(2, v_markers).set(3, v_title).set(4, v_titleText).setRef(5, ref(v_paramText)).setRef(6, ref(v_leadingColon)).create()));
    }
    if (!(LINE(258,x_is_callable(v_callback)))) {
      throw_exception(LINE(259,create_object("mwexception", Array(ArrayInit(1).set(0, concat3("Tag hook for ", toString(v_name), " is not callable\n")).create()))));
    }
    (v_return = LINE(261,x_call_user_func_array(v_callback, v_args)));
  }
  if (same(v_return, true)) {
    (v_args = Array(ArrayInit(7).set(0, v_parser).set(1, v_holders).set(2, v_markers).set(3, v_title).set(4, v_titleText).setRef(5, ref(v_paramText)).setRef(6, ref(v_leadingColon)).create()));
    (v_return = LINE(266,x_call_user_func_array(ScalarArrays::sa_[11], v_args)));
  }
  if (same(v_return, false)) {
    return isset(v_paramText) ? ((LINE(271,concat5("[[", toString(v_titleText), "|", toString(v_paramText), "]]")))) : ((concat3("[[", toString(v_titleText), "]]")));
  }
  return v_return;
} /* function */
/* SRC: Parser_LinkHooks.php line 279 */
Variant c_linkmarkerreplacer::os_get(const char *s, int64 hash) {
  return c_ObjectData::os_get(s, hash);
}
Variant &c_linkmarkerreplacer::os_lval(const char *s, int64 hash) {
  return c_ObjectData::os_lval(s, hash);
}
void c_linkmarkerreplacer::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("markers", m_markers.isReferenced() ? ref(m_markers) : m_markers));
  props.push_back(NEW(ArrayElement)("nextId", m_nextId.isReferenced() ? ref(m_nextId) : m_nextId));
  props.push_back(NEW(ArrayElement)("parser", m_parser.isReferenced() ? ref(m_parser) : m_parser));
  props.push_back(NEW(ArrayElement)("holders", m_holders.isReferenced() ? ref(m_holders) : m_holders));
  props.push_back(NEW(ArrayElement)("callback", m_callback.isReferenced() ? ref(m_callback) : m_callback));
  c_ObjectData::o_get(props);
}
bool c_linkmarkerreplacer::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_EXISTS_STRING(0x18A8B9D71D4F2D02LL, parser, 6);
      break;
    case 6:
      HASH_EXISTS_STRING(0x07457EDA5A7122B6LL, nextId, 6);
      HASH_EXISTS_STRING(0x1E7DD8DF6E98F4C6LL, callback, 8);
      break;
    case 9:
      HASH_EXISTS_STRING(0x653408D93F754339LL, markers, 7);
      break;
    case 14:
      HASH_EXISTS_STRING(0x71313201CC7A998ELL, holders, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_exists(s, hash);
}
Variant c_linkmarkerreplacer::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                         parser, 6);
      break;
    case 6:
      HASH_RETURN_STRING(0x07457EDA5A7122B6LL, m_nextId,
                         nextId, 6);
      HASH_RETURN_STRING(0x1E7DD8DF6E98F4C6LL, m_callback,
                         callback, 8);
      break;
    case 9:
      HASH_RETURN_STRING(0x653408D93F754339LL, m_markers,
                         markers, 7);
      break;
    case 14:
      HASH_RETURN_STRING(0x71313201CC7A998ELL, m_holders,
                         holders, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_get(s, hash);
}
Variant c_linkmarkerreplacer::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_SET_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                      parser, 6);
      break;
    case 6:
      HASH_SET_STRING(0x07457EDA5A7122B6LL, m_nextId,
                      nextId, 6);
      HASH_SET_STRING(0x1E7DD8DF6E98F4C6LL, m_callback,
                      callback, 8);
      break;
    case 9:
      HASH_SET_STRING(0x653408D93F754339LL, m_markers,
                      markers, 7);
      break;
    case 14:
      HASH_SET_STRING(0x71313201CC7A998ELL, m_holders,
                      holders, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_set(s, hash, v, forInit);
}
Variant &c_linkmarkerreplacer::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 15) {
    case 2:
      HASH_RETURN_STRING(0x18A8B9D71D4F2D02LL, m_parser,
                         parser, 6);
      break;
    case 6:
      HASH_RETURN_STRING(0x07457EDA5A7122B6LL, m_nextId,
                         nextId, 6);
      HASH_RETURN_STRING(0x1E7DD8DF6E98F4C6LL, m_callback,
                         callback, 8);
      break;
    case 9:
      HASH_RETURN_STRING(0x653408D93F754339LL, m_markers,
                         markers, 7);
      break;
    case 14:
      HASH_RETURN_STRING(0x71313201CC7A998ELL, m_holders,
                         holders, 7);
      break;
    default:
      break;
  }
  return c_ObjectData::o_lval(s, hash);
}
Variant c_linkmarkerreplacer::os_constant(const char *s) {
  return c_ObjectData::os_constant(s);
}
IMPLEMENT_CLASS(linkmarkerreplacer)
ObjectData *c_linkmarkerreplacer::create(Variant v_parser, Variant v_holders, Variant v_callback) {
  init();
  t___construct(v_parser, v_holders, v_callback);
  return this;
}
ObjectData *c_linkmarkerreplacer::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    return (create(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
  } else return this;
}
void c_linkmarkerreplacer::dynConstruct(CArrRef params) {
  (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)));
}
ObjectData *c_linkmarkerreplacer::cloneImpl() {
  c_linkmarkerreplacer *obj = NEW(c_linkmarkerreplacer)();
  cloneSet(obj);
  return obj;
}
void c_linkmarkerreplacer::cloneSet(c_linkmarkerreplacer *clone) {
  clone->m_markers = m_markers.isReferenced() ? ref(m_markers) : m_markers;
  clone->m_nextId = m_nextId.isReferenced() ? ref(m_nextId) : m_nextId;
  clone->m_parser = m_parser.isReferenced() ? ref(m_parser) : m_parser;
  clone->m_holders = m_holders.isReferenced() ? ref(m_holders) : m_holders;
  clone->m_callback = m_callback.isReferenced() ? ref(m_callback) : m_callback;
  ObjectData::cloneSet(clone);
}
Variant c_linkmarkerreplacer::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x2DE38EBA0ED55F48LL, callback) {
        return (t_callback(params.rvalAt(0)));
      }
      break;
    case 2:
      HASH_GUARD(0x21C2BB2267C07FDALL, findmarker) {
        return (t_findmarker(params.rvalAt(0)));
      }
      break;
    case 5:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        return (t_expand(params.rvalAt(0)));
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke(s, params, hash, fatal);
}
Variant c_linkmarkerreplacer::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 7) {
    case 0:
      HASH_GUARD(0x2DE38EBA0ED55F48LL, callback) {
        return (t_callback(a0));
      }
      break;
    case 2:
      HASH_GUARD(0x21C2BB2267C07FDALL, findmarker) {
        return (t_findmarker(a0));
      }
      break;
    case 5:
      HASH_GUARD(0x39B7BB05F05A37CDLL, expand) {
        return (t_expand(a0));
      }
      break;
    case 7:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        return (t___construct(a0, a1, a2), null);
      }
      break;
    default:
      break;
  }
  return c_ObjectData::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_linkmarkerreplacer::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_ObjectData::os_invoke(c, s, params, hash, fatal);
}
Variant cw_linkmarkerreplacer$os_get(const char *s) {
  return c_linkmarkerreplacer::os_get(s, -1);
}
Variant &cw_linkmarkerreplacer$os_lval(const char *s) {
  return c_linkmarkerreplacer::os_lval(s, -1);
}
Variant cw_linkmarkerreplacer$os_constant(const char *s) {
  return c_linkmarkerreplacer::os_constant(s);
}
Variant cw_linkmarkerreplacer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_linkmarkerreplacer::os_invoke(c, s, params, -1, fatal);
}
void c_linkmarkerreplacer::init() {
  m_markers = null;
  m_nextId = null;
  m_parser = null;
  m_holders = null;
  m_callback = null;
}
/* SRC: Parser_LinkHooks.php line 283 */
void c_linkmarkerreplacer::t___construct(Variant v_parser, Variant v_holders, Variant v_callback) {
  INSTANCE_METHOD_INJECTION(LinkMarkerReplacer, LinkMarkerReplacer::__construct);
  bool oldInCtor = gasInCtor(true);
  (m_nextId = 0LL);
  (m_markers = ScalarArrays::sa_[0]);
  (m_parser = v_parser);
  (m_holders = v_holders);
  (m_callback = v_callback);
  gasInCtor(oldInCtor);
} /* function */
/* SRC: Parser_LinkHooks.php line 291 */
String c_linkmarkerreplacer::t_addmarker(CVarRef v_titleText, CVarRef v_paramText) {
  INSTANCE_METHOD_INJECTION(LinkMarkerReplacer, LinkMarkerReplacer::addMarker);
  Primitive v_id = 0;

  (v_id = m_nextId++);
  m_markers.set(v_id, (Array(ArrayInit(2).set(0, v_titleText).set(1, v_paramText).create())));
  return LINE(294,concat3("<!-- LINKMARKER ", toString(v_id), " -->"));
} /* function */
/* SRC: Parser_LinkHooks.php line 297 */
bool c_linkmarkerreplacer::t_findmarker(CVarRef v_string) {
  INSTANCE_METHOD_INJECTION(LinkMarkerReplacer, LinkMarkerReplacer::findMarker);
  return toBoolean(LINE(298,x_preg_match("/<!-- LINKMARKER [0-9]+ -->/", toString(v_string))));
} /* function */
/* SRC: Parser_LinkHooks.php line 301 */
Variant c_linkmarkerreplacer::t_expand(CVarRef v_string) {
  INSTANCE_METHOD_INJECTION(LinkMarkerReplacer, LinkMarkerReplacer::expand);
  return LINE(302,throw_fatal("unknown class stringutils", ((void*)NULL)));
} /* function */
/* SRC: Parser_LinkHooks.php line 305 */
Variant c_linkmarkerreplacer::t_callback(CVarRef v_m) {
  INSTANCE_METHOD_INJECTION(LinkMarkerReplacer, LinkMarkerReplacer::callback);
  int64 v_id = 0;
  Variant v_args;

  (v_id = LINE(306,x_intval(v_m.rvalAt(1LL, 0x5BCA7C69B794F8CELL))));
  if (!(LINE(307,x_array_key_exists(v_id, m_markers)))) return v_m.rvalAt(0LL, 0x77CFA1EEF01BCA90LL);
  (v_args = m_markers.rvalAt(v_id));
  LINE(309,x_array_unshift(2, ref(v_args), ((Object)(this))));
  LINE(310,x_array_unshift(2, ref(v_args), m_holders));
  LINE(311,x_array_unshift(2, ref(v_args), m_parser));
  return LINE(312,x_call_user_func_array(m_callback, toArray(v_args)));
} /* function */
Object co_parser_linkhooks(CArrRef params, bool init /* = true */) {
  return Object(p_parser_linkhooks(NEW(c_parser_linkhooks)())->dynCreate(params, init));
}
Object co_linkmarkerreplacer(CArrRef params, bool init /* = true */) {
  return Object(p_linkmarkerreplacer(NEW(c_linkmarkerreplacer)())->dynCreate(params, init));
}
Variant pm_php$Parser_LinkHooks_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::Parser_LinkHooks.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$Parser_LinkHooks_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
