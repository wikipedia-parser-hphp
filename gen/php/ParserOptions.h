
#ifndef __GENERATED_php_ParserOptions_h__
#define __GENERATED_php_ParserOptions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/ParserOptions.fw.h>

// Declarations
#include <cls/parseroptions.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$ParserOptions_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_parseroptions(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_ParserOptions_h__
