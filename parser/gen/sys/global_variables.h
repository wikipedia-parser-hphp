
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(wgMessageCache)
    GVS(wgStyleDirectory)
    GVS(wgRequest)
    GVS(wgUser)
    GVS(wgLang)
    GVS(wgOut)
    GVS(wgParserConf)
    GVS(wgParser)
    GVS(wgDefaultUserOptions)
    GVS(wgNamespacesToBeSearchedDefault)
    GVS(wgIP)
    GVS(wgUsePrivateIPs)
    GVS(wgSquidServers)
    GVS(wgSquidServersNoPurge)
    GVS(wgBlockOpenProxies)
    GVS(wgProxyPorts)
    GVS(wgProxyScriptPath)
    GVS(wgMemc)
    GVS(wgProxyMemcExpiry)
    GVS(wgProxyKey)
    GVS(wgProxyList)
    GVS(wgCanonicalNamespaceNames)
    GVS(wgExtraNamespaces)
    GVS(wgAllowImageMoving)
    GVS(wgContentNamespaces)
    GVS(wgNamespacesWithSubpages)
    GVS(wgContLang)
    GVS(wgThumbLimits)
    GVS(wgThumbUpright)
    GVS(wgStylePath)
    GVS(wgEnableUploads)
    GVS(wgDisableAnonTalk)
    GVS(wgSysopUserBans)
    GVS(wgJsMimeType)
    GVS(wgTitle)
    GVS(wgArticle)
    GVS(wgScript)
    GVS(wgContLanguageCode)
    GVS(wgMimeType)
    GVS(wgOutputEncoding)
    GVS(wgXhtmlDefaultNamespace)
    GVS(wgXhtmlNamespaces)
    GVS(wgDisableCounters)
    GVS(wgLogo)
    GVS(wgHideInterlanguageLinks)
    GVS(wgMaxCredits)
    GVS(wgShowCreditsIfMax)
    GVS(wgPageShowWatchingUsers)
    GVS(wgUseTrackbacks)
    GVS(wgUseSiteJs)
    GVS(wgArticlePath)
    GVS(wgScriptPath)
    GVS(wgServer)
    GVS(wgDisableLangConversion)
    GVS(wgUploadNavigationUrl)
    GVS(wgHandheldStyle)
    GVS(wgStyleVersion)
    GVS(wgUseTwoButtonsSearchForm)
    GVS(wgValidSkinNames)
    GVS(wgSkipSkins)
    GVS(wgDefaultSkin)
    GVS(wgFavicon)
    GVS(wgAppleTouchIcon)
    GVS(wgEnableDublinCoreRdf)
    GVS(wgEnableCreativeCommonsRdf)
    GVS(wgRightsPage)
    GVS(wgRightsUrl)
    GVS(wgDebugComments)
    GVS(wgBreakFrames)
    GVS(wgVariantArticlePath)
    GVS(wgActionPaths)
    GVS(wgUseAjax)
    GVS(wgAjaxWatch)
    GVS(wgVersion)
    GVS(wgEnableAPI)
    GVS(wgEnableWriteAPI)
    GVS(wgRestrictionTypes)
    GVS(wgLivePreview)
    GVS(wgMWSuggestTemplate)
    GVS(wgDBname)
    GVS(wgEnableMWSuggest)
    GVS(wgAllowUserCss)
    GVS(wgUseSiteCss)
    GVS(wgSquidMaxage)
    GVS(wgUseCategoryBrowser)
    GVS(wgShowDebug)
    GVS(action)
    GVS(wgFeedClasses)
    GVS(wgExtraSubtitle)
    GVS(wgShowIPinHeader)
    GVS(wgRightsText)
    GVS(wgRightsIcon)
    GVS(wgCopyrightIcon)
    GVS(wgRedirectScript)
    GVS(parserMemc)
    GVS(wgEnableSidebarCache)
    GVS(wgSidebarCacheExpiry)
    GVS(wgAntiLockFlags)
    GVS(wgAutoblockExpiry)
    GVS(wgMaxNameChars)
    GVS(wgReservedUsernames)
    GVS(wgMinimalPasswordLength)
    GVS(wgAuth)
    GVS(wgCookiePrefix)
    GVS(wgUseRCPatrol)
    GVS(wgEnableSorbs)
    GVS(wgProxyWhitelist)
    GVS(wgSorbsUrl)
    GVS(wgRateLimitsExcludedGroups)
    GVS(wgRateLimitsExcludedIPs)
    GVS(wgRateLimits)
    GVS(wgRateLimitLog)
    GVS(wgBlockAllowsUTEdit)
    GVS(wgClockSkewFudge)
    GVS(wgSecretKey)
    GVS(wgPasswordReminderResendTime)
    GVS(wgUseNPPatrol)
    GVS(wgAllowUserSkin)
    GVS(wgUseEnotif)
    GVS(wgShowUpdatedMarker)
    GVS(wgUseDynamicDates)
    GVS(wgRenderHashAppend)
    GVS(wgActiveUserEditCount)
    GVS(wgActiveUserDays)
    GVS(wgNewPasswordExpiry)
    GVS(wgPasswordSender)
    GVS(wgEnableEmail)
    GVS(wgEnableUserEmail)
    GVS(wgEmailAuthentication)
    GVS(wgGroupPermissions)
    GVS(wgAvailableRights)
    GVS(wgImplicitGroups)
    GVS(wgPasswordSalt)
    GVS(wgNewUserLog)
    GVS(wgCommandLineMode)
    GVS(wgUseTeX)
    GVS(wgInterwikiMagic)
    GVS(wgAllowExternalImages)
    GVS(wgAllowExternalImagesFrom)
    GVS(wgEnableImageWhitelist)
    GVS(wgAllowSpecialInclusion)
    GVS(wgMaxArticleSize)
    GVS(wgMaxPPNodeCount)
    GVS(wgMaxTemplateDepth)
    GVS(wgMaxPPExpandDepth)
    GVS(wgCleanSignatures)
    GVS(wgExternalLinkTarget)
    GVS(wgProfiler)
    GVS(wgDebugFunctionEntry)
    GVS(wgDebugProfiling)
    GVS(wgProfileToDatabase)
    GVS(wgLanguageNames)
    GVS(wgInputEncoding)
    GVS(IP)
    GVS(wgExtraLanguageNames)
    GVS(wgLocalTZoffset)
    GVS(wgDBtype)
    GVS(wgEditEncoding)
    GVS(wgExtensionAliasesFiles)
    GVS(wgTranslateNumerals)
    GVS(wgGrammarForms)
    GVS(wgEnableSerializedMessages)
    GVS(wgCheckSerialized)
    GVS(wgMetaNamespace)
    GVS(wgMetaNamespaceTalk)
    GVS(wgNamespaceAliases)
    GVS(wgAmericanDates)
    GVS(wgFullyInitialised)
    GVS(wgExceptionHooks)
    GVS(wgShowExceptionDetails)
    GVS(wgSitename)
    GVS(utfCombiningClass)
    GVS(utfCanonicalComp)
    GVS(utfCanonicalDecomp)
    GVS(utfCompatibilityDecomp)
    GVS(utfCheckNFC)
    GVS(attrib)
    GVS(space)
    GVS(wgHtmlEntities)
    GVS(wgHtmlEntityAliases)
    GVS(wgUseTidy)
    GVS(wgEnforceHtmlIds)
    GVS(wgLegacyEncoding)
    GVS(wgCompressRevisions)
    GVS(wgDefaultExternalStore)
    GVS(wgRevisionCacheExpiry)
    GVS(wgCacheEpoch)
    GVS(wgLocalInterwiki)
    GVS(wgPutIPinRC)
    GVS(wgRC2UDPAddress)
    GVS(wgRC2UDPOmitBots)
    GVS(wgRC2UDPPrefix)
    GVS(wgRC2UDPPort)
    GVS(wgLogRestrictions)
    GVS(wgRC2UDPInterwikiPrefix)
    GVS(wgUseDumbLinkUpdate)
    GVS(wgUpdateRowsPerJob)
    GVS(wgPagePropLinkInvalidations)
    GVS(wgUseCommaCount)
    GVS(wgEnableParserCache)
    GVS(wgNamespaceRobotPolicies)
    GVS(wgArticleRobotPolicies)
    GVS(wgDefaultRobotPolicy)
    GVS(wgUseETag)
    GVS(wgRedirectSources)
    GVS(wgUseSquid)
    GVS(wgDBtransactions)
    GVS(wgUseAutomaticEditSummaries)
    GVS(wgDeleteRevisionsLimit)
    GVS(wgDeferredUpdateList)
    GVS(wgRCMaxAge)
    GVS(wgHitcounterUpdateFreq)
    GVS(wgAllowPageInfo)
    GVS(wgUseFileCache)
    GVS(wgLegalTitleChars)
    GVS(wgMaxRedirects)
    GVS(wgInternalServer)
    GVS(wgRestrictionLevels)
    GVS(wgNamespaceProtection)
    GVS(wgEmailConfirmToEdit)
    GVS(wgWhitelistRead)
    GVS(wgCapitalLinks)
    GVS(wgMaximumMovedPages)
    GVS(wgInvalidRedirectTargets)
    GVS(wgHooks)
    GVS(wgDebugLogFile)
    GVS(wgProfileOnly)
    GVS(wgDebugRawPage)
    GVS(wgDebugLogPrefix)
    GVS(wgDebugLogGroups)
    GVS(wgShowHostnames)
    GVS(wgDBerrorLog)
    GVS(wgRequestTime)
    GVS(wgProfileLimit)
    GVS(wgReadOnlyFile)
    GVS(wgReadOnly)
    GVS(wgForceUIMsgAsContentMsg)
    GVS(wgUseGzip)
    GVS(wgDiff3)
    GVS(wgDiff)
    GVS(wgDisableOutputCompression)
    GVS(wgSiteNotice)
    GVS(wgDirectoryMode)
    GVS(wgStatsMethod)
    GVS(wgUDPProfilerHost)
    GVS(wgUDPProfilerPort)
    GVS(wgUrlProtocols)
    GVS(wgMaxShellMemory)
    GVS(wgMaxShellFileSize)
    GVS(wgMaxShellTime)
    GVS(wgShellLocale)
    GVS(wgPostCommitUpdateList)
    GVS(wgHttpOnlyBlacklist)
    GVS(wgSessionsInMemcached)
    GVS(wgCookiePath)
    GVS(wgCookieDomain)
    GVS(wgCookieSecure)
    GVS(wgCookieHttpOnly)
    GVS(wgDBprefix)
    GVS(wgMiserMode)
    GVS(wgScriptExtension)
    GVS(wgExtensionMessagesFiles)
    GVS(wgAllowDisplayTitle)
    GVS(wgAllowSlowParserFunctions)
    GVS(wgRestrictDisplayTitle)
    GVS(wgLinkHolderBatchSize)
    GVS(wgPreprocessorCacheThreshold)
    GVS(wgAlwaysUseTidy)
    GVS(wgExpensiveParserFunctionLimit)
    GVS(wgRawHtml)
    GVS(wgNoFollowLinks)
    GVS(wgNoFollowNsExceptions)
    GVS(wgNoFollowDomainExceptions)
    GVS(wgServerName)
    GVS(wgLocaltimezone)
    GVS(wgNonincludableNamespaces)
    GVS(wgEnableScaryTranscluding)
    GVS(wgTranscludeCacheExpiry)
    GVS(wgMaxTocLevel)
    GVS(wgMaxSigChars)
    GVS(wgCategoryPrefixedDefaultSortkey)
    GVS(__Test)
    GVS(options)
    GVS(optionsWithArgs)
    GVS(self)
    GVS(args)
    GVS(arg)
    GVS(option)
    GVS(param)
    GVS(bits)
    GVS(p)
    GVS(sep)
    GVS(wgUseNormalUser)
    GVS(wgWikiFarm)
    GVS(cluster)
    GVS(wgConf)
    GVS(wgLocalDatabases)
    GVS(wgNoDBParam)
    GVS(db)
    GVS(site)
    GVS(lang)
    GVS(DP)
    GVS(fname)
    GVS(hatesSafari)
    GVS(secure)
    GVS(pathBits)
    GVS(m)
    GVS(server)
    GVS(docRoot)
    GVS(matches)
    GVS(dbSuffix)
    GVS(filename)
    GVS(globals)
    GVS(cacheRecord)
    GVS(wikiTags)
    GVS(tag)
    GVS(dblist)
    GVS(oldUmask)
    GVS(file)
    GVS(wgStyleSheetPath)
    GVS(wgStockPath)
    GVS(wgFileStore)
    GVS(wgSharedThumbnailScriptPath)
    GVS(wgUploadPath)
    GVS(wgUploadDirectory)
    GVS(tmarray)
    GVS(hour)
    GVS(day)
    GVS(wgEmergencyContact)
    GVS(wgHTCPMulticastAddress)
    GVS(wgHTCPMulticastTTL)
    GVS(wgCheckDBSchema)
    GVS(oldtz)
    GVS(wgTexvc)
    GVS(wgTmpDirectory)
    GVS(wgDisableTextSearch)
    GVS(wgDisableSearchUpdate)
    GVS(wgUseImageResize)
    GVS(wgUseImageMagick)
    GVS(wgImageMagickConvertCommand)
    GVS(wgFileBlacklist)
    GVS(wgFileExtensions)
    GVS(wmgPrivateWikiUploads)
    GVS(wgMimeTypeBlacklist)
    GVS(wgSVGConverters)
    GVS(wgAllowUserJs)
    GVS(wgUseESI)
    GVS(wgLogQueries)
    GVS(wgSqlLogFile)
    GVS(wmgUseDualLicense)
    GVS(wgProfiling)
    GVS(wgProfileSampleRate)
    GVS(wgSiteMatrixFile)
    GVS(wgMaxIfExistCount)
    GVS(wmgUseFlaggedRevs)
    GVS(wgCategoryTreeDynamicTag)
    GVS(wmgUseProofreadPage)
    GVS(wmgUseLST)
    GVS(wmgUseSpamBlacklist)
    GVS(wgTitleBlacklistSources)
    GVS(wmgUseQuiz)
    GVS(wmgUseGadgets)
    GVS(wgFFmpegLocation)
    GVS(wgCortadoJarFile)
    GVS(wgContactUser)
    GVS(wgExtDistTarDir)
    GVS(wgExtDistTarUrl)
    GVS(wgExtDistWorkingCopy)
    GVS(wgExtDistRemoteClient)
    GVS(wgExtDistBranches)
    GVS(wgGlobalBlockingDatabase)
    GVS(wmgApplyGlobalBlocks)
    GVS(wgApplyGlobalBlocks)
    GVS(wgTrustedXffFile)
    GVS(wmgContactPageConf)
    GVS(ubUploadBlacklist)
    GVS(wgTimelineSettings)
    GVS(wgAllowRealName)
    GVS(wgSysopRangeBans)
    GVS(wgUploadSizeWarning)
    GVS(wgForeignFileRepos)
    GVS(title)
    GVS(wgUseHashTable)
    GVS(wgLanguageCode)
    GVS(wgLanguageCodeReal)
    GVS(wgUseLuceneSearch)
    GVS(wgMaxSquidPurgeTitles)
    GVS(wgThumbnailEpoch)
    GVS(oaiAgentRegex)
    GVS(oaiAuth)
    GVS(oaiAudit)
    GVS(oaiAuditDatabase)
    GVS(wgExtensionFunctions)
    GVS(wmgUseDPL)
    GVS(wmgUseSpecialNuke)
    GVS(wgAntiBotPayloads)
    GVS(wgTorLoadNodes)
    GVS(wgTorIPs)
    GVS(wgTorAutoConfirmAge)
    GVS(wgTorAutoConfirmCount)
    GVS(wgTorDisableAdminBlocks)
    GVS(wgTorTagChanges)
    GVS(wgDisabledActions)
    GVS(wgDisableHardRedirects)
    GVS(groupOverrides2)
    GVS(group)
    GVS(permissions)
    GVS(groupOverrides)
    GVS(wmgAutopromoteExtraGroups)
    GVS(wgAutopromote)
    GVS(wmgExtraImplicitGroups)
    GVS(wgLegacySchemaConversion)
    GVS(wgHTTPTimeout)
    GVS(wgDebugDumpSql)
    GVS(wgDBservers)
    GVS(key)
    GVS(val)
    GVS(wgDBserver)
    GVS(wgExternalServers)
    GVS(x)
    GVS(y)
    GVS(wgBrowserBlackList)
    GVS(wgScanSetSettings)
    GVS(wmgCaptchaPassword)
    GVS(wmgEnableCaptcha)
    GVS(wmgCaptchaSecret)
    GVS(wgCaptchaSecret)
    GVS(wgCaptchaDirectory)
    GVS(wgCaptchaDirectoryLevels)
    GVS(wgCaptchaStorageClass)
    GVS(wgCaptchaClass)
    GVS(wgCaptchaWhitelist)
    GVS(wgCaptchaRegexes)
    GVS(wgCaptchaTriggers)
    GVS(wmgEmergencyCaptcha)
    GVS(wgExternalDiffEngine)
    GVS(wgEnotifWatchlist)
    GVS(wgEnotifUserTalk)
    GVS(wgEnotifUseJobQ)
    GVS(sectionLoads)
    GVS(dbHostsByName)
    GVS(wgAlternateMaster)
    GVS(sectionsByDB)
    GVS(section)
    GVS(wmgUseCentralAuth)
    GVS(wgCentralAuthDryRun)
    GVS(wgCentralAuthCookies)
    GVS(wgCentralAuthUDPAddress)
    GVS(wgCentralAuthNew2UDPPrefix)
    GVS(wmgSecondLevelDomain)
    GVS(wgCentralAuthAutoLoginWikis)
    GVS(wgCentralAuthCookieDomain)
    GVS(wmgCentralAuthLoginIcon)
    GVS(wgCentralAuthLoginIcon)
    GVS(wgCentralAuthAutoNew)
    GVS(wgCentralAuthCreateOnView)
    GVS(wgCentralAuthCookiePrefix)
    GVS(wmgUseDismissableSiteNotice)
    GVS(wgMajorSiteNoticeID)
    GVS(wgImageMagickTempDir)
    GVS(wgMediaHandlers)
    GVS(wmgUseCentralNotice)
    GVS(wgNoticeProjects)
    GVS(wgNoticeCentralPath)
    GVS(wgNoticeLocalPath)
    GVS(wgNoticeCentralDirectory)
    GVS(wgNoticeLocalDirectory)
    GVS(wmgNoticeProject)
    GVS(wgNoticeProject)
    GVS(wmgCentralNoticeLoader)
    GVS(wgCentralNoticeLoader)
    GVS(wgNoticeTimeout)
    GVS(wgNoticeServerTimeout)
    GVS(wgNoticeCounterSource)
    GVS(wgNoticeInfrastructure)
    GVS(wgNoticeScroll)
    GVS(wmgUseTitleKey)
    GVS(wmgUseCollection)
    GVS(wgCollectionMWServeURL)
    GVS(wgCommunityCollectionNamespace)
    GVS(wgCollectionArticleNamespaces)
    GVS(wgCollectionFormats)
    GVS(wgLicenseURL)
    GVS(wmgCollectionPortletForLoggedInUsersOnly)
    GVS(wgCollectionPortletForLoggedInUsersOnly)
    GVS(wgAccountCreationThrottle)
    GVS(wmgUseNewUserMessage)
    GVS(wmgNewUserSuppressRC)
    GVS(wgNewUserSuppressRC)
    GVS(wmgNewUserMinorEdit)
    GVS(wgNewUserMinorEdit)
    GVS(wmgNewUserMessageOnAutoCreate)
    GVS(wgNewUserMessageOnAutoCreate)
    GVS(wmgUseCodeReview)
    GVS(wgSubversionProxy)
    GVS(wgCodeReviewENotif)
    GVS(wmgUseDrafts)
    GVS(wmgUseConfigure)
    GVS(wgConfigureHandler)
    GVS(wgConfigureDatabase)
    GVS(wgConfigureFileSystemCache)
    GVS(wgConfigureEditableSettings)
    GVS(wgConfigureWikis)
    GVS(wmgUseAbuseFilter)
    GVS(wgAbuseFilterAvailableActions)
    GVS(bin)
    GVS(wgUseRootUser)
    GVS(wgDBadminuser)
    GVS(wgDBuser)
    GVS(wgDBadminpassword)
    GVS(wgDBpassword)
    GVS(adminSettings)
    GVS(i)
    GVS(wgLBFactoryConf)
    GVS(wgShowSQLErrors)
    GVS(wgCaches)
    GVS(wgMemCachedServers)
    GVS(wgMemCachedDebug)
    GVS(wgMemCachedPersistent)
    GVS(wgMainCacheType)
    GVS(wgMessageCacheType)
    GVS(wgParserCacheType)
    GVS(wgCommandLineDarkBg)
    GVS(tester)
    GVS(wgParserTestFiles)
  END_GVS(525)

  // Dynamic Constants
  Variant k_MW_DB;
  Variant k_MW_PREFIX;
  Variant k_MW_ATTRIBS_REGEX;
  Variant k_MEDIAWIKI;
  Variant k_TESTWIKI;

  // Function/Method Static Variables
  Variant sv_title$$secureandsplit$$rxTc;
  bool sv_wfout$$lineStarted;
  Variant sv_wfshellexec$$disabled;
  Variant sv_wfincrstats$$socket;
  Variant sv_sanitizer$$removehtmltags$$htmlsingleallowed;
  Variant sv_language$$getcasemaps$$wikiUpperChars;
  bool sv_wfabruptexit$$called;
  Variant sv_parser$$replaceinternallinks2$$tc;
  Variant sv_skin$$getskinnames$$skinsInitialised;
  Variant sv_wfsuppresswarnings$$originalLevel;
  Variant sv_sanitizer$$removehtmltags$$staticInitialised;
  Variant sv_sanitizer$$removehtmltags$$htmlnest;
  Variant sv_language$$romannumeral$$table;
  Variant sv_stubobject$$_unstub$$recursionLevel;
  Variant sv_language$$hebrewnumeral$$table;
  Variant sv_sanitizer$$removehtmltags$$htmlsingleonly;
  Variant sv_utfnormal$$quickisnfcverify$$checkit;
  Variant sv_wfloadextensionmessages$$loaded;
  bool sv_wfinitshelllocale$$done;
  Variant sv_ip$$ispublic$$privateRanges;
  Variant sv_sanitizer$$removehtmltags$$htmlpairs;
  Variant sv_language$$getcasemaps$$wikiLowerChars;
  Variant sv_block$$formatexpiry$$msg;
  Variant sv_sanitizer$$removehtmltags$$htmllist;
  Variant sv_coreparserfunctions$$pagesincategory$$cache;
  Variant sv_ppframe_dom$$expand$$expansionDepth;
  Variant sv_parser$$replaceinternallinks2$$e1;
  Variant sv_language$$getfallbackfor$$cache;
  Variant sv_coreparserfunctions$$israw$$mwRaw;
  Variant sv_coreparserfunctions$$pagesize$$cache;
  Variant sv_linkcache$$singleton$$instance;
  Variant sv_sanitizer$$removehtmltags$$tabletags;
  Variant sv_user$$getmaxid$$res;
  Variant sv_parser$$getimageparams$$internalParamNames;
  Variant sv_sanitizer$$attributewhitelist$$list;
  Variant sv_linker$$splittrail$$regex;
  Variant sv_parser$$getimageparams$$internalParamMap;
  Variant sv_parser$$extracttagsandparams$$n;
  Variant sv_parser$$replaceinternallinks2$$e1_img;
  Variant sv_language$$newfromcode$$recursionLevel;
  Array sv_wfdebug$$cache;
  Variant sv_user$$isusablename$$reservedUsernames;
  Variant sv_utfnormal$$quickisnfcverify$$tailBytes;
  Variant sv_wfhostname$$host;
  Variant sv_backlinkcache$$getprefix$$prefixes;
  Variant sv_title$$newfromtext$$cachedcount;
  Variant sv_language$$loadlocalisation$$recursionGuard;
  Variant sv_article$$tryfilecache$$called;
  Variant sv_sanitizer$$removehtmltags$$htmlelements;
  Variant sv_sanitizer$$removehtmltags$$listtags;
  Variant sv_sanitizer$$removehtmltags$$htmlsingle;
  Variant sv_utfnormal$$quickisnfcverify$$utfCheckOrCombining;
  int64 sv_wfsuppresswarnings$$suppressCount;
  Variant sv_mwnamespace$$getcanonicalindex$$xNamespaces;
  Variant sv_sanitizer$$escapeid$$replace;
  Variant sv_parser$$transformmsg$$executing;
  int64 sv_wfdebug$$recursion;

  // Function/Method Static Variable Initialization Booleans
  Variant inited_sv_title$$secureandsplit$$rxTc;
  bool inited_sv_wfout$$lineStarted;
  bool inited_sv_wfshellexec$$disabled;
  bool inited_sv_wfincrstats$$socket;
  Variant inited_sv_sanitizer$$removehtmltags$$htmlsingleallowed;
  Variant inited_sv_language$$getcasemaps$$wikiUpperChars;
  bool inited_sv_wfabruptexit$$called;
  Variant inited_sv_parser$$replaceinternallinks2$$tc;
  Variant inited_sv_skin$$getskinnames$$skinsInitialised;
  bool inited_sv_wfsuppresswarnings$$originalLevel;
  Variant inited_sv_sanitizer$$removehtmltags$$staticInitialised;
  Variant inited_sv_sanitizer$$removehtmltags$$htmlnest;
  Variant inited_sv_language$$romannumeral$$table;
  Variant inited_sv_stubobject$$_unstub$$recursionLevel;
  Variant inited_sv_language$$hebrewnumeral$$table;
  Variant inited_sv_sanitizer$$removehtmltags$$htmlsingleonly;
  Variant inited_sv_utfnormal$$quickisnfcverify$$checkit;
  bool inited_sv_wfloadextensionmessages$$loaded;
  bool inited_sv_wfinitshelllocale$$done;
  Variant inited_sv_ip$$ispublic$$privateRanges;
  Variant inited_sv_sanitizer$$removehtmltags$$htmlpairs;
  Variant inited_sv_language$$getcasemaps$$wikiLowerChars;
  Variant inited_sv_block$$formatexpiry$$msg;
  Variant inited_sv_sanitizer$$removehtmltags$$htmllist;
  Variant inited_sv_coreparserfunctions$$pagesincategory$$cache;
  Variant inited_sv_ppframe_dom$$expand$$expansionDepth;
  Variant inited_sv_parser$$replaceinternallinks2$$e1;
  Variant inited_sv_language$$getfallbackfor$$cache;
  Variant inited_sv_coreparserfunctions$$israw$$mwRaw;
  Variant inited_sv_coreparserfunctions$$pagesize$$cache;
  Variant inited_sv_linkcache$$singleton$$instance;
  Variant inited_sv_sanitizer$$removehtmltags$$tabletags;
  Variant inited_sv_user$$getmaxid$$res;
  Variant inited_sv_parser$$getimageparams$$internalParamNames;
  Variant inited_sv_sanitizer$$attributewhitelist$$list;
  Variant inited_sv_linker$$splittrail$$regex;
  Variant inited_sv_parser$$getimageparams$$internalParamMap;
  Variant inited_sv_parser$$extracttagsandparams$$n;
  Variant inited_sv_parser$$replaceinternallinks2$$e1_img;
  Variant inited_sv_language$$newfromcode$$recursionLevel;
  bool inited_sv_wfdebug$$cache;
  Variant inited_sv_user$$isusablename$$reservedUsernames;
  Variant inited_sv_utfnormal$$quickisnfcverify$$tailBytes;
  bool inited_sv_wfhostname$$host;
  Variant inited_sv_backlinkcache$$getprefix$$prefixes;
  Variant inited_sv_title$$newfromtext$$cachedcount;
  Variant inited_sv_language$$loadlocalisation$$recursionGuard;
  Variant inited_sv_article$$tryfilecache$$called;
  Variant inited_sv_sanitizer$$removehtmltags$$htmlelements;
  Variant inited_sv_sanitizer$$removehtmltags$$listtags;
  Variant inited_sv_sanitizer$$removehtmltags$$htmlsingle;
  Variant inited_sv_utfnormal$$quickisnfcverify$$utfCheckOrCombining;
  bool inited_sv_wfsuppresswarnings$$suppressCount;
  Variant inited_sv_mwnamespace$$getcanonicalindex$$xNamespaces;
  Variant inited_sv_sanitizer$$escapeid$$replace;
  Variant inited_sv_parser$$transformmsg$$executing;
  bool inited_sv_wfdebug$$recursion;

  // Class Static Variables
  Variant s_magicword$$mDoubleUnderscoreArray;
  Variant s_language$$GREG_DAYS;
  Variant s_language$$mMergeableMapKeys;
  Variant s_magicword$$mDoubleUnderscoreIDs;
  Variant s_language$$mMergeableListKeys;
  Variant s_magicword$$mCacheTTLs;
  Variant s_magicword$$mVariableIDsInitialised;
  Variant s_user$$mCoreRights;
  Variant s_language$$mMonthAbbrevMsgs;
  Variant s_language$$mLocalisationKeys;
  Variant s_magicword$$mVariableIDs;
  Variant s_language$$mHebrewCalendarMonthGenMsgs;
  Variant s_title$$titleCache;
  Variant s_user$$mCacheVars;
  Variant s_language$$mLocalisationCache;
  Variant s_language$$mHebrewCalendarMonthMsgs;
  Variant s_language$$mMonthGenMsgs;
  Variant s_ppdstack$$false;
  Variant s_user$$mAllRights;
  Variant s_language$$mWeekdayAbbrevMsgs;
  Variant s_user$$mToggles;
  Variant s_language$$mHijriCalendarMonthMsgs;
  Variant s_language$$mMonthMsgs;
  Variant s_language$$mMergeableAliasListKeys;
  Variant s_title$$interwikiCache;
  Variant s_language$$mIranianCalendarMonthMsgs;
  Variant s_magicword$$mObjects;
  Variant s_language$$IRANIAN_DAYS;
  Variant s_language$$mLangObjCache;
  Variant s_language$$mWeekdayMsgs;

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$Parser2_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 537;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[335];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
