
#ifndef __GENERATED_cls_preprocessor_hash_h__
#define __GENERATED_cls_preprocessor_hash_h__

#include <cls/preprocessor.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_Hash.php line 9 */
class c_preprocessor_hash : virtual public c_preprocessor {
  BEGIN_CLASS_MAP(preprocessor_hash)
    PARENT_CLASS(preprocessor)
  END_CLASS_MAP(preprocessor_hash)
  DECLARE_CLASS(preprocessor_hash, Preprocessor_Hash, ObjectData)
  void init();
  public: Variant m_parser;
  public: void t___construct(Variant v_parser);
  public: ObjectData *create(Variant v_parser);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: p_ppframe_hash t_newframe();
  public: p_ppcustomframe_hash t_newcustomframe(CVarRef v_args);
  public: Variant t_preprocesstoobj(Variant v_text, Variant v_flags = 0LL);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_preprocessor_hash_h__
