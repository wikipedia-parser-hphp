
#ifndef __GENERATED_cls_ppframe_h__
#define __GENERATED_cls_ppframe_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 33541 */
class c_ppframe : virtual public ObjectData {
  BEGIN_CLASS_MAP(ppframe)
  END_CLASS_MAP(ppframe)
  DECLARE_CLASS(ppframe, PPFrame, ObjectData)
  void init();
  public: void t_newchild(CVarRef v_args = false, CVarRef v_title = false);
  public: void t_expand(CVarRef v_root, CVarRef v_flags = 0LL);
  public: void t_implodewithflags(CVarRef v_sep, CVarRef v_flags);
  public: void t_implode(CVarRef v_sep);
  public: void t_virtualimplode(CVarRef v_sep);
  public: void t_virtualbracketedimplode(CVarRef v_start, CVarRef v_sep, CVarRef v_end);
  public: void t_isempty();
  public: void t_getargument(CVarRef v_name);
  public: void t_loopcheck(CVarRef v_title);
  public: void t_istemplate();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppframe_h__
