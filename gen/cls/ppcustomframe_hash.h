
#ifndef __GENERATED_cls_ppcustomframe_hash_h__
#define __GENERATED_cls_ppcustomframe_hash_h__

#include <cls/ppframe_hash.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_Hash.php line 1297 */
class c_ppcustomframe_hash : virtual public c_ppframe_hash {
  BEGIN_CLASS_MAP(ppcustomframe_hash)
    PARENT_CLASS(ppframe)
    PARENT_CLASS(ppframe_hash)
  END_CLASS_MAP(ppcustomframe_hash)
  DECLARE_CLASS(ppcustomframe_hash, PPCustomFrame_Hash, ppframe_hash)
  void init();
  public: Variant m_args;
  public: void t___construct(Variant v_preprocessor, Variant v_args);
  public: ObjectData *create(Variant v_preprocessor, Variant v_args);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t___tostring();
  public: bool t_isempty();
  public: Variant t_getargument(CVarRef v_index);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppcustomframe_hash_h__
