
#ifndef __GENERATED_cls_revision_h__
#define __GENERATED_cls_revision_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 18157 */
class c_revision : virtual public ObjectData {
  BEGIN_CLASS_MAP(revision)
  END_CLASS_MAP(revision)
  DECLARE_CLASS(revision, Revision, ObjectData)
  void init();
  public: static Variant ti_newfromid(const char* cls, CVarRef v_id);
  public: static Variant ti_newfromtitle(const char* cls, CVarRef v_title, CVarRef v_id = 0LL);
  public: static Variant ti_loadfromid(const char* cls, CVarRef v_db, CVarRef v_id);
  public: static Variant ti_loadfrompageid(const char* cls, p_getdb v_db, CVarRef v_pageid, int64 v_id = 0LL);
  public: static Variant ti_loadfromtitle(const char* cls, CVarRef v_db, Object v_title, int64 v_id = 0LL);
  public: static Variant ti_loadfromtimestamp(const char* cls, p_getdb v_db, CVarRef v_title, Variant v_timestamp);
  public: static Variant ti_newfromconds(const char* cls, CVarRef v_conditions);
  public: static Variant ti_loadfromconds(const char* cls, CVarRef v_db, CVarRef v_conditions);
  public: static Array ti_fetchallrevisions(const char* cls, Object v_title);
  public: static Array ti_fetchrevision(const char* cls, Object v_title);
  public: static Array ti_fetchfromconds(const char* cls, CVarRef v_db, CVarRef v_conditions);
  public: static Array ti_selectfields(const char* cls);
  public: static Array ti_selecttextfields(const char* cls);
  public: static Array ti_selectpagefields(const char* cls);
  public: void t_revision(CVarRef v_row);
  public: ObjectData *create(CVarRef v_row);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_getid();
  public: Variant t_gettextid();
  public: Variant t_getparentid();
  public: Variant t_getsize();
  public: Variant t_gettitle();
  public: void t_settitle(CVarRef v_title);
  public: Variant t_getpage();
  public: Variant t_getuser(CVarRef v_audience = 1LL /* revision::FOR_PUBLIC */);
  public: Variant t_getrawuser();
  public: Variant t_getusertext(CVarRef v_audience = 1LL /* revision::FOR_PUBLIC */);
  public: Variant t_getrawusertext();
  public: Variant t_getcomment(CVarRef v_audience = 1LL /* revision::FOR_PUBLIC */);
  public: Variant t_getrawcomment();
  public: bool t_isminor();
  public: Variant t_isunpatrolled();
  public: bool t_isdeleted(CVarRef v_field);
  public: int64 t_getvisibility();
  public: Variant t_gettext(CVarRef v_audience = 1LL /* revision::FOR_PUBLIC */);
  public: Variant t_revtext();
  public: Variant t_getrawtext();
  public: Variant t_gettimestamp();
  public: Variant t_iscurrent();
  public: Variant t_getprevious();
  public: Variant t_getnext();
  public: int64 t_getpreviousrevisionid(CVarRef v_db);
  public: static Variant ti_getrevisiontext(const char* cls, CVarRef v_row, CStrRef v_prefix = "old_");
  public: static String ti_compressrevisiontext(const char* cls, Variant v_text);
  public: Variant t_inserton(CVarRef v_dbw);
  public: Variant t_loadtext();
  public: static Variant ti_newnullrevision(const char* cls, CVarRef v_dbw, CVarRef v_pageId, CVarRef v_summary, bool v_minor);
  public: Variant t_usercan(CVarRef v_field);
  public: static Variant ti_gettimestampfromid(const char* cls, CVarRef v_title, Variant v_id);
  public: static Variant ti_countbypageid(const char* cls, CVarRef v_db, CVarRef v_id);
  public: static Variant ti_countbytitle(const char* cls, CVarRef v_db, Object v_title);
  public: static Variant t_getrevisiontext(CVarRef v_row, CStrRef v_prefix = "old_") { return ti_getrevisiontext("revision", v_row, v_prefix); }
  public: static Array t_selecttextfields() { return ti_selecttextfields("revision"); }
  public: static Array t_selectpagefields() { return ti_selectpagefields("revision"); }
  public: static Array t_selectfields() { return ti_selectfields("revision"); }
  public: static Variant t_loadfromid(CVarRef v_db, CVarRef v_id) { return ti_loadfromid("revision", v_db, v_id); }
  public: static Variant t_newfromid(CVarRef v_id) { return ti_newfromid("revision", v_id); }
  public: static Array t_fetchrevision(Object v_title) { return ti_fetchrevision("revision", v_title); }
  public: static Variant t_loadfromconds(CVarRef v_db, CVarRef v_conditions) { return ti_loadfromconds("revision", v_db, v_conditions); }
  public: static Variant t_newfromconds(CVarRef v_conditions) { return ti_newfromconds("revision", v_conditions); }
  public: static Variant t_loadfromtitle(CVarRef v_db, Object v_title, int64 v_id = 0LL) { return ti_loadfromtitle("revision", v_db, v_title, v_id); }
  public: static Variant t_newfromtitle(CVarRef v_title, CVarRef v_id = 0LL) { return ti_newfromtitle("revision", v_title, v_id); }
  public: static Variant t_loadfrompageid(p_getdb v_db, CVarRef v_pageid, int64 v_id = 0LL) { return ti_loadfrompageid("revision", v_db, v_pageid, v_id); }
  public: static String t_compressrevisiontext(CVarRef v_text) { return ti_compressrevisiontext("revision", v_text); }
  public: static Variant t_countbypageid(CVarRef v_db, CVarRef v_id) { return ti_countbypageid("revision", v_db, v_id); }
  public: static Array t_fetchallrevisions(Object v_title) { return ti_fetchallrevisions("revision", v_title); }
  public: static Array t_fetchfromconds(CVarRef v_db, CVarRef v_conditions) { return ti_fetchfromconds("revision", v_db, v_conditions); }
  public: static Variant t_countbytitle(CVarRef v_db, Object v_title) { return ti_countbytitle("revision", v_db, v_title); }
  public: static Variant t_newnullrevision(CVarRef v_dbw, CVarRef v_pageId, CVarRef v_summary, bool v_minor) { return ti_newnullrevision("revision", v_dbw, v_pageId, v_summary, v_minor); }
  public: static Variant t_loadfromtimestamp(p_getdb v_db, CVarRef v_title, CVarRef v_timestamp) { return ti_loadfromtimestamp("revision", v_db, v_title, v_timestamp); }
  public: static Variant t_gettimestampfromid(CVarRef v_title, CVarRef v_id) { return ti_gettimestampfromid("revision", v_title, v_id); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_revision_h__
