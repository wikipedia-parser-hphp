
#ifndef __GENERATED_cls_skintemplate_h__
#define __GENERATED_cls_skintemplate_h__

#include <cls/skin.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 3277 */
class c_skintemplate : virtual public c_skin {
  BEGIN_CLASS_MAP(skintemplate)
    PARENT_CLASS(linker)
    PARENT_CLASS(skin)
  END_CLASS_MAP(skintemplate)
  DECLARE_CLASS(skintemplate, SkinTemplate, skin)
  void init();
  public: Variant m_stylename;
  public: Variant m_template;
  public: void t_initpage(p_outputpage v_out);
  public: void t_setupskinusercss(p_outputpage v_out);
  public: Variant t_setuptemplate(CVarRef v_classname, CVarRef v_repository = false, bool v_cache_dir = false);
  public: void t_outputpage(Variant v_out);
  public: void t_printorerror(CVarRef v_str);
  public: Variant t_buildpersonalurls();
  public: Variant t_tabaction(CVarRef v_title, CVarRef v_message, bool v_selected, Variant v_query = "", bool v_checkEdit = false);
  public: Array t_maketalkurldetails(CVarRef v_name, Variant v_urlaction = "");
  public: Array t_makearticleurldetails(CVarRef v_name, Variant v_urlaction = "");
  public: Variant t_buildcontentactionurls();
  public: Variant t_buildnavurls();
  public: Variant t_getnamespacekey();
  public: void t_setupuserjs(CVarRef v_allowUserJs);
  public: Variant t_setuppagecss();
  public: static Variant t_makei18nurl(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makei18nurl("skintemplate", v_name, v_urlaction); }
  public: static Variant t_getusableskins() { return ti_getusableskins("skintemplate"); }
  public: static String t_makeglobalvariablesscript(CVarRef v_data) { return ti_makeglobalvariablesscript("skintemplate", v_data); }
  public: static String t_makevariablesscript(CVarRef v_data) { return ti_makevariablesscript("skintemplate", v_data); }
  public: static Variant t_makensurl(CStrRef v_name, CVarRef v_urlaction = "", int64 v_namespace = 0LL /* NS_MAIN */) { return ti_makensurl("skintemplate", v_name, v_urlaction, v_namespace); }
  public: static Variant t_makemainpageurl(CVarRef v_urlaction = "") { return ti_makemainpageurl("skintemplate", v_urlaction); }
  public: static Variant t_makespecialurlsubpage(CStrRef v_name, CVarRef v_subpage, CVarRef v_urlaction = "") { return ti_makespecialurlsubpage("skintemplate", v_name, v_subpage, v_urlaction); }
  public: static Variant t_makeinternalorexternalurl(CVarRef v_name) { return ti_makeinternalorexternalurl("skintemplate", v_name); }
  public: static Variant t_newfromkey(CVarRef v_key) { return ti_newfromkey("skintemplate", v_key); }
  public: static Variant t_normalizekey(CVarRef v_key) { return ti_normalizekey("skintemplate", v_key); }
  public: static Variant t_getskinnames() { return ti_getskinnames("skintemplate"); }
  public: static Variant t_makeurl(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makeurl("skintemplate", v_name, v_urlaction); }
  public: static Array t_makeknownurldetails(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makeknownurldetails("skintemplate", v_name, v_urlaction); }
  public: static void t_checktitle(CVarRef v_title, CVarRef v_name) { ti_checktitle("skintemplate", v_title, v_name); }
  public: static Variant t_makespecialurl(CStrRef v_name, CVarRef v_urlaction = "") { return ti_makespecialurl("skintemplate", v_name, v_urlaction); }
  public: static Array t_makeurldetails(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makeurldetails("skintemplate", v_name, v_urlaction); }
  public: static Array t_splittrail(CVarRef v_trail) { return ti_splittrail("skintemplate", v_trail); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_skintemplate_h__
