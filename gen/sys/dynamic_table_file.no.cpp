
#include <cpp/base/hphp.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// File Invoke Table
Variant pm_php$CoreLinkFunctions_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$CoreParserFunctions_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$DateFormatter_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$LinkHolderArray_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$ParserCache_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$Parser_DiffTest_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$Parser_LinkHooks_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$ParserOptions_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$ParserOutput_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$Parser_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$Preprocessor_DOM_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$Preprocessor_Hash_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$Preprocessor_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant pm_php$Tidy_php(bool incOnce = false, LVariableTable* variables = NULL);

Variant invoke_file(CStrRef path, bool once /* = false */, LVariableTable* variables /* = NULL */,const char *currentDir /* = NULL */) {
  String s = canonicalize_path(path, "", 0);
  int64 hash = hash_string(s.data(), s.length());
  switch (hash & 31) {
    case 3:
      HASH_INCLUDE(0x25E803275E689C63LL, "Preprocessor_DOM.php", php$Preprocessor_DOM_php);
      break;
    case 5:
      HASH_INCLUDE(0x7FE741AF0628B0E5LL, "CoreParserFunctions.php", php$CoreParserFunctions_php);
      break;
    case 7:
      HASH_INCLUDE(0x196582A3292D37C7LL, "LinkHolderArray.php", php$LinkHolderArray_php);
      HASH_INCLUDE(0x115B7AEB63B84EC7LL, "ParserOutput.php", php$ParserOutput_php);
      break;
    case 9:
      HASH_INCLUDE(0x50BE9A28B832DC09LL, "Parser_DiffTest.php", php$Parser_DiffTest_php);
      break;
    case 14:
      HASH_INCLUDE(0x48B82F82876E848ELL, "Tidy.php", php$Tidy_php);
      break;
    case 15:
      HASH_INCLUDE(0x7901A7716EAF928FLL, "Preprocessor_Hash.php", php$Preprocessor_Hash_php);
      break;
    case 20:
      HASH_INCLUDE(0x3E55D936C49C25D4LL, "ParserCache.php", php$ParserCache_php);
      break;
    case 22:
      HASH_INCLUDE(0x65E1BF11929A4076LL, "Parser_LinkHooks.php", php$Parser_LinkHooks_php);
      break;
    case 27:
      HASH_INCLUDE(0x194C5D4EDF2D217BLL, "CoreLinkFunctions.php", php$CoreLinkFunctions_php);
      break;
    case 28:
      HASH_INCLUDE(0x12294D725D33899CLL, "DateFormatter.php", php$DateFormatter_php);
      HASH_INCLUDE(0x5AD4076C853844BCLL, "Preprocessor.php", php$Preprocessor_php);
      break;
    case 31:
      HASH_INCLUDE(0x71044A8C16E3589FLL, "ParserOptions.php", php$ParserOptions_php);
      HASH_INCLUDE(0x0CAAB104CDFBED3FLL, "Parser.php", php$Parser_php);
      break;
    default:
      break;
  }
  Logger::Verbose("Tried to invoke %s but file not found.", s.data());
  return throw_missing_file(s.data());
}

///////////////////////////////////////////////////////////////////////////////
}
