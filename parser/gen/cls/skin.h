
#ifndef __GENERATED_cls_skin_h__
#define __GENERATED_cls_skin_h__

#include <cls/linker.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 4684 */
class c_skin : virtual public c_linker {
  BEGIN_CLASS_MAP(skin)
    PARENT_CLASS(linker)
  END_CLASS_MAP(skin)
  DECLARE_CLASS(skin, Skin, linker)
  void init();
  public: Variant m_mWatchLinkNum;
  public: Variant m_searchboxes;
  public: Variant m_mRevisionId;
  public: Variant m_skinname;
  public: void t_skin();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: static Variant ti_getskinnames(const char* cls);
  public: static Variant ti_getusableskins(const char* cls);
  public: static Variant ti_normalizekey(const char* cls, Variant v_key);
  public: static Variant ti_newfromkey(const char* cls, Variant v_key);
  public: String t_getstylesheet();
  public: Variant t_getskinname();
  public: Variant t_qbsetting();
  public: void t_initpage(p_outputpage v_out);
  public: void t_preloadexistence();
  public: void t_addmetadatalinks(p_outputpage v_out);
  public: void t_setmembers();
  public: void t_outputpage(Variant v_out);
  public: static String ti_makevariablesscript(const char* cls, CVarRef v_data);
  public: static String ti_makeglobalvariablesscript(const char* cls, CVarRef v_data);
  public: String t_getheadscripts(CVarRef v_allowUserJs);
  public: Variant t_usercanpreview(CVarRef v_action);
  public: Variant t_generateuserjs();
  public: String t_generateuserstylesheet();
  public: String t_reallygenerateuserstylesheet();
  public: void t_setupusercss(Variant v_out);
  public: void t_setupskinusercss(p_outputpage v_out);
  public: Variant t_getbodyoptions();
  public: String t_getpageclasses(CVarRef v_title);
  public: Variant t_getlogo();
  public: String t_beforecontent();
  public: String t_dobeforecontent();
  public: String t_getcategorylinks();
  public: String t_drawcategorybrowser(CVarRef v_tree, Variant v_skin);
  public: Variant t_getcategories();
  public: String t_getquickbarcompensator(int64 v_rows = 1LL);
  public: Variant t_aftercontenthook();
  public: String t_generatedebughtml();
  public: String t_aftercontent();
  public: Variant t_bottomscripts();
  public: Variant t_printsource();
  public: String t_printfooter();
  public: String t_doaftercontent();
  public: Variant t_pagetitlelinks();
  public: Variant t_getundeletelink();
  public: Variant t_printablelink();
  public: String t_pagetitle();
  public: String t_pagesubtitle();
  public: Variant t_subpagesubtitle();
  public: bool t_showipinheader();
  public: Variant t_nameandlogin();
  public: Variant t_getsearchlink();
  public: String t_escapesearchlink();
  public: String t_searchform();
  public: String t_toplinks();
  public: Variant t_extensiontablinks();
  public: Variant t_variantlinks();
  public: String t_bottomlinks();
  public: String t_pagestats();
  public: String t_getcopyright(String v_type = "detect");
  public: Variant t_getcopyrighticon();
  public: String t_getpoweredby();
  public: String t_lastmodified();
  public: String t_logotext(CStrRef v_align = "");
  public: String t_specialpageslist();
  public: String t_mainpagelink();
  public: Variant t_copyrightlink();
  public: Variant t_footerlink(CStrRef v_desc, CStrRef v_page);
  public: Variant t_privacylink();
  public: Variant t_aboutlink();
  public: Variant t_disclaimerlink();
  public: Variant t_editthispage();
  public: Variant t_editurloptions();
  public: String t_deletethispage();
  public: String t_protectthispage();
  public: Variant t_watchthispage();
  public: Variant t_movethispage();
  public: Variant t_historylink();
  public: String t_whatlinkshere();
  public: String t_usercontribslink();
  public: bool t_showemailuser(CVarRef v_id);
  public: String t_emailuserlink();
  public: Variant t_watchpagelinkslink();
  public: String t_trackbacklink();
  public: String t_otherlanguages();
  public: Variant t_talklink();
  public: String t_commentlink();
  public: static Variant ti_makemainpageurl(const char* cls, Variant v_urlaction = "");
  public: static Variant ti_makespecialurl(const char* cls, CStrRef v_name, Variant v_urlaction = "");
  public: static Variant ti_makespecialurlsubpage(const char* cls, CStrRef v_name, CVarRef v_subpage, Variant v_urlaction = "");
  public: static Variant ti_makei18nurl(const char* cls, CVarRef v_name, Variant v_urlaction = "");
  public: static Variant ti_makeurl(const char* cls, CVarRef v_name, Variant v_urlaction = "");
  public: static Variant ti_makeinternalorexternalurl(const char* cls, CVarRef v_name);
  public: static Variant ti_makensurl(const char* cls, CStrRef v_name, Variant v_urlaction = "", int64 v_namespace = 0LL /* NS_MAIN */);
  public: static Array ti_makeurldetails(const char* cls, CVarRef v_name, Variant v_urlaction = "");
  public: static Array ti_makeknownurldetails(const char* cls, CVarRef v_name, Variant v_urlaction = "");
  public: static void ti_checktitle(const char* cls, Variant v_title, CVarRef v_name);
  public: Variant t_buildsidebar();
  public: static Variant t_makei18nurl(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makei18nurl("skin", v_name, v_urlaction); }
  public: static Variant t_getusableskins() { return ti_getusableskins("skin"); }
  public: static String t_makeglobalvariablesscript(CVarRef v_data) { return ti_makeglobalvariablesscript("skin", v_data); }
  public: static String t_makevariablesscript(CVarRef v_data) { return ti_makevariablesscript("skin", v_data); }
  public: static Variant t_makensurl(CStrRef v_name, CVarRef v_urlaction = "", int64 v_namespace = 0LL /* NS_MAIN */) { return ti_makensurl("skin", v_name, v_urlaction, v_namespace); }
  public: static Variant t_makemainpageurl(CVarRef v_urlaction = "") { return ti_makemainpageurl("skin", v_urlaction); }
  public: static Variant t_makespecialurlsubpage(CStrRef v_name, CVarRef v_subpage, CVarRef v_urlaction = "") { return ti_makespecialurlsubpage("skin", v_name, v_subpage, v_urlaction); }
  public: static Variant t_makeinternalorexternalurl(CVarRef v_name) { return ti_makeinternalorexternalurl("skin", v_name); }
  public: static Variant t_newfromkey(CVarRef v_key) { return ti_newfromkey("skin", v_key); }
  public: static Variant t_normalizekey(CVarRef v_key) { return ti_normalizekey("skin", v_key); }
  public: static Variant t_getskinnames() { return ti_getskinnames("skin"); }
  public: static Variant t_makeurl(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makeurl("skin", v_name, v_urlaction); }
  public: static Array t_makeknownurldetails(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makeknownurldetails("skin", v_name, v_urlaction); }
  public: static void t_checktitle(CVarRef v_title, CVarRef v_name) { ti_checktitle("skin", v_title, v_name); }
  public: static Variant t_makespecialurl(CStrRef v_name, CVarRef v_urlaction = "") { return ti_makespecialurl("skin", v_name, v_urlaction); }
  public: static Array t_makeurldetails(CVarRef v_name, CVarRef v_urlaction = "") { return ti_makeurldetails("skin", v_name, v_urlaction); }
  public: static Array t_splittrail(CVarRef v_trail) { return ti_splittrail("skin", v_trail); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_skin_h__
