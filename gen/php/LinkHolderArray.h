
#ifndef __GENERATED_php_LinkHolderArray_h__
#define __GENERATED_php_LinkHolderArray_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/LinkHolderArray.fw.h>

// Declarations
#include <cls/linkholderarray.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$LinkHolderArray_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_linkholderarray(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_LinkHolderArray_h__
