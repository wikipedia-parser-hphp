
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const int64 k_MW_MATH_MATHML;
extern const StaticString k_MEDIATYPE_MULTIMEDIA;
extern const int64 k_DB_MASTER;
extern const int64 k_NS_MEDIAWIKI;
extern const int64 k_DBO_PERSISTENT;
extern const int64 k_TC_SELF;
extern const StaticString k_MEDIATYPE_VIDEO;
extern const int64 k_MW_SPECIALPAGE_VERSION;
extern const int64 k_CACHE_MEMCACHED;
extern const bool k_UTF8_TAIL;
extern const int64 k_UNICODE_MAX;
extern const int64 k_ALF_PRELOAD_EXISTENCE;
extern const int64 k_UNICODE_HANGUL_NCOUNT;
extern const int64 k_DBO_IGNORE;
extern const StaticString k_MW_DATE_YMD;
extern const StaticString k_MW_CHAR_REFS_REGEX;
extern const StaticString k_UTF8_FDD0;
extern const int64 k_MW_MATH_SOURCE;
extern const StaticString k_RE_IPV6_WORD;
extern const int64 k_UNICODE_HANGUL_LEND;
extern const int64 k_APCOND_IPINRANGE;
extern const int64 k_DBO_NOBUFFER;
extern const int64 k_OT_MSG;
extern const int64 k_TS_DB;
extern const int64 k_TS_DB2;
extern const int64 k_APCOND_AGE_FROM_EDIT;
extern const int64 k_NS_PROJECT_TALK;
extern const int64 k_CACHE_ACCEL;
extern const int64 k_DB_WRITE;
extern const StaticString k_UTF8_HANGUL_LEND;
extern const int64 k_MW_SUPPORTS_PARSERFIRSTCALLINIT;
extern const int64 k_UNORM_FCD;
extern const int64 k_NS_IMAGE_TALK;
extern const int64 k_TS_POSTGRES;
extern const StaticString k_IP_ADDRESS_STRING;
extern const int64 k_TS_RFC2822;
extern const int64 k_UNICODE_HANGUL_TCOUNT;
extern const StaticString k_MW_DATE_ISO;
extern const int64 k_TC_MYSQL;
extern const int64 k_UNORM_NFKC;
extern const int64 k_UNORM_NFKD;
extern const int64 k_NS_HELP;
extern const int64 k_APCOND_EMAILCONFIRMED;
extern const StaticString k_UTF8_FDEF;
extern const int64 k_MW_REV_DELETED_USER;
extern const int64 k_UNICODE_HANGUL_VBASE;
extern const int64 k_UNICODE_HANGUL_FIRST;
extern const int64 k_EDIT_AUTOSUMMARY;
extern const StaticString k_UTF8_MAX;
extern const int64 k_TS_ISO_8601;
extern const bool k_NORMALIZE_ICU;
extern const int64 k_NS_USER;
extern const int64 k_MW_REV_DELETED_TEXT;
extern const int64 k_UNORM_NFC;
extern const int64 k_UNICODE_HANGUL_TEND;
extern const int64 k_UNORM_NFD;
extern const int64 k_AV_VIRUS_FOUND;
extern const int64 k_OT_HTML;
extern const int64 k_APCOND_INGROUPS;
extern const int64 k_MW_SUPPORTS_EDITFILTERMERGED;
extern const int64 k_UNORM_DEFAULT;
extern const int64 k_DB_SLAVE;
extern const int64 k_ALF_NO_LINK_LOCK;
extern const int64 k_MW_REV_DELETED_COMMENT;
extern const int64 k_CASCADE;
extern const StaticString k_UTF8_HANGUL_TEND;
extern const int64 k_TS_EXIF;
extern const int64 k_NS_HELP_TALK;
extern const int64 k_NS_PROJECT;
extern const int64 k_NS_SPECIAL;
extern const int64 k_DBO_DEFAULT;
extern const StaticString k_RE_IPV6_GAP;
extern const StaticString k_UTF8_HANGUL_TBASE;
extern const int64 k_NS_FILE;
extern const StaticString k_MW_DATE_MDY;
extern const int64 k_APCOND_AGE;
extern const int64 k_EDIT_UPDATE;
extern const bool k_AV_SCAN_FAILED;
extern const int64 k_LIST_AND;
extern const int64 k_RC_LOG;
extern const int64 k_NS_IMAGE;
extern const int64 k_NS_TALK;
extern const int64 k_MW_MATH_SIMPLE;
extern const StaticString k_RE_IPV6_PREFIX;
extern const StaticString k_MW_DATE_DMY;
extern const int64 k_NS_TEMPLATE;
extern const StaticString k_UTF8_OVERLONG_A;
extern const int64 k_UNORM_NONE;
extern const StaticString k_UTF8_OVERLONG_B;
extern const int64 k_CACHE_DB;
extern const StaticString k_UTF8_OVERLONG_C;
extern const int64 k_TS_MW;
extern const StaticString k_UTF8_HANGUL_LBASE;
extern const int64 k_APCOND_ISIP;
extern const int64 k_LIST_COMMA;
extern const int64 k_NS_CATEGORY;
extern const int64 k_UNICODE_SURROGATE_FIRST;
extern const int64 k_RC_NEW;
extern const int64 k_NS_FILE_TALK;
extern const int64 k_DB_READ;
extern const StaticString k_UTF8_FFFE;
extern const StaticString k_UTF8_FFFF;
extern const int64 k_TS_UNIX;
extern const StaticString k_EDIT_TOKEN_SUFFIX;
extern const bool k_UTF8_HEAD;
extern const int64 k_OT_WIKI;
extern const int64 k_TS_ORACLE;
extern const int64 k_APCOND_EDITCOUNT;
extern const int64 k_CACHE_NONE;
extern const int64 k_CACHE_ANYTHING;
extern const int64 k_NS_MEDIAWIKI_TALK;
extern const int64 k_GAID_FOR_UPDATE;
extern const int64 k_UNICODE_HANGUL_VEND;
extern const int64 k_UNICODE_HANGUL_VCOUNT;
extern const int64 k_DBO_TRX;
extern const int64 k_LIST_SET;
extern const StaticString k_RE_IPV6_ADD;
extern const int64 k_SFH_OBJECT_ARGS;
extern const int64 k_NS_MEDIA;
extern const int64 k_AV_SCAN_ABORTED;
extern const int64 k_SLH_PATTERN;
extern const StaticString k_UTF8_HANGUL_VEND;
extern const StaticString k_RE_IP_BYTE;
extern const StaticString k_MW_DATE_DEFAULT;
extern const int64 k_DB_LAST;
extern const StaticString k_RE_IP_PREFIX;
extern const int64 k_EDIT_FORCE_BOT;
extern const StaticString k_MEDIATYPE_BITMAP;
extern const int64 k_NS_USER_TALK;
extern const int64 k_UNICODE_HANGUL_LCOUNT;
extern const StaticString k_MEDIATYPE_EXECUTABLE;
extern const int64 k_MW_MATH_MODERN;
extern const int64 k_TC_SHELL;
extern const int64 k_ALF_NO_BLOCK_LOCK;
extern const StaticString k_RE_IPV6_BLOCK;
extern const StaticString k_RE_IPV6_V4_PREFIX;
extern const int64 k_NS_CATEGORY_TALK;
extern const int64 k_EDIT_NEW;
extern const StaticString k_UTF8_SURROGATE_FIRST;
extern const StaticString k_RE_IP_BLOCK;
extern const int64 k_RC_MOVE;
extern const int64 k_EDIT_DEFER_UPDATES;
extern const int64 k_RLH_FOR_UPDATE;
extern const int64 k_OT_PREPROCESS;
extern const int64 k_LIST_OR;
extern const int64 k_ALF_PRELOAD_LINKS;
extern const int64 k_DBO_DEBUG;
extern const int64 k_AV_NO_VIRUS;
extern const int64 k_MW_USER_VERSION;
extern const int64 k_EDIT_MINOR;
extern const int64 k_UNICODE_HANGUL_TBASE;
extern const int64 k_MW_MATH_HTML;
extern const StaticString k_MEDIATYPE_DRAWING;
extern const int64 k_UNICODE_SURROGATE_LAST;
extern const StaticString k_MEDIATYPE_TEXT;
extern const int64 k_MW_MATH_PNG;
extern const int64 k_NS_TEMPLATE_TALK;
extern const int64 k_UNICODE_HANGUL_LAST;
extern const int64 k_SFH_NO_HASH;
extern const int64 k_UNICODE_REPLACEMENT;
extern const StaticString k_MEDIATYPE_OFFICE;
extern const int64 k_NS_MAIN;
extern const int64 k_TC_HTML;
extern const int64 k_RC_EDIT;
extern const StaticString k_UTF8_HANGUL_VBASE;
extern const StaticString k_UTF8_HANGUL_LAST;
extern const StaticString k_UTF8_HANGUL_FIRST;
extern const StaticString k_RE_IP_ADD;
extern const int64 k_MW_REV_DELETED_RESTRICTED;
extern const int64 k_USER_TOKEN_LENGTH;
extern const StaticString k_MEDIATYPE_ARCHIVE;
extern const int64 k_UNICODE_HANGUL_LBASE;
extern const StaticString k_TBLSRC_URL;
extern const StaticString k_MEDIATYPE_AUDIO;
extern const int64 k_RC_MOVE_OVER_REDIRECT;
extern const int64 k_CACHE_DBA;
extern const StaticString k_UTF8_REPLACEMENT;
extern const StaticString k_MEDIATYPE_UNKNOWN;
extern const int64 k_LIST_NAMES;
extern const int64 k_TC_PCRE;
extern const int64 k_EDIT_SUPPRESS_RC;
extern const StaticString k_UTF8_SURROGATE_LAST;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 511) {
    case 0:
      HASH_RETURN(0x34DB5CCC429FEA00LL, k_RE_IPV6_PREFIX, RE_IPV6_PREFIX);
      break;
    case 2:
      HASH_RETURN(0x080741BDD303B402LL, k_NS_MEDIA, NS_MEDIA);
      HASH_RETURN(0x1BC96DED91FADE02LL, k_NS_TALK, NS_TALK);
      break;
    case 7:
      HASH_RETURN(0x44A337C174A40607LL, k_RE_IPV6_BLOCK, RE_IPV6_BLOCK);
      break;
    case 13:
      HASH_RETURN(0x3597B220C626640DLL, k_TS_MW, TS_MW);
      HASH_RETURN(0x63DD546A4381040DLL, k_OT_MSG, OT_MSG);
      HASH_RETURN(0x6A6F21C655032A0DLL, k_APCOND_INGROUPS, APCOND_INGROUPS);
      break;
    case 15:
      HASH_RETURN(0x25FA83AE9EB3160FLL, k_NS_PROJECT_TALK, NS_PROJECT_TALK);
      break;
    case 18:
      HASH_RETURN(0x215391F73D415A12LL, k_APCOND_AGE_FROM_EDIT, APCOND_AGE_FROM_EDIT);
      break;
    case 23:
      HASH_RETURN(0x3CE34196AE04B017LL, k_CACHE_DB, CACHE_DB);
      break;
    case 24:
      HASH_RETURN(0x38BD3051E011F818LL, k_DBO_TRX, DBO_TRX);
      break;
    case 25:
      HASH_RETURN(0x5B7935278C7C8619LL, k_EDIT_TOKEN_SUFFIX, EDIT_TOKEN_SUFFIX);
      HASH_RETURN(0x6481C72422EF9019LL, g->k_MEDIAWIKI, MEDIAWIKI);
      break;
    case 28:
      HASH_RETURN(0x30C5F43ADD26FE1CLL, k_RE_IP_BLOCK, RE_IP_BLOCK);
      break;
    case 32:
      HASH_RETURN(0x3AE958BCCC8AA420LL, k_UNICODE_HANGUL_LBASE, UNICODE_HANGUL_LBASE);
      HASH_RETURN(0x7052C67504792E20LL, k_MEDIATYPE_OFFICE, MEDIATYPE_OFFICE);
      break;
    case 36:
      HASH_RETURN(0x7EC379E7577A9624LL, k_NS_HELP, NS_HELP);
      break;
    case 41:
      HASH_RETURN(0x521F0CABF49C8629LL, k_MW_SUPPORTS_EDITFILTERMERGED, MW_SUPPORTS_EDITFILTERMERGED);
      break;
    case 44:
      HASH_RETURN(0x7D250630FEE13C2CLL, k_MW_MATH_MATHML, MW_MATH_MATHML);
      HASH_RETURN(0x037FB77FEFBC1E2CLL, k_CACHE_ANYTHING, CACHE_ANYTHING);
      break;
    case 48:
      HASH_RETURN(0x75C60067A794F430LL, k_UTF8_HANGUL_LBASE, UTF8_HANGUL_LBASE);
      break;
    case 51:
      HASH_RETURN(0x7D62F1BD206E7233LL, k_MW_SPECIALPAGE_VERSION, MW_SPECIALPAGE_VERSION);
      break;
    case 52:
      HASH_RETURN(0x49A12012F06FD034LL, k_MW_DATE_DMY, MW_DATE_DMY);
      break;
    case 53:
      HASH_RETURN(0x51F6468961935635LL, k_ALF_PRELOAD_LINKS, ALF_PRELOAD_LINKS);
      break;
    case 55:
      HASH_RETURN(0x5747A35111846637LL, k_UNICODE_HANGUL_TCOUNT, UNICODE_HANGUL_TCOUNT);
      HASH_RETURN(0x65A6A8A8A7D78437LL, k_TS_RFC2822, TS_RFC2822);
      break;
    case 59:
      HASH_RETURN(0x7B2A562921061E3BLL, k_UNICODE_HANGUL_VBASE, UNICODE_HANGUL_VBASE);
      break;
    case 62:
      HASH_RETURN(0x3F59146969ED7E3ELL, k_CACHE_ACCEL, CACHE_ACCEL);
      break;
    case 63:
      HASH_RETURN(0x5B3042082E43463FLL, k_LIST_NAMES, LIST_NAMES);
      break;
    case 64:
      HASH_RETURN(0x54B3D538F88BF040LL, k_NS_TEMPLATE, NS_TEMPLATE);
      break;
    case 66:
      HASH_RETURN(0x08B031B5FF3BC042LL, k_MW_MATH_MODERN, MW_MATH_MODERN);
      break;
    case 72:
      HASH_RETURN(0x0304D4250D7F8A48LL, k_MEDIATYPE_BITMAP, MEDIATYPE_BITMAP);
      break;
    case 74:
      HASH_RETURN(0x5DF9710D8151304ALL, k_DBO_DEBUG, DBO_DEBUG);
      break;
    case 77:
      HASH_RETURN(0x74F6479C244DB04DLL, k_MW_DATE_DEFAULT, MW_DATE_DEFAULT);
      break;
    case 79:
      HASH_RETURN(0x4DA2C12845F6664FLL, k_MW_REV_DELETED_TEXT, MW_REV_DELETED_TEXT);
      break;
    case 85:
      HASH_RETURN(0x26A7C87F34AE0255LL, k_UTF8_OVERLONG_B, UTF8_OVERLONG_B);
      break;
    case 91:
      HASH_RETURN(0x2B502CC63CE4F85BLL, k_LIST_SET, LIST_SET);
      break;
    case 93:
      HASH_RETURN(0x7F2A2F19C160C65DLL, k_MEDIATYPE_AUDIO, MEDIATYPE_AUDIO);
      break;
    case 95:
      HASH_RETURN(0x02F3B2CA6D728C5FLL, k_UTF8_OVERLONG_C, UTF8_OVERLONG_C);
      break;
    case 97:
      HASH_RETURN(0x0A4FD418B2FA7661LL, k_LIST_COMMA, LIST_COMMA);
      break;
    case 98:
      HASH_RETURN(0x17E9926863D83C62LL, k_TBLSRC_URL, TBLSRC_URL);
      break;
    case 103:
      HASH_RETURN(0x1551A4A841687467LL, k_NS_USER, NS_USER);
      break;
    case 104:
      HASH_RETURN(0x019E44A6A24C2668LL, k_MEDIATYPE_DRAWING, MEDIATYPE_DRAWING);
      break;
    case 105:
      HASH_RETURN(0x5C5E0C3BFEE44669LL, k_NS_IMAGE_TALK, NS_IMAGE_TALK);
      break;
    case 108:
      HASH_RETURN(0x1CA02A0C50B55E6CLL, k_TS_EXIF, TS_EXIF);
      HASH_RETURN(0x6EA301FB353D246CLL, k_CACHE_MEMCACHED, CACHE_MEMCACHED);
      break;
    case 109:
      HASH_RETURN(0x7CBECD6C91F5C26DLL, k_APCOND_EDITCOUNT, APCOND_EDITCOUNT);
      break;
    case 114:
      HASH_RETURN(0x1A21106BA3DFF072LL, k_UTF8_FFFF, UTF8_FFFF);
      break;
    case 121:
      HASH_RETURN(0x0997335C7A77FA79LL, k_DBO_IGNORE, DBO_IGNORE);
      break;
    case 122:
      HASH_RETURN(0x176F13E42FD1827ALL, k_MW_REV_DELETED_USER, MW_REV_DELETED_USER);
      HASH_RETURN(0x1AA0F7D777EA067ALL, k_RC_MOVE_OVER_REDIRECT, RC_MOVE_OVER_REDIRECT);
      break;
    case 125:
      HASH_RETURN(0x6E9078C19232FE7DLL, k_MW_MATH_SOURCE, MW_MATH_SOURCE);
      break;
    case 130:
      HASH_RETURN(0x3829DBCFFD674082LL, k_DBO_PERSISTENT, DBO_PERSISTENT);
      HASH_RETURN(0x5E3B1CFD130F1082LL, k_MEDIATYPE_UNKNOWN, MEDIATYPE_UNKNOWN);
      break;
    case 132:
      HASH_RETURN(0x33D11CC2864EE684LL, k_UNICODE_HANGUL_LAST, UNICODE_HANGUL_LAST);
      break;
    case 133:
      HASH_RETURN(0x2A895AFDB57A2485LL, k_MW_MATH_HTML, MW_MATH_HTML);
      break;
    case 140:
      HASH_RETURN(0x0A51A63DC1BE368CLL, k_TS_DB2, TS_DB2);
      break;
    case 141:
      HASH_RETURN(0x2E51C6FF7658788DLL, k_NORMALIZE_ICU, NORMALIZE_ICU);
      break;
    case 142:
      HASH_RETURN(0x3E9BE9A1F9F6D28ELL, k_UTF8_SURROGATE_FIRST, UTF8_SURROGATE_FIRST);
      break;
    case 143:
      HASH_RETURN(0x374BB905E4EE348FLL, k_TC_PCRE, TC_PCRE);
      break;
    case 144:
      HASH_RETURN(0x7291D2899F2D4C90LL, k_RC_MOVE, RC_MOVE);
      break;
    case 145:
      HASH_RETURN(0x433C8254E2953091LL, k_IP_ADDRESS_STRING, IP_ADDRESS_STRING);
      break;
    case 149:
      HASH_RETURN(0x40F08E0E47E30E95LL, k_UTF8_FFFE, UTF8_FFFE);
      break;
    case 152:
      HASH_RETURN(0x695B29A868792498LL, k_EDIT_FORCE_BOT, EDIT_FORCE_BOT);
      HASH_RETURN(0x5E2B4F68FF786898LL, k_OT_WIKI, OT_WIKI);
      break;
    case 156:
      HASH_RETURN(0x477BB30CC87FE09CLL, k_CACHE_NONE, CACHE_NONE);
      break;
    case 157:
      HASH_RETURN(0x4A73D4FD6BCCDE9DLL, k_UTF8_HEAD, UTF8_HEAD);
      break;
    case 165:
      HASH_RETURN(0x5526525D9B3400A5LL, k_UNICODE_SURROGATE_FIRST, UNICODE_SURROGATE_FIRST);
      break;
    case 173:
      HASH_RETURN(0x0A139CE9645262ADLL, k_EDIT_UPDATE, EDIT_UPDATE);
      break;
    case 179:
      HASH_RETURN(0x16B6E2DBFA442CB3LL, k_UNICODE_HANGUL_VCOUNT, UNICODE_HANGUL_VCOUNT);
      break;
    case 181:
      HASH_RETURN(0x73BD9B8038E9C8B5LL, k_RE_IP_BYTE, RE_IP_BYTE);
      break;
    case 184:
      HASH_RETURN(0x18EC7E58689D24B8LL, k_TC_MYSQL, TC_MYSQL);
      break;
    case 192:
      HASH_RETURN(0x21345E48185D08C0LL, k_ALF_NO_LINK_LOCK, ALF_NO_LINK_LOCK);
      break;
    case 196:
      HASH_RETURN(0x186BD06C6FB200C4LL, k_UNORM_FCD, UNORM_FCD);
      break;
    case 203:
      HASH_RETURN(0x11ECE4DB67E584CBLL, k_UTF8_HANGUL_VEND, UTF8_HANGUL_VEND);
      break;
    case 208:
      HASH_RETURN(0x7F7CA5FDC784CCD0LL, k_APCOND_EMAILCONFIRMED, APCOND_EMAILCONFIRMED);
      break;
    case 209:
      HASH_RETURN(0x63C731B58CF688D1LL, k_EDIT_DEFER_UPDATES, EDIT_DEFER_UPDATES);
      break;
    case 210:
      HASH_RETURN(0x208F6CD6101078D2LL, k_UNORM_NONE, UNORM_NONE);
      break;
    case 211:
      HASH_RETURN(0x207D21365C9EA4D3LL, k_UNICODE_SURROGATE_LAST, UNICODE_SURROGATE_LAST);
      break;
    case 221:
      HASH_RETURN(0x4C98F3AF43851CDDLL, k_TC_SHELL, TC_SHELL);
      break;
    case 225:
      HASH_RETURN(0x5B49598A36AE0AE1LL, k_TS_POSTGRES, TS_POSTGRES);
      break;
    case 231:
      HASH_RETURN(0x527D1DCD5AFBF4E7LL, k_GAID_FOR_UPDATE, GAID_FOR_UPDATE);
      break;
    case 234:
      HASH_RETURN(0x2950FAFBACAD0CEALL, k_UNICODE_HANGUL_LCOUNT, UNICODE_HANGUL_LCOUNT);
      HASH_RETURN(0x52672AC667608AEALL, k_MEDIATYPE_EXECUTABLE, MEDIATYPE_EXECUTABLE);
      break;
    case 235:
      HASH_RETURN(0x0D460E39F8C018EBLL, k_NS_TEMPLATE_TALK, NS_TEMPLATE_TALK);
      break;
    case 243:
      HASH_RETURN(0x3F136E18D6C2A2F3LL, k_APCOND_AGE, APCOND_AGE);
      break;
    case 244:
      HASH_RETURN(0x5F77CB7780C9D2F4LL, k_EDIT_SUPPRESS_RC, EDIT_SUPPRESS_RC);
      break;
    case 245:
      HASH_RETURN(0x2E97AC4E447B26F5LL, k_UNORM_NFD, UNORM_NFD);
      break;
    case 248:
      HASH_RETURN(0x45755BD5BEBB9EF8LL, k_LIST_AND, LIST_AND);
      break;
    case 250:
      HASH_RETURN(0x20622068D0A92AFALL, k_NS_MEDIAWIKI_TALK, NS_MEDIAWIKI_TALK);
      break;
    case 252:
      HASH_RETURN(0x083F40FAA50BB0FCLL, k_AV_VIRUS_FOUND, AV_VIRUS_FOUND);
      break;
    case 254:
      HASH_RETURN(0x5003D785CE55F0FELL, k_RC_NEW, RC_NEW);
      break;
    case 255:
      HASH_RETURN(0x4EBCDEADBBC386FFLL, k_ALF_PRELOAD_EXISTENCE, ALF_PRELOAD_EXISTENCE);
      break;
    case 261:
      HASH_RETURN(0x38DADF46D30B6105LL, k_ALF_NO_BLOCK_LOCK, ALF_NO_BLOCK_LOCK);
      break;
    case 262:
      HASH_RETURN(0x42066F8E2B3A5B06LL, k_TC_HTML, TC_HTML);
      HASH_RETURN(0x269292323C90CD06LL, k_NS_CATEGORY, NS_CATEGORY);
      break;
    case 265:
      HASH_RETURN(0x37900B9FAFDC3909LL, k_UTF8_SURROGATE_LAST, UTF8_SURROGATE_LAST);
      HASH_RETURN(0x59E6E69E71AD2109LL, k_UTF8_OVERLONG_A, UTF8_OVERLONG_A);
      break;
    case 271:
      HASH_RETURN(0x2D0EE62D5CE94F0FLL, k_AV_SCAN_FAILED, AV_SCAN_FAILED);
      break;
    case 275:
      HASH_RETURN(0x6F9C8379E2398913LL, k_UNORM_NFC, UNORM_NFC);
      HASH_RETURN(0x21971E34CB8BC313LL, k_DBO_DEFAULT, DBO_DEFAULT);
      break;
    case 277:
      HASH_RETURN(0x1DA7582A2E261B15LL, k_RE_IPV6_V4_PREFIX, RE_IPV6_V4_PREFIX);
      HASH_RETURN(0x421DB1DF662D3515LL, k_MW_DATE_YMD, MW_DATE_YMD);
      break;
    case 283:
      HASH_RETURN(0x3AD72A7E8AA7E11BLL, k_UNICODE_HANGUL_NCOUNT, UNICODE_HANGUL_NCOUNT);
      break;
    case 284:
      HASH_RETURN(0x391F12DEC7F3791CLL, k_UTF8_MAX, UTF8_MAX);
      break;
    case 286:
      HASH_RETURN(0x4FB71B14C48DB11ELL, k_UTF8_FDEF, UTF8_FDEF);
      break;
    case 288:
      HASH_RETURN(0x443EE53DBC3C7320LL, k_UNICODE_HANGUL_TBASE, UNICODE_HANGUL_TBASE);
      HASH_RETURN(0x2C5BC2FC88589D20LL, k_RLH_FOR_UPDATE, RLH_FOR_UPDATE);
      break;
    case 289:
      HASH_RETURN(0x75376A3FB9537B21LL, k_NS_MAIN, NS_MAIN);
      break;
    case 291:
      HASH_RETURN(0x3304B425D036F923LL, k_RE_IPV6_WORD, RE_IPV6_WORD);
      break;
    case 292:
      HASH_RETURN(0x53B1C38B7A4DA324LL, k_DB_READ, DB_READ);
      break;
    case 294:
      HASH_RETURN(0x1D8E65386B5AE726LL, g->k_TESTWIKI, TESTWIKI);
      break;
    case 296:
      HASH_RETURN(0x7F7E194759C97F28LL, g->k_MW_DB, MW_DB);
      break;
    case 297:
      HASH_RETURN(0x08E8834131576929LL, k_CASCADE, CASCADE);
      break;
    case 300:
      HASH_RETURN(0x62C53BACA482172CLL, k_RC_LOG, RC_LOG);
      break;
    case 302:
      HASH_RETURN(0x2D1B05555882C72ELL, k_NS_MEDIAWIKI, NS_MEDIAWIKI);
      break;
    case 305:
      HASH_RETURN(0x4627462727030D31LL, k_NS_FILE_TALK, NS_FILE_TALK);
      HASH_RETURN(0x44A6CE3530FB9931LL, k_MEDIATYPE_MULTIMEDIA, MEDIATYPE_MULTIMEDIA);
      break;
    case 312:
      HASH_RETURN(0x23E846867CDB6F38LL, k_TS_ISO_8601, TS_ISO_8601);
      break;
    case 313:
      HASH_RETURN(0x5DF6285B5AEE2539LL, k_UNICODE_HANGUL_TEND, UNICODE_HANGUL_TEND);
      break;
    case 314:
      HASH_RETURN(0x45EBC8EBC4C81F3ALL, k_OT_PREPROCESS, OT_PREPROCESS);
      break;
    case 320:
      HASH_RETURN(0x46B77316066F2340LL, k_UNICODE_MAX, UNICODE_MAX);
      break;
    case 322:
      HASH_RETURN(0x41D32E7219C2BD42LL, k_NS_USER_TALK, NS_USER_TALK);
      break;
    case 323:
      HASH_RETURN(0x3A598B0D2FB33F43LL, g->k_MW_PREFIX, MW_PREFIX);
      break;
    case 329:
      HASH_RETURN(0x183B6E3A28B30549LL, k_UNORM_NFKC, UNORM_NFKC);
      break;
    case 331:
      HASH_RETURN(0x26BC3722C53A194BLL, k_APCOND_IPINRANGE, APCOND_IPINRANGE);
      break;
    case 332:
      HASH_RETURN(0x57A6DDE3284E934CLL, k_UTF8_HANGUL_FIRST, UTF8_HANGUL_FIRST);
      break;
    case 339:
      HASH_RETURN(0x6F46DF6101BAAD53LL, k_NS_IMAGE, NS_IMAGE);
      break;
    case 343:
      HASH_RETURN(0x342C7831D98F3557LL, k_NS_CATEGORY_TALK, NS_CATEGORY_TALK);
      break;
    case 345:
      HASH_RETURN(0x12F17465B641B759LL, g->k_MW_ATTRIBS_REGEX, MW_ATTRIBS_REGEX);
      break;
    case 348:
      HASH_RETURN(0x5E78592E40B2B95CLL, k_MW_USER_VERSION, MW_USER_VERSION);
      break;
    case 349:
      HASH_RETURN(0x5F5C5DD87205495DLL, k_UTF8_HANGUL_LEND, UTF8_HANGUL_LEND);
      break;
    case 350:
      HASH_RETURN(0x277C336F7E11135ELL, k_UTF8_HANGUL_TBASE, UTF8_HANGUL_TBASE);
      break;
    case 351:
      HASH_RETURN(0x60AD49CE9D8E995FLL, k_NS_HELP_TALK, NS_HELP_TALK);
      break;
    case 353:
      HASH_RETURN(0x7CEDC76988A5CD61LL, k_UNORM_NFKD, UNORM_NFKD);
      break;
    case 356:
      HASH_RETURN(0x350CDB938F0B2964LL, k_UTF8_HANGUL_TEND, UTF8_HANGUL_TEND);
      break;
    case 357:
      HASH_RETURN(0x0A5599F88862DD65LL, k_EDIT_AUTOSUMMARY, EDIT_AUTOSUMMARY);
      break;
    case 358:
      HASH_RETURN(0x53C29BA6FB396966LL, k_RE_IPV6_ADD, RE_IPV6_ADD);
      break;
    case 359:
      HASH_RETURN(0x1A97B0076C879367LL, k_NS_FILE, NS_FILE);
      break;
    case 362:
      HASH_RETURN(0x544C22106D33016ALL, k_APCOND_ISIP, APCOND_ISIP);
      break;
    case 363:
      HASH_RETURN(0x7032185E15490B6BLL, k_TS_ORACLE, TS_ORACLE);
      break;
    case 365:
      HASH_RETURN(0x0EC4FBFBA096936DLL, k_UTF8_HANGUL_VBASE, UTF8_HANGUL_VBASE);
      HASH_RETURN(0x338FD9F56959E76DLL, k_UTF8_REPLACEMENT, UTF8_REPLACEMENT);
      break;
    case 366:
      HASH_RETURN(0x64FA6D494C53A96ELL, k_LIST_OR, LIST_OR);
      break;
    case 368:
      HASH_RETURN(0x4C1D0C36632BDF70LL, k_OT_HTML, OT_HTML);
      break;
    case 369:
      HASH_RETURN(0x438E3C8F79ADA771LL, k_UNICODE_HANGUL_VEND, UNICODE_HANGUL_VEND);
      break;
    case 373:
      HASH_RETURN(0x2E2C33121835E575LL, k_SLH_PATTERN, SLH_PATTERN);
      break;
    case 377:
      HASH_RETURN(0x160074B355313379LL, k_RE_IPV6_GAP, RE_IPV6_GAP);
      HASH_RETURN(0x7A5B9C85D0D14179LL, k_DB_SLAVE, DB_SLAVE);
      break;
    case 387:
      HASH_RETURN(0x0A080492290E9983LL, k_MW_MATH_PNG, MW_MATH_PNG);
      HASH_RETURN(0x39454943C9F77183LL, k_SFH_NO_HASH, SFH_NO_HASH);
      break;
    case 389:
      HASH_RETURN(0x2866005D5A9D5985LL, k_TC_SELF, TC_SELF);
      HASH_RETURN(0x7C0FD65E28A86F85LL, k_DB_WRITE, DB_WRITE);
      HASH_RETURN(0x23B4DB5351182985LL, k_AV_SCAN_ABORTED, AV_SCAN_ABORTED);
      break;
    case 390:
      HASH_RETURN(0x11E62BAF1A3DA786LL, k_NS_PROJECT, NS_PROJECT);
      break;
    case 392:
      HASH_RETURN(0x0AA9352BAD8D9D88LL, k_UTF8_TAIL, UTF8_TAIL);
      break;
    case 393:
      HASH_RETURN(0x5A60AFFEAFAA8389LL, k_RC_EDIT, RC_EDIT);
      break;
    case 396:
      HASH_RETURN(0x7C7C96A77F65158CLL, k_SFH_OBJECT_ARGS, SFH_OBJECT_ARGS);
      break;
    case 398:
      HASH_RETURN(0x2333E56E2C55278ELL, k_UTF8_FDD0, UTF8_FDD0);
      break;
    case 399:
      HASH_RETURN(0x7B9F71265D42818FLL, k_DBO_NOBUFFER, DBO_NOBUFFER);
      break;
    case 405:
      HASH_RETURN(0x27781AFA09CABB95LL, k_MW_MATH_SIMPLE, MW_MATH_SIMPLE);
      break;
    case 408:
      HASH_RETURN(0x0FD72E11A4756798LL, k_UNICODE_HANGUL_LEND, UNICODE_HANGUL_LEND);
      HASH_RETURN(0x0A30BEAD88CBBD98LL, k_CACHE_DBA, CACHE_DBA);
      break;
    case 411:
      HASH_RETURN(0x461B3AC61C86EF9BLL, k_MEDIATYPE_ARCHIVE, MEDIATYPE_ARCHIVE);
      break;
    case 418:
      HASH_RETURN(0x71A531D38C8AF5A2LL, k_MW_DATE_MDY, MW_DATE_MDY);
      break;
    case 420:
      HASH_RETURN(0x62967D603A9045A4LL, k_MW_SUPPORTS_PARSERFIRSTCALLINIT, MW_SUPPORTS_PARSERFIRSTCALLINIT);
      break;
    case 425:
      HASH_RETURN(0x268F79DDF64067A9LL, k_UNICODE_REPLACEMENT, UNICODE_REPLACEMENT);
      HASH_RETURN(0x5EF576D23F8A43A9LL, k_EDIT_MINOR, EDIT_MINOR);
      break;
    case 426:
      HASH_RETURN(0x4CF2BECB79A69BAALL, k_MW_REV_DELETED_COMMENT, MW_REV_DELETED_COMMENT);
      break;
    case 430:
      HASH_RETURN(0x570AC8815B2163AELL, k_DB_LAST, DB_LAST);
      break;
    case 435:
      HASH_RETURN(0x17C7C612882071B3LL, k_UNORM_DEFAULT, UNORM_DEFAULT);
      break;
    case 440:
      HASH_RETURN(0x2D215102D9E151B8LL, k_UNICODE_HANGUL_FIRST, UNICODE_HANGUL_FIRST);
      break;
    case 442:
      HASH_RETURN(0x5519329301794FBALL, k_NS_SPECIAL, NS_SPECIAL);
      break;
    case 445:
      HASH_RETURN(0x00FE617A7A8213BDLL, k_TS_DB, TS_DB);
      break;
    case 447:
      HASH_RETURN(0x6CB43A3CF163D9BFLL, k_AV_NO_VIRUS, AV_NO_VIRUS);
      break;
    case 450:
      HASH_RETURN(0x51EBFCB49FDB87C2LL, k_DB_MASTER, DB_MASTER);
      break;
    case 452:
      HASH_RETURN(0x66BA058A2AD091C4LL, k_MEDIATYPE_TEXT, MEDIATYPE_TEXT);
      break;
    case 456:
      HASH_RETURN(0x4B23E9CBF3D567C8LL, k_RE_IP_PREFIX, RE_IP_PREFIX);
      break;
    case 461:
      HASH_RETURN(0x301D26EAB65A0DCDLL, k_MW_REV_DELETED_RESTRICTED, MW_REV_DELETED_RESTRICTED);
      break;
    case 476:
      HASH_RETURN(0x10B884F72337B1DCLL, k_MW_CHAR_REFS_REGEX, MW_CHAR_REFS_REGEX);
      HASH_RETURN(0x08B942D15A8F39DCLL, k_MEDIATYPE_VIDEO, MEDIATYPE_VIDEO);
      break;
    case 480:
      HASH_RETURN(0x6B63CAE7EF975FE0LL, k_UTF8_HANGUL_LAST, UTF8_HANGUL_LAST);
      break;
    case 481:
      HASH_RETURN(0x00A60AE76C32D1E1LL, k_MW_DATE_ISO, MW_DATE_ISO);
      break;
    case 488:
      HASH_RETURN(0x284FD6B18ECA73E8LL, k_USER_TOKEN_LENGTH, USER_TOKEN_LENGTH);
      break;
    case 495:
      HASH_RETURN(0x6EBDE8C33B7C7BEFLL, k_TS_UNIX, TS_UNIX);
      break;
    case 499:
      HASH_RETURN(0x3576495BDD81C3F3LL, k_EDIT_NEW, EDIT_NEW);
      break;
    case 511:
      HASH_RETURN(0x39F3573C91951BFFLL, k_RE_IP_ADD, RE_IP_ADD);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
