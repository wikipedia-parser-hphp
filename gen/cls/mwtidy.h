
#ifndef __GENERATED_cls_mwtidy_h__
#define __GENERATED_cls_mwtidy_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Tidy.php line 12 */
class c_mwtidy : virtual public ObjectData {
  BEGIN_CLASS_MAP(mwtidy)
  END_CLASS_MAP(mwtidy)
  DECLARE_CLASS(mwtidy, MWTidy, ObjectData)
  void init();
  public: static Variant ti_tidy(const char* cls, CVarRef v_text);
  public: static bool ti_checkerrors(const char* cls, CVarRef v_text, Variant v_errorStr = null);
  public: static Variant ti_execexternaltidy(const char* cls, CVarRef v_text, bool v_stderr = false, Variant v_retval = null);
  public: static Variant ti_execinternaltidy(const char* cls, Variant v_text, bool v_stderr = false, Variant v_retval = null);
  public: static bool t_checkerrors(CVarRef v_text, CVarRef v_errorStr = null_variant) { return ti_checkerrors("mwtidy", v_text, v_errorStr); }
  public: static Variant t_execinternaltidy(CVarRef v_text, bool v_stderr = false, CVarRef v_retval = null_variant) { return ti_execinternaltidy("mwtidy", v_text, v_stderr, v_retval); }
  public: static Variant t_execexternaltidy(CVarRef v_text, bool v_stderr = false, CVarRef v_retval = null_variant) { return ti_execexternaltidy("mwtidy", v_text, v_stderr, v_retval); }
  public: static Variant t_tidy(CVarRef v_text) { return ti_tidy("mwtidy", v_text); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_mwtidy_h__
