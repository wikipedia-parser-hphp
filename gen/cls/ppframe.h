
#ifndef __GENERATED_cls_ppframe_h__
#define __GENERATED_cls_ppframe_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor.php line 23 */
class c_ppframe : virtual public ObjectData {
  // public: void t_newchild(CVarRef v_args = false, CVarRef v_title = false) = 0;
  // public: void t_expand(CVarRef v_root, CVarRef v_flags = 0LL) = 0;
  // public: void t_implodewithflags(CVarRef v_sep, CVarRef v_flags) = 0;
  // public: void t_implode(CVarRef v_sep) = 0;
  // public: void t_virtualimplode(CVarRef v_sep) = 0;
  // public: void t_virtualbracketedimplode(CVarRef v_start, CVarRef v_sep, CVarRef v_end) = 0;
  // public: void t_isempty() = 0;
  // public: void t_getarguments() = 0;
  // public: void t_getnumberedarguments() = 0;
  // public: void t_getnamedarguments() = 0;
  // public: void t_getargument(CVarRef v_name) = 0;
  // public: void t_loopcheck(CVarRef v_title) = 0;
  // public: void t_istemplate() = 0;
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppframe_h__
