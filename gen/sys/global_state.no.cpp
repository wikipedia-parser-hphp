
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

static void print(FILE *fp, String s) {
  if (fp) {
    fwrite(s.c_str(), 1, s.size(), fp);
    return;
  }
  echo(s);
}
static void output_dynamic_constants(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_constants = json_decode('");
  Array dynamic_constants;
  String s = f_json_encode(dynamic_constants);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_static_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$static_global_variables = json_decode('");
  Array static_global_variables;
  static_global_variables.set("gv_argc", g->get("argc"));
  static_global_variables.set("gv_argv", g->get("argv"));
  static_global_variables.set("gv__SERVER", g->get("_SERVER"));
  static_global_variables.set("gv__GET", g->get("_GET"));
  static_global_variables.set("gv__POST", g->get("_POST"));
  static_global_variables.set("gv__COOKIE", g->get("_COOKIE"));
  static_global_variables.set("gv__FILES", g->get("_FILES"));
  static_global_variables.set("gv__ENV", g->get("_ENV"));
  static_global_variables.set("gv__REQUEST", g->get("_REQUEST"));
  static_global_variables.set("gv__SESSION", g->get("_SESSION"));
  static_global_variables.set("gv_HTTP_RAW_POST_DATA", g->get("HTTP_RAW_POST_DATA"));
  static_global_variables.set("gv_http_response_header", g->get("http_response_header"));
  static_global_variables.set("gv_wgServer", g->get("wgServer"));
  static_global_variables.set("gv_wgRegisterInternalExternals", g->get("wgRegisterInternalExternals"));
  static_global_variables.set("gv_wgCacheEpoch", g->get("wgCacheEpoch"));
  static_global_variables.set("gv_wgUseTeX", g->get("wgUseTeX"));
  static_global_variables.set("gv_wgUseDynamicDates", g->get("wgUseDynamicDates"));
  static_global_variables.set("gv_wgInterwikiMagic", g->get("wgInterwikiMagic"));
  static_global_variables.set("gv_wgAllowExternalImages", g->get("wgAllowExternalImages"));
  static_global_variables.set("gv_wgAllowExternalImagesFrom", g->get("wgAllowExternalImagesFrom"));
  static_global_variables.set("gv_wgEnableImageWhitelist", g->get("wgEnableImageWhitelist"));
  static_global_variables.set("gv_wgAllowSpecialInclusion", g->get("wgAllowSpecialInclusion"));
  static_global_variables.set("gv_wgMaxArticleSize", g->get("wgMaxArticleSize"));
  static_global_variables.set("gv_wgMaxPPNodeCount", g->get("wgMaxPPNodeCount"));
  static_global_variables.set("gv_wgMaxTemplateDepth", g->get("wgMaxTemplateDepth"));
  static_global_variables.set("gv_wgMaxPPExpandDepth", g->get("wgMaxPPExpandDepth"));
  static_global_variables.set("gv_wgCleanSignatures", g->get("wgCleanSignatures"));
  static_global_variables.set("gv_wgExternalLinkTarget", g->get("wgExternalLinkTarget"));
  static_global_variables.set("gv_wgUser", g->get("wgUser"));
  static_global_variables.set("gv_wgHooks", g->get("wgHooks"));
  static_global_variables.set("gv_wgMemc", g->get("wgMemc"));
  static_global_variables.set("gv_wgPreprocessorCacheThreshold", g->get("wgPreprocessorCacheThreshold"));
  static_global_variables.set("gv_wgContLang", g->get("wgContLang"));
  static_global_variables.set("gv_wgLinkHolderBatchSize", g->get("wgLinkHolderBatchSize"));
  static_global_variables.set("gv_wgUseTidy", g->get("wgUseTidy"));
  static_global_variables.set("gv_wgAlwaysUseTidy", g->get("wgAlwaysUseTidy"));
  static_global_variables.set("gv_wgExpensiveParserFunctionLimit", g->get("wgExpensiveParserFunctionLimit"));
  static_global_variables.set("gv_wgLang", g->get("wgLang"));
  static_global_variables.set("gv_wgRawHtml", g->get("wgRawHtml"));
  static_global_variables.set("gv_wgNoFollowLinks", g->get("wgNoFollowLinks"));
  static_global_variables.set("gv_wgNoFollowNsExceptions", g->get("wgNoFollowNsExceptions"));
  static_global_variables.set("gv_wgNoFollowDomainExceptions", g->get("wgNoFollowDomainExceptions"));
  static_global_variables.set("gv_wgSitename", g->get("wgSitename"));
  static_global_variables.set("gv_wgServerName", g->get("wgServerName"));
  static_global_variables.set("gv_wgScriptPath", g->get("wgScriptPath"));
  static_global_variables.set("gv_wgStylePath", g->get("wgStylePath"));
  static_global_variables.set("gv_wgLocaltimezone", g->get("wgLocaltimezone"));
  static_global_variables.set("gv_wgContLanguageCode", g->get("wgContLanguageCode"));
  static_global_variables.set("gv_wgNonincludableNamespaces", g->get("wgNonincludableNamespaces"));
  static_global_variables.set("gv_wgEnableScaryTranscluding", g->get("wgEnableScaryTranscluding"));
  static_global_variables.set("gv_wgTranscludeCacheExpiry", g->get("wgTranscludeCacheExpiry"));
  static_global_variables.set("gv_wgMaxTocLevel", g->get("wgMaxTocLevel"));
  static_global_variables.set("gv_wgEnforceHtmlIds", g->get("wgEnforceHtmlIds"));
  static_global_variables.set("gv_wgLegalTitleChars", g->get("wgLegalTitleChars"));
  static_global_variables.set("gv_wgMaxSigChars", g->get("wgMaxSigChars"));
  static_global_variables.set("gv_wgTitle", g->get("wgTitle"));
  static_global_variables.set("gv_wgCategoryPrefixedDefaultSortkey", g->get("wgCategoryPrefixedDefaultSortkey"));
  static_global_variables.set("gv_parserMemc", g->get("parserMemc"));
  static_global_variables.set("gv_wgRequest", g->get("wgRequest"));
  static_global_variables.set("gv_wgParserCacheExpireTime", g->get("wgParserCacheExpireTime"));
  static_global_variables.set("gv_wgTidyInternal", g->get("wgTidyInternal"));
  static_global_variables.set("gv_wgTidyConf", g->get("wgTidyConf"));
  static_global_variables.set("gv_wgTidyBin", g->get("wgTidyBin"));
  static_global_variables.set("gv_wgTidyOpts", g->get("wgTidyOpts"));
  static_global_variables.set("gv_IP", g->get("IP"));
  static_global_variables.set("gv_wgDebugTidy", g->get("wgDebugTidy"));
  static_global_variables.set("gv_wgAllowDisplayTitle", g->get("wgAllowDisplayTitle"));
  static_global_variables.set("gv_wgAllowSlowParserFunctions", g->get("wgAllowSlowParserFunctions"));
  static_global_variables.set("gv_wgRestrictDisplayTitle", g->get("wgRestrictDisplayTitle"));
  String s = f_json_encode(static_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_dynamic_global_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$dynamic_global_variables = json_decode('");
  Array dynamic_global_variables;
  dynamic_global_variables = *get_variable_table();
  String s = f_json_encode(dynamic_global_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_variables = json_decode('");
  Array method_static_variables;
  method_static_variables.set("sv_parser_linkhooks$$replaceinternallinks2$$tc", g->sv_parser_linkhooks$$replaceinternallinks2$$tc);
  method_static_variables.set("sv_ppframe_hash$$expand$$expansionDepth", g->sv_ppframe_hash$$expand$$expansionDepth);
  method_static_variables.set("sv_parser$$replaceinternallinks2$$tc", g->sv_parser$$replaceinternallinks2$$tc);
  method_static_variables.set("sv_parser_linkhooks$$replaceinternallinks2$$titleRegex", g->sv_parser_linkhooks$$replaceinternallinks2$$titleRegex);
  method_static_variables.set("sv_parser_difftest$$init$$doneHook", g->sv_parser_difftest$$init$$doneHook);
  method_static_variables.set("sv_coreparserfunctions$$pagesincategory$$cache", g->sv_coreparserfunctions$$pagesincategory$$cache);
  method_static_variables.set("sv_ppframe_dom$$expand$$expansionDepth", g->sv_ppframe_dom$$expand$$expansionDepth);
  method_static_variables.set("sv_dateformatter$$getinstance$$dateFormatter", g->sv_dateformatter$$getinstance$$dateFormatter);
  method_static_variables.set("sv_parser$$replaceinternallinks2$$e1", g->sv_parser$$replaceinternallinks2$$e1);
  method_static_variables.set("sv_coreparserfunctions$$israw$$mwRaw", g->sv_coreparserfunctions$$israw$$mwRaw);
  method_static_variables.set("sv_coreparserfunctions$$pagesize$$cache", g->sv_coreparserfunctions$$pagesize$$cache);
  method_static_variables.set("sv_parser$$getimageparams$$internalParamNames", g->sv_parser$$getimageparams$$internalParamNames);
  method_static_variables.set("sv_parser$$getimageparams$$internalParamMap", g->sv_parser$$getimageparams$$internalParamMap);
  method_static_variables.set("sv_parser$$extracttagsandparams$$n", g->sv_parser$$extracttagsandparams$$n);
  method_static_variables.set("sv_parser$$replaceinternallinks2$$e1_img", g->sv_parser$$replaceinternallinks2$$e1_img);
  method_static_variables.set("sv_parsercache$$singleton$$instance", g->sv_parsercache$$singleton$$instance);
  method_static_variables.set("sv_parser$$transformmsg$$executing", g->sv_parser$$transformmsg$$executing);
  String s = f_json_encode(method_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_method_static_inited(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$method_static_inited = json_decode('");
  Array method_static_inited;
  method_static_inited.set("inited_sv_parser_linkhooks$$replaceinternallinks2$$tc", g->inited_sv_parser_linkhooks$$replaceinternallinks2$$tc);
  method_static_inited.set("inited_sv_ppframe_hash$$expand$$expansionDepth", g->inited_sv_ppframe_hash$$expand$$expansionDepth);
  method_static_inited.set("inited_sv_parser$$replaceinternallinks2$$tc", g->inited_sv_parser$$replaceinternallinks2$$tc);
  method_static_inited.set("inited_sv_parser_linkhooks$$replaceinternallinks2$$titleRegex", g->inited_sv_parser_linkhooks$$replaceinternallinks2$$titleRegex);
  method_static_inited.set("inited_sv_parser_difftest$$init$$doneHook", g->inited_sv_parser_difftest$$init$$doneHook);
  method_static_inited.set("inited_sv_coreparserfunctions$$pagesincategory$$cache", g->inited_sv_coreparserfunctions$$pagesincategory$$cache);
  method_static_inited.set("inited_sv_ppframe_dom$$expand$$expansionDepth", g->inited_sv_ppframe_dom$$expand$$expansionDepth);
  method_static_inited.set("inited_sv_dateformatter$$getinstance$$dateFormatter", g->inited_sv_dateformatter$$getinstance$$dateFormatter);
  method_static_inited.set("inited_sv_parser$$replaceinternallinks2$$e1", g->inited_sv_parser$$replaceinternallinks2$$e1);
  method_static_inited.set("inited_sv_coreparserfunctions$$israw$$mwRaw", g->inited_sv_coreparserfunctions$$israw$$mwRaw);
  method_static_inited.set("inited_sv_coreparserfunctions$$pagesize$$cache", g->inited_sv_coreparserfunctions$$pagesize$$cache);
  method_static_inited.set("inited_sv_parser$$getimageparams$$internalParamNames", g->inited_sv_parser$$getimageparams$$internalParamNames);
  method_static_inited.set("inited_sv_parser$$getimageparams$$internalParamMap", g->inited_sv_parser$$getimageparams$$internalParamMap);
  method_static_inited.set("inited_sv_parser$$extracttagsandparams$$n", g->inited_sv_parser$$extracttagsandparams$$n);
  method_static_inited.set("inited_sv_parser$$replaceinternallinks2$$e1_img", g->inited_sv_parser$$replaceinternallinks2$$e1_img);
  method_static_inited.set("inited_sv_parsercache$$singleton$$instance", g->inited_sv_parsercache$$singleton$$instance);
  method_static_inited.set("inited_sv_parser$$transformmsg$$executing", g->inited_sv_parser$$transformmsg$$executing);
  String s = f_json_encode(method_static_inited);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_class_static_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$class_static_variables = json_decode('");
  Array class_static_variables;
  class_static_variables.set("s_ppdstack$$false", g->s_ppdstack$$false);
  String s = f_json_encode(class_static_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_pseudomain_variables(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$pseudomain_variables = json_decode('");
  Array pseudomain_variables;
  pseudomain_variables.set("run_pm_php$CoreLinkFunctions_php", g->run_pm_php$CoreLinkFunctions_php);
  pseudomain_variables.set("run_pm_php$CoreParserFunctions_php", g->run_pm_php$CoreParserFunctions_php);
  pseudomain_variables.set("run_pm_php$DateFormatter_php", g->run_pm_php$DateFormatter_php);
  pseudomain_variables.set("run_pm_php$LinkHolderArray_php", g->run_pm_php$LinkHolderArray_php);
  pseudomain_variables.set("run_pm_php$ParserCache_php", g->run_pm_php$ParserCache_php);
  pseudomain_variables.set("run_pm_php$Parser_DiffTest_php", g->run_pm_php$Parser_DiffTest_php);
  pseudomain_variables.set("run_pm_php$Parser_LinkHooks_php", g->run_pm_php$Parser_LinkHooks_php);
  pseudomain_variables.set("run_pm_php$ParserOptions_php", g->run_pm_php$ParserOptions_php);
  pseudomain_variables.set("run_pm_php$ParserOutput_php", g->run_pm_php$ParserOutput_php);
  pseudomain_variables.set("run_pm_php$Parser_php", g->run_pm_php$Parser_php);
  pseudomain_variables.set("run_pm_php$Preprocessor_DOM_php", g->run_pm_php$Preprocessor_DOM_php);
  pseudomain_variables.set("run_pm_php$Preprocessor_Hash_php", g->run_pm_php$Preprocessor_Hash_php);
  pseudomain_variables.set("run_pm_php$Preprocessor_php", g->run_pm_php$Preprocessor_php);
  pseudomain_variables.set("run_pm_php$Tidy_php", g->run_pm_php$Tidy_php);
  String s = f_json_encode(pseudomain_variables);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_functions(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_functions = json_decode('");
  Array redeclared_functions;
  String s = f_json_encode(redeclared_functions);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

static void output_redeclared_classes(FILE *fp) {
  DECLARE_GLOBAL_VARIABLES(g);
  print(fp, "\n$redeclared_classes = json_decode('");
  Array redeclared_classes;
  String s = f_json_encode(redeclared_classes);
  s = StringUtil::CEncode(s, "\\\'");
  print(fp, s);
  print(fp, "', true);\n");
}

void output_global_state(FILE *fp) {
  output_static_global_variables(fp);
  output_dynamic_global_variables(fp);
  output_dynamic_constants(fp);
  output_method_static_variables(fp);
  output_method_static_inited(fp);
  output_class_static_variables(fp);
  output_pseudomain_variables(fp);
  output_redeclared_functions(fp);
  output_redeclared_classes(fp);
}


///////////////////////////////////////////////////////////////////////////////
}
