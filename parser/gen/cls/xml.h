
#ifndef __GENERATED_cls_xml_h__
#define __GENERATED_cls_xml_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 15301 */
class c_xml : virtual public ObjectData {
  BEGIN_CLASS_MAP(xml)
  END_CLASS_MAP(xml)
  DECLARE_CLASS(xml, Xml, ObjectData)
  void init();
  public: static String ti_element(const char* cls, CVarRef v_element, CVarRef v_attribs = null_variant, CVarRef v_contents = "", bool v_allowShortTag = true);
  public: static Variant ti_expandattributes(const char* cls, CVarRef v_attribs);
  public: static String ti_elementclean(const char* cls, CVarRef v_element, Variant v_attribs = ScalarArrays::sa_[0], Variant v_contents = "");
  public: static String ti_openelement(const char* cls, CVarRef v_element, CVarRef v_attribs = null_variant);
  public: static String ti_closeelement(const char* cls, CStrRef v_element);
  public: static String ti_tags(const char* cls, CVarRef v_element, CVarRef v_attribs = null_variant, CVarRef v_contents = null_variant);
  public: static String ti_namespaceselector(const char* cls, Variant v_selected = "", CVarRef v_all = null_variant, CVarRef v_element_name = "namespace", CVarRef v_label = null_variant);
  public: static String ti_monthselector(const char* cls, Variant v_selected = "", CVarRef v_allmonths = null_variant, CStrRef v_id = "month");
  public: static String ti_datemenu(const char* cls, CVarRef v_year, CVarRef v_month);
  public: static Array ti_languageselector(const char* cls, Variant v_selected, bool v_customisedOnly = true);
  public: static String ti_span(const char* cls, CVarRef v_text, CVarRef v_class, CVarRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_wrapclass(const char* cls, CVarRef v_text, CVarRef v_class, CStrRef v_tag = "span", CArrRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_input(const char* cls, CVarRef v_name, CVarRef v_size = false, CVarRef v_value = false, CVarRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_password(const char* cls, CVarRef v_name, CVarRef v_size = false, CVarRef v_value = false, CVarRef v_attribs = ScalarArrays::sa_[0]);
  public: static Array ti_attrib(const char* cls, CVarRef v_name, CVarRef v_present = true);
  public: static String ti_check(const char* cls, CVarRef v_name, bool v_checked = false, CVarRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_radio(const char* cls, CVarRef v_name, CVarRef v_value, CVarRef v_checked = false, CVarRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_label(const char* cls, CVarRef v_label, CVarRef v_id);
  public: static String ti_inputlabel(const char* cls, Variant v_label, CVarRef v_name, CVarRef v_id, bool v_size = false, bool v_value = false, CArrRef v_attribs = ScalarArrays::sa_[0]);
  public: static Array ti_inputlabelsep(const char* cls, CVarRef v_label, CVarRef v_name, CVarRef v_id, bool v_size = false, bool v_value = false, CArrRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_checklabel(const char* cls, CVarRef v_label, CStrRef v_name, CStrRef v_id, bool v_checked = false, CArrRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_radiolabel(const char* cls, CVarRef v_label, CVarRef v_name, CVarRef v_value, CVarRef v_id, bool v_checked = false, CArrRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_submitbutton(const char* cls, CVarRef v_value, CArrRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_hidden(const char* cls, CVarRef v_name, CVarRef v_value, CVarRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_option(const char* cls, CVarRef v_text, CVarRef v_value = null_variant, CVarRef v_selected = false, Variant v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_listdropdown(const char* cls, CStrRef v_name = "", CVarRef v_list = "", CVarRef v_other = "", CStrRef v_selected = "", CStrRef v_class = "", CVarRef v_tabindex = null_variant);
  public: static String ti_fieldset(const char* cls, CVarRef v_legend = false, CVarRef v_content = false, CVarRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_textarea(const char* cls, CVarRef v_name, CVarRef v_content, CVarRef v_cols = 40LL, CVarRef v_rows = 5LL, CVarRef v_attribs = ScalarArrays::sa_[0]);
  public: static String ti_escapejsstring(const char* cls, CVarRef v_string);
  public: static Variant ti_encodejsvar(const char* cls, CVarRef v_value);
  public: static bool ti_iswellformed(const char* cls, CStrRef v_text);
  public: static bool ti_iswellformedxmlfragment(const char* cls, CVarRef v_text);
  public: static Variant ti_escapetagsonly(const char* cls, CVarRef v_in);
  public: static String ti_buildform(const char* cls, CArrRef v_fields, CVarRef v_submitLabel = null_variant);
  public: static String ti_buildtable(const char* cls, CArrRef v_rows, Variant v_attribs = ScalarArrays::sa_[0], CVarRef v_headers = null_variant);
  public: static String ti_buildtablerow(const char* cls, Variant v_attribs, CVarRef v_cells);
  public: static bool t_iswellformedxmlfragment(CVarRef v_text) { return ti_iswellformedxmlfragment("xml", v_text); }
  public: static Variant t_expandattributes(CVarRef v_attribs) { return ti_expandattributes("xml", v_attribs); }
  public: static String t_span(CVarRef v_text, CVarRef v_class, CVarRef v_attribs = ScalarArrays::sa_[0]) { return ti_span("xml", v_text, v_class, v_attribs); }
  public: static bool t_iswellformed(CStrRef v_text) { return ti_iswellformed("xml", v_text); }
  public: static String t_textarea(CVarRef v_name, CVarRef v_content, CVarRef v_cols = 40LL, CVarRef v_rows = 5LL, CVarRef v_attribs = ScalarArrays::sa_[0]) { return ti_textarea("xml", v_name, v_content, v_cols, v_rows, v_attribs); }
  public: static String t_password(CVarRef v_name, CVarRef v_size = false, CVarRef v_value = false, CVarRef v_attribs = ScalarArrays::sa_[0]) { return ti_password("xml", v_name, v_size, v_value, v_attribs); }
  public: static String t_option(CVarRef v_text, CVarRef v_value = null_variant, CVarRef v_selected = false, CVarRef v_attribs = ScalarArrays::sa_[0]) { return ti_option("xml", v_text, v_value, v_selected, v_attribs); }
  public: static String t_check(CVarRef v_name, bool v_checked = false, CVarRef v_attribs = ScalarArrays::sa_[0]) { return ti_check("xml", v_name, v_checked, v_attribs); }
  public: static String t_closeelement(CStrRef v_element) { return ti_closeelement("xml", v_element); }
  public: static String t_checklabel(CVarRef v_label, CStrRef v_name, CStrRef v_id, bool v_checked = false, CArrRef v_attribs = ScalarArrays::sa_[0]) { return ti_checklabel("xml", v_label, v_name, v_id, v_checked, v_attribs); }
  public: static String t_wrapclass(CVarRef v_text, CVarRef v_class, CStrRef v_tag = "span", CArrRef v_attribs = ScalarArrays::sa_[0]) { return ti_wrapclass("xml", v_text, v_class, v_tag, v_attribs); }
  public: static Variant t_encodejsvar(CVarRef v_value) { return ti_encodejsvar("xml", v_value); }
  public: static String t_radio(CVarRef v_name, CVarRef v_value, CVarRef v_checked = false, CVarRef v_attribs = ScalarArrays::sa_[0]) { return ti_radio("xml", v_name, v_value, v_checked, v_attribs); }
  public: static String t_openelement(CVarRef v_element, CVarRef v_attribs = null_variant) { return ti_openelement("xml", v_element, v_attribs); }
  public: static Array t_languageselector(CVarRef v_selected, bool v_customisedOnly = true) { return ti_languageselector("xml", v_selected, v_customisedOnly); }
  public: static String t_buildform(CArrRef v_fields, CVarRef v_submitLabel = null_variant) { return ti_buildform("xml", v_fields, v_submitLabel); }
  public: static Variant t_escapetagsonly(CVarRef v_in) { return ti_escapetagsonly("xml", v_in); }
  public: static Array t_attrib(CVarRef v_name, CVarRef v_present = true) { return ti_attrib("xml", v_name, v_present); }
  public: static String t_buildtable(CArrRef v_rows, CVarRef v_attribs = ScalarArrays::sa_[0], CVarRef v_headers = null_variant) { return ti_buildtable("xml", v_rows, v_attribs, v_headers); }
  public: static String t_input(CVarRef v_name, CVarRef v_size = false, CVarRef v_value = false, CVarRef v_attribs = ScalarArrays::sa_[0]) { return ti_input("xml", v_name, v_size, v_value, v_attribs); }
  public: static String t_label(CVarRef v_label, CVarRef v_id) { return ti_label("xml", v_label, v_id); }
  public: static String t_radiolabel(CVarRef v_label, CVarRef v_name, CVarRef v_value, CVarRef v_id, bool v_checked = false, CArrRef v_attribs = ScalarArrays::sa_[0]) { return ti_radiolabel("xml", v_label, v_name, v_value, v_id, v_checked, v_attribs); }
  public: static String t_escapejsstring(CVarRef v_string) { return ti_escapejsstring("xml", v_string); }
  public: static String t_fieldset(CVarRef v_legend = false, CVarRef v_content = false, CVarRef v_attribs = ScalarArrays::sa_[0]) { return ti_fieldset("xml", v_legend, v_content, v_attribs); }
  public: static String t_inputlabel(CVarRef v_label, CVarRef v_name, CVarRef v_id, bool v_size = false, bool v_value = false, CArrRef v_attribs = ScalarArrays::sa_[0]) { return ti_inputlabel("xml", v_label, v_name, v_id, v_size, v_value, v_attribs); }
  public: static String t_namespaceselector(CVarRef v_selected = "", CVarRef v_all = null_variant, CVarRef v_element_name = "namespace", CVarRef v_label = null_variant) { return ti_namespaceselector("xml", v_selected, v_all, v_element_name, v_label); }
  public: static String t_datemenu(CVarRef v_year, CVarRef v_month) { return ti_datemenu("xml", v_year, v_month); }
  public: static Array t_inputlabelsep(CVarRef v_label, CVarRef v_name, CVarRef v_id, bool v_size = false, bool v_value = false, CArrRef v_attribs = ScalarArrays::sa_[0]) { return ti_inputlabelsep("xml", v_label, v_name, v_id, v_size, v_value, v_attribs); }
  public: static String t_element(CVarRef v_element, CVarRef v_attribs = null_variant, CVarRef v_contents = "", bool v_allowShortTag = true) { return ti_element("xml", v_element, v_attribs, v_contents, v_allowShortTag); }
  public: static String t_elementclean(CVarRef v_element, CVarRef v_attribs = ScalarArrays::sa_[0], CVarRef v_contents = "") { return ti_elementclean("xml", v_element, v_attribs, v_contents); }
  public: static String t_monthselector(CVarRef v_selected = "", CVarRef v_allmonths = null_variant, CStrRef v_id = "month") { return ti_monthselector("xml", v_selected, v_allmonths, v_id); }
  public: static String t_listdropdown(CStrRef v_name = "", CVarRef v_list = "", CVarRef v_other = "", CStrRef v_selected = "", CStrRef v_class = "", CVarRef v_tabindex = null_variant) { return ti_listdropdown("xml", v_name, v_list, v_other, v_selected, v_class, v_tabindex); }
  public: static String t_buildtablerow(CVarRef v_attribs, CVarRef v_cells) { return ti_buildtablerow("xml", v_attribs, v_cells); }
  public: static String t_hidden(CVarRef v_name, CVarRef v_value, CVarRef v_attribs = ScalarArrays::sa_[0]) { return ti_hidden("xml", v_name, v_value, v_attribs); }
  public: static String t_submitbutton(CVarRef v_value, CArrRef v_attribs = ScalarArrays::sa_[0]) { return ti_submitbutton("xml", v_value, v_attribs); }
  public: static String t_tags(CVarRef v_element, CVarRef v_attribs = null_variant, CVarRef v_contents = null_variant) { return ti_tags("xml", v_element, v_attribs, v_contents); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_xml_h__
