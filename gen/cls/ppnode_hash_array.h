
#ifndef __GENERATED_cls_ppnode_hash_array_h__
#define __GENERATED_cls_ppnode_hash_array_h__

#include <cls/ppnode.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_Hash.php line 1555 */
class c_ppnode_hash_array : virtual public c_ppnode {
  BEGIN_CLASS_MAP(ppnode_hash_array)
    PARENT_CLASS(ppnode)
  END_CLASS_MAP(ppnode_hash_array)
  DECLARE_CLASS(ppnode_hash_array, PPNode_Hash_Array, ObjectData)
  void init();
  public: Variant m_value;
  public: Variant m_nextSibling;
  public: void t___construct(Variant v_value);
  public: ObjectData *create(Variant v_value);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t___tostring();
  public: int t_getlength();
  public: Variant t_item(CVarRef v_i);
  public: String t_getname();
  public: Variant t_getnextsibling();
  public: bool t_getchildren();
  public: bool t_getfirstchild();
  public: bool t_getchildrenoftype(CVarRef v_name);
  public: void t_splitarg();
  public: void t_splitext();
  public: void t_splitheading();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppnode_hash_array_h__
