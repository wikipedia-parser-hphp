
#ifndef __GENERATED_cls_ansitermcolorer_h__
#define __GENERATED_cls_ansitermcolorer_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 44679 */
class c_ansitermcolorer : virtual public ObjectData {
  BEGIN_CLASS_MAP(ansitermcolorer)
  END_CLASS_MAP(ansitermcolorer)
  DECLARE_CLASS(ansitermcolorer, AnsiTermColorer, ObjectData)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: String t_color(CVarRef v_color);
  public: String t_reset();
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ansitermcolorer_h__
