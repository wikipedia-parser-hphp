// BEGIN INCLUDED FILES
/* The difference between zend_symtable_X and zend_hash_X is that
 * the _symtable version will check if the key is a string of an
 * integer, and if so, use the int version instead. We can use the
 * zend_hash_X versions safely for symbol tables, since
 * variables cant be integer strings, but we cant safely use
 * zend_hash_X versions for hashtable lookups. Well-named, they are.
 */

/* An important note of zval*s and zval**s. Frequently, zval** are
 * fetched from arrays and symbol tables. The zval** will point into
 * the array, and so updating it will update the relevant array
 * entry. It is normally not the same to dereference it to a local
 * variable, and pass a reference to that.
 */



// Some common functions
#include "php.h"

/*
 * The runtime needs its own initialization and finalization.  phc is
 * responsible for generating calls to these functions.
 */

void
init_runtime ()
{
}

void
finalize_runtime ()
{
}

static void
ht_debug (HashTable * ht)
{
  printf ("\nHASH\n");
  if (ht == NULL)
    {
      printf ("NULL\n");
      return;
    }
  for (zend_hash_internal_pointer_reset (ht);
       zend_hash_has_more_elements (ht) == SUCCESS;
       zend_hash_move_forward (ht))
    {
      char *key;
      unsigned keylen;
      unsigned long idx;
      int type;
      zval **ppzval;
      zval *zvp;

      type = zend_hash_get_current_key_ex (ht, &key, &keylen, &idx, 0, NULL);
      zend_hash_get_current_data (ht, (void **) &ppzval);

      zvp = *ppzval;

      if (type == HASH_KEY_IS_STRING)
	{
	  printf ("%s", key);
	}
      else
	{
	  printf ("%ld", idx);
	}

      printf (": addr = %08lX, refcount = %d, is_ref = %d ",
	      (long unsigned int) (*ppzval), (*ppzval)->refcount,
	      (*ppzval)->is_ref);
      switch (Z_TYPE_P (zvp))
	{
	case IS_NULL:
	  printf ("(NULL)");
	  break;
	case IS_LONG:
	  printf ("(%ldL)", Z_LVAL_P (zvp));
	  break;
	case IS_DOUBLE:
	  printf ("(%lff)", Z_DVAL_P (zvp));
	  break;
	case IS_BOOL:
	  printf (Z_BVAL_P (zvp) ? "(true)" : "(false)");
	  break;
	case IS_ARRAY:
	  printf ("(array(%d))", Z_ARRVAL_P (zvp)->nNumOfElements);
	  break;
	case IS_OBJECT:
	  printf ("(Object)");
	  break;
	case IS_STRING:
	  printf ("(\"%s\")", Z_STRVAL_P (zvp));
	  break;
	case IS_RESOURCE:
	  printf ("(Resource)");
	  break;
	default:
	  printf ("(Invalid: %d)", Z_TYPE_P (zvp));
	}

      printf ("\n");

    }
  printf ("END HASH\n");
}

// Call ht_debug on the named var in the given symbol table
static void
ht_var_debug (HashTable * st, char *name)
{
  zval **p_zvp;
  if (zend_symtable_find (st, name, strlen (name) + 1,
			  (void **) &p_zvp) != SUCCESS)
    {
      printf ("VAR NOT IN SYMBOL TABLE: '%s'\n", name);
      return;
    }

  if (Z_TYPE_P (*p_zvp) != IS_ARRAY)
    {
      printf ("NOT HASH\n");
      return;
    }

  ht_debug ((*p_zvp)->value.ht);
}

static zval* counters;

static void init_counters ()
{
  ALLOC_INIT_ZVAL (counters);
  array_init (counters);
}

// Dump and cleanup memory
static void finalize_counters ()
{
  HashTable* ht = Z_ARRVAL_P (counters);
  for (zend_hash_internal_pointer_reset (ht);
       zend_hash_has_more_elements (ht) == SUCCESS;
       zend_hash_move_forward (ht))
    {
      char *key;
      zval **p_zvp;

      zend_hash_get_current_key_ex (ht, &key, NULL, NULL, 0, NULL);
      zend_hash_get_current_data (ht, (void **) &p_zvp);

      fprintf (stderr, "COUNTER:%s:%ld\n", key, Z_LVAL_P (*p_zvp));
    }

  zval_ptr_dtor (&counters);
}

static void increment_counter (char* name, int length, ulong hashval)
{
  zval** p_zvp;
  int success = zend_hash_quick_find (Z_ARRVAL_P (counters),
				      name,
				      length,
				      hashval,
				      (void **) &p_zvp);

  if (success == SUCCESS)
    {
      Z_LVAL_PP (p_zvp)++;
    }
  else
    {

      zval* new_val;
      ALLOC_INIT_ZVAL (new_val);
      ZVAL_LONG (new_val, 1);

      zend_hash_quick_add (Z_ARRVAL_P (counters),
			   name,
			   length,
			   hashval,
			   &new_val,
			   sizeof (zval *),
			   NULL);
    }
}



/* Make a copy of *P_ZVP, storing it in *P_ZVP. */
static zval *
zvp_clone_ex (zval * zvp)
{
  // TODO: use INIT_PZVAL_COPY
  zval *clone;
  MAKE_STD_ZVAL (clone);
  clone->value = zvp->value;
  clone->type = zvp->type;
  zval_copy_ctor (clone);
  return clone;
}


static inline int
in_copy_on_write (zval * zvp)
{
  return (zvp->refcount > 1 && !zvp->is_ref);
}

static inline int
in_change_on_write (zval * zvp)
{
  return (zvp->refcount > 1 && zvp->is_ref);
}

/* If *P_ZVP is in a copy-on-write set, separate it by overwriting
 * *P_ZVP with a clone of itself, and lowering the refcount on the
 * original. */
static void
sep_copy_on_write (zval ** p_zvp)
{
  if (!in_copy_on_write (*p_zvp))
    return;

  zval *old = *p_zvp;

  *p_zvp = zvp_clone_ex (*p_zvp);

  zval_ptr_dtor (&old);
}

/* If *P_ZVP is in a copy-on-write set, separate it by overwriting
 * *P_ZVP with a clone of itself, and lowering the refcount on the
 * original. */
static void
sep_change_on_write (zval ** p_zvp)
{
  assert (in_change_on_write (*p_zvp));

  zval *old = *p_zvp;

  *p_zvp = zvp_clone_ex (*p_zvp);

  zval_ptr_dtor (&old);
}

/* Assign RHS into LHS, by reference. After this, LHS will point to the same
 * zval* as RHS. */
static void
copy_into_ref (zval ** lhs, zval ** rhs)
{
  (*rhs)->is_ref = 1;
  (*rhs)->refcount++;
  zval_ptr_dtor (lhs);
  *lhs = *rhs;
}


// Overwrite one zval with another
static void
overwrite_lhs (zval * lhs, zval * rhs)
{
  // First, call the destructor to remove any data structures
  // associated with lhs that will now be overwritten
  zval_dtor (lhs);
  // Overwrite LHS
  lhs->value = rhs->value;
  lhs->type = rhs->type;
  zval_copy_ctor (lhs);
}

// Overwrite one zval with another
static void
overwrite_lhs_no_copy (zval * lhs, zval * rhs)
{
  // First, call the destructor to remove any data structures
  // associated with lhs that will now be overwritten
  zval_dtor (lhs);
  // Overwrite LHS
  lhs->value = rhs->value;
  lhs->type = rhs->type;
}

/* Write P_RHS into the symbol table as a variable named VAR_NAME. */
// NOTE: We do not alter p_rhs's refcount, unless p_lhs joins its
// Copy-on-write set.
// TODO: this is crying out to be inlined.
static void
write_var (zval ** p_lhs, zval * rhs)
{
  if (!(*p_lhs)->is_ref)
    {
      zval_ptr_dtor (p_lhs);
      // Take a copy of RHS for LHS.
      if (rhs->is_ref)
	{
	  *p_lhs = zvp_clone_ex (rhs);
	}
      else			// share a copy
	{
	  rhs->refcount++;
	  *p_lhs = rhs;
	}
    }
  else
    {
      overwrite_lhs (*p_lhs, rhs);
    }
}

// TODO: this functino does too much, and much might be redundant
static zval **
get_st_entry (HashTable * st, char *name, int length, ulong hashval TSRMLS_DC)
{
  zval **p_zvp;
  if (zend_hash_quick_find
      (st, name, length, hashval, (void **) &p_zvp) == SUCCESS)
    {
      assert (p_zvp != NULL);
      return p_zvp;
    }

  // If we dont find it, put EG (uninitialized_zval_ptr) into the
  // hashtable, and return a pointer to its container.
  EG (uninitialized_zval_ptr)->refcount++;
  int result = zend_hash_quick_add (st, name, length, hashval,
				    &EG (uninitialized_zval_ptr),
				    sizeof (zval *), (void **) &p_zvp);
  assert (result == SUCCESS);
  assert (p_zvp != NULL);

  return p_zvp;
}

/* Read the variable named VAR_NAME from the local symbol table and
 * return it. If the variable doent exist, a new one is created and
 * *IS_NEW is set.  */
static zval *
read_var (HashTable * st, char *name, int length, ulong hashval TSRMLS_DC)
{
  zval **p_zvp;
  if (zend_hash_quick_find
      (st, name, length, hashval, (void **) &p_zvp) == SUCCESS)
    return *p_zvp;

  return EG (uninitialized_zval_ptr);
}

static long
get_integer_index (zval * ind TSRMLS_DC)
{
  long index;
  switch (Z_TYPE_P (ind))
    {
    case IS_DOUBLE:
      return (long) Z_DVAL_P (ind);

    case IS_LONG:
    case IS_BOOL:
      return Z_LVAL_P (ind);

    case IS_NULL:
      return 0;

    default:
      php_error_docref (NULL TSRMLS_CC, E_WARNING, "Illegal offset type");
    }
}

static zval *
read_string_index (zval * var, zval * ind TSRMLS_DC)
{
  // This must always allocate memory, since we cant return the
  // passed string.
  assert (Z_TYPE_P (var) == IS_STRING);
  long index = get_integer_index (ind TSRMLS_CC);

  zval *result;
  ALLOC_INIT_ZVAL (result);

  if (index >= Z_STRLEN_P (var) || index < 0)
    {
      // this is 1 byte long, must be copied
      ZVAL_STRINGL (result, "", 0, 1);
    }
  else
    {
      char *string = Z_STRVAL_P (var);
      ZVAL_STRINGL (result, &string[index], 1, 1);
    }

  return result;
}

/* Given a string (p_lhs), write into it for $x[i] = $y; */
void
write_string_index (zval ** p_lhs, zval * ind, zval * rhs TSRMLS_DC)
{
  assert (Z_TYPE_P (*p_lhs) == IS_STRING);

  long index = get_integer_index (ind TSRMLS_CC);

  // Get the appropriate character
  char new_char;
  if (Z_TYPE_P (rhs) != IS_STRING)
    {
      // TODO: remove allocate
      zval *copy = zvp_clone_ex (rhs);
      convert_to_string (copy);
      new_char = Z_STRVAL_P (copy)[0];
      zval_ptr_dtor (&copy);
    }
  else
    {
      new_char = Z_STRVAL_P (rhs)[0];
    }

  // Bounds check
  if (index < 0)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Illegal string offset:  %ld", index);
      return;
    }

  // We overwrite if it's change-on-write
  sep_copy_on_write (p_lhs);

  if (index > Z_STRLEN_PP (p_lhs))
    {
      // Extend to fix new
      int len = Z_STRLEN_PP (p_lhs);
      int new_length = index + 1;	// space for the new character
      Z_STRVAL_PP (p_lhs) = erealloc (Z_STRVAL_PP (p_lhs), new_length + 1);

      // pad with ' '
      memset (&Z_STRVAL_PP (p_lhs)[len], ' ', index - len);

      // change the strlen
      Z_STRLEN_PP (p_lhs) = new_length;

      // add a null terminator
      Z_STRVAL_PP (p_lhs)[new_length] = '\0';
    }

  // write in the first character of the new value
  Z_STRVAL_PP (p_lhs)[index] = new_char;


  // index < 0: E_WARNING illegal string offset
}

// Extract the hashtable from a hash-valued zval
static HashTable *
extract_ht_ex (zval * arr TSRMLS_DC)
{
  // TODO: this likely should be inlined somewhere.
  assert (!in_copy_on_write (arr));
  if (Z_TYPE_P (arr) == IS_NULL)
    {
      array_init (arr);
    }
  else if (Z_TYPE_P (arr) != IS_ARRAY)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an array");
      array_init (arr);
    }
  return Z_ARRVAL_P (arr);
}


/* P_VAR points into a symbol table, at a variable which we wish to index as a hashtable. */
static HashTable *
extract_ht (zval ** p_var TSRMLS_DC)
{
  sep_copy_on_write (p_var);

  return extract_ht_ex (*p_var TSRMLS_CC);
}

/* Using IND as a key to HT, call the appropriate zend_index_X
 * function with data as a parameter, and return its result. This
 * updates the zval** pointed to by DATA. */
static int
ht_find (HashTable * ht, zval * ind, zval *** data)
{
  int result;
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      result = zend_hash_index_find (ht, Z_LVAL_P (ind), (void **) data);
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      result = zend_hash_index_find (ht, (long) Z_DVAL_P (ind),
				     (void **) data);
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      result = zend_hash_find (ht, "", sizeof (""), (void **)data);
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      result = zend_symtable_find (ht, Z_STRVAL_P (ind),
				   Z_STRLEN_P (ind) + 1, (void **) data);
    }
  else
    {
      // TODO: I believe this might need a warning.

      // TODO avoid alloc
      // use a string index for other types
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);

      result = zend_symtable_find (ht, Z_STRVAL_P (string_index),
				   Z_STRLEN_P (string_index) + 1,
				   (void **) data);
      zval_ptr_dtor (&string_index);
    }
  return result;
}


static int
check_array_index_type (zval * ind TSRMLS_DC)
{
  if (Z_TYPE_P (ind) == IS_OBJECT || Z_TYPE_P (ind) == IS_ARRAY)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING, "Illegal offset type");
      return 0;
    }

  return 1;
}

// Update a hashtable using a zval* index
static void
ht_update (HashTable * ht, zval * ind, zval * val, zval *** dest)
{
  int result;
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      result = zend_hash_index_update (ht, Z_LVAL_P (ind), &val,
				       sizeof (zval *), (void **) dest);
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      result = zend_hash_index_update (ht, (long) Z_DVAL_P (ind),
				       &val, sizeof (zval *), (void **) dest);
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      result = zend_hash_update (ht, "", sizeof (""), &val,
				 sizeof (zval *), (void **) dest);
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      result = zend_symtable_update (ht, Z_STRVAL_P (ind),
				     Z_STRLEN_P (ind) + 1,
				     &val, sizeof (zval *), (void **) dest);
    }
  else
    {
      // TODO avoid alloc
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);
      result = zend_symtable_update (ht, Z_STRVAL_P (string_index),
				     Z_STRLEN_P (string_index) + 1,
				     &val, sizeof (zval *), (void **) dest);

      zval_ptr_dtor (&string_index);
    }
  assert (result == SUCCESS);
}

// Delete from a hashtable using a zval* index
static void
ht_delete (HashTable * ht, zval * ind)
{
  // This may fail if the index doesnt exist, which is fine.
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      zend_hash_index_del (ht, Z_LVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      zend_hash_index_del (ht, (long) Z_DVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      zend_hash_del (ht, "", sizeof (""));
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      zend_hash_del (ht, Z_STRVAL_P (ind), Z_STRLEN_P (ind) + 1);
    }
  else
    {
      // TODO avoid alloc
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);
      zend_hash_del (ht, Z_STRVAL_P (string_index),
		     Z_STRLEN_P (string_index) + 1);

      zval_ptr_dtor (&string_index);
    }
}

// Check if a key exists in a hashtable 
static int
ht_exists (HashTable * ht, zval * ind)
{
  if (Z_TYPE_P (ind) == IS_LONG || Z_TYPE_P (ind) == IS_BOOL)
    {
      return zend_hash_index_exists (ht, Z_LVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_DOUBLE)
    {
      return zend_hash_index_exists (ht, (long) Z_DVAL_P (ind));
    }
  else if (Z_TYPE_P (ind) == IS_NULL)
    {
      return zend_hash_exists (ht, "", sizeof (""));
    }
  else if (Z_TYPE_P (ind) == IS_STRING)
    {
      return zend_hash_exists (ht, Z_STRVAL_P (ind), Z_STRLEN_P (ind) + 1);
    }
  else
    {
      // TODO avoid alloc
      int result;
      zval *string_index;
      MAKE_STD_ZVAL (string_index);
      string_index->value = ind->value;
      string_index->type = ind->type;
      zval_copy_ctor (string_index);
      convert_to_string (string_index);
      result = zend_hash_exists (ht, Z_STRVAL_P (string_index),
				 Z_STRLEN_P (string_index) + 1);
      zval_ptr_dtor (&string_index);
      return result;
    }
  assert (0);
}

static zval **
get_ht_entry (zval ** p_var, zval * ind TSRMLS_DC)
{
  if (Z_TYPE_P (*p_var) == IS_STRING)
    {
      if (Z_STRLEN_PP (p_var) > 0)
	{
	  php_error_docref (NULL TSRMLS_CC, E_ERROR,
			    "Cannot create references to/from string offsets nor overloaded objects");
	}
    }

  if (Z_TYPE_P (*p_var) != IS_ARRAY)
    {
      zval_ptr_dtor (p_var);
      ALLOC_INIT_ZVAL (*p_var);
      array_init (*p_var);
    }

  HashTable *ht = extract_ht (p_var TSRMLS_CC);

  zval **data;
  if (ht_find (ht, ind, &data) == SUCCESS)
    {
      assert (data != NULL);
      return data;
    }

  // If we dont find it, put EG (uninitialized_zval_ptr) into the
  // hashtable, and return a pointer to its container.
  EG (uninitialized_zval_ptr)->refcount++;
  ht_update (ht, ind, EG (uninitialized_zval_ptr), &data);

  assert (data != NULL);

  return data;
}


// Like extract_ht_ex, but for objects 
static HashTable *
extract_field_ex (zval * obj TSRMLS_DC)
{
  // TODO: this likely should be inlined somewhere.
  assert (!in_copy_on_write (obj));
  if (Z_TYPE_P (obj) == IS_NULL)
    {
      assert (0);
      // TODO: implement initialization
    }
  else if (Z_TYPE_P (obj) != IS_OBJECT)
    {
      // TODO: test if this is the right error message
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an object");
      // TODO: implement initialization
      assert (0);
    }
  return Z_OBJPROP_P (obj);
}

// Like extract_ht, but for objects
static HashTable *
extract_field (zval ** p_var TSRMLS_DC)
{
  sep_copy_on_write (p_var);

  return extract_field_ex (*p_var TSRMLS_CC);
}

// Like get_ht_entry, but for objects
static zval **
get_field (zval ** p_var, char *ind TSRMLS_DC)
{
  if (Z_TYPE_P (*p_var) != IS_OBJECT)
    {
      // TODO: implement initialization
      assert (0);
    }

  HashTable *ht = extract_field (p_var TSRMLS_CC);

  zval **data;
  if (zend_symtable_find (ht, ind, strlen (ind) + 1, (void **) &data) ==
      SUCCESS)
    {
      assert (data != NULL);
      return data;
    }

  // If we dont find it, put EG (uninitialized_zval_ptr) into the
  // hashtable, and return a pointer to its container.
  EG (uninitialized_zval_ptr)->refcount++;
  zend_symtable_update (ht, ind, strlen (ind) + 1,
			&EG (uninitialized_zval_ptr), sizeof (zval *),
			(void **) &data);

  assert (data != NULL);

  return data;
}

void
read_array (zval ** result, zval * array, zval * ind TSRMLS_DC)
{
  // Memory can be allocated in read_string_index
  if (array == EG (uninitialized_zval_ptr))
    {
      *result = array;
      return;
    }

  // Since we know its an array, and we dont write to it, we dont need
  // to separate it.
  HashTable *ht = Z_ARRVAL_P (array);

  // find the result
  zval **p_result;
  if (ht_find (ht, ind, &p_result) == SUCCESS)
    {
      *result = *p_result;
      return;
    }

  *result = EG (uninitialized_zval_ptr);
}

/* If its not an array, convert it into an array. */
static void
check_array_type (zval ** p_var TSRMLS_DC)
{
  if ((Z_TYPE_P (*p_var) == IS_BOOL && !Z_BVAL_PP (p_var))
      || Z_TYPE_P (*p_var) == IS_NULL
      || (Z_TYPE_P (*p_var) == IS_STRING && Z_STRLEN_PP (p_var) == 0))
    {
      // Non ref use new values
      if (!PZVAL_IS_REF (*p_var))
	{
	  zval_ptr_dtor (p_var);
	  ALLOC_INIT_ZVAL (*p_var);
	}
      else
	// Refs are just replaced
	zval_dtor (*p_var);

      array_init (*p_var);
    }
  else if (Z_TYPE_PP (p_var) != IS_STRING && Z_TYPE_PP (p_var) != IS_ARRAY)
    {
      // TODO: why are these different types than pushing
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an array");
    }
}

/* If its not an array, convert it into an object. */
static void
check_object_type (zval ** p_var TSRMLS_DC)
{
  // TODO: implement
}

/* Push EG (uninitialized_zval_ptr) and return a pointer into the ht
 * for it */
/*
 * Converted to array automatically:
 *    ""
 *    NULL
 *    false
 *
 * Warning, no conversion:
 *    ints
 *    floats
 *    true
 *
 * Error, no conversion:
 *    strings other than ""
 */
// TODO: objects, resources, etc
static zval **
push_and_index_ht (zval ** p_var TSRMLS_DC)
{
  // Check for errors conditions

  if (Z_TYPE_P (*p_var) == IS_STRING && Z_STRLEN_PP (p_var) > 0)
    {
      php_error_docref (NULL TSRMLS_CC, E_ERROR,
			"[] operator not supported for strings");
      assert (0);		// unreachable
    }

  if (Z_TYPE_P (*p_var) == IS_BOOL && Z_BVAL_PP (p_var)
      || Z_TYPE_P (*p_var) == IS_LONG || Z_TYPE_P (*p_var) == IS_DOUBLE)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Cannot use a scalar value as an array");
      return NULL;
    }

  if (Z_TYPE_P (*p_var) != IS_ARRAY)
    {
      zval_ptr_dtor (p_var);
      ALLOC_INIT_ZVAL (*p_var);
      array_init (*p_var);
    }

  // if its not an array, make it an array
  HashTable *ht = extract_ht (p_var TSRMLS_CC);
  zval **data;

  EG (uninitialized_zval_ptr)->refcount++;
  int result = zend_hash_next_index_insert (ht, &EG (uninitialized_zval_ptr),
					    sizeof (zval *), (void **) &data);
  assert (result == SUCCESS);

  assert (data);

  return data;
}


/*
 * isset
 */
static int
isset_var (HashTable * st, char *name, int length)
{
  return zend_hash_exists (st, name, length);
}

static int
isset_array (zval ** p_var, zval * ind)
{
  if (Z_TYPE_P (*p_var) == IS_STRING)
    {
      ind = zvp_clone_ex (ind);
      convert_to_long (ind);
      int result = (Z_LVAL_P (ind) >= 0
		    && Z_LVAL_P (ind) < Z_STRLEN_PP (p_var));
      assert (ind->refcount == 1);
      zval_ptr_dtor (&ind);
      return result;
    }

  // NO error required; return false
  if (Z_TYPE_P (*p_var) != IS_ARRAY)
    return 0;

  // if its not an array, make it an array
  HashTable *ht = Z_ARRVAL_P (*p_var);

  zval **data;
  if (ht_find (ht, ind, &data) == SUCCESS)
    {
      return !ZVAL_IS_NULL (*data);
    }
  else
    return 0;
}


static zval **
fetch_var_arg_by_ref (zval ** p_arg)
{
  // We are passing by reference
  sep_copy_on_write (p_arg);

  // We don't need to restore ->is_ref afterwards,
  // because the called function will reduce the
  // refcount of arg on return, and will reset is_ref to
  // 0 when refcount drops to 1.  If the refcount does
  // not drop to 1 when the function returns, but we did
  // set is_ref to 1 here, that means that is_ref must
  // already have been 1 to start with (since if it had
  // not, that means that the variable would have been
  // in a copy-on-write set, and would have been
  // seperated above).
  (*p_arg)->is_ref = 1;

  return p_arg;
}

/* Dont pass-by-ref */
static zval *
fetch_var_arg (zval * arg, int *is_arg_new)
{
  if (arg->is_ref)
    {
      // We dont separate since we don't own one of ARG's references.
      arg = zvp_clone_ex (arg);
      *is_arg_new = 1;

      // It seems we get incorrect refcounts without this.
      // TODO This decreases the refcount to zero, which seems wrong,
      // but gives the right answer. We should look at how zend does
      // this.

      arg->refcount--;
    }
  return arg;
}

// TODO dont overwrite line numbers if we're compiling an extension
static void
phc_setup_error (int init, char *filename, int line_number,
		 zend_function * function TSRMLS_DC)
{
  static int old_in_compilation;
  static int old_in_execution;
  static char *old_filename;
  static int old_lineno;
  static zend_function *old_function;
  if (init)
    {
      if (filename == NULL)
	filename = "[phc_compiled_file]";
      // Save old values
      old_in_compilation = CG (in_compilation);
      old_in_execution = EG (in_execution);
      old_filename = CG (compiled_filename);
      old_lineno = CG (zend_lineno);
      old_function = EG (function_state_ptr)->function;
      // Put in our values
      CG (in_compilation) = 1;
      EG (in_execution) = 1;
      CG (compiled_filename) = filename;
      CG (zend_lineno) = line_number;
      if (function)
	EG (function_state_ptr)->function = function;
    }
  else
    {
      CG (in_compilation) = old_in_compilation;
      EG (in_execution) = old_in_execution;
      CG (compiled_filename) = old_filename;
      CG (zend_lineno) = old_lineno;
      EG (function_state_ptr)->function = old_function;
    }
}

static void
initialize_function_call (zend_fcall_info * fci, zend_fcall_info_cache * fcic,
			  char *function_name, char *filename,
			  int line_number TSRMLS_DC)
{
  if (fcic->initialized)
    return;

  zval fn;
  INIT_PZVAL (&fn);
  ZVAL_STRING (&fn, function_name, 0);
  int result = zend_fcall_info_init (&fn, fci, fcic TSRMLS_CC);
  if (result != SUCCESS)
    {
      phc_setup_error (1, filename, line_number, NULL TSRMLS_CC);
      php_error_docref (NULL TSRMLS_CC, E_ERROR,
			"Call to undefined function %s()", function_name);
    }
}

/*
 * Initialize zend_fcall_info for a method lookup
 *
 * Implementation partly based on zend_call_method in Zend/zend_interfaces.c
 * Main difference is that we use Z_OBJ_HTT_PP(obj)->get_method to retrieve
 * the function handler for the method instead of looking it up directly;
 * this means that we correctly deal with __call.
 */

static void
initialize_method_call (zend_fcall_info * fci, zend_fcall_info_cache * fcic,
			zval ** obj, char *function_name,
			char *filename, int line_number TSRMLS_DC)
{
  if (fcic->initialized)
    return;

  zend_class_entry *obj_ce;
  obj_ce = Z_OBJCE_PP (obj);

  /*
   * we do not initialize fci.
   *   function_table  --  not initialized by zend_call_method
   *   function_name   --  zend_call_method initializes this to a pointer to
   *                       a zval 'z_fname', but does not initialize z_fname
   *                       in case of a method invocation
   *   retval_ptr_ptr  --  should be initialized by caller
   *   param_count     --  should be initialized by caller
   *   params          --  should be initialized by caller
   */
  fci->size = sizeof (*fci);
  fci->object_pp = obj;
  fci->no_separation = 1;
  fci->symbol_table = NULL;

  fcic->initialized = 1;
  fcic->calling_scope = obj_ce;
  fcic->object_pp = obj;
  fcic->function_handler
    = Z_OBJ_HT_PP (obj)->get_method (obj,
				     function_name,
				     strlen (function_name) TSRMLS_CC);

  if (fcic->function_handler == NULL)
    {
      phc_setup_error (1, filename, line_number, NULL TSRMLS_CC);
      php_error_docref (NULL TSRMLS_CC, E_ERROR,
			"Call to undefined method %s::%s",
			obj_ce->name, function_name);
    }
}

/*
 * Like initialize_method_call, but return 0 if no constructor is defined
 * rather than giving an error.
 */

static int 
initialize_constructor_call (zend_fcall_info * fci,
			     zend_fcall_info_cache * fcic, zval ** obj,
			     char *filename, int line_number TSRMLS_DC)
{
  if (fcic->initialized)
    return;

  zend_class_entry *obj_ce;
  obj_ce = Z_OBJCE_PP (obj);

  /*
   * we do not initialize fci.
   *   function_table  --  not initialized by zend_call_method
   *   function_name   --  zend_call_method initializes this to a pointer to
   *                       a zval 'z_fname', but does not initialize z_fname
   *                       in case of a method invocation
   *   retval_ptr_ptr  --  should be initialized by caller
   *   param_count     --  should be initialized by caller
   *   params          --  should be initialized by caller
   */
  fci->size = sizeof (*fci);
  fci->object_pp = obj;
  fci->no_separation = 1;
  fci->symbol_table = NULL;

  fcic->initialized = 1;
  fcic->calling_scope = obj_ce;
  fcic->object_pp = obj;
  fcic->function_handler
    = Z_OBJ_HT_PP (obj)->get_constructor (*obj TSRMLS_CC);

  return (fcic->function_handler != NULL);
}
// vi:set ts=8:

/*
 * Creates a copy of *in using persistent memory, optionally destroying *in
 *
 * Does not work for objects/resources and will loop on self-recursive arrays.
 */

zval *
persistent_clone (zval * in, int destroy_in TSRMLS_DC)
{
  zval *out = pemalloc (sizeof (zval), 1);
  *out = *in;

  switch (Z_TYPE_P (in))
    {
    case IS_NULL:
    case IS_LONG:
    case IS_DOUBLE:
    case IS_BOOL:
      /* nothing more to be done */
      break;
    case IS_STRING:
      Z_STRVAL_P (out) = pemalloc (Z_STRLEN_P (in) + 1, 1);
      memcpy (Z_STRVAL_P (out), Z_STRVAL_P (in), Z_STRLEN_P (in) + 1);
      break;
    case IS_ARRAY:
      {
	HashTable *old_arr = Z_ARRVAL_P (in);
	HashTable *new_arr = pemalloc (sizeof (HashTable), 1);
	zend_hash_init (new_arr, old_arr->nNumOfElements, NULL, ZVAL_PTR_DTOR,
			/* persistent */ 1);

	for (zend_hash_internal_pointer_reset (old_arr);
	     zend_hash_has_more_elements (old_arr) == SUCCESS;
	     zend_hash_move_forward (old_arr))
	  {
	    char *key;
	    uint keylen;
	    ulong idx;
	    int type;
	    zval **old_elem, *new_elem;

	    type =
	      zend_hash_get_current_key_ex (old_arr, &key, &keylen, &idx, 0,
					    NULL);
	    assert (zend_hash_get_current_data
		    (old_arr, (void **) &old_elem) == SUCCESS);

	    new_elem = persistent_clone (*old_elem, destroy_in TSRMLS_CC);

	    if (type == HASH_KEY_IS_STRING)
	      zend_hash_add (new_arr, key, keylen, &new_elem, sizeof (zval *),
			     NULL);
	    else
	      zend_hash_index_update (new_arr, idx, &new_elem,
				      sizeof (zval *), NULL);

	  }

	Z_ARRVAL_P (out) = new_arr;
      }
      break;
    default:
      /* other types are not supported */
      assert (0);
    }

  zval_ptr_dtor (&in);
  return out;
}

/*
 * Wrapper around zend_declare_property which 
 *
 * - Asserts that the ZEND_INTERNAL_CLASS flag is cleared
 *   (otherwise we cannot add complex (i.e., array) properties)
 * - Creates a persistent clone of the property to be added before
 *   calling zend_declare_property, since the memory for this property
 *   should only be deallocated when the module is shut down
 *   (and not when the request finishes)
 * - Cleans up after zend_declare_property by re-allocating the name of 
 *   the property using persistent memory, for much the same reason
 */

static int
phc_declare_property (zend_class_entry * ce, char *name, int name_length,
		      zval * property, int access_type TSRMLS_DC)
{
  assert (!(ce->type & ZEND_INTERNAL_CLASS));
  assert (zend_declare_property
	  (ce, name, name_length, persistent_clone (property, 1 TSRMLS_CC),
	   access_type TSRMLS_CC) == SUCCESS);

  zend_property_info *property_info;
  assert (zend_hash_find
	  (&ce->properties_info, name, name_length + 1,
	   (void **) &property_info) == SUCCESS);
  efree (property_info->name);
  property_info->name = name;

  return SUCCESS;
}

// vi:set ts=8:


static void
cast_var (zval ** p_zvp, int type)
{
  assert (type >= 0 && type <= 6);
  if ((*p_zvp)->type == type)
    return;

  sep_copy_on_write (p_zvp);
  zval *zvp = *p_zvp;

  switch (type)
    {
    case IS_NULL:
      convert_to_null (zvp);
      break;
    case IS_BOOL:
      convert_to_boolean (zvp);
      break;
    case IS_LONG:
      convert_to_long (zvp);
      break;
    case IS_DOUBLE:
      convert_to_double (zvp);
      break;
    case IS_STRING:
      convert_to_string (zvp);
      break;
    case IS_ARRAY:
      convert_to_array (zvp);
      break;
    case IS_OBJECT:
      convert_to_object (zvp);
      break;
    default:
      assert (0);		// TODO unimplemented
      break;
    }
}

/* Copies a constant into ZVP. Note that LENGTH does not include the NULL-terminating byte. */
static void
get_constant (char *name, int length, zval ** p_zvp TSRMLS_DC)
{
  MAKE_STD_ZVAL (*p_zvp);
  // zend_get_constant returns 1 for success, not SUCCESS
  int result = zend_get_constant (name, length, *p_zvp TSRMLS_CC);
  if (result == 0)
    ZVAL_STRINGL (*p_zvp, name, length, 1);
}

/* The function call mechanism deals specially with EG(uninitialize_zval_ptr)
 * (or sometime EG(uninitialize_zval)), so we need to use this too. This
 * particular zval can also be set, but there is an implicit guarantee 
 * of the information below.
 *
 * If assertions are off, this should be inlined to nothing.
 */
static void
phc_check_invariants (TSRMLS_D)
{
  assert (EG (uninitialized_zval_ptr) == &EG (uninitialized_zval));
  assert (EG (uninitialized_zval).refcount >= 1);
  assert (EG (uninitialized_zval).value.lval == 0);
  assert (EG (uninitialized_zval).type == IS_NULL);
  assert (EG (uninitialized_zval).is_ref == 0);
}


static int
check_unset_index_type (zval * ind TSRMLS_DC)
{
  if (Z_TYPE_P (ind) == IS_OBJECT || Z_TYPE_P (ind) == IS_ARRAY)
    {
      php_error_docref (NULL TSRMLS_CC, E_WARNING,
			"Illegal offset type in unset");
      return 0;
    }

  return 1;
}



/*
 * unset
 */

static void
unset_var (HashTable * st, char *name, int length)
{
  zend_hash_del (st, name, length);
}

static void
unset_array (zval ** p_var, zval * ind TSRMLS_DC)
{
  // NO error required
  if (Z_TYPE_PP (p_var) != IS_ARRAY)
    {
      if (Z_TYPE_PP (p_var) == IS_STRING)
	{
	  php_error_docref (NULL TSRMLS_CC, E_ERROR,
			    "Cannot unset string offsets");
	}
      else if (Z_TYPE_PP (p_var) != IS_NULL)
	{
	  php_error_docref (NULL TSRMLS_CC, E_WARNING,
			    "Cannot unset offsets in a non-array variable");
	}

      return;
    }

  // if its not an array, make it an array
  HashTable *ht = Z_ARRVAL_P (*p_var);

  ht_delete (ht, ind);
}

/*
 * Lookup variable whose name is var_var in st. We do not call
 * ht_find because ht_find uses zend_symtable_find to search for strings
 * rather than zend_hash_find. The difference is that zend_symtable_find
 * will convert strings to integers where possible: arrays are always
 * integer-indexed if at all possible. Variable names however should
 * _always_ be treated as strings.
 * 
 */

/*
 * If the parameter is a string, returns the parameter, with the refcount
 * incremented. If its not a string, returns a new zval, with a refcount of
 * 1. Either way, zval_dtor_ptr must be run by the caller on the return
 * value.
 */
zval*
get_string_val (zval* zvp)
{
  if (Z_TYPE_P (zvp) == IS_STRING)
    {
      zvp->refcount++;
      return zvp;
    }
  else
    {
      zval* clone = zvp_clone_ex (zvp);
      convert_to_string (clone);
      return clone;
    }
}

zval **
get_var_var (HashTable * st, zval * index TSRMLS_DC)
{
  zval* str_index = get_string_val (index);
  char* name = Z_STRVAL_P (str_index);
  int length = Z_STRLEN_P (str_index) + 1;
  unsigned long hash = zend_get_hash_value (name, length);

  zval** result = get_st_entry (st, name, length, hash TSRMLS_CC);
  zval_ptr_dtor (&str_index);
  return result;
}

/* 
 * Read the variable described by var_var from symbol table st
 * See comments for get_var_var
 */
zval *
read_var_var (HashTable * st, zval * index TSRMLS_DC)
{
  zval* str_index = get_string_val (index);
  char* name = Z_STRVAL_P (str_index);
  int length = Z_STRLEN_P (str_index) + 1;
  unsigned long hash = zend_get_hash_value (name, length);

  zval* result = read_var (st, name, length, hash TSRMLS_CC);
  zval_ptr_dtor (&str_index);
  return result;
}

static void
phc_builtin_eval (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  // If the user wrote "return ..", we need to store the
  // return value; however, in that case, zend_eval_string
  // will slap an extra "return" onto the front of the string,
  // so we must remove the "return" from the string the user
  // wrote. If the user did not write "return", he is not
  // interested in the return value, and we must pass NULL
  // instead or rhs to avoid zend_eval_string adding "return".

  // convert to a string
  // TODO avoid allocation
  zval *copy = zvp_clone_ex (arg);
  convert_to_string (copy);

  if (*p_result && !strncmp (Z_STRVAL_P (copy), "return ", 7))
    {
      zend_eval_string (Z_STRVAL_P (copy) + 7, *p_result,
			"eval'd code" TSRMLS_CC);
    }
  else
    {
      zend_eval_string (Z_STRVAL_P (copy), NULL, "eval'd code" TSRMLS_CC);
    }

  // cleanup
  assert (copy->refcount == 1);
  zval_ptr_dtor (&copy);
}

static void
phc_builtin_exit (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  if (Z_TYPE_P (arg) == IS_LONG)
    EG (exit_status) = Z_LVAL_P (arg);
  else
    zend_print_variable (arg);

  zend_bailout ();
}

static void
phc_builtin_die (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  phc_builtin_exit (arg, p_result, filename TSRMLS_CC);
}

static void
phc_builtin_echo (zval * arg, zval ** p_result TSRMLS_DC)
{
  assert (*p_result == NULL);
  zend_print_variable (arg);
}

static void
phc_builtin_print (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  zval *echo_arg = NULL;
  phc_builtin_echo (arg, &echo_arg TSRMLS_CC);

  if (*p_result)
    ZVAL_LONG (*p_result, 1);
}

// TODO is there a memory leak here is result has a value?
// TOOD isnt this just the same as isset
static void
phc_builtin_empty (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  assert (*p_result);
  ZVAL_BOOL (*p_result, !zend_is_true (arg));
}

// For require, include, require_once and include_once.

// Include:
//    return 1 for success
//    Warning, and return false for failure
// Require:
//    return 1 for success
//    Fail for failure
// Include_once
//    Return true if already included
//    Return 1 for success
//    Warning and return false for failure
// Require_once:
//    Return true if already included
//    return 1 for success
//    Fail for failure
//
static void
include_backend (zval * arg, zval ** p_result, char *filename, int type, int is_once, char* error, char* error_function TSRMLS_DC)
{
  // In the event that the Zend engine cannot find the file, after checking the
  // include path, it tries the current directory. It does this only if the
  // interpreter is executing, and it checks the interpreters opcodes for a
  // filename (see streams/plain_wrapper.c:1352)

  // An alternative is to add the directory to include_path, but its
  // semantically incorrect (get_included_path() would give the wrong answer),
  // and error prone (if people overwrite include_path).
  // TODO: though we could add it for this function only

  assert (EG (active_op_array) == NULL);
  assert (filename != NULL);

  zval *arg_file = arg;
  // Check we have a string
  if (Z_TYPE_P (arg_file) != IS_STRING)
    {
      arg_file = zvp_clone_ex (arg_file);
      convert_to_string (arg_file);
    }

  zend_file_handle handle;
  zend_op_array* new_op_array;
  zend_function zf;

  // Check the _ONCE varieties (based on zend_vm_def.h)
   if (is_once)
     {
       if (IS_ABSOLUTE_PATH (Z_STRVAL_P (arg_file), Z_STRLEN_P (arg_file)))
	 {
	   // Get the proper path name for require
	   cwd_state state;

	   state.cwd_length = 0;
	   state.cwd = malloc(1);
	   state.cwd[0] = 0;
	   int success = !virtual_file_ex(&state, Z_STRVAL_P(arg_file), NULL, 1)
	     && zend_hash_exists(&EG(included_files), state.cwd,
				 state.cwd_length+1);

	   free (state.cwd);

	   if (!success)
	     goto cleanup;
	 }
     }


   // Compile the file
   // Pretend the interpreter is running
   EG (in_execution) = 1;

   int success = zend_stream_open (Z_STRVAL_P (arg_file), &handle TSRMLS_CC);

   // Stop pretending
   EG (in_execution) = 0;
   EG (active_op_array) = NULL;

   if (success != SUCCESS)
     goto fail;


   if (is_once)
     {
       // Check it hadnt been included already
       int once_success = zend_hash_add_empty_element(&EG(included_files),
						      handle.opened_path,
						      strlen (handle.opened_path)+1);
       // Return true 
       if (once_success != SUCCESS)
	 {
	   ZVAL_BOOL (*p_result, 1);
	   goto cleanup;
	 }
     }

   if (!handle.opened_path)
     handle.opened_path = estrndup (Z_STRVAL_P(arg_file), Z_STRLEN_P (arg_file));

   // run it
   success = zend_execute_scripts (type TSRMLS_CC, p_result, 1, &handle);
   assert (success == SUCCESS);
   zend_stream_close (&handle);

   // Success
   if (*p_result)
       ZVAL_LONG (*p_result, 1);


   goto cleanup;


fail:

   php_error_docref (error_function
		     TSRMLS_CC, 
		     (type == ZEND_INCLUDE) ? E_WARNING : E_ERROR,
		     error,
		     php_strip_url_passwd (Z_STRVAL_P (arg_file)),
		     STR_PRINT (PG (include_path)));


   // Failure
   if (*p_result)
     ZVAL_BOOL (*p_result, 0);

cleanup:

   if (handle.opened_path)
     efree (handle.opened_path);
   zend_destroy_file_handle (&handle TSRMLS_CC);


  if (arg != arg_file)
    zval_ptr_dtor (&arg_file);
}

static void
phc_builtin_include (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_INCLUDE,
		    0,
		    "Failed opening '%s' for inclusion (include_path='%s')",
		    "function.include"
		    TSRMLS_CC);
}

static void
phc_builtin_require (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_REQUIRE,
		    0,
		    "Failed opening required '%s' (include_path='%s')",
		    "function.require"
		    TSRMLS_CC);
}

static void
phc_builtin_include_once (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_INCLUDE,
		    1,
		    "Failed opening '%s' for inclusion (include_path='%s')",
		    "function.include_once"
		    TSRMLS_CC);
}

static void
phc_builtin_require_once (zval * arg, zval ** p_result, char *filename TSRMLS_DC)
{
  include_backend ( arg,
		    p_result,
		    filename,
		    ZEND_REQUIRE,
		    1,
		    "Failed opening required '%s' (include_path='%s')",
		    "function.require_once"
		    TSRMLS_CC);
}

// END INCLUDED FILES
int saved_refcount;
static zend_fcall_info array_keys_fci;
static zend_fcall_info_cache array_keys_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info is_null_fci;
static zend_fcall_info_cache is_null_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info version_compare_fci;
static zend_fcall_info_cache version_compare_fcic = {0,NULL,NULL,NULL};
static zend_fcall_info wfsetvar_fci;
static zend_fcall_info_cache wfsetvar_fcic = {0,NULL,NULL,NULL};
// class ParserOutput
// {
// 	var $mText = NULL;
// 	var $mLanguageLinks = NULL;
// 	var $mCategories = NULL;
// 	var $mContainsOldMagic = NULL;
// 	var $mTitleText = NULL;
// 	var $mCacheTime = '';
// 	var $mVersion = Parser::VERSION;
// 	var $mLinks = array();
// 	var $mTemplates = array();
// 	var $mTemplateIds = array();
// 	var $mImages = array();
// 	var $mExternalLinks = array();
// 	var $mNewSection = False;
// 	var $mNoGallery = False;
// 	var $mHeadItems = array();
// 	var $mOutputHooks = array();
// 	var $mWarnings = array();
// 	var $mSections = array();
// 	var $mProperties = array();
// 	private $mIndexPolicy = '';
// 	private $displayTitle = False;
// 	function parseroutput($text = '', $languageLinks = array(), $categoryLinks = array(), $containsOldMagic = False, $titletext = '')
// 	{
// 		$this->mText = $text;
// 		$this->mLanguageLinks = $languageLinks;
// 		$this->mCategories = $categoryLinks;
// 		$this->mContainsOldMagic = $containsOldMagic;
// 		$this->mTitleText = $titletext;
// 	}
// 	function gettext()
// 	{
// 		$TSt9 = $this->mText;
// 		return $TSt9;
// 	}
// 	function &getlanguagelinks()
// 	{
// 		$TSt10 =& $this->mLanguageLinks;
// 		return $TSt10;
// 	}
// 	function getcategorylinks()
// 	{
// 		$TLE100 = param_is_ref (NULL, "array_keys", 0);
// 		;
// 		if (TLE100) goto L128 else goto L129;
// 	L128:
// 		$TMIt99 =& $this->mCategories;
// 		goto L130;
// 	L129:
// 		$TMIt99 = $this->mCategories;
// 		goto L130;
// 	L130:
// 		$TLE11 = array_keys($TMIt99);
// 		return $TLE11;
// 	}
// 	function &getcategories()
// 	{
// 		$TSt12 =& $this->mCategories;
// 		return $TSt12;
// 	}
// 	function getcachetime()
// 	{
// 		$TSt13 = $this->mCacheTime;
// 		return $TSt13;
// 	}
// 	function gettitletext()
// 	{
// 		$TSt14 = $this->mTitleText;
// 		return $TSt14;
// 	}
// 	function getsections()
// 	{
// 		$TSt15 = $this->mSections;
// 		return $TSt15;
// 	}
// 	function &getlinks()
// 	{
// 		$TSt16 =& $this->mLinks;
// 		return $TSt16;
// 	}
// 	function &gettemplates()
// 	{
// 		$TSt17 =& $this->mTemplates;
// 		return $TSt17;
// 	}
// 	function &getimages()
// 	{
// 		$TSt18 =& $this->mImages;
// 		return $TSt18;
// 	}
// 	function &getexternallinks()
// 	{
// 		$TSt19 =& $this->mExternalLinks;
// 		return $TSt19;
// 	}
// 	function getnogallery()
// 	{
// 		$TSt20 = $this->mNoGallery;
// 		return $TSt20;
// 	}
// 	function getsubtitle()
// 	{
// 		$TSt21 = $this->mSubtitle;
// 		return $TSt21;
// 	}
// 	function getoutputhooks()
// 	{
// 		$TSt22 = $this->mOutputHooks;
// 		$TLE23 = (array) $TSt22;
// 		return $TLE23;
// 	}
// 	function getwarnings()
// 	{
// 		$TLE102 = param_is_ref (NULL, "array_keys", 0);
// 		;
// 		if (TLE102) goto L131 else goto L132;
// 	L131:
// 		$TMIt101 =& $this->mWarnings;
// 		goto L133;
// 	L132:
// 		$TMIt101 = $this->mWarnings;
// 		goto L133;
// 	L133:
// 		$TLE24 = array_keys($TMIt101);
// 		return $TLE24;
// 	}
// 	function getindexpolicy()
// 	{
// 		$TSt25 = $this->mIndexPolicy;
// 		return $TSt25;
// 	}
// 	function containsoldmagic()
// 	{
// 		$TSt26 = $this->mContainsOldMagic;
// 		return $TSt26;
// 	}
// 	function settext($text)
// 	{
// 		$TLE104 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE104) goto L134 else goto L135;
// 	L134:
// 		$TMIt103 =& $this->mText;
// 		goto L136;
// 	L135:
// 		$TMIt103 = $this->mText;
// 		goto L136;
// 	L136:
// 		$TLE27 = wfsetvar($TMIt103, $text);
// 		return $TLE27;
// 	}
// 	function setlanguagelinks($ll)
// 	{
// 		$TLE106 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE106) goto L137 else goto L138;
// 	L137:
// 		$TMIt105 =& $this->mLanguageLinks;
// 		goto L139;
// 	L138:
// 		$TMIt105 = $this->mLanguageLinks;
// 		goto L139;
// 	L139:
// 		$TLE28 = wfsetvar($TMIt105, $ll);
// 		return $TLE28;
// 	}
// 	function setcategorylinks($cl)
// 	{
// 		$TLE108 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE108) goto L140 else goto L141;
// 	L140:
// 		$TMIt107 =& $this->mCategories;
// 		goto L142;
// 	L141:
// 		$TMIt107 = $this->mCategories;
// 		goto L142;
// 	L142:
// 		$TLE29 = wfsetvar($TMIt107, $cl);
// 		return $TLE29;
// 	}
// 	function setcontainsoldmagic($com)
// 	{
// 		$TLE110 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE110) goto L143 else goto L144;
// 	L143:
// 		$TMIt109 =& $this->mContainsOldMagic;
// 		goto L145;
// 	L144:
// 		$TMIt109 = $this->mContainsOldMagic;
// 		goto L145;
// 	L145:
// 		$TLE30 = wfsetvar($TMIt109, $com);
// 		return $TLE30;
// 	}
// 	function setcachetime($t)
// 	{
// 		$TLE112 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE112) goto L146 else goto L147;
// 	L146:
// 		$TMIt111 =& $this->mCacheTime;
// 		goto L148;
// 	L147:
// 		$TMIt111 = $this->mCacheTime;
// 		goto L148;
// 	L148:
// 		$TLE31 = wfsetvar($TMIt111, $t);
// 		return $TLE31;
// 	}
// 	function settitletext($t)
// 	{
// 		$TLE114 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE114) goto L149 else goto L150;
// 	L149:
// 		$TMIt113 =& $this->mTitleText;
// 		goto L151;
// 	L150:
// 		$TMIt113 = $this->mTitleText;
// 		goto L151;
// 	L151:
// 		$TLE32 = wfsetvar($TMIt113, $t);
// 		return $TLE32;
// 	}
// 	function setsections($toc)
// 	{
// 		$TLE116 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE116) goto L152 else goto L153;
// 	L152:
// 		$TMIt115 =& $this->mSections;
// 		goto L154;
// 	L153:
// 		$TMIt115 = $this->mSections;
// 		goto L154;
// 	L154:
// 		$TLE33 = wfsetvar($TMIt115, $toc);
// 		return $TLE33;
// 	}
// 	function setindexpolicy($policy)
// 	{
// 		$TLE118 = param_is_ref (NULL, "wfsetvar", 0);
// 		;
// 		if (TLE118) goto L155 else goto L156;
// 	L155:
// 		$TMIt117 =& $this->mIndexPolicy;
// 		goto L157;
// 	L156:
// 		$TMIt117 = $this->mIndexPolicy;
// 		goto L157;
// 	L157:
// 		$TLE34 = wfsetvar($TMIt117, $policy);
// 		return $TLE34;
// 	}
// 	function addcategory($c, $sort)
// 	{
// 		$TSt35 =& $this->mCategories;
// 		$TSt35[$c] = $sort;
// 	}
// 	function addlanguagelink($t)
// 	{
// 		$TSt36 =& $this->mLanguageLinks;
// 		$TSt36[] = $t;
// 	}
// 	function addexternallink($url)
// 	{
// 		$TSt37 =& $this->mExternalLinks;
// 		$TLE38 = 1;
// 		$TSt37[$url] = $TLE38;
// 	}
// 	function addwarning($s)
// 	{
// 		$TSt39 =& $this->mWarnings;
// 		$TLE40 = 1;
// 		$TSt39[$s] = $TLE40;
// 	}
// 	function addoutputhook($hook, $data = False)
// 	{
// 		$TSt41 =& $this->mOutputHooks;
// 		unset($TSa42);
// 		$TSa42 = (array) $TSa42;
// 		$TSa42[] = $hook;
// 		$TSa42[] = $data;
// 		$TSt41[] = $TSa42;
// 	}
// 	function setnewsection($value)
// 	{
// 		$TLE43 = (bool) $value;
// 		$this->mNewSection = $TLE43;
// 	}
// 	function getnewsection()
// 	{
// 		$TSt44 = $this->mNewSection;
// 		$TLE45 = (bool) $TSt44;
// 		return $TLE45;
// 	}
// 	function addlink($title, $id = NULL)
// 	{
// 		$ns = $title->getnamespace();
// 		$dbk = $title->getdbkey();
// 		$TLE46 = NS_MEDIA;
// 		$TLE47 = ($ns == $TLE46);
// 		if (TLE47) goto L164 else goto L165;
// 	L164:
// 		$ns = NS_FILE;
// 		goto L166;
// 	L165:
// 		$TLE48 = NS_SPECIAL;
// 		$TLE49 = ($ns == $TLE48);
// 		if (TLE49) goto L161 else goto L162;
// 	L161:
// 		$TLE50 = NULL;
// 		return $TLE50;
// 		goto L163;
// 	L162:
// 		$TLE51 = '';
// 		$TLE52 = ($dbk === $TLE51);
// 		if (TLE52) goto L158 else goto L159;
// 	L158:
// 		$TLE53 = NULL;
// 		return $TLE53;
// 		goto L160;
// 	L159:
// 		goto L160;
// 	L160:
// 		goto L163;
// 	L163:
// 		goto L166;
// 	L166:
// 		$TMIt119 = $this->mLinks;
// 		$TLE54 = isset($TMIt119[$ns]);
// 		$TLE55 = !$TLE54;
// 		if (TLE55) goto L167 else goto L168;
// 	L167:
// 		$TSt56 =& $this->mLinks;
// 		unset($TSa57);
// 		$TSa57 = (array) $TSa57;
// 		$TSt56[$ns] = $TSa57;
// 		goto L169;
// 	L168:
// 		goto L169;
// 	L169:
// 		$TLE58 = is_null($id);
// 		if (TLE58) goto L170 else goto L171;
// 	L170:
// 		$id = $title->getarticleid();
// 		goto L172;
// 	L171:
// 		goto L172;
// 	L172:
// 		$TSt59 =& $this->mLinks;
// 		$TSi60 =& $TSt59[$ns];
// 		$TSi60[$dbk] = $id;
// 	}
// 	function addimage($name)
// 	{
// 		$TSt61 =& $this->mImages;
// 		$TLE62 = 1;
// 		$TSt61[$name] = $TLE62;
// 	}
// 	function addtemplate($title, $page_id, $rev_id)
// 	{
// 		$ns = $title->getnamespace();
// 		$dbk = $title->getdbkey();
// 		$TMIt120 = $this->mTemplates;
// 		$TLE63 = isset($TMIt120[$ns]);
// 		$TLE64 = !$TLE63;
// 		if (TLE64) goto L173 else goto L174;
// 	L173:
// 		$TSt65 =& $this->mTemplates;
// 		unset($TSa66);
// 		$TSa66 = (array) $TSa66;
// 		$TSt65[$ns] = $TSa66;
// 		goto L175;
// 	L174:
// 		goto L175;
// 	L175:
// 		$TSt67 =& $this->mTemplates;
// 		$TSi68 =& $TSt67[$ns];
// 		$TSi68[$dbk] = $page_id;
// 		$TMIt121 = $this->mTemplateIds;
// 		$TLE69 = isset($TMIt121[$ns]);
// 		$TLE70 = !$TLE69;
// 		if (TLE70) goto L176 else goto L177;
// 	L176:
// 		$TSt71 =& $this->mTemplateIds;
// 		unset($TSa72);
// 		$TSa72 = (array) $TSa72;
// 		$TSt71[$ns] = $TSa72;
// 		goto L178;
// 	L177:
// 		goto L178;
// 	L178:
// 		$TSt73 =& $this->mTemplateIds;
// 		$TSi74 =& $TSt73[$ns];
// 		$TSi74[$dbk] = $rev_id;
// 	}
// 	function expired($touched)
// 	{
// 		global $wgCacheEpoch;
// 		$TLE75 = $this->getcachetime();
// 		$TLE76 = -1;
// 		$TLE0 = ($TLE75 == $TLE76);
// 		if (TLE0) goto L179 else goto L180;
// 	L179:
// 		$TEF1 = $TLE0;
// 		goto L181;
// 	L180:
// 		$TLE77 = $this->getcachetime();
// 		$TEF1 = ($TLE77 < $touched);
// 		goto L181;
// 	L181:
// 		$TLE2 = (bool) $TEF1;
// 		if (TLE2) goto L182 else goto L183;
// 	L182:
// 		$TEF3 = $TLE2;
// 		goto L184;
// 	L183:
// 		$TLE78 = $this->getcachetime();
// 		$TEF3 = ($TLE78 <= $wgCacheEpoch);
// 		goto L184;
// 	L184:
// 		$TLE4 = (bool) $TEF3;
// 		if (TLE4) goto L185 else goto L186;
// 	L185:
// 		$TEF5 = $TLE4;
// 		goto L187;
// 	L186:
// 		$TMIt122 = $this->mVersion;
// 		$TLE79 = isset($TMIt122);
// 		$TEF5 = !$TLE79;
// 		goto L187;
// 	L187:
// 		$TLE6 = (bool) $TEF5;
// 		if (TLE6) goto L191 else goto L192;
// 	L191:
// 		$TEF7 = $TLE6;
// 		goto L193;
// 	L192:
// 		$TLE80 = Parser::VERSION;
// 		$TLE81 = 'lt';
// 		$TLE124 = param_is_ref (NULL, "version_compare", 0);
// 		;
// 		if (TLE124) goto L188 else goto L189;
// 	L188:
// 		$TMIt123 =& $this->mVersion;
// 		goto L190;
// 	L189:
// 		$TMIt123 = $this->mVersion;
// 		goto L190;
// 	L190:
// 		$TEF7 = version_compare($TMIt123, $TLE80, $TLE81);
// 		goto L193;
// 	L193:
// 		$TLE82 = (bool) $TEF7;
// 		return $TLE82;
// 	}
// 	function addheaditem($section, $tag = False)
// 	{
// 		$TLE83 = False;
// 		$TLE84 = ($tag !== $TLE83);
// 		if (TLE84) goto L194 else goto L195;
// 	L194:
// 		$TSt85 =& $this->mHeadItems;
// 		$TSt85[$tag] = $section;
// 		goto L196;
// 	L195:
// 		$TSt86 =& $this->mHeadItems;
// 		$TSt86[] = $section;
// 		goto L196;
// 	L196:
// 	}
// 	public function setdisplaytitle($text)
// 	{
// 		$this->displayTitle = $text;
// 	}
// 	public function getdisplaytitle()
// 	{
// 		$TSt87 = $this->displayTitle;
// 		return $TSt87;
// 	}
// 	public function setflag($flag)
// 	{
// 		$TSt88 =& $this->mFlags;
// 		$TLE89 = True;
// 		$TSt88[$flag] = $TLE89;
// 	}
// 	public function getflag($flag)
// 	{
// 		$TMIt125 = $this->mFlags;
// 		$TLE90 = isset($TMIt125[$flag]);
// 		return $TLE90;
// 	}
// 	public function setproperty($name, $value)
// 	{
// 		$TSt91 =& $this->mProperties;
// 		$TSt91[$name] = $value;
// 	}
// 	public function getproperty($name)
// 	{
// 		$TMIt126 = $this->mProperties;
// 		$TLE92 = isset($TMIt126[$name]);
// 		if (TLE92) goto L197 else goto L198;
// 	L197:
// 		$TSt93 = $this->mProperties;
// 		$TSi94 = $TSt93[$name];
// 		$TEF8 = $TSi94;
// 		goto L199;
// 	L198:
// 		$TEF8 = False;
// 		goto L199;
// 	L199:
// 		return $TEF8;
// 	}
// 	public function getproperties()
// 	{
// 		$TMIt127 = $this->mProperties;
// 		$TLE95 = isset($TMIt127);
// 		$TLE96 = !$TLE95;
// 		if (TLE96) goto L200 else goto L201;
// 	L200:
// 		unset($TSa97);
// 		$TSa97 = (array) $TSa97;
// 		$this->mProperties = $TSa97;
// 		goto L202;
// 	L201:
// 		goto L202;
// 	L202:
// 		$TSt98 = $this->mProperties;
// 		return $TSt98;
// 	}
// }
// function parseroutput($text = '', $languageLinks = array(), $categoryLinks = array(), $containsOldMagic = False, $titletext = '')
// {
// 	$this->mText = $text;
// 	$this->mLanguageLinks = $languageLinks;
// 	$this->mCategories = $categoryLinks;
// 	$this->mContainsOldMagic = $containsOldMagic;
// 	$this->mTitleText = $titletext;
// }
PHP_METHOD(ParserOutput, parseroutput)
{
zval* local_categoryLinks = NULL;
zval* local_containsOldMagic = NULL;
zval* local_languageLinks = NULL;
zval* local_text = NULL;
zval* local_this = getThis();
zval* local_titletext = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[5];
zend_get_parameters_array(0, num_args, params);
// param 0
if (num_args <= 0)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = '';
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[0] = default_value;
}
params[0]->refcount++;
if (local_text != NULL)
{
	zval_ptr_dtor (&local_text);
}
local_text = params[0];
// param 1
if (num_args <= 1)
{
zval* default_value;
{
zval* local_TSa203 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa203);
{
if (local_TSa203 != NULL)
{
zval_ptr_dtor (&local_TSa203);
local_TSa203 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa203 = (array) $TSa203;
{
      if (local_TSa203 == NULL)
    {
      local_TSa203 = EG (uninitialized_zval_ptr);
      local_TSa203->refcount++;
    }
  zval** p_lhs = &local_TSa203;

    zval* rhs;
  if (local_TSa203 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa203;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa203;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa203 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa203;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa203 != NULL)
{
zval_ptr_dtor (&local_TSa203);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[1] = default_value;
}
params[1]->refcount++;
if (local_languageLinks != NULL)
{
	zval_ptr_dtor (&local_languageLinks);
}
local_languageLinks = params[1];
// param 2
if (num_args <= 2)
{
zval* default_value;
{
zval* local_TSa204 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa204);
{
if (local_TSa204 != NULL)
{
zval_ptr_dtor (&local_TSa204);
local_TSa204 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa204 = (array) $TSa204;
{
      if (local_TSa204 == NULL)
    {
      local_TSa204 = EG (uninitialized_zval_ptr);
      local_TSa204->refcount++;
    }
  zval** p_lhs = &local_TSa204;

    zval* rhs;
  if (local_TSa204 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa204;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa204;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa204 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa204;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa204 != NULL)
{
zval_ptr_dtor (&local_TSa204);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[2] = default_value;
}
params[2]->refcount++;
if (local_categoryLinks != NULL)
{
	zval_ptr_dtor (&local_categoryLinks);
}
local_categoryLinks = params[2];
// param 3
if (num_args <= 3)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = False;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[3] = default_value;
}
params[3]->refcount++;
if (local_containsOldMagic != NULL)
{
	zval_ptr_dtor (&local_containsOldMagic);
}
local_containsOldMagic = params[3];
// param 4
if (num_args <= 4)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = '';
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[4] = default_value;
}
params[4]->refcount++;
if (local_titletext != NULL)
{
	zval_ptr_dtor (&local_titletext);
}
local_titletext = params[4];
}
// Function body
// $this->mText = $text;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_text == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_text;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mText", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mLanguageLinks = $languageLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_languageLinks == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_languageLinks;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mLanguageLinks", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mCategories = $categoryLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_categoryLinks == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_categoryLinks;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCategories", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mContainsOldMagic = $containsOldMagic;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_containsOldMagic == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_containsOldMagic;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mContainsOldMagic", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// $this->mTitleText = $titletext;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_titletext == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_titletext;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTitleText", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_categoryLinks != NULL)
{
zval_ptr_dtor (&local_categoryLinks);
}
if (local_containsOldMagic != NULL)
{
zval_ptr_dtor (&local_containsOldMagic);
}
if (local_languageLinks != NULL)
{
zval_ptr_dtor (&local_languageLinks);
}
if (local_text != NULL)
{
zval_ptr_dtor (&local_text);
}
if (local_titletext != NULL)
{
zval_ptr_dtor (&local_titletext);
}
}
// function gettext()
// {
// 	$TSt9 = $this->mText;
// 	return $TSt9;
// }
PHP_METHOD(ParserOutput, gettext)
{
zval* local_TSt9 = NULL;
zval* local_this = getThis();
// Function body
// $TSt9 = $this->mText;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mText", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt9 == NULL)
    {
      local_TSt9 = EG (uninitialized_zval_ptr);
      local_TSt9->refcount++;
    }
  zval** p_lhs = &local_TSt9;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt9;
{
     zval* rhs;
  if (local_TSt9 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt9;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt9 != NULL)
{
zval_ptr_dtor (&local_TSt9);
}
}
// function &getlanguagelinks()
// {
// 	$TSt10 =& $this->mLanguageLinks;
// 	return $TSt10;
// }
PHP_METHOD(ParserOutput, getlanguagelinks)
{
zval* local_TSt10 = NULL;
zval* local_this = getThis();
// Function body
// $TSt10 =& $this->mLanguageLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mLanguageLinks", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt10 == NULL)
    {
      local_TSt10 = EG (uninitialized_zval_ptr);
      local_TSt10->refcount++;
    }
  zval** p_lhs = &local_TSt10;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// return $TSt10;
{
     if (local_TSt10 == NULL)
    {
      local_TSt10 = EG (uninitialized_zval_ptr);
      local_TSt10->refcount++;
    }
  zval** p_rhs = &local_TSt10;

   sep_copy_on_write (p_rhs);
   zval_ptr_dtor (return_value_ptr);
   (*p_rhs)->is_ref = 1;
   (*p_rhs)->refcount++;
   *return_value_ptr = *p_rhs;
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt10 != NULL)
{
zval_ptr_dtor (&local_TSt10);
}
if (*return_value_ptr)
	saved_refcount = (*return_value_ptr)->refcount;
}
// function getcategorylinks()
// {
// 	$TLE100 = param_is_ref (NULL, "array_keys", 0);
// 	;
// 	if (TLE100) goto L128 else goto L129;
// L128:
// 	$TMIt99 =& $this->mCategories;
// 	goto L130;
// L129:
// 	$TMIt99 = $this->mCategories;
// 	goto L130;
// L130:
// 	$TLE11 = array_keys($TMIt99);
// 	return $TLE11;
// }
PHP_METHOD(ParserOutput, getcategorylinks)
{
zval* local_TLE100 = NULL;
zval* local_TLE11 = NULL;
zval* local_TMIt99 = NULL;
zval* local_this = getThis();
// Function body
// $TLE100 = param_is_ref (NULL, "array_keys", 0);
// ;
{
   initialize_function_call (&array_keys_fci, &array_keys_fcic, "array_keys", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = array_keys_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE100 == NULL)
    {
      local_TLE100 = EG (uninitialized_zval_ptr);
      local_TLE100->refcount++;
    }
  zval** p_lhs = &local_TLE100;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE100) goto L128 else goto L129;
{
     zval* p_cond;
  if (local_TLE100 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE100;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L128;
   else
      goto L129;
phc_check_invariants (TSRMLS_C);
}
// L128:
L128:;
// $TMIt99 =& $this->mCategories;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCategories", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt99 == NULL)
    {
      local_TMIt99 = EG (uninitialized_zval_ptr);
      local_TMIt99->refcount++;
    }
  zval** p_lhs = &local_TMIt99;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L130;
{
goto L130;
phc_check_invariants (TSRMLS_C);
}
// L129:
L129:;
// $TMIt99 = $this->mCategories;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCategories", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt99 == NULL)
    {
      local_TMIt99 = EG (uninitialized_zval_ptr);
      local_TMIt99->refcount++;
    }
  zval** p_lhs = &local_TMIt99;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L130;
{
goto L130;
phc_check_invariants (TSRMLS_C);
}
// L130:
L130:;
// $TLE11 = array_keys($TMIt99);
{
   initialize_function_call (&array_keys_fci, &array_keys_fcic, "array_keys", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 46 TSRMLS_CC);
      zend_function* signature = array_keys_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt99 == NULL)
    {
      local_TMIt99 = EG (uninitialized_zval_ptr);
      local_TMIt99->refcount++;
    }
  zval** p_arg = &local_TMIt99;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt99 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt99;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 46, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = array_keys_fci.param_count;
   zval*** params_save = array_keys_fci.params;
   zval** retval_save = array_keys_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   array_keys_fci.params = args_ind;
   array_keys_fci.param_count = 1;
   array_keys_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&array_keys_fci, &array_keys_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   array_keys_fci.params = params_save;
   array_keys_fci.param_count = param_count_save;
   array_keys_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE11 == NULL)
    {
      local_TLE11 = EG (uninitialized_zval_ptr);
      local_TLE11->refcount++;
    }
  zval** p_lhs = &local_TLE11;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE11;
{
     zval* rhs;
  if (local_TLE11 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE11;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE100 != NULL)
{
zval_ptr_dtor (&local_TLE100);
}
if (local_TLE11 != NULL)
{
zval_ptr_dtor (&local_TLE11);
}
if (local_TMIt99 != NULL)
{
zval_ptr_dtor (&local_TMIt99);
}
}
// function &getcategories()
// {
// 	$TSt12 =& $this->mCategories;
// 	return $TSt12;
// }
PHP_METHOD(ParserOutput, getcategories)
{
zval* local_TSt12 = NULL;
zval* local_this = getThis();
// Function body
// $TSt12 =& $this->mCategories;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCategories", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt12 == NULL)
    {
      local_TSt12 = EG (uninitialized_zval_ptr);
      local_TSt12->refcount++;
    }
  zval** p_lhs = &local_TSt12;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// return $TSt12;
{
     if (local_TSt12 == NULL)
    {
      local_TSt12 = EG (uninitialized_zval_ptr);
      local_TSt12->refcount++;
    }
  zval** p_rhs = &local_TSt12;

   sep_copy_on_write (p_rhs);
   zval_ptr_dtor (return_value_ptr);
   (*p_rhs)->is_ref = 1;
   (*p_rhs)->refcount++;
   *return_value_ptr = *p_rhs;
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt12 != NULL)
{
zval_ptr_dtor (&local_TSt12);
}
if (*return_value_ptr)
	saved_refcount = (*return_value_ptr)->refcount;
}
// function getcachetime()
// {
// 	$TSt13 = $this->mCacheTime;
// 	return $TSt13;
// }
PHP_METHOD(ParserOutput, getcachetime)
{
zval* local_TSt13 = NULL;
zval* local_this = getThis();
// Function body
// $TSt13 = $this->mCacheTime;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCacheTime", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt13 == NULL)
    {
      local_TSt13 = EG (uninitialized_zval_ptr);
      local_TSt13->refcount++;
    }
  zval** p_lhs = &local_TSt13;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt13;
{
     zval* rhs;
  if (local_TSt13 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt13;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt13 != NULL)
{
zval_ptr_dtor (&local_TSt13);
}
}
// function gettitletext()
// {
// 	$TSt14 = $this->mTitleText;
// 	return $TSt14;
// }
PHP_METHOD(ParserOutput, gettitletext)
{
zval* local_TSt14 = NULL;
zval* local_this = getThis();
// Function body
// $TSt14 = $this->mTitleText;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTitleText", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt14 == NULL)
    {
      local_TSt14 = EG (uninitialized_zval_ptr);
      local_TSt14->refcount++;
    }
  zval** p_lhs = &local_TSt14;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt14;
{
     zval* rhs;
  if (local_TSt14 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt14;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt14 != NULL)
{
zval_ptr_dtor (&local_TSt14);
}
}
// function getsections()
// {
// 	$TSt15 = $this->mSections;
// 	return $TSt15;
// }
PHP_METHOD(ParserOutput, getsections)
{
zval* local_TSt15 = NULL;
zval* local_this = getThis();
// Function body
// $TSt15 = $this->mSections;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mSections", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt15 == NULL)
    {
      local_TSt15 = EG (uninitialized_zval_ptr);
      local_TSt15->refcount++;
    }
  zval** p_lhs = &local_TSt15;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt15;
{
     zval* rhs;
  if (local_TSt15 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt15;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt15 != NULL)
{
zval_ptr_dtor (&local_TSt15);
}
}
// function &getlinks()
// {
// 	$TSt16 =& $this->mLinks;
// 	return $TSt16;
// }
PHP_METHOD(ParserOutput, getlinks)
{
zval* local_TSt16 = NULL;
zval* local_this = getThis();
// Function body
// $TSt16 =& $this->mLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mLinks", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt16 == NULL)
    {
      local_TSt16 = EG (uninitialized_zval_ptr);
      local_TSt16->refcount++;
    }
  zval** p_lhs = &local_TSt16;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// return $TSt16;
{
     if (local_TSt16 == NULL)
    {
      local_TSt16 = EG (uninitialized_zval_ptr);
      local_TSt16->refcount++;
    }
  zval** p_rhs = &local_TSt16;

   sep_copy_on_write (p_rhs);
   zval_ptr_dtor (return_value_ptr);
   (*p_rhs)->is_ref = 1;
   (*p_rhs)->refcount++;
   *return_value_ptr = *p_rhs;
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt16 != NULL)
{
zval_ptr_dtor (&local_TSt16);
}
if (*return_value_ptr)
	saved_refcount = (*return_value_ptr)->refcount;
}
// function &gettemplates()
// {
// 	$TSt17 =& $this->mTemplates;
// 	return $TSt17;
// }
PHP_METHOD(ParserOutput, gettemplates)
{
zval* local_TSt17 = NULL;
zval* local_this = getThis();
// Function body
// $TSt17 =& $this->mTemplates;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplates", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt17 == NULL)
    {
      local_TSt17 = EG (uninitialized_zval_ptr);
      local_TSt17->refcount++;
    }
  zval** p_lhs = &local_TSt17;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// return $TSt17;
{
     if (local_TSt17 == NULL)
    {
      local_TSt17 = EG (uninitialized_zval_ptr);
      local_TSt17->refcount++;
    }
  zval** p_rhs = &local_TSt17;

   sep_copy_on_write (p_rhs);
   zval_ptr_dtor (return_value_ptr);
   (*p_rhs)->is_ref = 1;
   (*p_rhs)->refcount++;
   *return_value_ptr = *p_rhs;
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt17 != NULL)
{
zval_ptr_dtor (&local_TSt17);
}
if (*return_value_ptr)
	saved_refcount = (*return_value_ptr)->refcount;
}
// function &getimages()
// {
// 	$TSt18 =& $this->mImages;
// 	return $TSt18;
// }
PHP_METHOD(ParserOutput, getimages)
{
zval* local_TSt18 = NULL;
zval* local_this = getThis();
// Function body
// $TSt18 =& $this->mImages;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mImages", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt18 == NULL)
    {
      local_TSt18 = EG (uninitialized_zval_ptr);
      local_TSt18->refcount++;
    }
  zval** p_lhs = &local_TSt18;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// return $TSt18;
{
     if (local_TSt18 == NULL)
    {
      local_TSt18 = EG (uninitialized_zval_ptr);
      local_TSt18->refcount++;
    }
  zval** p_rhs = &local_TSt18;

   sep_copy_on_write (p_rhs);
   zval_ptr_dtor (return_value_ptr);
   (*p_rhs)->is_ref = 1;
   (*p_rhs)->refcount++;
   *return_value_ptr = *p_rhs;
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt18 != NULL)
{
zval_ptr_dtor (&local_TSt18);
}
if (*return_value_ptr)
	saved_refcount = (*return_value_ptr)->refcount;
}
// function &getexternallinks()
// {
// 	$TSt19 =& $this->mExternalLinks;
// 	return $TSt19;
// }
PHP_METHOD(ParserOutput, getexternallinks)
{
zval* local_TSt19 = NULL;
zval* local_this = getThis();
// Function body
// $TSt19 =& $this->mExternalLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mExternalLinks", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt19 == NULL)
    {
      local_TSt19 = EG (uninitialized_zval_ptr);
      local_TSt19->refcount++;
    }
  zval** p_lhs = &local_TSt19;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// return $TSt19;
{
     if (local_TSt19 == NULL)
    {
      local_TSt19 = EG (uninitialized_zval_ptr);
      local_TSt19->refcount++;
    }
  zval** p_rhs = &local_TSt19;

   sep_copy_on_write (p_rhs);
   zval_ptr_dtor (return_value_ptr);
   (*p_rhs)->is_ref = 1;
   (*p_rhs)->refcount++;
   *return_value_ptr = *p_rhs;
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt19 != NULL)
{
zval_ptr_dtor (&local_TSt19);
}
if (*return_value_ptr)
	saved_refcount = (*return_value_ptr)->refcount;
}
// function getnogallery()
// {
// 	$TSt20 = $this->mNoGallery;
// 	return $TSt20;
// }
PHP_METHOD(ParserOutput, getnogallery)
{
zval* local_TSt20 = NULL;
zval* local_this = getThis();
// Function body
// $TSt20 = $this->mNoGallery;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mNoGallery", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt20 == NULL)
    {
      local_TSt20 = EG (uninitialized_zval_ptr);
      local_TSt20->refcount++;
    }
  zval** p_lhs = &local_TSt20;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt20;
{
     zval* rhs;
  if (local_TSt20 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt20;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt20 != NULL)
{
zval_ptr_dtor (&local_TSt20);
}
}
// function getsubtitle()
// {
// 	$TSt21 = $this->mSubtitle;
// 	return $TSt21;
// }
PHP_METHOD(ParserOutput, getsubtitle)
{
zval* local_TSt21 = NULL;
zval* local_this = getThis();
// Function body
// $TSt21 = $this->mSubtitle;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mSubtitle", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt21 == NULL)
    {
      local_TSt21 = EG (uninitialized_zval_ptr);
      local_TSt21->refcount++;
    }
  zval** p_lhs = &local_TSt21;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt21;
{
     zval* rhs;
  if (local_TSt21 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt21;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt21 != NULL)
{
zval_ptr_dtor (&local_TSt21);
}
}
// function getoutputhooks()
// {
// 	$TSt22 = $this->mOutputHooks;
// 	$TLE23 = (array) $TSt22;
// 	return $TLE23;
// }
PHP_METHOD(ParserOutput, getoutputhooks)
{
zval* local_TLE23 = NULL;
zval* local_TSt22 = NULL;
zval* local_this = getThis();
// Function body
// $TSt22 = $this->mOutputHooks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mOutputHooks", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt22 == NULL)
    {
      local_TSt22 = EG (uninitialized_zval_ptr);
      local_TSt22->refcount++;
    }
  zval** p_lhs = &local_TSt22;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE23 = (array) $TSt22;
{
      if (local_TLE23 == NULL)
    {
      local_TLE23 = EG (uninitialized_zval_ptr);
      local_TLE23->refcount++;
    }
  zval** p_lhs = &local_TLE23;

    zval* rhs;
  if (local_TSt22 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt22;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// return $TLE23;
{
     zval* rhs;
  if (local_TLE23 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE23;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE23 != NULL)
{
zval_ptr_dtor (&local_TLE23);
}
if (local_TSt22 != NULL)
{
zval_ptr_dtor (&local_TSt22);
}
}
// function getwarnings()
// {
// 	$TLE102 = param_is_ref (NULL, "array_keys", 0);
// 	;
// 	if (TLE102) goto L131 else goto L132;
// L131:
// 	$TMIt101 =& $this->mWarnings;
// 	goto L133;
// L132:
// 	$TMIt101 = $this->mWarnings;
// 	goto L133;
// L133:
// 	$TLE24 = array_keys($TMIt101);
// 	return $TLE24;
// }
PHP_METHOD(ParserOutput, getwarnings)
{
zval* local_TLE102 = NULL;
zval* local_TLE24 = NULL;
zval* local_TMIt101 = NULL;
zval* local_this = getThis();
// Function body
// $TLE102 = param_is_ref (NULL, "array_keys", 0);
// ;
{
   initialize_function_call (&array_keys_fci, &array_keys_fcic, "array_keys", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = array_keys_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE102 == NULL)
    {
      local_TLE102 = EG (uninitialized_zval_ptr);
      local_TLE102->refcount++;
    }
  zval** p_lhs = &local_TLE102;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE102) goto L131 else goto L132;
{
     zval* p_cond;
  if (local_TLE102 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE102;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L131;
   else
      goto L132;
phc_check_invariants (TSRMLS_C);
}
// L131:
L131:;
// $TMIt101 =& $this->mWarnings;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mWarnings", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt101 == NULL)
    {
      local_TMIt101 = EG (uninitialized_zval_ptr);
      local_TMIt101->refcount++;
    }
  zval** p_lhs = &local_TMIt101;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L133;
{
goto L133;
phc_check_invariants (TSRMLS_C);
}
// L132:
L132:;
// $TMIt101 = $this->mWarnings;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mWarnings", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt101 == NULL)
    {
      local_TMIt101 = EG (uninitialized_zval_ptr);
      local_TMIt101->refcount++;
    }
  zval** p_lhs = &local_TMIt101;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L133;
{
goto L133;
phc_check_invariants (TSRMLS_C);
}
// L133:
L133:;
// $TLE24 = array_keys($TMIt101);
{
   initialize_function_call (&array_keys_fci, &array_keys_fcic, "array_keys", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 58 TSRMLS_CC);
      zend_function* signature = array_keys_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt101 == NULL)
    {
      local_TMIt101 = EG (uninitialized_zval_ptr);
      local_TMIt101->refcount++;
    }
  zval** p_arg = &local_TMIt101;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt101 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt101;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 58, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = array_keys_fci.param_count;
   zval*** params_save = array_keys_fci.params;
   zval** retval_save = array_keys_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   array_keys_fci.params = args_ind;
   array_keys_fci.param_count = 1;
   array_keys_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&array_keys_fci, &array_keys_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   array_keys_fci.params = params_save;
   array_keys_fci.param_count = param_count_save;
   array_keys_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE24 == NULL)
    {
      local_TLE24 = EG (uninitialized_zval_ptr);
      local_TLE24->refcount++;
    }
  zval** p_lhs = &local_TLE24;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE24;
{
     zval* rhs;
  if (local_TLE24 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE24;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE102 != NULL)
{
zval_ptr_dtor (&local_TLE102);
}
if (local_TLE24 != NULL)
{
zval_ptr_dtor (&local_TLE24);
}
if (local_TMIt101 != NULL)
{
zval_ptr_dtor (&local_TMIt101);
}
}
// function getindexpolicy()
// {
// 	$TSt25 = $this->mIndexPolicy;
// 	return $TSt25;
// }
PHP_METHOD(ParserOutput, getindexpolicy)
{
zval* local_TSt25 = NULL;
zval* local_this = getThis();
// Function body
// $TSt25 = $this->mIndexPolicy;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIndexPolicy", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt25 == NULL)
    {
      local_TSt25 = EG (uninitialized_zval_ptr);
      local_TSt25->refcount++;
    }
  zval** p_lhs = &local_TSt25;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt25;
{
     zval* rhs;
  if (local_TSt25 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt25;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt25 != NULL)
{
zval_ptr_dtor (&local_TSt25);
}
}
// function containsoldmagic()
// {
// 	$TSt26 = $this->mContainsOldMagic;
// 	return $TSt26;
// }
PHP_METHOD(ParserOutput, containsoldmagic)
{
zval* local_TSt26 = NULL;
zval* local_this = getThis();
// Function body
// $TSt26 = $this->mContainsOldMagic;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mContainsOldMagic", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt26 == NULL)
    {
      local_TSt26 = EG (uninitialized_zval_ptr);
      local_TSt26->refcount++;
    }
  zval** p_lhs = &local_TSt26;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt26;
{
     zval* rhs;
  if (local_TSt26 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt26;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt26 != NULL)
{
zval_ptr_dtor (&local_TSt26);
}
}
// function settext($text)
// {
// 	$TLE104 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE104) goto L134 else goto L135;
// L134:
// 	$TMIt103 =& $this->mText;
// 	goto L136;
// L135:
// 	$TMIt103 = $this->mText;
// 	goto L136;
// L136:
// 	$TLE27 = wfsetvar($TMIt103, $text);
// 	return $TLE27;
// }
PHP_METHOD(ParserOutput, settext)
{
zval* local_TLE104 = NULL;
zval* local_TLE27 = NULL;
zval* local_TMIt103 = NULL;
zval* local_text = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_text != NULL)
{
	zval_ptr_dtor (&local_text);
}
local_text = params[0];
}
// Function body
// $TLE104 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE104 == NULL)
    {
      local_TLE104 = EG (uninitialized_zval_ptr);
      local_TLE104->refcount++;
    }
  zval** p_lhs = &local_TLE104;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE104) goto L134 else goto L135;
{
     zval* p_cond;
  if (local_TLE104 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE104;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L134;
   else
      goto L135;
phc_check_invariants (TSRMLS_C);
}
// L134:
L134:;
// $TMIt103 =& $this->mText;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mText", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt103 == NULL)
    {
      local_TMIt103 = EG (uninitialized_zval_ptr);
      local_TMIt103->refcount++;
    }
  zval** p_lhs = &local_TMIt103;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L136;
{
goto L136;
phc_check_invariants (TSRMLS_C);
}
// L135:
L135:;
// $TMIt103 = $this->mText;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mText", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt103 == NULL)
    {
      local_TMIt103 = EG (uninitialized_zval_ptr);
      local_TMIt103->refcount++;
    }
  zval** p_lhs = &local_TMIt103;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L136;
{
goto L136;
phc_check_invariants (TSRMLS_C);
}
// L136:
L136:;
// $TLE27 = wfsetvar($TMIt103, $text);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 62 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt103 == NULL)
    {
      local_TMIt103 = EG (uninitialized_zval_ptr);
      local_TMIt103->refcount++;
    }
  zval** p_arg = &local_TMIt103;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt103 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt103;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_text == NULL)
    {
      local_text = EG (uninitialized_zval_ptr);
      local_text->refcount++;
    }
  zval** p_arg = &local_text;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_text == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_text;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 62, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE27 == NULL)
    {
      local_TLE27 = EG (uninitialized_zval_ptr);
      local_TLE27->refcount++;
    }
  zval** p_lhs = &local_TLE27;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE27;
{
     zval* rhs;
  if (local_TLE27 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE27;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE104 != NULL)
{
zval_ptr_dtor (&local_TLE104);
}
if (local_TLE27 != NULL)
{
zval_ptr_dtor (&local_TLE27);
}
if (local_TMIt103 != NULL)
{
zval_ptr_dtor (&local_TMIt103);
}
if (local_text != NULL)
{
zval_ptr_dtor (&local_text);
}
}
// function setlanguagelinks($ll)
// {
// 	$TLE106 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE106) goto L137 else goto L138;
// L137:
// 	$TMIt105 =& $this->mLanguageLinks;
// 	goto L139;
// L138:
// 	$TMIt105 = $this->mLanguageLinks;
// 	goto L139;
// L139:
// 	$TLE28 = wfsetvar($TMIt105, $ll);
// 	return $TLE28;
// }
PHP_METHOD(ParserOutput, setlanguagelinks)
{
zval* local_TLE106 = NULL;
zval* local_TLE28 = NULL;
zval* local_TMIt105 = NULL;
zval* local_ll = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_ll != NULL)
{
	zval_ptr_dtor (&local_ll);
}
local_ll = params[0];
}
// Function body
// $TLE106 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE106 == NULL)
    {
      local_TLE106 = EG (uninitialized_zval_ptr);
      local_TLE106->refcount++;
    }
  zval** p_lhs = &local_TLE106;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE106) goto L137 else goto L138;
{
     zval* p_cond;
  if (local_TLE106 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE106;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L137;
   else
      goto L138;
phc_check_invariants (TSRMLS_C);
}
// L137:
L137:;
// $TMIt105 =& $this->mLanguageLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mLanguageLinks", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt105 == NULL)
    {
      local_TMIt105 = EG (uninitialized_zval_ptr);
      local_TMIt105->refcount++;
    }
  zval** p_lhs = &local_TMIt105;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L139;
{
goto L139;
phc_check_invariants (TSRMLS_C);
}
// L138:
L138:;
// $TMIt105 = $this->mLanguageLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mLanguageLinks", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt105 == NULL)
    {
      local_TMIt105 = EG (uninitialized_zval_ptr);
      local_TMIt105->refcount++;
    }
  zval** p_lhs = &local_TMIt105;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L139;
{
goto L139;
phc_check_invariants (TSRMLS_C);
}
// L139:
L139:;
// $TLE28 = wfsetvar($TMIt105, $ll);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 63 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt105 == NULL)
    {
      local_TMIt105 = EG (uninitialized_zval_ptr);
      local_TMIt105->refcount++;
    }
  zval** p_arg = &local_TMIt105;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt105 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt105;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_ll == NULL)
    {
      local_ll = EG (uninitialized_zval_ptr);
      local_ll->refcount++;
    }
  zval** p_arg = &local_ll;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_ll == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_ll;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 63, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE28 == NULL)
    {
      local_TLE28 = EG (uninitialized_zval_ptr);
      local_TLE28->refcount++;
    }
  zval** p_lhs = &local_TLE28;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE28;
{
     zval* rhs;
  if (local_TLE28 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE28;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE106 != NULL)
{
zval_ptr_dtor (&local_TLE106);
}
if (local_TLE28 != NULL)
{
zval_ptr_dtor (&local_TLE28);
}
if (local_TMIt105 != NULL)
{
zval_ptr_dtor (&local_TMIt105);
}
if (local_ll != NULL)
{
zval_ptr_dtor (&local_ll);
}
}
// function setcategorylinks($cl)
// {
// 	$TLE108 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE108) goto L140 else goto L141;
// L140:
// 	$TMIt107 =& $this->mCategories;
// 	goto L142;
// L141:
// 	$TMIt107 = $this->mCategories;
// 	goto L142;
// L142:
// 	$TLE29 = wfsetvar($TMIt107, $cl);
// 	return $TLE29;
// }
PHP_METHOD(ParserOutput, setcategorylinks)
{
zval* local_TLE108 = NULL;
zval* local_TLE29 = NULL;
zval* local_TMIt107 = NULL;
zval* local_cl = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_cl != NULL)
{
	zval_ptr_dtor (&local_cl);
}
local_cl = params[0];
}
// Function body
// $TLE108 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE108 == NULL)
    {
      local_TLE108 = EG (uninitialized_zval_ptr);
      local_TLE108->refcount++;
    }
  zval** p_lhs = &local_TLE108;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE108) goto L140 else goto L141;
{
     zval* p_cond;
  if (local_TLE108 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE108;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L140;
   else
      goto L141;
phc_check_invariants (TSRMLS_C);
}
// L140:
L140:;
// $TMIt107 =& $this->mCategories;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCategories", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt107 == NULL)
    {
      local_TMIt107 = EG (uninitialized_zval_ptr);
      local_TMIt107->refcount++;
    }
  zval** p_lhs = &local_TMIt107;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L142;
{
goto L142;
phc_check_invariants (TSRMLS_C);
}
// L141:
L141:;
// $TMIt107 = $this->mCategories;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCategories", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt107 == NULL)
    {
      local_TMIt107 = EG (uninitialized_zval_ptr);
      local_TMIt107->refcount++;
    }
  zval** p_lhs = &local_TMIt107;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L142;
{
goto L142;
phc_check_invariants (TSRMLS_C);
}
// L142:
L142:;
// $TLE29 = wfsetvar($TMIt107, $cl);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 64 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt107 == NULL)
    {
      local_TMIt107 = EG (uninitialized_zval_ptr);
      local_TMIt107->refcount++;
    }
  zval** p_arg = &local_TMIt107;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt107 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt107;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_cl == NULL)
    {
      local_cl = EG (uninitialized_zval_ptr);
      local_cl->refcount++;
    }
  zval** p_arg = &local_cl;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_cl == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_cl;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 64, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE29 == NULL)
    {
      local_TLE29 = EG (uninitialized_zval_ptr);
      local_TLE29->refcount++;
    }
  zval** p_lhs = &local_TLE29;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE29;
{
     zval* rhs;
  if (local_TLE29 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE29;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE108 != NULL)
{
zval_ptr_dtor (&local_TLE108);
}
if (local_TLE29 != NULL)
{
zval_ptr_dtor (&local_TLE29);
}
if (local_TMIt107 != NULL)
{
zval_ptr_dtor (&local_TMIt107);
}
if (local_cl != NULL)
{
zval_ptr_dtor (&local_cl);
}
}
// function setcontainsoldmagic($com)
// {
// 	$TLE110 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE110) goto L143 else goto L144;
// L143:
// 	$TMIt109 =& $this->mContainsOldMagic;
// 	goto L145;
// L144:
// 	$TMIt109 = $this->mContainsOldMagic;
// 	goto L145;
// L145:
// 	$TLE30 = wfsetvar($TMIt109, $com);
// 	return $TLE30;
// }
PHP_METHOD(ParserOutput, setcontainsoldmagic)
{
zval* local_TLE110 = NULL;
zval* local_TLE30 = NULL;
zval* local_TMIt109 = NULL;
zval* local_com = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_com != NULL)
{
	zval_ptr_dtor (&local_com);
}
local_com = params[0];
}
// Function body
// $TLE110 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE110 == NULL)
    {
      local_TLE110 = EG (uninitialized_zval_ptr);
      local_TLE110->refcount++;
    }
  zval** p_lhs = &local_TLE110;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE110) goto L143 else goto L144;
{
     zval* p_cond;
  if (local_TLE110 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE110;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L143;
   else
      goto L144;
phc_check_invariants (TSRMLS_C);
}
// L143:
L143:;
// $TMIt109 =& $this->mContainsOldMagic;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mContainsOldMagic", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt109 == NULL)
    {
      local_TMIt109 = EG (uninitialized_zval_ptr);
      local_TMIt109->refcount++;
    }
  zval** p_lhs = &local_TMIt109;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L145;
{
goto L145;
phc_check_invariants (TSRMLS_C);
}
// L144:
L144:;
// $TMIt109 = $this->mContainsOldMagic;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mContainsOldMagic", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt109 == NULL)
    {
      local_TMIt109 = EG (uninitialized_zval_ptr);
      local_TMIt109->refcount++;
    }
  zval** p_lhs = &local_TMIt109;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L145;
{
goto L145;
phc_check_invariants (TSRMLS_C);
}
// L145:
L145:;
// $TLE30 = wfsetvar($TMIt109, $com);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 65 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt109 == NULL)
    {
      local_TMIt109 = EG (uninitialized_zval_ptr);
      local_TMIt109->refcount++;
    }
  zval** p_arg = &local_TMIt109;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt109 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt109;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_com == NULL)
    {
      local_com = EG (uninitialized_zval_ptr);
      local_com->refcount++;
    }
  zval** p_arg = &local_com;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_com == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_com;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 65, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE30 == NULL)
    {
      local_TLE30 = EG (uninitialized_zval_ptr);
      local_TLE30->refcount++;
    }
  zval** p_lhs = &local_TLE30;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE30;
{
     zval* rhs;
  if (local_TLE30 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE30;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE110 != NULL)
{
zval_ptr_dtor (&local_TLE110);
}
if (local_TLE30 != NULL)
{
zval_ptr_dtor (&local_TLE30);
}
if (local_TMIt109 != NULL)
{
zval_ptr_dtor (&local_TMIt109);
}
if (local_com != NULL)
{
zval_ptr_dtor (&local_com);
}
}
// function setcachetime($t)
// {
// 	$TLE112 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE112) goto L146 else goto L147;
// L146:
// 	$TMIt111 =& $this->mCacheTime;
// 	goto L148;
// L147:
// 	$TMIt111 = $this->mCacheTime;
// 	goto L148;
// L148:
// 	$TLE31 = wfsetvar($TMIt111, $t);
// 	return $TLE31;
// }
PHP_METHOD(ParserOutput, setcachetime)
{
zval* local_TLE112 = NULL;
zval* local_TLE31 = NULL;
zval* local_TMIt111 = NULL;
zval* local_t = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_t != NULL)
{
	zval_ptr_dtor (&local_t);
}
local_t = params[0];
}
// Function body
// $TLE112 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE112 == NULL)
    {
      local_TLE112 = EG (uninitialized_zval_ptr);
      local_TLE112->refcount++;
    }
  zval** p_lhs = &local_TLE112;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE112) goto L146 else goto L147;
{
     zval* p_cond;
  if (local_TLE112 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE112;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L146;
   else
      goto L147;
phc_check_invariants (TSRMLS_C);
}
// L146:
L146:;
// $TMIt111 =& $this->mCacheTime;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCacheTime", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt111 == NULL)
    {
      local_TMIt111 = EG (uninitialized_zval_ptr);
      local_TMIt111->refcount++;
    }
  zval** p_lhs = &local_TMIt111;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L148;
{
goto L148;
phc_check_invariants (TSRMLS_C);
}
// L147:
L147:;
// $TMIt111 = $this->mCacheTime;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCacheTime", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt111 == NULL)
    {
      local_TMIt111 = EG (uninitialized_zval_ptr);
      local_TMIt111->refcount++;
    }
  zval** p_lhs = &local_TMIt111;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L148;
{
goto L148;
phc_check_invariants (TSRMLS_C);
}
// L148:
L148:;
// $TLE31 = wfsetvar($TMIt111, $t);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 66 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt111 == NULL)
    {
      local_TMIt111 = EG (uninitialized_zval_ptr);
      local_TMIt111->refcount++;
    }
  zval** p_arg = &local_TMIt111;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt111 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt111;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_t == NULL)
    {
      local_t = EG (uninitialized_zval_ptr);
      local_t->refcount++;
    }
  zval** p_arg = &local_t;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_t == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_t;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 66, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE31 == NULL)
    {
      local_TLE31 = EG (uninitialized_zval_ptr);
      local_TLE31->refcount++;
    }
  zval** p_lhs = &local_TLE31;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE31;
{
     zval* rhs;
  if (local_TLE31 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE31;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE112 != NULL)
{
zval_ptr_dtor (&local_TLE112);
}
if (local_TLE31 != NULL)
{
zval_ptr_dtor (&local_TLE31);
}
if (local_TMIt111 != NULL)
{
zval_ptr_dtor (&local_TMIt111);
}
if (local_t != NULL)
{
zval_ptr_dtor (&local_t);
}
}
// function settitletext($t)
// {
// 	$TLE114 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE114) goto L149 else goto L150;
// L149:
// 	$TMIt113 =& $this->mTitleText;
// 	goto L151;
// L150:
// 	$TMIt113 = $this->mTitleText;
// 	goto L151;
// L151:
// 	$TLE32 = wfsetvar($TMIt113, $t);
// 	return $TLE32;
// }
PHP_METHOD(ParserOutput, settitletext)
{
zval* local_TLE114 = NULL;
zval* local_TLE32 = NULL;
zval* local_TMIt113 = NULL;
zval* local_t = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_t != NULL)
{
	zval_ptr_dtor (&local_t);
}
local_t = params[0];
}
// Function body
// $TLE114 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE114 == NULL)
    {
      local_TLE114 = EG (uninitialized_zval_ptr);
      local_TLE114->refcount++;
    }
  zval** p_lhs = &local_TLE114;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE114) goto L149 else goto L150;
{
     zval* p_cond;
  if (local_TLE114 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE114;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L149;
   else
      goto L150;
phc_check_invariants (TSRMLS_C);
}
// L149:
L149:;
// $TMIt113 =& $this->mTitleText;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTitleText", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt113 == NULL)
    {
      local_TMIt113 = EG (uninitialized_zval_ptr);
      local_TMIt113->refcount++;
    }
  zval** p_lhs = &local_TMIt113;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L151;
{
goto L151;
phc_check_invariants (TSRMLS_C);
}
// L150:
L150:;
// $TMIt113 = $this->mTitleText;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTitleText", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt113 == NULL)
    {
      local_TMIt113 = EG (uninitialized_zval_ptr);
      local_TMIt113->refcount++;
    }
  zval** p_lhs = &local_TMIt113;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L151;
{
goto L151;
phc_check_invariants (TSRMLS_C);
}
// L151:
L151:;
// $TLE32 = wfsetvar($TMIt113, $t);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 67 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt113 == NULL)
    {
      local_TMIt113 = EG (uninitialized_zval_ptr);
      local_TMIt113->refcount++;
    }
  zval** p_arg = &local_TMIt113;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt113 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt113;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_t == NULL)
    {
      local_t = EG (uninitialized_zval_ptr);
      local_t->refcount++;
    }
  zval** p_arg = &local_t;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_t == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_t;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 67, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE32 == NULL)
    {
      local_TLE32 = EG (uninitialized_zval_ptr);
      local_TLE32->refcount++;
    }
  zval** p_lhs = &local_TLE32;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE32;
{
     zval* rhs;
  if (local_TLE32 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE32;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE114 != NULL)
{
zval_ptr_dtor (&local_TLE114);
}
if (local_TLE32 != NULL)
{
zval_ptr_dtor (&local_TLE32);
}
if (local_TMIt113 != NULL)
{
zval_ptr_dtor (&local_TMIt113);
}
if (local_t != NULL)
{
zval_ptr_dtor (&local_t);
}
}
// function setsections($toc)
// {
// 	$TLE116 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE116) goto L152 else goto L153;
// L152:
// 	$TMIt115 =& $this->mSections;
// 	goto L154;
// L153:
// 	$TMIt115 = $this->mSections;
// 	goto L154;
// L154:
// 	$TLE33 = wfsetvar($TMIt115, $toc);
// 	return $TLE33;
// }
PHP_METHOD(ParserOutput, setsections)
{
zval* local_TLE116 = NULL;
zval* local_TLE33 = NULL;
zval* local_TMIt115 = NULL;
zval* local_this = getThis();
zval* local_toc = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_toc != NULL)
{
	zval_ptr_dtor (&local_toc);
}
local_toc = params[0];
}
// Function body
// $TLE116 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE116 == NULL)
    {
      local_TLE116 = EG (uninitialized_zval_ptr);
      local_TLE116->refcount++;
    }
  zval** p_lhs = &local_TLE116;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE116) goto L152 else goto L153;
{
     zval* p_cond;
  if (local_TLE116 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE116;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L152;
   else
      goto L153;
phc_check_invariants (TSRMLS_C);
}
// L152:
L152:;
// $TMIt115 =& $this->mSections;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mSections", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt115 == NULL)
    {
      local_TMIt115 = EG (uninitialized_zval_ptr);
      local_TMIt115->refcount++;
    }
  zval** p_lhs = &local_TMIt115;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L154;
{
goto L154;
phc_check_invariants (TSRMLS_C);
}
// L153:
L153:;
// $TMIt115 = $this->mSections;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mSections", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt115 == NULL)
    {
      local_TMIt115 = EG (uninitialized_zval_ptr);
      local_TMIt115->refcount++;
    }
  zval** p_lhs = &local_TMIt115;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L154;
{
goto L154;
phc_check_invariants (TSRMLS_C);
}
// L154:
L154:;
// $TLE33 = wfsetvar($TMIt115, $toc);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 68 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt115 == NULL)
    {
      local_TMIt115 = EG (uninitialized_zval_ptr);
      local_TMIt115->refcount++;
    }
  zval** p_arg = &local_TMIt115;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt115 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt115;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_toc == NULL)
    {
      local_toc = EG (uninitialized_zval_ptr);
      local_toc->refcount++;
    }
  zval** p_arg = &local_toc;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_toc == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_toc;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 68, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE33 == NULL)
    {
      local_TLE33 = EG (uninitialized_zval_ptr);
      local_TLE33->refcount++;
    }
  zval** p_lhs = &local_TLE33;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE33;
{
     zval* rhs;
  if (local_TLE33 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE33;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE116 != NULL)
{
zval_ptr_dtor (&local_TLE116);
}
if (local_TLE33 != NULL)
{
zval_ptr_dtor (&local_TLE33);
}
if (local_TMIt115 != NULL)
{
zval_ptr_dtor (&local_TMIt115);
}
if (local_toc != NULL)
{
zval_ptr_dtor (&local_toc);
}
}
// function setindexpolicy($policy)
// {
// 	$TLE118 = param_is_ref (NULL, "wfsetvar", 0);
// 	;
// 	if (TLE118) goto L155 else goto L156;
// L155:
// 	$TMIt117 =& $this->mIndexPolicy;
// 	goto L157;
// L156:
// 	$TMIt117 = $this->mIndexPolicy;
// 	goto L157;
// L157:
// 	$TLE34 = wfsetvar($TMIt117, $policy);
// 	return $TLE34;
// }
PHP_METHOD(ParserOutput, setindexpolicy)
{
zval* local_TLE118 = NULL;
zval* local_TLE34 = NULL;
zval* local_TMIt117 = NULL;
zval* local_policy = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_policy != NULL)
{
	zval_ptr_dtor (&local_policy);
}
local_policy = params[0];
}
// Function body
// $TLE118 = param_is_ref (NULL, "wfsetvar", 0);
// ;
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = wfsetvar_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE118 == NULL)
    {
      local_TLE118 = EG (uninitialized_zval_ptr);
      local_TLE118->refcount++;
    }
  zval** p_lhs = &local_TLE118;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE118) goto L155 else goto L156;
{
     zval* p_cond;
  if (local_TLE118 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE118;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L155;
   else
      goto L156;
phc_check_invariants (TSRMLS_C);
}
// L155:
L155:;
// $TMIt117 =& $this->mIndexPolicy;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIndexPolicy", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt117 == NULL)
    {
      local_TMIt117 = EG (uninitialized_zval_ptr);
      local_TMIt117->refcount++;
    }
  zval** p_lhs = &local_TMIt117;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L157;
{
goto L157;
phc_check_invariants (TSRMLS_C);
}
// L156:
L156:;
// $TMIt117 = $this->mIndexPolicy;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mIndexPolicy", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt117 == NULL)
    {
      local_TMIt117 = EG (uninitialized_zval_ptr);
      local_TMIt117->refcount++;
    }
  zval** p_lhs = &local_TMIt117;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L157;
{
goto L157;
phc_check_invariants (TSRMLS_C);
}
// L157:
L157:;
// $TLE34 = wfsetvar($TMIt117, $policy);
{
   initialize_function_call (&wfsetvar_fci, &wfsetvar_fcic, "wfsetvar", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 69 TSRMLS_CC);
      zend_function* signature = wfsetvar_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[2];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [2];
   zval* args [2];
   zval** args_ind [2];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt117 == NULL)
    {
      local_TMIt117 = EG (uninitialized_zval_ptr);
      local_TMIt117->refcount++;
    }
  zval** p_arg = &local_TMIt117;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt117 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt117;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_policy == NULL)
    {
      local_policy = EG (uninitialized_zval_ptr);
      local_policy->refcount++;
    }
  zval** p_arg = &local_policy;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_policy == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_policy;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 69, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = wfsetvar_fci.param_count;
   zval*** params_save = wfsetvar_fci.params;
   zval** retval_save = wfsetvar_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   wfsetvar_fci.params = args_ind;
   wfsetvar_fci.param_count = 2;
   wfsetvar_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&wfsetvar_fci, &wfsetvar_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   wfsetvar_fci.params = params_save;
   wfsetvar_fci.param_count = param_count_save;
   wfsetvar_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 2; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE34 == NULL)
    {
      local_TLE34 = EG (uninitialized_zval_ptr);
      local_TLE34->refcount++;
    }
  zval** p_lhs = &local_TLE34;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// return $TLE34;
{
     zval* rhs;
  if (local_TLE34 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE34;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE118 != NULL)
{
zval_ptr_dtor (&local_TLE118);
}
if (local_TLE34 != NULL)
{
zval_ptr_dtor (&local_TLE34);
}
if (local_TMIt117 != NULL)
{
zval_ptr_dtor (&local_TMIt117);
}
if (local_policy != NULL)
{
zval_ptr_dtor (&local_policy);
}
}
// function addcategory($c, $sort)
// {
// 	$TSt35 =& $this->mCategories;
// 	$TSt35[$c] = $sort;
// }
PHP_METHOD(ParserOutput, addcategory)
{
zval* local_TSt35 = NULL;
zval* local_c = NULL;
zval* local_sort = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[2];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_c != NULL)
{
	zval_ptr_dtor (&local_c);
}
local_c = params[0];
// param 1
params[1]->refcount++;
if (local_sort != NULL)
{
	zval_ptr_dtor (&local_sort);
}
local_sort = params[1];
}
// Function body
// $TSt35 =& $this->mCategories;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mCategories", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt35 == NULL)
    {
      local_TSt35 = EG (uninitialized_zval_ptr);
      local_TSt35->refcount++;
    }
  zval** p_lhs = &local_TSt35;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TSt35[$c] = $sort;
{
     if (local_TSt35 == NULL)
    {
      local_TSt35 = EG (uninitialized_zval_ptr);
      local_TSt35->refcount++;
    }
  zval** p_array = &local_TSt35;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_c == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_c;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_sort == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_sort;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_sort == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_sort;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt35 != NULL)
{
zval_ptr_dtor (&local_TSt35);
}
if (local_c != NULL)
{
zval_ptr_dtor (&local_c);
}
if (local_sort != NULL)
{
zval_ptr_dtor (&local_sort);
}
}
// function addlanguagelink($t)
// {
// 	$TSt36 =& $this->mLanguageLinks;
// 	$TSt36[] = $t;
// }
PHP_METHOD(ParserOutput, addlanguagelink)
{
zval* local_TSt36 = NULL;
zval* local_t = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_t != NULL)
{
	zval_ptr_dtor (&local_t);
}
local_t = params[0];
}
// Function body
// $TSt36 =& $this->mLanguageLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mLanguageLinks", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt36 == NULL)
    {
      local_TSt36 = EG (uninitialized_zval_ptr);
      local_TSt36->refcount++;
    }
  zval** p_lhs = &local_TSt36;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TSt36[] = $t;
{
     if (local_TSt36 == NULL)
    {
      local_TSt36 = EG (uninitialized_zval_ptr);
      local_TSt36->refcount++;
    }
  zval** p_array = &local_TSt36;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_t == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_t;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt36 != NULL)
{
zval_ptr_dtor (&local_TSt36);
}
if (local_t != NULL)
{
zval_ptr_dtor (&local_t);
}
}
// function addexternallink($url)
// {
// 	$TSt37 =& $this->mExternalLinks;
// 	$TLE38 = 1;
// 	$TSt37[$url] = $TLE38;
// }
PHP_METHOD(ParserOutput, addexternallink)
{
zval* local_TLE38 = NULL;
zval* local_TSt37 = NULL;
zval* local_this = getThis();
zval* local_url = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_url != NULL)
{
	zval_ptr_dtor (&local_url);
}
local_url = params[0];
}
// Function body
// $TSt37 =& $this->mExternalLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mExternalLinks", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt37 == NULL)
    {
      local_TSt37 = EG (uninitialized_zval_ptr);
      local_TSt37->refcount++;
    }
  zval** p_lhs = &local_TSt37;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TLE38 = 1;
{
        if (local_TLE38 == NULL)
    {
      local_TLE38 = EG (uninitialized_zval_ptr);
      local_TLE38->refcount++;
    }
  zval** p_lhs = &local_TLE38;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TSt37[$url] = $TLE38;
{
     if (local_TSt37 == NULL)
    {
      local_TSt37 = EG (uninitialized_zval_ptr);
      local_TSt37->refcount++;
    }
  zval** p_array = &local_TSt37;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_url == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_url;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TLE38 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE38;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TLE38 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE38;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE38 != NULL)
{
zval_ptr_dtor (&local_TLE38);
}
if (local_TSt37 != NULL)
{
zval_ptr_dtor (&local_TSt37);
}
if (local_url != NULL)
{
zval_ptr_dtor (&local_url);
}
}
// function addwarning($s)
// {
// 	$TSt39 =& $this->mWarnings;
// 	$TLE40 = 1;
// 	$TSt39[$s] = $TLE40;
// }
PHP_METHOD(ParserOutput, addwarning)
{
zval* local_TLE40 = NULL;
zval* local_TSt39 = NULL;
zval* local_s = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_s != NULL)
{
	zval_ptr_dtor (&local_s);
}
local_s = params[0];
}
// Function body
// $TSt39 =& $this->mWarnings;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mWarnings", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt39 == NULL)
    {
      local_TSt39 = EG (uninitialized_zval_ptr);
      local_TSt39->refcount++;
    }
  zval** p_lhs = &local_TSt39;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TLE40 = 1;
{
        if (local_TLE40 == NULL)
    {
      local_TLE40 = EG (uninitialized_zval_ptr);
      local_TLE40->refcount++;
    }
  zval** p_lhs = &local_TLE40;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TSt39[$s] = $TLE40;
{
     if (local_TSt39 == NULL)
    {
      local_TSt39 = EG (uninitialized_zval_ptr);
      local_TSt39->refcount++;
    }
  zval** p_array = &local_TSt39;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_s == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_s;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TLE40 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE40;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TLE40 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE40;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE40 != NULL)
{
zval_ptr_dtor (&local_TLE40);
}
if (local_TSt39 != NULL)
{
zval_ptr_dtor (&local_TSt39);
}
if (local_s != NULL)
{
zval_ptr_dtor (&local_s);
}
}
// function addoutputhook($hook, $data = False)
// {
// 	$TSt41 =& $this->mOutputHooks;
// 	unset($TSa42);
// 	$TSa42 = (array) $TSa42;
// 	$TSa42[] = $hook;
// 	$TSa42[] = $data;
// 	$TSt41[] = $TSa42;
// }
PHP_METHOD(ParserOutput, addoutputhook)
{
zval* local_TSa42 = NULL;
zval* local_TSt41 = NULL;
zval* local_data = NULL;
zval* local_hook = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[2];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_hook != NULL)
{
	zval_ptr_dtor (&local_hook);
}
local_hook = params[0];
// param 1
if (num_args <= 1)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = False;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[1] = default_value;
}
params[1]->refcount++;
if (local_data != NULL)
{
	zval_ptr_dtor (&local_data);
}
local_data = params[1];
}
// Function body
// $TSt41 =& $this->mOutputHooks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mOutputHooks", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt41 == NULL)
    {
      local_TSt41 = EG (uninitialized_zval_ptr);
      local_TSt41->refcount++;
    }
  zval** p_lhs = &local_TSt41;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// unset($TSa42);
{
if (local_TSa42 != NULL)
{
zval_ptr_dtor (&local_TSa42);
local_TSa42 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa42 = (array) $TSa42;
{
      if (local_TSa42 == NULL)
    {
      local_TSa42 = EG (uninitialized_zval_ptr);
      local_TSa42->refcount++;
    }
  zval** p_lhs = &local_TSa42;

    zval* rhs;
  if (local_TSa42 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa42;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSa42[] = $hook;
{
     if (local_TSa42 == NULL)
    {
      local_TSa42 = EG (uninitialized_zval_ptr);
      local_TSa42->refcount++;
    }
  zval** p_array = &local_TSa42;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_hook == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_hook;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSa42[] = $data;
{
     if (local_TSa42 == NULL)
    {
      local_TSa42 = EG (uninitialized_zval_ptr);
      local_TSa42->refcount++;
    }
  zval** p_array = &local_TSa42;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_data == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_data;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// $TSt41[] = $TSa42;
{
     if (local_TSt41 == NULL)
    {
      local_TSt41 = EG (uninitialized_zval_ptr);
      local_TSt41->refcount++;
    }
  zval** p_array = &local_TSt41;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_TSa42 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa42;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSa42 != NULL)
{
zval_ptr_dtor (&local_TSa42);
}
if (local_TSt41 != NULL)
{
zval_ptr_dtor (&local_TSt41);
}
if (local_data != NULL)
{
zval_ptr_dtor (&local_data);
}
if (local_hook != NULL)
{
zval_ptr_dtor (&local_hook);
}
}
// function setnewsection($value)
// {
// 	$TLE43 = (bool) $value;
// 	$this->mNewSection = $TLE43;
// }
PHP_METHOD(ParserOutput, setnewsection)
{
zval* local_TLE43 = NULL;
zval* local_this = getThis();
zval* local_value = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_value != NULL)
{
	zval_ptr_dtor (&local_value);
}
local_value = params[0];
}
// Function body
// $TLE43 = (bool) $value;
{
      if (local_TLE43 == NULL)
    {
      local_TLE43 = EG (uninitialized_zval_ptr);
      local_TLE43->refcount++;
    }
  zval** p_lhs = &local_TLE43;

    zval* rhs;
  if (local_value == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_value;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $this->mNewSection = $TLE43;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TLE43 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE43;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mNewSection", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE43 != NULL)
{
zval_ptr_dtor (&local_TLE43);
}
if (local_value != NULL)
{
zval_ptr_dtor (&local_value);
}
}
// function getnewsection()
// {
// 	$TSt44 = $this->mNewSection;
// 	$TLE45 = (bool) $TSt44;
// 	return $TLE45;
// }
PHP_METHOD(ParserOutput, getnewsection)
{
zval* local_TLE45 = NULL;
zval* local_TSt44 = NULL;
zval* local_this = getThis();
// Function body
// $TSt44 = $this->mNewSection;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mNewSection", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt44 == NULL)
    {
      local_TSt44 = EG (uninitialized_zval_ptr);
      local_TSt44->refcount++;
    }
  zval** p_lhs = &local_TSt44;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE45 = (bool) $TSt44;
{
      if (local_TLE45 == NULL)
    {
      local_TLE45 = EG (uninitialized_zval_ptr);
      local_TLE45->refcount++;
    }
  zval** p_lhs = &local_TLE45;

    zval* rhs;
  if (local_TSt44 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt44;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// return $TLE45;
{
     zval* rhs;
  if (local_TLE45 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE45;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE45 != NULL)
{
zval_ptr_dtor (&local_TLE45);
}
if (local_TSt44 != NULL)
{
zval_ptr_dtor (&local_TSt44);
}
}
// function addlink($title, $id = NULL)
// {
// 	$ns = $title->getnamespace();
// 	$dbk = $title->getdbkey();
// 	$TLE46 = NS_MEDIA;
// 	$TLE47 = ($ns == $TLE46);
// 	if (TLE47) goto L164 else goto L165;
// L164:
// 	$ns = NS_FILE;
// 	goto L166;
// L165:
// 	$TLE48 = NS_SPECIAL;
// 	$TLE49 = ($ns == $TLE48);
// 	if (TLE49) goto L161 else goto L162;
// L161:
// 	$TLE50 = NULL;
// 	return $TLE50;
// 	goto L163;
// L162:
// 	$TLE51 = '';
// 	$TLE52 = ($dbk === $TLE51);
// 	if (TLE52) goto L158 else goto L159;
// L158:
// 	$TLE53 = NULL;
// 	return $TLE53;
// 	goto L160;
// L159:
// 	goto L160;
// L160:
// 	goto L163;
// L163:
// 	goto L166;
// L166:
// 	$TMIt119 = $this->mLinks;
// 	$TLE54 = isset($TMIt119[$ns]);
// 	$TLE55 = !$TLE54;
// 	if (TLE55) goto L167 else goto L168;
// L167:
// 	$TSt56 =& $this->mLinks;
// 	unset($TSa57);
// 	$TSa57 = (array) $TSa57;
// 	$TSt56[$ns] = $TSa57;
// 	goto L169;
// L168:
// 	goto L169;
// L169:
// 	$TLE58 = is_null($id);
// 	if (TLE58) goto L170 else goto L171;
// L170:
// 	$id = $title->getarticleid();
// 	goto L172;
// L171:
// 	goto L172;
// L172:
// 	$TSt59 =& $this->mLinks;
// 	$TSi60 =& $TSt59[$ns];
// 	$TSi60[$dbk] = $id;
// }
PHP_METHOD(ParserOutput, addlink)
{
zval* local_TLE46 = NULL;
zval* local_TLE47 = NULL;
zval* local_TLE48 = NULL;
zval* local_TLE49 = NULL;
zval* local_TLE50 = NULL;
zval* local_TLE51 = NULL;
zval* local_TLE52 = NULL;
zval* local_TLE53 = NULL;
zval* local_TLE54 = NULL;
zval* local_TLE55 = NULL;
zval* local_TLE58 = NULL;
zval* local_TMIt119 = NULL;
zval* local_TSa57 = NULL;
zval* local_TSi60 = NULL;
zval* local_TSt56 = NULL;
zval* local_TSt59 = NULL;
zval* local_dbk = NULL;
zval* local_id = NULL;
zval* local_ns = NULL;
zval* local_this = getThis();
zval* local_title = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[2];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_title != NULL)
{
	zval_ptr_dtor (&local_title);
}
local_title = params[0];
// param 1
if (num_args <= 1)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[1] = default_value;
}
params[1]->refcount++;
if (local_id != NULL)
{
	zval_ptr_dtor (&local_id);
}
local_id = params[1];
}
// Function body
// $ns = $title->getnamespace();
{
     if (local_title == NULL)
    {
      local_title = EG (uninitialized_zval_ptr);
      local_title->refcount++;
    }
  zval** p_obj = &local_title;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getnamespace", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 88 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 88, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_ns == NULL)
    {
      local_ns = EG (uninitialized_zval_ptr);
      local_ns->refcount++;
    }
  zval** p_lhs = &local_ns;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $dbk = $title->getdbkey();
{
     if (local_title == NULL)
    {
      local_title = EG (uninitialized_zval_ptr);
      local_title->refcount++;
    }
  zval** p_obj = &local_title;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getdbkey", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 89 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 89, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_dbk == NULL)
    {
      local_dbk = EG (uninitialized_zval_ptr);
      local_dbk->refcount++;
    }
  zval** p_lhs = &local_dbk;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE46 = NS_MEDIA;
{
   // No null-terminator in length for get_constant.
   // zend_get_constant always returns a copy of the constant.
     if (local_TLE46 == NULL)
    {
      local_TLE46 = EG (uninitialized_zval_ptr);
      local_TLE46->refcount++;
    }
  zval** p_lhs = &local_TLE46;

   if (!(*p_lhs)->is_ref)
   {
        zval_ptr_dtor (p_lhs);
   get_constant ("NS_MEDIA", 8, p_lhs TSRMLS_CC);

   }
   else
   {
         zval* constant;
    get_constant ("NS_MEDIA", 8, p_lhs TSRMLS_CC);
    overwrite_lhs_no_copy (*p_lhs, constant);
    safe_free_zval_ptr (constant);

   }
phc_check_invariants (TSRMLS_C);
}
// $TLE47 = ($ns == $TLE46);
{
    if (local_TLE47 == NULL)
    {
      local_TLE47 = EG (uninitialized_zval_ptr);
      local_TLE47->refcount++;
    }
  zval** p_lhs = &local_TLE47;

    zval* left;
  if (local_ns == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_ns;

    zval* right;
  if (local_TLE46 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE46;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE47) goto L164 else goto L165;
{
     zval* p_cond;
  if (local_TLE47 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE47;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L164;
   else
      goto L165;
phc_check_invariants (TSRMLS_C);
}
// L164:
L164:;
// $ns = NS_FILE;
{
   // No null-terminator in length for get_constant.
   // zend_get_constant always returns a copy of the constant.
     if (local_ns == NULL)
    {
      local_ns = EG (uninitialized_zval_ptr);
      local_ns->refcount++;
    }
  zval** p_lhs = &local_ns;

   if (!(*p_lhs)->is_ref)
   {
        zval_ptr_dtor (p_lhs);
   get_constant ("NS_FILE", 7, p_lhs TSRMLS_CC);

   }
   else
   {
         zval* constant;
    get_constant ("NS_FILE", 7, p_lhs TSRMLS_CC);
    overwrite_lhs_no_copy (*p_lhs, constant);
    safe_free_zval_ptr (constant);

   }
phc_check_invariants (TSRMLS_C);
}
// goto L166;
{
goto L166;
phc_check_invariants (TSRMLS_C);
}
// L165:
L165:;
// $TLE48 = NS_SPECIAL;
{
   // No null-terminator in length for get_constant.
   // zend_get_constant always returns a copy of the constant.
     if (local_TLE48 == NULL)
    {
      local_TLE48 = EG (uninitialized_zval_ptr);
      local_TLE48->refcount++;
    }
  zval** p_lhs = &local_TLE48;

   if (!(*p_lhs)->is_ref)
   {
        zval_ptr_dtor (p_lhs);
   get_constant ("NS_SPECIAL", 10, p_lhs TSRMLS_CC);

   }
   else
   {
         zval* constant;
    get_constant ("NS_SPECIAL", 10, p_lhs TSRMLS_CC);
    overwrite_lhs_no_copy (*p_lhs, constant);
    safe_free_zval_ptr (constant);

   }
phc_check_invariants (TSRMLS_C);
}
// $TLE49 = ($ns == $TLE48);
{
    if (local_TLE49 == NULL)
    {
      local_TLE49 = EG (uninitialized_zval_ptr);
      local_TLE49->refcount++;
    }
  zval** p_lhs = &local_TLE49;

    zval* left;
  if (local_ns == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_ns;

    zval* right;
  if (local_TLE48 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE48;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE49) goto L161 else goto L162;
{
     zval* p_cond;
  if (local_TLE49 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE49;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L161;
   else
      goto L162;
phc_check_invariants (TSRMLS_C);
}
// L161:
L161:;
// $TLE50 = NULL;
{
        if (local_TLE50 == NULL)
    {
      local_TLE50 = EG (uninitialized_zval_ptr);
      local_TLE50->refcount++;
    }
  zval** p_lhs = &local_TLE50;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
// return $TLE50;
{
     zval* rhs;
  if (local_TLE50 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE50;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// goto L163;
{
goto L163;
phc_check_invariants (TSRMLS_C);
}
// L162:
L162:;
// $TLE51 = '';
{
        if (local_TLE51 == NULL)
    {
      local_TLE51 = EG (uninitialized_zval_ptr);
      local_TLE51->refcount++;
    }
  zval** p_lhs = &local_TLE51;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE52 = ($dbk === $TLE51);
{
    if (local_TLE52 == NULL)
    {
      local_TLE52 = EG (uninitialized_zval_ptr);
      local_TLE52->refcount++;
    }
  zval** p_lhs = &local_TLE52;

    zval* left;
  if (local_dbk == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_dbk;

    zval* right;
  if (local_TLE51 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE51;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_identical_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE52) goto L158 else goto L159;
{
     zval* p_cond;
  if (local_TLE52 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE52;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L158;
   else
      goto L159;
phc_check_invariants (TSRMLS_C);
}
// L158:
L158:;
// $TLE53 = NULL;
{
        if (local_TLE53 == NULL)
    {
      local_TLE53 = EG (uninitialized_zval_ptr);
      local_TLE53->refcount++;
    }
  zval** p_lhs = &local_TLE53;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
// return $TLE53;
{
     zval* rhs;
  if (local_TLE53 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE53;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// goto L160;
{
goto L160;
phc_check_invariants (TSRMLS_C);
}
// L159:
L159:;
// goto L160;
{
goto L160;
phc_check_invariants (TSRMLS_C);
}
// L160:
L160:;
// goto L163;
{
goto L163;
phc_check_invariants (TSRMLS_C);
}
// L163:
L163:;
// goto L166;
{
goto L166;
phc_check_invariants (TSRMLS_C);
}
// L166:
L166:;
// $TMIt119 = $this->mLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mLinks", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt119 == NULL)
    {
      local_TMIt119 = EG (uninitialized_zval_ptr);
      local_TMIt119->refcount++;
    }
  zval** p_lhs = &local_TMIt119;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE54 = isset($TMIt119[$ns]);
{
if (local_TLE54 == NULL)
{
local_TLE54 = EG (uninitialized_zval_ptr);
local_TLE54->refcount++;
}
zval** p_lhs = &local_TLE54;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
if (local_TMIt119 == NULL)
{
local_TMIt119 = EG (uninitialized_zval_ptr);
local_TMIt119->refcount++;
}
zval** u_array = &local_TMIt119;
zval* u_index;
if (local_ns == NULL)
{
u_index = EG (uninitialized_zval_ptr);
}
else
{
u_index = local_ns;
}
ZVAL_BOOL(value, isset_array (u_array, u_index));
phc_check_invariants (TSRMLS_C);
}
// $TLE55 = !$TLE54;
{
     if (local_TLE55 == NULL)
    {
      local_TLE55 = EG (uninitialized_zval_ptr);
      local_TLE55->refcount++;
    }
  zval** p_lhs = &local_TLE55;

     zval* rhs;
  if (local_TLE54 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE54;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE55) goto L167 else goto L168;
{
     zval* p_cond;
  if (local_TLE55 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE55;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L167;
   else
      goto L168;
phc_check_invariants (TSRMLS_C);
}
// L167:
L167:;
// $TSt56 =& $this->mLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mLinks", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt56 == NULL)
    {
      local_TSt56 = EG (uninitialized_zval_ptr);
      local_TSt56->refcount++;
    }
  zval** p_lhs = &local_TSt56;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// unset($TSa57);
{
if (local_TSa57 != NULL)
{
zval_ptr_dtor (&local_TSa57);
local_TSa57 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa57 = (array) $TSa57;
{
      if (local_TSa57 == NULL)
    {
      local_TSa57 = EG (uninitialized_zval_ptr);
      local_TSa57->refcount++;
    }
  zval** p_lhs = &local_TSa57;

    zval* rhs;
  if (local_TSa57 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa57;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSt56[$ns] = $TSa57;
{
     if (local_TSt56 == NULL)
    {
      local_TSt56 = EG (uninitialized_zval_ptr);
      local_TSt56->refcount++;
    }
  zval** p_array = &local_TSt56;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_ns == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_ns;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TSa57 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa57;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TSa57 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa57;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// goto L169;
{
goto L169;
phc_check_invariants (TSRMLS_C);
}
// L168:
L168:;
// goto L169;
{
goto L169;
phc_check_invariants (TSRMLS_C);
}
// L169:
L169:;
// $TLE58 = is_null($id);
{
   initialize_function_call (&is_null_fci, &is_null_fcic, "is_null", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 104 TSRMLS_CC);
      zend_function* signature = is_null_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[1];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [1];
   zval* args [1];
   zval** args_ind [1];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_id == NULL)
    {
      local_id = EG (uninitialized_zval_ptr);
      local_id->refcount++;
    }
  zval** p_arg = &local_id;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_id == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_id;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 104, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = is_null_fci.param_count;
   zval*** params_save = is_null_fci.params;
   zval** retval_save = is_null_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   is_null_fci.params = args_ind;
   is_null_fci.param_count = 1;
   is_null_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&is_null_fci, &is_null_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   is_null_fci.params = params_save;
   is_null_fci.param_count = param_count_save;
   is_null_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 1; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE58 == NULL)
    {
      local_TLE58 = EG (uninitialized_zval_ptr);
      local_TLE58->refcount++;
    }
  zval** p_lhs = &local_TLE58;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// if (TLE58) goto L170 else goto L171;
{
     zval* p_cond;
  if (local_TLE58 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE58;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L170;
   else
      goto L171;
phc_check_invariants (TSRMLS_C);
}
// L170:
L170:;
// $id = $title->getarticleid();
{
     if (local_title == NULL)
    {
      local_title = EG (uninitialized_zval_ptr);
      local_title->refcount++;
    }
  zval** p_obj = &local_title;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getarticleid", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 105 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 105, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_id == NULL)
    {
      local_id = EG (uninitialized_zval_ptr);
      local_id->refcount++;
    }
  zval** p_lhs = &local_id;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// goto L172;
{
goto L172;
phc_check_invariants (TSRMLS_C);
}
// L171:
L171:;
// goto L172;
{
goto L172;
phc_check_invariants (TSRMLS_C);
}
// L172:
L172:;
// $TSt59 =& $this->mLinks;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mLinks", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt59 == NULL)
    {
      local_TSt59 = EG (uninitialized_zval_ptr);
      local_TSt59->refcount++;
    }
  zval** p_lhs = &local_TSt59;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TSi60 =& $TSt59[$ns];
{
     if (local_TSi60 == NULL)
    {
      local_TSi60 = EG (uninitialized_zval_ptr);
      local_TSi60->refcount++;
    }
  zval** p_lhs = &local_TSi60;

     if (local_TSt59 == NULL)
    {
      local_TSt59 = EG (uninitialized_zval_ptr);
      local_TSt59->refcount++;
    }
  zval** p_r_array = &local_TSt59;

     zval* r_index;
  if (local_ns == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_ns;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// $TSi60[$dbk] = $id;
{
     if (local_TSi60 == NULL)
    {
      local_TSi60 = EG (uninitialized_zval_ptr);
      local_TSi60->refcount++;
    }
  zval** p_array = &local_TSi60;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_dbk == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_dbk;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_id == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_id;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_id == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_id;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE46 != NULL)
{
zval_ptr_dtor (&local_TLE46);
}
if (local_TLE47 != NULL)
{
zval_ptr_dtor (&local_TLE47);
}
if (local_TLE48 != NULL)
{
zval_ptr_dtor (&local_TLE48);
}
if (local_TLE49 != NULL)
{
zval_ptr_dtor (&local_TLE49);
}
if (local_TLE50 != NULL)
{
zval_ptr_dtor (&local_TLE50);
}
if (local_TLE51 != NULL)
{
zval_ptr_dtor (&local_TLE51);
}
if (local_TLE52 != NULL)
{
zval_ptr_dtor (&local_TLE52);
}
if (local_TLE53 != NULL)
{
zval_ptr_dtor (&local_TLE53);
}
if (local_TLE54 != NULL)
{
zval_ptr_dtor (&local_TLE54);
}
if (local_TLE55 != NULL)
{
zval_ptr_dtor (&local_TLE55);
}
if (local_TLE58 != NULL)
{
zval_ptr_dtor (&local_TLE58);
}
if (local_TMIt119 != NULL)
{
zval_ptr_dtor (&local_TMIt119);
}
if (local_TSa57 != NULL)
{
zval_ptr_dtor (&local_TSa57);
}
if (local_TSi60 != NULL)
{
zval_ptr_dtor (&local_TSi60);
}
if (local_TSt56 != NULL)
{
zval_ptr_dtor (&local_TSt56);
}
if (local_TSt59 != NULL)
{
zval_ptr_dtor (&local_TSt59);
}
if (local_dbk != NULL)
{
zval_ptr_dtor (&local_dbk);
}
if (local_id != NULL)
{
zval_ptr_dtor (&local_id);
}
if (local_ns != NULL)
{
zval_ptr_dtor (&local_ns);
}
if (local_title != NULL)
{
zval_ptr_dtor (&local_title);
}
}
// function addimage($name)
// {
// 	$TSt61 =& $this->mImages;
// 	$TLE62 = 1;
// 	$TSt61[$name] = $TLE62;
// }
PHP_METHOD(ParserOutput, addimage)
{
zval* local_TLE62 = NULL;
zval* local_TSt61 = NULL;
zval* local_name = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_name != NULL)
{
	zval_ptr_dtor (&local_name);
}
local_name = params[0];
}
// Function body
// $TSt61 =& $this->mImages;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mImages", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt61 == NULL)
    {
      local_TSt61 = EG (uninitialized_zval_ptr);
      local_TSt61->refcount++;
    }
  zval** p_lhs = &local_TSt61;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TLE62 = 1;
{
        if (local_TLE62 == NULL)
    {
      local_TLE62 = EG (uninitialized_zval_ptr);
      local_TLE62->refcount++;
    }
  zval** p_lhs = &local_TLE62;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TSt61[$name] = $TLE62;
{
     if (local_TSt61 == NULL)
    {
      local_TSt61 = EG (uninitialized_zval_ptr);
      local_TSt61->refcount++;
    }
  zval** p_array = &local_TSt61;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_name == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_name;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TLE62 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE62;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TLE62 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE62;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE62 != NULL)
{
zval_ptr_dtor (&local_TLE62);
}
if (local_TSt61 != NULL)
{
zval_ptr_dtor (&local_TSt61);
}
if (local_name != NULL)
{
zval_ptr_dtor (&local_name);
}
}
// function addtemplate($title, $page_id, $rev_id)
// {
// 	$ns = $title->getnamespace();
// 	$dbk = $title->getdbkey();
// 	$TMIt120 = $this->mTemplates;
// 	$TLE63 = isset($TMIt120[$ns]);
// 	$TLE64 = !$TLE63;
// 	if (TLE64) goto L173 else goto L174;
// L173:
// 	$TSt65 =& $this->mTemplates;
// 	unset($TSa66);
// 	$TSa66 = (array) $TSa66;
// 	$TSt65[$ns] = $TSa66;
// 	goto L175;
// L174:
// 	goto L175;
// L175:
// 	$TSt67 =& $this->mTemplates;
// 	$TSi68 =& $TSt67[$ns];
// 	$TSi68[$dbk] = $page_id;
// 	$TMIt121 = $this->mTemplateIds;
// 	$TLE69 = isset($TMIt121[$ns]);
// 	$TLE70 = !$TLE69;
// 	if (TLE70) goto L176 else goto L177;
// L176:
// 	$TSt71 =& $this->mTemplateIds;
// 	unset($TSa72);
// 	$TSa72 = (array) $TSa72;
// 	$TSt71[$ns] = $TSa72;
// 	goto L178;
// L177:
// 	goto L178;
// L178:
// 	$TSt73 =& $this->mTemplateIds;
// 	$TSi74 =& $TSt73[$ns];
// 	$TSi74[$dbk] = $rev_id;
// }
PHP_METHOD(ParserOutput, addtemplate)
{
zval* local_TLE63 = NULL;
zval* local_TLE64 = NULL;
zval* local_TLE69 = NULL;
zval* local_TLE70 = NULL;
zval* local_TMIt120 = NULL;
zval* local_TMIt121 = NULL;
zval* local_TSa66 = NULL;
zval* local_TSa72 = NULL;
zval* local_TSi68 = NULL;
zval* local_TSi74 = NULL;
zval* local_TSt65 = NULL;
zval* local_TSt67 = NULL;
zval* local_TSt71 = NULL;
zval* local_TSt73 = NULL;
zval* local_dbk = NULL;
zval* local_ns = NULL;
zval* local_page_id = NULL;
zval* local_rev_id = NULL;
zval* local_this = getThis();
zval* local_title = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[3];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_title != NULL)
{
	zval_ptr_dtor (&local_title);
}
local_title = params[0];
// param 1
params[1]->refcount++;
if (local_page_id != NULL)
{
	zval_ptr_dtor (&local_page_id);
}
local_page_id = params[1];
// param 2
params[2]->refcount++;
if (local_rev_id != NULL)
{
	zval_ptr_dtor (&local_rev_id);
}
local_rev_id = params[2];
}
// Function body
// $ns = $title->getnamespace();
{
     if (local_title == NULL)
    {
      local_title = EG (uninitialized_zval_ptr);
      local_title->refcount++;
    }
  zval** p_obj = &local_title;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getnamespace", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 115 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 115, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_ns == NULL)
    {
      local_ns = EG (uninitialized_zval_ptr);
      local_ns->refcount++;
    }
  zval** p_lhs = &local_ns;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $dbk = $title->getdbkey();
{
     if (local_title == NULL)
    {
      local_title = EG (uninitialized_zval_ptr);
      local_title->refcount++;
    }
  zval** p_obj = &local_title;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getdbkey", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 116 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 116, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_dbk == NULL)
    {
      local_dbk = EG (uninitialized_zval_ptr);
      local_dbk->refcount++;
    }
  zval** p_lhs = &local_dbk;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TMIt120 = $this->mTemplates;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplates", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt120 == NULL)
    {
      local_TMIt120 = EG (uninitialized_zval_ptr);
      local_TMIt120->refcount++;
    }
  zval** p_lhs = &local_TMIt120;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE63 = isset($TMIt120[$ns]);
{
if (local_TLE63 == NULL)
{
local_TLE63 = EG (uninitialized_zval_ptr);
local_TLE63->refcount++;
}
zval** p_lhs = &local_TLE63;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
if (local_TMIt120 == NULL)
{
local_TMIt120 = EG (uninitialized_zval_ptr);
local_TMIt120->refcount++;
}
zval** u_array = &local_TMIt120;
zval* u_index;
if (local_ns == NULL)
{
u_index = EG (uninitialized_zval_ptr);
}
else
{
u_index = local_ns;
}
ZVAL_BOOL(value, isset_array (u_array, u_index));
phc_check_invariants (TSRMLS_C);
}
// $TLE64 = !$TLE63;
{
     if (local_TLE64 == NULL)
    {
      local_TLE64 = EG (uninitialized_zval_ptr);
      local_TLE64->refcount++;
    }
  zval** p_lhs = &local_TLE64;

     zval* rhs;
  if (local_TLE63 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE63;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE64) goto L173 else goto L174;
{
     zval* p_cond;
  if (local_TLE64 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE64;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L173;
   else
      goto L174;
phc_check_invariants (TSRMLS_C);
}
// L173:
L173:;
// $TSt65 =& $this->mTemplates;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplates", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt65 == NULL)
    {
      local_TSt65 = EG (uninitialized_zval_ptr);
      local_TSt65->refcount++;
    }
  zval** p_lhs = &local_TSt65;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// unset($TSa66);
{
if (local_TSa66 != NULL)
{
zval_ptr_dtor (&local_TSa66);
local_TSa66 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa66 = (array) $TSa66;
{
      if (local_TSa66 == NULL)
    {
      local_TSa66 = EG (uninitialized_zval_ptr);
      local_TSa66->refcount++;
    }
  zval** p_lhs = &local_TSa66;

    zval* rhs;
  if (local_TSa66 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa66;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSt65[$ns] = $TSa66;
{
     if (local_TSt65 == NULL)
    {
      local_TSt65 = EG (uninitialized_zval_ptr);
      local_TSt65->refcount++;
    }
  zval** p_array = &local_TSt65;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_ns == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_ns;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TSa66 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa66;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TSa66 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa66;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// goto L175;
{
goto L175;
phc_check_invariants (TSRMLS_C);
}
// L174:
L174:;
// goto L175;
{
goto L175;
phc_check_invariants (TSRMLS_C);
}
// L175:
L175:;
// $TSt67 =& $this->mTemplates;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplates", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt67 == NULL)
    {
      local_TSt67 = EG (uninitialized_zval_ptr);
      local_TSt67->refcount++;
    }
  zval** p_lhs = &local_TSt67;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TSi68 =& $TSt67[$ns];
{
     if (local_TSi68 == NULL)
    {
      local_TSi68 = EG (uninitialized_zval_ptr);
      local_TSi68->refcount++;
    }
  zval** p_lhs = &local_TSi68;

     if (local_TSt67 == NULL)
    {
      local_TSt67 = EG (uninitialized_zval_ptr);
      local_TSt67->refcount++;
    }
  zval** p_r_array = &local_TSt67;

     zval* r_index;
  if (local_ns == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_ns;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// $TSi68[$dbk] = $page_id;
{
     if (local_TSi68 == NULL)
    {
      local_TSi68 = EG (uninitialized_zval_ptr);
      local_TSi68->refcount++;
    }
  zval** p_array = &local_TSi68;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_dbk == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_dbk;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_page_id == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_page_id;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_page_id == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_page_id;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// $TMIt121 = $this->mTemplateIds;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplateIds", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt121 == NULL)
    {
      local_TMIt121 = EG (uninitialized_zval_ptr);
      local_TMIt121->refcount++;
    }
  zval** p_lhs = &local_TMIt121;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE69 = isset($TMIt121[$ns]);
{
if (local_TLE69 == NULL)
{
local_TLE69 = EG (uninitialized_zval_ptr);
local_TLE69->refcount++;
}
zval** p_lhs = &local_TLE69;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
if (local_TMIt121 == NULL)
{
local_TMIt121 = EG (uninitialized_zval_ptr);
local_TMIt121->refcount++;
}
zval** u_array = &local_TMIt121;
zval* u_index;
if (local_ns == NULL)
{
u_index = EG (uninitialized_zval_ptr);
}
else
{
u_index = local_ns;
}
ZVAL_BOOL(value, isset_array (u_array, u_index));
phc_check_invariants (TSRMLS_C);
}
// $TLE70 = !$TLE69;
{
     if (local_TLE70 == NULL)
    {
      local_TLE70 = EG (uninitialized_zval_ptr);
      local_TLE70->refcount++;
    }
  zval** p_lhs = &local_TLE70;

     zval* rhs;
  if (local_TLE69 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE69;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE70) goto L176 else goto L177;
{
     zval* p_cond;
  if (local_TLE70 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE70;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L176;
   else
      goto L177;
phc_check_invariants (TSRMLS_C);
}
// L176:
L176:;
// $TSt71 =& $this->mTemplateIds;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplateIds", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt71 == NULL)
    {
      local_TSt71 = EG (uninitialized_zval_ptr);
      local_TSt71->refcount++;
    }
  zval** p_lhs = &local_TSt71;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// unset($TSa72);
{
if (local_TSa72 != NULL)
{
zval_ptr_dtor (&local_TSa72);
local_TSa72 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa72 = (array) $TSa72;
{
      if (local_TSa72 == NULL)
    {
      local_TSa72 = EG (uninitialized_zval_ptr);
      local_TSa72->refcount++;
    }
  zval** p_lhs = &local_TSa72;

    zval* rhs;
  if (local_TSa72 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa72;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $TSt71[$ns] = $TSa72;
{
     if (local_TSt71 == NULL)
    {
      local_TSt71 = EG (uninitialized_zval_ptr);
      local_TSt71->refcount++;
    }
  zval** p_array = &local_TSt71;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_ns == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_ns;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TSa72 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa72;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TSa72 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa72;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// goto L178;
{
goto L178;
phc_check_invariants (TSRMLS_C);
}
// L177:
L177:;
// goto L178;
{
goto L178;
phc_check_invariants (TSRMLS_C);
}
// L178:
L178:;
// $TSt73 =& $this->mTemplateIds;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mTemplateIds", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt73 == NULL)
    {
      local_TSt73 = EG (uninitialized_zval_ptr);
      local_TSt73->refcount++;
    }
  zval** p_lhs = &local_TSt73;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TSi74 =& $TSt73[$ns];
{
     if (local_TSi74 == NULL)
    {
      local_TSi74 = EG (uninitialized_zval_ptr);
      local_TSi74->refcount++;
    }
  zval** p_lhs = &local_TSi74;

     if (local_TSt73 == NULL)
    {
      local_TSt73 = EG (uninitialized_zval_ptr);
      local_TSt73->refcount++;
    }
  zval** p_r_array = &local_TSt73;

     zval* r_index;
  if (local_ns == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_ns;

   check_array_type (p_r_array TSRMLS_CC);
   zval** p_rhs = get_ht_entry (p_r_array, r_index TSRMLS_CC);
   sep_copy_on_write (p_rhs);
   copy_into_ref (p_lhs, p_rhs);
phc_check_invariants (TSRMLS_C);
}
// $TSi74[$dbk] = $rev_id;
{
     if (local_TSi74 == NULL)
    {
      local_TSi74 = EG (uninitialized_zval_ptr);
      local_TSi74->refcount++;
    }
  zval** p_array = &local_TSi74;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_dbk == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_dbk;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_rev_id == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_rev_id;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_rev_id == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_rev_id;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE63 != NULL)
{
zval_ptr_dtor (&local_TLE63);
}
if (local_TLE64 != NULL)
{
zval_ptr_dtor (&local_TLE64);
}
if (local_TLE69 != NULL)
{
zval_ptr_dtor (&local_TLE69);
}
if (local_TLE70 != NULL)
{
zval_ptr_dtor (&local_TLE70);
}
if (local_TMIt120 != NULL)
{
zval_ptr_dtor (&local_TMIt120);
}
if (local_TMIt121 != NULL)
{
zval_ptr_dtor (&local_TMIt121);
}
if (local_TSa66 != NULL)
{
zval_ptr_dtor (&local_TSa66);
}
if (local_TSa72 != NULL)
{
zval_ptr_dtor (&local_TSa72);
}
if (local_TSi68 != NULL)
{
zval_ptr_dtor (&local_TSi68);
}
if (local_TSi74 != NULL)
{
zval_ptr_dtor (&local_TSi74);
}
if (local_TSt65 != NULL)
{
zval_ptr_dtor (&local_TSt65);
}
if (local_TSt67 != NULL)
{
zval_ptr_dtor (&local_TSt67);
}
if (local_TSt71 != NULL)
{
zval_ptr_dtor (&local_TSt71);
}
if (local_TSt73 != NULL)
{
zval_ptr_dtor (&local_TSt73);
}
if (local_dbk != NULL)
{
zval_ptr_dtor (&local_dbk);
}
if (local_ns != NULL)
{
zval_ptr_dtor (&local_ns);
}
if (local_page_id != NULL)
{
zval_ptr_dtor (&local_page_id);
}
if (local_rev_id != NULL)
{
zval_ptr_dtor (&local_rev_id);
}
if (local_title != NULL)
{
zval_ptr_dtor (&local_title);
}
}
// function expired($touched)
// {
// 	global $wgCacheEpoch;
// 	$TLE75 = $this->getcachetime();
// 	$TLE76 = -1;
// 	$TLE0 = ($TLE75 == $TLE76);
// 	if (TLE0) goto L179 else goto L180;
// L179:
// 	$TEF1 = $TLE0;
// 	goto L181;
// L180:
// 	$TLE77 = $this->getcachetime();
// 	$TEF1 = ($TLE77 < $touched);
// 	goto L181;
// L181:
// 	$TLE2 = (bool) $TEF1;
// 	if (TLE2) goto L182 else goto L183;
// L182:
// 	$TEF3 = $TLE2;
// 	goto L184;
// L183:
// 	$TLE78 = $this->getcachetime();
// 	$TEF3 = ($TLE78 <= $wgCacheEpoch);
// 	goto L184;
// L184:
// 	$TLE4 = (bool) $TEF3;
// 	if (TLE4) goto L185 else goto L186;
// L185:
// 	$TEF5 = $TLE4;
// 	goto L187;
// L186:
// 	$TMIt122 = $this->mVersion;
// 	$TLE79 = isset($TMIt122);
// 	$TEF5 = !$TLE79;
// 	goto L187;
// L187:
// 	$TLE6 = (bool) $TEF5;
// 	if (TLE6) goto L191 else goto L192;
// L191:
// 	$TEF7 = $TLE6;
// 	goto L193;
// L192:
// 	$TLE80 = Parser::VERSION;
// 	$TLE81 = 'lt';
// 	$TLE124 = param_is_ref (NULL, "version_compare", 0);
// 	;
// 	if (TLE124) goto L188 else goto L189;
// L188:
// 	$TMIt123 =& $this->mVersion;
// 	goto L190;
// L189:
// 	$TMIt123 = $this->mVersion;
// 	goto L190;
// L190:
// 	$TEF7 = version_compare($TMIt123, $TLE80, $TLE81);
// 	goto L193;
// L193:
// 	$TLE82 = (bool) $TEF7;
// 	return $TLE82;
// }
PHP_METHOD(ParserOutput, expired)
{
zval* local_TEF1 = NULL;
zval* local_TEF3 = NULL;
zval* local_TEF5 = NULL;
zval* local_TEF7 = NULL;
zval* local_TLE0 = NULL;
zval* local_TLE124 = NULL;
zval* local_TLE2 = NULL;
zval* local_TLE4 = NULL;
zval* local_TLE6 = NULL;
zval* local_TLE75 = NULL;
zval* local_TLE76 = NULL;
zval* local_TLE77 = NULL;
zval* local_TLE78 = NULL;
zval* local_TLE79 = NULL;
zval* local_TLE80 = NULL;
zval* local_TLE81 = NULL;
zval* local_TLE82 = NULL;
zval* local_TMIt122 = NULL;
zval* local_TMIt123 = NULL;
zval* local_this = getThis();
zval* local_touched = NULL;
zval* local_wgCacheEpoch = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_touched != NULL)
{
	zval_ptr_dtor (&local_touched);
}
local_touched = params[0];
}
// Function body
// global $wgCacheEpoch;
{
     if (local_wgCacheEpoch == NULL)
    {
      local_wgCacheEpoch = EG (uninitialized_zval_ptr);
      local_wgCacheEpoch->refcount++;
    }
  zval** p_local = &local_wgCacheEpoch;

     zval** p_global = get_st_entry (&EG(symbol_table), "wgCacheEpoch", 12 + 1, 2181899014u TSRMLS_CC);

   sep_copy_on_write (p_global);
   copy_into_ref (p_local, p_global);
phc_check_invariants (TSRMLS_C);
}
// $TLE75 = $this->getcachetime();
{
     if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getcachetime", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 138 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 138, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE75 == NULL)
    {
      local_TLE75 = EG (uninitialized_zval_ptr);
      local_TLE75->refcount++;
    }
  zval** p_lhs = &local_TLE75;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TLE76 = -1;
{
        if (local_TLE76 == NULL)
    {
      local_TLE76 = EG (uninitialized_zval_ptr);
      local_TLE76->refcount++;
    }
  zval** p_lhs = &local_TLE76;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_LONG (value, -1);

phc_check_invariants (TSRMLS_C);
}
// $TLE0 = ($TLE75 == $TLE76);
{
    if (local_TLE0 == NULL)
    {
      local_TLE0 = EG (uninitialized_zval_ptr);
      local_TLE0->refcount++;
    }
  zval** p_lhs = &local_TLE0;

    zval* left;
  if (local_TLE75 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE75;

    zval* right;
  if (local_TLE76 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE76;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE0) goto L179 else goto L180;
{
     zval* p_cond;
  if (local_TLE0 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE0;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L179;
   else
      goto L180;
phc_check_invariants (TSRMLS_C);
}
// L179:
L179:;
// $TEF1 = $TLE0;
{
    if (local_TEF1 == NULL)
    {
      local_TEF1 = EG (uninitialized_zval_ptr);
      local_TEF1->refcount++;
    }
  zval** p_lhs = &local_TEF1;

    zval* rhs;
  if (local_TLE0 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE0;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L181;
{
goto L181;
phc_check_invariants (TSRMLS_C);
}
// L180:
L180:;
// $TLE77 = $this->getcachetime();
{
     if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getcachetime", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 139 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 139, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE77 == NULL)
    {
      local_TLE77 = EG (uninitialized_zval_ptr);
      local_TLE77->refcount++;
    }
  zval** p_lhs = &local_TLE77;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TEF1 = ($TLE77 < $touched);
{
    if (local_TEF1 == NULL)
    {
      local_TEF1 = EG (uninitialized_zval_ptr);
      local_TEF1->refcount++;
    }
  zval** p_lhs = &local_TEF1;

    zval* left;
  if (local_TLE77 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE77;

    zval* right;
  if (local_touched == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_touched;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_smaller_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L181;
{
goto L181;
phc_check_invariants (TSRMLS_C);
}
// L181:
L181:;
// $TLE2 = (bool) $TEF1;
{
      if (local_TLE2 == NULL)
    {
      local_TLE2 = EG (uninitialized_zval_ptr);
      local_TLE2->refcount++;
    }
  zval** p_lhs = &local_TLE2;

    zval* rhs;
  if (local_TEF1 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF1;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// if (TLE2) goto L182 else goto L183;
{
     zval* p_cond;
  if (local_TLE2 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE2;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L182;
   else
      goto L183;
phc_check_invariants (TSRMLS_C);
}
// L182:
L182:;
// $TEF3 = $TLE2;
{
    if (local_TEF3 == NULL)
    {
      local_TEF3 = EG (uninitialized_zval_ptr);
      local_TEF3->refcount++;
    }
  zval** p_lhs = &local_TEF3;

    zval* rhs;
  if (local_TLE2 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE2;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L184;
{
goto L184;
phc_check_invariants (TSRMLS_C);
}
// L183:
L183:;
// $TLE78 = $this->getcachetime();
{
     if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

   zend_fcall_info fci_object;
   zend_fcall_info_cache fcic_object = {0, NULL, NULL, NULL};
   initialize_method_call (&fci_object, &fcic_object, p_obj, "getcachetime", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 140 TSRMLS_CC);
      zend_function* signature = fcic_object.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[0];
   int abr_index = 0;
   

   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [0];
   zval* args [0];
   zval** args_ind [0];

   int af_index = 0;
   

   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 140, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = fci_object.param_count;
   zval*** params_save = fci_object.params;
   zval** retval_save = fci_object.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   fci_object.params = args_ind;
   fci_object.param_count = 0;
   fci_object.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&fci_object, &fcic_object TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   fci_object.params = params_save;
   fci_object.param_count = param_count_save;
   fci_object.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 0; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TLE78 == NULL)
    {
      local_TLE78 = EG (uninitialized_zval_ptr);
      local_TLE78->refcount++;
    }
  zval** p_lhs = &local_TLE78;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// $TEF3 = ($TLE78 <= $wgCacheEpoch);
{
    if (local_TEF3 == NULL)
    {
      local_TEF3 = EG (uninitialized_zval_ptr);
      local_TEF3->refcount++;
    }
  zval** p_lhs = &local_TEF3;

    zval* left;
  if (local_TLE78 == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_TLE78;

    zval* right;
  if (local_wgCacheEpoch == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_wgCacheEpoch;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_smaller_or_equal_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L184;
{
goto L184;
phc_check_invariants (TSRMLS_C);
}
// L184:
L184:;
// $TLE4 = (bool) $TEF3;
{
      if (local_TLE4 == NULL)
    {
      local_TLE4 = EG (uninitialized_zval_ptr);
      local_TLE4->refcount++;
    }
  zval** p_lhs = &local_TLE4;

    zval* rhs;
  if (local_TEF3 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF3;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// if (TLE4) goto L185 else goto L186;
{
     zval* p_cond;
  if (local_TLE4 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE4;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L185;
   else
      goto L186;
phc_check_invariants (TSRMLS_C);
}
// L185:
L185:;
// $TEF5 = $TLE4;
{
    if (local_TEF5 == NULL)
    {
      local_TEF5 = EG (uninitialized_zval_ptr);
      local_TEF5->refcount++;
    }
  zval** p_lhs = &local_TEF5;

    zval* rhs;
  if (local_TLE4 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE4;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L187;
{
goto L187;
phc_check_invariants (TSRMLS_C);
}
// L186:
L186:;
// $TMIt122 = $this->mVersion;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mVersion", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt122 == NULL)
    {
      local_TMIt122 = EG (uninitialized_zval_ptr);
      local_TMIt122->refcount++;
    }
  zval** p_lhs = &local_TMIt122;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE79 = isset($TMIt122);
{
if (local_TLE79 == NULL)
{
local_TLE79 = EG (uninitialized_zval_ptr);
local_TLE79->refcount++;
}
zval** p_lhs = &local_TLE79;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
ZVAL_BOOL(value, local_TMIt122 != NULL && !ZVAL_IS_NULL(local_TMIt122));
phc_check_invariants (TSRMLS_C);
}
// $TEF5 = !$TLE79;
{
     if (local_TEF5 == NULL)
    {
      local_TEF5 = EG (uninitialized_zval_ptr);
      local_TEF5->refcount++;
    }
  zval** p_lhs = &local_TEF5;

     zval* rhs;
  if (local_TLE79 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE79;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// goto L187;
{
goto L187;
phc_check_invariants (TSRMLS_C);
}
// L187:
L187:;
// $TLE6 = (bool) $TEF5;
{
      if (local_TLE6 == NULL)
    {
      local_TLE6 = EG (uninitialized_zval_ptr);
      local_TLE6->refcount++;
    }
  zval** p_lhs = &local_TLE6;

    zval* rhs;
  if (local_TEF5 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF5;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// if (TLE6) goto L191 else goto L192;
{
     zval* p_cond;
  if (local_TLE6 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE6;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L191;
   else
      goto L192;
phc_check_invariants (TSRMLS_C);
}
// L191:
L191:;
// $TEF7 = $TLE6;
{
    if (local_TEF7 == NULL)
    {
      local_TEF7 = EG (uninitialized_zval_ptr);
      local_TEF7->refcount++;
    }
  zval** p_lhs = &local_TEF7;

    zval* rhs;
  if (local_TLE6 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE6;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L193;
{
goto L193;
phc_check_invariants (TSRMLS_C);
}
// L192:
L192:;
// $TLE80 = Parser::VERSION;
{
   // No null-terminator in length for get_constant.
   // zend_get_constant always returns a copy of the constant.
     if (local_TLE80 == NULL)
    {
      local_TLE80 = EG (uninitialized_zval_ptr);
      local_TLE80->refcount++;
    }
  zval** p_lhs = &local_TLE80;

   if (!(*p_lhs)->is_ref)
   {
        zval_ptr_dtor (p_lhs);
   get_constant ("Parser::VERSION", 15, p_lhs TSRMLS_CC);

   }
   else
   {
         zval* constant;
    get_constant ("Parser::VERSION", 15, p_lhs TSRMLS_CC);
    overwrite_lhs_no_copy (*p_lhs, constant);
    safe_free_zval_ptr (constant);

   }
phc_check_invariants (TSRMLS_C);
}
// $TLE81 = 'lt';
{
        if (local_TLE81 == NULL)
    {
      local_TLE81 = EG (uninitialized_zval_ptr);
      local_TLE81->refcount++;
    }
  zval** p_lhs = &local_TLE81;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "lt", 2, 1);

phc_check_invariants (TSRMLS_C);
}
// $TLE124 = param_is_ref (NULL, "version_compare", 0);
// ;
{
   initialize_function_call (&version_compare_fci, &version_compare_fcic, "version_compare", "<unknown>", 0 TSRMLS_CC);
	zend_function* signature = version_compare_fcic.function_handler;
	zend_arg_info* arg_info = signature->common.arg_info;
	int count = 0;
	while (arg_info && count < 0)
	{
		count++;
		arg_info++;
	}

	  if (local_TLE124 == NULL)
    {
      local_TLE124 = EG (uninitialized_zval_ptr);
      local_TLE124->refcount++;
    }
  zval** p_lhs = &local_TLE124;

	zval* rhs;
	ALLOC_INIT_ZVAL (rhs);
	if (arg_info && count == 0)
	{
		ZVAL_BOOL (rhs, arg_info->pass_by_reference);
	}
	else
	{
		ZVAL_BOOL (rhs, signature->common.pass_rest_by_reference);
	}
	write_var (p_lhs, rhs);
	zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// if (TLE124) goto L188 else goto L189;
{
     zval* p_cond;
  if (local_TLE124 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE124;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L188;
   else
      goto L189;
phc_check_invariants (TSRMLS_C);
}
// L188:
L188:;
// $TMIt123 =& $this->mVersion;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mVersion", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TMIt123 == NULL)
    {
      local_TMIt123 = EG (uninitialized_zval_ptr);
      local_TMIt123->refcount++;
    }
  zval** p_lhs = &local_TMIt123;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// goto L190;
{
goto L190;
phc_check_invariants (TSRMLS_C);
}
// L189:
L189:;
// $TMIt123 = $this->mVersion;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mVersion", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt123 == NULL)
    {
      local_TMIt123 = EG (uninitialized_zval_ptr);
      local_TMIt123->refcount++;
    }
  zval** p_lhs = &local_TMIt123;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// goto L190;
{
goto L190;
phc_check_invariants (TSRMLS_C);
}
// L190:
L190:;
// $TEF7 = version_compare($TMIt123, $TLE80, $TLE81);
{
   initialize_function_call (&version_compare_fci, &version_compare_fcic, "version_compare", "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 142 TSRMLS_CC);
      zend_function* signature = version_compare_fcic.function_handler;
   zend_arg_info* arg_info = signature->common.arg_info; // optional

   int by_ref[3];
   int abr_index = 0;
      // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;
   // TODO: find names to replace index
   if (arg_info)
   {
      by_ref[abr_index] = arg_info->pass_by_reference;
      arg_info++;
   }
   else
      by_ref[abr_index] = signature->common.pass_rest_by_reference;

   abr_index++;


   // Setup array of arguments
   // TODO: i think arrays of size 0 is an error
   int destruct [3];
   zval* args [3];
   zval** args_ind [3];

   int af_index = 0;
      destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TMIt123 == NULL)
    {
      local_TMIt123 = EG (uninitialized_zval_ptr);
      local_TMIt123->refcount++;
    }
  zval** p_arg = &local_TMIt123;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TMIt123 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TMIt123;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE80 == NULL)
    {
      local_TLE80 = EG (uninitialized_zval_ptr);
      local_TLE80->refcount++;
    }
  zval** p_arg = &local_TLE80;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE80 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE80;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;
   destruct[af_index] = 0;
   if (by_ref[af_index])
   {
        if (local_TLE81 == NULL)
    {
      local_TLE81 = EG (uninitialized_zval_ptr);
      local_TLE81->refcount++;
    }
  zval** p_arg = &local_TLE81;

      args_ind[af_index] = fetch_var_arg_by_ref (p_arg);
      assert (!in_copy_on_write (*args_ind[af_index]));
      args[af_index] = *args_ind[af_index];
   }
   else
   {
        zval* arg;
  if (local_TLE81 == NULL)
    arg = EG (uninitialized_zval_ptr);
  else
    arg = local_TLE81;

      args[af_index] = fetch_var_arg (arg, &destruct[af_index]);
      args_ind[af_index] = &args[af_index];
   }
   af_index++;


   phc_setup_error (1, "/home/mdupont/2009/02/introspector/rdfintrospector/mediawiki/trunk/phase3/includes/parser/ParserOutput.php", 142, NULL TSRMLS_CC);

   // save existing parameters, in case of recursion
   int param_count_save = version_compare_fci.param_count;
   zval*** params_save = version_compare_fci.params;
   zval** retval_save = version_compare_fci.retval_ptr_ptr;

   zval* rhs = NULL;

   // set up params
   version_compare_fci.params = args_ind;
   version_compare_fci.param_count = 3;
   version_compare_fci.retval_ptr_ptr = &rhs;

   // call the function
   int success = zend_call_function (&version_compare_fci, &version_compare_fcic TSRMLS_CC);
   assert(success == SUCCESS);

   // restore params
   version_compare_fci.params = params_save;
   version_compare_fci.param_count = param_count_save;
   version_compare_fci.retval_ptr_ptr = retval_save;

   // unset the errors
   phc_setup_error (0, NULL, 0, NULL TSRMLS_CC);

   int i;
   for (i = 0; i < 3; i++)
   {
      if (destruct[i])
      {
	 assert (destruct[i]);
	 zval_ptr_dtor (args_ind[i]);
      }
   }


   // When the Zend engine returns by reference, it allocates a zval into
   // retval_ptr_ptr. To return by reference, the callee writes into the
   // retval_ptr_ptr, freeing the allocated value as it does.  (Note, it may
   // not actually return anything). So the zval returned - whether we return
   // it, or it is the allocated zval - has a refcount of 1.
 
   // The caller is responsible for cleaning that up (note, this is unaffected
   // by whether it is added to some COW set).

   // For reasons unknown, the Zend API resets the refcount and is_ref fields
   // of the return value after the function returns (unless the callee is
   // interpreted). If the function is supposed to return by reference, this
   // loses the refcount. This only happens when non-interpreted code is
   // called. We work around it, when compiled code is called, by saving the
   // refcount into SAVED_REFCOUNT, in the return statement. The downside is
   // that we may create an error if our code is called by a callback, and
   // returns by reference, and the callback returns by reference. At least
   // this is an obscure case.
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
   {
      assert (rhs != EG(uninitialized_zval_ptr));
      rhs->is_ref = 1;
      if (saved_refcount != 0)
      {
	 rhs->refcount = saved_refcount;
      }
      rhs->refcount++;
   }
   saved_refcount = 0; // for 'obscure cases'

        if (local_TEF7 == NULL)
    {
      local_TEF7 = EG (uninitialized_zval_ptr);
      local_TEF7->refcount++;
    }
  zval** p_lhs = &local_TEF7;

   write_var (p_lhs, rhs);


   zval_ptr_dtor (&rhs);
   if(signature->common.return_reference && signature->type != ZEND_USER_FUNCTION)
      zval_ptr_dtor (&rhs);

phc_check_invariants (TSRMLS_C);
}
// goto L193;
{
goto L193;
phc_check_invariants (TSRMLS_C);
}
// L193:
L193:;
// $TLE82 = (bool) $TEF7;
{
      if (local_TLE82 == NULL)
    {
      local_TLE82 = EG (uninitialized_zval_ptr);
      local_TLE82->refcount++;
    }
  zval** p_lhs = &local_TLE82;

    zval* rhs;
  if (local_TEF7 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF7;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_BOOL >= 0 && IS_BOOL <= 6);
  if ((*p_lhs)->type != IS_BOOL)
  {
    sep_copy_on_write (p_lhs);
    convert_to_boolean (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// return $TLE82;
{
     zval* rhs;
  if (local_TLE82 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE82;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TEF1 != NULL)
{
zval_ptr_dtor (&local_TEF1);
}
if (local_TEF3 != NULL)
{
zval_ptr_dtor (&local_TEF3);
}
if (local_TEF5 != NULL)
{
zval_ptr_dtor (&local_TEF5);
}
if (local_TEF7 != NULL)
{
zval_ptr_dtor (&local_TEF7);
}
if (local_TLE0 != NULL)
{
zval_ptr_dtor (&local_TLE0);
}
if (local_TLE124 != NULL)
{
zval_ptr_dtor (&local_TLE124);
}
if (local_TLE2 != NULL)
{
zval_ptr_dtor (&local_TLE2);
}
if (local_TLE4 != NULL)
{
zval_ptr_dtor (&local_TLE4);
}
if (local_TLE6 != NULL)
{
zval_ptr_dtor (&local_TLE6);
}
if (local_TLE75 != NULL)
{
zval_ptr_dtor (&local_TLE75);
}
if (local_TLE76 != NULL)
{
zval_ptr_dtor (&local_TLE76);
}
if (local_TLE77 != NULL)
{
zval_ptr_dtor (&local_TLE77);
}
if (local_TLE78 != NULL)
{
zval_ptr_dtor (&local_TLE78);
}
if (local_TLE79 != NULL)
{
zval_ptr_dtor (&local_TLE79);
}
if (local_TLE80 != NULL)
{
zval_ptr_dtor (&local_TLE80);
}
if (local_TLE81 != NULL)
{
zval_ptr_dtor (&local_TLE81);
}
if (local_TLE82 != NULL)
{
zval_ptr_dtor (&local_TLE82);
}
if (local_TMIt122 != NULL)
{
zval_ptr_dtor (&local_TMIt122);
}
if (local_TMIt123 != NULL)
{
zval_ptr_dtor (&local_TMIt123);
}
if (local_touched != NULL)
{
zval_ptr_dtor (&local_touched);
}
if (local_wgCacheEpoch != NULL)
{
zval_ptr_dtor (&local_wgCacheEpoch);
}
}
// function addheaditem($section, $tag = False)
// {
// 	$TLE83 = False;
// 	$TLE84 = ($tag !== $TLE83);
// 	if (TLE84) goto L194 else goto L195;
// L194:
// 	$TSt85 =& $this->mHeadItems;
// 	$TSt85[$tag] = $section;
// 	goto L196;
// L195:
// 	$TSt86 =& $this->mHeadItems;
// 	$TSt86[] = $section;
// 	goto L196;
// L196:
// }
PHP_METHOD(ParserOutput, addheaditem)
{
zval* local_TLE83 = NULL;
zval* local_TLE84 = NULL;
zval* local_TSt85 = NULL;
zval* local_TSt86 = NULL;
zval* local_section = NULL;
zval* local_tag = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[2];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_section != NULL)
{
	zval_ptr_dtor (&local_section);
}
local_section = params[0];
// param 1
if (num_args <= 1)
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = False;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
default_value->refcount--;
	params[1] = default_value;
}
params[1]->refcount++;
if (local_tag != NULL)
{
	zval_ptr_dtor (&local_tag);
}
local_tag = params[1];
}
// Function body
// $TLE83 = False;
{
        if (local_TLE83 == NULL)
    {
      local_TLE83 = EG (uninitialized_zval_ptr);
      local_TLE83->refcount++;
    }
  zval** p_lhs = &local_TLE83;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
// $TLE84 = ($tag !== $TLE83);
{
    if (local_TLE84 == NULL)
    {
      local_TLE84 = EG (uninitialized_zval_ptr);
      local_TLE84->refcount++;
    }
  zval** p_lhs = &local_TLE84;

    zval* left;
  if (local_tag == NULL)
    left = EG (uninitialized_zval_ptr);
  else
    left = local_tag;

    zval* right;
  if (local_TLE83 == NULL)
    right = EG (uninitialized_zval_ptr);
  else
    right = local_TLE83;

  if (in_copy_on_write (*p_lhs))
    {
      zval_ptr_dtor (p_lhs);
      ALLOC_INIT_ZVAL (*p_lhs);
    }

  zval old = **p_lhs;
  int result_is_operand = (*p_lhs == left || *p_lhs == right);
  is_not_identical_function (*p_lhs, left, right TSRMLS_CC);

  // If the result is one of the operands, the operator function
  // will already have cleaned up the result
  if (!result_is_operand)
    zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE84) goto L194 else goto L195;
{
     zval* p_cond;
  if (local_TLE84 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE84;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L194;
   else
      goto L195;
phc_check_invariants (TSRMLS_C);
}
// L194:
L194:;
// $TSt85 =& $this->mHeadItems;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mHeadItems", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt85 == NULL)
    {
      local_TSt85 = EG (uninitialized_zval_ptr);
      local_TSt85->refcount++;
    }
  zval** p_lhs = &local_TSt85;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TSt85[$tag] = $section;
{
     if (local_TSt85 == NULL)
    {
      local_TSt85 = EG (uninitialized_zval_ptr);
      local_TSt85->refcount++;
    }
  zval** p_array = &local_TSt85;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_tag == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_tag;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_section == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_section;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_section == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_section;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// goto L196;
{
goto L196;
phc_check_invariants (TSRMLS_C);
}
// L195:
L195:;
// $TSt86 =& $this->mHeadItems;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mHeadItems", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt86 == NULL)
    {
      local_TSt86 = EG (uninitialized_zval_ptr);
      local_TSt86->refcount++;
    }
  zval** p_lhs = &local_TSt86;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TSt86[] = $section;
{
     if (local_TSt86 == NULL)
    {
      local_TSt86 = EG (uninitialized_zval_ptr);
      local_TSt86->refcount++;
    }
  zval** p_array = &local_TSt86;

   // Push EG(uninit) and get a pointer to the symtable entry
   zval** p_lhs = push_and_index_ht (p_array TSRMLS_CC);
   if (p_lhs != NULL)
   {
        zval* rhs;
  if (local_section == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_section;

      if (*p_lhs != rhs)
	 write_var (p_lhs, rhs);
   }
   // I think if this is NULL, then the LHS is a bool or similar, and you cant
   // push onto it.
phc_check_invariants (TSRMLS_C);
}
// goto L196;
{
goto L196;
phc_check_invariants (TSRMLS_C);
}
// L196:
L196:;
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE83 != NULL)
{
zval_ptr_dtor (&local_TLE83);
}
if (local_TLE84 != NULL)
{
zval_ptr_dtor (&local_TLE84);
}
if (local_TSt85 != NULL)
{
zval_ptr_dtor (&local_TSt85);
}
if (local_TSt86 != NULL)
{
zval_ptr_dtor (&local_TSt86);
}
if (local_section != NULL)
{
zval_ptr_dtor (&local_section);
}
if (local_tag != NULL)
{
zval_ptr_dtor (&local_tag);
}
}
// public function setdisplaytitle($text)
// {
// 	$this->displayTitle = $text;
// }
PHP_METHOD(ParserOutput, setdisplaytitle)
{
zval* local_text = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_text != NULL)
{
	zval_ptr_dtor (&local_text);
}
local_text = params[0];
}
// Function body
// $this->displayTitle = $text;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_text == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_text;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "displayTitle", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_text != NULL)
{
zval_ptr_dtor (&local_text);
}
}
// public function getdisplaytitle()
// {
// 	$TSt87 = $this->displayTitle;
// 	return $TSt87;
// }
PHP_METHOD(ParserOutput, getdisplaytitle)
{
zval* local_TSt87 = NULL;
zval* local_this = getThis();
// Function body
// $TSt87 = $this->displayTitle;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "displayTitle", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt87 == NULL)
    {
      local_TSt87 = EG (uninitialized_zval_ptr);
      local_TSt87->refcount++;
    }
  zval** p_lhs = &local_TSt87;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt87;
{
     zval* rhs;
  if (local_TSt87 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt87;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt87 != NULL)
{
zval_ptr_dtor (&local_TSt87);
}
}
// public function setflag($flag)
// {
// 	$TSt88 =& $this->mFlags;
// 	$TLE89 = True;
// 	$TSt88[$flag] = $TLE89;
// }
PHP_METHOD(ParserOutput, setflag)
{
zval* local_TLE89 = NULL;
zval* local_TSt88 = NULL;
zval* local_flag = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_flag != NULL)
{
	zval_ptr_dtor (&local_flag);
}
local_flag = params[0];
}
// Function body
// $TSt88 =& $this->mFlags;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mFlags", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt88 == NULL)
    {
      local_TSt88 = EG (uninitialized_zval_ptr);
      local_TSt88->refcount++;
    }
  zval** p_lhs = &local_TSt88;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TLE89 = True;
{
        if (local_TLE89 == NULL)
    {
      local_TLE89 = EG (uninitialized_zval_ptr);
      local_TLE89->refcount++;
    }
  zval** p_lhs = &local_TLE89;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 1);

phc_check_invariants (TSRMLS_C);
}
// $TSt88[$flag] = $TLE89;
{
     if (local_TSt88 == NULL)
    {
      local_TSt88 = EG (uninitialized_zval_ptr);
      local_TSt88->refcount++;
    }
  zval** p_array = &local_TSt88;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_flag == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_flag;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_TLE89 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE89;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_TLE89 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE89;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE89 != NULL)
{
zval_ptr_dtor (&local_TLE89);
}
if (local_TSt88 != NULL)
{
zval_ptr_dtor (&local_TSt88);
}
if (local_flag != NULL)
{
zval_ptr_dtor (&local_flag);
}
}
// public function getflag($flag)
// {
// 	$TMIt125 = $this->mFlags;
// 	$TLE90 = isset($TMIt125[$flag]);
// 	return $TLE90;
// }
PHP_METHOD(ParserOutput, getflag)
{
zval* local_TLE90 = NULL;
zval* local_TMIt125 = NULL;
zval* local_flag = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_flag != NULL)
{
	zval_ptr_dtor (&local_flag);
}
local_flag = params[0];
}
// Function body
// $TMIt125 = $this->mFlags;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mFlags", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt125 == NULL)
    {
      local_TMIt125 = EG (uninitialized_zval_ptr);
      local_TMIt125->refcount++;
    }
  zval** p_lhs = &local_TMIt125;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE90 = isset($TMIt125[$flag]);
{
if (local_TLE90 == NULL)
{
local_TLE90 = EG (uninitialized_zval_ptr);
local_TLE90->refcount++;
}
zval** p_lhs = &local_TLE90;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
if (local_TMIt125 == NULL)
{
local_TMIt125 = EG (uninitialized_zval_ptr);
local_TMIt125->refcount++;
}
zval** u_array = &local_TMIt125;
zval* u_index;
if (local_flag == NULL)
{
u_index = EG (uninitialized_zval_ptr);
}
else
{
u_index = local_flag;
}
ZVAL_BOOL(value, isset_array (u_array, u_index));
phc_check_invariants (TSRMLS_C);
}
// return $TLE90;
{
     zval* rhs;
  if (local_TLE90 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE90;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE90 != NULL)
{
zval_ptr_dtor (&local_TLE90);
}
if (local_TMIt125 != NULL)
{
zval_ptr_dtor (&local_TMIt125);
}
if (local_flag != NULL)
{
zval_ptr_dtor (&local_flag);
}
}
// public function setproperty($name, $value)
// {
// 	$TSt91 =& $this->mProperties;
// 	$TSt91[$name] = $value;
// }
PHP_METHOD(ParserOutput, setproperty)
{
zval* local_TSt91 = NULL;
zval* local_name = NULL;
zval* local_this = getThis();
zval* local_value = NULL;
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[2];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_name != NULL)
{
	zval_ptr_dtor (&local_name);
}
local_name = params[0];
// param 1
params[1]->refcount++;
if (local_value != NULL)
{
	zval_ptr_dtor (&local_value);
}
local_value = params[1];
}
// Function body
// $TSt91 =& $this->mProperties;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mProperties", 0);

	zval** field = Z_OBJ_HT_PP(p_obj)->get_property_ptr_ptr(*p_obj, &field_name TSRMLS_CC);
	sep_copy_on_write (field);
	  if (local_TSt91 == NULL)
    {
      local_TSt91 = EG (uninitialized_zval_ptr);
      local_TSt91->refcount++;
    }
  zval** p_lhs = &local_TSt91;

	copy_into_ref (p_lhs, field);
phc_check_invariants (TSRMLS_C);
}
// $TSt91[$name] = $value;
{
     if (local_TSt91 == NULL)
    {
      local_TSt91 = EG (uninitialized_zval_ptr);
      local_TSt91->refcount++;
    }
  zval** p_array = &local_TSt91;

   check_array_type (p_array TSRMLS_CC);

     zval* index;
  if (local_name == NULL)
    index = EG (uninitialized_zval_ptr);
  else
    index = local_name;


   // String indexing
   if (Z_TYPE_PP (p_array) == IS_STRING && Z_STRLEN_PP (p_array) > 0)
   {
        zval* rhs;
  if (local_value == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_value;

      write_string_index (p_array, index, rhs TSRMLS_CC);
   }
   else if (Z_TYPE_PP (p_array) == IS_ARRAY)
   {
      zval** p_lhs = get_ht_entry (p_array, index TSRMLS_CC);
        zval* rhs;
  if (local_value == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_value;

      if (*p_lhs != rhs)
      {
	 write_var (p_lhs, rhs);
      }
   }
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TSt91 != NULL)
{
zval_ptr_dtor (&local_TSt91);
}
if (local_name != NULL)
{
zval_ptr_dtor (&local_name);
}
if (local_value != NULL)
{
zval_ptr_dtor (&local_value);
}
}
// public function getproperty($name)
// {
// 	$TMIt126 = $this->mProperties;
// 	$TLE92 = isset($TMIt126[$name]);
// 	if (TLE92) goto L197 else goto L198;
// L197:
// 	$TSt93 = $this->mProperties;
// 	$TSi94 = $TSt93[$name];
// 	$TEF8 = $TSi94;
// 	goto L199;
// L198:
// 	$TEF8 = False;
// 	goto L199;
// L199:
// 	return $TEF8;
// }
PHP_METHOD(ParserOutput, getproperty)
{
zval* local_TEF8 = NULL;
zval* local_TLE92 = NULL;
zval* local_TMIt126 = NULL;
zval* local_TSi94 = NULL;
zval* local_TSt93 = NULL;
zval* local_name = NULL;
zval* local_this = getThis();
// Add all parameters as local variables
{
int num_args = ZEND_NUM_ARGS ();
zval* params[1];
zend_get_parameters_array(0, num_args, params);
// param 0
params[0]->refcount++;
if (local_name != NULL)
{
	zval_ptr_dtor (&local_name);
}
local_name = params[0];
}
// Function body
// $TMIt126 = $this->mProperties;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mProperties", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt126 == NULL)
    {
      local_TMIt126 = EG (uninitialized_zval_ptr);
      local_TMIt126->refcount++;
    }
  zval** p_lhs = &local_TMIt126;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE92 = isset($TMIt126[$name]);
{
if (local_TLE92 == NULL)
{
local_TLE92 = EG (uninitialized_zval_ptr);
local_TLE92->refcount++;
}
zval** p_lhs = &local_TLE92;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
if (local_TMIt126 == NULL)
{
local_TMIt126 = EG (uninitialized_zval_ptr);
local_TMIt126->refcount++;
}
zval** u_array = &local_TMIt126;
zval* u_index;
if (local_name == NULL)
{
u_index = EG (uninitialized_zval_ptr);
}
else
{
u_index = local_name;
}
ZVAL_BOOL(value, isset_array (u_array, u_index));
phc_check_invariants (TSRMLS_C);
}
// if (TLE92) goto L197 else goto L198;
{
     zval* p_cond;
  if (local_TLE92 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE92;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L197;
   else
      goto L198;
phc_check_invariants (TSRMLS_C);
}
// L197:
L197:;
// $TSt93 = $this->mProperties;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mProperties", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt93 == NULL)
    {
      local_TSt93 = EG (uninitialized_zval_ptr);
      local_TSt93->refcount++;
    }
  zval** p_lhs = &local_TSt93;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TSi94 = $TSt93[$name];
{
     if (local_TSi94 == NULL)
    {
      local_TSi94 = EG (uninitialized_zval_ptr);
      local_TSi94->refcount++;
    }
  zval** p_lhs = &local_TSi94;

     zval* r_array;
  if (local_TSt93 == NULL)
    r_array = EG (uninitialized_zval_ptr);
  else
    r_array = local_TSt93;

     zval* r_index;
  if (local_name == NULL)
    r_index = EG (uninitialized_zval_ptr);
  else
    r_index = local_name;


   zval* rhs;
   int is_rhs_new = 0;
    if (Z_TYPE_P (r_array) != IS_ARRAY)
    {
      if (Z_TYPE_P (r_array) == IS_STRING)
	{
	  is_rhs_new = 1;
	  rhs = read_string_index (r_array, r_index TSRMLS_CC);
	}
      else
	// TODO: warning here?
	rhs = EG (uninitialized_zval_ptr);
    }
    else
    {
      if (check_array_index_type (r_index TSRMLS_CC))
	{
	  // Read array variable
	  read_array (&rhs, r_array, r_index TSRMLS_CC);
	}
      else
	rhs = *p_lhs; // HACK to fail  *p_lhs != rhs
    }

   if (*p_lhs != rhs)
      write_var (p_lhs, rhs);

   if (is_rhs_new) zval_ptr_dtor (&rhs);
phc_check_invariants (TSRMLS_C);
}
// $TEF8 = $TSi94;
{
    if (local_TEF8 == NULL)
    {
      local_TEF8 = EG (uninitialized_zval_ptr);
      local_TEF8->refcount++;
    }
  zval** p_lhs = &local_TEF8;

    zval* rhs;
  if (local_TSi94 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSi94;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
// goto L199;
{
goto L199;
phc_check_invariants (TSRMLS_C);
}
// L198:
L198:;
// $TEF8 = False;
{
        if (local_TEF8 == NULL)
    {
      local_TEF8 = EG (uninitialized_zval_ptr);
      local_TEF8->refcount++;
    }
  zval** p_lhs = &local_TEF8;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
// goto L199;
{
goto L199;
phc_check_invariants (TSRMLS_C);
}
// L199:
L199:;
// return $TEF8;
{
     zval* rhs;
  if (local_TEF8 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TEF8;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TEF8 != NULL)
{
zval_ptr_dtor (&local_TEF8);
}
if (local_TLE92 != NULL)
{
zval_ptr_dtor (&local_TLE92);
}
if (local_TMIt126 != NULL)
{
zval_ptr_dtor (&local_TMIt126);
}
if (local_TSi94 != NULL)
{
zval_ptr_dtor (&local_TSi94);
}
if (local_TSt93 != NULL)
{
zval_ptr_dtor (&local_TSt93);
}
if (local_name != NULL)
{
zval_ptr_dtor (&local_name);
}
}
// public function getproperties()
// {
// 	$TMIt127 = $this->mProperties;
// 	$TLE95 = isset($TMIt127);
// 	$TLE96 = !$TLE95;
// 	if (TLE96) goto L200 else goto L201;
// L200:
// 	unset($TSa97);
// 	$TSa97 = (array) $TSa97;
// 	$this->mProperties = $TSa97;
// 	goto L202;
// L201:
// 	goto L202;
// L202:
// 	$TSt98 = $this->mProperties;
// 	return $TSt98;
// }
PHP_METHOD(ParserOutput, getproperties)
{
zval* local_TLE95 = NULL;
zval* local_TLE96 = NULL;
zval* local_TMIt127 = NULL;
zval* local_TSa97 = NULL;
zval* local_TSt98 = NULL;
zval* local_this = getThis();
// Function body
// $TMIt127 = $this->mProperties;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mProperties", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TMIt127 == NULL)
    {
      local_TMIt127 = EG (uninitialized_zval_ptr);
      local_TMIt127->refcount++;
    }
  zval** p_lhs = &local_TMIt127;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// $TLE95 = isset($TMIt127);
{
if (local_TLE95 == NULL)
{
local_TLE95 = EG (uninitialized_zval_ptr);
local_TLE95->refcount++;
}
zval** p_lhs = &local_TLE95;
zval* value;
if ((*p_lhs)->is_ref)
{
  // Always overwrite the current value
  value = *p_lhs;
  zval_dtor (value);
}
else
{
	ALLOC_INIT_ZVAL (value);
	zval_ptr_dtor (p_lhs);
	*p_lhs = value;
}
ZVAL_BOOL(value, local_TMIt127 != NULL && !ZVAL_IS_NULL(local_TMIt127));
phc_check_invariants (TSRMLS_C);
}
// $TLE96 = !$TLE95;
{
     if (local_TLE96 == NULL)
    {
      local_TLE96 = EG (uninitialized_zval_ptr);
      local_TLE96->refcount++;
    }
  zval** p_lhs = &local_TLE96;

     zval* rhs;
  if (local_TLE95 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TLE95;

   if (in_copy_on_write (*p_lhs))
   {
     zval_ptr_dtor (p_lhs);
     ALLOC_INIT_ZVAL (*p_lhs);
   }

   zval old = **p_lhs;
   int result_is_operand = (*p_lhs == rhs);
   boolean_not_function (*p_lhs, rhs TSRMLS_CC);
   if (!result_is_operand)
	zval_dtor (&old);
phc_check_invariants (TSRMLS_C);
}
// if (TLE96) goto L200 else goto L201;
{
     zval* p_cond;
  if (local_TLE96 == NULL)
    p_cond = EG (uninitialized_zval_ptr);
  else
    p_cond = local_TLE96;

   zend_bool bcond = zend_is_true (p_cond);
   if (bcond)
      goto L200;
   else
      goto L201;
phc_check_invariants (TSRMLS_C);
}
// L200:
L200:;
// unset($TSa97);
{
if (local_TSa97 != NULL)
{
zval_ptr_dtor (&local_TSa97);
local_TSa97 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa97 = (array) $TSa97;
{
      if (local_TSa97 == NULL)
    {
      local_TSa97 = EG (uninitialized_zval_ptr);
      local_TSa97->refcount++;
    }
  zval** p_lhs = &local_TSa97;

    zval* rhs;
  if (local_TSa97 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa97;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $this->mProperties = $TSa97;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

	  zval* rhs;
  if (local_TSa97 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa97;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mProperties", 0);

	Z_OBJ_HT_PP(p_obj)->write_property(*p_obj, &field_name, rhs TSRMLS_CC);
phc_check_invariants (TSRMLS_C);
}
// goto L202;
{
goto L202;
phc_check_invariants (TSRMLS_C);
}
// L201:
L201:;
// goto L202;
{
goto L202;
phc_check_invariants (TSRMLS_C);
}
// L202:
L202:;
// $TSt98 = $this->mProperties;
{
	  if (local_this == NULL)
    {
      local_this = EG (uninitialized_zval_ptr);
      local_this->refcount++;
    }
  zval** p_obj = &local_this;

		zval field_name;
	INIT_ZVAL (field_name);
	ZVAL_STRING (&field_name, "mProperties", 0);

	// I *think* this is correct, but documentation of the Zend API is scarce :)
	zval* field = Z_OBJ_HT_PP(p_obj)->read_property(*p_obj, &field_name, BP_VAR_R TSRMLS_CC);
	  if (local_TSt98 == NULL)
    {
      local_TSt98 = EG (uninitialized_zval_ptr);
      local_TSt98->refcount++;
    }
  zval** p_lhs = &local_TSt98;

	write_var (p_lhs, field); 
phc_check_invariants (TSRMLS_C);
}
// return $TSt98;
{
     zval* rhs;
  if (local_TSt98 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSt98;

   // Run-time return by reference has different semantics to compile-time.
   // If the function has CTRBR and RTRBR, the the assignment will be
   // reference. If one or the other is return-by-copy, the result will be
   // by copy. Its a question of whether its separated at return-time (which
   // we do here) or at the call-site.
   return_value->value = rhs->value;
   return_value->type = rhs->type;
   zval_copy_ctor (return_value);
   goto end_of_function;
phc_check_invariants (TSRMLS_C);
}
// Method exit
end_of_function:__attribute__((unused));
if (local_TLE95 != NULL)
{
zval_ptr_dtor (&local_TLE95);
}
if (local_TLE96 != NULL)
{
zval_ptr_dtor (&local_TLE96);
}
if (local_TMIt127 != NULL)
{
zval_ptr_dtor (&local_TMIt127);
}
if (local_TSa97 != NULL)
{
zval_ptr_dtor (&local_TSa97);
}
if (local_TSt98 != NULL)
{
zval_ptr_dtor (&local_TSt98);
}
}
// ArgInfo structures (necessary to support compile time pass-by-reference)
ZEND_BEGIN_ARG_INFO_EX(ParserOutput_parseroutput_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "text")
ZEND_ARG_INFO(0, "languageLinks")
ZEND_ARG_INFO(0, "categoryLinks")
ZEND_ARG_INFO(0, "containsOldMagic")
ZEND_ARG_INFO(0, "titletext")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_gettext_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getlanguagelinks_arg_info, 0, 1, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getcategorylinks_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getcategories_arg_info, 0, 1, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getcachetime_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_gettitletext_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getsections_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getlinks_arg_info, 0, 1, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_gettemplates_arg_info, 0, 1, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getimages_arg_info, 0, 1, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getexternallinks_arg_info, 0, 1, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getnogallery_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getsubtitle_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getoutputhooks_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getwarnings_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getindexpolicy_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_containsoldmagic_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_settext_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "text")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_setlanguagelinks_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "ll")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_setcategorylinks_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "cl")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_setcontainsoldmagic_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "com")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_setcachetime_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "t")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_settitletext_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "t")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_setsections_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "toc")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_setindexpolicy_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "policy")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_addcategory_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "c")
ZEND_ARG_INFO(0, "sort")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_addlanguagelink_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "t")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_addexternallink_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "url")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_addwarning_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "s")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_addoutputhook_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "hook")
ZEND_ARG_INFO(0, "data")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_setnewsection_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "value")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getnewsection_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_addlink_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "title")
ZEND_ARG_INFO(0, "id")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_addimage_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "name")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_addtemplate_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "title")
ZEND_ARG_INFO(0, "page_id")
ZEND_ARG_INFO(0, "rev_id")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_expired_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "touched")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_addheaditem_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "section")
ZEND_ARG_INFO(0, "tag")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_setdisplaytitle_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "text")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getdisplaytitle_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_setflag_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "flag")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getflag_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "flag")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_setproperty_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "name")
ZEND_ARG_INFO(0, "value")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getproperty_arg_info, 0, 0, 0)
ZEND_ARG_INFO(0, "name")
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(ParserOutput_getproperties_arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

static function_entry ParserOutput_functions[] = {
PHP_ME(ParserOutput, parseroutput, ParserOutput_parseroutput_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, gettext, ParserOutput_gettext_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getlanguagelinks, ParserOutput_getlanguagelinks_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getcategorylinks, ParserOutput_getcategorylinks_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getcategories, ParserOutput_getcategories_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getcachetime, ParserOutput_getcachetime_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, gettitletext, ParserOutput_gettitletext_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getsections, ParserOutput_getsections_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getlinks, ParserOutput_getlinks_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, gettemplates, ParserOutput_gettemplates_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getimages, ParserOutput_getimages_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getexternallinks, ParserOutput_getexternallinks_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getnogallery, ParserOutput_getnogallery_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getsubtitle, ParserOutput_getsubtitle_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getoutputhooks, ParserOutput_getoutputhooks_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getwarnings, ParserOutput_getwarnings_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getindexpolicy, ParserOutput_getindexpolicy_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, containsoldmagic, ParserOutput_containsoldmagic_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, settext, ParserOutput_settext_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, setlanguagelinks, ParserOutput_setlanguagelinks_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, setcategorylinks, ParserOutput_setcategorylinks_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, setcontainsoldmagic, ParserOutput_setcontainsoldmagic_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, setcachetime, ParserOutput_setcachetime_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, settitletext, ParserOutput_settitletext_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, setsections, ParserOutput_setsections_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, setindexpolicy, ParserOutput_setindexpolicy_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, addcategory, ParserOutput_addcategory_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, addlanguagelink, ParserOutput_addlanguagelink_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, addexternallink, ParserOutput_addexternallink_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, addwarning, ParserOutput_addwarning_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, addoutputhook, ParserOutput_addoutputhook_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, setnewsection, ParserOutput_setnewsection_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getnewsection, ParserOutput_getnewsection_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, addlink, ParserOutput_addlink_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, addimage, ParserOutput_addimage_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, addtemplate, ParserOutput_addtemplate_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, expired, ParserOutput_expired_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, addheaditem, ParserOutput_addheaditem_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, setdisplaytitle, ParserOutput_setdisplaytitle_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getdisplaytitle, ParserOutput_getdisplaytitle_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, setflag, ParserOutput_setflag_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getflag, ParserOutput_getflag_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, setproperty, ParserOutput_setproperty_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getproperty, ParserOutput_getproperty_arg_info, ZEND_ACC_PUBLIC)
PHP_ME(ParserOutput, getproperties, ParserOutput_getproperties_arg_info, ZEND_ACC_PUBLIC)
{ NULL, NULL, NULL }
};
// function __MAIN__()
// {
// }
PHP_FUNCTION(__MAIN__)
{
// Function body
// Method exit
end_of_function:__attribute__((unused));
}
// Module initialization
PHP_MINIT_FUNCTION(app)
{
{
zend_class_entry ce; // temp
zend_class_entry* ce_reg; // once registered, ce_ptr should be used
INIT_CLASS_ENTRY(ce, "ParserOutput", ParserOutput_functions);
ce_reg = zend_register_internal_class(&ce TSRMLS_CC);
ce_reg->type &= ~ZEND_INTERNAL_CLASS;
{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mText", 5, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mLanguageLinks", 14, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mCategories", 11, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mContainsOldMagic", 17, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = NULL;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_NULL (value);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mTitleText", 10, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = '';
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mCacheTime", 10, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = Parser::VERSION;
{
   // No null-terminator in length for get_constant.
   // zend_get_constant always returns a copy of the constant.
     if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   if (!(*p_lhs)->is_ref)
   {
        zval_ptr_dtor (p_lhs);
   get_constant ("Parser::VERSION", 15, p_lhs TSRMLS_CC);

   }
   else
   {
         zval* constant;
    get_constant ("Parser::VERSION", 15, p_lhs TSRMLS_CC);
    overwrite_lhs_no_copy (*p_lhs, constant);
    safe_free_zval_ptr (constant);

   }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mVersion", 8, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local_TSa205 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa205);
{
if (local_TSa205 != NULL)
{
zval_ptr_dtor (&local_TSa205);
local_TSa205 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa205 = (array) $TSa205;
{
      if (local_TSa205 == NULL)
    {
      local_TSa205 = EG (uninitialized_zval_ptr);
      local_TSa205->refcount++;
    }
  zval** p_lhs = &local_TSa205;

    zval* rhs;
  if (local_TSa205 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa205;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa205;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa205 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa205;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa205 != NULL)
{
zval_ptr_dtor (&local_TSa205);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mLinks", 6, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local_TSa206 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa206);
{
if (local_TSa206 != NULL)
{
zval_ptr_dtor (&local_TSa206);
local_TSa206 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa206 = (array) $TSa206;
{
      if (local_TSa206 == NULL)
    {
      local_TSa206 = EG (uninitialized_zval_ptr);
      local_TSa206->refcount++;
    }
  zval** p_lhs = &local_TSa206;

    zval* rhs;
  if (local_TSa206 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa206;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa206;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa206 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa206;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa206 != NULL)
{
zval_ptr_dtor (&local_TSa206);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mTemplates", 10, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local_TSa207 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa207);
{
if (local_TSa207 != NULL)
{
zval_ptr_dtor (&local_TSa207);
local_TSa207 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa207 = (array) $TSa207;
{
      if (local_TSa207 == NULL)
    {
      local_TSa207 = EG (uninitialized_zval_ptr);
      local_TSa207->refcount++;
    }
  zval** p_lhs = &local_TSa207;

    zval* rhs;
  if (local_TSa207 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa207;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa207;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa207 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa207;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa207 != NULL)
{
zval_ptr_dtor (&local_TSa207);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mTemplateIds", 12, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local_TSa208 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa208);
{
if (local_TSa208 != NULL)
{
zval_ptr_dtor (&local_TSa208);
local_TSa208 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa208 = (array) $TSa208;
{
      if (local_TSa208 == NULL)
    {
      local_TSa208 = EG (uninitialized_zval_ptr);
      local_TSa208->refcount++;
    }
  zval** p_lhs = &local_TSa208;

    zval* rhs;
  if (local_TSa208 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa208;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa208;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa208 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa208;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa208 != NULL)
{
zval_ptr_dtor (&local_TSa208);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mImages", 7, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local_TSa209 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa209);
{
if (local_TSa209 != NULL)
{
zval_ptr_dtor (&local_TSa209);
local_TSa209 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa209 = (array) $TSa209;
{
      if (local_TSa209 == NULL)
    {
      local_TSa209 = EG (uninitialized_zval_ptr);
      local_TSa209->refcount++;
    }
  zval** p_lhs = &local_TSa209;

    zval* rhs;
  if (local_TSa209 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa209;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa209;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa209 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa209;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa209 != NULL)
{
zval_ptr_dtor (&local_TSa209);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mExternalLinks", 14, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = False;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mNewSection", 11, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = False;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mNoGallery", 10, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local_TSa210 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa210);
{
if (local_TSa210 != NULL)
{
zval_ptr_dtor (&local_TSa210);
local_TSa210 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa210 = (array) $TSa210;
{
      if (local_TSa210 == NULL)
    {
      local_TSa210 = EG (uninitialized_zval_ptr);
      local_TSa210->refcount++;
    }
  zval** p_lhs = &local_TSa210;

    zval* rhs;
  if (local_TSa210 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa210;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa210;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa210 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa210;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa210 != NULL)
{
zval_ptr_dtor (&local_TSa210);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mHeadItems", 10, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local_TSa211 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa211);
{
if (local_TSa211 != NULL)
{
zval_ptr_dtor (&local_TSa211);
local_TSa211 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa211 = (array) $TSa211;
{
      if (local_TSa211 == NULL)
    {
      local_TSa211 = EG (uninitialized_zval_ptr);
      local_TSa211->refcount++;
    }
  zval** p_lhs = &local_TSa211;

    zval* rhs;
  if (local_TSa211 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa211;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa211;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa211 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa211;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa211 != NULL)
{
zval_ptr_dtor (&local_TSa211);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mOutputHooks", 12, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local_TSa212 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa212);
{
if (local_TSa212 != NULL)
{
zval_ptr_dtor (&local_TSa212);
local_TSa212 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa212 = (array) $TSa212;
{
      if (local_TSa212 == NULL)
    {
      local_TSa212 = EG (uninitialized_zval_ptr);
      local_TSa212->refcount++;
    }
  zval** p_lhs = &local_TSa212;

    zval* rhs;
  if (local_TSa212 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa212;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa212;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa212 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa212;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa212 != NULL)
{
zval_ptr_dtor (&local_TSa212);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mWarnings", 9, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local_TSa213 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa213);
{
if (local_TSa213 != NULL)
{
zval_ptr_dtor (&local_TSa213);
local_TSa213 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa213 = (array) $TSa213;
{
      if (local_TSa213 == NULL)
    {
      local_TSa213 = EG (uninitialized_zval_ptr);
      local_TSa213->refcount++;
    }
  zval** p_lhs = &local_TSa213;

    zval* rhs;
  if (local_TSa213 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa213;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa213;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa213 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa213;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa213 != NULL)
{
zval_ptr_dtor (&local_TSa213);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mSections", 9, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local_TSa214 = NULL;
zval* local___static_value__ = NULL;
// unset($TSa214);
{
if (local_TSa214 != NULL)
{
zval_ptr_dtor (&local_TSa214);
local_TSa214 = NULL;
}
phc_check_invariants (TSRMLS_C);
}
// $TSa214 = (array) $TSa214;
{
      if (local_TSa214 == NULL)
    {
      local_TSa214 = EG (uninitialized_zval_ptr);
      local_TSa214->refcount++;
    }
  zval** p_lhs = &local_TSa214;

    zval* rhs;
  if (local_TSa214 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa214;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }

    assert (IS_ARRAY >= 0 && IS_ARRAY <= 6);
  if ((*p_lhs)->type != IS_ARRAY)
  {
    sep_copy_on_write (p_lhs);
    convert_to_array (*p_lhs);
  }

phc_check_invariants (TSRMLS_C);
}
// $__static_value__ = $TSa214;
{
    if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

    zval* rhs;
  if (local_TSa214 == NULL)
    rhs = EG (uninitialized_zval_ptr);
  else
    rhs = local_TSa214;

  if (*p_lhs != rhs)
    {
        if ((*p_lhs)->is_ref)
      overwrite_lhs (*p_lhs, rhs);
  else
    {
      zval_ptr_dtor (p_lhs);
        if (rhs->is_ref)
    {
      // Take a copy of RHS for LHS
      *p_lhs = zvp_clone_ex (rhs);
    }
  else
    {
      // Share a copy
      rhs->refcount++;
      *p_lhs = rhs;
    }

    }

    }
phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local_TSa214 != NULL)
{
zval_ptr_dtor (&local_TSa214);
}
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mProperties", 11, default_value, ZEND_ACC_PUBLIC TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = '';
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_STRINGL(value, "", 0, 1);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "mIndexPolicy", 12, default_value, ZEND_ACC_PRIVATE TSRMLS_CC);}{
zval* default_value;
{
zval* local___static_value__ = NULL;
// $__static_value__ = False;
{
        if (local___static_value__ == NULL)
    {
      local___static_value__ = EG (uninitialized_zval_ptr);
      local___static_value__->refcount++;
    }
  zval** p_lhs = &local___static_value__;

   zval* value;
   if ((*p_lhs)->is_ref)
   {
     // Always overwrite the current value
     value = *p_lhs;
     zval_dtor (value);
   }
   else
   {
     ALLOC_INIT_ZVAL (value);
     zval_ptr_dtor (p_lhs);
     *p_lhs = value;
   }

   ZVAL_BOOL (value, 0);

phc_check_invariants (TSRMLS_C);
}
default_value = local___static_value__;
assert(!default_value->is_ref);
default_value->refcount++;
if (local___static_value__ != NULL)
{
zval_ptr_dtor (&local___static_value__);
}
}
phc_declare_property(ce_reg, "displayTitle", 12, default_value, ZEND_ACC_PRIVATE TSRMLS_CC);}}return SUCCESS;}// ArgInfo structures (necessary to support compile time pass-by-reference)
ZEND_BEGIN_ARG_INFO_EX(app___MAIN___arg_info, 0, 0, 0)
ZEND_END_ARG_INFO()

static function_entry app_functions[] = {
PHP_FE(__MAIN__, app___MAIN___arg_info)
{ NULL, NULL, NULL }
};
// Register the module itself with PHP
zend_module_entry app_module_entry = {
STANDARD_MODULE_HEADER, 
"app",
app_functions,
PHP_MINIT(app), /* MINIT */
NULL, /* MSHUTDOWN */
NULL, /* RINIT */
NULL, /* RSHUTDOWN */
NULL, /* MINFO */
"1.0",
STANDARD_MODULE_PROPERTIES
};
#include <sapi/embed/php_embed.h>
#include <signal.h>

void sighandler(int signum)
{
	switch(signum)
	{
		case SIGABRT:
			printf("SIGABRT received!\n");
			break;
		case SIGSEGV:
			printf("SIGSEGV received!\n");
			break;
		default:
			printf("Unknown signal received!\n");
			break;
	}

	printf("This could be a bug in phc. If you suspect it is, please email\n");
	printf("a bug report to phc-general@phpcompiler.org.\n");
	exit(-1);
}

int
main (int argc, char* argv[])
{
   int phc_exit_status;
   signal(SIGABRT, sighandler);
   signal(SIGSEGV, sighandler);

   TSRMLS_D;
   int dealloc_pools = 1;
   php_embed_init (argc, argv PTSRMLS_CC);
   zend_first_try
   {

      // initialize the phc runtime
      init_runtime();

      // load the compiled extension
      zend_startup_module (&app_module_entry);

      zval main_name;
      ZVAL_STRING (&main_name, "__MAIN__", NULL);

      zval retval;

      // Use standard errors, on stdout
      zend_alter_ini_entry ("report_zend_debug", sizeof("report_zend_debug"), "0", sizeof("0") - 1, PHP_INI_ALL, PHP_INI_STAGE_RUNTIME);
      zend_alter_ini_entry ("display_startup_errors", sizeof("display_startup_errors"), "1", sizeof("1") - 1, PHP_INI_ALL, PHP_INI_STAGE_RUNTIME);

      // initialize all the constants
saved_refcount = 0;

      // call __MAIN__
      int success = call_user_function( 
                   EG (function_table),
                   NULL,
                   &main_name,
                   &retval,
                   0,
                   NULL
                   TSRMLS_CC);

      assert (success == SUCCESS);

      // finalize the runtime
      finalize_runtime();

   }
   zend_catch
   {
		dealloc_pools = 0;
   }
   zend_end_try ();
   if (dealloc_pools)
   {
   }
   phc_exit_status = EG(exit_status);
   php_embed_shutdown (TSRMLS_C);

  return phc_exit_status;
}
