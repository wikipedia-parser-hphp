
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

CVarRef GlobalVariables::getRefByIdx(ssize_t idx, Variant &k) {
  DECLARE_GLOBAL_VARIABLES(g);
  static const char *names[] = {
    "argc",
    "argv",
    "_SERVER",
    "_GET",
    "_POST",
    "_COOKIE",
    "_FILES",
    "_ENV",
    "_REQUEST",
    "_SESSION",
    "HTTP_RAW_POST_DATA",
    "http_response_header",
    "wgMessageCache",
    "wgStyleDirectory",
    "wgRequest",
    "wgUser",
    "wgLang",
    "wgOut",
    "wgParserConf",
    "wgParser",
    "wgDefaultUserOptions",
    "wgNamespacesToBeSearchedDefault",
    "wgIP",
    "wgUsePrivateIPs",
    "wgSquidServers",
    "wgSquidServersNoPurge",
    "wgBlockOpenProxies",
    "wgProxyPorts",
    "wgProxyScriptPath",
    "wgMemc",
    "wgProxyMemcExpiry",
    "wgProxyKey",
    "wgProxyList",
    "wgCanonicalNamespaceNames",
    "wgExtraNamespaces",
    "wgAllowImageMoving",
    "wgContentNamespaces",
    "wgNamespacesWithSubpages",
    "wgContLang",
    "wgThumbLimits",
    "wgThumbUpright",
    "wgStylePath",
    "wgEnableUploads",
    "wgDisableAnonTalk",
    "wgSysopUserBans",
    "wgJsMimeType",
    "wgTitle",
    "wgArticle",
    "wgScript",
    "wgContLanguageCode",
    "wgMimeType",
    "wgOutputEncoding",
    "wgXhtmlDefaultNamespace",
    "wgXhtmlNamespaces",
    "wgDisableCounters",
    "wgLogo",
    "wgHideInterlanguageLinks",
    "wgMaxCredits",
    "wgShowCreditsIfMax",
    "wgPageShowWatchingUsers",
    "wgUseTrackbacks",
    "wgUseSiteJs",
    "wgArticlePath",
    "wgScriptPath",
    "wgServer",
    "wgDisableLangConversion",
    "wgUploadNavigationUrl",
    "wgHandheldStyle",
    "wgStyleVersion",
    "wgUseTwoButtonsSearchForm",
    "wgValidSkinNames",
    "wgSkipSkins",
    "wgDefaultSkin",
    "wgFavicon",
    "wgAppleTouchIcon",
    "wgEnableDublinCoreRdf",
    "wgEnableCreativeCommonsRdf",
    "wgRightsPage",
    "wgRightsUrl",
    "wgDebugComments",
    "wgBreakFrames",
    "wgVariantArticlePath",
    "wgActionPaths",
    "wgUseAjax",
    "wgAjaxWatch",
    "wgVersion",
    "wgEnableAPI",
    "wgEnableWriteAPI",
    "wgRestrictionTypes",
    "wgLivePreview",
    "wgMWSuggestTemplate",
    "wgDBname",
    "wgEnableMWSuggest",
    "wgAllowUserCss",
    "wgUseSiteCss",
    "wgSquidMaxage",
    "wgUseCategoryBrowser",
    "wgShowDebug",
    "action",
    "wgFeedClasses",
    "wgExtraSubtitle",
    "wgShowIPinHeader",
    "wgRightsText",
    "wgRightsIcon",
    "wgCopyrightIcon",
    "wgRedirectScript",
    "parserMemc",
    "wgEnableSidebarCache",
    "wgSidebarCacheExpiry",
    "wgAntiLockFlags",
    "wgAutoblockExpiry",
    "wgMaxNameChars",
    "wgReservedUsernames",
    "wgMinimalPasswordLength",
    "wgAuth",
    "wgCookiePrefix",
    "wgUseRCPatrol",
    "wgEnableSorbs",
    "wgProxyWhitelist",
    "wgSorbsUrl",
    "wgRateLimitsExcludedGroups",
    "wgRateLimitsExcludedIPs",
    "wgRateLimits",
    "wgRateLimitLog",
    "wgBlockAllowsUTEdit",
    "wgClockSkewFudge",
    "wgSecretKey",
    "wgPasswordReminderResendTime",
    "wgUseNPPatrol",
    "wgAllowUserSkin",
    "wgUseEnotif",
    "wgShowUpdatedMarker",
    "wgUseDynamicDates",
    "wgRenderHashAppend",
    "wgActiveUserEditCount",
    "wgActiveUserDays",
    "wgNewPasswordExpiry",
    "wgPasswordSender",
    "wgEnableEmail",
    "wgEnableUserEmail",
    "wgEmailAuthentication",
    "wgGroupPermissions",
    "wgAvailableRights",
    "wgImplicitGroups",
    "wgPasswordSalt",
    "wgNewUserLog",
    "wgCommandLineMode",
    "wgUseTeX",
    "wgInterwikiMagic",
    "wgAllowExternalImages",
    "wgAllowExternalImagesFrom",
    "wgEnableImageWhitelist",
    "wgAllowSpecialInclusion",
    "wgMaxArticleSize",
    "wgMaxPPNodeCount",
    "wgMaxTemplateDepth",
    "wgMaxPPExpandDepth",
    "wgCleanSignatures",
    "wgExternalLinkTarget",
    "wgProfiler",
    "wgDebugFunctionEntry",
    "wgDebugProfiling",
    "wgProfileToDatabase",
    "wgLanguageNames",
    "wgInputEncoding",
    "IP",
    "wgExtraLanguageNames",
    "wgLocalTZoffset",
    "wgDBtype",
    "wgEditEncoding",
    "wgExtensionAliasesFiles",
    "wgTranslateNumerals",
    "wgGrammarForms",
    "wgEnableSerializedMessages",
    "wgCheckSerialized",
    "wgMetaNamespace",
    "wgMetaNamespaceTalk",
    "wgNamespaceAliases",
    "wgAmericanDates",
    "wgFullyInitialised",
    "wgExceptionHooks",
    "wgShowExceptionDetails",
    "wgSitename",
    "utfCombiningClass",
    "utfCanonicalComp",
    "utfCanonicalDecomp",
    "utfCompatibilityDecomp",
    "utfCheckNFC",
    "attrib",
    "space",
    "wgHtmlEntities",
    "wgHtmlEntityAliases",
    "wgUseTidy",
    "wgEnforceHtmlIds",
    "wgLegacyEncoding",
    "wgCompressRevisions",
    "wgDefaultExternalStore",
    "wgRevisionCacheExpiry",
    "wgCacheEpoch",
    "wgLocalInterwiki",
    "wgPutIPinRC",
    "wgRC2UDPAddress",
    "wgRC2UDPOmitBots",
    "wgRC2UDPPrefix",
    "wgRC2UDPPort",
    "wgLogRestrictions",
    "wgRC2UDPInterwikiPrefix",
    "wgUseDumbLinkUpdate",
    "wgUpdateRowsPerJob",
    "wgPagePropLinkInvalidations",
    "wgUseCommaCount",
    "wgEnableParserCache",
    "wgNamespaceRobotPolicies",
    "wgArticleRobotPolicies",
    "wgDefaultRobotPolicy",
    "wgUseETag",
    "wgRedirectSources",
    "wgUseSquid",
    "wgDBtransactions",
    "wgUseAutomaticEditSummaries",
    "wgDeleteRevisionsLimit",
    "wgDeferredUpdateList",
    "wgRCMaxAge",
    "wgHitcounterUpdateFreq",
    "wgAllowPageInfo",
    "wgUseFileCache",
    "wgLegalTitleChars",
    "wgMaxRedirects",
    "wgInternalServer",
    "wgRestrictionLevels",
    "wgNamespaceProtection",
    "wgEmailConfirmToEdit",
    "wgWhitelistRead",
    "wgCapitalLinks",
    "wgMaximumMovedPages",
    "wgInvalidRedirectTargets",
    "wgHooks",
    "wgDebugLogFile",
    "wgProfileOnly",
    "wgDebugRawPage",
    "wgDebugLogPrefix",
    "wgDebugLogGroups",
    "wgShowHostnames",
    "wgDBerrorLog",
    "wgRequestTime",
    "wgProfileLimit",
    "wgReadOnlyFile",
    "wgReadOnly",
    "wgForceUIMsgAsContentMsg",
    "wgUseGzip",
    "wgDiff3",
    "wgDiff",
    "wgDisableOutputCompression",
    "wgSiteNotice",
    "wgDirectoryMode",
    "wgStatsMethod",
    "wgUDPProfilerHost",
    "wgUDPProfilerPort",
    "wgUrlProtocols",
    "wgMaxShellMemory",
    "wgMaxShellFileSize",
    "wgMaxShellTime",
    "wgShellLocale",
    "wgPostCommitUpdateList",
    "wgHttpOnlyBlacklist",
    "wgSessionsInMemcached",
    "wgCookiePath",
    "wgCookieDomain",
    "wgCookieSecure",
    "wgCookieHttpOnly",
    "wgDBprefix",
    "wgMiserMode",
    "wgScriptExtension",
    "wgExtensionMessagesFiles",
    "wgAllowDisplayTitle",
    "wgAllowSlowParserFunctions",
    "wgRestrictDisplayTitle",
    "wgLinkHolderBatchSize",
    "wgPreprocessorCacheThreshold",
    "wgAlwaysUseTidy",
    "wgExpensiveParserFunctionLimit",
    "wgRawHtml",
    "wgNoFollowLinks",
    "wgNoFollowNsExceptions",
    "wgNoFollowDomainExceptions",
    "wgServerName",
    "wgLocaltimezone",
    "wgNonincludableNamespaces",
    "wgEnableScaryTranscluding",
    "wgTranscludeCacheExpiry",
    "wgMaxTocLevel",
    "wgMaxSigChars",
    "wgCategoryPrefixedDefaultSortkey",
    "__Test",
    "options",
    "optionsWithArgs",
    "self",
    "args",
    "arg",
    "option",
    "param",
    "bits",
    "p",
    "sep",
    "wgUseNormalUser",
    "wgWikiFarm",
    "cluster",
    "wgConf",
    "wgLocalDatabases",
    "wgNoDBParam",
    "db",
    "site",
    "lang",
    "DP",
    "fname",
    "hatesSafari",
    "secure",
    "pathBits",
    "m",
    "server",
    "docRoot",
    "matches",
    "dbSuffix",
    "filename",
    "globals",
    "cacheRecord",
    "wikiTags",
    "tag",
    "dblist",
    "oldUmask",
    "file",
    "wgStyleSheetPath",
    "wgStockPath",
    "wgFileStore",
    "wgSharedThumbnailScriptPath",
    "wgUploadPath",
    "wgUploadDirectory",
    "tmarray",
    "hour",
    "day",
    "wgEmergencyContact",
    "wgHTCPMulticastAddress",
    "wgHTCPMulticastTTL",
    "wgCheckDBSchema",
    "oldtz",
    "wgTexvc",
    "wgTmpDirectory",
    "wgDisableTextSearch",
    "wgDisableSearchUpdate",
    "wgUseImageResize",
    "wgUseImageMagick",
    "wgImageMagickConvertCommand",
    "wgFileBlacklist",
    "wgFileExtensions",
    "wmgPrivateWikiUploads",
    "wgMimeTypeBlacklist",
    "wgSVGConverters",
    "wgAllowUserJs",
    "wgUseESI",
    "wgLogQueries",
    "wgSqlLogFile",
    "wmgUseDualLicense",
    "wgProfiling",
    "wgProfileSampleRate",
    "wgSiteMatrixFile",
    "wgMaxIfExistCount",
    "wmgUseFlaggedRevs",
    "wgCategoryTreeDynamicTag",
    "wmgUseProofreadPage",
    "wmgUseLST",
    "wmgUseSpamBlacklist",
    "wgTitleBlacklistSources",
    "wmgUseQuiz",
    "wmgUseGadgets",
    "wgFFmpegLocation",
    "wgCortadoJarFile",
    "wgContactUser",
    "wgExtDistTarDir",
    "wgExtDistTarUrl",
    "wgExtDistWorkingCopy",
    "wgExtDistRemoteClient",
    "wgExtDistBranches",
    "wgGlobalBlockingDatabase",
    "wmgApplyGlobalBlocks",
    "wgApplyGlobalBlocks",
    "wgTrustedXffFile",
    "wmgContactPageConf",
    "ubUploadBlacklist",
    "wgTimelineSettings",
    "wgAllowRealName",
    "wgSysopRangeBans",
    "wgUploadSizeWarning",
    "wgForeignFileRepos",
    "title",
    "wgUseHashTable",
    "wgLanguageCode",
    "wgLanguageCodeReal",
    "wgUseLuceneSearch",
    "wgMaxSquidPurgeTitles",
    "wgThumbnailEpoch",
    "oaiAgentRegex",
    "oaiAuth",
    "oaiAudit",
    "oaiAuditDatabase",
    "wgExtensionFunctions",
    "wmgUseDPL",
    "wmgUseSpecialNuke",
    "wgAntiBotPayloads",
    "wgTorLoadNodes",
    "wgTorIPs",
    "wgTorAutoConfirmAge",
    "wgTorAutoConfirmCount",
    "wgTorDisableAdminBlocks",
    "wgTorTagChanges",
    "wgDisabledActions",
    "wgDisableHardRedirects",
    "groupOverrides2",
    "group",
    "permissions",
    "groupOverrides",
    "wmgAutopromoteExtraGroups",
    "wgAutopromote",
    "wmgExtraImplicitGroups",
    "wgLegacySchemaConversion",
    "wgHTTPTimeout",
    "wgDebugDumpSql",
    "wgDBservers",
    "key",
    "val",
    "wgDBserver",
    "wgExternalServers",
    "x",
    "y",
    "wgBrowserBlackList",
    "wgScanSetSettings",
    "wmgCaptchaPassword",
    "wmgEnableCaptcha",
    "wmgCaptchaSecret",
    "wgCaptchaSecret",
    "wgCaptchaDirectory",
    "wgCaptchaDirectoryLevels",
    "wgCaptchaStorageClass",
    "wgCaptchaClass",
    "wgCaptchaWhitelist",
    "wgCaptchaRegexes",
    "wgCaptchaTriggers",
    "wmgEmergencyCaptcha",
    "wgExternalDiffEngine",
    "wgEnotifWatchlist",
    "wgEnotifUserTalk",
    "wgEnotifUseJobQ",
    "sectionLoads",
    "dbHostsByName",
    "wgAlternateMaster",
    "sectionsByDB",
    "section",
    "wmgUseCentralAuth",
    "wgCentralAuthDryRun",
    "wgCentralAuthCookies",
    "wgCentralAuthUDPAddress",
    "wgCentralAuthNew2UDPPrefix",
    "wmgSecondLevelDomain",
    "wgCentralAuthAutoLoginWikis",
    "wgCentralAuthCookieDomain",
    "wmgCentralAuthLoginIcon",
    "wgCentralAuthLoginIcon",
    "wgCentralAuthAutoNew",
    "wgCentralAuthCreateOnView",
    "wgCentralAuthCookiePrefix",
    "wmgUseDismissableSiteNotice",
    "wgMajorSiteNoticeID",
    "wgImageMagickTempDir",
    "wgMediaHandlers",
    "wmgUseCentralNotice",
    "wgNoticeProjects",
    "wgNoticeCentralPath",
    "wgNoticeLocalPath",
    "wgNoticeCentralDirectory",
    "wgNoticeLocalDirectory",
    "wmgNoticeProject",
    "wgNoticeProject",
    "wmgCentralNoticeLoader",
    "wgCentralNoticeLoader",
    "wgNoticeTimeout",
    "wgNoticeServerTimeout",
    "wgNoticeCounterSource",
    "wgNoticeInfrastructure",
    "wgNoticeScroll",
    "wmgUseTitleKey",
    "wmgUseCollection",
    "wgCollectionMWServeURL",
    "wgCommunityCollectionNamespace",
    "wgCollectionArticleNamespaces",
    "wgCollectionFormats",
    "wgLicenseURL",
    "wmgCollectionPortletForLoggedInUsersOnly",
    "wgCollectionPortletForLoggedInUsersOnly",
    "wgAccountCreationThrottle",
    "wmgUseNewUserMessage",
    "wmgNewUserSuppressRC",
    "wgNewUserSuppressRC",
    "wmgNewUserMinorEdit",
    "wgNewUserMinorEdit",
    "wmgNewUserMessageOnAutoCreate",
    "wgNewUserMessageOnAutoCreate",
    "wmgUseCodeReview",
    "wgSubversionProxy",
    "wgCodeReviewENotif",
    "wmgUseDrafts",
    "wmgUseConfigure",
    "wgConfigureHandler",
    "wgConfigureDatabase",
    "wgConfigureFileSystemCache",
    "wgConfigureEditableSettings",
    "wgConfigureWikis",
    "wmgUseAbuseFilter",
    "wgAbuseFilterAvailableActions",
    "bin",
    "wgUseRootUser",
    "wgDBadminuser",
    "wgDBuser",
    "wgDBadminpassword",
    "wgDBpassword",
    "adminSettings",
    "i",
    "wgLBFactoryConf",
    "wgShowSQLErrors",
    "wgCaches",
    "wgMemCachedServers",
    "wgMemCachedDebug",
    "wgMemCachedPersistent",
    "wgMainCacheType",
    "wgMessageCacheType",
    "wgParserCacheType",
    "wgCommandLineDarkBg",
    "tester",
    "wgParserTestFiles",
  };
  if (idx >= 0 && idx < 537) {
    k = names[idx];
    switch (idx) {
    case 0: return gv_argc;
    case 1: return gv_argv;
    case 2: return gv__SERVER;
    case 3: return gv__GET;
    case 4: return gv__POST;
    case 5: return gv__COOKIE;
    case 6: return gv__FILES;
    case 7: return gv__ENV;
    case 8: return gv__REQUEST;
    case 9: return gv__SESSION;
    case 10: return gv_HTTP_RAW_POST_DATA;
    case 11: return gv_http_response_header;
    case 12: return GV(wgMessageCache);
    case 13: return GV(wgStyleDirectory);
    case 14: return GV(wgRequest);
    case 15: return GV(wgUser);
    case 16: return GV(wgLang);
    case 17: return GV(wgOut);
    case 18: return GV(wgParserConf);
    case 19: return GV(wgParser);
    case 20: return GV(wgDefaultUserOptions);
    case 21: return GV(wgNamespacesToBeSearchedDefault);
    case 22: return GV(wgIP);
    case 23: return GV(wgUsePrivateIPs);
    case 24: return GV(wgSquidServers);
    case 25: return GV(wgSquidServersNoPurge);
    case 26: return GV(wgBlockOpenProxies);
    case 27: return GV(wgProxyPorts);
    case 28: return GV(wgProxyScriptPath);
    case 29: return GV(wgMemc);
    case 30: return GV(wgProxyMemcExpiry);
    case 31: return GV(wgProxyKey);
    case 32: return GV(wgProxyList);
    case 33: return GV(wgCanonicalNamespaceNames);
    case 34: return GV(wgExtraNamespaces);
    case 35: return GV(wgAllowImageMoving);
    case 36: return GV(wgContentNamespaces);
    case 37: return GV(wgNamespacesWithSubpages);
    case 38: return GV(wgContLang);
    case 39: return GV(wgThumbLimits);
    case 40: return GV(wgThumbUpright);
    case 41: return GV(wgStylePath);
    case 42: return GV(wgEnableUploads);
    case 43: return GV(wgDisableAnonTalk);
    case 44: return GV(wgSysopUserBans);
    case 45: return GV(wgJsMimeType);
    case 46: return GV(wgTitle);
    case 47: return GV(wgArticle);
    case 48: return GV(wgScript);
    case 49: return GV(wgContLanguageCode);
    case 50: return GV(wgMimeType);
    case 51: return GV(wgOutputEncoding);
    case 52: return GV(wgXhtmlDefaultNamespace);
    case 53: return GV(wgXhtmlNamespaces);
    case 54: return GV(wgDisableCounters);
    case 55: return GV(wgLogo);
    case 56: return GV(wgHideInterlanguageLinks);
    case 57: return GV(wgMaxCredits);
    case 58: return GV(wgShowCreditsIfMax);
    case 59: return GV(wgPageShowWatchingUsers);
    case 60: return GV(wgUseTrackbacks);
    case 61: return GV(wgUseSiteJs);
    case 62: return GV(wgArticlePath);
    case 63: return GV(wgScriptPath);
    case 64: return GV(wgServer);
    case 65: return GV(wgDisableLangConversion);
    case 66: return GV(wgUploadNavigationUrl);
    case 67: return GV(wgHandheldStyle);
    case 68: return GV(wgStyleVersion);
    case 69: return GV(wgUseTwoButtonsSearchForm);
    case 70: return GV(wgValidSkinNames);
    case 71: return GV(wgSkipSkins);
    case 72: return GV(wgDefaultSkin);
    case 73: return GV(wgFavicon);
    case 74: return GV(wgAppleTouchIcon);
    case 75: return GV(wgEnableDublinCoreRdf);
    case 76: return GV(wgEnableCreativeCommonsRdf);
    case 77: return GV(wgRightsPage);
    case 78: return GV(wgRightsUrl);
    case 79: return GV(wgDebugComments);
    case 80: return GV(wgBreakFrames);
    case 81: return GV(wgVariantArticlePath);
    case 82: return GV(wgActionPaths);
    case 83: return GV(wgUseAjax);
    case 84: return GV(wgAjaxWatch);
    case 85: return GV(wgVersion);
    case 86: return GV(wgEnableAPI);
    case 87: return GV(wgEnableWriteAPI);
    case 88: return GV(wgRestrictionTypes);
    case 89: return GV(wgLivePreview);
    case 90: return GV(wgMWSuggestTemplate);
    case 91: return GV(wgDBname);
    case 92: return GV(wgEnableMWSuggest);
    case 93: return GV(wgAllowUserCss);
    case 94: return GV(wgUseSiteCss);
    case 95: return GV(wgSquidMaxage);
    case 96: return GV(wgUseCategoryBrowser);
    case 97: return GV(wgShowDebug);
    case 98: return GV(action);
    case 99: return GV(wgFeedClasses);
    case 100: return GV(wgExtraSubtitle);
    case 101: return GV(wgShowIPinHeader);
    case 102: return GV(wgRightsText);
    case 103: return GV(wgRightsIcon);
    case 104: return GV(wgCopyrightIcon);
    case 105: return GV(wgRedirectScript);
    case 106: return GV(parserMemc);
    case 107: return GV(wgEnableSidebarCache);
    case 108: return GV(wgSidebarCacheExpiry);
    case 109: return GV(wgAntiLockFlags);
    case 110: return GV(wgAutoblockExpiry);
    case 111: return GV(wgMaxNameChars);
    case 112: return GV(wgReservedUsernames);
    case 113: return GV(wgMinimalPasswordLength);
    case 114: return GV(wgAuth);
    case 115: return GV(wgCookiePrefix);
    case 116: return GV(wgUseRCPatrol);
    case 117: return GV(wgEnableSorbs);
    case 118: return GV(wgProxyWhitelist);
    case 119: return GV(wgSorbsUrl);
    case 120: return GV(wgRateLimitsExcludedGroups);
    case 121: return GV(wgRateLimitsExcludedIPs);
    case 122: return GV(wgRateLimits);
    case 123: return GV(wgRateLimitLog);
    case 124: return GV(wgBlockAllowsUTEdit);
    case 125: return GV(wgClockSkewFudge);
    case 126: return GV(wgSecretKey);
    case 127: return GV(wgPasswordReminderResendTime);
    case 128: return GV(wgUseNPPatrol);
    case 129: return GV(wgAllowUserSkin);
    case 130: return GV(wgUseEnotif);
    case 131: return GV(wgShowUpdatedMarker);
    case 132: return GV(wgUseDynamicDates);
    case 133: return GV(wgRenderHashAppend);
    case 134: return GV(wgActiveUserEditCount);
    case 135: return GV(wgActiveUserDays);
    case 136: return GV(wgNewPasswordExpiry);
    case 137: return GV(wgPasswordSender);
    case 138: return GV(wgEnableEmail);
    case 139: return GV(wgEnableUserEmail);
    case 140: return GV(wgEmailAuthentication);
    case 141: return GV(wgGroupPermissions);
    case 142: return GV(wgAvailableRights);
    case 143: return GV(wgImplicitGroups);
    case 144: return GV(wgPasswordSalt);
    case 145: return GV(wgNewUserLog);
    case 146: return GV(wgCommandLineMode);
    case 147: return GV(wgUseTeX);
    case 148: return GV(wgInterwikiMagic);
    case 149: return GV(wgAllowExternalImages);
    case 150: return GV(wgAllowExternalImagesFrom);
    case 151: return GV(wgEnableImageWhitelist);
    case 152: return GV(wgAllowSpecialInclusion);
    case 153: return GV(wgMaxArticleSize);
    case 154: return GV(wgMaxPPNodeCount);
    case 155: return GV(wgMaxTemplateDepth);
    case 156: return GV(wgMaxPPExpandDepth);
    case 157: return GV(wgCleanSignatures);
    case 158: return GV(wgExternalLinkTarget);
    case 159: return GV(wgProfiler);
    case 160: return GV(wgDebugFunctionEntry);
    case 161: return GV(wgDebugProfiling);
    case 162: return GV(wgProfileToDatabase);
    case 163: return GV(wgLanguageNames);
    case 164: return GV(wgInputEncoding);
    case 165: return GV(IP);
    case 166: return GV(wgExtraLanguageNames);
    case 167: return GV(wgLocalTZoffset);
    case 168: return GV(wgDBtype);
    case 169: return GV(wgEditEncoding);
    case 170: return GV(wgExtensionAliasesFiles);
    case 171: return GV(wgTranslateNumerals);
    case 172: return GV(wgGrammarForms);
    case 173: return GV(wgEnableSerializedMessages);
    case 174: return GV(wgCheckSerialized);
    case 175: return GV(wgMetaNamespace);
    case 176: return GV(wgMetaNamespaceTalk);
    case 177: return GV(wgNamespaceAliases);
    case 178: return GV(wgAmericanDates);
    case 179: return GV(wgFullyInitialised);
    case 180: return GV(wgExceptionHooks);
    case 181: return GV(wgShowExceptionDetails);
    case 182: return GV(wgSitename);
    case 183: return GV(utfCombiningClass);
    case 184: return GV(utfCanonicalComp);
    case 185: return GV(utfCanonicalDecomp);
    case 186: return GV(utfCompatibilityDecomp);
    case 187: return GV(utfCheckNFC);
    case 188: return GV(attrib);
    case 189: return GV(space);
    case 190: return GV(wgHtmlEntities);
    case 191: return GV(wgHtmlEntityAliases);
    case 192: return GV(wgUseTidy);
    case 193: return GV(wgEnforceHtmlIds);
    case 194: return GV(wgLegacyEncoding);
    case 195: return GV(wgCompressRevisions);
    case 196: return GV(wgDefaultExternalStore);
    case 197: return GV(wgRevisionCacheExpiry);
    case 198: return GV(wgCacheEpoch);
    case 199: return GV(wgLocalInterwiki);
    case 200: return GV(wgPutIPinRC);
    case 201: return GV(wgRC2UDPAddress);
    case 202: return GV(wgRC2UDPOmitBots);
    case 203: return GV(wgRC2UDPPrefix);
    case 204: return GV(wgRC2UDPPort);
    case 205: return GV(wgLogRestrictions);
    case 206: return GV(wgRC2UDPInterwikiPrefix);
    case 207: return GV(wgUseDumbLinkUpdate);
    case 208: return GV(wgUpdateRowsPerJob);
    case 209: return GV(wgPagePropLinkInvalidations);
    case 210: return GV(wgUseCommaCount);
    case 211: return GV(wgEnableParserCache);
    case 212: return GV(wgNamespaceRobotPolicies);
    case 213: return GV(wgArticleRobotPolicies);
    case 214: return GV(wgDefaultRobotPolicy);
    case 215: return GV(wgUseETag);
    case 216: return GV(wgRedirectSources);
    case 217: return GV(wgUseSquid);
    case 218: return GV(wgDBtransactions);
    case 219: return GV(wgUseAutomaticEditSummaries);
    case 220: return GV(wgDeleteRevisionsLimit);
    case 221: return GV(wgDeferredUpdateList);
    case 222: return GV(wgRCMaxAge);
    case 223: return GV(wgHitcounterUpdateFreq);
    case 224: return GV(wgAllowPageInfo);
    case 225: return GV(wgUseFileCache);
    case 226: return GV(wgLegalTitleChars);
    case 227: return GV(wgMaxRedirects);
    case 228: return GV(wgInternalServer);
    case 229: return GV(wgRestrictionLevels);
    case 230: return GV(wgNamespaceProtection);
    case 231: return GV(wgEmailConfirmToEdit);
    case 232: return GV(wgWhitelistRead);
    case 233: return GV(wgCapitalLinks);
    case 234: return GV(wgMaximumMovedPages);
    case 235: return GV(wgInvalidRedirectTargets);
    case 236: return GV(wgHooks);
    case 237: return GV(wgDebugLogFile);
    case 238: return GV(wgProfileOnly);
    case 239: return GV(wgDebugRawPage);
    case 240: return GV(wgDebugLogPrefix);
    case 241: return GV(wgDebugLogGroups);
    case 242: return GV(wgShowHostnames);
    case 243: return GV(wgDBerrorLog);
    case 244: return GV(wgRequestTime);
    case 245: return GV(wgProfileLimit);
    case 246: return GV(wgReadOnlyFile);
    case 247: return GV(wgReadOnly);
    case 248: return GV(wgForceUIMsgAsContentMsg);
    case 249: return GV(wgUseGzip);
    case 250: return GV(wgDiff3);
    case 251: return GV(wgDiff);
    case 252: return GV(wgDisableOutputCompression);
    case 253: return GV(wgSiteNotice);
    case 254: return GV(wgDirectoryMode);
    case 255: return GV(wgStatsMethod);
    case 256: return GV(wgUDPProfilerHost);
    case 257: return GV(wgUDPProfilerPort);
    case 258: return GV(wgUrlProtocols);
    case 259: return GV(wgMaxShellMemory);
    case 260: return GV(wgMaxShellFileSize);
    case 261: return GV(wgMaxShellTime);
    case 262: return GV(wgShellLocale);
    case 263: return GV(wgPostCommitUpdateList);
    case 264: return GV(wgHttpOnlyBlacklist);
    case 265: return GV(wgSessionsInMemcached);
    case 266: return GV(wgCookiePath);
    case 267: return GV(wgCookieDomain);
    case 268: return GV(wgCookieSecure);
    case 269: return GV(wgCookieHttpOnly);
    case 270: return GV(wgDBprefix);
    case 271: return GV(wgMiserMode);
    case 272: return GV(wgScriptExtension);
    case 273: return GV(wgExtensionMessagesFiles);
    case 274: return GV(wgAllowDisplayTitle);
    case 275: return GV(wgAllowSlowParserFunctions);
    case 276: return GV(wgRestrictDisplayTitle);
    case 277: return GV(wgLinkHolderBatchSize);
    case 278: return GV(wgPreprocessorCacheThreshold);
    case 279: return GV(wgAlwaysUseTidy);
    case 280: return GV(wgExpensiveParserFunctionLimit);
    case 281: return GV(wgRawHtml);
    case 282: return GV(wgNoFollowLinks);
    case 283: return GV(wgNoFollowNsExceptions);
    case 284: return GV(wgNoFollowDomainExceptions);
    case 285: return GV(wgServerName);
    case 286: return GV(wgLocaltimezone);
    case 287: return GV(wgNonincludableNamespaces);
    case 288: return GV(wgEnableScaryTranscluding);
    case 289: return GV(wgTranscludeCacheExpiry);
    case 290: return GV(wgMaxTocLevel);
    case 291: return GV(wgMaxSigChars);
    case 292: return GV(wgCategoryPrefixedDefaultSortkey);
    case 293: return GV(__Test);
    case 294: return GV(options);
    case 295: return GV(optionsWithArgs);
    case 296: return GV(self);
    case 297: return GV(args);
    case 298: return GV(arg);
    case 299: return GV(option);
    case 300: return GV(param);
    case 301: return GV(bits);
    case 302: return GV(p);
    case 303: return GV(sep);
    case 304: return GV(wgUseNormalUser);
    case 305: return GV(wgWikiFarm);
    case 306: return GV(cluster);
    case 307: return GV(wgConf);
    case 308: return GV(wgLocalDatabases);
    case 309: return GV(wgNoDBParam);
    case 310: return GV(db);
    case 311: return GV(site);
    case 312: return GV(lang);
    case 313: return GV(DP);
    case 314: return GV(fname);
    case 315: return GV(hatesSafari);
    case 316: return GV(secure);
    case 317: return GV(pathBits);
    case 318: return GV(m);
    case 319: return GV(server);
    case 320: return GV(docRoot);
    case 321: return GV(matches);
    case 322: return GV(dbSuffix);
    case 323: return GV(filename);
    case 324: return GV(globals);
    case 325: return GV(cacheRecord);
    case 326: return GV(wikiTags);
    case 327: return GV(tag);
    case 328: return GV(dblist);
    case 329: return GV(oldUmask);
    case 330: return GV(file);
    case 331: return GV(wgStyleSheetPath);
    case 332: return GV(wgStockPath);
    case 333: return GV(wgFileStore);
    case 334: return GV(wgSharedThumbnailScriptPath);
    case 335: return GV(wgUploadPath);
    case 336: return GV(wgUploadDirectory);
    case 337: return GV(tmarray);
    case 338: return GV(hour);
    case 339: return GV(day);
    case 340: return GV(wgEmergencyContact);
    case 341: return GV(wgHTCPMulticastAddress);
    case 342: return GV(wgHTCPMulticastTTL);
    case 343: return GV(wgCheckDBSchema);
    case 344: return GV(oldtz);
    case 345: return GV(wgTexvc);
    case 346: return GV(wgTmpDirectory);
    case 347: return GV(wgDisableTextSearch);
    case 348: return GV(wgDisableSearchUpdate);
    case 349: return GV(wgUseImageResize);
    case 350: return GV(wgUseImageMagick);
    case 351: return GV(wgImageMagickConvertCommand);
    case 352: return GV(wgFileBlacklist);
    case 353: return GV(wgFileExtensions);
    case 354: return GV(wmgPrivateWikiUploads);
    case 355: return GV(wgMimeTypeBlacklist);
    case 356: return GV(wgSVGConverters);
    case 357: return GV(wgAllowUserJs);
    case 358: return GV(wgUseESI);
    case 359: return GV(wgLogQueries);
    case 360: return GV(wgSqlLogFile);
    case 361: return GV(wmgUseDualLicense);
    case 362: return GV(wgProfiling);
    case 363: return GV(wgProfileSampleRate);
    case 364: return GV(wgSiteMatrixFile);
    case 365: return GV(wgMaxIfExistCount);
    case 366: return GV(wmgUseFlaggedRevs);
    case 367: return GV(wgCategoryTreeDynamicTag);
    case 368: return GV(wmgUseProofreadPage);
    case 369: return GV(wmgUseLST);
    case 370: return GV(wmgUseSpamBlacklist);
    case 371: return GV(wgTitleBlacklistSources);
    case 372: return GV(wmgUseQuiz);
    case 373: return GV(wmgUseGadgets);
    case 374: return GV(wgFFmpegLocation);
    case 375: return GV(wgCortadoJarFile);
    case 376: return GV(wgContactUser);
    case 377: return GV(wgExtDistTarDir);
    case 378: return GV(wgExtDistTarUrl);
    case 379: return GV(wgExtDistWorkingCopy);
    case 380: return GV(wgExtDistRemoteClient);
    case 381: return GV(wgExtDistBranches);
    case 382: return GV(wgGlobalBlockingDatabase);
    case 383: return GV(wmgApplyGlobalBlocks);
    case 384: return GV(wgApplyGlobalBlocks);
    case 385: return GV(wgTrustedXffFile);
    case 386: return GV(wmgContactPageConf);
    case 387: return GV(ubUploadBlacklist);
    case 388: return GV(wgTimelineSettings);
    case 389: return GV(wgAllowRealName);
    case 390: return GV(wgSysopRangeBans);
    case 391: return GV(wgUploadSizeWarning);
    case 392: return GV(wgForeignFileRepos);
    case 393: return GV(title);
    case 394: return GV(wgUseHashTable);
    case 395: return GV(wgLanguageCode);
    case 396: return GV(wgLanguageCodeReal);
    case 397: return GV(wgUseLuceneSearch);
    case 398: return GV(wgMaxSquidPurgeTitles);
    case 399: return GV(wgThumbnailEpoch);
    case 400: return GV(oaiAgentRegex);
    case 401: return GV(oaiAuth);
    case 402: return GV(oaiAudit);
    case 403: return GV(oaiAuditDatabase);
    case 404: return GV(wgExtensionFunctions);
    case 405: return GV(wmgUseDPL);
    case 406: return GV(wmgUseSpecialNuke);
    case 407: return GV(wgAntiBotPayloads);
    case 408: return GV(wgTorLoadNodes);
    case 409: return GV(wgTorIPs);
    case 410: return GV(wgTorAutoConfirmAge);
    case 411: return GV(wgTorAutoConfirmCount);
    case 412: return GV(wgTorDisableAdminBlocks);
    case 413: return GV(wgTorTagChanges);
    case 414: return GV(wgDisabledActions);
    case 415: return GV(wgDisableHardRedirects);
    case 416: return GV(groupOverrides2);
    case 417: return GV(group);
    case 418: return GV(permissions);
    case 419: return GV(groupOverrides);
    case 420: return GV(wmgAutopromoteExtraGroups);
    case 421: return GV(wgAutopromote);
    case 422: return GV(wmgExtraImplicitGroups);
    case 423: return GV(wgLegacySchemaConversion);
    case 424: return GV(wgHTTPTimeout);
    case 425: return GV(wgDebugDumpSql);
    case 426: return GV(wgDBservers);
    case 427: return GV(key);
    case 428: return GV(val);
    case 429: return GV(wgDBserver);
    case 430: return GV(wgExternalServers);
    case 431: return GV(x);
    case 432: return GV(y);
    case 433: return GV(wgBrowserBlackList);
    case 434: return GV(wgScanSetSettings);
    case 435: return GV(wmgCaptchaPassword);
    case 436: return GV(wmgEnableCaptcha);
    case 437: return GV(wmgCaptchaSecret);
    case 438: return GV(wgCaptchaSecret);
    case 439: return GV(wgCaptchaDirectory);
    case 440: return GV(wgCaptchaDirectoryLevels);
    case 441: return GV(wgCaptchaStorageClass);
    case 442: return GV(wgCaptchaClass);
    case 443: return GV(wgCaptchaWhitelist);
    case 444: return GV(wgCaptchaRegexes);
    case 445: return GV(wgCaptchaTriggers);
    case 446: return GV(wmgEmergencyCaptcha);
    case 447: return GV(wgExternalDiffEngine);
    case 448: return GV(wgEnotifWatchlist);
    case 449: return GV(wgEnotifUserTalk);
    case 450: return GV(wgEnotifUseJobQ);
    case 451: return GV(sectionLoads);
    case 452: return GV(dbHostsByName);
    case 453: return GV(wgAlternateMaster);
    case 454: return GV(sectionsByDB);
    case 455: return GV(section);
    case 456: return GV(wmgUseCentralAuth);
    case 457: return GV(wgCentralAuthDryRun);
    case 458: return GV(wgCentralAuthCookies);
    case 459: return GV(wgCentralAuthUDPAddress);
    case 460: return GV(wgCentralAuthNew2UDPPrefix);
    case 461: return GV(wmgSecondLevelDomain);
    case 462: return GV(wgCentralAuthAutoLoginWikis);
    case 463: return GV(wgCentralAuthCookieDomain);
    case 464: return GV(wmgCentralAuthLoginIcon);
    case 465: return GV(wgCentralAuthLoginIcon);
    case 466: return GV(wgCentralAuthAutoNew);
    case 467: return GV(wgCentralAuthCreateOnView);
    case 468: return GV(wgCentralAuthCookiePrefix);
    case 469: return GV(wmgUseDismissableSiteNotice);
    case 470: return GV(wgMajorSiteNoticeID);
    case 471: return GV(wgImageMagickTempDir);
    case 472: return GV(wgMediaHandlers);
    case 473: return GV(wmgUseCentralNotice);
    case 474: return GV(wgNoticeProjects);
    case 475: return GV(wgNoticeCentralPath);
    case 476: return GV(wgNoticeLocalPath);
    case 477: return GV(wgNoticeCentralDirectory);
    case 478: return GV(wgNoticeLocalDirectory);
    case 479: return GV(wmgNoticeProject);
    case 480: return GV(wgNoticeProject);
    case 481: return GV(wmgCentralNoticeLoader);
    case 482: return GV(wgCentralNoticeLoader);
    case 483: return GV(wgNoticeTimeout);
    case 484: return GV(wgNoticeServerTimeout);
    case 485: return GV(wgNoticeCounterSource);
    case 486: return GV(wgNoticeInfrastructure);
    case 487: return GV(wgNoticeScroll);
    case 488: return GV(wmgUseTitleKey);
    case 489: return GV(wmgUseCollection);
    case 490: return GV(wgCollectionMWServeURL);
    case 491: return GV(wgCommunityCollectionNamespace);
    case 492: return GV(wgCollectionArticleNamespaces);
    case 493: return GV(wgCollectionFormats);
    case 494: return GV(wgLicenseURL);
    case 495: return GV(wmgCollectionPortletForLoggedInUsersOnly);
    case 496: return GV(wgCollectionPortletForLoggedInUsersOnly);
    case 497: return GV(wgAccountCreationThrottle);
    case 498: return GV(wmgUseNewUserMessage);
    case 499: return GV(wmgNewUserSuppressRC);
    case 500: return GV(wgNewUserSuppressRC);
    case 501: return GV(wmgNewUserMinorEdit);
    case 502: return GV(wgNewUserMinorEdit);
    case 503: return GV(wmgNewUserMessageOnAutoCreate);
    case 504: return GV(wgNewUserMessageOnAutoCreate);
    case 505: return GV(wmgUseCodeReview);
    case 506: return GV(wgSubversionProxy);
    case 507: return GV(wgCodeReviewENotif);
    case 508: return GV(wmgUseDrafts);
    case 509: return GV(wmgUseConfigure);
    case 510: return GV(wgConfigureHandler);
    case 511: return GV(wgConfigureDatabase);
    case 512: return GV(wgConfigureFileSystemCache);
    case 513: return GV(wgConfigureEditableSettings);
    case 514: return GV(wgConfigureWikis);
    case 515: return GV(wmgUseAbuseFilter);
    case 516: return GV(wgAbuseFilterAvailableActions);
    case 517: return GV(bin);
    case 518: return GV(wgUseRootUser);
    case 519: return GV(wgDBadminuser);
    case 520: return GV(wgDBuser);
    case 521: return GV(wgDBadminpassword);
    case 522: return GV(wgDBpassword);
    case 523: return GV(adminSettings);
    case 524: return GV(i);
    case 525: return GV(wgLBFactoryConf);
    case 526: return GV(wgShowSQLErrors);
    case 527: return GV(wgCaches);
    case 528: return GV(wgMemCachedServers);
    case 529: return GV(wgMemCachedDebug);
    case 530: return GV(wgMemCachedPersistent);
    case 531: return GV(wgMainCacheType);
    case 532: return GV(wgMessageCacheType);
    case 533: return GV(wgParserCacheType);
    case 534: return GV(wgCommandLineDarkBg);
    case 535: return GV(tester);
    case 536: return GV(wgParserTestFiles);
    }
  }
  return Globals::getRefByIdx(idx, k);
}

///////////////////////////////////////////////////////////////////////////////
}
