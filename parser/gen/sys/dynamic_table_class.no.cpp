
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Invoke Tables
Object co_title(CArrRef params, bool init = true);
Variant cw_title$os_get(const char *s);
Variant &cw_title$os_lval(const char *s);
Variant cw_title$os_constant(const char *s);
Variant cw_title$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_request(CArrRef params, bool init = true);
Variant cw_request$os_get(const char *s);
Variant &cw_request$os_lval(const char *s);
Variant cw_request$os_constant(const char *s);
Variant cw_request$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_errorpageerror(CArrRef params, bool init = true);
Variant cw_errorpageerror$os_get(const char *s);
Variant &cw_errorpageerror$os_lval(const char *s);
Variant cw_errorpageerror$os_constant(const char *s);
Variant cw_errorpageerror$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_stubuser(CArrRef params, bool init = true);
Variant cw_stubuser$os_get(const char *s);
Variant &cw_stubuser$os_lval(const char *s);
Variant cw_stubuser$os_constant(const char *s);
Variant cw_stubuser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_magicwordarray(CArrRef params, bool init = true);
Variant cw_magicwordarray$os_get(const char *s);
Variant &cw_magicwordarray$os_lval(const char *s);
Variant cw_magicwordarray$os_constant(const char *s);
Variant cw_magicwordarray$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_linker(CArrRef params, bool init = true);
Variant cw_linker$os_get(const char *s);
Variant &cw_linker$os_lval(const char *s);
Variant cw_linker$os_constant(const char *s);
Variant cw_linker$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_utfnormal(CArrRef params, bool init = true);
Variant cw_utfnormal$os_get(const char *s);
Variant &cw_utfnormal$os_lval(const char *s);
Variant cw_utfnormal$os_constant(const char *s);
Variant cw_utfnormal$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_stripstate(CArrRef params, bool init = true);
Variant cw_stripstate$os_get(const char *s);
Variant &cw_stripstate$os_lval(const char *s);
Variant cw_stripstate$os_constant(const char *s);
Variant cw_stripstate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_hashtablereplacer(CArrRef params, bool init = true);
Variant cw_hashtablereplacer$os_get(const char *s);
Variant &cw_hashtablereplacer$os_lval(const char *s);
Variant cw_hashtablereplacer$os_constant(const char *s);
Variant cw_hashtablereplacer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_explodeiterator(CArrRef params, bool init = true);
Variant cw_explodeiterator$os_get(const char *s);
Variant &cw_explodeiterator$os_lval(const char *s);
Variant cw_explodeiterator$os_constant(const char *s);
Variant cw_explodeiterator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_linksupdate(CArrRef params, bool init = true);
Variant cw_linksupdate$os_get(const char *s);
Variant &cw_linksupdate$os_lval(const char *s);
Variant cw_linksupdate$os_constant(const char *s);
Variant cw_linksupdate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_getdb(CArrRef params, bool init = true);
Variant cw_getdb$os_get(const char *s);
Variant &cw_getdb$os_lval(const char *s);
Variant cw_getdb$os_constant(const char *s);
Variant cw_getdb$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parsertest(CArrRef params, bool init = true);
Variant cw_parsertest$os_get(const char *s);
Variant &cw_parsertest$os_lval(const char *s);
Variant cw_parsertest$os_constant(const char *s);
Variant cw_parsertest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_htmlcacheupdate(CArrRef params, bool init = true);
Variant cw_htmlcacheupdate$os_get(const char *s);
Variant &cw_htmlcacheupdate$os_lval(const char *s);
Variant cw_htmlcacheupdate$os_constant(const char *s);
Variant cw_htmlcacheupdate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_interwiki(CArrRef params, bool init = true);
Variant cw_interwiki$os_get(const char *s);
Variant &cw_interwiki$os_lval(const char *s);
Variant cw_interwiki$os_constant(const char *s);
Variant cw_interwiki$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_messagecache(CArrRef params, bool init = true);
Variant cw_messagecache$os_get(const char *s);
Variant &cw_messagecache$os_lval(const char *s);
Variant cw_messagecache$os_constant(const char *s);
Variant cw_messagecache$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_htmlfilecache(CArrRef params, bool init = true);
Variant cw_htmlfilecache$os_get(const char *s);
Variant &cw_htmlfilecache$os_lval(const char *s);
Variant cw_htmlfilecache$os_constant(const char *s);
Variant cw_htmlfilecache$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ansitermcolorer(CArrRef params, bool init = true);
Variant cw_ansitermcolorer$os_get(const char *s);
Variant &cw_ansitermcolorer$os_lval(const char *s);
Variant cw_ansitermcolorer$os_constant(const char *s);
Variant cw_ansitermcolorer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_stubobject(CArrRef params, bool init = true);
Variant cw_stubobject$os_get(const char *s);
Variant &cw_stubobject$os_lval(const char *s);
Variant cw_stubobject$os_constant(const char *s);
Variant cw_stubobject$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_doublereplacer(CArrRef params, bool init = true);
Variant cw_doublereplacer$os_get(const char *s);
Variant &cw_doublereplacer$os_lval(const char *s);
Variant cw_doublereplacer$os_constant(const char *s);
Variant cw_doublereplacer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_status(CArrRef params, bool init = true);
Variant cw_status$os_get(const char *s);
Variant &cw_status$os_lval(const char *s);
Variant cw_status$os_constant(const char *s);
Variant cw_status$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_mwexception(CArrRef params, bool init = true);
Variant cw_mwexception$os_get(const char *s);
Variant &cw_mwexception$os_lval(const char *s);
Variant cw_mwexception$os_constant(const char *s);
Variant cw_mwexception$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_fakememcachedclient(CArrRef params, bool init = true);
Variant cw_fakememcachedclient$os_get(const char *s);
Variant &cw_fakememcachedclient$os_lval(const char *s);
Variant cw_fakememcachedclient$os_constant(const char *s);
Variant cw_fakememcachedclient$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_replacer(CArrRef params, bool init = true);
Variant cw_replacer$os_get(const char *s);
Variant &cw_replacer$os_lval(const char *s);
Variant cw_replacer$os_constant(const char *s);
Variant cw_replacer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parseroptions(CArrRef params, bool init = true);
Variant cw_parseroptions$os_get(const char *s);
Variant &cw_parseroptions$os_lval(const char *s);
Variant cw_parseroptions$os_constant(const char *s);
Variant cw_parseroptions$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ip(CArrRef params, bool init = true);
Variant cw_ip$os_get(const char *s);
Variant &cw_ip$os_lval(const char *s);
Variant cw_ip$os_constant(const char *s);
Variant cw_ip$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_outputpage(CArrRef params, bool init = true);
Variant cw_outputpage$os_get(const char *s);
Variant &cw_outputpage$os_lval(const char *s);
Variant cw_outputpage$os_constant(const char *s);
Variant cw_outputpage$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppdpart(CArrRef params, bool init = true);
Variant cw_ppdpart$os_get(const char *s);
Variant &cw_ppdpart$os_lval(const char *s);
Variant cw_ppdpart$os_constant(const char *s);
Variant cw_ppdpart$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_magicword(CArrRef params, bool init = true);
Variant cw_magicword$os_get(const char *s);
Variant &cw_magicword$os_lval(const char *s);
Variant cw_magicword$os_constant(const char *s);
Variant cw_magicword$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_revision(CArrRef params, bool init = true);
Variant cw_revision$os_get(const char *s);
Variant &cw_revision$os_lval(const char *s);
Variant cw_revision$os_constant(const char *s);
Variant cw_revision$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppdstackelement(CArrRef params, bool init = true);
Variant cw_ppdstackelement$os_get(const char *s);
Variant &cw_ppdstackelement$os_lval(const char *s);
Variant cw_ppdstackelement$os_constant(const char *s);
Variant cw_ppdstackelement$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_linkholderarray(CArrRef params, bool init = true);
Variant cw_linkholderarray$os_get(const char *s);
Variant &cw_linkholderarray$os_lval(const char *s);
Variant cw_linkholderarray$os_constant(const char *s);
Variant cw_linkholderarray$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppnode_dom(CArrRef params, bool init = true);
Variant cw_ppnode_dom$os_get(const char *s);
Variant &cw_ppnode_dom$os_lval(const char *s);
Variant cw_ppnode_dom$os_constant(const char *s);
Variant cw_ppnode_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_fakeconverter(CArrRef params, bool init = true);
Variant cw_fakeconverter$os_get(const char *s);
Variant &cw_fakeconverter$os_lval(const char *s);
Variant cw_fakeconverter$os_constant(const char *s);
Variant cw_fakeconverter$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_skintemplate(CArrRef params, bool init = true);
Variant cw_skintemplate$os_get(const char *s);
Variant &cw_skintemplate$os_lval(const char *s);
Variant cw_skintemplate$os_constant(const char *s);
Variant cw_skintemplate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_mediawiki_i18n(CArrRef params, bool init = true);
Variant cw_mediawiki_i18n$os_get(const char *s);
Variant &cw_mediawiki_i18n$os_lval(const char *s);
Variant cw_mediawiki_i18n$os_constant(const char *s);
Variant cw_mediawiki_i18n$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_memcached(CArrRef params, bool init = true);
Variant cw_memcached$os_get(const char *s);
Variant &cw_memcached$os_lval(const char *s);
Variant cw_memcached$os_constant(const char *s);
Variant cw_memcached$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_regexlikereplacer(CArrRef params, bool init = true);
Variant cw_regexlikereplacer$os_get(const char *s);
Variant &cw_regexlikereplacer$os_lval(const char *s);
Variant cw_regexlikereplacer$os_constant(const char *s);
Variant cw_regexlikereplacer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_testrecorder(CArrRef params, bool init = true);
Variant cw_testrecorder$os_get(const char *s);
Variant &cw_testrecorder$os_lval(const char *s);
Variant cw_testrecorder$os_constant(const char *s);
Variant cw_testrecorder$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_proveparsertest(CArrRef params, bool init = true);
Variant cw_proveparsertest$os_get(const char *s);
Variant &cw_proveparsertest$os_lval(const char *s);
Variant cw_proveparsertest$os_constant(const char *s);
Variant cw_proveparsertest$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parser(CArrRef params, bool init = true);
Variant cw_parser$os_get(const char *s);
Variant &cw_parser$os_lval(const char *s);
Variant cw_parser$os_constant(const char *s);
Variant cw_parser$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_stubuserlang(CArrRef params, bool init = true);
Variant cw_stubuserlang$os_get(const char *s);
Variant &cw_stubuserlang$os_lval(const char *s);
Variant cw_stubuserlang$os_constant(const char *s);
Variant cw_stubuserlang$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppdstack(CArrRef params, bool init = true);
Variant cw_ppdstack$os_get(const char *s);
Variant &cw_ppdstack$os_lval(const char *s);
Variant cw_ppdstack$os_constant(const char *s);
Variant cw_ppdstack$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_titlearrayfromresult(CArrRef params, bool init = true);
Variant cw_titlearrayfromresult$os_get(const char *s);
Variant &cw_titlearrayfromresult$os_lval(const char *s);
Variant cw_titlearrayfromresult$os_constant(const char *s);
Variant cw_titlearrayfromresult$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_provetestrecorder(CArrRef params, bool init = true);
Variant cw_provetestrecorder$os_get(const char *s);
Variant &cw_provetestrecorder$os_lval(const char *s);
Variant cw_provetestrecorder$os_constant(const char *s);
Variant cw_provetestrecorder$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_linkbatch(CArrRef params, bool init = true);
Variant cw_linkbatch$os_get(const char *s);
Variant &cw_linkbatch$os_lval(const char *s);
Variant cw_linkbatch$os_constant(const char *s);
Variant cw_linkbatch$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_preprocessor(CArrRef params, bool init = true);
Variant cw_preprocessor$os_get(const char *s);
Variant &cw_preprocessor$os_lval(const char *s);
Variant cw_preprocessor$os_constant(const char *s);
Variant cw_preprocessor$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_passworderror(CArrRef params, bool init = true);
Variant cw_passworderror$os_get(const char *s);
Variant &cw_passworderror$os_lval(const char *s);
Variant cw_passworderror$os_constant(const char *s);
Variant cw_passworderror$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_preprocessor_dom(CArrRef params, bool init = true);
Variant cw_preprocessor_dom$os_get(const char *s);
Variant &cw_preprocessor_dom$os_lval(const char *s);
Variant cw_preprocessor_dom$os_constant(const char *s);
Variant cw_preprocessor_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_monobooktemplate(CArrRef params, bool init = true);
Variant cw_monobooktemplate$os_get(const char *s);
Variant &cw_monobooktemplate$os_lval(const char *s);
Variant cw_monobooktemplate$os_constant(const char *s);
Variant cw_monobooktemplate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_stubcontlang(CArrRef params, bool init = true);
Variant cw_stubcontlang$os_get(const char *s);
Variant &cw_stubcontlang$os_lval(const char *s);
Variant cw_stubcontlang$os_constant(const char *s);
Variant cw_stubcontlang$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_article(CArrRef params, bool init = true);
Variant cw_article$os_get(const char *s);
Variant &cw_article$os_lval(const char *s);
Variant cw_article$os_constant(const char *s);
Variant cw_article$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_skin(CArrRef params, bool init = true);
Variant cw_skin$os_get(const char *s);
Variant &cw_skin$os_lval(const char *s);
Variant cw_skin$os_constant(const char *s);
Variant cw_skin$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_parseroutput(CArrRef params, bool init = true);
Variant cw_parseroutput$os_get(const char *s);
Variant &cw_parseroutput$os_lval(const char *s);
Variant cw_parseroutput$os_constant(const char *s);
Variant cw_parseroutput$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_skinmonobook(CArrRef params, bool init = true);
Variant cw_skinmonobook$os_get(const char *s);
Variant &cw_skinmonobook$os_lval(const char *s);
Variant cw_skinmonobook$os_constant(const char *s);
Variant cw_skinmonobook$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppframe_dom(CArrRef params, bool init = true);
Variant cw_ppframe_dom$os_get(const char *s);
Variant &cw_ppframe_dom$os_lval(const char *s);
Variant cw_ppframe_dom$os_constant(const char *s);
Variant cw_ppframe_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_citerator(CArrRef params, bool init = true);
Variant cw_citerator$os_get(const char *s);
Variant &cw_citerator$os_lval(const char *s);
Variant cw_citerator$os_constant(const char *s);
Variant cw_citerator$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppcustomframe_dom(CArrRef params, bool init = true);
Variant cw_ppcustomframe_dom$os_get(const char *s);
Variant &cw_ppcustomframe_dom$os_lval(const char *s);
Variant cw_ppcustomframe_dom$os_constant(const char *s);
Variant cw_ppcustomframe_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ctraversable(CArrRef params, bool init = true);
Variant cw_ctraversable$os_get(const char *s);
Variant &cw_ctraversable$os_lval(const char *s);
Variant cw_ctraversable$os_constant(const char *s);
Variant cw_ctraversable$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_linkcache(CArrRef params, bool init = true);
Variant cw_linkcache$os_get(const char *s);
Variant &cw_linkcache$os_lval(const char *s);
Variant cw_linkcache$os_constant(const char *s);
Variant cw_linkcache$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_fatalerror(CArrRef params, bool init = true);
Variant cw_fatalerror$os_get(const char *s);
Variant &cw_fatalerror$os_lval(const char *s);
Variant cw_fatalerror$os_constant(const char *s);
Variant cw_fatalerror$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_language(CArrRef params, bool init = true);
Variant cw_language$os_get(const char *s);
Variant &cw_language$os_lval(const char *s);
Variant cw_language$os_constant(const char *s);
Variant cw_language$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_dbtestrecorder(CArrRef params, bool init = true);
Variant cw_dbtestrecorder$os_get(const char *s);
Variant &cw_dbtestrecorder$os_lval(const char *s);
Variant cw_dbtestrecorder$os_constant(const char *s);
Variant cw_dbtestrecorder$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_pptemplateframe_dom(CArrRef params, bool init = true);
Variant cw_pptemplateframe_dom$os_get(const char *s);
Variant &cw_pptemplateframe_dom$os_lval(const char *s);
Variant cw_pptemplateframe_dom$os_constant(const char *s);
Variant cw_pptemplateframe_dom$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_block(CArrRef params, bool init = true);
Variant cw_block$os_get(const char *s);
Variant &cw_block$os_lval(const char *s);
Variant cw_block$os_constant(const char *s);
Variant cw_block$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_onlyincludereplacer(CArrRef params, bool init = true);
Variant cw_onlyincludereplacer$os_get(const char *s);
Variant &cw_onlyincludereplacer$os_lval(const char *s);
Variant cw_onlyincludereplacer$os_constant(const char *s);
Variant cw_onlyincludereplacer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_recentchange(CArrRef params, bool init = true);
Variant cw_recentchange$os_get(const char *s);
Variant &cw_recentchange$os_lval(const char *s);
Variant cw_recentchange$os_constant(const char *s);
Variant cw_recentchange$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_coreparserfunctions(CArrRef params, bool init = true);
Variant cw_coreparserfunctions$os_get(const char *s);
Variant &cw_coreparserfunctions$os_lval(const char *s);
Variant cw_coreparserfunctions$os_constant(const char *s);
Variant cw_coreparserfunctions$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_profiler(CArrRef params, bool init = true);
Variant cw_profiler$os_get(const char *s);
Variant &cw_profiler$os_lval(const char *s);
Variant cw_profiler$os_constant(const char *s);
Variant cw_profiler$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_dummytermcolorer(CArrRef params, bool init = true);
Variant cw_dummytermcolorer$os_get(const char *s);
Variant &cw_dummytermcolorer$os_lval(const char *s);
Variant cw_dummytermcolorer$os_constant(const char *s);
Variant cw_dummytermcolorer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_titlearray(CArrRef params, bool init = true);
Variant cw_titlearray$os_get(const char *s);
Variant &cw_titlearray$os_lval(const char *s);
Variant cw_titlearray$os_constant(const char *s);
Variant cw_titlearray$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_user(CArrRef params, bool init = true);
Variant cw_user$os_get(const char *s);
Variant &cw_user$os_lval(const char *s);
Variant cw_user$os_constant(const char *s);
Variant cw_user$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppframe(CArrRef params, bool init = true);
Variant cw_ppframe$os_get(const char *s);
Variant &cw_ppframe$os_lval(const char *s);
Variant cw_ppframe$os_constant(const char *s);
Variant cw_ppframe$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_filecache(CArrRef params, bool init = true);
Variant cw_filecache$os_get(const char *s);
Variant &cw_filecache$os_lval(const char *s);
Variant cw_filecache$os_constant(const char *s);
Variant cw_filecache$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_xmlselect(CArrRef params, bool init = true);
Variant cw_xmlselect$os_get(const char *s);
Variant &cw_xmlselect$os_lval(const char *s);
Variant cw_xmlselect$os_constant(const char *s);
Variant cw_xmlselect$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_sitestatsupdate(CArrRef params, bool init = true);
Variant cw_sitestatsupdate$os_get(const char *s);
Variant &cw_sitestatsupdate$os_lval(const char *s);
Variant cw_sitestatsupdate$os_constant(const char *s);
Variant cw_sitestatsupdate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_replacementarray(CArrRef params, bool init = true);
Variant cw_replacementarray$os_get(const char *s);
Variant &cw_replacementarray$os_lval(const char *s);
Variant cw_replacementarray$os_constant(const char *s);
Variant cw_replacementarray$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_ppnode(CArrRef params, bool init = true);
Variant cw_ppnode$os_get(const char *s);
Variant &cw_ppnode$os_lval(const char *s);
Variant cw_ppnode$os_constant(const char *s);
Variant cw_ppnode$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_memcachedclientforwiki(CArrRef params, bool init = true);
Variant cw_memcachedclientforwiki$os_get(const char *s);
Variant &cw_memcachedclientforwiki$os_lval(const char *s);
Variant cw_memcachedclientforwiki$os_constant(const char *s);
Variant cw_memcachedclientforwiki$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_quicktemplate(CArrRef params, bool init = true);
Variant cw_quicktemplate$os_get(const char *s);
Variant &cw_quicktemplate$os_lval(const char *s);
Variant cw_quicktemplate$os_constant(const char *s);
Variant cw_quicktemplate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_searchupdate(CArrRef params, bool init = true);
Variant cw_searchupdate$os_get(const char *s);
Variant &cw_searchupdate$os_lval(const char *s);
Variant cw_searchupdate$os_constant(const char *s);
Variant cw_searchupdate$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_xml(CArrRef params, bool init = true);
Variant cw_xml$os_get(const char *s);
Variant &cw_xml$os_lval(const char *s);
Variant cw_xml$os_constant(const char *s);
Variant cw_xml$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_backlinkcache(CArrRef params, bool init = true);
Variant cw_backlinkcache$os_get(const char *s);
Variant &cw_backlinkcache$os_lval(const char *s);
Variant cw_backlinkcache$os_constant(const char *s);
Variant cw_backlinkcache$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_mwnamespace(CArrRef params, bool init = true);
Variant cw_mwnamespace$os_get(const char *s);
Variant &cw_mwnamespace$os_lval(const char *s);
Variant cw_mwnamespace$os_constant(const char *s);
Variant cw_mwnamespace$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_repogroup(CArrRef params, bool init = true);
Variant cw_repogroup$os_get(const char *s);
Variant &cw_repogroup$os_lval(const char *s);
Variant cw_repogroup$os_constant(const char *s);
Variant cw_repogroup$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_dbtestpreviewer(CArrRef params, bool init = true);
Variant cw_dbtestpreviewer$os_get(const char *s);
Variant &cw_dbtestpreviewer$os_lval(const char *s);
Variant cw_dbtestpreviewer$os_constant(const char *s);
Variant cw_dbtestpreviewer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_sanitizer(CArrRef params, bool init = true);
Variant cw_sanitizer$os_get(const char *s);
Variant &cw_sanitizer$os_lval(const char *s);
Variant cw_sanitizer$os_constant(const char *s);
Variant cw_sanitizer$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object co_stringutils(CArrRef params, bool init = true);
Variant cw_stringutils$os_get(const char *s);
Variant &cw_stringutils$os_lval(const char *s);
Variant cw_stringutils$os_constant(const char *s);
Variant cw_stringutils$os_invoke(const char *c, const char *s, CArrRef params, bool fatal = true);
Object create_object(const char *s, CArrRef params, bool init /* = true */,ObjectData* root /* = NULL*/) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 255) {
    case 8:
      HASH_CREATE_OBJECT(0x719E6FEB9B482608LL, backlinkcache);
      break;
    case 10:
      HASH_CREATE_OBJECT(0x45722B250518B20ALL, xmlselect);
      break;
    case 11:
      HASH_CREATE_OBJECT(0x3BE796379B4BAD0BLL, linkcache);
      break;
    case 12:
      HASH_CREATE_OBJECT(0x66F0A1235B93260CLL, skintemplate);
      break;
    case 17:
      HASH_CREATE_OBJECT(0x7DBDE360033FA011LL, magicword);
      break;
    case 19:
      HASH_CREATE_OBJECT(0x6A4099D912021513LL, request);
      HASH_CREATE_OBJECT(0x226A59FD69013113LL, ppframe);
      break;
    case 20:
      HASH_CREATE_OBJECT(0x1986122197FD4B14LL, xml);
      break;
    case 22:
      HASH_CREATE_OBJECT(0x07EFF3A256A25E16LL, mediawiki_i18n);
      break;
    case 23:
      HASH_CREATE_OBJECT(0x3FBE7F766A671217LL, preprocessor);
      break;
    case 24:
      HASH_CREATE_OBJECT(0x36EE676D53682E18LL, stubcontlang);
      break;
    case 25:
      HASH_CREATE_OBJECT(0x22BB4C671F0F2519LL, interwiki);
      break;
    case 26:
      HASH_CREATE_OBJECT(0x20B99026ECFF151ALL, linker);
      break;
    case 28:
      HASH_CREATE_OBJECT(0x4D05E87E5106D61CLL, messagecache);
      break;
    case 37:
      HASH_CREATE_OBJECT(0x48FAC7E5326CE625LL, citerator);
      break;
    case 45:
      HASH_CREATE_OBJECT(0x4638E1CDC3D8982DLL, language);
      break;
    case 49:
      HASH_CREATE_OBJECT(0x03FDCCDBB2FF3F31LL, doublereplacer);
      break;
    case 53:
      HASH_CREATE_OBJECT(0x1CF8E9EB317FDD35LL, sitestatsupdate);
      break;
    case 54:
      HASH_CREATE_OBJECT(0x3B5D5B251E2AC936LL, stripstate);
      break;
    case 66:
      HASH_CREATE_OBJECT(0x2986D323A4AA5C42LL, linksupdate);
      break;
    case 68:
      HASH_CREATE_OBJECT(0x6143CF5F8E106F44LL, preprocessor_dom);
      break;
    case 73:
      HASH_CREATE_OBJECT(0x1533BD5B65009649LL, outputpage);
      break;
    case 74:
      HASH_CREATE_OBJECT(0x1CFD43F7FBC5C84ALL, htmlfilecache);
      HASH_CREATE_OBJECT(0x72A49A22C192034ALL, memcached);
      HASH_CREATE_OBJECT(0x426AA156C563834ALL, regexlikereplacer);
      HASH_CREATE_OBJECT(0x1108000F8985794ALL, ctraversable);
      break;
    case 75:
      HASH_CREATE_OBJECT(0x0DDE353F4EC6BB4BLL, explodeiterator);
      break;
    case 76:
      HASH_CREATE_OBJECT(0x7ABD1C02DBBBE94CLL, ip);
      break;
    case 80:
      HASH_CREATE_OBJECT(0x36187993F616C150LL, status);
      HASH_CREATE_OBJECT(0x2240BAE4B416D850LL, replacer);
      break;
    case 86:
      HASH_CREATE_OBJECT(0x1969AA24C9CF8556LL, getdb);
      HASH_CREATE_OBJECT(0x1FE53785BD47A556LL, skinmonobook);
      break;
    case 89:
      HASH_CREATE_OBJECT(0x41F18C209AE6D959LL, ppframe_dom);
      break;
    case 91:
      HASH_CREATE_OBJECT(0x6680147A5CCA355BLL, magicwordarray);
      break;
    case 95:
      HASH_CREATE_OBJECT(0x359A492673A57F5FLL, utfnormal);
      break;
    case 99:
      HASH_CREATE_OBJECT(0x0E87D23B697F8063LL, skin);
      break;
    case 102:
      HASH_CREATE_OBJECT(0x4547ADA8DCEEF266LL, ppnode_dom);
      break;
    case 108:
      HASH_CREATE_OBJECT(0x57B36C06F20EC56CLL, quicktemplate);
      break;
    case 113:
      HASH_CREATE_OBJECT(0x5D070E434E31EF71LL, ansitermcolorer);
      break;
    case 115:
      HASH_CREATE_OBJECT(0x2FCD01C05FEE3B73LL, revision);
      HASH_CREATE_OBJECT(0x43A8AC6D22474973LL, ppnode);
      break;
    case 120:
      HASH_CREATE_OBJECT(0x618EBE2D39198378LL, linkbatch);
      HASH_CREATE_OBJECT(0x2A74B7AA03B66078LL, block);
      HASH_CREATE_OBJECT(0x023A5A2B5DB08278LL, titlearray);
      break;
    case 132:
      HASH_CREATE_OBJECT(0x3E6E89ADFEAEF284LL, stringutils);
      break;
    case 134:
      HASH_CREATE_OBJECT(0x40DBD1FF40845386LL, filecache);
      break;
    case 136:
      HASH_CREATE_OBJECT(0x69634AA9A7570D88LL, stubuser);
      HASH_CREATE_OBJECT(0x2EC2DAC3B2E2EF88LL, replacementarray);
      break;
    case 138:
      HASH_CREATE_OBJECT(0x5530B55C5821638ALL, provetestrecorder);
      HASH_CREATE_OBJECT(0x063C3117451D0D8ALL, recentchange);
      break;
    case 143:
      HASH_CREATE_OBJECT(0x465F5F4F142EDE8FLL, parser);
      break;
    case 144:
      HASH_CREATE_OBJECT(0x0301D2F46DDC7F90LL, linkholderarray);
      break;
    case 147:
      HASH_CREATE_OBJECT(0x7C178B2BFCA03693LL, parseroptions);
      break;
    case 148:
      HASH_CREATE_OBJECT(0x5237B58036959B94LL, monobooktemplate);
      break;
    case 150:
      HASH_CREATE_OBJECT(0x46EA41789271F796LL, mwexception);
      break;
    case 153:
      HASH_CREATE_OBJECT(0x235A20528EB49A99LL, article);
      break;
    case 154:
      HASH_CREATE_OBJECT(0x34DF39CB9333BC9ALL, dbtestrecorder);
      break;
    case 155:
      HASH_CREATE_OBJECT(0x4415A62DC36C579BLL, testrecorder);
      break;
    case 157:
      HASH_CREATE_OBJECT(0x5C22714D1FD2AD9DLL, stubobject);
      break;
    case 162:
      HASH_CREATE_OBJECT(0x3288321CF00F37A2LL, dummytermcolorer);
      break;
    case 165:
      HASH_CREATE_OBJECT(0x5A05AA8027F4B1A5LL, fakeconverter);
      break;
    case 167:
      HASH_CREATE_OBJECT(0x1EDAA558735CEAA7LL, hashtablereplacer);
      break;
    case 168:
      HASH_CREATE_OBJECT(0x292F71DA174B8FA8LL, coreparserfunctions);
      break;
    case 169:
      HASH_CREATE_OBJECT(0x4B93A289B6561FA9LL, memcachedclientforwiki);
      break;
    case 170:
      HASH_CREATE_OBJECT(0x5864D527BF8B26AALL, repogroup);
      break;
    case 173:
      HASH_CREATE_OBJECT(0x55DB9FC9EF75BBADLL, parsertest);
      break;
    case 175:
      HASH_CREATE_OBJECT(0x1B35D2121776BEAFLL, stubuserlang);
      break;
    case 177:
      HASH_CREATE_OBJECT(0x7E1D464E403FBAB1LL, profiler);
      break;
    case 179:
      HASH_CREATE_OBJECT(0x7CDB2B8E09A18DB3LL, htmlcacheupdate);
      break;
    case 182:
      HASH_CREATE_OBJECT(0x1FB0C5A1E83638B6LL, pptemplateframe_dom);
      break;
    case 188:
      HASH_CREATE_OBJECT(0x7D281747E80237BCLL, titlearrayfromresult);
      break;
    case 193:
      HASH_CREATE_OBJECT(0x758B1E2426B4FCC1LL, onlyincludereplacer);
      break;
    case 197:
      HASH_CREATE_OBJECT(0x7CBBE2907466E7C5LL, ppcustomframe_dom);
      break;
    case 203:
      HASH_CREATE_OBJECT(0x638DFF1F9ED1A7CBLL, mwnamespace);
      break;
    case 208:
      HASH_CREATE_OBJECT(0x1750D618C7769DD0LL, sanitizer);
      break;
    case 210:
      HASH_CREATE_OBJECT(0x6566F74128FE72D2LL, ppdstackelement);
      break;
    case 211:
      HASH_CREATE_OBJECT(0x6AEDB6AEFDF88DD3LL, errorpageerror);
      break;
    case 218:
      HASH_CREATE_OBJECT(0x1A6AAD89F34B37DALL, dbtestpreviewer);
      break;
    case 222:
      HASH_CREATE_OBJECT(0x1FFE0211B5F57DDELL, parseroutput);
      break;
    case 223:
      HASH_CREATE_OBJECT(0x3688C6C8D473D7DFLL, fakememcachedclient);
      break;
    case 224:
      HASH_CREATE_OBJECT(0x7548FC1BD48B04E0LL, searchupdate);
      break;
    case 227:
      HASH_CREATE_OBJECT(0x025B7C56B69451E3LL, ppdstack);
      break;
    case 233:
      HASH_CREATE_OBJECT(0x3F43904B4376CCE9LL, passworderror);
      HASH_CREATE_OBJECT(0x3539F3FCCD5001E9LL, fatalerror);
      break;
    case 235:
      HASH_CREATE_OBJECT(0x132FD54E0D0A99EBLL, title);
      break;
    case 237:
      HASH_CREATE_OBJECT(0x7C7CEDA62E334FEDLL, ppdpart);
      break;
    case 240:
      HASH_CREATE_OBJECT(0x3C53864DC03D20F0LL, user);
      break;
    case 241:
      HASH_CREATE_OBJECT(0x3B5AAAC3203573F1LL, proveparsertest);
      break;
    default:
      break;
  }
  return create_builtin_object(s, params, init, root);
}
Variant invoke_static_method(const char *s, const char *method, CArrRef params, bool fatal) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 255) {
    case 8:
      HASH_INVOKE_STATIC_METHOD(0x719E6FEB9B482608LL, backlinkcache);
      break;
    case 10:
      HASH_INVOKE_STATIC_METHOD(0x45722B250518B20ALL, xmlselect);
      break;
    case 11:
      HASH_INVOKE_STATIC_METHOD(0x3BE796379B4BAD0BLL, linkcache);
      break;
    case 12:
      HASH_INVOKE_STATIC_METHOD(0x66F0A1235B93260CLL, skintemplate);
      break;
    case 17:
      HASH_INVOKE_STATIC_METHOD(0x7DBDE360033FA011LL, magicword);
      break;
    case 19:
      HASH_INVOKE_STATIC_METHOD(0x6A4099D912021513LL, request);
      HASH_INVOKE_STATIC_METHOD(0x226A59FD69013113LL, ppframe);
      break;
    case 20:
      HASH_INVOKE_STATIC_METHOD(0x1986122197FD4B14LL, xml);
      break;
    case 22:
      HASH_INVOKE_STATIC_METHOD(0x07EFF3A256A25E16LL, mediawiki_i18n);
      break;
    case 23:
      HASH_INVOKE_STATIC_METHOD(0x3FBE7F766A671217LL, preprocessor);
      break;
    case 24:
      HASH_INVOKE_STATIC_METHOD(0x36EE676D53682E18LL, stubcontlang);
      break;
    case 25:
      HASH_INVOKE_STATIC_METHOD(0x22BB4C671F0F2519LL, interwiki);
      break;
    case 26:
      HASH_INVOKE_STATIC_METHOD(0x20B99026ECFF151ALL, linker);
      break;
    case 28:
      HASH_INVOKE_STATIC_METHOD(0x4D05E87E5106D61CLL, messagecache);
      break;
    case 37:
      HASH_INVOKE_STATIC_METHOD(0x48FAC7E5326CE625LL, citerator);
      break;
    case 45:
      HASH_INVOKE_STATIC_METHOD(0x4638E1CDC3D8982DLL, language);
      break;
    case 49:
      HASH_INVOKE_STATIC_METHOD(0x03FDCCDBB2FF3F31LL, doublereplacer);
      break;
    case 53:
      HASH_INVOKE_STATIC_METHOD(0x1CF8E9EB317FDD35LL, sitestatsupdate);
      break;
    case 54:
      HASH_INVOKE_STATIC_METHOD(0x3B5D5B251E2AC936LL, stripstate);
      break;
    case 66:
      HASH_INVOKE_STATIC_METHOD(0x2986D323A4AA5C42LL, linksupdate);
      break;
    case 68:
      HASH_INVOKE_STATIC_METHOD(0x6143CF5F8E106F44LL, preprocessor_dom);
      break;
    case 73:
      HASH_INVOKE_STATIC_METHOD(0x1533BD5B65009649LL, outputpage);
      break;
    case 74:
      HASH_INVOKE_STATIC_METHOD(0x1CFD43F7FBC5C84ALL, htmlfilecache);
      HASH_INVOKE_STATIC_METHOD(0x72A49A22C192034ALL, memcached);
      HASH_INVOKE_STATIC_METHOD(0x426AA156C563834ALL, regexlikereplacer);
      HASH_INVOKE_STATIC_METHOD(0x1108000F8985794ALL, ctraversable);
      break;
    case 75:
      HASH_INVOKE_STATIC_METHOD(0x0DDE353F4EC6BB4BLL, explodeiterator);
      break;
    case 76:
      HASH_INVOKE_STATIC_METHOD(0x7ABD1C02DBBBE94CLL, ip);
      break;
    case 80:
      HASH_INVOKE_STATIC_METHOD(0x36187993F616C150LL, status);
      HASH_INVOKE_STATIC_METHOD(0x2240BAE4B416D850LL, replacer);
      break;
    case 86:
      HASH_INVOKE_STATIC_METHOD(0x1969AA24C9CF8556LL, getdb);
      HASH_INVOKE_STATIC_METHOD(0x1FE53785BD47A556LL, skinmonobook);
      break;
    case 89:
      HASH_INVOKE_STATIC_METHOD(0x41F18C209AE6D959LL, ppframe_dom);
      break;
    case 91:
      HASH_INVOKE_STATIC_METHOD(0x6680147A5CCA355BLL, magicwordarray);
      break;
    case 95:
      HASH_INVOKE_STATIC_METHOD(0x359A492673A57F5FLL, utfnormal);
      break;
    case 99:
      HASH_INVOKE_STATIC_METHOD(0x0E87D23B697F8063LL, skin);
      break;
    case 102:
      HASH_INVOKE_STATIC_METHOD(0x4547ADA8DCEEF266LL, ppnode_dom);
      break;
    case 108:
      HASH_INVOKE_STATIC_METHOD(0x57B36C06F20EC56CLL, quicktemplate);
      break;
    case 113:
      HASH_INVOKE_STATIC_METHOD(0x5D070E434E31EF71LL, ansitermcolorer);
      break;
    case 115:
      HASH_INVOKE_STATIC_METHOD(0x2FCD01C05FEE3B73LL, revision);
      HASH_INVOKE_STATIC_METHOD(0x43A8AC6D22474973LL, ppnode);
      break;
    case 120:
      HASH_INVOKE_STATIC_METHOD(0x618EBE2D39198378LL, linkbatch);
      HASH_INVOKE_STATIC_METHOD(0x2A74B7AA03B66078LL, block);
      HASH_INVOKE_STATIC_METHOD(0x023A5A2B5DB08278LL, titlearray);
      break;
    case 132:
      HASH_INVOKE_STATIC_METHOD(0x3E6E89ADFEAEF284LL, stringutils);
      break;
    case 134:
      HASH_INVOKE_STATIC_METHOD(0x40DBD1FF40845386LL, filecache);
      break;
    case 136:
      HASH_INVOKE_STATIC_METHOD(0x69634AA9A7570D88LL, stubuser);
      HASH_INVOKE_STATIC_METHOD(0x2EC2DAC3B2E2EF88LL, replacementarray);
      break;
    case 138:
      HASH_INVOKE_STATIC_METHOD(0x5530B55C5821638ALL, provetestrecorder);
      HASH_INVOKE_STATIC_METHOD(0x063C3117451D0D8ALL, recentchange);
      break;
    case 143:
      HASH_INVOKE_STATIC_METHOD(0x465F5F4F142EDE8FLL, parser);
      break;
    case 144:
      HASH_INVOKE_STATIC_METHOD(0x0301D2F46DDC7F90LL, linkholderarray);
      break;
    case 147:
      HASH_INVOKE_STATIC_METHOD(0x7C178B2BFCA03693LL, parseroptions);
      break;
    case 148:
      HASH_INVOKE_STATIC_METHOD(0x5237B58036959B94LL, monobooktemplate);
      break;
    case 150:
      HASH_INVOKE_STATIC_METHOD(0x46EA41789271F796LL, mwexception);
      break;
    case 153:
      HASH_INVOKE_STATIC_METHOD(0x235A20528EB49A99LL, article);
      break;
    case 154:
      HASH_INVOKE_STATIC_METHOD(0x34DF39CB9333BC9ALL, dbtestrecorder);
      break;
    case 155:
      HASH_INVOKE_STATIC_METHOD(0x4415A62DC36C579BLL, testrecorder);
      break;
    case 157:
      HASH_INVOKE_STATIC_METHOD(0x5C22714D1FD2AD9DLL, stubobject);
      break;
    case 162:
      HASH_INVOKE_STATIC_METHOD(0x3288321CF00F37A2LL, dummytermcolorer);
      break;
    case 165:
      HASH_INVOKE_STATIC_METHOD(0x5A05AA8027F4B1A5LL, fakeconverter);
      break;
    case 167:
      HASH_INVOKE_STATIC_METHOD(0x1EDAA558735CEAA7LL, hashtablereplacer);
      break;
    case 168:
      HASH_INVOKE_STATIC_METHOD(0x292F71DA174B8FA8LL, coreparserfunctions);
      break;
    case 169:
      HASH_INVOKE_STATIC_METHOD(0x4B93A289B6561FA9LL, memcachedclientforwiki);
      break;
    case 170:
      HASH_INVOKE_STATIC_METHOD(0x5864D527BF8B26AALL, repogroup);
      break;
    case 173:
      HASH_INVOKE_STATIC_METHOD(0x55DB9FC9EF75BBADLL, parsertest);
      break;
    case 175:
      HASH_INVOKE_STATIC_METHOD(0x1B35D2121776BEAFLL, stubuserlang);
      break;
    case 177:
      HASH_INVOKE_STATIC_METHOD(0x7E1D464E403FBAB1LL, profiler);
      break;
    case 179:
      HASH_INVOKE_STATIC_METHOD(0x7CDB2B8E09A18DB3LL, htmlcacheupdate);
      break;
    case 182:
      HASH_INVOKE_STATIC_METHOD(0x1FB0C5A1E83638B6LL, pptemplateframe_dom);
      break;
    case 188:
      HASH_INVOKE_STATIC_METHOD(0x7D281747E80237BCLL, titlearrayfromresult);
      break;
    case 193:
      HASH_INVOKE_STATIC_METHOD(0x758B1E2426B4FCC1LL, onlyincludereplacer);
      break;
    case 197:
      HASH_INVOKE_STATIC_METHOD(0x7CBBE2907466E7C5LL, ppcustomframe_dom);
      break;
    case 203:
      HASH_INVOKE_STATIC_METHOD(0x638DFF1F9ED1A7CBLL, mwnamespace);
      break;
    case 208:
      HASH_INVOKE_STATIC_METHOD(0x1750D618C7769DD0LL, sanitizer);
      break;
    case 210:
      HASH_INVOKE_STATIC_METHOD(0x6566F74128FE72D2LL, ppdstackelement);
      break;
    case 211:
      HASH_INVOKE_STATIC_METHOD(0x6AEDB6AEFDF88DD3LL, errorpageerror);
      break;
    case 218:
      HASH_INVOKE_STATIC_METHOD(0x1A6AAD89F34B37DALL, dbtestpreviewer);
      break;
    case 222:
      HASH_INVOKE_STATIC_METHOD(0x1FFE0211B5F57DDELL, parseroutput);
      break;
    case 223:
      HASH_INVOKE_STATIC_METHOD(0x3688C6C8D473D7DFLL, fakememcachedclient);
      break;
    case 224:
      HASH_INVOKE_STATIC_METHOD(0x7548FC1BD48B04E0LL, searchupdate);
      break;
    case 227:
      HASH_INVOKE_STATIC_METHOD(0x025B7C56B69451E3LL, ppdstack);
      break;
    case 233:
      HASH_INVOKE_STATIC_METHOD(0x3F43904B4376CCE9LL, passworderror);
      HASH_INVOKE_STATIC_METHOD(0x3539F3FCCD5001E9LL, fatalerror);
      break;
    case 235:
      HASH_INVOKE_STATIC_METHOD(0x132FD54E0D0A99EBLL, title);
      break;
    case 237:
      HASH_INVOKE_STATIC_METHOD(0x7C7CEDA62E334FEDLL, ppdpart);
      break;
    case 240:
      HASH_INVOKE_STATIC_METHOD(0x3C53864DC03D20F0LL, user);
      break;
    case 241:
      HASH_INVOKE_STATIC_METHOD(0x3B5AAAC3203573F1LL, proveparsertest);
      break;
    default:
      break;
  }
  return invoke_builtin_static_method(s, method, params, fatal);
}
Variant get_static_property(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 255) {
    case 8:
      HASH_GET_STATIC_PROPERTY(0x719E6FEB9B482608LL, backlinkcache);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY(0x45722B250518B20ALL, xmlselect);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY(0x3BE796379B4BAD0BLL, linkcache);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY(0x66F0A1235B93260CLL, skintemplate);
      break;
    case 17:
      HASH_GET_STATIC_PROPERTY(0x7DBDE360033FA011LL, magicword);
      break;
    case 19:
      HASH_GET_STATIC_PROPERTY(0x6A4099D912021513LL, request);
      HASH_GET_STATIC_PROPERTY(0x226A59FD69013113LL, ppframe);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY(0x1986122197FD4B14LL, xml);
      break;
    case 22:
      HASH_GET_STATIC_PROPERTY(0x07EFF3A256A25E16LL, mediawiki_i18n);
      break;
    case 23:
      HASH_GET_STATIC_PROPERTY(0x3FBE7F766A671217LL, preprocessor);
      break;
    case 24:
      HASH_GET_STATIC_PROPERTY(0x36EE676D53682E18LL, stubcontlang);
      break;
    case 25:
      HASH_GET_STATIC_PROPERTY(0x22BB4C671F0F2519LL, interwiki);
      break;
    case 26:
      HASH_GET_STATIC_PROPERTY(0x20B99026ECFF151ALL, linker);
      break;
    case 28:
      HASH_GET_STATIC_PROPERTY(0x4D05E87E5106D61CLL, messagecache);
      break;
    case 37:
      HASH_GET_STATIC_PROPERTY(0x48FAC7E5326CE625LL, citerator);
      break;
    case 45:
      HASH_GET_STATIC_PROPERTY(0x4638E1CDC3D8982DLL, language);
      break;
    case 49:
      HASH_GET_STATIC_PROPERTY(0x03FDCCDBB2FF3F31LL, doublereplacer);
      break;
    case 53:
      HASH_GET_STATIC_PROPERTY(0x1CF8E9EB317FDD35LL, sitestatsupdate);
      break;
    case 54:
      HASH_GET_STATIC_PROPERTY(0x3B5D5B251E2AC936LL, stripstate);
      break;
    case 66:
      HASH_GET_STATIC_PROPERTY(0x2986D323A4AA5C42LL, linksupdate);
      break;
    case 68:
      HASH_GET_STATIC_PROPERTY(0x6143CF5F8E106F44LL, preprocessor_dom);
      break;
    case 73:
      HASH_GET_STATIC_PROPERTY(0x1533BD5B65009649LL, outputpage);
      break;
    case 74:
      HASH_GET_STATIC_PROPERTY(0x1CFD43F7FBC5C84ALL, htmlfilecache);
      HASH_GET_STATIC_PROPERTY(0x72A49A22C192034ALL, memcached);
      HASH_GET_STATIC_PROPERTY(0x426AA156C563834ALL, regexlikereplacer);
      HASH_GET_STATIC_PROPERTY(0x1108000F8985794ALL, ctraversable);
      break;
    case 75:
      HASH_GET_STATIC_PROPERTY(0x0DDE353F4EC6BB4BLL, explodeiterator);
      break;
    case 76:
      HASH_GET_STATIC_PROPERTY(0x7ABD1C02DBBBE94CLL, ip);
      break;
    case 80:
      HASH_GET_STATIC_PROPERTY(0x36187993F616C150LL, status);
      HASH_GET_STATIC_PROPERTY(0x2240BAE4B416D850LL, replacer);
      break;
    case 86:
      HASH_GET_STATIC_PROPERTY(0x1969AA24C9CF8556LL, getdb);
      HASH_GET_STATIC_PROPERTY(0x1FE53785BD47A556LL, skinmonobook);
      break;
    case 89:
      HASH_GET_STATIC_PROPERTY(0x41F18C209AE6D959LL, ppframe_dom);
      break;
    case 91:
      HASH_GET_STATIC_PROPERTY(0x6680147A5CCA355BLL, magicwordarray);
      break;
    case 95:
      HASH_GET_STATIC_PROPERTY(0x359A492673A57F5FLL, utfnormal);
      break;
    case 99:
      HASH_GET_STATIC_PROPERTY(0x0E87D23B697F8063LL, skin);
      break;
    case 102:
      HASH_GET_STATIC_PROPERTY(0x4547ADA8DCEEF266LL, ppnode_dom);
      break;
    case 108:
      HASH_GET_STATIC_PROPERTY(0x57B36C06F20EC56CLL, quicktemplate);
      break;
    case 113:
      HASH_GET_STATIC_PROPERTY(0x5D070E434E31EF71LL, ansitermcolorer);
      break;
    case 115:
      HASH_GET_STATIC_PROPERTY(0x2FCD01C05FEE3B73LL, revision);
      HASH_GET_STATIC_PROPERTY(0x43A8AC6D22474973LL, ppnode);
      break;
    case 120:
      HASH_GET_STATIC_PROPERTY(0x618EBE2D39198378LL, linkbatch);
      HASH_GET_STATIC_PROPERTY(0x2A74B7AA03B66078LL, block);
      HASH_GET_STATIC_PROPERTY(0x023A5A2B5DB08278LL, titlearray);
      break;
    case 132:
      HASH_GET_STATIC_PROPERTY(0x3E6E89ADFEAEF284LL, stringutils);
      break;
    case 134:
      HASH_GET_STATIC_PROPERTY(0x40DBD1FF40845386LL, filecache);
      break;
    case 136:
      HASH_GET_STATIC_PROPERTY(0x69634AA9A7570D88LL, stubuser);
      HASH_GET_STATIC_PROPERTY(0x2EC2DAC3B2E2EF88LL, replacementarray);
      break;
    case 138:
      HASH_GET_STATIC_PROPERTY(0x5530B55C5821638ALL, provetestrecorder);
      HASH_GET_STATIC_PROPERTY(0x063C3117451D0D8ALL, recentchange);
      break;
    case 143:
      HASH_GET_STATIC_PROPERTY(0x465F5F4F142EDE8FLL, parser);
      break;
    case 144:
      HASH_GET_STATIC_PROPERTY(0x0301D2F46DDC7F90LL, linkholderarray);
      break;
    case 147:
      HASH_GET_STATIC_PROPERTY(0x7C178B2BFCA03693LL, parseroptions);
      break;
    case 148:
      HASH_GET_STATIC_PROPERTY(0x5237B58036959B94LL, monobooktemplate);
      break;
    case 150:
      HASH_GET_STATIC_PROPERTY(0x46EA41789271F796LL, mwexception);
      break;
    case 153:
      HASH_GET_STATIC_PROPERTY(0x235A20528EB49A99LL, article);
      break;
    case 154:
      HASH_GET_STATIC_PROPERTY(0x34DF39CB9333BC9ALL, dbtestrecorder);
      break;
    case 155:
      HASH_GET_STATIC_PROPERTY(0x4415A62DC36C579BLL, testrecorder);
      break;
    case 157:
      HASH_GET_STATIC_PROPERTY(0x5C22714D1FD2AD9DLL, stubobject);
      break;
    case 162:
      HASH_GET_STATIC_PROPERTY(0x3288321CF00F37A2LL, dummytermcolorer);
      break;
    case 165:
      HASH_GET_STATIC_PROPERTY(0x5A05AA8027F4B1A5LL, fakeconverter);
      break;
    case 167:
      HASH_GET_STATIC_PROPERTY(0x1EDAA558735CEAA7LL, hashtablereplacer);
      break;
    case 168:
      HASH_GET_STATIC_PROPERTY(0x292F71DA174B8FA8LL, coreparserfunctions);
      break;
    case 169:
      HASH_GET_STATIC_PROPERTY(0x4B93A289B6561FA9LL, memcachedclientforwiki);
      break;
    case 170:
      HASH_GET_STATIC_PROPERTY(0x5864D527BF8B26AALL, repogroup);
      break;
    case 173:
      HASH_GET_STATIC_PROPERTY(0x55DB9FC9EF75BBADLL, parsertest);
      break;
    case 175:
      HASH_GET_STATIC_PROPERTY(0x1B35D2121776BEAFLL, stubuserlang);
      break;
    case 177:
      HASH_GET_STATIC_PROPERTY(0x7E1D464E403FBAB1LL, profiler);
      break;
    case 179:
      HASH_GET_STATIC_PROPERTY(0x7CDB2B8E09A18DB3LL, htmlcacheupdate);
      break;
    case 182:
      HASH_GET_STATIC_PROPERTY(0x1FB0C5A1E83638B6LL, pptemplateframe_dom);
      break;
    case 188:
      HASH_GET_STATIC_PROPERTY(0x7D281747E80237BCLL, titlearrayfromresult);
      break;
    case 193:
      HASH_GET_STATIC_PROPERTY(0x758B1E2426B4FCC1LL, onlyincludereplacer);
      break;
    case 197:
      HASH_GET_STATIC_PROPERTY(0x7CBBE2907466E7C5LL, ppcustomframe_dom);
      break;
    case 203:
      HASH_GET_STATIC_PROPERTY(0x638DFF1F9ED1A7CBLL, mwnamespace);
      break;
    case 208:
      HASH_GET_STATIC_PROPERTY(0x1750D618C7769DD0LL, sanitizer);
      break;
    case 210:
      HASH_GET_STATIC_PROPERTY(0x6566F74128FE72D2LL, ppdstackelement);
      break;
    case 211:
      HASH_GET_STATIC_PROPERTY(0x6AEDB6AEFDF88DD3LL, errorpageerror);
      break;
    case 218:
      HASH_GET_STATIC_PROPERTY(0x1A6AAD89F34B37DALL, dbtestpreviewer);
      break;
    case 222:
      HASH_GET_STATIC_PROPERTY(0x1FFE0211B5F57DDELL, parseroutput);
      break;
    case 223:
      HASH_GET_STATIC_PROPERTY(0x3688C6C8D473D7DFLL, fakememcachedclient);
      break;
    case 224:
      HASH_GET_STATIC_PROPERTY(0x7548FC1BD48B04E0LL, searchupdate);
      break;
    case 227:
      HASH_GET_STATIC_PROPERTY(0x025B7C56B69451E3LL, ppdstack);
      break;
    case 233:
      HASH_GET_STATIC_PROPERTY(0x3F43904B4376CCE9LL, passworderror);
      HASH_GET_STATIC_PROPERTY(0x3539F3FCCD5001E9LL, fatalerror);
      break;
    case 235:
      HASH_GET_STATIC_PROPERTY(0x132FD54E0D0A99EBLL, title);
      break;
    case 237:
      HASH_GET_STATIC_PROPERTY(0x7C7CEDA62E334FEDLL, ppdpart);
      break;
    case 240:
      HASH_GET_STATIC_PROPERTY(0x3C53864DC03D20F0LL, user);
      break;
    case 241:
      HASH_GET_STATIC_PROPERTY(0x3B5AAAC3203573F1LL, proveparsertest);
      break;
    default:
      break;
  }
  return get_builtin_static_property(s, prop);
}
Variant *get_static_property_lv(const char *s, const char *prop) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 255) {
    case 8:
      HASH_GET_STATIC_PROPERTY_LV(0x719E6FEB9B482608LL, backlinkcache);
      break;
    case 10:
      HASH_GET_STATIC_PROPERTY_LV(0x45722B250518B20ALL, xmlselect);
      break;
    case 11:
      HASH_GET_STATIC_PROPERTY_LV(0x3BE796379B4BAD0BLL, linkcache);
      break;
    case 12:
      HASH_GET_STATIC_PROPERTY_LV(0x66F0A1235B93260CLL, skintemplate);
      break;
    case 17:
      HASH_GET_STATIC_PROPERTY_LV(0x7DBDE360033FA011LL, magicword);
      break;
    case 19:
      HASH_GET_STATIC_PROPERTY_LV(0x6A4099D912021513LL, request);
      HASH_GET_STATIC_PROPERTY_LV(0x226A59FD69013113LL, ppframe);
      break;
    case 20:
      HASH_GET_STATIC_PROPERTY_LV(0x1986122197FD4B14LL, xml);
      break;
    case 22:
      HASH_GET_STATIC_PROPERTY_LV(0x07EFF3A256A25E16LL, mediawiki_i18n);
      break;
    case 23:
      HASH_GET_STATIC_PROPERTY_LV(0x3FBE7F766A671217LL, preprocessor);
      break;
    case 24:
      HASH_GET_STATIC_PROPERTY_LV(0x36EE676D53682E18LL, stubcontlang);
      break;
    case 25:
      HASH_GET_STATIC_PROPERTY_LV(0x22BB4C671F0F2519LL, interwiki);
      break;
    case 26:
      HASH_GET_STATIC_PROPERTY_LV(0x20B99026ECFF151ALL, linker);
      break;
    case 28:
      HASH_GET_STATIC_PROPERTY_LV(0x4D05E87E5106D61CLL, messagecache);
      break;
    case 37:
      HASH_GET_STATIC_PROPERTY_LV(0x48FAC7E5326CE625LL, citerator);
      break;
    case 45:
      HASH_GET_STATIC_PROPERTY_LV(0x4638E1CDC3D8982DLL, language);
      break;
    case 49:
      HASH_GET_STATIC_PROPERTY_LV(0x03FDCCDBB2FF3F31LL, doublereplacer);
      break;
    case 53:
      HASH_GET_STATIC_PROPERTY_LV(0x1CF8E9EB317FDD35LL, sitestatsupdate);
      break;
    case 54:
      HASH_GET_STATIC_PROPERTY_LV(0x3B5D5B251E2AC936LL, stripstate);
      break;
    case 66:
      HASH_GET_STATIC_PROPERTY_LV(0x2986D323A4AA5C42LL, linksupdate);
      break;
    case 68:
      HASH_GET_STATIC_PROPERTY_LV(0x6143CF5F8E106F44LL, preprocessor_dom);
      break;
    case 73:
      HASH_GET_STATIC_PROPERTY_LV(0x1533BD5B65009649LL, outputpage);
      break;
    case 74:
      HASH_GET_STATIC_PROPERTY_LV(0x1CFD43F7FBC5C84ALL, htmlfilecache);
      HASH_GET_STATIC_PROPERTY_LV(0x72A49A22C192034ALL, memcached);
      HASH_GET_STATIC_PROPERTY_LV(0x426AA156C563834ALL, regexlikereplacer);
      HASH_GET_STATIC_PROPERTY_LV(0x1108000F8985794ALL, ctraversable);
      break;
    case 75:
      HASH_GET_STATIC_PROPERTY_LV(0x0DDE353F4EC6BB4BLL, explodeiterator);
      break;
    case 76:
      HASH_GET_STATIC_PROPERTY_LV(0x7ABD1C02DBBBE94CLL, ip);
      break;
    case 80:
      HASH_GET_STATIC_PROPERTY_LV(0x36187993F616C150LL, status);
      HASH_GET_STATIC_PROPERTY_LV(0x2240BAE4B416D850LL, replacer);
      break;
    case 86:
      HASH_GET_STATIC_PROPERTY_LV(0x1969AA24C9CF8556LL, getdb);
      HASH_GET_STATIC_PROPERTY_LV(0x1FE53785BD47A556LL, skinmonobook);
      break;
    case 89:
      HASH_GET_STATIC_PROPERTY_LV(0x41F18C209AE6D959LL, ppframe_dom);
      break;
    case 91:
      HASH_GET_STATIC_PROPERTY_LV(0x6680147A5CCA355BLL, magicwordarray);
      break;
    case 95:
      HASH_GET_STATIC_PROPERTY_LV(0x359A492673A57F5FLL, utfnormal);
      break;
    case 99:
      HASH_GET_STATIC_PROPERTY_LV(0x0E87D23B697F8063LL, skin);
      break;
    case 102:
      HASH_GET_STATIC_PROPERTY_LV(0x4547ADA8DCEEF266LL, ppnode_dom);
      break;
    case 108:
      HASH_GET_STATIC_PROPERTY_LV(0x57B36C06F20EC56CLL, quicktemplate);
      break;
    case 113:
      HASH_GET_STATIC_PROPERTY_LV(0x5D070E434E31EF71LL, ansitermcolorer);
      break;
    case 115:
      HASH_GET_STATIC_PROPERTY_LV(0x2FCD01C05FEE3B73LL, revision);
      HASH_GET_STATIC_PROPERTY_LV(0x43A8AC6D22474973LL, ppnode);
      break;
    case 120:
      HASH_GET_STATIC_PROPERTY_LV(0x618EBE2D39198378LL, linkbatch);
      HASH_GET_STATIC_PROPERTY_LV(0x2A74B7AA03B66078LL, block);
      HASH_GET_STATIC_PROPERTY_LV(0x023A5A2B5DB08278LL, titlearray);
      break;
    case 132:
      HASH_GET_STATIC_PROPERTY_LV(0x3E6E89ADFEAEF284LL, stringutils);
      break;
    case 134:
      HASH_GET_STATIC_PROPERTY_LV(0x40DBD1FF40845386LL, filecache);
      break;
    case 136:
      HASH_GET_STATIC_PROPERTY_LV(0x69634AA9A7570D88LL, stubuser);
      HASH_GET_STATIC_PROPERTY_LV(0x2EC2DAC3B2E2EF88LL, replacementarray);
      break;
    case 138:
      HASH_GET_STATIC_PROPERTY_LV(0x5530B55C5821638ALL, provetestrecorder);
      HASH_GET_STATIC_PROPERTY_LV(0x063C3117451D0D8ALL, recentchange);
      break;
    case 143:
      HASH_GET_STATIC_PROPERTY_LV(0x465F5F4F142EDE8FLL, parser);
      break;
    case 144:
      HASH_GET_STATIC_PROPERTY_LV(0x0301D2F46DDC7F90LL, linkholderarray);
      break;
    case 147:
      HASH_GET_STATIC_PROPERTY_LV(0x7C178B2BFCA03693LL, parseroptions);
      break;
    case 148:
      HASH_GET_STATIC_PROPERTY_LV(0x5237B58036959B94LL, monobooktemplate);
      break;
    case 150:
      HASH_GET_STATIC_PROPERTY_LV(0x46EA41789271F796LL, mwexception);
      break;
    case 153:
      HASH_GET_STATIC_PROPERTY_LV(0x235A20528EB49A99LL, article);
      break;
    case 154:
      HASH_GET_STATIC_PROPERTY_LV(0x34DF39CB9333BC9ALL, dbtestrecorder);
      break;
    case 155:
      HASH_GET_STATIC_PROPERTY_LV(0x4415A62DC36C579BLL, testrecorder);
      break;
    case 157:
      HASH_GET_STATIC_PROPERTY_LV(0x5C22714D1FD2AD9DLL, stubobject);
      break;
    case 162:
      HASH_GET_STATIC_PROPERTY_LV(0x3288321CF00F37A2LL, dummytermcolorer);
      break;
    case 165:
      HASH_GET_STATIC_PROPERTY_LV(0x5A05AA8027F4B1A5LL, fakeconverter);
      break;
    case 167:
      HASH_GET_STATIC_PROPERTY_LV(0x1EDAA558735CEAA7LL, hashtablereplacer);
      break;
    case 168:
      HASH_GET_STATIC_PROPERTY_LV(0x292F71DA174B8FA8LL, coreparserfunctions);
      break;
    case 169:
      HASH_GET_STATIC_PROPERTY_LV(0x4B93A289B6561FA9LL, memcachedclientforwiki);
      break;
    case 170:
      HASH_GET_STATIC_PROPERTY_LV(0x5864D527BF8B26AALL, repogroup);
      break;
    case 173:
      HASH_GET_STATIC_PROPERTY_LV(0x55DB9FC9EF75BBADLL, parsertest);
      break;
    case 175:
      HASH_GET_STATIC_PROPERTY_LV(0x1B35D2121776BEAFLL, stubuserlang);
      break;
    case 177:
      HASH_GET_STATIC_PROPERTY_LV(0x7E1D464E403FBAB1LL, profiler);
      break;
    case 179:
      HASH_GET_STATIC_PROPERTY_LV(0x7CDB2B8E09A18DB3LL, htmlcacheupdate);
      break;
    case 182:
      HASH_GET_STATIC_PROPERTY_LV(0x1FB0C5A1E83638B6LL, pptemplateframe_dom);
      break;
    case 188:
      HASH_GET_STATIC_PROPERTY_LV(0x7D281747E80237BCLL, titlearrayfromresult);
      break;
    case 193:
      HASH_GET_STATIC_PROPERTY_LV(0x758B1E2426B4FCC1LL, onlyincludereplacer);
      break;
    case 197:
      HASH_GET_STATIC_PROPERTY_LV(0x7CBBE2907466E7C5LL, ppcustomframe_dom);
      break;
    case 203:
      HASH_GET_STATIC_PROPERTY_LV(0x638DFF1F9ED1A7CBLL, mwnamespace);
      break;
    case 208:
      HASH_GET_STATIC_PROPERTY_LV(0x1750D618C7769DD0LL, sanitizer);
      break;
    case 210:
      HASH_GET_STATIC_PROPERTY_LV(0x6566F74128FE72D2LL, ppdstackelement);
      break;
    case 211:
      HASH_GET_STATIC_PROPERTY_LV(0x6AEDB6AEFDF88DD3LL, errorpageerror);
      break;
    case 218:
      HASH_GET_STATIC_PROPERTY_LV(0x1A6AAD89F34B37DALL, dbtestpreviewer);
      break;
    case 222:
      HASH_GET_STATIC_PROPERTY_LV(0x1FFE0211B5F57DDELL, parseroutput);
      break;
    case 223:
      HASH_GET_STATIC_PROPERTY_LV(0x3688C6C8D473D7DFLL, fakememcachedclient);
      break;
    case 224:
      HASH_GET_STATIC_PROPERTY_LV(0x7548FC1BD48B04E0LL, searchupdate);
      break;
    case 227:
      HASH_GET_STATIC_PROPERTY_LV(0x025B7C56B69451E3LL, ppdstack);
      break;
    case 233:
      HASH_GET_STATIC_PROPERTY_LV(0x3F43904B4376CCE9LL, passworderror);
      HASH_GET_STATIC_PROPERTY_LV(0x3539F3FCCD5001E9LL, fatalerror);
      break;
    case 235:
      HASH_GET_STATIC_PROPERTY_LV(0x132FD54E0D0A99EBLL, title);
      break;
    case 237:
      HASH_GET_STATIC_PROPERTY_LV(0x7C7CEDA62E334FEDLL, ppdpart);
      break;
    case 240:
      HASH_GET_STATIC_PROPERTY_LV(0x3C53864DC03D20F0LL, user);
      break;
    case 241:
      HASH_GET_STATIC_PROPERTY_LV(0x3B5AAAC3203573F1LL, proveparsertest);
      break;
    default:
      break;
  }
  return get_builtin_static_property_lv(s, prop);
}
Variant get_class_constant(const char *s, const char *constant) {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string_i(s);
  switch (hash & 255) {
    case 8:
      HASH_GET_CLASS_CONSTANT(0x719E6FEB9B482608LL, backlinkcache);
      break;
    case 10:
      HASH_GET_CLASS_CONSTANT(0x45722B250518B20ALL, xmlselect);
      break;
    case 11:
      HASH_GET_CLASS_CONSTANT(0x3BE796379B4BAD0BLL, linkcache);
      break;
    case 12:
      HASH_GET_CLASS_CONSTANT(0x66F0A1235B93260CLL, skintemplate);
      break;
    case 17:
      HASH_GET_CLASS_CONSTANT(0x7DBDE360033FA011LL, magicword);
      break;
    case 19:
      HASH_GET_CLASS_CONSTANT(0x6A4099D912021513LL, request);
      HASH_GET_CLASS_CONSTANT(0x226A59FD69013113LL, ppframe);
      break;
    case 20:
      HASH_GET_CLASS_CONSTANT(0x1986122197FD4B14LL, xml);
      break;
    case 22:
      HASH_GET_CLASS_CONSTANT(0x07EFF3A256A25E16LL, mediawiki_i18n);
      break;
    case 23:
      HASH_GET_CLASS_CONSTANT(0x3FBE7F766A671217LL, preprocessor);
      break;
    case 24:
      HASH_GET_CLASS_CONSTANT(0x36EE676D53682E18LL, stubcontlang);
      break;
    case 25:
      HASH_GET_CLASS_CONSTANT(0x22BB4C671F0F2519LL, interwiki);
      break;
    case 26:
      HASH_GET_CLASS_CONSTANT(0x20B99026ECFF151ALL, linker);
      break;
    case 28:
      HASH_GET_CLASS_CONSTANT(0x4D05E87E5106D61CLL, messagecache);
      break;
    case 37:
      HASH_GET_CLASS_CONSTANT(0x48FAC7E5326CE625LL, citerator);
      break;
    case 45:
      HASH_GET_CLASS_CONSTANT(0x4638E1CDC3D8982DLL, language);
      break;
    case 49:
      HASH_GET_CLASS_CONSTANT(0x03FDCCDBB2FF3F31LL, doublereplacer);
      break;
    case 53:
      HASH_GET_CLASS_CONSTANT(0x1CF8E9EB317FDD35LL, sitestatsupdate);
      break;
    case 54:
      HASH_GET_CLASS_CONSTANT(0x3B5D5B251E2AC936LL, stripstate);
      break;
    case 66:
      HASH_GET_CLASS_CONSTANT(0x2986D323A4AA5C42LL, linksupdate);
      break;
    case 68:
      HASH_GET_CLASS_CONSTANT(0x6143CF5F8E106F44LL, preprocessor_dom);
      break;
    case 73:
      HASH_GET_CLASS_CONSTANT(0x1533BD5B65009649LL, outputpage);
      break;
    case 74:
      HASH_GET_CLASS_CONSTANT(0x1CFD43F7FBC5C84ALL, htmlfilecache);
      HASH_GET_CLASS_CONSTANT(0x72A49A22C192034ALL, memcached);
      HASH_GET_CLASS_CONSTANT(0x426AA156C563834ALL, regexlikereplacer);
      HASH_GET_CLASS_CONSTANT(0x1108000F8985794ALL, ctraversable);
      break;
    case 75:
      HASH_GET_CLASS_CONSTANT(0x0DDE353F4EC6BB4BLL, explodeiterator);
      break;
    case 76:
      HASH_GET_CLASS_CONSTANT(0x7ABD1C02DBBBE94CLL, ip);
      break;
    case 80:
      HASH_GET_CLASS_CONSTANT(0x36187993F616C150LL, status);
      HASH_GET_CLASS_CONSTANT(0x2240BAE4B416D850LL, replacer);
      break;
    case 86:
      HASH_GET_CLASS_CONSTANT(0x1969AA24C9CF8556LL, getdb);
      HASH_GET_CLASS_CONSTANT(0x1FE53785BD47A556LL, skinmonobook);
      break;
    case 89:
      HASH_GET_CLASS_CONSTANT(0x41F18C209AE6D959LL, ppframe_dom);
      break;
    case 91:
      HASH_GET_CLASS_CONSTANT(0x6680147A5CCA355BLL, magicwordarray);
      break;
    case 95:
      HASH_GET_CLASS_CONSTANT(0x359A492673A57F5FLL, utfnormal);
      break;
    case 99:
      HASH_GET_CLASS_CONSTANT(0x0E87D23B697F8063LL, skin);
      break;
    case 102:
      HASH_GET_CLASS_CONSTANT(0x4547ADA8DCEEF266LL, ppnode_dom);
      break;
    case 108:
      HASH_GET_CLASS_CONSTANT(0x57B36C06F20EC56CLL, quicktemplate);
      break;
    case 113:
      HASH_GET_CLASS_CONSTANT(0x5D070E434E31EF71LL, ansitermcolorer);
      break;
    case 115:
      HASH_GET_CLASS_CONSTANT(0x2FCD01C05FEE3B73LL, revision);
      HASH_GET_CLASS_CONSTANT(0x43A8AC6D22474973LL, ppnode);
      break;
    case 120:
      HASH_GET_CLASS_CONSTANT(0x618EBE2D39198378LL, linkbatch);
      HASH_GET_CLASS_CONSTANT(0x2A74B7AA03B66078LL, block);
      HASH_GET_CLASS_CONSTANT(0x023A5A2B5DB08278LL, titlearray);
      break;
    case 132:
      HASH_GET_CLASS_CONSTANT(0x3E6E89ADFEAEF284LL, stringutils);
      break;
    case 134:
      HASH_GET_CLASS_CONSTANT(0x40DBD1FF40845386LL, filecache);
      break;
    case 136:
      HASH_GET_CLASS_CONSTANT(0x69634AA9A7570D88LL, stubuser);
      HASH_GET_CLASS_CONSTANT(0x2EC2DAC3B2E2EF88LL, replacementarray);
      break;
    case 138:
      HASH_GET_CLASS_CONSTANT(0x5530B55C5821638ALL, provetestrecorder);
      HASH_GET_CLASS_CONSTANT(0x063C3117451D0D8ALL, recentchange);
      break;
    case 143:
      HASH_GET_CLASS_CONSTANT(0x465F5F4F142EDE8FLL, parser);
      break;
    case 144:
      HASH_GET_CLASS_CONSTANT(0x0301D2F46DDC7F90LL, linkholderarray);
      break;
    case 147:
      HASH_GET_CLASS_CONSTANT(0x7C178B2BFCA03693LL, parseroptions);
      break;
    case 148:
      HASH_GET_CLASS_CONSTANT(0x5237B58036959B94LL, monobooktemplate);
      break;
    case 150:
      HASH_GET_CLASS_CONSTANT(0x46EA41789271F796LL, mwexception);
      break;
    case 153:
      HASH_GET_CLASS_CONSTANT(0x235A20528EB49A99LL, article);
      break;
    case 154:
      HASH_GET_CLASS_CONSTANT(0x34DF39CB9333BC9ALL, dbtestrecorder);
      break;
    case 155:
      HASH_GET_CLASS_CONSTANT(0x4415A62DC36C579BLL, testrecorder);
      break;
    case 157:
      HASH_GET_CLASS_CONSTANT(0x5C22714D1FD2AD9DLL, stubobject);
      break;
    case 162:
      HASH_GET_CLASS_CONSTANT(0x3288321CF00F37A2LL, dummytermcolorer);
      break;
    case 165:
      HASH_GET_CLASS_CONSTANT(0x5A05AA8027F4B1A5LL, fakeconverter);
      break;
    case 167:
      HASH_GET_CLASS_CONSTANT(0x1EDAA558735CEAA7LL, hashtablereplacer);
      break;
    case 168:
      HASH_GET_CLASS_CONSTANT(0x292F71DA174B8FA8LL, coreparserfunctions);
      break;
    case 169:
      HASH_GET_CLASS_CONSTANT(0x4B93A289B6561FA9LL, memcachedclientforwiki);
      break;
    case 170:
      HASH_GET_CLASS_CONSTANT(0x5864D527BF8B26AALL, repogroup);
      break;
    case 173:
      HASH_GET_CLASS_CONSTANT(0x55DB9FC9EF75BBADLL, parsertest);
      break;
    case 175:
      HASH_GET_CLASS_CONSTANT(0x1B35D2121776BEAFLL, stubuserlang);
      break;
    case 177:
      HASH_GET_CLASS_CONSTANT(0x7E1D464E403FBAB1LL, profiler);
      break;
    case 179:
      HASH_GET_CLASS_CONSTANT(0x7CDB2B8E09A18DB3LL, htmlcacheupdate);
      break;
    case 182:
      HASH_GET_CLASS_CONSTANT(0x1FB0C5A1E83638B6LL, pptemplateframe_dom);
      break;
    case 188:
      HASH_GET_CLASS_CONSTANT(0x7D281747E80237BCLL, titlearrayfromresult);
      break;
    case 193:
      HASH_GET_CLASS_CONSTANT(0x758B1E2426B4FCC1LL, onlyincludereplacer);
      break;
    case 197:
      HASH_GET_CLASS_CONSTANT(0x7CBBE2907466E7C5LL, ppcustomframe_dom);
      break;
    case 203:
      HASH_GET_CLASS_CONSTANT(0x638DFF1F9ED1A7CBLL, mwnamespace);
      break;
    case 208:
      HASH_GET_CLASS_CONSTANT(0x1750D618C7769DD0LL, sanitizer);
      break;
    case 210:
      HASH_GET_CLASS_CONSTANT(0x6566F74128FE72D2LL, ppdstackelement);
      break;
    case 211:
      HASH_GET_CLASS_CONSTANT(0x6AEDB6AEFDF88DD3LL, errorpageerror);
      break;
    case 218:
      HASH_GET_CLASS_CONSTANT(0x1A6AAD89F34B37DALL, dbtestpreviewer);
      break;
    case 222:
      HASH_GET_CLASS_CONSTANT(0x1FFE0211B5F57DDELL, parseroutput);
      break;
    case 223:
      HASH_GET_CLASS_CONSTANT(0x3688C6C8D473D7DFLL, fakememcachedclient);
      break;
    case 224:
      HASH_GET_CLASS_CONSTANT(0x7548FC1BD48B04E0LL, searchupdate);
      break;
    case 227:
      HASH_GET_CLASS_CONSTANT(0x025B7C56B69451E3LL, ppdstack);
      break;
    case 233:
      HASH_GET_CLASS_CONSTANT(0x3F43904B4376CCE9LL, passworderror);
      HASH_GET_CLASS_CONSTANT(0x3539F3FCCD5001E9LL, fatalerror);
      break;
    case 235:
      HASH_GET_CLASS_CONSTANT(0x132FD54E0D0A99EBLL, title);
      break;
    case 237:
      HASH_GET_CLASS_CONSTANT(0x7C7CEDA62E334FEDLL, ppdpart);
      break;
    case 240:
      HASH_GET_CLASS_CONSTANT(0x3C53864DC03D20F0LL, user);
      break;
    case 241:
      HASH_GET_CLASS_CONSTANT(0x3B5AAAC3203573F1LL, proveparsertest);
      break;
    default:
      break;
  }
  return get_builtin_class_constant(s, constant);
}

///////////////////////////////////////////////////////////////////////////////
}
