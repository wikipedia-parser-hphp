
#ifndef __GENERATED_cls_htmlfilecache_h__
#define __GENERATED_cls_htmlfilecache_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 75 */
class c_htmlfilecache : virtual public ObjectData {
  BEGIN_CLASS_MAP(htmlfilecache)
  END_CLASS_MAP(htmlfilecache)
  DECLARE_CLASS(htmlfilecache, HTMLFileCache, ObjectData)
  void init();
  public: static void ti_clearfilecache(const char* cls);
  public: static void t_clearfilecache() { ti_clearfilecache("htmlfilecache"); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_htmlfilecache_h__
