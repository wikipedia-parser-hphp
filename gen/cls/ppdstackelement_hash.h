
#ifndef __GENERATED_cls_ppdstackelement_hash_h__
#define __GENERATED_cls_ppdstackelement_hash_h__

#include <cls/ppdstackelement.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_Hash.php line 677 */
class c_ppdstackelement_hash : virtual public c_ppdstackelement {
  BEGIN_CLASS_MAP(ppdstackelement_hash)
    PARENT_CLASS(ppdstackelement)
  END_CLASS_MAP(ppdstackelement_hash)
  DECLARE_CLASS(ppdstackelement_hash, PPDStackElement_Hash, ppdstackelement)
  void init();
  public: void t___construct(Variant v_data = ScalarArrays::sa_[0]);
  public: ObjectData *create(Variant v_data = ScalarArrays::sa_[0]);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Variant t_breaksyntax(Variant v_openingCount = false);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppdstackelement_hash_h__
