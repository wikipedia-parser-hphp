
#ifndef __GENERATED_cls_ppdstack_hash_h__
#define __GENERATED_cls_ppdstack_hash_h__

#include <cls/ppdstack.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_Hash.php line 666 */
class c_ppdstack_hash : virtual public c_ppdstack {
  BEGIN_CLASS_MAP(ppdstack_hash)
    PARENT_CLASS(ppdstack)
  END_CLASS_MAP(ppdstack_hash)
  DECLARE_CLASS(ppdstack_hash, PPDStack_Hash, ppdstack)
  void init();
  public: void t___construct();
  public: ObjectData *create();
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppdstack_hash_h__
