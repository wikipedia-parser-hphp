
#ifndef __GENERATED_cls_mwnamespace_h__
#define __GENERATED_cls_mwnamespace_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Parser2.php line 1068 */
class c_mwnamespace : virtual public ObjectData {
  BEGIN_CLASS_MAP(mwnamespace)
  END_CLASS_MAP(mwnamespace)
  DECLARE_CLASS(mwnamespace, MWNamespace, ObjectData)
  void init();
  public: static bool ti_ismovable(const char* cls, CVarRef v_index);
  public: static bool ti_ismain(const char* cls, CVarRef v_index);
  public: static bool ti_istalk(const char* cls, CVarRef v_index);
  public: static Variant ti_gettalk(const char* cls, CVarRef v_index);
  public: static Variant ti_getsubject(const char* cls, CVarRef v_index);
  public: static Variant ti_getcanonicalname(const char* cls, CVarRef v_index);
  public: static Variant ti_getcanonicalindex(const char* cls, CVarRef v_name);
  public: static bool ti_cantalk(const char* cls, CVarRef v_index);
  public: static bool ti_iscontent(const char* cls, CVarRef v_index);
  public: static bool ti_iswatchable(const char* cls, CVarRef v_index);
  public: static bool ti_hassubpages(const char* cls, CVarRef v_index);
  public: static bool t_ismovable(CVarRef v_index) { return ti_ismovable("mwnamespace", v_index); }
  public: static bool t_hassubpages(CVarRef v_index) { return ti_hassubpages("mwnamespace", v_index); }
  public: static bool t_iswatchable(CVarRef v_index) { return ti_iswatchable("mwnamespace", v_index); }
  public: static bool t_iscontent(CVarRef v_index) { return ti_iscontent("mwnamespace", v_index); }
  public: static Variant t_getsubject(CVarRef v_index) { return ti_getsubject("mwnamespace", v_index); }
  public: static bool t_istalk(CVarRef v_index) { return ti_istalk("mwnamespace", v_index); }
  public: static bool t_cantalk(CVarRef v_index) { return ti_cantalk("mwnamespace", v_index); }
  public: static Variant t_gettalk(CVarRef v_index) { return ti_gettalk("mwnamespace", v_index); }
  public: static Variant t_getcanonicalname(CVarRef v_index) { return ti_getcanonicalname("mwnamespace", v_index); }
  public: static Variant t_getcanonicalindex(CVarRef v_name) { return ti_getcanonicalindex("mwnamespace", v_name); }
  public: static bool t_ismain(CVarRef v_index) { return ti_ismain("mwnamespace", v_index); }
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_mwnamespace_h__
