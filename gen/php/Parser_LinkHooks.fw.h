
#ifndef __GENERATED_php_Parser_LinkHooks_fw_h__
#define __GENERATED_php_Parser_LinkHooks_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const Variant k_SLH_PATTERN;


// 2. Classes
FORWARD_DECLARE_CLASS(parser_linkhooks)
extern const StaticString q_parser_linkhooks_VERSION;
extern const int64 q_parser_linkhooks_SLH_PATTERN;
extern const StaticString q_parser_linkhooks_EXT_LINK_URL_CLASS;
extern const StaticString q_parser_linkhooks_EXT_IMAGE_REGEX;
FORWARD_DECLARE_CLASS(linkmarkerreplacer)

///////////////////////////////////////////////////////////////////////////////
}
#include <php/CoreLinkFunctions.fw.h>
#include <php/CoreParserFunctions.fw.h>
#include <php/LinkHolderArray.fw.h>
#include <php/Parser.fw.h>

#endif // __GENERATED_php_Parser_LinkHooks_fw_h__
