
#ifndef __GENERATED_php_CoreLinkFunctions_h__
#define __GENERATED_php_CoreLinkFunctions_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/CoreLinkFunctions.fw.h>

// Declarations
#include <cls/corelinkfunctions.h>
#include <php/Parser_LinkHooks.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$CoreLinkFunctions_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_corelinkfunctions(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_CoreLinkFunctions_h__
