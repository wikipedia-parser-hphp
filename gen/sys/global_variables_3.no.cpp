
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 255) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 10:
      HASH_INDEX(0x62566BC9A306C80ALL, wgAllowDisplayTitle, 66);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x34441773493C6710LL, wgEnableImageWhitelist, 20);
      break;
    case 18:
      HASH_INDEX(0x19A9C284C412E512LL, wgServerName, 43);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      HASH_INDEX(0x2FF9B075F5BFC313LL, wgExpensiveParserFunctionLimit, 36);
      break;
    case 21:
      HASH_INDEX(0x3AD645EB48693B15LL, wgTitle, 55);
      break;
    case 30:
      HASH_INDEX(0x70FBE2FBA2859F1ELL, wgCategoryPrefixedDefaultSortkey, 56);
      break;
    case 38:
      HASH_INDEX(0x5713CE531BD6F326LL, wgNoFollowDomainExceptions, 41);
      break;
    case 50:
      HASH_INDEX(0x7D8C1AC5E54A2F32LL, wgSitename, 42);
      break;
    case 51:
      HASH_INDEX(0x296D9A8065A85433LL, wgUseTeX, 15);
      break;
    case 54:
      HASH_INDEX(0x0F25E179DFFE0636LL, wgUseTidy, 34);
      break;
    case 60:
      HASH_INDEX(0x3354B8ACF06BC43CLL, wgMaxArticleSize, 22);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 68:
      HASH_INDEX(0x167513A6A61FC144LL, wgDebugTidy, 65);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 74:
      HASH_INDEX(0x323EAC15D211984ALL, wgContLang, 32);
      break;
    case 76:
      HASH_INDEX(0x7ABD1C02DBBBE94CLL, IP, 64);
      break;
    case 77:
      HASH_INDEX(0x3A5C7A341C25754DLL, wgServer, 12);
      break;
    case 78:
      HASH_INDEX(0x5C8355EBD3A39F4ELL, wgTidyOpts, 63);
      break;
    case 80:
      HASH_INDEX(0x30E66D49BBCCAD50LL, wgMemc, 30);
      break;
    case 89:
      HASH_INDEX(0x6A62472A9F803459LL, wgRegisterInternalExternals, 13);
      HASH_INDEX(0x10F91D80DF2BB559LL, wgLocaltimezone, 46);
      break;
    case 91:
      HASH_INDEX(0x0139765761C67A5BLL, wgTidyInternal, 60);
      break;
    case 105:
      HASH_INDEX(0x2E259B8159F52469LL, wgInterwikiMagic, 17);
      break;
    case 108:
      HASH_INDEX(0x10B90601AD3E2C6CLL, wgLinkHolderBatchSize, 33);
      HASH_INDEX(0x0318A2A42E99B86CLL, wgLegalTitleChars, 53);
      break;
    case 110:
      HASH_INDEX(0x71CF7D337911E36ELL, wgScriptPath, 44);
      break;
    case 114:
      HASH_INDEX(0x6E99E49E33B88472LL, wgAllowExternalImages, 18);
      break;
    case 123:
      HASH_INDEX(0x11B7E569AC1CB27BLL, wgEnforceHtmlIds, 52);
      break;
    case 129:
      HASH_INDEX(0x6532D7EA771A6881LL, wgEnableScaryTranscluding, 49);
      break;
    case 136:
      HASH_INDEX(0x43CAB6B0484C7788LL, wgContLanguageCode, 47);
      break;
    case 140:
      HASH_INDEX(0x1936366D2D1FA48CLL, wgMaxPPExpandDepth, 25);
      break;
    case 142:
      HASH_INDEX(0x143D72CD152B8F8ELL, wgTidyConf, 61);
      break;
    case 144:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 150:
      HASH_INDEX(0x2032537EAFB84696LL, wgNoFollowLinks, 39);
      break;
    case 156:
      HASH_INDEX(0x3AD9F28E2BBC1E9CLL, wgUser, 28);
      break;
    case 159:
      HASH_INDEX(0x786379403A37E89FLL, wgAllowExternalImagesFrom, 19);
      HASH_INDEX(0x7197AC66F7344F9FLL, wgExternalLinkTarget, 27);
      break;
    case 161:
      HASH_INDEX(0x5739DEAC95D0EDA1LL, wgTidyBin, 62);
      break;
    case 163:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      HASH_INDEX(0x22BB9E58DA1D68A3LL, wgMaxTemplateDepth, 24);
      break;
    case 171:
      HASH_INDEX(0x4243E2C0D99B5EABLL, wgPreprocessorCacheThreshold, 31);
      break;
    case 178:
      HASH_INDEX(0x45B9FC509805E5B2LL, wgMaxSigChars, 54);
      break;
    case 179:
      HASH_INDEX(0x2A71D707D84BDDB3LL, wgAlwaysUseTidy, 35);
      break;
    case 191:
      HASH_INDEX(0x512BB6600AF1A1BFLL, wgCacheEpoch, 14);
      break;
    case 194:
      HASH_INDEX(0x414DA0222C674AC2LL, wgParserCacheExpireTime, 59);
      break;
    case 196:
      HASH_INDEX(0x43E03BE5B85A1DC4LL, wgAllowSlowParserFunctions, 67);
      break;
    case 198:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      break;
    case 201:
      HASH_INDEX(0x2FCD4CD058FE6AC9LL, wgMaxTocLevel, 51);
      break;
    case 202:
      HASH_INDEX(0x45F7742DD17392CALL, wgRawHtml, 38);
      break;
    case 206:
      HASH_INDEX(0x32E38C199DC208CELL, wgCleanSignatures, 26);
      break;
    case 209:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      break;
    case 213:
      HASH_INDEX(0x6BFA60F06006B7D5LL, wgUseDynamicDates, 16);
      break;
    case 216:
      HASH_INDEX(0x00B8EAEC735E0DD8LL, wgNoFollowNsExceptions, 40);
      break;
    case 226:
      HASH_INDEX(0x2119480B686F60E2LL, wgRequest, 58);
      break;
    case 228:
      HASH_INDEX(0x0E4F0A1D74C1FBE4LL, wgRestrictDisplayTitle, 68);
      break;
    case 237:
      HASH_INDEX(0x34DEC5871A3E97EDLL, wgAllowSpecialInclusion, 21);
      break;
    case 238:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 239:
      HASH_INDEX(0x61FA7102D48317EFLL, wgMaxPPNodeCount, 23);
      break;
    case 242:
      HASH_INDEX(0x2E884B89708A87F2LL, wgHooks, 29);
      break;
    case 245:
      HASH_INDEX(0x43FBB2C4FE020BF5LL, wgNonincludableNamespaces, 48);
      break;
    case 246:
      HASH_INDEX(0x299EDB527A96ADF6LL, wgStylePath, 45);
      break;
    case 248:
      HASH_INDEX(0x2E3A9BE7A3D449F8LL, wgLang, 37);
      HASH_INDEX(0x107325AF94A9A5F8LL, wgTranscludeCacheExpiry, 50);
      break;
    case 250:
      HASH_INDEX(0x25D14844D69061FALL, parserMemc, 57);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 69) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
