
#ifndef __GENERATED_cls_ppdpart_hash_h__
#define __GENERATED_cls_ppdpart_hash_h__

#include <cls/ppdpart.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: Preprocessor_Hash.php line 712 */
class c_ppdpart_hash : virtual public c_ppdpart {
  BEGIN_CLASS_MAP(ppdpart_hash)
    PARENT_CLASS(ppdpart)
  END_CLASS_MAP(ppdpart_hash)
  DECLARE_CLASS(ppdpart_hash, PPDPart_Hash, ppdpart)
  void init();
  public: void t___construct(Variant v_out = "");
  public: ObjectData *create(Variant v_out = "");
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_ppdpart_hash_h__
